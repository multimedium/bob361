<?php
$routing = array(
	'thumbnail(:any)'		=> 'thumbnail$1',
	'admin(:any)'			=> 'admin$1',
	'install(:any)'			=> 'install$1',
	'download(:any)'		=> 'download$1',
	
	'home(:any)'            => 'home$1',
	'team(:any)'            => 'team$1',
	'projects(:any)'        => 'portfolio$1',
	'news(:any)'			=> 'news$1',
	'publications(:any)'    => 'news/publications/$1',
	'jobs(:any)'			=> 'news/jobs/$1',
	'awards(:any)'			=> 'news/awards/$1',
	'office(:any)'			=> 'team/$1',

    'search(:any)'          => 'search$1',

	'rss/(:string)'         => 'news/rss/$1',
	'sitemap.xml'           => 'sitemap',
	'(:any)'				=> 'text/detail/$1'
);

/*
M_Debug::setDebugMode(true);
M_Debug::setDebugEmailRecipient('niels.verbist@multimedium.be');
M_Debug::setCacheBusting(TRUE, new M_Date('20-10-2014 10:28'));
M_Debug::setGrowlNotifications(true);
*/