<?php
/**
 * M_DateFormatPlaceholdersIt
 * 
 * @package Core
 */
class M_DateFormatPlaceholdersIt implements MI_DateFormatPlaceholders {
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get placeholders
	 * 
	 * @access public
	 * @return array $placeholders
	 *		Returns an associative array, of which the keys are the date part
	 *		symbols used in date formats, and the values are the corresponding
	 *		placeholders
	 */
	public static function getPlaceholders() {
		return array(
			'yyyyy' => 'aaaaa',
			'yyyy'  => 'aaaa',
			'yyy'   => 'aaaa',
			'yy'    => 'aa',
			'y'     => 'aaaa',
			'YYYY'  => 'aaaa',
			'YYY'   => 'aaa',
			'YY'    => 'aa',
			'MMMMM' => 'm',
			'MMMM'  => 'mese',
			'MMM'   => 'mese',
			'MM'    => 'mm',
			'M'     => 'm',
			'dd'    => 'giorno',
			'd'     => 'giorno',
			'EEEEE' => 'giorno (pe. M)',
			'EEEE'  => 'giorno (pe. martedì)',
			'EEE'   => 'giorno (pe. mar)',
			'EE'    => 'giorno (pe. mar)',
			'E'     => 'giorno (pe. mar)',
			'ee'    => 'giorno della settimana (pe. 01)',
			'e'     => 'giorno della settimana (pe. 1)',
			'a'     => 'AM',
			'hh'    => 'oo',
			'h'     => 'o',
			'HH'    => 'oo',
			'H'     => 'o',
			'KK'    => 'oo',
			'K'     => 'o',
			'kk'    => 'oo',
			'k'     => 'o',
			'mm'    => 'mm',
			'm'     => 'm',
			'ss'    => 'ss',
			's'     => 's'
		);
	}
}