<?php
/**
 * M_DateTimezoneDefinitionEuropeBrussels class
 * 
 * Will construct an instance of{@link M_DateTimezoneDefinition} which contains
 * the standard values of the timezone Europe/Brussels. Note that these
 * standard values are derived from various sources on the internet,
 * specifically the start dates for the Standard Time and Daylight Saving Time
 * components.
 * 
 * @author Tim Segers
 * @package Core
 */
class M_DateTimezoneDefinitionEuropeBrussels extends M_DateTimezoneDefinition {
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		// Set the default values for the timezone, including the timezone
		// ID and the (optional) last modified date
		$this->setTimezoneId('Europe/Brussels');
		$this->setLastModifiedDate(new M_Date(array(
			'year'		=>	2012,
			'month'		=>	3,
			'day'		=>	13,
			'hour'		=>	0,
			'minute'	=>	0,
			'second'	=>	0
		)));
		
		// Create a new Standard Time component
		$component = new M_DateTimezoneComponent();
		$component->setType(M_DateTimezoneComponent::TYPE_STANDARD);
		
		// Set the (optional) name, for easier use
		$component->setName('Europe/Brussels Standard Time');
		
		// Set its start date
		$component->setStartDate(new M_Date(array(
			'year'		=>	1970,
			'month'		=>	10,
			'day'		=>	25,
			'hour'		=>	3,
			'minute'	=>	0,
			'second'	=>	0
		)));
		
		// GMT offset before transition
		$component->setOffsetFrom('+0200');
		
		// GMT offset after transition
		$component->setOffsetTo('+0100');
		
		// Create the recurrence rule for Standard time
		$rrule = new M_CalendarRecurrenceRule();
		$rrule->setFrequency(M_CalendarRecurrenceRule::FREQ_YEARLY);
		$rrule->addMonth(10);
		$rrule->addWeekday(M_CalendarRecurrenceRule::SUNDAY, -1);
		$rrule->setInterval(1);
		
		// Adding it to the component
		$component->setRecurrenceRule($rrule);
		
		// Now add the component to the timezone definition
		$this->addComponent($component);
		
		// Now create a new Daylight Saving Time Component
		$component = new M_DateTimezoneComponent();
		$component->setType(M_DateTimezoneComponent::TYPE_DAYLIGHT);
		
		// Set the (optional) name, for easier use
		$component->setName('Europe/Brussels Daylight Saving Time');
		
		// Set its start date
		$component->setStartDate(new M_Date(array(
			'year'		=>	1970,
			'month'		=>	3,
			'day'		=>	29,
			'hour'		=>	2,
			'minute'	=>	0,
			'second'	=>	0
		)));
		
		// GMT offset before transition
		$component->setOffsetFrom('+0100');
		
		// GMT offset after transition
		$component->setOffsetTo('+0200');
		
		// Create the recurrence rule for Standard time
		$rrule = new M_CalendarRecurrenceRule();
		$rrule->setFrequency(M_CalendarRecurrenceRule::FREQ_YEARLY);
		$rrule->addMonth(3);
		$rrule->addWeekday(M_CalendarRecurrenceRule::SUNDAY, -1);
		$rrule->setInterval(1);
		
		// Adding it to the component
		$component->setRecurrenceRule($rrule);
		
		// Now add the component to the timezone definition
		$this->addComponent($component);
	}
}