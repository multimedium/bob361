<?php
/**
 * M_Date class
 * 
 * @package Core
 */

// Set default timezone:
// (M_Date obviously uses date functions. This would generate notices, since PHP
// version 5.3.0, if no default timezone would have been set previously. Therefor,
// we check if a timezone has been set and, if not already done, we set the 
// default timezone to Europe/Brussels)
if(! M_DateTimezone::isDefaultTimezoneAvailable()) {
	M_DateTimezone::setDefaultTimezone('Europe/Brussels');
}

/**
 * M_Date class
 * 
 * The M_Date class is used to handle dates (+time). It offers
 * tools to manipulate dates, perform time calculations and
 * to run time validations.
 * 
 * NOTE:
 * M_Date is part of the Locale API. For more information, start the
 * intro at {@link M_Locale}
 * 
 * @package Core
 */
class M_Date {
	
	/* -- CONSTANTS -- */
	
	/**
	 * M_Date part constant
	 * 
	 * This constant can be used to address a given part of the date.
	 * For examples on how this constant may be used, read the docs at
	 * 
	 * - {@link M_Date::get()}
	 * - {@link M_Date::set()}
	 * - {@link M_Date::addTime()}
	 * - {@link M_Date::subtractTime()}
	 * - {@link M_Date::getElapsedTimeSince()}
	 * - {@link M_Date::getTimeArray()}
	 * - {@link M_Date::getNumberOfSeconds()}
	 * - etc...
	 * 
	 * This specific constant is used to address the year that is 
	 * contained by the date object.
	 */
	const YEAR        = 'year';
	
	/**
	 * M_Date part constant
	 * 
	 * This constant can be used to address a given part of the date.
	 * For examples on how this constant may be used, read the docs at
	 * 
	 * - {@link M_Date::get()}
	 * - {@link M_Date::set()}
	 * - {@link M_Date::addTime()}
	 * - {@link M_Date::subtractTime()}
	 * - {@link M_Date::getElapsedTimeSince()}
	 * - {@link M_Date::getTimeArray()}
	 * - {@link M_Date::getNumberOfSeconds()}
	 * - etc...
	 * 
	 * This specific constant is used to address the month that is 
	 * contained by the date object.
	 */
	const MONTH       = 'month';
	
	/**
	 * M_Date part constant
	 * 
	 * This constant can be used to address a given part of the date.
	 * For examples on how this constant may be used, read the docs at
	 * 
	 * - {@link M_Date::get()}
	 * - {@link M_Date::set()}
	 * - etc...
	 * 
	 * This specific constant is used to address the day (of the month) 
	 * that is contained by the date object.
	 */
	const DAY         = 'day';
	
	/**
	 * M_Date part constant
	 * 
	 * This constant can be used to address a given part of the date.
	 * For examples on how this constant may be used, read the docs at
	 * 
	 * - {@link M_Date::get()}
	 * - {@link M_Date::set()}
	 * - etc...
	 * 
	 * This specific constant is used to address the day (of the week) 
	 * that is contained by the date object.
	 */
	const WEEKDAY     = 'weekday';
	
	/**
	 * M_Date part constant
	 * 
	 * This constant can be used to address a given part of the date.
	 * For examples on how this constant may be used, read the docs at
	 * 
	 * - {@link M_Date::get()}
	 * - {@link M_Date::set()}
	 * - {@link M_Date::addTime()}
	 * - {@link M_Date::subtractTime()}
	 * - {@link M_Date::getElapsedTimeSince()}
	 * - {@link M_Date::getTimeArray()}
	 * - {@link M_Date::getNumberOfSeconds()}
	 * - etc...
	 * 
	 * This specific constant is used to address the hour that is 
	 * contained by the date object.
	 */
	const HOUR        = 'hour';
	
	/**
	 * M_Date part constant
	 * 
	 * This constant can be used to address a given part of the date.
	 * For examples on how this constant may be used, read the docs at
	 * 
	 * - {@link M_Date::get()}
	 * - {@link M_Date::set()}
	 * - {@link M_Date::addTime()}
	 * - {@link M_Date::subtractTime()}
	 * - {@link M_Date::getElapsedTimeSince()}
	 * - {@link M_Date::getTimeArray()}
	 * - {@link M_Date::getNumberOfSeconds()}
	 * - etc...
	 * 
	 * This specific constant is used to address the number of minutes 
	 * that is contained by the date object.
	 */
	const MINUTE      = 'minute';
	
	/**
	 * M_Date part constant
	 * 
	 * This constant can be used to address a given part of the date.
	 * For examples on how this constant may be used, read the docs at
	 * 
	 * - {@link M_Date::get()}
	 * - {@link M_Date::set()}
	 * - {@link M_Date::addTime()}
	 * - {@link M_Date::subtractTime()}
	 * - {@link M_Date::getElapsedTimeSince()}
	 * - {@link M_Date::getTimeArray()}
	 * - {@link M_Date::getNumberOfSeconds()}
	 * - etc...
	 * 
	 * This specific constant is used to address the number of seconds 
	 * that is contained by the date object.
	 */
	const SECOND      = 'second';
	
	/**
	 * M_Date part constant
	 * 
	 * This constant can be used to address a given part of the date.
	 * For examples on how this constant may be used, read the docs at
	 * 
	 * - {@link M_Date::get()}
	 * - {@link M_Date::set()}
	 * 
	 * This specific constant is used to address the UNIX Timestamp 
	 * that represents the date and time.
	 */
	const TIMESTAMP   = 'timestamp';
	
	/**
	 * M_Date part constant
	 * 
	 * This constant can be used to address a given part of the date.
	 * For examples on how this constant may be used, read the docs at
	 * 
	 * - {@link M_Date::get()}
	 * - {@link M_Date::set()}
	 * 
	 * This specific constant is used to address the week number of
	 * the date object.
	 */
	const WEEK        = 'week';
	
	/**
	 * Timespan constant
	 * 
	 * This constant holds the number of seconds in a year. This constant
	 * is available in the public API, and (among others) improves 
	 * readability of written code.
	 */
	const SECONDS_IN_YEAR   = 31556926;
	
	/**
	 * Timespan constant
	 * 
	 * This constant holds the number of seconds in a month. This constant
	 * is available in the public API, and (among others) improves 
	 * readability of written code.
	 */
	const SECONDS_IN_MONTH  = 2629743.83;
	
	/**
	 * Timespan constant
	 * 
	 * This constant holds the number of seconds in a week. This constant
	 * is available in the public API, and (among others) improves 
	 * readability of written code.
	 */
	const SECONDS_IN_WEEK   = 604800;
	
	/**
	 * Timespan constant
	 * 
	 * This constant holds the number of seconds in a day. This constant
	 * is available in the public API, and (among others) improves 
	 * readability of written code.
	 */
	const SECONDS_IN_DAY    = 86400;
	
	/**
	 * Timespan constant
	 * 
	 * This constant holds the number of seconds in an hour. This constant
	 * is available in the public API, and (among others) improves 
	 * readability of written code.
	 */
	const SECONDS_IN_HOUR   = 3600;
	
	/**
	 * Timespan constant
	 * 
	 * This constant holds the number of seconds in a minute. This constant
	 * is available in the public API, and (among others) improves 
	 * readability of written code.
	 */
	const SECONDS_IN_MINUTE = 60;
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const FULL = 'full';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const LONG = 'long';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const MEDIUM = 'medium';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const SHORT = 'short';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const FULL_TIME = 'full-time';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const LONG_TIME = 'long-time';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const MEDIUM_TIME = 'medium-time';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const SHORT_TIME = 'short-time';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const FULL_DATE_TIME = 'full-date-time';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const LONG_DATE_TIME = 'long-date-time';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const MEDIUM_DATE_TIME = 'medium-date-time';
	
	/**
	 * Date Format
	 * 
	 * This constant can be used to address a given date format. For more
	 * information about formatting dates, read the documentation at
	 * {@link M_Date::toString()}.
	 */
	const SHORT_DATE_TIME = 'short-date-time';
	
	/**
	 * Weekday constant
	 * 
	 * This constant can be used to address a given weekday. This specific constant
	 * is used to refer to Monday. For example, this constant is currently in use
	 * by {@link M_Date::isMonday()}
	 */
	const MONDAY = 1;
	
	/**
	 * Weekday constant
	 * 
	 * This constant can be used to address a given weekday. This specific constant
	 * is used to refer to Tuesday. For example, this constanM_Dateurrently in use
	 * by {@link M_Date::isTuesday()}
	 */
	const TUESDAY = 2;
	
	/**
	 * Weekday constant
	 * 
	 * This constant can be used to address a given weekday. This specific constant
	 * is used to refer to Wednesday. For example, this constant is currently in use
	 * by {@link M_Date::isWednesday()}
	 */
	const WEDNESDAY = 3;
	
	/**
	 * Weekday constant
	 * 
	 * This constant can be used to address a given weekday. This specific constant
	 * is used to refer to Thursday. For example, this constant is currently in use
	 * by {@link M_Date::isThursday()}
	 */
	const THURSDAY = 4;
	
	/**
	 * Weekday constant
	 * 
	 * This constant can be used to address a given weekday. This specific constant
	 * is used to refer to Friday. For example, this constant is currently in use
	 * by {@link M_Date::isFriday()}
	 */
	const FRIDAY = 5;
	
	/**
	 * Weekday constant
	 * 
	 * This constant can be used to address a given weekday. This specific constant
	 * is used to refer to Saturday. For example, this constant is currently in use
	 * by {@link M_Date::isSaturday()}
	 */
	const SATURDAY = 6;
	
	/**
	 * Weekday constant
	 * 
	 * This constant can be used to address a given weekday. This specific constant
	 * is used to refer to Sunday. For example, this constant is currently in use
	 * by {@link M_Date::isSunday()}
	 */
	const SUNDAY = 0;
	
	/* -- PROPERTIES -- */
	
	/**
	 * Internal Variable: UNIX Timestamp
	 * 
	 * This property holds the UNIX timestamp, representing
	 * all date parts contained in the object.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_timestamp;
	
	/**
	 * Internal Variable: Year
	 * 
	 * @access private
	 * @var integer
	 */
	private $_year;
	
	/**
	 * Internal Variable: Month
	 * 
	 * @access private
	 * @var integer
	 */
	private $_month;
	
	/**
	 * Internal Variable: Day (of the month)
	 * 
	 * @access private
	 * @var integer
	 */
	private $_day;
	
	/**
	 * Internal Variable: Day (of the week)
	 * 
	 * This property holds a numeric representation of the day
	 * of the week (0 for Sunday, 6 for Saturday).
	 * 
	 * @access private
	 * @var integer
	 */
	private $_weekday;
	
	/**
	 * Internal Variable: Hour
	 * 
	 * @access private
	 * @var integer
	 */
	private $_hour;
	
	/**
	 * Internal Variable: Minute
	 * 
	 * @access private
	 * @var integer
	 */
	private $_minute;
	
	/**
	 * Internal Variable: Second
	 * 
	 * @access private
	 * @var integer
	 */
	private $_second;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * Example 1, construct an M_Date with a timestamp
	 * <code>
	 *    // Construct an M_Date object that represents the current
	 *    // date and time
	 *    $date = new M_Date(time());
	 * </code>
	 * 
	 * Example 2, construct an M_Date with an associative array
	 * <code>
	 *    // Construct an M_Date object that represents xmas 2008
	 *    $date = new M_Date(array(
	 *       'year'   => 2008,
	 *       'month'  => 12,
	 *       'day'    => 25
	 *    ));
	 * </code>
	 * 
	 * If you construct an M_Date object with an associative array,
	 * the missing date parts will be defaulted to the current
	 * date and time. For example, if you want to construct a date
	 * that represents the same day + hour last year, you would
	 * do the following:
	 * 
	 * <code>
	 *    // Construct an M_Date object that represents the xmas 2008
	 *    $date = new M_Date(array(
	 *       'year'   => date('Y') - 1
	 *    ));
	 * </code>
	 * 
	 * Example 3, construct an M_Date object with a date string
	 * <code>
	 *    // Construct an M_Date object that represents xmas 2008
	 *    $date = new M_Date('25-12-2008');
	 *    // Some alternatives:
	 *    $date = new M_Date('2008-12-25');
	 *    $date = new M_Date('25 Dec 2008');
	 * </code>
	 * 
	 * Note that an M_Date object can be constructed without passing
	 * in any arguments. If you construct an M_Date object without 
	 * arguments, the M_Date will be defaulted to the current date 
	 * and time.
	 * 
	 * @access public
	 * @throws M_DateException
	 * @param mixed $date
	 * @return M_Date
	 */
	public function __construct($date = NULL) {
		// If a date is given:
		if($date) {
			// If the provided date is numeric, we assume it is a timestamp.
			if(is_numeric($date)) {
				$this->_initFromTimestamp((int) $date);
			}
			// If the provided date is an array, we assume it is an associative
			// array, contained the date parts to mount the corresponding UNIX
			// timestamp. Note that the missing parts in the array will be filled
			// in with the current date + time
			elseif(is_array($date)) {
				$this->_year = !isset($date['year']) ? date('Y') : (int) $date['year'];
				$this->_month = !isset($date['month']) ? date('n') : (int) $date['month'];
				$this->_day = !isset($date['day']) ? date('j') : (int) $date['day'];
				$this->_hour = !isset($date['hour']) ? date('G') : (int) $date['hour'];
				$this->_minute = !isset($date['minute']) ? (int) date('i') : (int) $date['minute'];
				$this->_second = !isset($date['second']) ? (int) date('s') : (int) $date['second'];
				$this->_timestamp = mktime(
					$this->_hour, 
					$this->_minute, 
					$this->_second, 
					$this->_month, 
					$this->_day,
					$this->_year
				);
				$this->_weekday = (int) date('w', $this->_timestamp); // 0 (for Sunday) through 6 (for Saturday)
			}
			// If the date is a string, we'll use PHP's built-in strtotime()
			// function to convert it to a timestamp.
			else {
				// strtotime() returns a timestamp on success, FALSE otherwise. 
				// Previous to PHP 5.1.0, this function would return -1 on 
				// failure. 
				$temp = strtotime($date);
				if($temp === FALSE || $temp == -1) {
					throw new M_DateException(sprintf('Failed to mount an M_Date object with the string "%s"', $date));
				} else {
					$this->_initFromTimestamp((int) $temp);
				}
			}
		}
		// If a date has not been provided to the constructor, we'll default
		// the date to the current date+time
		else {
			$this->_initFromTimestamp(time());
		}
	}
	
	/**
	 * Construct with string
	 * 
	 * This static constructor allows you to construct an instance of {@link M_Date}
	 * with a given string, a given date format, and a given locale. Let's assume
	 * that you have a Spanish string, which is describing a date in the format
	 * {@link M_Date::LONG_DATE_TIME}. That string would be
	 * 
	 * <code>12 de marzo de 2012 09:12:17</code>
	 * 
	 * With this string, you can construct an instance of {@link M_Date}, like so:
	 * 
	 * <code>
	 *    // Construct an instance of M_Date with a Spanish string
	 *    M_Date::constructWithString('12 de marzo de 2012 09:12:17', M_Date::LONG_DATE_TIME, 'es');
	 * </code>
	 * 
	 * NOTE:
	 * This method will throw an exception, if the provided string could not be 
	 * parsed into an instance of {@link M_Date}.
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * @param string $format
	 * @param string $locale
	 * @return M_Date 
	 */
	public static function constructWithString($string, $format = NULL, $locale = NULL) {
		// If no format has been provided
		if(! $format) {
			// Then, we construct the date with the regular constructor:
			return new self($string);
		}
		
		// If a locale is not provided to this function, we'll default to the 
		// locale that has been set in the LC_TIME category. If we retrieve the
		// locale from this category, it will fallback to LANG as the default.
		// So, if not provided:
		if(! $locale) {
			// Get the locale for LC_TIME:
			$locale = M_Locale::getCategory(M_Locale::LC_TIME);
		}
		
		// If still no locale could be determined with the LC_TIME category,
		if(! $locale) {
			// Then, we'll intitiate the locale data as NULL
			$data = NULL;
		}
		// If a locale has been selected:
		else {
			// We'll fetch that locale's data:
			$data = M_LocaleData::getInstance($locale);
		}
		
		// With the format provided, we get a string that describes the date format
		// using the predefined variables/placeholders.
		$format = self::getDateFormat($format, $locale);
		
		// We define a piece of regular expression for each special character
		// that we might encounter in the provided string. We'll use these pieces
		// to compose a regular expression, that can be matched against the string
		$chars = array(
			// Year. Normally the length specifies the padding, but 
			// for two letters it also specifies the maximum length.
			'yyyyy' => '([0-9]{4})',
			'yyyy'  => '([0-9]{4})',
			'yyy'   => '([0-9]{4})',
			'yy'    => '([0-9]{2})',
			'y'     => '([0-9]{4})',
			
			// Year. The number of symbols defines the length of the year number:
			'YYYY'  => '([0-9]{4})',
			'YYY'   => '([0-9]{3})',
			'YY'    => '([0-9]{2})',
			
			// Month names:
			// We match any string. We cannot match the month name with an 
			// alphanumerical pattern, because it may carry letters with accents, 
			// punctuation, and others.
			'MMMMM'  => '(.*)',
			'MMMM'   => '(.*)',
			'MMM'    => '(.*)',
			
			// Numerical months:
			'MM'    => '([0-9]{2})',
			'M'     => '([0-9]{1,2})',
			
			// Week of year
			'w'     => '([0-9]{2})',
			
			// TODO: Week of month
			// 'W'     => '',
			
			// Day of the month
			'dd'    => '([0-9]{2})',
			'd'     => '([0-9]{1,2})',
			
			// Day of the year:
			'D'     => '([0-9]{1,3})',
			
			// Day of week
			// We match any string. We cannot match the day name with an 
			// alphanumerical pattern, because it may carry letters with accents, 
			// punctuation, and others.
			'EEEEE' => '(.*)',
			'EEEE'  => '(.*)',
			'EEE'   => '(.*)',
			'EE'    => '(.*)',
			'E'     => '(.*)',
			
			// Numerical day of week:
			'ee'    => '([0-9]{2})',
			'e'     => '([0-9]{1})',
			
			// AM/PM
			// We match any string. We cannot match with an alphanumerical pattern, 
			// because it may carry letters with accents, punctuation, and others.
			'a'     => '(.*)',
			
			// Hours (0-12)
			'hh'    => '([0-9]{2})',
			'h'     => '([0-9]{1,2})',
			
			// Hours (0-23)
			'HH'    => '([0-9]{2})',
			'H'     => '([0-9]{1,2})',
			
			// Hour (0-11)
			'KK'    => '([0-9]{2})',
			'K'     => '([0-9]{1,2})',
			
			// Hour (1-24)
			'kk'    => '([0-9]{2})',
			'k'     => '([0-9]{1,2})',
			
			// Minutes
			'mm'    => '([0-9]{2})',
			'm'     => '([0-9]{1,2})',
			
			// Seconds
			'ss'    => '([0-9]{2})',
			's'     => '([0-9]{1,2})'
		);
		
		// First, we escape the provided format, so this format does not create
		// syntax errors when ran as a regular expression:
		$format = preg_quote($format, '/');
		
		// We log the placeholders we have encountered...
		$log = array();
		
		// For each of the special characters we have defined before:
		foreach($chars as $char => $pattern) {
			// The current character is...
			$oneChar = substr($char, 0, 1);
			
			// If this character has been encountered before, in another form
			// (for example, we will not parse MMM if MMMM was encountered before)
			if(in_array($oneChar, $log)) {
				// Then, we skip this iteration
				continue;
			}
			
			// Start looking for the current character...
			$pos = strpos($format, $char);
			$charLength = strlen($char);
			
			// While the character is present in the string:
			if($pos !== FALSE) {
				// Is escaped?
				$isEscaped = ($pos > 0 && substr($format, $pos - 1, 1) == '\\');
				
				// Is the character already a pointer? We can tell if it is 
				// wrapped by the ` quote
				$isWrapped = (
					! $isEscaped && 
					$pos > 0 && 
					substr($format, $pos - 1, 1) == '`' &&
					substr($format, $pos + $charLength - 2, 1) == '`'
				);
				
				// If not escaped, and not wrapped:
				if(! $isEscaped && ! $isWrapped) {
					// We replace the current character by the corresponding pattern,
					// in the provided format. Note that we add the original character,
					// wrapped with `, to the pattern. We do this in order to know what
					// this piece of expression extracts from the string. We will refer
					// to this as the "pointer"
					$format = substr($format, 0, $pos) . $pattern . '`' . $char . '`' . substr($format, $pos + $charLength);
				}
				
				// Prepare cursor for next loop:
				$pos = strpos($format, $char, $pos + $charLength);
				
				// Log the character, so other forms are not parsed:
				// (temporary fix; assuming a date part will only provided in 
				//  one format, in the date string)
				array_push($log, $oneChar);
			}
		}
		
		// By now, the provided format should have been converted to a complete
		// regular expression that can be used to match the provided string against.
		// We finish the expression, by wrapping it with the delimiters:
		$expression = '/^' . $format . '$/';
		
		// Now, we'll look for pointers in the expression (see earlier), in order 
		// to determine the position of each date part, in the pattern. This 
		// position can be used later, in order identify the extracted pieces
		// from the provided string as a specific date part.
		$match = array();
		preg_match_all('/`([^`]+)`/', $expression, $match);
		
		// We'll store the positions in:
		$positions = array();
		
		// For each of the pointers encountered:
		foreach($match[1] as $i => $currentMatch) {
			// Store the position of the current date part in the pattern:
			$positions[(string) $currentMatch] = $i + 1;
			
			// Once we have determined the position, we remove the pointer from 
			// the regular expression:
			$expression = str_replace('`' . $currentMatch . '`', '', $expression);
		}
		
		// By now, the pointers have been removed, we have a clean regular 
		// expression, and we know the position of each date part in the expression.
		// We try to match the provided string with the expression:
		if(! preg_match($expression, $string, $match)) {
			// If the provided string does not match the expression, then the 
			// date cannot be constructed. We throw an exception to inform about 
			// the error:
			throw new M_Exception(sprintf(
				'Cannot construct date with the string %s; The string does ' . 
				'match the expected format; Regular expression used: %s',
				$string,
				$expression
			));
		}
		
		// We have a match! This means that the subject has passed the first
		// test, and that the expected format has been matched. However, this
		// does not mean that the subject represents a valid date. We'll check
		// for a valid date now. But first, we collect the date parts that
		// have been extracted in the $elements array:
		$elements = array();
		foreach($positions as $element => $index) {
			$elements[$element] = $match[$index];
		}

		// We check if a day of the month has been extracted. 
		$has_dd = array_key_exists('dd', $elements);
		$has_d  = array_key_exists('d',  $elements);

		// If that is the case:
		if($has_dd || $has_d) {
			// Then, we'll use that day in order to check the date. We create
			// an additional 'day' key that stores the day extracted:
			$elements['day'] = $has_dd
				? (int) M_Helper::ltrimCharlist($elements['dd'], '0')
				: (int) $elements['d'];
		}
		// If no day available:
		else {
			// Then, we default the day to the current day:
			$elements['day'] = self::getCurrentDay();
		}

		// We check if a month has been extracted. 
		$has_MM = array_key_exists('MM', $elements);
		$has_M  = array_key_exists('M',  $elements);

		// If that is the case:
		if($has_MM || $has_M) {
			// Then, we'll use that numerical month in order to check the 
			// date. We create an additional 'month' key that stores the month
			// extracted:
			$elements['month'] = $has_MM
				? (int) M_Helper::ltrimCharlist($elements['MM'], '0')
				: (int) $elements['M'];
		}
		// If no numerical month has been extracted, we'll check for written
		// month names:
		else {
			// We check if a month name have been provided:
			$has_MMMMM = array_key_exists('MMMMM', $elements);
			$has_MMMM  = array_key_exists('MMMM',  $elements);
			$has_MMM   = array_key_exists('MMM',  $elements);

			// If so:
			if($has_MMMMM || $has_MMMM || $has_MMM) {
				// In order to match the month name in the provided string, 
				// we'll create an array of month names to match with. We 
				// initiate the array now:
				$monthNames = array();

				// If locale data is available:
				if($data) {
					// We define the format in which the month names are to
					// be fetched from the locale data:
					$monthFormat = $has_MMMMM ? 'narrow' : ($has_MMMM ? 'wide' : 'abbreviated');

					// For each of the months:
					for($i = 1; $i < 13; $i ++) {
						// We get the month name from the locale data, and 
						// we add it to the array of months:
						$monthNames[$i] = $data->getMonth($i, $monthFormat);
					}
				}
				// If no locale data is available:
				else {
					// Then, we'll default to English month names. We prepare
					// the format in which we'll fetch the month names:
					$monthFormat = $has_MMMMM ? 'M' : ($has_MMMM ? 'F' : 'M');
					$monthLength = $has_MMMMM ? 1 : NULL;

					// For each of the months:
					for($i = 1; $i < 13; $i ++) {
						// We get the month name:
						$monthNames[$i] = date($monthFormat, mktime(0, 0, 1, $i, 1, 2012));

						// If a month length has been defined:
						if($monthLength) {
							// Get a substring to match the length:
							$monthNames[$i] = substr($monthNames[$i], 0, $monthLength);
						}
					}
				}

				// Now, we have an array with month names. We'll loop through
				// this collection, in order to match one of the month names.
				// So, for each of the months:
				foreach($monthNames as $i => $monthName) {
					// If the current month matches the provided one:
					if(
						($has_MMMMM && $elements['MMMMM'] == $monthName) ||
						($has_MMMM  && $elements['MMMM']  == $monthName) ||
						($has_MMM   && $elements['MMM']   == $monthName)
					) {
						// Then, we'll use the current month (its numerical
						// value) in order to check the date. We create an 
						// additional 'month' key that stores the month
						// extracted:
						$elements['month'] = $i;

						// Break out of the loop
						break;
					}
				}
				
				// If no month matched
				if(! array_key_exists('month', $elements)) {
					// Then, we were not able to match a month name to the provided
					// string. In this case, we are unable to construct the date
					// object. We throw an exception to inform about the error:
					throw new M_Exception(sprintf(
						'Cannot construct date with the string %s; The month name ' . 
						'is not recognized in the current locale.',
						$string
					));
				}
				
			}
			// If no month name is available either:
			else {
				// Then, we default the day to the current month:
				$elements['month'] = self::getCurrentMonth();
			}
		}

		// The month must be a number in the range of 1-12!
		if($elements['month'] < 1 || $elements['month'] > 12) {
			// If that is not the case, then the date cannot be constructed.
			// We throw an exception to inform about the error:
			throw new M_Exception(sprintf(
				'Cannot construct date with the string %s; The month must ' . 
				'be a number, ranging from 1 to 12! Instead, the number %s was ' . 
				'extracted from the string.',
				$string,
				$elements['month']
			));
		}

		// We check if a year has been extracted:
		$has_yy = array_key_exists('yy', $elements);
		$has_YY = array_key_exists('YY', $elements);

		// If a year has been provided, in short format (2 digits):
		if($has_yy || $has_YY) {
			// A year can be interpreted in 2 different manners. For example,
			// the year 08 is probably referring to the year 2008, because the
			// last 2 digits are lower than the last two digits of the current
			// year. However, if 81 is provided, then the year is probably
			// referring to 1981. At the same time, the number is 15 is most
			// likely referring to 2015, even if we are in 2012. So, first
			// get the number provided:
			$temp = (int) M_Helper::ltrimCharlist($has_yy ? $elements['yy'] : $elements['YY'], '0');

			// Get the current century (in thousands):
			$century = (int) (substr(self::getCurrentYear(), 0, 2) . '00');

			// We create an additional 'year' key that stores the full year
			// extracted from the string...
			$elements['year'] =
				// If the number is referring to a year in the current century:
				(abs((int) M_Helper::ltrimCharlist(substr(self::getCurrentYear(), 2, 2)) - $temp) > 10)
					// Then, default to previous century:
					? $century - 100 + $temp
					// If not, default to current century:
					: $century + $temp;
		}
		// If a year has been provided in another format:
		else {
			// If the year has been provided in three digits:
			if(array_key_exists('YYY', $elements)) {
				// Get the year provided:
				$temp = (int) M_Helper::ltrimCharlist($elements['YYY'], '0');

				// Get the centuries, for comparison:
				$centuryCurrent  = (int) M_Helper::ltrimCharlist(substr(self::getCurrentYear(), 1, 1));
				$centuryProvided = (int) substr($elements['YYY'], 0, 1);

				// Compare the year with the current year. With the result,
				// we create an additional 'year' key that stores the full 
				// year extracted from the string...
				$elements['year'] = 
					($centuryProvided > 0 && $centuryProvided > $centuryCurrent) 
						? 1000 + $temp 
						: 2000 + $temp;
			}
			// If not provided in three digits:
			else {
				// Check other formats:
				if(array_key_exists('YYYY',  $elements)) {
					$elements['year'] = (int) $elements['YYYY'];
				}
				elseif(array_key_exists('yyyyy', $elements)) {
					$elements['year'] = (int) M_Helper::ltrimCharlist($elements['yyyyy'], '0');
				}
				elseif(array_key_exists('yyyy', $elements)) {
					$elements['year'] = (int) $elements['yyyy'];
				}
				elseif(array_key_exists('y', $elements)) {
					$elements['year'] = (int) $elements['y'];
				}
				// If we arrive here, then no year has been provided at all
				// (none of the possible formats are provided)
				else {
					$elements['year'] = self::getCurrentYear();
				}
			}
		}

		// We have done all of this in order to obtain 3 minimum date parts.
		// By now, we know that we have a year, a month, and a day of the
		// month.

		// In order to check whether or not the date is an existing date,
		// we use the checkdate() function with the date elements extracted.
		// If the date elements do not pass this test:
		if(! checkdate($elements['month'], $elements['day'], $elements['year'])) {
			// Then we cannot construct a valid date. In this case, we throw an
			// exception to inform about the error
			throw new M_Exception(sprintf(
				'Cannot construct date with the string %s; The date %s/%s/%s ' . 
				'(dd/MM/yyyy) is not an existing date!',
				$string,
				$elements['day'],
				$elements['month'],
				$elements['year']
			));
		}
		
		// Now, finally, we can assume that the subject is a valid date.
		// However, we are not yet done! Information about the time might
		// also be present in the string.

		// We'll create an additional 'hour' key that stores the 
		// hour extracted. We start by checking if the hour is provided 
		// in the 0-23 format, with leading zero:
		if(array_key_exists('HH', $elements)) {
			// Yes, extract!
			$elements['hour'] = (int) M_Helper::ltrimCharlist($elements['HH'], '0');
		}
		// Provided in 0-23 format, without leading zero?
		elseif(array_key_exists('H', $elements)) {
			// Yes, extract!
			$elements['hour'] = (int) $elements['H'];
		}
		// Provided in 1-24 format, with leading zero?
		elseif(array_key_exists('kk', $elements)) {
			// Yes, extract!
			$temp = (int) M_Helper::ltrimCharlist((int) $elements['kk'], '0');

			// When 24, the hour is 0
			$elements['hour'] = $temp == 24 ? 0 : $temp;
		}
		// Provided in 1-24 format, without leading zero?
		elseif(array_key_exists('k', $elements)) {
			// Yes, extract!
			$temp = (int) $elements['k'];

			// When 24, the hour is 0
			$elements['hour'] = $temp == 24 ? 0 : $temp;
		}
		// If not, we'll check for alternative formats that use the AM/PM
		// string to indicate antemeridiem or postmeridiem:
		else {
			// Provided in 1-12, with leading zero?
			$has_hh = array_key_exists('hh', $elements);
			// Provided in 1-12, without leading zero?
			$has_h  = array_key_exists('h', $elements);
			// Provided in 0-11, with leading zero?
			$has_KK = array_key_exists('KK', $elements);
			// Provided in 0-11, without leading zero?
			$has_K  = array_key_exists('K', $elements);

			// In these formats, the string AM/PM may be provided in order
			// know what hour of the day this is. We determine whether the
			// time is AM, or PM (AM by default):
			$isPM  = array_key_exists('a', $elements) 
				? (strtoupper($elements['a']) == ($data ? $data->getMeridiem('PM') : 'PM'))
				: FALSE;

			// If so:
			if($has_hh || $has_h || $has_KK || $has_K) {
				// If 1-12 provided
				if($has_hh || $has_h) {
					// Extract the 1-12 format:
					$temp = $has_hh
						? (int) M_Helper::ltrimCharlist((int) $elements['hh'], '0')
						: (int) $elements['h'];
				}
				// Extract the 0-11 format:
				else {
					// Extract the 0-11 format:
					$temp = $has_hh
						? (int) M_Helper::ltrimCharlist((int) $elements['hh'], '0')
						: (int) $elements['h'];
				}

				// Convert the hour to 0-24:
				if($isPM) {
					// By adding 12, if PM
					$temp += 12;
				}

				// Then, convert to 0-23
				if($temp == 24) {
					// By setting 24 to 0
					$temp = 0;
				}

				// Set the hour:
				$elements['hour'] = $temp;
			}
			// If we arrive here, then no hour has been provided at all. 
			else {
				// In this case, we default to the current hour:
				$elements['hour'] = self::getCurrentHour();
			}
		}

		// As a final result, the hour must be a number in the range 
		// of 0-23
		if($elements['hour'] < 0 || $elements['hour'] > 23) {
			// If that is not the case, then the date cannot be constructed.
			// We throw an exception to inform about the error:
			throw new M_Exception(sprintf(
				'Cannot construct date with the string %s; The hour must ' . 
				'be a number, ranging from 0 to 23! Instead, %s has ' . 
				'extracted the hour %s',
				$string,
				$elements['hour']
			));
		}

		// We'll create an additional 'minute' key that stores the 
		// minutes extracted. We start by checking if the minutes are 
		// provided with leading zero:
		if(array_key_exists('mm', $elements)) {
			// Yes, extract!
			$elements['minute'] = (int) M_Helper::ltrimCharlist($elements['mm'], '0');
		}
		// Provided without leading zero?
		elseif(array_key_exists('m', $elements)) {
			// Yes, extract!
			$elements['minute'] = (int) $elements['m'];
		}
		// If not provided:
		else {
			// In this case, we default to the current minutes:
			$elements['minute'] = self::getCurrentMinutes();
		}

		// As a final result, the minutes must be a number in the range 
		// of 0-59
		if($elements['minute'] < 0 || $elements['minute'] > 59) {
			// If that is not the case, then the date cannot be constructed.
			// We throw an exception to inform about the error:
			throw new M_Exception(sprintf(
				'Cannot construct date with the string %s; The minute must ' . 
				'be a number, ranging from 0 to 59! Instead, %s has ' . 
				'extracted the minute %s',
				$string,
				$elements['minute']
			));
		}

		// We'll create an additional 'minute' key that stores the 
		// minutes extracted. We start by checking if the minutes are 
		// provided with leading zero:
		if(array_key_exists('ss', $elements)) {
			// Yes, extract!
			$elements['second'] = (int) M_Helper::ltrimCharlist($elements['ss'], '0');
		}
		// Provided without leading zero?
		elseif(array_key_exists('s', $elements)) {
			// Yes, extract!
			$elements['second'] = (int) $elements['s'];
		}
		// If not provided:
		else {
			// In this case, we default to the current seconds:
			$elements['second'] = self::getCurrentSeconds();
		}

		// As a final result, the second must be a number in the range 
		// of 0-59
		if($elements['second'] < 0 || $elements['second'] > 59) {
			// If that is not the case, then the date cannot be constructed.
			// We throw an exception to inform about the error:
			throw new M_Exception(sprintf(
				'Cannot construct date with the string %s; The second must ' . 
				'be a number, ranging from 0 to 59! Instead, %s has ' . 
				'extracted the second %s',
				$string,
				$elements['second']
			));
		}

		// Finally, we can construct a new M_Date instance with the 
		// date and time parts extracted. We return this instance as the
		// result of this constructor:
		return new M_Date(array(
			'year'   => $elements['year'],
			'month'  => $elements['month'],
			'day'    => $elements['day'],
			'hour'   => $elements['hour'],
			'minute' => $elements['minute'],
			'second' => $elements['second']
		));
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get date format, with placeholders
	 * 
	 * This static getter allows you to retrieve a date format with placeholder
	 * characters. Typically, the resulting string is used as a hint (placeholder 
	 * attribute) of an input control, in HTML.
	 * 
	 * <code>
	 *    // Get the SHORT date format in placeholders, in Dutch
	 *    M_Date::getDateFormatWithPlaceholders(M_Date::SHORT, 'nl');
	 * </code>
	 * 
	 * This method would output the following string:
	 * 
	 * <code>dd-mm-jj</code>
	 * 
	 * @static
	 * @access public
	 * @param string $format
	 * @param string $locale
	 * @return string
	 */
	public static function getDateFormatWithPlaceholders($format, $locale = NULL) {
		// If a locale is not provided to this function, we'll default to the 
		// locale that has been set in the LC_TIME category. If we retrieve the
		// locale from this category, it will fallback to LANG as the default.
		// So, if not provided:
		if(! $locale) {
			// Get the locale for LC_TIME:
			$locale = M_Locale::getCategory(M_Locale::LC_TIME);
		}
		
		// We will not retrieve the placeholders from LDML files, since this info
		// is not provided by these files. Instead, the core provides with a set
		// of classes that provide with predefined placeholder characters for a 
		// set of languages. We create a queue of languages, in which the last
		// entry is always "en". This way, we always fall back to the English
		// placeholders for unsupported languages:
		$queue = array();
		
		// If a locale has been provided, or if we defaulted successfully to the 
		// active locale:
		if($locale) {
			// Then, we add it to the queue:
			array_push($queue, strtolower($locale));
		}
		
		// If no locale has been added to the queue, or if the locale in the queue
		// is not English:
		if(count($queue) == 0 || $queue[0] != 'en') {
			// Then, we add English to the queue:
			array_push($queue, 'en');
		}
		
		// For each of the locales in the queue:
		foreach($queue as $currentLocale) {
			// With the current locale, we compose the name of the class that 
			// provides us with the placeholders (without the M_ prefix):
			$class = 'DateFormatPlaceholders' . ucfirst($currentLocale);
			
			// We also compose the path to the class file:
			$path  = M_Loader::getAbsolute('core/Date/' . $class . '.class.php');
			
			// If the class file exists:
			if(is_file($path)) {
				// Then, we load the class file:
				M_Loader::loadAbsolute($path);
				
				// We construct a callback...
				$callback = new M_Callback();
				// ...with the loaded core class (now, with M_ prefix):
				$callback->setClassName('M_' . $class);
				// ...with the static function getPlaceholders(), which should
				//    be available in the loaded class
				$callback->setFunctionName('getPlaceholders');
				
				// Get the placeholders, by running the static callback:
				$placeholders = $callback->run();
				
				// Now, we have placeholders. We stop here:
				break;
			}
		}
		
		// The output:
		$out = '';
		
		// Now, we explode the date format into pieces. For each of the pieces:
		foreach(self::_getArrayFromDateFormat($format, $locale) as $current) {
			// If the current string is not literal, and a placeholder is 
			// available for the current piece:
			if(! $current['isLiteral'] && array_key_exists($current['string'], $placeholders)) {
				// Then, add the placeholder to the output:
				$out .= $placeholders[$current['string']];
			}
			// In any other case:
			else {
				// We add the current string as-is, to the output
				$out .= $current['string'];
			}
		}
		
		// Return the final format:
		return $out;
	}
	
	/**
	 * Get current year
	 * 
	 * @static
	 * @access public
	 * @return integer
	 */
	public static function getCurrentYear() {
		return date('Y');
	}
	
	/**
	 * Get current month
	 * 
	 * @static
	 * @access public
	 * @return integer
	 */
	public static function getCurrentMonth() {
		return date('n');
	}
	
	/**
	 * Get current week
	 * 
	 * Provides the current week number (1-52) of the current year
	 * 
	 * @static
	 * @access public
	 * @return integer
	 */
	public static function getCurrentWeek() {
		return date('W');
	}
	
	/**
	 * Get current day (of month)
	 * 
	 * @static
	 * @access public
	 * @return integer
	 */
	public static function getCurrentDay() {
		return date('j');
	}
	
	/**
	 * Get current hour
	 * 
	 * 24-hour format of an hour without leading zeros.
	 * 
	 * The hour is always provided in 24-hour format, because this way the hour 
	 * is always unique and we avoid this value from being ambiguous. On the practical
	 * side of it: you should never worry about displaying 12-hour or 24-hour 
	 * format. Instead, you should always use formatting functions to display 
	 * date information:
	 * 
	 * - {@link M_Date::toString()}
	 * - {@link M_Date::toRelativeString()}
	 * 
	 * @static
	 * @access public
	 * @return integer
	 */
	public static function getCurrentHour() {
		return date('G');
	}
	
	/**
	 * Get current minutes
	 * 
	 * @static
	 * @access public
	 * @return integer
	 */
	public static function getCurrentMinutes() {
		return ((int) date('i'));
	}
	
	/**
	 * Get current seconds
	 * 
	 * @static
	 * @access public
	 * @return integer
	 */
	public static function getCurrentSeconds() {
		return ((int) date('s'));
	}
	
	/**
	 * Get current microseconds
	 * 
	 * @static
	 * @access public
	 * @return integer
	 */
	public static function getCurrentMicroseconds() {
		if(phpversion() < '5.2.1') {
			return ((int) date('u'));
		} else {
			return 0;
		}
	}
	
	/**
	 * Construct With Week Number
	 * 
	 * May be used to construct a certain weekday (0-6) from a requested
	 * week (1-52) during a year.
	 * 
	 * The default functionality will provide the date of Monday during the
	 * requested week. For example, in 2011:
	 * 
	 * <code>
	 *	$date = M_Date::constructWithWeekNumber(43);
	 *	echo $date->toString();
	 * </code>
	 * 
	 * Will output the following:
	 * 
	 * <code>
	 *	Sunday, October 23rd 2011
	 * </code>
	 * 
	 * As second parameter you may provide the required weekday. Typically,
	 * when used in a calendar for example, we need Monday as first day of
	 * the week rather than Sunday. For example:
	 * 
	 * <code>
	 *	$date = M_Date::constructWithWeekNumber(43, 1);
	 *	echo $date->toString();
	 * </code>
	 * 
	 * Will output the following:
	 * 
	 * <code>
	 *	Monday, October 24th 2011
	 * </code>
	 * 
	 * As last argument you may provide the year we have to calcualte for. If
	 * not provided, the current year will be used.
	 * 
	 * @access public
	 * @param int $weekNumber
	 *		The week number, 1-53
	 * @param int $weekday
	 *		The day of the week, 0-6. By default, Sunday (0) will be used.
	 * @param int $year
	 *		The year we have to calculate for. If not given, the current
	 *		year will be used.
	 * @return M_Date
	 */
	public static function constructWithWeekNumber($weekNumber, $weekday = 0, $year = null) {
		// If no year was specified, fetch the current year
		if(! $year) $year = self::getCurrentYear();
		
		// Count from '0104' because January 4th is always in week 1
		// (according to ISO 8601).
		$time = strtotime($year . "0104 + " . ($weekNumber - 1) . " weeks");
		
		// Get the time of the first day of the week, which is Sunday
		$date = strtotime("- " . date("w", $time) . " days", $time);
		
		// If another day than Sunday was requested
		if($weekday > 0 ) {
			$date = strtotime("+ " . $weekday . " days", $date);
		}
		
		// Return the date
		return new M_Date($date);
	}
	
	/**
	 * Get Number Of Weeks In Year
	 * 
	 * Will provide the number of weeks (52 or 53) in a given year.
	 * 
	 * @access public
	 * @param int $year
	 * @return int
	 */
	public static function getNumberOfWeeksInYear($year) {
		// Construct the 28th of December, which is always in the last week
		// of the year
		$date = new M_Date(array(
			'year'	=>	$year,
			'month'	=>	12,
			'day'	=>	28
		));
		
		// Fetch the week number
		$weeks = date("W", $date->getTimestamp()); 
		
		// Because for some reason we sometimes get 51 as result of the previous
		// line, we make sure that the number of weeks is minimally 52
		if($weeks < 52) $weeks = 52;
		
		// Return the number of weeks
		return $weeks;
	}
	
	/**
	 * Getter for date parts
	 * 
	 * This methods can be used to get the values of a specific
	 * date part. The available date parts are:
	 * 
	 * <code>
	 *    M_Date::YEAR
	 *    M_Date::MONTH
	 *    M_Date::DAY
	 *    M_Date::WEEKDAY
	 *    M_Date::HOUR
	 *    M_Date::MINUTE
	 *    M_Date::SECOND
	 *    M_Date::TIMESTAMP
	 * </code>
	 * 
	 * Example 1, get the week day of xmas 2008
	 * <code>
	 *    $date = new M_Date('25 Dec 2008');
	 *    echo $date->get(M_Date::WEEKDAY);
	 * </code>
	 * 
	 * TIP:
	 * Note that the M_Date class offers an implementation of PHP's
	 * magic getter and setter methods. So, you could rewrite the
	 * above example as following:
	 * 
	 * <code>
	 *    $date = new M_Date('25 Dec 2008');
	 *    echo $date->weekday;
	 * </code>
	 * 
	 * Note that you can call the get() method without passing in
	 * any argument. In that case, the method will return the UNIX
	 * timestamp of the date by default.
	 * 
	 * @access public
	 * @throws M_DateException
	 * @param string $part
	 * 		The (date) part you want to get from the date.
	 * @return mixed
	 */
	public function get($part = NULL) {
		if($part) {
			$property = '_' . $part;
			if(isset($this->$property)) {
				return $this->$property;
			} else {
				throw new M_DateException(sprintf('Could not recognize date part "%s"', $part));
			}
		} else {
			return $this->_timestamp;
		}
	}
	
	/**
	 * Setter for date parts
	 * 
	 * This methods can be used to set the values of a specific
	 * date part. The available date parts are:
	 * 
	 * <code>
	 *    M_Date::YEAR
	 *    M_Date::MONTH
	 *    M_Date::DAY
	 *    M_Date::WEEKDAY
	 *    M_Date::HOUR
	 *    M_Date::MINUTE
	 *    M_Date::SECOND
	 *    M_Date::TIMESTAMP
	 * </code>
	 * 
	 * Example 1, change the month of a constructed date
	 * <code>
	 *    $date = new M_Date('25 Dec 2008');
	 *    echo $date->set(1, M_Date::MONTH); // Now, it's 25 Jan 2008
	 * </code>
	 * 
	 * TIP:
	 * Note that the M_Date class offers an implementation of PHP's
	 * magic getter and setter methods. So, you could rewrite the
	 * above example as following:
	 * 
	 * <code>
	 *    $date = new M_Date('25 Dec 2008');
	 *    $date->month = 1; // Now, it's 25 Jan 2008
	 * </code>
	 * 
	 * Note that you can call the set() method without passing in
	 * the data part argument. In that case, the method will 
	 * re-build the entire M_Date, based on the provided UNIX 
	 * timestamp.
	 * 
	 * @access public
	 * @throws M_DateException
	 * @param integer $value
	 * 		The new value of the date part
	 * @param string $part
	 * 		The (date) part you want to set in the date.
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function set($value, $part = NULL) {
		if($part) {
			if($part == self::TIMESTAMP) {
				$this->_initFromTimestamp((int) $value);
			} else {
				$property = '_' . $part;
				if(isset($this->$property)) {
					$this->$property = (int) $value;
				} else {
					throw new M_DateException(sprintf('Could not recognize date part "%s"', $part));
				}
			}
			$this->_timestamp = mktime(
				$this->_hour, 
				$this->_minute, 
				$this->_second, 
				$this->_month, 
				$this->_day,
				$this->_year
			);
		} else {
			$this->_initFromTimestamp((int) $value);
		}
		return $this;
	}
	
	/**
	 * Set timestamp
	 * 
	 * @access public
	 * @param integer $time
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTimestamp($time) {
		return $this->set($time, self::TIMESTAMP);
	}
	
	/**
	 * Set year
	 * 
	 * @access public
	 * @param integer $year
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setYear($year) {
		return $this->set($year, self::YEAR);
	}
	
	/**
	 * Set month
	 * 
	 * @access public
	 * @param integer $month
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setMonth($month) {
		return $this->set($month, self::MONTH);
	}
	
	/**
	 * Set day
	 * 
	 * @access public
	 * @param integer $day
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDay($day) {
		return $this->set($day, self::DAY);
	}
	
	/**
	 * Set weekday
	 * 
	 * @access public
	 * @param integer $day
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setWeekday($day) {
		return $this->set($day, self::WEEKDAY);
	}
	
	/**
	 * Set hour
	 * 
	 * @access public
	 * @param integer $hour
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setHour($hour) {
		return $this->set($hour, self::HOUR);
	}
	
	/**
	 * Set minutes
	 * 
	 * @access public
	 * @param integer $minutes
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setMinutes($minutes) {
		return $this->set($minutes, self::MINUTE);
	}
	
	/**
	 * Set seconds
	 * 
	 * @access public
	 * @param integer $seconds
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSeconds($seconds) {
		return $this->set($seconds, self::SECOND);
	}
	
	/**
	 * Set To Next Weekday
	 * 
	 * Allows for setting this date to the next specified weekday. For example,
	 * if we have the following date:
	 * 
	 * <code>
	 *	$date = new M_Date('01-01-2012');
	 *	echo $date->toString();
	 * </code>
	 * 
	 * We would get as output:
	 * 
	 * <code>
	 *	Sunday January 1st 2012
	 * </code>
	 * 
	 * Now imagine that we need the first friday of January 2012. In that case,
	 * you may use this function like this:
	 * 
	 * <code>
	 *	$date->setToNextWeekday(5);
	 *	echo $date;
	 * </code>
	 * 
	 * Which would result in the following output:
	 * 
	 * <code>
	 *	Friday January 6th 2012
	 * </code>
	 * 
	 * NOTE: weekdays are specified 0-6, with 0 being Sunday.
	 * 
	 * @access public
	 * @param type $weekday
	 * @return M_Date
	 */
	public function setToNextWeekday($weekday) {
		// Is this date the required weekday?
		if($weekday == $this->getWeekday()) return $this;
		
		// Add one day every time, until we hit the correct weekday
		while($this->getWeekday() != $weekday) {
			$this->addTime(1, self::DAY);
		}
		
		// Return self;
		return $this;
	}
	
	/**
	 * Set Time
	 * 
	 * Allows for setting the time of this date in hours, minutes and seconds.
	 * This is particularly handy to reset an M_Date to 00:00:00 or 23:59:59.
	 * 
	 * @access public
	 * @param int $hour
	 * @param int $minutes
	 * @param int $seconds
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTime($hour, $minutes, $seconds) {
		return $this
			->setHour($hour)
			->setMinutes($minutes)
			->setSeconds($seconds);
	}

	/**
	 * Set Time using a string
	 *
	 * Allows for setting the time of this date in hours, minutes and seconds.
	 * Use a string in format h:m:s
	 *
	 * @param type $time
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTimeString($time) {
		$time = explode(':',$time);

		//check is we have found 3 element (h, m and s)
		if(count($time) != 3) {
			throw new M_Exception(
				sprintf(
					'Cannot set time using string "%s"',
					$time
				)
			);
		}

		$this
			->setHour($time[0])
			->setMinutes($time[1])
			->setSeconds($time[2]);

		return $this;
	}

	/**
	 * Add time to the current date
	 * 
	 * This method will basically travel ahead in time, by 
	 * adding the provided amount of time to the date.
	 * 
	 * Example 1, construct an M_Date object that represents 
	 * the day after xmas 2008:
	 * <code>
	 *    $date = new M_Date('25 Dec 2008');
	 *    $date->addTime(1, M_Date::DAY);
	 * </code>
	 * 
	 * Example 2, add 3 years to the date
	 * <code>
	 *    $date = new M_Date('25 Dec 2008');
	 *    $date->addTime(3, M_Date::YEAR); // Now, it's 25 Dec 2011
	 * </code>
	 * 
	 * @access public
	 * @param string $time
	 * 		The amount of time to be added to the date.
	 * @param string $part
	 * 		The date part in which the amount of time is being
	 * 		expressed.
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function addTime($time, $part) {
		// If the time to be added is ZERO or lower, then this function should 
		// not do anything to the original date object. If that is the case:
		if($time <= 0) {
			// Then, we do an early return:
			return $this;
		}
		
		// Depending on the date part that has been requested, we add with a
		// given number of seconds to the current date's timestamp
		switch($part) {
			case self::HOUR:
			case self::MINUTE:
			case self::WEEK:
			case self::SECOND:
				$this->_initFromTimestamp($this->_timestamp + self::getNumberOfSeconds($time, $part));
				break;

			// When adding an entire day, we need to apply a different logic
			case self::DAY:
				// We add to the current day number
				$newDay   = $this->getDay() + $time;
				$newMonth = $this->getMonth();
				$newYear  = $this->getYear();
				
				// We get a calendar, to check the maximum number of days in the
				// month:
				$calendar = new M_Calendar($newYear, $newMonth);

				// WHile the new day is higher than the last day of the month:
				while($newDay > $calendar->getNumberOfDays()) {
					// Then, we go to the next month, by adding 1 to the month:
					$newMonth += 1;

					// The month cannot be greater than 12:
					if($newMonth > 12) {
						// In that case, we reset to month 1 and we advance a year:
						$newMonth = 1;
						$newYear += 1;
					}

					// We reduce with the number of days in the current calendar:
					$newDay  -= $calendar->getNumberOfDays();
					$calendar = new M_Calendar($newYear, $newMonth);
				}

				// Re-initiate the date object:
				$this->_initFromTimestamp(mktime(
					$this->_hour,
					$this->_minute,
					$this->_second,
					$newMonth,
					$newDay,
					$newYear
				));

				// Done!
				break;

			// When adding an entire month
			case self::MONTH:
				// We add to the current month number
				$newMonth = $this->getMonth() + $time;
				$newYear  = $this->getYear();

				// The month cannot be greater than 12:
				while($newMonth > 12) {
					// In that case, we reset to month 1 and we advance a year:
					$newMonth -= 12;
					$newYear  += 1;
				}

				// Re-initiate the date object:
				$this->_initFromTimestamp(mktime(
					$this->_hour,
					$this->_minute,
					$this->_second,
					$newMonth,
					$this->_day,
					$newYear
				));

				// Done!
				break;

			// When adding an entire year
			case self::YEAR:
				// add the year(s)
				$this->setYear($this->getYear() + $time);

				// Done!
				break;
			
			default:
				// unkown date part so throw exception because we don't know what to do
				throw new M_Exception(sprintf('Invalid value %s for parameter $part in %s::%s', $part, __CLASS__, __FUNCTION__));
		}
		return $this;
	}
	
	/**
	 * Set the date to tomorrow
	 * 
	 * Will set the day of the month to the next one (without affecting time).
	 * This is particularly handy if you want to advance a day in time, without
	 * having to worry about Daylight Saving Time.
	 * 
	 * @access public
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function setToDayAfterWithSameTime() {
		/* $day   = $this->_day + 1;
		$month = $this->_month;
		$year  = $this->_year;
		
		// If the day is now higher than 28, we check if we have not surpassed
		// the number of days in the calendar.
		if($day > 28) {
			// Get the calendar:
			$calendar = new M_Calendar($this->_year, $this->_month);
			
			// Surpassed # of days?
			if($day > $calendar->getNumberOfDays()) {
				// Then, set the day number to 1 (begin of next month)
				$day = 1;
				
				// Also, proceed to next month
				if(++ $month > 12) {
					// If surpassed # of months
					$month = 1;
					$year += 1;
				}
			}
		}
		
		// Set new date parts:
		$this->_initFromTimestamp(mktime(
			$this->_hour, 
			$this->_minute, 
			$this->_second, 
			$month, 
			$day,
			$year
		)); */

		return $this->addTime(1, self::DAY);
	}
	
	/**
	 * Subtract time from the current date
	 * 
	 * This method will basically travel back in time, by 
	 * subtracting the provided amount of time from the date.
	 * 
	 * Example 1, construct an M_Date object that represents 
	 * the day before xmas 2008:
	 * <code>
	 *    $date = new M_Date('25 Dec 2008');
	 *    $date->subtractTime(1, M_Date::DAY);
	 * </code>
	 * 
	 * Example 2, go three years back
	 * <code>
	 *    $date = new M_Date('25 Dec 2008');
	 *    $date->subtractTime(3, M_Date::YEAR); // Now, it's 25 Dec 2005
	 * </code>
	 * 
	 * @access public
	 * @param string $time
	 * 		The amount of time to be subtracted from the date.
	 * @param string $part
	 * 		The date part in which the amount of time is being
	 * 		expressed.
	 * @return M_Date
	 *		Returns itself, for a fluent programming interface
	 */
	public function subtractTime($time, $part) {
		// Depending on the date part that has been requested, we reduce with a
		// given number of seconds in the current date's timestamp
		switch($part) {
			case self::HOUR:
			case self::MINUTE:
			case self::WEEK:
			case self::SECOND:
				$this->_initFromTimestamp($this->_timestamp - self::getNumberOfSeconds($time, $part));
				break;

			// When subtracting an entire day, we need to apply a different logic
			case self::DAY:
				// We subtract from the current day number
				$newDay   = $this->getDay() - $time;
				$newMonth = $this->getMonth();
				$newYear  = $this->getYear();

				// Of course, the day number is minimum 1. So, if we now have a
				// number that is ZERO or lower:
				while($newDay < 1) {
					// Then, we go to the previous month, by reducing the month
					// number by 1:
					$newMonth -= 1;

					// Here, we have the same problem. The month cannot be ZERO:
					if($newMonth < 1) {
						// In that case, we reset to month 12 and we go back a year:
						$newMonth = 12;
						$newYear -= 1;
					}

					// We add the number of days in the previous month to the
					// day number:
					$calendar = new M_Calendar($newYear, $newMonth);
					$newDay  += $calendar->getNumberOfDays();
				}

				// Re-initiate the date object:
				$this->_initFromTimestamp(mktime(
					$this->_hour,
					$this->_minute,
					$this->_second,
					$newMonth,
					$newDay,
					$newYear
				));

				// Done!
				break;

			// When subtracting an entire month
			case self::MONTH:
				// We subtract from the current month number
				$newMonth = $this->getMonth() - $time;
				$newYear  = $this->getYear();

				// The month cannot be ZERO or lower:
				while($newMonth < 1) {
					// In that case, we add 12 to the month and we go back a year:
					$newMonth += 12;
					$newYear  -= 1;
				}

				// Re-initiate the date object:
				$this->_initFromTimestamp(mktime(
					$this->_hour,
					$this->_minute,
					$this->_second,
					$newMonth,
					$this->_day,
					$newYear
				));

				// Done!
				break;

			// When subtracting an entire year
			case self::YEAR:
				// Substract a year
				$this->setYear($this->getYear() - $time);
				
				// Done!
				break;
			
			default:
				// unkown date part so throw exception because we don't know what to do
				throw new M_Exception(sprintf('Invalid value %s for parameter $part in %s::%s', $part, __CLASS__, __FUNCTION__));
		}
		return $this;
	}

	/**
	 * Get elapsed time
	 * 
	 * This method will return the amount of time that has elapsed
	 * since the provided date (object). Note that you can request 
	 * the amount of elapsed time in different date parts: years,
	 * months, weeks, days, etc...
	 * 
	 * Example 1, get the number of hours between christmas and
	 * New Year :)
	 * <code>
	 *    $christmas = new M_Date('25 Dec 2008');
	 *    $newyear   = new M_Date('1 Jan 2009 00:00:00');
	 *    echo $newyear->getElapsedTimeSince($christmas, M_Date::HOUR);
	 * </code>
	 * 
	 * Example 2, get the number of days between christmas and
	 * "St Nicolas"
	 * <code>
	 *    $christmas = new M_Date('25 Dec 2008');
	 *    $stnicolas = new M_Date('6 Dec 2008');
	 *    echo $christmas->getElapsedTimeSince($stnicolas, M_Date::DAY);
	 * </code>
	 * 
	 * Note that the date part argument is optional. If this argument
	 * is omitted, this method will assume you are requesting the 
	 * amount of time expressed in a number of seconds.
	 * 
	 * @access public
	 * @param M_Date $since
	 * 		This M_Date object is used as a reference, to calculate the
	 * 		amount of time that has elapsed since then.
	 * @param string $part
	 * 		The date part in which the amount of time is being
	 * 		requested.
	 * @return int
	 */
	public function getElapsedTimeSince(M_Date $since, $part = self::SECOND) {
		// We calculate the difference in time in seconds.
		$secs = $this->_timestamp - $since->get(M_Date::TIMESTAMP);

		// If the date we compare with is BEFORE this date, then no time has elapsed
		if($secs < 0) {
			// In that case, we return ZERO
			return 0;
		}
		
		// Then, depending on the date part that has been requested,
		// we apply a factor on the number of seconds, for the end-result:
		switch($part) {
			// The factor can be applied to a week, a day, an hour, and a minute:
			case self::WEEK:
				return floor($secs / self::SECONDS_IN_WEEK);

			case self::DAY:
				return floor($secs / self::SECONDS_IN_DAY);

			case self::HOUR:
				return floor($secs / self::SECONDS_IN_HOUR);

			case self::MINUTE:
				return floor($secs / self::SECONDS_IN_MINUTE);

			case self::SECOND:
				return $secs;
			
			// However, we cannot simply apply this logic to the "year" part, since
			// the number of seconds in each year is variable.
			case self::YEAR:
				// We get the number of years, by subtracting the year numbers:
				$yearDiff = $this->getYear() - $since->getYear();

				// Careful: the calculated difference may be too big. If, for
				// example, this date's month is lower than the month of the date
				// we are comparing with, then the year has not yet fully elapsed.
				// The same logic goes for day numbers, hours, minutes and seconds.
				// We prepare a map of date-part getters, in order to prepare for
				// comparisons:
				$getters = array('getMonth', 'getDay', 'getHour', 'getMinutes', 'getSeconds');

				// For each of the getters:
				foreach($getters as $getter) {
					// If this object's date part (e.g. month) is lower than
					// date part of the object we are comparing with (for example,
					// if this date's month is lower than the month of the object
					// we're comparing with):
					if($since->$getter() > $this->$getter()) {
						// Then, the year has not yet fully elapsed. So, we
						// reduce the difference by one, and we return the result:
						return (-- $yearDiff);
					}

					// If this object's date part equals the date part of the object
					// we are comparing with, then it is still possible that the
					// year has not fully elapsed yet... However, if they do not
					// match:
					if($since->$getter() != $this->$getter()) {
						// Then, it means that the this object's date part is
						// bigger than the other one, and that the year has fully
						// elapsed already. So, we return the calculated difference:
						return $yearDiff;
					}
				}

				// If we are still here, then we return the year difference
				return $yearDiff;

			// Again, if we are retrieving the number of months, we cannot apply
			// the (seconds * factor) logic, since the number of seconds in each
			// month is variable...
			case self::MONTH:
				// If both the date objects' years are the same, then we can simply
				// calculate the number of elapsed months by subtracting:
				if($this->getYear() == $since->getYear()) {
					// Return the result:
					$diff = ($this->getMonth() - $since->getMonth());
				}
				// If both the date objects' months are the same, then we can simply
				// calculate the number of elapsed months by multipling the year
				// difference with a factor of 12
				elseif($this->getMonth() == $since->getMonth()) {
					// Return the result:
					$diff = (($this->getYear() - $since->getYear()) * 12);
				}
				// If we are still here, then the calculation is somewhat more
				// complicated.
				else {
					$diff = 
						// Add 12 months for each full year between the two dates:
						(($this->getYear() - $since->getYear() - 1) * 12)
						// Add the remaining months of the compared date
						+ (12 - $since->getMonth())
						// And add the elapsed months in this object:
						+ $this->getMonth();
				}

				// Careful: the calculated difference may still be 1 too big. If,
				// for example, this date's day is lower than the day of the date
				// we are comparing with, then the month has not yet fully elapsed.
				// The same logic goes for hours, minutes and seconds.
				// We prepare a map of date-part getters, in order to prepare for
				// comparisons:
				$getters = array('getDay', 'getHour', 'getMinutes', 'getSeconds');

				// For each of the getters:
				foreach($getters as $getter) {
					// If this object's date part (e.g. hour) is lower than
					// date part of the object we are comparing with (for example,
					// if this date's hour is lower than the hour of the object
					// we're comparing with):
					if($since->$getter() > $this->$getter()) {
						// Then, the month has not yet fully elapsed. So, we
						// reduce the difference by one, and we return the result:
						return (-- $diff);
					}

					// If this object's date part equals the date part of the object
					// we are comparing with, then it is still possible that the
					// month has not fully elapsed yet... However, if they do not
					// match:
					if($since->$getter() != $this->$getter()) {
						// Then, it means that the this object's date part is
						// bigger than the other one, and that the month has fully
						// elapsed already. So, we return the calculated difference:
						return $diff;
					}
				}

				// If we are still here, then we return the year difference
				return $diff;

			default:
				throw new M_DateException(sprintf(
					'Unable to calculate elapsed time; unrecognized date part %s',
					$part
				));
		}
	}
	
	/**
	 * Get the elapsed time in a string
	 * 
	 * This method will return the amount of time that has elapsed
	 * since the provided date (object), expressed in every date-part possible
	 * 
	 * If the total amount of seconds exceeds the seconds/minute, the string
	 * will contain the number of seconds and the number of minutes. If it
	 * exceeds the number of minutes in one hour: it will add the number of hours
	 * ... until the number of years.
	 * 
	 * Example 1, get the elapsed time string between 1 jan and
	 * New Year :)
	 * <code>
	 *    $christmas = new M_Date('25 Dec 2008');
	 *    $christmasNight   = new M_Date('26 Dec 2008 01:04:36');
	 *    echo $christmasNight->getElapsedTimeSinceString($christmas);
	 * </code>
	 * 
	 * Will output 1 day 1 hour 4 minutes and 36 seconds
	 * 
	 * @author Ben Brughmans, Tom Bauwens
	 * @see getElapsedTimeSince
	 * @param M_Date $since
	 *		The date to use for calculation of elapsed time
	 * @param boolean $includeZeroValues
	 *		Set to TRUE if you want to include zero values, FALSE if not. NOTE:
	 *		this setting is no longer in use, but remains a parameter to
	 *		maintain backwards compatibility
	 * @param string $format
	 *		The format; accepted values are {@link M_Date::FULL}, {@link M_Date::MEDIUM}
	 *		and {@link M_Date::SHORT}
	 * @return string
	 */
	public function getElapsedTimeSinceString(M_Date $since, array $parts = null, $includeZeroValues = TRUE, $format = self::SHORT) {
		// Output array
		$out = array();
		
		// Set (untranslated) strings for output:
		switch($format) {
			// Fully written:
			case self::FULL:
				$year    = '1 year';
				$years   = '@count years';
				$month   = '1 month';
				$months  = '@count months';
				$week    = '1 week';
				$weeks   = '@count weeks';
				$day     = '1 day';
				$days    = '@count days';
				$hour    = '1 hour';
				$hours   = '@count hours';
				$minute  = '1 minute';
				$minutes = '@count minutes';
				$second  = '1 second';
				$seconds = '@count seconds';
				break;
			
			// Short/Medium
			case self::SHORT:
			case self::MEDIUM:
			default:
				$year    = '1 yr';
				$years   = '@count yrs';
				$month   = '1 mth';
				$months  = '@count mths';
				$week    = '1 wk';
				$weeks   = '@count wks';
				$day     = '1 day';
				$days    = '@count days';
				$hour    = '1 hr';
				$hours   = '@count hrs';
				$minute  = '1 min';
				$minutes = '@count mins';
				$second  = '1 sec';
				$seconds = '@count secs';
				break;
		}
		
		// For each of the elements in the elapsed time
		foreach($this->getElapsedTimeSinceArray($since, $parts) AS $key => $value) {
			// If the current value is more than 0
			if($value > 0) {
				// Then, add the string for this value:
				switch($key) {
					case self::YEAR:
						array_push($out, p($year, $years, $value));
						break;
						
					case self::MONTH:
						array_push($out, p($month, $months, $value));
						break;
						
					case self::WEEK:
						array_push($out, p($week, $weeks, $value));
						break;
						
					case self::DAY:
						array_push($out, p($day, $days, $value));
						break;
						
					case self::HOUR:
						array_push($out, p($hour, $hours, $value));
						break;
						
					case self::MINUTE:
						array_push($out, p($minute, $minutes, $value));
						break;
						
					case self::SECOND:
						array_push($out, p($second, $seconds, $value));
						break;
				}
			}
		}
		
		// Return the string:
		return implode(' ', $out);
	}

	/**
	 * Get an array containing all the date parts since a given date
	 *
	 * @see M_Date::getElapsedTimeSinceString()
	 * @param M_Date $since
	 * @param array $parts
	 *		Set the parts which you want to include in the output
	 *		array. E.g. if you only want to calculate days, hours and minutes pass an array like:
	 *		$parts = array(
	 *			self::DAY,
	 *			self::HOUR,
	 *			self::MINUTE
	 *		);
	 *
	 * @return array
	 */
	public function getElapsedTimeSinceArray(M_Date $since, array $parts = null) {
		$elapsed = array();

		//if no specific parts are provided: use all parts
		if (is_null($parts)) {
			$parts = array(
				self::YEAR,
				self::MONTH,
				self::WEEK,
				self::DAY,
				self::HOUR,
				self::MINUTE,
				self::SECOND
			);
		}

		/* create an element for each wanted part in the output array */
		foreach($parts AS $part) {
			$elapsed[$part] = 0;
		}
		
		$totalSeconds = $this->getElapsedTimeSince($since);
		if ($totalSeconds >= self::SECONDS_IN_YEAR && in_array(self::YEAR, $parts)) {
			$years = floor($totalSeconds / self::SECONDS_IN_YEAR);
			$elapsed[self::YEAR] = $years;
			//subtract the years from the total nbr of seconds
			$totalSeconds -= ($years*self::SECONDS_IN_YEAR);
		}
		if ($totalSeconds >= self::SECONDS_IN_MONTH && in_array(self::MONTH, $parts)) {
			$months = floor($totalSeconds / self::SECONDS_IN_MONTH);
			$elapsed[self::MONTH] = $months;
			//subtract the months from the total nbr of seconds
			$totalSeconds -= ($months*self::SECONDS_IN_MONTH);
		}
		if ($totalSeconds >= self::SECONDS_IN_WEEK && in_array(self::WEEK, $parts)) {
			$weeks = floor($totalSeconds / self::SECONDS_IN_WEEK);
			$elapsed[self::WEEK] = $weeks;
			//subtract the weeks from the total nbr of seconds
			$totalSeconds -= ($weeks*self::SECONDS_IN_WEEK);
		}
		if ($totalSeconds >= self::SECONDS_IN_DAY && in_array(self::DAY, $parts)) {
			$days = floor($totalSeconds / self::SECONDS_IN_DAY);
			$elapsed[self::DAY] = $days;
			//subtract the days from the total nbr of seconds
			$totalSeconds -= ($days*self::SECONDS_IN_DAY);
		}
		if ($totalSeconds >= self::SECONDS_IN_HOUR && in_array(self::HOUR, $parts)) {
			$hours = floor($totalSeconds / self::SECONDS_IN_HOUR);
			$elapsed[self::HOUR] = $hours;
			//subtract the hours from the total nbr of seconds
			$totalSeconds -= ($hours*self::SECONDS_IN_HOUR);
		}
		if ($totalSeconds >= self::SECONDS_IN_MINUTE && in_array(self::MINUTE, $parts)) {
			$minutes = floor($totalSeconds / self::SECONDS_IN_MINUTE);
			$elapsed[self::MINUTE] = $minutes;
			//subtract the minutes from the total nbr of seconds
			$totalSeconds -= ($minutes*self::SECONDS_IN_MINUTE);
		}if ($totalSeconds > 0 && in_array(self::SECOND, $parts)) {
			//and at last the total amount of seconds
			$elapsed[self::SECOND] = round($totalSeconds);
		}

		return $elapsed;
	}
	
	/**
	 * Get time overview
	 * 
	 * Will calculate the number of years, months, weeks, days, hours
	 * and minutes there are in a given amount of time. The given amount
	 * of time is expressed by a number and a date part. For example, 
	 * you can choose to input 60 seconds, or 4 years. If the second
	 * argument to this method is omitted, this method will assume you
	 * are expressing time with a number of seconds.
	 * 
	 * This method will return an associative array, with values for
	 * the following keys:
	 * 
	 * <code>
	 *   Array (
	 *      'years'   => [number-of-years],
	 *      'months'  => [number-of-months],
	 *      'weeks'   => [number-of-weeks],
	 *      'days'    => [number-of-days],
	 *      'hours'   => [number-of-hours],
	 *      'minutes' => [number-of-minutes],
	 *      'seconds' => [number-of-seconds],
	 *   )
	 * </code>
	 * 
	 * Example 1
	 * <code>
	 *    $xmas2007  = new M_Date('25 Dec 2007');
	 *    $stnicolas = new M_Date('6 Dec 2008');
	 *    // Get elapsed time since xmas 2007, until St Nicolas 2008:
	 *    // (expressed in a number of days)
	 *    $elapsed = $stnicolas->getElapsedTimeSince($xmas2007, M_Date::DAY);
	 *    // give an overview of the elapsed time:
	 *    $overview = M_Date::getTimeArray($elapsed, M_Date::DAY);
	 *    printf(
	 *       "%d months, %d weeks and %d days have gone by", 
	 *       $overview['months'], 
	 *       $overview['weeks'], 
	 *       $overview['days']
	 *    );
	 * </code>
	 * 
	 * Example 1 will output the following result:
	 * <code>
	 *    11 months, 1 weeks and 5 days have gone by
	 * </code>
	 * 
	 * @access public
	 * @param integer $value
	 * 		The amount of time to parse an overview out of.
	 * @param string $part
	 * 		The date part in which the amount of time is being
	 * 		expressed.
	 * @return array
	 * 		The time overview
	 */
	public static function getTimeArray($value, $part = self::SECOND) {
		// $part defines the unit in which the amount of time is
		// being expressed. We convert this to seconds to do the
		// math work
		$seconds = self::getNumberOfSeconds($value, $part);
		
		// calculate the elements in the time overview:
		$years   = floor($seconds / self::SECONDS_IN_YEAR);
		$seconds %= self::SECONDS_IN_YEAR;
		$months  = floor($seconds / self::SECONDS_IN_MONTH);
		$seconds %= self::SECONDS_IN_MONTH;
		$weeks   = floor($seconds / self::SECONDS_IN_WEEK);
		$seconds %= self::SECONDS_IN_WEEK;
		$days    = floor($seconds / self::SECONDS_IN_DAY);
		$seconds %= self::SECONDS_IN_DAY;
		$hours   = floor($seconds / self::SECONDS_IN_HOUR);
		$seconds %= self::SECONDS_IN_HOUR;
		$minutes = floor($seconds / self::SECONDS_IN_MINUTE);
		$seconds %= self::SECONDS_IN_MINUTE;
		return array(
			'years'   => $years,
			self::YEAR   => $years,
			'months'  => $months,
			self::MONTH	=>	$months,
			'weeks'   => $weeks,
			self::WEEK	=>	$months,
			'days'    => $days,
			self::DAY	=>	$days,
			'hours'   => $hours,
			self::HOUR   => $hours,
			'minutes' => $minutes,
			self::MINUTE => $minutes,
			'seconds' => $seconds,
			self::SECOND => $seconds
		);
	}
	
	/**
	 * Get number of seconds
	 * 
	 * This method will calculate the number of seconds there 
	 * are in a given amount of time. The given amount of time 
	 * is expressed by a number and a date part. For example, 
	 * you can choose to input 2 hours, or 4 years.
	 * 
	 * Example 1, get the number of seconds in 2 hours
	 * <code>
	 *    echo M_Date::getNumberOfSeconds(2, M_Date::HOUR);
	 * </code>
	 * 
	 * Example 2, get the number of seconds in 4 years
	 * <code>
	 *    echo M_Date::getNumberOfSeconds(4, M_Date::YEAR);
	 * </code>
	 * 
	 * @access public
	 * @param integer $time
	 * 		The amount of time.
	 * @param string $part
	 * 		The date part in which the amount of time is being
	 * 		expressed.
	 * @return array
	 * 		The number of seconds
	 */
	public function getNumberOfSeconds($time, $part) {
		switch($part) {
			case self::YEAR:
				return $time * self::SECONDS_IN_YEAR;
			
			case self::MONTH:
				return $time * self::SECONDS_IN_MONTH;
			
			case self::WEEK:
				return $time * self::SECONDS_IN_WEEK;
			
			case self::DAY:
				return $time * self::SECONDS_IN_DAY;
			
			case self::HOUR:
				return $time * self::SECONDS_IN_HOUR;
			
			case self::MINUTE:
				return $time * self::SECONDS_IN_MINUTE;
			
			case self::SECOND:
				return $time;
			
			default:
				throw new M_DateException('Unrecognized date part');
		}
	}
	
	/**
	 * Equals a date?
	 * 
	 * Example 1, check if 2 dates are equal
	 * <code>
	 *    $date1 = new M_Date('25 Dec 2008');
	 *    $date2 = new M_Date('26 Dec 2008');
	 *    if($date1->equals($date2)) {
	 *       // ...
	 *    }
	 * </code>
	 * 
	 * IMPORTANT NOTE
	 * $compareValue can either be an M_Date object, or an integer. If an
	 * integer is given, it is the value of a specific date part to be
	 * compared directly.
	 * 
	 * Example 2, check if the date's time is "10 past the hour"
	 * <code>
	 *    $date = new M_Date;
	 *    if($date->equals(10, M_Date::MINUTE)) {
	 *       echo "It's 10 past the hour";
	 *    }
	 * </code>
	 * 
	 * Example 3, alternative for Example 2
	 * <code>
	 *    $ten  = new M_Date(array('minute' => 10));
	 *    $date = new M_Date;
	 *    if($date->equals($ten, M_Date::MINUTE)) {
	 *       echo "It's 10 past the hour";
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @param mixed $compareValue
	 * 		The date to be compared with
	 * @param string $part
	 * 		The date part to compare
	 * @return boolean
	 */
	public function equals($compareValue, $part = NULL) {
		$v = (is_object($compareValue) && $compareValue instanceof self)
				? $compareValue->get($part)
				: (int) $compareValue;
		
		return ($v == $this->get($part));
	}

	/**
	 * Does this day equal another date?
	 *
	 * This function will check if this M_Date object equals another
	 * M_Date object, based on day, month and year.
	 *
	 * Example: check if the current date equals a given date, i.e. 26/10/2010:
	 * <code>
	 *	$date1 = new M_Date('26 Oct 2010 14:57');
	 *	$date2 = new M_Date('26 Oct 2010 21:30');
	 *	M_Debug::dump($date1->equalsDDMMYYYY($date2));
	 * </code>
	 *
	 * The above code would output (boolean) TRUE.
	 *
	 * NOTE: this function does not take into account wheter the times on both
	 * dates match as well. Only day, month and year are compared. Also note
	 * that all 3 of these must match for the function to return TRUE.
	 *
	 * @author Tim Segers
	 * @access public
	 * @param M_Date $date
	 * @return boolean $flag
	 *		Returns TRUE if the 2 days match, FALSE if not.
	 */
	public function equalsDDMMYYYY(M_Date $date) {
		$req = $this->equals($date, M_Date::DAY) && $this->equals($date, M_Date::MONTH) && $this->equals($date, M_Date::YEAR);
		return $req ? TRUE : FALSE;
	}
	
	/**
	 * Is day?
	 * 
	 * Will check if the date is a given day, without taking into account the time.
	 * 
	 * Example 1, check if 2 dates are equal
	 * <code>
	 *    $date1 = new M_Date('25 Dec 2009 12:05');
	 *    $date2 = new M_Date('25 Dec 2009 13:59');
	 *    var_dump($date1->isDate($date2));
	 * </code>
	 * 
	 * The example above would output (boolean) TRUE, while the following example
	 * would produce the output (boolean) FALSE:
	 * 
	 * <code>
	 *    $date1 = new M_Date('25 Dec 2009 12:05');
	 *    $date2 = new M_Date('26 Dec 2009 12:05');
	 *    var_dump($date1->isDate($date2));
	 * </code>
	 * 
	 * This method compares day, month and year, but does not take into account
	 * (at all) the time of the day.
	 * 
	 * @see M_Date::isTime()
	 * @access public
	 * @param M_Date $compareDate
	 * 		The date to be compared with
	 * @return boolean $flag
	 * 		Returns TRUE if the dates match, FALSE if not
	 */
	public function isDate(M_Date $compareDate) {
		return (
			$this->getDay() == $compareDate->getDay() &&
			$this->getMonth() == $compareDate->getMonth() && 
			$this->getYear() && $compareDate->getYear()
		);
	}
	
	/**
	 * Is time?
	 * 
	 * Will compare the time (hour:minutes:seconds) with the time of another date, 
	 * without taking into account the date.
	 * 
	 * Example 1, check if 2 times are equal
	 * <code>
	 *    $date1 = new M_Date('25 Dec 2009 12:05');
	 *    $date2 = new M_Date('25 Dec 2009 13:59');
	 *    var_dump($date1->isTime($date2));
	 * </code>
	 * 
	 * The example above would output (boolean) FALSE, while the following example
	 * would produce the output (boolean) TRUE:
	 * 
	 * <code>
	 *    $date1 = new M_Date('25 Dec 2009 12:05');
	 *    $date2 = new M_Date('26 Dec 2009 12:05');
	 *    var_dump($date1->isTime($date2));
	 * </code>
	 * 
	 * This method compares hour, minutes and seconds, but does not take into 
	 * account (at all) the date.
	 * 
	 * Note that you may choose to only compare hour and minutes, without taking
	 * into account the seconds. As a matter of fact, this would be the default
	 * behavior of this method. If you also want to add the seconds to the 
	 * comparison, you should provide (boolean) TRUE as a second argument.
	 * 
	 * @see M_Date::isDate()
	 * @access public
	 * @param M_Date $compareDate
	 * 		The date to be compared with
	 * @param boolean $compareSeconds
	 * 		Set to TRUE if you also want to compare seconds, FALSE if not
	 * @return boolean $flag
	 * 		Returns TRUE if the times match, FALSE if not
	 */
	public function isTime(M_Date $compareDate, $compareSeconds = FALSE) {
		// Start with comparing hour + minutes:
		$rs = (
			$this->getHour() == $compareDate->getHour() &&
			$this->getMinutes() == $compareDate->getMinutes()
		);
		
		// If seconds should not be taken into account:
		if(! $compareSeconds) {
			// Then, return the current result
			return $rs;
		}
		// If seconds are to be taken into account
		else {
			// Add the comparison of seconds:
			return ($rs && $this->getSeconds() == $compareDate->getSeconds());
		}
	}
	
	/**
	 * Is after given date?
	 * 
	 * IMPORTANT NOTE
	 * The arguments to this method can be provided in exactly the same
	 * ways as in a {@link M_Date::equals()} call.
	 * 
	 * @access public
	 * @param mixed $compareValue
	 * 		The date to be compared with
	 * @param string $part
	 * 		The date part to compare
	 * @return boolean
	 */
	public function isAfter($compareValue, $part = NULL) {
		$v = (is_object($compareValue) && $compareValue instanceof self)
				? $compareValue->get($part)
				: (int) $compareValue;
		
		return ($v < $this->get($part));
	}
	
	/**
	 * Is before given date?
	 * 
	 * IMPORTANT NOTE
	 * The arguments to this method can be provided in exactly the same
	 * ways as in a {@link M_Date::equals()} call.
	 * 
	 * @access public
	 * @param mixed $compareValue
	 * 		The date to be compared with
	 * @param string $part
	 * 		The date part to compare
	 * @return boolean
	 */
	public function isBefore($compareValue, $part = NULL) {
		$v = (is_object($compareValue) && $compareValue instanceof self)
				? $compareValue->get($part)
				: (int) $compareValue;
		
		return ($v > $this->get($part));
	}
	
	/**
	 * Is date within range?
	 * 
	 * This method will check if the date falls in between the given
	 * range of time, defined by a start date and an end date.
	 * 
	 * <code>
	 *    $date = new M_Date('25 Dec 2008');
	 *    if($date->isInRange(new M_Date('1 Dec 2008'), new M_Date('31 Dec 2008'))) {
	 *       echo 'The date falls in december 2008!';
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @param M_Date $start
	 * 		The start date of the time range.
	 * @param M_Date $end
	 * 		The end date of the time range.
	 * @return boolean
	 */
	public function isInRange(M_Date $start, M_Date $end) {
		return (
			$start->get(M_Date::TIMESTAMP) <= $this->_timestamp &&
			$end->get(M_Date::TIMESTAMP) >= $this->_timestamp
		);
	}
	
	/**
	 * Is date within (offset) range?
	 * 
	 * This method will check if the date falls in between the given
	 * range of time, defined by a start date and an amount of time
	 * (=offset).
	 * 
	 * <code>
	 *    $date = new M_Date('25 Dec 2008');
	 *    // check if the date falls in the range "from 1 Dec 2008 till
	 *    // two weeks after"
	 *    if($date->isInRange(new M_Date('1 Dec 2008', 2, M_Date::WEEK))) {
	 *       echo 'Yes :)';
	 *    } else {
	 *       echo 'No :(';
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @param M_Date $start
	 * 		The start date of the time range.
	 * @param integer $end
	 * 		The amount of time, starting from the start date
	 * @param integer $offsetPart
	 * 		The date part in which the amount of time is being
	 * 		expressed.
	 * @return boolean
	 */
	public function isInOffsetRange(M_Date $start, $offset, $offsetPart = NULL) {
		if($this->isAfter($start)) {
			$start->addTime($offset, $offsetPart);
			return $this->isBefore($start);
		}
		return FALSE;
	}
	
	/**
	 * Is today?
	 * 
	 * Will return TRUE if the date equals the current date
	 * (YYYY-MM-DD comparison only, time is not compared)
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isToday() {
		return (
			date('Y') == $this->_year &&
			date('n') == $this->_month &&
			date('j') == $this->_day
		);
	}
	
	/**
	 * Is tomorrow?
	 * 
	 * Will return TRUE if the date equals the day after the
	 * current date. (YYYY-MM-DD comparison only, time is not 
	 * compared)
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isTomorrow() {
		$tomorrow = getdate(time() + self::SECONDS_IN_DAY);
		return (
			(int) $tomorrow['year'] == $this->_year &&
			(int) $tomorrow['mon']  == $this->_month &&
			(int) $tomorrow['mday'] == $this->_day
		);
	}
	
	/**
	 * Is tomorrow?
	 * 
	 * Will return TRUE if the date equals the day before the
	 * current date. (YYYY-MM-DD comparison only, time is not 
	 * compared)
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isYesterday() {
		$yesterday = getdate(time() - self::SECONDS_IN_DAY);
		return (
			(int) $yesterday['year'] == $this->_year &&
			(int) $yesterday['mon']  == $this->_month &&
			(int) $yesterday['mday'] == $this->_day
		);
	}
	
	/**
	 * Is leap year?
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isLeapYear() {
		return ((int) date('L', $this->_timestamp) == 1);
	}

	/**
	 * Is n-th Weekday of the Month?
	 *
	 * This function will check if this M_Date is the n-th weekday of the
	 * month. For example, this function can be used if this M_Date is the
	 * 4th wednesday of the month. For example:
	 * 
	 * <code>
	 *		$date = new M_Date('27 Oct 2010 12:05);
	 *		$date->isNthWeekdayOfMonth(4, 4) // 4th wednesday
	 * </code>
	 *
	 * This example would return TRUE, as that date is indeed the 4th
	 * wednesday of the month. If it were not, this function would
	 * return FALSE
	 *
	 * @author Tim Segers
	 * @access public
	 * @param int $n
	 *		Modifier describing the n-th weekday of the month
	 * @param int $weekday
	 *		Integer describing the day of the week (0-6, 0 being sunday)
	 * @return boolean
	 *		Returns TRUE if check succeeded, FALSE if not
	 */
	public function isNthWeekdayOfMonth($n, $weekday) {
		// First of all, check if this M_Date matches the requested weekday
		if($this->getWeekday() != $weekday) return FALSE;

		// If it does, we have to check if this is the n-th weekday of the month.
		// To accomplish this, we will keep on subtracting weeks from the
		// date until we end up in the previous month, counting how many
		// times we had to subtract an additional week. If that number equals
		// n (e.g. if n = 4 we have to go back 4 weeks in time to end up
		// in the previous month), the check is valid, and we return TRUE. If
		// the number does not equal n (e.g. we had to go back less OR more
		// weeks to get to the previous month), the check is invalid, and we
		// return FALSE instead.
		/* @var $newDate M_Date */
		$newDate = clone $this;

		// Initiate a counter for keeping track of how many weeks we had to
		// go back in time to reach the previous month
		$t = 0;

		// Initiate a counter for the while loop itself. As we have to loop
		// maximum $n times, we set the counter to $n
		$i = $n;

		// If we ran through the loop without reaching the previous month,
		// it means this M_Date is later in the month than n. To check this
		// as well, initiate a boolean for internal use
		$checked = FALSE;

		while($i > 0) {
			// Subtract a week every time the loop is ran
			$newDate->subtractTime(1, M_Date::WEEK);
			$t++;
			
			// If the month of the new date does not match this M_Date
			// object's month, we found the number of weeks we have to go back,
			// so we have to break out of the loop and set $checked to TRUE
			if($newDate->getMonth() != $this->getMonth()) {
				$i = 0;
				$checked = TRUE;
			}
		}

		// Only if we reached the previous month AND the week counter is correct:
		if($n == $t && $checked) {
			return TRUE;
		}
		
		// If we are still here it means either requirement failed, so we
		// return FALSE
		return FALSE;
	}

	/**
	 * Is last weekday of month?
	 *
	 * This function will check if this M_Date object is the last weekday
	 * of the month, by a given weekday (0-6). For example, to check if this
	 * is the last wednesday of the month:
	 *
	 * <code>
	 *		$date = new M_Date('27 Oct 2010 12:05);
	 *		$date->isLastWeekdayOfMonth(4) // wednesday
	 * </code>
	 *
	 * This example would return TRUE, as the specified date is indeed the
	 * last wednesday of the month. If it were not, this example would return
	 * FALSE instead.
	 *
	 * @author Tim Segers
	 * @access public
	 * @param int $weekday
	 *		The weekday we have to run this check for (0-6)
	 * @return boolean
	 *		Returns TRUE if this is the last weekday of the month, FALSE if not
	 */
	public function isLastWeekdayOfMonth($weekday) {
		// First of all, check if this M_Date matches the requested weekday
		if($this->getWeekday() != $weekday) return FALSE;

		// If it does, we will add one week, and check if the resulting date
		// still lays in the same month. If it does it means this is NOT the
		// last (weekday) of the month, so we return FALSE. If it doesn't, we
		// return TRUE instead
		$newDate = clone $this;
		$newDate->addTime(1, M_Date::WEEK);

		$req = $newDate->getMonth() != $this->getMonth();
		return $req ? TRUE : FALSE;
	}
	
	/**
	 * Export date to array
	 * 
	 * Will export the date into an associative array with values
	 * for the following keys:
	 * 
	 * <code>
	 *   Array (
	 *      'year'    => [year],
	 *      'month'   => [month],
	 *      'day'     => [day],
	 *      'weekday' => [weekday],
	 *      'hour'    => [hour],
	 *      'minute'  => [minute],
	 *      'second'  => [second],
	 *   )
	 * </code>
	 * 
	 * @access public
	 * @return array
	 */
	public function toArray() {
		return array(
			'year'    => $this->_year,
			'month'   => $this->_month,
			'day'     => $this->_day,
			'weekday' => $this->_weekday,
			'hour'    => $this->_hour,
			'minute'  => $this->_minute,
			'second'  => $this->_second
		);
	}
	
	/**
	 * Get timestamp
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTimestamp() {
		return $this->_timestamp;
	}
	
	/**
	 * Get year
	 * 
	 * @access public
	 * @return integer
	 */
	public function getYear() {
		return $this->_year;
	}
	
	/**
	 * Get month
	 * 
	 * @access public
	 * @return integer
	 */
	public function getMonth() {
		return $this->_month;
	}
	
	/**
	 * Get day (of month)
	 * 
	 * @access public
	 * @return integer
	 */
	public function getDay() {
		return $this->_day;
	}
	
	/**
	 * Get day of week
	 * 
	 * Will return the index number of the weekday. This can then be used to
	 * determine which day of the week the date is at...
	 * 
	 * NOTE:
	 * 0 (for Sunday) through 6 (for Saturday)
	 * 
	 * @access public
	 * @return integer
	 */
	public function getWeekday() {
		return $this->_weekday;
	}
	
	/**
	 * Get day of year
	 *
	 * Will return the number of the day, in the year. This can then be used to
	 * determine which day of the year the date is at... The result of this method
	 * is a number, ranging from 0 to 365
	 *
	 * @access public
	 * @return integer
	 */
	public function getYearday() {
		return date('z', $this->_timestamp);
	}

	/**
	 * Get Week Number
	 *
	 * ISO-8601 week number of year, weeks starting on Monday (added in PHP 4.1.0)
	 *
	 * @access public
	 * @return int
	 */
	public function getWeekNumber() {
		return date('W', $this->_timestamp);
	}
	
	/**
	 * Is Monday?
	 * 
	 * Will determine whether or not the date is on a monday. Will return TRUE if
	 * so, FALSE if not.
	 * 
	 * @uses M_Date::getWeekday()
	 * @uses M_Date::MONDAY
	 * @access public
	 * @return boolean
	 */
	public function isMonday() {
		return ($this->getWeekday() == self::MONDAY);
	}
	
	/**
	 * Is Tuesday?
	 * 
	 * Will determine whether or not the date is on a tuesday. Will return TRUE if
	 * so, FALSE if not.
	 * 
	 * @uses M_Date::getWeekday()
	 * @uses M_Date::TUESDAY
	 * @access public
	 * @return boolean
	 */
	public function isTuesday() {
		return ($this->getWeekday() == self::TUESDAY);
	}
	
	/**
	 * Is Wednesday?
	 * 
	 * Will determine whether or not the date is on a wednesday. Will return TRUE 
	 * if so, FALSE if not.
	 * 
	 * @uses M_Date::getWeekday()
	 * @uses M_Date::WEDNESDAY
	 * @access public
	 * @return boolean
	 */
	public function isWednesday() {
		return ($this->getWeekday() == self::WEDNESDAY);
	}
	
	/**
	 * Is Thursday?
	 * 
	 * Will determine whether or not the date is on a thursday. Will return TRUE 
	 * if so, FALSE if not.
	 * 
	 * @uses M_Date::getWeekday()
	 * @uses M_Date::THURSDAY
	 * @access public
	 * @return boolean
	 */
	public function isThursday() {
		return ($this->getWeekday() == self::THURSDAY);
	}
	
	/**
	 * Is Friday?
	 * 
	 * Will determine whether or not the date is on a friday. Will return TRUE 
	 * if so, FALSE if not.
	 * 
	 * @uses M_Date::getWeekday()
	 * @uses M_Date::FRIDAY
	 * @access public
	 * @return boolean
	 */
	public function isFriday() {
		return ($this->getWeekday() == self::FRIDAY);
	}
	
	/**
	 * Is Saturday?
	 * 
	 * Will determine whether or not the date is on a saturday. Will return TRUE 
	 * if so, FALSE if not.
	 * 
	 * @uses M_Date::getWeekday()
	 * @uses M_Date::SATURDAY
	 * @access public
	 * @return boolean
	 */
	public function isSaturday() {
		return ($this->getWeekday() == self::SATURDAY);
	}
	
	/**
	 * Is Sunday?
	 * 
	 * Will determine whether or not the date is on a sunday. Will return TRUE 
	 * if so, FALSE if not.
	 * 
	 * @uses M_Date::getWeekday()
	 * @uses M_Date::SUNDAY
	 * @access public
	 * @return boolean
	 */
	public function isSunday() {
		return ($this->getWeekday() == self::SUNDAY);
	}
	
	/**
	 * Get hour
	 * 
	 * @access public
	 * @return integer
	 */
	public function getHour() {
		return $this->_hour;
	}
	
	/**
	 * Get minutes
	 * 
	 * @access public
	 * @return integer
	 */
	public function getMinutes() {
		return $this->_minute;
	}
	
	/**
	 * Get seconds
	 * 
	 * @access public
	 * @return integer
	 */
	public function getSeconds() {
		return $this->_second;
	}
	
	/**
	 * Export date to MySQL Date
	 * 
	 * This method will export the M_Date object to a MySQL Date
	 * string, which has the following format: YYYY-MM-DD
	 * 
	 * @access public
	 * @return string
	 */
	public function getMysqlDate() {
		return $this->_year . '-' . $this->_month . '-' . $this->_day;
	}
	
	/**
	 * Export date to MySQL DateTime
	 * 
	 * This method will export the M_Date object to a MySQL DateTime
	 * string, which has the following format: YYYY-MM-DD HH:mm:ss
	 * 
	 * @access public
	 * @return string
	 */
	public function getMysqlDateTime() {
		return $this->_year . '-' . $this->_month . '-' . $this->_day . ' ' . $this->_hour . ':' . $this->_minute . ':' . $this->_second;
	}
	
	/**
	 * Export date to MySQL Time
	 * 
	 * This method will export the M_Date object to a MySQL Time
	 * string, which has the following format: HH:mm:ss
	 * 
	 * @access public
	 * @return string
	 */
	public function getMysqlTime() {
		return $this->_hour . ':' . $this->_minute . ':' . $this->_second;
	}

	/**
	 * Export date to RFC 2822 format
	 * 
	 * @see http://www.faqs.org/rfcs/rfc2822.html
	 * @return string
	 */
	public function getRfc2822() {
		return date('r', $this->_timestamp);
	}
	
	/**
	 * Get a date format
	 * 
	 * This method will provide with the pattern that is used to format a date,
	 * in the requested format. Consider the following example, to obtain the
	 * formatting pattern of a "short" date, in DUTCH:
	 * 
	 * <code>
	 *    $date = new M_Date();
	 *    $date->getDateFormat(M_Date::SHORT, 'nl');
	 * </code>
	 * 
	 * NOTE:
	 * The formatting pattern contains special characters from the Locale Data
	 * (LDML files). In order to translate into other syntaxes (for example, for
	 * PHP's date() function), you can use the following methods:
	 * 
	 * - {@link M_Date::getDateFormatTranslated()}
	 * - {@link M_Date::getDateFormatTranslationTableForPHP()}
	 * - {@link M_Date::getDateFormatTranslationTableForJQueryUi()}
	 * 
	 * @static
	 * @access public
	 * @param string $format
	 * 		The date format.
	 * @param string $locale
	 * 		The locale name from which to retrieve the date pattern
	 * 		for date parsing.
	 * @return string
	 */
	public static function getDateFormat($format = NULL, $locale = NULL) {
		// set the format to default value, if not provided:
		if($format == NULL) {
			$format = self::MEDIUM;
		}
		// If the format is not one of the predefined formats, we interpret it
		// as a formatting pattern.
		else {
			if(! in_array($format, array(self::FULL, self::FULL_DATE_TIME, self::FULL_TIME, self::LONG, self::LONG_DATE_TIME, self::LONG_TIME, self::MEDIUM, self::MEDIUM_DATE_TIME, self::MEDIUM_TIME, self::SHORT, self::SHORT_DATE_TIME, self::SHORT_TIME))) {
				return $format;
			}
		}
		
		// if locale is not provided, set to locale that has been set in
		// the LC_TIME category
		if(! $locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_TIME);
		}
		
		// if still no locale has been found, we set the default formatting pattern
		if(! $locale) {
			// Set a default one (we default to english formatting)
			switch($format) {
				case self::FULL:
					return 'EEEE, MMMM d, yyyy';
				
				case self::FULL_DATE_TIME:
					return 'EEEE, MMMM d, yyyy h:mm:ss a v';
				
				case self::FULL_TIME:
					return 'h:mm:ss a v';
				
				case self::LONG:
					return 'MMMM d, yyyy';
				
				case self::LONG_DATE_TIME:
					return 'MMMM d, yyyy h:mm:ss a z';
				
				case self::LONG_TIME:
					return 'h:mm:ss a z';
				
				case self::MEDIUM:
					return 'MMM d, yyyy';
				
				case self::MEDIUM_DATE_TIME:
					return 'MMM d, yyyy h:mm:ss a';
				
				case self::MEDIUM_TIME:
					return 'h:mm:ss a';
				
				case self::SHORT:
					return 'M/d/yy';
				
				case self::SHORT_DATE_TIME:
					return 'M/d/yy h:mm a';
				
				// case self::SHORT_TIME:
				default:
					return 'h:mm a';
			}
		}
		// if the locale has been identified successfully, we load the
		// locale's data, in order to obtain the requested locale's data
		// and the date pattern:
		else {
			// Get locale data:
			$data = M_LocaleData::getInstance($locale);
			
			// Get date format:
			switch($format) {
				case self::FULL:
				case self::LONG:
				case self::MEDIUM:
				case self::SHORT:
					return $data->getDateFormat($format);
				
				case self::FULL_DATE_TIME:
					return $data->getDateFormat(self::FULL) . ' ' . $data->getTimeFormat(self::FULL);
				
				case self::LONG_DATE_TIME:
					return $data->getDateFormat(self::LONG) . ' ' . $data->getTimeFormat(self::LONG);
				
				case self::MEDIUM_DATE_TIME:
					return $data->getDateFormat(self::MEDIUM) . ' ' . $data->getTimeFormat(self::MEDIUM);
				
				case self::SHORT_DATE_TIME:
					return $data->getDateFormat(self::SHORT) . ' ' . $data->getTimeFormat(self::SHORT);
				
				case self::FULL_TIME:
					return $data->getTimeFormat(self::FULL);
				
				case self::LONG_TIME:
					return $data->getTimeFormat(self::LONG);
				
				case self::MEDIUM_TIME:
					return $data->getTimeFormat(self::MEDIUM);
				
				// case self::SHORT_TIME:
				default:
					return $data->getTimeFormat(self::SHORT);
			}
		}
	}
	
	/**
	 * Translate a date format
	 * 
	 * This method will translate the date formatting pattern, given a translation
	 * table. Typically, this is used to make date formats available to other
	 * libraries (such as jQuery UI)
	 * 
	 * Example 1, get short date format for jQuery UI:
	 * <code>
	 *    echo M_Date::getDateFormatTranslated(
	 *       M_Date::getDateFormat(M_Date::SHORT), 
	 *       M_Date::getDateFormatTranslationTableForJQueryUi()
	 *    );
	 * </code>
	 * 
	 * NOTE:
	 * M_Date provides with several translation tables, to easily prepare date
	 * formats in popular syntaxes:
	 * 
	 * - {@link M_Date::getDateFormatTranslationTableForPHP()}
	 * - {@link M_Date::getDateFormatTranslationTableForJQueryUI()}
	 * 
	 * @access public
	 * @param string $format
	 * 		The date format.
	 * @param string $locale
	 * 		The locale name from which to retrieve the date pattern
	 * 		for date parsing.
	 * @return string
	 */
	public static function getDateFormatTranslated($format, array $translationTable = array()) {
		// If the translation table is empty, we return the original format:
		if(count($translationTable) == 0) {
			return $format;
		}
		
		// Now, we have the date pattern to be used to render the date
		// into the correct format:
		$output  = '';
		
		// We explode the formatting pattern into literal, and non-literal parts:
		$parts   = explode("'", trim(str_replace("''", '$_', $format)));
		
		// Is the first part literal?
		$literal = empty($parts[0]);
		
		// Literal start- and end-quotes:
		$literalStartQuote = isset($translationTable['literalStartQuote']) ? $translationTable['literalStartQuote'] : '\'';
		$literalEndQuote   = isset($translationTable['literalEndQuote'])   ? $translationTable['literalEndQuote']   : '\'';
		
		// For each of the parts:
		for($i = $literal ? 1 : 0, $n = count($parts); $i < $n; $i ++) {
			// If literal, simply add to output:
			if($literal) {
				$output .= preg_replace(
					'/([^\s]{1})/im', 
					str_replace('\\', '\\\\', $literalStartQuote) . '$1' . str_replace('\\', '\\\\', $literalEndQuote), 
					str_replace('$_', "'", $parts[$i])
				);
			}
			// If non-literal, translate the special characters:
			else {
                //				$output .= str_replace('$_', "'", preg_replace(
//					'/([GyYuQqMLwWdDFgEecahHKkmsSAzZvV]+)/e',
//					'(isset($translationTable[\'$1\']) ? $translationTable[\'$1\'] : \'$1\');',
//					$parts[$i]
//				));

                $output .= str_replace('$_', "'", preg_replace_callback(
                    '/([GyYuQqMLwWdDFgEecahHKkmsSAzZvV]+)/',
                    function($matches) use ($translationTable) {
                        foreach($matches as $match){
                            if(isset($translationTable[$match])) {
                                return $translationTable[$match];
                            }

                            return $match;
                        }
                    },
                    $parts[$i]
                ));
			}
			
			// Switch literal state:
			$literal = ! $literal;
		}
		
		// return the translated pattern
		return $output;
	}
	
	/**
	 * Get translation table, for date format
	 * 
	 * This method will provide with a translation table, which can be used to
	 * translate a date format into a string that is compatible with PHP's built-in
	 * date() function.
	 * 
	 * @see M_Date::getDateFormatTranslated()
	 * @access public
	 * @return array
	 */
	public function getDateFormatTranslationTableForPHP() {
		return array(
			// Year. Normally the length specifies the padding, but 
			// for two letters it also specifies the maximum length.
			'y'     => 'Y',
			'yyy'   => 'Y',
			'yyyy'  => 'Y',
			'yyyyy' => '0Y',
			'yy'    => 'y',
			// Year
			'YY'    => 'y',
			'YYY'   => 'Y',
			'YYYY'  => 'Y',
			// Extended year
			'u'     => 'Y',
			// Quarter
			'Q'     => '',
			'QQ'    => '',
			'QQQ'   => '',
			'QQQQ'  => '',
			'q'     => '',
			'qq'    => '',
			'qqq'   => '',
			'qqqq'  => '',
			// Month - Use one or two for the numerical month, three for 
			// the abbreviation, or four for the full name, or five for 
			// the narrow name:
			// 1..2    09
			// 3       Sept
			// 4       September
			// 5       S
			'M'     => 'n', // Without leading zero
			'MM'    => 'm', // With leading zero
			'MMM'   => 'M',
			'MMMM'  => 'F',
			'MMMMM' => 'M',
			// Stand-Alone Month - Use one or two for the numerical month, 
			// three for the abbreviation, or four for the full name, or 
			// 5 for the narrow name
			'L'     => 'n', // Without leading zero
			'LL'    => 'm', // With leading zero
			'LLL'   => 'M',
			'LLLL'  => 'F',
			'LLLLL' => 'M',
			// Special symbol for Chinese leap month, used in combination 
			// with M. Only used with the Chinese calendar.
			'l'     => '',
			// Week of year
			'w'     => 'W',
			// Week of month
			'W'     => '',
			// Date - Day of the month
			'd'     => 'j',
			'dd'    => 'd', // with leading zero
			// Day of year
			'D'     => 'z',
			// Day of week in month
			// For example: "2nd Wed in July"
			'F'     => '',
			// Modified Julian day
			'g'     => '',
			// E
			// Day of week - Use one through three letters for the short 
			// day, or four for the full name, or five for the narrow name:
			// 1..3    Tues
			// 4       Tuesday
			// 5       T
			'E'     => 'D',
			'EE'    => 'D',
			'EEE'   => 'D',
			'EEEE'  => 'l',
			'EEEEE' => 'D',
			// AM/PM
			'a'     => 'a',
			// Hour [1-12]
			'h'     => 'g',
			'hh'    => 'h',
			// Hour [0-23]
			'H'     => 'G',
			'HH'    => 'H',
			// Hour [0-11]
			'K'     => 'g',
			'KK'    => 'h',
			// Hour [1-24]
			'k'     => 'G',
			'kk'    => 'H',
			// Minute. Use one or two for zero padding.
			'm'     => 'i',
			'mm'    => 'i',
			// Second. Use one or two for zero padding.
			's'     => 's',
			'ss'    => 's',
			// Fractional Second
			// - rounds to the count of letters.
			// (for example, 3457 is for 12.34567 - SSSS)
			'S'     => '',
			'SS'    => '',
			'SSS'   => '',
			'SSSS'  => '',
			'SSSSS' => '',
			// Milliseconds in day
			'A'     => '',
			// Timezones
			'z'     => '',
			'zz'    => '',
			'zzz'   => '',
			'zzzz'  => '',
			'Z'     => '',
			'ZZ'    => '',
			'ZZZ'   => '',
			'ZZZZ'  => '',
			'v'     => '',
			'vv'    => '',
			'vvv'   => '',
			'vvvv'  => '',
			'V'     => '',
			'VV'    => '',
			'VVV'   => '',
			'VVVV'  => '',
			// Literal start quote:
			'literalStartQuote' => '\\',
			// Literal end quote
			'literalEndQuote' => ''
		);
	}
	
	/**
	 * Get translation table, for date format
	 * 
	 * This method will provide with a translation table, which can be used to
	 * translate a date format into a string that is compatible with jQuery UI.
	 * Typically, this is used for jQuery UI's Datepicker.
	 * 
	 * @see M_Date::getDateFormatTranslated()
	 * @access public
	 * @return array
	 */
	public static function getDateFormatTranslationTableForJQueryUi() {
		return array(
			// Year. Normally the length specifies the padding, but 
			// for two letters it also specifies the maximum length.
			'y'     => 'y',
			'yyy'   => 'yy',
			'yyyy'  => 'yy',
			'yyyyy' => '0yy',
			'yy'    => 'y',
			// Year
			'YY'    => 'y',
			'YYY'   => 'yy',
			'YYYY'  => 'yy',
			// Extended year
			'u'     => '',
			// Quarter
			'Q'     => '',
			'QQ'    => '',
			'QQQ'   => '',
			'QQQQ'  => '',
			'q'     => '',
			'qq'    => '',
			'qqq'   => '',
			'qqqq'  => '',
			// Month - Use one or two for the numerical month, three for 
			// the abbreviation, or four for the full name, or five for 
			// the narrow name:
			// 1..2    09
			// 3       Sept
			// 4       September
			// 5       S
			'M'     => 'm', // Without leading zero
			'MM'    => 'mm', // With leading zero
			'MMM'   => 'M',
			'MMMM'  => 'MM',
			'MMMMM' => 'M',
			// Stand-Alone Month - Use one or two for the numerical month, 
			// three for the abbreviation, or four for the full name, or 
			// 5 for the narrow name
			'L'     => 'm', // Without leading zero
			'LL'    => 'mm', // With leading zero
			'LLL'   => 'M',
			'LLLL'  => 'MM',
			'LLLLL' => 'M',
			// Special symbol for Chinese leap month, used in combination 
			// with M. Only used with the Chinese calendar.
			'l'     => '',
			// Week of year
			'w'     => '',
			// Week of month
			'W'     => '',
			// Date - Day of the month
			'd'     => 'd',
			'dd'    => 'dd', // with leading zero
			// Day of year
			'D'     => 'o',
			// Day of week in month
			// For example: "2nd Wed in July"
			'F'     => '',
			// Modified Julian day
			'g'     => '',
			// E
			// Day of week - Use one through three letters for the short 
			// day, or four for the full name, or five for the narrow name:
			// 1..3    Tues
			// 4       Tuesday
			// 5       T
			'E'     => 'D',
			'EE'    => 'D',
			'EEE'   => 'D',
			'EEEE'  => 'DD',
			'EEEEE' => 'D',
			// e
			// Local day of week
			// Same as E except adds a numeric value that will depend on 
			// the local starting day of the week, using one or two letters. 
			// For this example, Monday is the first day of the week.
			// 1..2    2
			// 3       Tues
			// 4       Tuesday
			// 5       T
			'e'     => 'w',
			'ee'    => 'w',
			// AM/PM
			'a'     => '',
			// Hour [1-12]
			'h'     => '',
			'hh'    => '',
			// Hour [0-23]
			'H'     => '',
			'HH'    => '',
			// Hour [0-11]
			'K'     => '',
			'KK'    => '',
			// Hour [1-24]
			'k'     => '',
			'kk'    => '',
			// Minute. Use one or two for zero padding.
			'm'     => '',
			'mm'    => '',
			// Second. Use one or two for zero padding.
			's'     => '',
			'ss'    => '',
			// Fractional Second
			// - rounds to the count of letters.
			// (for example, 3457 is for 12.34567 - SSSS)
			'S'     => '',
			'SS'    => '',
			'SSS'   => '',
			'SSSS'  => '',
			'SSSSS' => '',
			// Milliseconds in day
			'A'     => '',
			// Timezones
			'z'     => '',
			'zz'    => '',
			'zzz'   => '',
			'zzzz'  => '',
			'Z'     => '',
			'ZZ'    => '',
			'ZZZ'   => '',
			'ZZZZ'  => '',
			'v'     => '',
			'vv'    => '',
			'vvv'   => '',
			'vvvv'  => '',
			'V'     => '',
			'VV'    => '',
			'VVV'   => '',
			'VVVV'  => '',
			// Literal start quote:
			'literalStartQuote' => '\'',
			// Literal end quote
			'literalEndQuote' => '\''
		);
	}
	
	/**
	 * Get date string
	 * 
	 * This method will parse the date string, in the requested format
	 * of the specified locale. Both parameters are optional. They are 
	 * defaulted to the following values:
	 * 
	 * <code>format</code>
	 * The format is defaulted to "medium" (M_Date::MEDIUM). The possible 
	 * values of this parameter are M_Date::FULL, M_Date::LONG, M_Date::MEDIUM, 
	 * or M_Date::SHORT.
	 * 
	 * <code>locale</code>
	 * The locale name is defaulted to the one that has been set in the
	 * locale category LC_TIME, see {@link M_Locale::getCategory()} for 
	 * more info. The locale's data will be used to fetch locale data
	 * (date patterns, calendar items, etc.)
	 * 
	 * For more information on how dates should be parsed, using date 
	 * patterns that are retrieved from a locale's data, read the docs
	 * on {@link M_LocaleData::getDateFormat()}.
	 * 
	 * IMPORTANT NOTE:
	 * If no locale name has been provided to this method, and no locale
	 * has been set in any of the locale categories, this method will
	 * use the following date patterns by default:
	 * 
	 * <code>
	 *    full       EEEE, MMMM d, yyyy
	 *    long       MMMM d, yyyy
	 *    medium     MMM d, yyyy
	 *    short      M/d/yy
	 * </code>
	 * 
	 * Note that, in this case, the symbols will be replaced by english
	 * names.
	 * 
	 * Example 1, get short format of date, in Dutch
	 * <code>
	 *    $date = new M_Date;
	 *    echo $date->toString(M_Date::SHORT, 'nl');
	 * </code>
	 * 
	 * Example 2, get short format of date, in the current locale
	 * <code>
	 *    Locale::setCategory(Locale::LC_TIME, 'es');
	 *    
	 *    $date = new M_Date;
	 *    echo $date->toString(M_Date::SHORT);
	 * </code>
	 * 
	 * Thanks to the 4 predefined constants, you never should have to think
	 * about how a date should be parsed. However, in some cases, you might
	 * want to enforce your own date pattern. If you want to alter the
	 * date pattern in which the date will be parsed, just pass in a date
	 * pattern instead of a date format! Check out Example 3 to learn more.
	 * Again, you should read {@link M_LocaleData::getDateFormat()} for more
	 * information on how to compose date patterns.
	 * 
	 * Example 3, get my own date format, in Dutch
	 * <code>
	 *    $date = new M_Date;
	 *    echo $date->toString('yy-MM-dd hh:mm:ss a', 'nl');
	 * </code>
	 * 
	 * Example 3, get my own date format, in the current locale
	 * <code>
	 *    Locale::setCategory(Locale::LC_TIME, 'es');
	 *    
	 *    $date = new M_Date;
	 *    echo $date->toString('yy-MM-dd hh:mm:ss a');
	 * </code>
	 * 
	 * @access public
	 * @param string $format
	 * 		The date format.
	 * @param string $locale
	 * 		The locale name from which to retrieve the date pattern
	 * 		for date parsing.
	 * @return string
	 */
	public function toString($format = NULL, $locale = NULL) {
		// If locale is not provided
		if(! $locale) {
			// Set to locale that has been set in the LC_TIME category
			$locale = M_Locale::getCategory(M_Locale::LC_TIME);
		}
		
		// Set $data to...
		$data = ! $locale
			// NULL, if still no locale has been found
			? NULL
			// Locale Data, if a locale is known by now:
			: M_LocaleData::getInstance($locale);
		
		// The output string:
		$out = '';
		
		// We explode the date format into pieces. For each of the pieces:
		foreach(self::_getArrayFromDateFormat($format, $locale) as $current) {
			// If the current string is not literal:
			if(! $current['isLiteral']) {
				// Then, convert the current piece to a localized date part:
				$out .= $this->getLocaleDateSymbol($current['string'], $data);
			}
			// In literal:
			else {
				// We add the current string as-is, to the output
				$out .= $current['string'];
			}
		}
		
		// return the output string:
		return $out;
	}

	/**
	 * Get relative date (string)
	 * 
	 * This will compare the date and time that is represented by the object
	 * to the current date and time. Based on the difference, this method
	 * will return a relative string, describing the time that has passed
	 * by in the calculated difference. The result will be localized into
	 * the currently active locale, or in the locale that has been provided
	 * to this method.
	 * 
	 * Example 1
	 * <code>
	 *    $date = new M_Date;
	 *    $date->subtractTime(1, M_Date::DAY);
	 *    echo $date->toRelativeString(); // will output "yesterday"
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * This method will look up the currently active locale name by fetching
	 * the value of the locale category LC_TIME. Read the documentation
	 * on {@link M_Locale::getCategory()} to learn more.
	 * 
	 * Example 2
	 * <code>
	 *    Locale::setCategory(Locale::LC_TIME, 'es');
	 *    
	 *    $date = new M_Date;
	 *    $date->subtractTime(1, M_Date::DAY);
	 *    echo $date->toRelativeString(); // will output "ayer"
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * If the difference is too big for any relative string to make sense,
	 * this method will return the formatted date (see {@link M_Date::toString()}),
	 * using the provided date format. Typically, this method will go as far as 
	 * shown below:
	 * 
	 * - "Yesterday"
	 * - "Today"
	 * - "Tomorrow"
	 * 
	 * @access public
	 * @param string $format
	 * 		The date format.
	 * @param string $locale
	 * 		The locale name
	 * @return string
	 */
	public function toRelativeString($format, $locale = NULL) {
		
		/*
		 * @internal: this calculation wasn't correct. If the difference between
		 * the date and now was less than 24hours, this always resulted in "today",
		 * though the date could be e.g. 23pm yesterday
		 */
		// We still don't load the locale's data. First, we calculate 
		// the difference between the date and today. If that difference
		// is not too big, then we'll check if any string is available to
		// describe the difference in the locale's data.
		
		// $diff = ceil(($this->_timestamp - time()) / self::SECONDS_IN_DAY);
		
		/**
		 * @internal: use the internal methods to check if this date is today,
		 * yesterday or tomorrow. Set diff by default to a number higher as 1
		 */
		$diff = 10;
		if ($this->isToday()) {
			$diff = 0;
		}elseif($this->isTomorrow()) {
			$diff = 1;
		}elseif($this->isYesterday()) {
			$diff = -1;
		}
		
		// If the difference is max 2 days:
		if($diff < 3 && $diff > -3) {
			// If locale is not provided
			if(! $locale) {
				// Fall back to locale that has been set in the LC_TIME category
				$locale = M_Locale::getCategory(M_Locale::LC_TIME);
			}
			
			// If locale is known:
			if($locale) {
				// Load the locale data:
				$data = M_LocaleData::getInstance($locale);
				
				// Get the relative day name (e.g. Yesterday, Today, ...)
				$relativeDayName = $data->getCalendarFieldName('day', ($diff > 0 ? ceil($diff) : floor($diff)));
			}
			// If locale is not known, we default to english (as usual)
			else {
				switch($diff) {
					case -2:
						$relativeDayName = false;
						break;
					
					case -1:
						$relativeDayName = 'yesterday';
						break;
					
					case 1:
						$relativeDayName = 'tomorrow';
						break;
					
					case 2:
						$relativeDayName = false;
						break;
					
					// case 0:
					default:
						$relativeDayName = 'today';
						break;
				}
			}
			
			// If relative day name is available, we return it:
			if($relativeDayName) {
				// Depending on the format, we add some information to the 
				// day name:
				switch($format) {
					case self::FULL_DATE_TIME:
					case self::FULL_TIME:
						return $relativeDayName . ' ' . $this->toString(self::FULL_TIME, $locale);
					
					case self::LONG_DATE_TIME:
					case self::LONG_TIME:
						return $relativeDayName . ' ' . $this->toString(self::LONG_TIME, $locale);
					
					case self::MEDIUM_DATE_TIME:
					case self::MEDIUM_TIME:
						return $relativeDayName . ' ' . $this->toString(self::MEDIUM_TIME, $locale);
					
					case self::SHORT_DATE_TIME:
					case self::SHORT_TIME:
						return $relativeDayName . ' ' . $this->toString(self::SHORT_TIME, $locale);
					
					default:
						return $relativeDayName;
				}
			}
			// If not, we format the date, and return that as the result:
			else {
				return $this->toString($format, $locale);
			}
		}
		
		// If we're still here, it means that the difference in time was too big
		// for a relative string to have sense. In this case, we simply return a
		// formatted date:
		return $this->toString($format, $locale);
	}
	
	/**
	 * Get timespan string
	 * 
	 * @access public
	 * @param M_Date $to
	 * 		The date to use as "end" date
	 * @return string
	 */
	public function toTimespanString(M_Date $to) {
		// TODO: calculate timespan, and render for display!
		// e.g. From 31 december 1900 to 1 january 2009
	}
	
	/**
	 * Get the value of a date symbol
	 * 
	 * This method is used by the {@link M_Date::toString()} method, to get
	 * the value of a date symbol in the date format string, or date pattern.
	 * 
	 * Example 1, get date's month name in Dutch
	 * <code>
	 *    $date = new M_Date;
	 *    echo $date->getLocaleDateSymbol('MMMM', LocaleData::getInstance('nl'));
	 * </code>
	 * 
	 * Example 1, get date's weekday name in Dutch
	 * <code>
	 *    $date = new M_Date;
	 *    echo $date->getLocaleDateSymbol('EEEE', LocaleData::getInstance('nl'));
	 * </code>
	 * 
	 * Check the documentation on {@link M_LocaleData::getDateFormat()} to learn
	 * more about the symbols that can be requested from this method.
	 * 
	 * @access public
	 * @param string $symbol
	 * 		The date symbol. Possible base values: G y Y u Q q M L w W d D F g E 
	 * 		e c a h H K k m s S A z Z v V
	 * @param LocaleData $data
	 * 		The locale data from which the value of the requested symbol is
	 * 		fetched. Note that this parameter is optional. If omitted, this
	 * 		method will use PHP's {@link date()} function to get locale
	 * 		dependent values.
	 * @return string
	 */
	public function getLocaleDateSymbol($symbol, M_LocaleData $data = NULL) {
		switch($symbol) {
			// Year. Normally the length specifies the padding, but 
			// for two letters it also specifies the maximum length. 
			case 'y':
			case 'yyy':
			case 'yyyy':
			case 'yyyyy':
				return $this->_addZeroPadding($this->_year, strlen($symbol));
			
			case 'yy':
				return substr((string) $this->_year, -2);
			
			// Year
			case 'YY':
			case 'YYY':
			case 'YYYY':
				return substr((string) $this->_year, -strlen($symbol));
			
			// TODO: extended year
			case 'u':
				break;
			
			// TODO: Quarter - Use one or two for the numerical quarter, three 
			// for the abbreviation, or four for the full name.
			// 1..2    02
			// 3       Q2
			// 4       2nd quarter
			case 'Q':
			case 'QQ':
				break;
			
			case 'QQQ':
				break;
			
			case 'QQQQ':
				break;
			
			// TODO: Stand-Alone Quarter - Use one or two for the numerical 
			// quarter, three for the abbreviation, or four for the full 
			// name.
			case 'q':
			case 'qq':
				break;
			
			case 'qqq':
				break;
			
			case 'qqqq':
				break;
			
			// Month - Use one or two for the numerical month, three for 
			// the abbreviation, or four for the full name, or five for 
			// the narrow name:
			// 1..2    09
			// 3       Sept
			// 4       September
			// 5       S
			case 'M':
			case 'MM':
				return $this->_addZeroPadding($this->_month, strlen($symbol));
			
			case 'MMM':
				if($data == NULL) {
					return date('M', $this->_timestamp);
				} else {
					return $data->getMonth($this->_month, 'abbreviated');
				}
				break;
			
			case 'MMMM':
				if($data == NULL) {
					return date('F', $this->_timestamp);
				} else {
					return $data->getMonth($this->_month, 'wide');
				}
				break;
			
			case 'MMMMM':
				if($data == NULL) {
					return substr(date('M', $this->_timestamp), 0, 1);
				} else {
					return $data->getMonth($this->_month, 'narrow');
				}
				break;
			
			// Stand-Alone Month - Use one or two for the numerical month, 
			// three for the abbreviation, or four for the full name, or 
			// 5 for the narrow name
			case 'L':
			case 'LL':
				break;
			
			case 'LLL':
				break;
			
			case 'LLLL':
				break;
			
			case 'LLLLL':
				break;
			
			// Special symbol for Chinese leap month, used in combination 
			// with M. Only used with the Chinese calendar.
			case 'l':
				break;
			
			// Week of year
			case 'w':
				return date('W', $this->_timestamp);
			
			// TODO: Week of month
			case 'W':
				break;
			
			// Date - Day of the month
			case 'd':
			case 'dd':
				return $this->_addZeroPadding($this->_day, strlen($symbol));
			
			// Day of year
			case 'D':
				return date('z', $this->_timestamp) + 1;
			
			// TODO: Day of Week in Month.
			// For example: "2nd Wed in July"
			case 'F':
				break;
			
			// TODO: Modified Julian day.
			case 'g':
				break;
			
			// E
			// Day of week - Use one through three letters for the short 
			// day, or four for the full name, or five for the narrow name:
			// 1..3    Tues
			// 4       Tuesday
			// 5       T
			
			// TODO: c
			// Stand-Alone local day of week - Use one letter for the 
			// local numeric value (same as 'e'), three for the short day, 
			// or four for the full name, or five for the narrow name.
			// 1       2
			// 3       Tues
			// 4       Tuesday
			// 5       T
			case 'E':
			case 'EE':
			case 'EEE':
				if($data == NULL) {
					return date('D', $this->_timestamp);
				} else {
					return $data->getDay($this->_weekday, 'abbreviated');
				}
				break;
			
			case 'EEEE':
				if($data == NULL) {
					return date('l', $this->_timestamp);
				} else {
					return $data->getDay($this->_weekday, 'wide');
				}
				break;
			
			case 'EEEEE':
				if($data == NULL) {
					return substr(date('D', $this->_timestamp), 0, 1);
				} else {
					return $data->getDay($this->_weekday, 'narrow');
				}
				break;
			
			// e
			// Local day of week
			// Same as E except adds a numeric value that will depend on 
			// the local starting day of the week, using one or two letters. 
			// For this example, Monday is the first day of the week.
			// 1..2    2
			// 3       Tues
			// 4       Tuesday
			// 5       T
			case 'e':
			case 'ee':
				return $this->_addZeroPadding($this->getWeekday(), strlen($symbol));
			
			// AM or PM
			case 'a':
				if($data == NULL) {
					return date('a', $this->_timestamp);
				} else {
					return $data->getMeridiem($this->_hour > 11 ? 'PM' : 'AM');
				}
				break;
			
			// Hour [1-12]
			case 'h':
			case 'hh':
				$h = ($this->_hour > 12) ? $this->_hour - 12 : $this->_hour;
				if($h == 0) {
					$h = 12;
				}
				return $this->_addZeroPadding($h, strlen($symbol));
			
			// Hour [0-23]
			case 'H':
			case 'HH':
				return $this->_addZeroPadding($this->_hour, strlen($symbol));
			
			// Hour [0-11]
			case 'K':
			case 'KK':
				$h = ($this->_hour > 11) ? $this->_hour - 12 : $this->_hour;
				return $this->_addZeroPadding($h, strlen($symbol));
			
			// Hour [1-24]
			case 'k':
			case 'kk':
				$h = ($this->_hour == 0) ? 24 : $this->_hour;
				return $this->_addZeroPadding($h, strlen($symbol));
			
			// Minute. Use one or two for zero padding.
			case 'm':
			case 'mm':
				return $this->_addZeroPadding($this->_minute, strlen($symbol));
			
			// Second. Use one or two for zero padding.
			case 's':
			case 'ss':
				return $this->_addZeroPadding($this->_second, strlen($symbol));
			
			// TODO: Fractional Second
			// - rounds to the count of letters.
			// (for example, 3457 is for 12.34567 - SSSS)
			case 'S':
			case 'SS':
				break;
			
			// TODO: Milliseconds in day
			case 'A':
				break;
			
			// TODO: timezones
			// http://www.unicode.org/reports/tr35/#Date_Field_Symbol_Table
			case 'z':
			case 'zz':
			case 'zzz':
			case 'zzzz':
			case 'Z':
			case 'ZZ':
			case 'ZZZ':
			case 'ZZZZ':
			case 'v':
			case 'vv':
			case 'vvv':
			case 'vvvv':
			case 'V':
			case 'VV':
			case 'VVV':
			case 'VVVV':
				break;
			
			default:
				return $symbol;
		}
	}
	
	/* -- PHP Magic Methods -- */
	
	/**
	 * Export to string
	 * 
	 * This is a "magic method", called automatically by PHP when the
	 * M_Date object is being casted to a string. For example, an echo 
	 * call on the M_Date object will print the result of this function.
	 * 
	 * Example 1
	 * <code>
	 *    $date = new M_Date;
	 *    echo $date; // prints the result of __toString()
	 * </code>
	 * 
	 * Example 2
	 * <code>
	 *    $date = new M_Date;
	 *    $str  = (string) $date; // saves the result of __toString()
	 * </code>
	 * 
	 * This method will parse the SHORT date format, in the currently 
	 * active locale. Read {@link M_Date::toString()} for more info.
	 * 
	 * Example 3 (has exactly the same result as Example 1)
	 * <code>
	 *    $date = new M_Date;
	 *    echo $date->toString(M_Date::SHORT);
	 * </code>
	 * 
	 * @access public
	 * @return string
	 */
	public function __toString() {
		// return $this->toString(M_Date::SHORT);
		return $this->toRelativeString(M_Date::SHORT);
	}
	
	/**
	 * Magic Getter
	 * 
	 * Check the documentation on {@link M_Date::get()} to learn more 
	 * about the magic getter in the context of an M_Date object.
	 * 
	 * @access public
	 * @uses M_Date::get()
	 * @param string $part
	 * 		The (date) part you want to get from the date.
	 * @return string
	 */
	public function __get($part) {
		return $this->get($part);
	}
	
	/**
	 * Magic Setter
	 * 
	 * Check the documentation on {@link M_Date::set()} to learn more 
	 * about the magic setter in the context of an M_Date object.
	 * 
	 * @access public
	 * @uses M_Date::set()
	 * @param integer $value
	 * 		The new value of the date part
	 * @param string $part
	 * 		The (date) part you want to set in the date.
	 * @return string
	 */
	public function __set($part, $value) {
		$this->set($value, $part);
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Initiate the M_Date object, based on a timestamp
	 * 
	 * This method is for internal use only (note the private 
	 * keyword in the method's signature). It is used by the
	 * M_Date constructor and other methods to (re-)set the
	 * values of internal variables.
	 * 
	 * @access protected
	 * @param integer $time
	 * 		The UNIX timestamp from which to extract the values
	 * 		of internal variables.
	 * @return void
	 */
	protected function _initFromTimestamp($time) {
		$temp = getdate($time);
		$this->_timestamp = $time;
		$this->_year = (int) $temp['year'];
		$this->_month = (int) $temp['mon'];
		$this->_day = (int) $temp['mday'];
		$this->_weekday = (int) $temp['wday']; // 0 (for Sunday) through 6 (for Saturday)
		$this->_hour = (int) $temp['hours'];
		$this->_minute = (int) $temp['minutes'];
		$this->_second = (int) $temp['seconds'];
	}
	
	/**
	 * Add zero padding to a number
	 * 
	 * ... is used by date formatting functions of the M_Date class
	 * 
	 * @access protected
	 * @param integer $number
	 * 		The number to which zero padding should be added
	 * @param integer $length
	 * 		The length of the padding
	 * @return string
	 */
	protected function _addZeroPadding($number, $length) {
		$s = (string) $number;
		$l = strlen($s);
		if($l < $length) {
			return str_repeat('0', ($length - $l)) . $s;
		} else {
			return $s;
		}
	}
	
	/**
	 * Get array from date format
	 * 
	 * This protected method is used internally, in order to break up a given
	 * date format string into an array that contains the pieces in the format.
	 * The result of this method is an array, in which each value in turn is 
	 * another array. These arrays contain the piece extracted and a boolean that
	 * indicates whether or not the piece is literal.
	 * 
	 * A literal piece in the date format is a piece of text that is not converted
	 * to a date part. It is escaped in the date format string by wrapping it with 
	 * single quotes.
	 * 
	 * Examples:
	 * <code>
	 *    M_Date::_getArrayFromDateFormat(M_Date::SHORT, 'en');
	 *    M_Date::_getArrayFromDateFormat(M_Date::SHORT);
	 *    M_Date::_getArrayFromDateFormat('dd/MM/yyyy');
	 * </code>
	 * 
	 * @static
	 * @access protected
	 * @param string $format
	 * @param string $locale
	 * @return array
	 */
	protected static function _getArrayFromDateFormat($format, $locale = NULL) {
		// Convert the provided format into a string with date symbols.
		$format = self::getDateFormat($format, $locale);
		
		// Output array:
		$out = array();
		
		// First of all, we replace the string '' by a temporary string. We'll
		// explode the string with the ' as the glue character, and two single
		// quotes would break that method. Two single quotes will later be converted
		// back to a single quote. Then, we explode the string into an array:
		$elements = explode("'", trim(str_replace("''", '$_', $format)));
		
		// We prepare a boolean that indicates whether or not the current piece
		// is literal. If the first element in the array is empty, then the first
		// piece in the date format is literal:
		$isLiteral = empty($elements[0]);
		
		// For each of the elements in the array:
		for($i = $isLiteral ? 1 : 0, $n = count($elements); $i < $n; $i ++) {
			// We restore the single quote that we replaced earlier with a 
			// temporary string
			$current = str_replace('$_', "'", $elements[$i]);
			
			// If the current element is literal:
			if($isLiteral) {
				// Then, we add the element as-is to the output:
				array_push($out, array(
					'string'    => $current,
					'isLiteral' => TRUE
				));
			}
			// If the current element is not literal:
			else {
				// Create a temporary string, in which we'll group symbols:
				$symbol = '';
				
				// Another string, to group other characters:
				$misc = '';
				
				// Then, we'll go through each of the characters in the current
				// string in order to group symbols. Only symbols will be added
				// as non-literal to the output. So, for each of the characters:
				for($j = 0, $m = strlen($current), $jlast = $m - 1; $j < $m; $j ++) {
					// If the current character is a symbol:
					if(strpos('GyYuQqMLwWdDFgEecahHKkmsSAzZvV', $current[$j]) !== FALSE) {
						// If other characters have been grouped in the previous
						// iteration:
						if($misc != '') {
							// Then, we add an entry to the output array:
							array_push($out, array(
								'string'    => $misc,
								'isLiteral' => TRUE
							));
							
							// And, we empty the temporary string:
							$misc = '';
						}
						
						// Then, we'll add the character to the temporary string.
						// First, we make sure that the temporary string is empty,
						// or that it contains the same symbol (for grouping).
						// If that is not the case:
						if(! ($symbol == '' || $symbol[0] == $current[$j])) {
							// Then, we add an entry to the output array with the
							// previous group of symbols:
							array_push($out, array(
								'string'    => $symbol,
								'isLiteral' => FALSE
							));
							
							// We empty the previous group, in order to create
							// a new one:
							$symbol = '';
						}
						
						// We add the current symbol to the group string:
						$symbol .= $current[$j];
						
						// If this is the last character in the string:
						if($j == $jlast) {
							// Then, we add an entry to the output array with the
							// current group of symbols:
							array_push($out, array(
								'string'    => $symbol,
								'isLiteral' => FALSE
							));
						}
					}
					// If the current character is not a symbol:
					else {
						// If other symbols have been grouped in the previous
						// iteration:
						if($symbol != '') {
							// Then, we add an entry with the grouped symbols to 
							// the output array:
							array_push($out, array(
								'string'    => $symbol,
								'isLiteral' => FALSE
							));
							
							// And, we empty the temporary string:
							$symbol = '';
						}
						
						// We add the current character to the group:
						$misc .= $current[$j];
						
						// If this is the last character in the string:
						if($j == $jlast) {
							// Then, we add an entry to the output array with the
							// current group of characters:
							array_push($out, array(
								'string'    => $misc,
								'isLiteral' => FALSE
							));
						}
					}
				}
			}
			
			// The next element in the array is not literal if the current one
			// was, and vice versa:
			$isLiteral = ! $isLiteral;
		}
		
		// Return the array:
		return $out;
	}
	
	/**
	 * Get new date in timezone
	 * 
	 * This method allows you to the convert a date between two given timezones
	 * If the original timezone is not given, we take the default application timezone
	 * into account.
	 * 
	 * Please note that in this method, default PHP-classes are responsible for
	 * the conversion calculations.
	 * These classes are supported by PHP 5.2 or higher
	 * 
	 * @param string $timezone
	 * @param string $originalTimezone
	 * @return M_Date
	 */
	public function getNewDateInTimezone($timezone, $originalTimezone = null) {
		// Check if the wanted timezone an the original timezone are the same
		if($timezone == $originalTimezone) {
			return new M_Date($this->getMysqlDateTime());
		}
		
		// If the original timezone is not provided, the default timezone of the
		// application is taken into account
		if(is_null($originalTimezone)) {
			$originalTimezone = new DateTimeZone(M_DateTimezone::getDefaultTimezone());
		} else {
			$originalTimezone = new DateTimeZone($originalTimezone);
		}
		
		// We create a {@link DateTime} object to do the timezone calculation of this
		// specific date
		$dateTimeCalc = new DateTime($this->getMysqlDateTime(), $originalTimezone);
		// At this point, the magic takes place and the date will be converted to the 
		// given timezone
		$dateTimeCalc->setTimezone(new DateTimeZone($timezone));
		
		// We return a new date, so everything in the {@link M_Date} class can 
		// be initiated again
		return new M_Date($dateTimeCalc->format('Y-m-d H:i:s'));
	}
}