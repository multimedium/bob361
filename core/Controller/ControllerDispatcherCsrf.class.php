<?php
/**
 * M_ControllerDispatcherCsrf
 * 
 * @package Core
 */
class M_ControllerDispatcherCsrf extends M_ObjectSingleton {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Singleton instance
	 * 
	 * @static
	 * @access private
	 * @var M_ControllerDispatcherCsrf
	 */
	private static $_instance;
	
	/**
	 * Session Namespace
	 * 
	 * @see M_ControllerDispatcherCsrf::_getSessionNamespace()
	 * @access private
	 * @var M_SessionNamespace
	 */
	private $_sessionNamespace;
	
	/**
	 * Rules
	 * 
	 * @see M_ControllerDispatcherCsrf::addRuleProtected()
	 * @access private
	 * @var array
	 */
	private $_rules = array();
	
	/**
	 * Flag: Enable Unique Token Per Page Request?
	 * 
	 * @see M_ControllerDispatcherCsrf::setEnableUniqueTokenPerPageRequest()
	 * @access private
	 * @var bool
	 */
	private $_enabledUniqueTokenPerPageRequest = FALSE;
	
	/**
	 * Enabled?
	 * 
	 * @see M_ControllerDispatcherCsrf::setEnabled()
	 * @access private
	 * @var bool
	 */
	private $_enabled = FALSE;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * PRIVATE Constructor
	 * 
	 * @access public
	 * @return ControllerDisparcherCsrf
	 */
	private function __construct() {}
	
	/**
	 * Singleton Constructor
	 * 
	 * @static
	 * @access public
	 * @return M_ControllerDispatcherCsrf
	 */
	public static function getInstance() {
		// If the singleton instance has not yet been constructed
		if(! self::$_instance) {
			// Then, construct now:
			self::$_instance = new self();
		}
		
		// Return the singleton
		return self::$_instance;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Add Rule for protected URI
	 * 
	 * An attacker would create forged HTTP requests and trick a victim into 
	 * submitting them via image tags, XSS, or numerous other techniques. In doing
	 * so, CSRF takes advantage of web applications that allow attackers to 
	 * predict all the details of a particular action. Since browsers send 
	 * credentials like session cookies automatically, attackers can create 
	 * malicious web pages which generate forged requests that are indistinguishable 
	 * from legitimate ones.
	 * 
	 * Preventing CSRF requires the inclusion of an unpredictable token in the 
	 * body or URL of each HTTP request. Such tokens should at a minimum be 
	 * unique per user session, but can also be unique per page request.
	 * 
	 * By adding a rule, you can protect a URL. Once the rule is added, all links
	 * that are rendered by the application will include such a unique token for
	 * protection against CSRF.
	 * 
	 * @access public
	 * @param string $rule
	 * @return M_ControllerDispatcherCsrf $csrf
	 *		Returns itself, for a fluent programming interface
	 */
	public function addRuleProtected($rule) {
		// Add the rule:
		array_push($this->_rules, (string) $rule);
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set flag: Enable Unique Token Per Page Request
	 * 
	 * Preventing CSRF requires the inclusion of an unpredictable token in the 
	 * body or URL of each HTTP request. Such tokens should at a minimum be 
	 * unique per user session, but can also be unique per page request.
	 * 
	 * By providing TRUE to this argument, you enable a unique token per page
	 * request, thus creating a stronger protection against CSRF.
	 * 
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE to enable a unique token per page request, FALSE to 
	 *		use a unique token for the entire duration of the user's session.
	 * @return M_ControllerDispatcherCsrf $csrf
	 *		Returns itself, for a fluent programming interface
	 */
	public function setEnableUniqueTokenPerPageRequest($flag) {
		// Set to enabled/disabled:
		$this->_enabledUniqueTokenPerPageRequest = (bool) $flag;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Enable/Disable Protection against CSRF 
	 * 
	 * @access public
	 * @param bool $flag
	 * @return M_ControllerDispatcherCsrf $csrf
	 *		Returns itself, for a fluent programming interface
	 */
	public function setEnabled($flag) {
		// Set to enabled/disabled:
		$this->_enabled = (bool) $flag;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set Memory For Next Page Request
	 * 
	 * Typically, this method is called by {@link M_ControllerDispatcher}, in 
	 * order to prepare for the next page request. This enables the application
	 * to validate the unique token when the next page request is handled.
	 * 
	 * @access public
	 * @return M_ControllerDispatcherCsrf $csrf
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPersistentMemoryForNextPageRequest() {
		// For each of the rules defined for protection:
		foreach($this->_rules as $rule) {
			// Move the sequence number to the next position, only if a unique
			// token per page request.
			// (This will also cause the previous OTP to be stored in memory)
			$this->_getOtpSequenceNumberAndSeed($rule, TRUE);
		}
		
		// Return myself
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Validate a URI
	 * 
	 * This method can be used, and is used by {@link M_ControllerDispatcher},
	 * to assess whether or not a given URI is secure. If the URI provided is not
	 * expected to be protected against CSRF, this function will return TRUE.
	 * However, if protection is expected, this function will make sure the unique
	 * token is present in the URI, and:
	 * 
	 * - Return TRUE if the unique token is correct
	 * - Return FALSE if the unique token is not valid
	 * 
	 * NOTE:
	 * If the URI is protected, and is considered valid by this function, the
	 * unique token will be removed from the URI provided.
	 * 
	 * @access public
	 * @param M_Uri $uri
	 *		The URI that is to be validated by this method
	 * @return bool $flag
	 *		Returns TRUE if the URI is valid, FALSE if not
	 */
	public function getValidationAndRemoveProtectionIn(M_Uri & $uri) {
		// We extract the locale and the path from the URI provided:
		list($locale, $path) = M_ControllerDispatcherPathRewriter::getLocaleAndPathFromUri($uri);
		
		// Get the rule that matches with the path:
		$rule = $this->_getRuleMatchingPath($path);
		
		// If a rule can be found, it means that the path should have been 
		// protected against CSRF. However, if no rule is found:
		if(! $rule) {
			// Then, no protection is expected. We return SUCCESS, since we
			// do not need to validate the protection:
			return TRUE;
		}
		
		// If we are still here, it means that a rule has been defined. Once a
		// rule applies to a URI, we expect protection against CSRF to be present
		// in the path of the URI. This protection will be present in the form
		// of a One-Time-Password as the last path element. So, we get the last
		// element in the path:
		list($otp, $path) = M_Uri::popElementFromPath($uri->getPath());
		
		// Now, we assume that $otp holds the One-Time-Password. We need to validate
		// this OTP to make sure the URI is secure. In order to do this, we compare
		// this OTP to the OTP generated in the previous page request. If they
		// match:
		if($otp == $this->_getOtpPrevious($rule)) {
			// Then, the protection is valid. We trim off the protection. We 
			// reuse the path without OTP to do so:
			$uri->setPath($path);
			
			// Return SUCCESS to indicate a valid URI:
			return TRUE;
		}
		
		// If we are still here, it means that the OTP is not okay. We return
		// FAILURE as the outcome of the validation:
		return FALSE;
	}
	
	/**
	 * Get Protected HTML
	 * 
	 * This method is used by the application in order to secure the HTML source
	 * code against CSRF attacks. It will look for all URI's used in the source
	 * code, and add unique tokens wherever required. The return value of this 
	 * function is the modified HTML Source Code.
	 * 
	 * Typically, this function is used as a callback function for PHP's output
	 * buffering - see {@link ob_start()}.
	 * 
	 * @access public
	 * @param string $sourceCode
	 * @return string
	 */
	public function getHtmlProtected($sourceCode) {
		// If not enabled:
		if(! $this->_enabled) {
			// Do nothing:
			return $sourceCode;
		}
		
		// If no rules for protection have been added
		if(count($this->_rules) == 0) {
			// Do nothing:
			return $sourceCode;
		}
		
		// If we are still here, then protection against CSRF is enabled. So, the
		// first step in protecting the HTML is to look for any URL in the source
		// code. We look for URL's that have been wrapped by single or double quotes,
		// as HTML attributes, javascript variables, etc.
		return preg_replace_callback(
			'/([\'"]{1})(https?:\/\/[^\'"]+)([\'"]{1})/i',
			array($this, '_getProtectedHtmlMatches'),
			$sourceCode
		);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Parse matched URI
	 * 
	 * Is used internally in order to add protection to URI's found in the HTML 
	 * source code that is processed by {@link M_ControllerDispatcherCsrf::getHtmlProtected()}.
	 * 
	 * @access protected
	 * @param array $matches
	 * @return string
	 */
	protected function _getProtectedHtmlMatches(array $matches) {
		// Construct an instance of M_Uri to work with:
		$uri = new M_Uri($matches[2]);
		
		// We extract the locale and the path from the URI:
		list($locale, $path) = M_ControllerDispatcherPathRewriter::getLocaleAndPathFromUri($uri);
		
		// Get the rule that matches with the path:
		$rule = $this->_getRuleMatchingPath($path);
		
		// If a rule can be found, it means that the path must be protected
		// against CSRF. However, if no rule is found:
		if(! $rule) {
			// Then, we leave the original piece of HTML unaffected:
			return $matches[0];
		}
		
		// If the URI is pointing to an existing file on the server, we will not
		// protect the URI. That would break our application, since another path
		// element would then be appended to the URI, thus rendering a 404 error 
		// for the file
		// (we assume this can only be the case if no locale was extracted)
		if(! $locale) {
			// Construct the path to the file:
			$file = new M_File($path);
			
			// If referring to an existing file:
			if($file->exists()) {
				// Then, we leave the original piece of HTML unaffected:
				return $matches[0];
			}
		}
		
		// Preventing CSRF requires the inclusion of an unpredictable token in 
		// the body or URL of each HTTP request. Such tokens should at a minimum 
		// be unique per user session, but can also be unique per request for a 
		// stronger protection. We will generate a One-Time-Password as unique
		// token, unique for each of the protected rules and unique per page request.
		// Add the One-Time-Password to the URI:
		$uri->appendPath($this->_getOtp($rule));
		
		// Return the final result
		return $matches[1] . $uri->toString() . $matches[3];
	}
	
	/**
	 * Get Protection Rule for given path
	 * 
	 * @access protected
	 * @param string $path
	 * @return string
	 */
	protected function _getRuleMatchingPath($path) {
		foreach($this->_rules as $rule) {
			if(M_ControllerDispatcherPathRewriter::isPathMatchingRule($rule, $path)) {
				return $rule;
			}
		}
		return NULL;
	}
	
	/**
	 * Get One-Time-Password for a given rule
	 * 
	 * This method will return the One-Time-Password, which is the unique token
	 * to be added to each request, for a given rule.
	 * 
	 * @access protected
	 * @param string $rule
	 * @return string
	 */
	protected function _getOtp($rule) {
		// Local cache of OTP
		static $otpCache = array();
		
		// Preventing CSRF requires the inclusion of an unpredictable token in 
		// the body or URL of each HTTP request. Such tokens should at a minimum 
		// be unique per user session, but can also be unique per request for a 
		// stronger protection. We will generate a One-Time-Password as a token, 
		// unique for each of the protected rules and unique per page request.
		// If this OTP has not yet been generated earlier:
		if(! array_key_exists($rule, $otpCache)) {
			// For the rule that applies, we obtain the OTP Sequence number and 
			// seed. We need that in order to generate a One-Time-Password:
			list($sequence, $seed) = $this->_getOtpSequenceNumberAndSeed($rule);

			// With this information, we construct a One-Time-Password Generator.
			// We provide it with the OTP challenge while doing so:
			$otp = new M_OneTimePasswordGenerator(M_OneTimePasswordHelper::getOtpChallenge('sha1', $sequence, $seed));

			// Provide the Generator with the secret passphrase for the OTP. This
			// passphrase is generated with the rule as input:
			$otp->setPassPhrase($this->_getOtpPassPhrase($rule));
			
			// Render the One-Time-Password, and store in local cache:
			$otpCache[$rule] = $otp->getOneTimePasswordHexadecimal();
		}
		
		// Return the One-Time-Password:
		return $otpCache[$rule];
	}
	
	/**
	 * Get Previous One-Time-Password for a given rule
	 * 
	 * This method will return the previous One-Time-Password, which should have
	 * been used as the unique token for the current request, for a given rule.
	 * 
	 * @access protected
	 * @param string $rule
	 * @return string
	 */
	protected function _getOtpPrevious($rule) {
		// Get the session namespace:
		$ns = $this->_getSessionNamespace();
		
		// Get the sequence numbers stored:
		$otp = $ns->get('otp', array());
		
		// If no OTP is stored (yet) for the rule provided:
		if(! array_key_exists($rule, $otp)) {
			// We return NULL
			return NULL;
		}
		// If an OTP already exists:
		else {
			// Return the OTP
			return $otp[$rule];
		}
	}
	
	/**
	 * Get One-Time-Password's Secret PassPhrase, for a given rule
	 * 
	 * @see M_ControllerDispatcherCsrf::_getOtp()
	 * @access protected
	 * @param string $rule
	 * @return string
	 */
	protected function _getOtpPassPhrase($rule) {
		return sha1(strtr(strtolower($rule), array(
			'(' => '3',
			')' => '1',
			':' => 'D',
			'*' => '5',
			'/' => '9'
		)));
	}
	
	/**
	 * Get One-Time-Password's Sequence Number and Seed, for a given rule
	 * 
	 * @see M_ControllerDispatcherCsrf::_getOtp()
	 * @access protected
	 * @param string $rule
	 *		The rule for which to obtain the sequence number and seed
	 * @param bool $proceedToNextSequenceNumber
	 *		Set to TRUE if you want to proceed to the next sequence number in the
	 *		chain of unique One-Time-Passwords.
	 * @return array $numberAndSeed
	 *		An array with the sequence number at index 0, and the seed at index 1
	 */
	protected function _getOtpSequenceNumberAndSeed($rule, $proceedToNextSequenceNumber = FALSE) {
		// Get the session namespace:
		$ns = $this->_getSessionNamespace();
		
		// Get the sequence numbers stored:
		$otp = $ns->get('otpChallenge', array());
		
		// If no sequence number is stored (yet) for the rule provided:
		if(! array_key_exists($rule, $otp)) {
			// Then, start storing the number now. We set the sequence to a random
			// number somewhere in between 50 and 100:
			$otp[$rule] = $this->_getOtpNewSequenceNumberAndSeed();
		}
		
		// If we should proceed to the next sequence number:
		if($proceedToNextSequenceNumber) {
			// If we should move to the next sequence number, we will also store
			// the One-Time-Password as the previous OTP in the chain.
			$prev = $ns->get('otp', array());
			$prev[$rule] = $this->_getOtp($rule);
			$ns->otp = $prev;
			
			// If a unique token - a unique OTP - is to be generated for each
			// page request in the current session:
			if($this->_enabledUniqueTokenPerPageRequest) {
				// Decrement the sequence number by 1:
				$otp[$rule][0] -= 1;

				// The number must not go lower than 1! 
				// If that is the case though:
				if($otp[$rule][0] < 1) {
					// Then, we generate a new sequence number and seed:
					$otp[$rule] = $this->_getOtpNewSequenceNumberAndSeed();
				}
			}
		}
		
		// Store the components for the OTP Challenge in the session namespace
		$ns->otpChallenge = $otp;
		
		// Return the sequence number:
		return $otp[$rule];
	}
	
	/**
	 * Get new Sequence Number and Seed
	 * 
	 * Once the end is reached, and it is no longer possible to proceed to the 
	 * next sequence number, this method is called internally in order to obtain
	 * a new (random) sequence number and a new (random) seed.
	 * 
	 * @see M_ControllerDispatcherCsrf::_getOtpSequenceNumberAndSeed()
	 * @access protected
	 * @param string $rule
	 *		The rule for which to obtain the sequence number and seed
	 * @param bool $proceedToNextSequenceNumber
	 *		Set to TRUE if you want to proceed to the next sequence number in the
	 *		chain of unique One-Time-Passwords.
	 * @return array $numberAndSeed
	 *		An array with the sequence number at index 0, and the seed at index 1
	 */
	protected function _getOtpNewSequenceNumberAndSeed() {
		return array(
			0 => rand(50, 100),
			1 => M_OneTimePasswordHelper::getRandomSeed()
		);
	}
	
	/**
	 * Get Session Namespace
	 * 
	 * This method is used internally, in order to obtain the session namespace
	 * that is used to store persistent variables in the session.
	 * 
	 * @access protected
	 * @return M_SessionNamespace
	 */
	protected function _getSessionNamespace() {
		// If not requested before;
		if(! $this->_sessionNamespace) {
			// Then, construct now:
			$this->_sessionNamespace = new M_SessionNamespace('CSRF');
		}
		
		// Return the namespace:
		return $this->_sessionNamespace;
	}
}