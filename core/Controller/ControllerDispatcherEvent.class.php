<?php
/**
 * M_ControllerDispatcherEvent class
 * 
 * M_ControllerDispatcherEvent is used to construct events that are 
 * triggered by the {@link M_ControllerDispatcher} class.
 * 
 * @package Core
 */
class M_ControllerDispatcherEvent extends M_Event {

	/* -- CONSTANTS -- */

	/**
	 * Event Type
	 * 
	 * Triggered in {@link M_ControllerDispatcher}, when the controller
	 * dispatcher has received the host name.
	 *
	 * @var string
	 */
	const RECEIVE_HOST = 'received-host';

	/**
	 * Event Type
	 *
	 * Triggered in {@link M_ControllerDispatcher}, when the controller
	 * dispatcher has received the full (rewritten) path.
	 *
	 * @var string
	 */
	const RECEIVE_PATH = 'received-path';

	/**
	 * Event Type
	 *
	 * Triggered in {@link M_ControllerDispatcher}, when the controller
	 * dispatcher has constructed the controller for the request.
	 *
	 * @var string
	 */
	const CONSTRUCT_CONTROLLER = 'construct-controller';

	/* -- PROPERTIES -- */

	/**
	 * Controller
	 *
	 * This property stores the {@link MI_Controller} object that has been
	 * constructed by the Controller Dispatcher.
	 *
	 * @access private
	 * @var MI_Controller $controller
	 */
	private $_controller;

	/* -- GETTERS -- */

	/**
	 * Get controller dispatcher
	 *
	 * This method will provide with the {@link M_ControllerDispatcher} singleton
	 * that dispatched the event. In effect, this method is an alias for
	 * {@link M_Event::getTarget()}
	 *
	 * @uses M_Event::getTarget()
	 * @access public
	 * @return M_ControllerDispatcher
	 */
	public function getControllerDispatcher() {
		return $this->getTarget();
	}

	/**
	 * Get controller
	 *
	 * This method will provide with the {@link MI_Controller} object that has
	 * been constructed by the Controller Dispatcher.
	 *
	 * @access public
	 * @return MI_Controller
	 */
	public function getController() {
		return $this->_controller;
	}

	/* -- SETTERS -- */

	/**
	 * Set controller
	 *
	 * This method is typically used to set the {@link MI_Controller} object
	 * that has been constructed, by the Controller Dispatcher.
	 *
	 * @access public
	 * @param MI_Controller $controller
	 * @return M_ControllerDispatcherEvent $event
	 *		Returns itself, for a fluent programming interface
	 */
	public function setController(MI_Controller $controller) {
		$this->_controller = $controller;
		return $this;
	}
}