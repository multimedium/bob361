<?php
/**
 * MI_Controller Interface
 * 
 * A controller is a class that handles a set of specific page requests.
 * Typically, the controllers in the application implement this 
 * interface, by inheriting from the abstract {@link M_Controller}
 * class.
 * 
 * Note that the MI_Controller interface extends the {@link MI_EventDispatcher}
 * interface. This way, event listeners can be attached to a controller, in order
 * to respond to significant events.
 * 
 * Typically, the controllers in the app implement the {@link MI_EventDispatcher}
 * interface, by inheriting from the abstract {@link M_Controller} class, which
 * in turn inherits from {@link M_EventDispatcher}.
 * 
 * @package Core
 */
interface MI_Controller extends MI_EventDispatcher {
	/**
	 * Magic routing
	 * 
	 * {@link M_ControllerDispatcher} will dispatch a given request to a controller.
	 * It will also check the routing rules that are defined by the specific 
	 * controller's __router() method.
	 * 
	 * The return value of this method is expected in the same way as expected
	 * by {@link M_ControllerDispatcher::setRoutingRules()}.
	 * 
	 * @access public
	 * @return array $rules
	 * 		Returns an array with routing/rewriting rules
	 */
	public function __router();
	
	/**
	 * Handle error
	 * 
	 * {@link M_ControllerDispatcher} will run the controller inside
	 * a try{} block. Any exception that is thrown during the processing
	 * of the page request, is catched and passed into the controller's
	 * handleError() method.
	 * 
	 * @access public
	 * @param Exception $exception
	 * 		The exception that has been thrown, due to an error in the 
	 * 		page request.
	 * @return void
	 */
	public function handleError(Exception $exception);
}