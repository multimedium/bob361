<?php
/**
 * M_FilterTextPunctuation class
 *
 * This specific Text Filter can be used to remove all punctuation marks 
 * from the subject. Typically, this filter is used to prepare a text
 * for search indexing, where punctuation marks should not be included
 * in the text.
 * 
 * NOTE:
 * M_FilterTextAccents, a subclass of {@link M_FilterTextDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterTextPunctuation extends M_FilterTextDecorator {
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * @access public
	 * @return string
	 */
	public function apply() {
		// Trim the subject
		// (not only white-space characters, but also punctuation marks)
		$subject = trim($this->_decoratedFilter->apply(), " .!?,;:\"')\t\n\r\0\x0B");
		// Remove the typical 's abbreviation
		// eg. "Let's go!" => "Let go!"
		// eg. "Tom's car" => "Tom car"
		$subject = preg_replace('/([\S]+)\'s+/m', '\1', $subject);
		// Remove other punctuation marks in the text.
		$subject = preg_replace('/[\s\.\?\':;,!"\'\)\(\[\]\/\-]+/m', ' ', $subject);
		// Remove the mutated text
		return $subject;
	}
}
?>