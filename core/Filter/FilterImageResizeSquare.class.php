<?php
/**
 * M_FilterImageResizeSquare class
 *
 * This specific Image Filter is similar to {@link M_FilterImageResize},
 * as it can be used to resize an image subject. Note however that
 * M_FilterImageResizeSquare will make sure that the resulting image
 * resource is a square image (width = height).
 * 
 * NOTE:
 * M_FilterImageResizeSquare, a subclass of {@link M_FilterImageDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterImageResizeSquare extends M_FilterImageResize {
	/**
	 * Size
	 * 
	 * This property stores the size (width and height) to which the 
	 * image should be resized.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_size;

	/**
	 * Constructor
	 * 
	 * @access public
	 * @param MI_Filter $decoratedFilter
	 * 		The decorated object
	 * @param integer $size
	 * 		The size (width + height) to which the image should be resized.
	 * @param string|int $x
	 *		Where should we start to crop
	 * @return M_FilterImageResize
	 */
	public function __construct(MI_Filter $decoratedFilter, $size, $cropX = self::POSITION_CENTER, $cropY = self::POSITION_TOP) {
		$this->_decoratedFilter = $decoratedFilter;

		//in square, height and width are equal
		$this->_size = (int) $size;
		$this->setWidth($this->_size);
		$this->setHeight($this->_size);

		$this->setCropX($cropX);
		$this->setCropY($cropY);
	}
	
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * Note that image filters are expected to return an image resource.
	 * An image resource should implement the {@link M_ImageResource}
	 * interface.
	 * 
	 * @access public
	 * @return M_ImageResource
	 */
	public function apply() {
		$rs = $this->_decoratedFilter->apply();
		$h = $rs->getHeight();
		$w = $rs->getWidth();
		if($w > $h) {
			$size = M_ImageRatio::getForcedToHeight($w, $h, $this->_size);
			$rs->resize($size[0], $size[1]);
			$rs->crop(
				$this->_getCropXPosition($size[0], $this->getCropX()),
				0,
				$this->_size,
				$this->_size
			);
		} else {
			$size = M_ImageRatio::getForcedToWidth($w, $h, $this->_size);			
			$rs->resize($size[0], $size[1]);
			$rs->crop(
				0,
				$this->_getCropYPosition($size[1], $this->getCropY()),
				$this->_size,
				$this->_size
			);
		}
		return $rs;
	}

	/* -- Additional to MI_Filter interface -- */
	
	/**
	 * Get size
	 * 
	 * This method will return the size (width and height) to which the 
	 * image is being resized.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getSize() {
		return $this->_size;
	}
	
	/**
	 * Set size
	 * 
	 * This method can be used to set the size to which the image
	 * should be resized.
	 * 
	 * @access public
	 * @param integer $size
	 * 		The size to resize to (expressed in pixels)
	 * @return void
	 */
	public function setSize($size) {
		$this->_size = (int) $size;
	}
}
?>