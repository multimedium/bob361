<?php
/**
 * M_FilterImageDecorator class
 * 
 * Each filter in the core needs to implement the {@link MI_Filter}
 * interface. Typically, image filter classes will do so by extending 
 * the abstract M_FilterImageDecorator class.
 * 
 * For more information on filters, read the docs on:
 * 
 * - {@link MI_Filter}
 * - {@link MI_Filter::apply()}
 * 
 * @package Core
 */
abstract class M_FilterImageDecorator implements MI_Filter {

	/**
	 * Position: top (used to position overlay or cropping)
	 *
	 * @var const POSITION_TOP
	 */
	const POSITION_TOP = 'top';

	/**
	 * Position: bottom (used to position overlay or cropping)
	 *
	 * @var const POSITION_BOTTOM
	 */
	const POSITION_BOTTOM = 'bottom';

	/**
	 * Position: center (used to position overlay or cropping)
	 *
	 * @var const POSITION_CENTER
	 */
	const POSITION_CENTER = 'center';

	/**
	 * Position: left (used to position overlay or cropping)
	 *
	 * @var const POSITION_LEFT
	 */
	const POSITION_LEFT = 'left';

	/**
	 * Position: right (used to position overlay or cropping)
	 *
	 * @var const POSITION_RIGHT
	 */
	const POSITION_RIGHT = 'right';

	/**
	 * Decorated object
	 * 
	 * This property stores the decorated object (which in turn can be
	 * a decorator object on a decorated object).
	 *
	 * @access protected
	 * @var MI_Filter
	 */
	protected $_decoratedFilter;
	
	/**
	 * Constructor
	 * 
	 * Note that the constructor expects a decorated object to be 
	 * provided. Note that the decorated object also implements the 
	 * {@link MI_Filter} interface, and can therefore in turn be a 
	 * decorator on a decorated subject.
	 *
	 * @access public
	 * @param MI_Filter $decoratedFilter
	 * 		The decorated object.
	 * @return M_FilterImageDecorator
	 */
	function __construct(MI_Filter $decoratedFilter) {
		$this->_decoratedFilter = $decoratedFilter;
	}
}
?>