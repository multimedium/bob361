<?php
/**
 * Create a pixelated effect in an image
 *
 * @author Ben Brughmans
 */
class M_FilterImagePixelate extends M_FilterImageDecorator {

	/**
	 * The blocksize in pixels
	 *
	 * @var int
	 */
	private $_blocksize;

	/**
	 * Advanced pixelation
	 * 
	 * @var bool
	 */
	private $_advanced;
	
	/**
	 * Apply a pixelated effect
	 *
	 * @param MI_Filter $decoratedFilter
	 * 		The decorated object
	 * @param int $blocksize The size of the blocks in pixels
	 * @param bool $advanced Advanced pixelation
	 * @return M_FilterImageDecorator
	 */
	public function __construct(MI_Filter $decoratedFilter, $blocksize = 10, $advanced = false) {
		parent::__construct($decoratedFilter);
		$this->setBlocksize((int)$blocksize);
		$this->setAdvanced((bool)$advanced);
		return $this;
	}

	/**
	 * The size of the blocks in pixels
	 * 
	 * @param int $size
	 * @return M_FilterImagePixelate
	 */
	public function setBlocksize($size) {
		$this->_blocksize = (int)$size;
		return $this;
	}

	/**
	 * Advanced pixelation
	 *
	 * @param bool $advanced
	 * @return M_FilterImagePixelate
	 */
	public function setAdvanced($advanced) {
		$this->_advanced = (bool)$advanced;
		return $this;
	}

	/**
	 * Apply the filter
	 *
	 * This method is a required implementation in any filter class.
	 * For more information, read the documentation on:
	 *
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 *
	 * Note that image filters are expected to return an image resource.
	 * An image resource should implement the {@link M_ImageResource}
	 * interface.
	 *
	 * @access public
	 * @return M_ImageResourceGd
	 */
	public function apply() {
		$rs = $this->_decoratedFilter->apply();
		
		return $rs->pixelate($this->_blocksize, $this->_advanced);
	}
}