<?php
/**
 * M_FilterTextHtmlAttribue class
 *
 * This specific Text Filter can be used to safely use a string as
 * HTML-attribute.
 *
 * A filter is made available for this purpose, because the
 * results of PHP's built-in htmlentities() are not always satisfactory.
 * 
 * NOTE:
 * M_FilterTextHtmlAttribute, a subclass of {@link M_FilterTextDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterTextHtmlAttribute extends M_FilterTextDecorator {
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * @access public
	 * @return string
	 */
	public function apply() {
        // Remove quotes and other special characters that do not belong in HTML
		// attribute value:

		// - &
        $string = str_replace("&", '&amp;', $this->_decoratedFilter->apply());
        $string = str_replace("&amp;amp;", '&amp;', $string);

		// - Quotes
        $string = str_replace('"', '&quot;',  $string);
		$string = str_replace("'", '&rsquo;', $string);

		// - EURO Sign
		$string = str_replace(chr(226).chr(130).chr(172), '&euro;', $string);

		// - New lines
        $string = str_replace("\r", '', $string);
        return str_replace("\n", '&lb;',   $string);
	}
}