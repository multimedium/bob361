<?php
/**
 * M_FilterImageResizeForce class
 *
 * This specific Image Filter can be used to resize the image subject to a 
 * forced width and height. This way an image will always have the given width
 * and height. Opposed to {@link M_FilterImageResize} which will resize untill
 * both dimensions are met, we now resize untill one dimension meets the 
 * requirements. In addition we crop the image so it meets both requirements.
 * 
 * Note that this class will use {@link M_ImageRatio} to maintain the
 * original image's aspect ratio (if requested).
 * 
 * NOTE:
 * M_FilterImageResizeForced, a subclass of {@link M_FilterImageDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterImageResizeForced extends M_FilterImageResize {
	
	/**
	 * Flag: maintain aspect ratio
	 * 
	 * This property stores a boolean flag that indicates whether or
	 * not the image's aspect ratio should be maintained when resizing.
	 * 
	 * When forcing an image to be resized to a width and height, we always want
	 * the image ratio to be respected.
	 * 
	 * @access private
	 * @var boolean
	 */
	protected $_keepAspectRatio = true;
	
	/**
	 * Flag: autogrow
	 * 
	 * This property stores a boolean flag that indicates whether or
	 * not the image is allowed to grow when the required dimensions are bigger
	 * as the ones of the image itself. As a result this filter wouldn't do
	 * anything, but when we allow the image to grow: the result will meet
	 * the users requirements
	 * 
	 * @access private
	 * @var boolean
	 */
	private $_autoGrow;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param MI_Filter $decoratedFilter
	 * 		The decorated object
	 * @param integer $width
	 * 		The width to which the image should be resized. Note that, 
	 * 		if the original image's aspect ratio is to be maintained, 
	 * 		this width will be interpreted as the maximum width.
	 * @param integer $height
	 * 		The height to which the image should be resized. Note that, 
	 * 		if the original image's aspect ratio is to be maintained, 
	 * 		this height will be interpreted as the maximum height.
	 * @param boolean $autoGrow
	 * 		Set to TRUE (default) to let the image grow
	 * @param int|string
	 *		The x-position where we start to crop when the resized image exceeds
	 *		the defined width
	 * @param int|string
	 *		The y-position where we start to crop when the resized image exceeds
	 *		the defined height
	 *
	 * @return M_FilterImageResize
	 */
	function __construct(MI_Filter $decoratedFilter, $width, $height, $autoGrow = true,  $cropX = self::POSITION_CENTER, $cropY = self::POSITION_TOP) {
		$this->_decoratedFilter = $decoratedFilter;
		$this->_width  = (int) $width;
		$this->_height = (int) $height;
		$this->_autoGrow = (bool) $autoGrow;
		$this->setCropX($cropX);
		$this->setCropY($cropY);
	}
	
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * Note that image filters are expected to return an image resource.
	 * An image resource should implement the {@link M_ImageResource}
	 * interface.
	 * 
	 * @access public
	 * @return M_ImageResourceGd
	 */
	public function apply() {
		/* @var $rs M_ImageResourceGd */
		$rs = $this->_decoratedFilter->apply();
		$size = M_ImageRatio::getMinSize(
			$rs->getWidth(), 
			$rs->getHeight(), 
			$this->_width, 
			$this->_height,
			$this->_autoGrow
		);
		
		//resize to a minimum width and height
		$rs->resize($size[0], $size[1]);

		//crop the image if it's bigger as requested and center the image
		//horizontally or vertically
		if($rs->getWidth() > $this->_width) {
			//start to crop on?
			$rs->crop(
				$this->_getCropXPosition($rs->getWidth()),
				0,
				$this->_width,
				$this->_height
			);
		} elseif($rs->getHeight() > $this->_height) {
			$rs->crop(
				0,
				$this->_getCropYPosition($rs->getHeight()),
				$this->_width,
				$this->_height
			);
		}
		return $rs;
	}
}