<?php
/**
 * M_FilterTextDecorator class
 * 
 * Each filter in the core needs to implement the {@link MI_Filter}
 * interface. Typically, text filter classes will do so by extending 
 * the abstract M_FilterTextDecorator class.
 * 
 * For more information on filters, read the docs on:
 * 
 * - {@link MI_Filter}
 * - {@link MI_Filter::apply()}
 * 
 * @package Core
 */
abstract class M_FilterTextDecorator implements MI_Filter {
	/**
	 * Decorated object
	 * 
	 * This property stores the decorated object (which in turn can be
	 * a decorator object on a decorated object).
	 *
	 * @access protected
	 * @var MI_Filter
	 */
	protected $_decoratedFilter;
	
	/**
	 * Constructor
	 * 
	 * Note that the constructor expects a decorated object to be 
	 * provided. Note that the decorated object also implements the 
	 * {@link MI_Filter} interface, and can therefore in turn be a 
	 * decorator on a decorated subject.
	 *
	 * @access public
	 * @param MI_Filter $decoratedFilter
	 * 		The decorated object.
	 * @return M_FilterTextDecorator
	 */
	function __construct(MI_Filter $decoratedFilter) {
		$this->_decoratedFilter = $decoratedFilter;
	}
}
?>