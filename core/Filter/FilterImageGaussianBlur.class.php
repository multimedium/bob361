<?php
class M_FilterImageGaussianBlur extends M_FilterImageDecorator {

	public function apply() {
		return $this->_decoratedFilter->apply()->gaussianBlur();
	}
}