<?php
/**
 * M_DbColumn class
 *
 * The M_DbColumn class allows you to create or manipulate database
 * table columns. Typically, this class is used to make changes in the
 * database (create table, add columns, ...). Consider the following
 * examples:
 * 
 * Example 1, create new table "news"
 * <code>
 *    // Define the "id" column:
 *    $id = new M_DbColumn();
 *    $id->setName('id');
 *    $id->setType(M_DbColumn::TYPE_INTEGER);
 *    
 *    // Define the "category" column:
 *    $category = new M_DbColumn();
 *    $category->setName('category');
 *    $category->setType(M_DbColumn::TYPE_INTEGER);
 *    
 *    // Define the "name" column:
 *    $name = new M_DbColumn();
 *    $name->setName('name');
 *    $name->setType(M_DbColumn::TYPE_VARCHAR);
 *    $name->setLength(255);
 *    
 *    // Construct a new table
 *    $db = M_Db::getInstance();
 *    $table = $db->getNewTable();
 *    
 *    // Populate the table with properties, and create the table:
 *    $table->setName('news');
 *    $table->setPrimaryKey($id);
 *    $table->setIndex('category', $category);
 *    $table->addColumn($id);
 *    $table->addColumn($category);
 *    $table->addColumn($name);
 *    $table->create();
 * </code>
 * 
 * @package Core
 */
class M_DbColumn implements MI_DbColumn {
	/**
	 * Type Constant
	 *
	 * This specific constant is used to address the data type VARCHAR.
	 * Typically, this constant is used to set the column's data type.
	 * For more info, read {@link M_DbColumn::setType()}.
	 */
	const TYPE_VARCHAR = 'varchar';
	
	/**
	 * Type Constant
	 *
	 * This specific constant is used to address the data type TEXT.
	 * Typically, this constant is used to set the column's data type.
	 * For more info, read {@link M_DbColumn::setType()}.
	 */
	const TYPE_TEXT = 'text';
	
	/**
	 * Type Constant
	 *
	 * This specific constant is used to address the data type BLOB.
	 * Typically, this constant is used to set the column's data type.
	 * For more info, read {@link M_DbColumn::setType()}.
	 */
	const TYPE_BLOB = 'blob';
	
	/**
	 * Type Constant
	 *
	 * This specific constant is used to address the data type INTEGER.
	 * Typically, this constant is used to set the column's data type.
	 * For more info, read {@link M_DbColumn::setType()}.
	 */
	const TYPE_INTEGER = 'int';
	
	/**
	 * Type Constant
	 *
	 * This specific constant is used to address the data type TINY_INTEGER.
	 * Typically, this constant is used to set the column's data type.
	 * For more info, read {@link M_DbColumn::setType()}.
	 */
	const TYPE_TINY_INTEGER = 'tinyint';
	
	/**
	 * Type Constant
	 *
	 * This specific constant is used to address the data type FLOAT.
	 * Typically, this constant is used to set the column's data type.
	 * For more info, read {@link M_DbColumn::setType()}.
	 */
	const TYPE_FLOAT = 'float';
	
	/**
	 * Type Constraint
	 *
	 * This specific constant is used to address the data type DATE.
	 * Typically, this constant is used to set the column's data type.
	 * For more info, read {@link M_DbColumn::setType()}.
	 */
	const TYPE_DATE = 'date';
	
	/**
	 * Type Constraint
	 *
	 * This specific constant is used to address the data type DATETIME.
	 * Typically, this constant is used to set the column's data type.
	 * For more info, read {@link M_DbColumn::setType()}.
	 */
	const TYPE_DATETIME = 'datetime';
	
	/**
	 * Charset Constraint
	 * 
	 * This specific constract is used to address the charset type for a textual
	 * column to utf8
	 * For more info, read {@link M_DbColumn::setCharset()}.
	 */
	const CHARSET_UTF8 = 'utf8';
	
	/**
	 * Collate Constraint
	 * 
	 * This specific constraint is used to adress the collate type to 
	 * utf8_general_ci for a textual column
	 * 
	 * For more info, read {@link M_DbColumn::setCollate()}.
	 */
	const COLLATE_UTF8_GENERAL_CI = 'utf8_general_ci';
	
	/**
	 * Name
	 * 
	 * This property stores the name of the database table column.
	 *
	 * @access private
	 * @var string
	 */
	private $_name;
	
	/**
	 * Name
	 * 
	 * This property stores the data type of the column. For more info,
	 * read the docs on:
	 * 
	 * - {@link M_DbColumn::setType()}
	 * - {@link M_DbColumn::TYPE_VARCHAR}
	 * - {@link M_DbColumn::TYPE_TEXT}
	 * - {@link M_DbColumn::TYPE_BLOB}
	 * - {@link M_DbColumn::TYPE_INTEGER}
	 * - {@link M_DbColumn::TYPE_TINY_INTEGER}
	 * - {@link M_DbColumn::TYPE_FLOAT}
	 * - {@link M_DbColumn::TYPE_DATETIME}
	 * - {@link M_DbColumn::TYPE_DATE}
	 *
	 * @access private
	 * @var string
	 */
	private $_type;
	
	/**
	 * Length
	 * 
	 * This property stores the size/length of the database table column.
	 *
	 * @access private
	 * @var string
	 */
	private $_length;
	
	/**
	 * Collate
	 * 
	 * This property stores the collate of the database table column
	 *
	 * Note: by default we use utf8_general_ci
	 * 
	 * @access private
	 * @var str
	 */
	private $_collate = self::COLLATE_UTF8_GENERAL_CI;
	
	/**
	 * Charset
	 * 
	 * This property stores the charset of the database table column
	 *
	 * Note: by default we use utf8
	 * 
	 * @access private
	 * @var str
	 */
	private $_charset = self::CHARSET_UTF8;
	
	/**
	 * Comments
	 * 
	 * This property stores the columns comments
	 *
	 * @var str
	 */
	private $_comments;
	
	/**
	 * Default value
	 * 
	 * This property stores the default value of the database table 
	 * column.
	 *
	 * @access private
	 * @var string
	 */
	private $_defaultValue;
	
	/**
	 * Is Auto-Increment?
	 * 
	 * This property stores the (boolean) flag that tells whether or not
	 * the database table column has the option AUTO-INCREMENT enabled.
	 *
	 * @access private
	 * @var boolean
	 */
	private $_isAutoIncrement = FALSE;
	
	/**
	 * Is Auto-Increment?
	 * 
	 * This property stores the (boolean) flag that tells whether or not
	 * the database table column has the option NOT-NULL enabled.
	 *
	 * @access private
	 * @var boolean
	 */
	private $_isNotNull = TRUE;
	
	/**
	 * Is unsigned?
	 * 
	 * This property stores the (boolean) flag that tells whether or
	 * not the database table column has the option UNSIGNED enabled.
	 * 
	 * @access private
	 * @var boolean
	 */
	private $_isUnsigned = FALSE;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the database table column
	 * @return M_DbColumn
	 */
	public function __construct($name = NULL) {
		if($name) {
			$this->setName($name);
		}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get name
	 * 
	 * @access public
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}
	
	/**
	 * Get type
	 * 
	 * @access public
	 * @return string
	 */
	public function getType() {
		return $this->_type;
	}
	
	/**
	 * Get all supported text-types
	 *
	 * @internal for now we only support text, maybe sometime we
	 * could also support TYPE_MEDIUMTEXT, ...
	 * @return array
	 */
	public static function getTextTypes() {
		return array(
			self::TYPE_TEXT,
			self::TYPE_VARCHAR
		);
	}
	
	/**
	 * Get all supported numeric-types
	 *
	 * @return array
	 */
	public static function getNumericTypes() {
		return array(
			self::TYPE_FLOAT,
			self::TYPE_INTEGER,
			self::TYPE_TINY_INTEGER
		);
	}
	
	/**
	 * Get length
	 * 
	 * @access public
	 * @return integer
	 */
	public function getLength() {
		return $this->_length;
	}
	
	/**
	 * Get collate
	 * 
	 * @access public
	 * @return string
	 */
	public function getCollate() {
		return $this->_collate;
	}
	
	/**
	 * Get charset
	 * 
	 * @access public
	 * @return string
	 */
	public function getCharset() {
		return $this->_charset;
	}
	
	/**
	 * Get comments
	 * 
	 * @access public
	 * @return string
	 */
	public function getComments() {
		return $this->_comments;
	}
	
	/**
	 * Get default value
	 * 
	 * @access public
	 * @return string
	 */
	public function getDefaultValue() {
		return $this->_defaultValue;
	}
	
	/**
	 * Get flag: "Is Auto-Increment?"
	 * 
	 * This method will tell whether or not the option AUTO-INCREMENT
	 * has been enabled for the database table column. The return value
	 * is TRUE if the option has been enabled, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isAutoIncrement() {
		return $this->_isAutoIncrement;
	}
	
	/**
	 * Get flag: "Is Not-Null?"
	 * 
	 * This method will tell whether or not the option NOT-NULL has 
	 * been enabled for the database table column. The return value
	 * is TRUE if the option has been enabled, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isNotNull() {
		return $this->_isNotNull;
	}
	
	/**
	 * Get flag: "Is unsigned?"
	 * 
	 * This method will tell whether or not the option UNSIGNED has been
	 * enabled for the database table column. The return value is TRUE
	 * if the option has been enabled, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isUnsigned() {
		return $this->_isUnsigned;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set name
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the database table column
	 * @return void
	 */
	public function setName($name) {
		$this->_name = $name;
	}
	
	/**
	 * Set type
	 * 
	 * NOTE:
	 * An exception is thrown, if the given data type could not have
	 * been recognized.
	 *
	 * @throws M_DbException
	 * @access public
	 * @param string $type
	 * 		The data type of the database table column
	 * @return void
	 */
	public function setType($type) {
		switch($type) {
			case self::TYPE_INTEGER:
			case self::TYPE_TINY_INTEGER:
			case self::TYPE_FLOAT:
			case self::TYPE_BLOB:
			case self::TYPE_TEXT:
			case self::TYPE_VARCHAR:
			case self::TYPE_DATE:
			case self::TYPE_DATETIME:
				$this->_type = $type;
				break;
			
			default:
				throw new M_DbException(sprintf('%s: %s: Unrecognized data type "%s"', __CLASS__, __METHOD__, $type));
				break;
		}
	}
	
	/**
	 * Set size/length
	 *
	 * @access public
	 * @param integer $length
	 * 		The size/length of the database table column
	 * @return void
	 */
	public function setLength($length) {
		$length = (int) $length;
		switch($this->_type) {
			case self::TYPE_INTEGER:
			case self::TYPE_TINY_INTEGER:
				$this->_length = $length;
				break;
			
			case self::TYPE_VARCHAR:
				$this->_length = $length > 255 ? 255 : $length;
				break;
			
			// No size/length required:
			case self::TYPE_BLOB:
			case self::TYPE_TEXT:
			case self::TYPE_FLOAT:
			case self::TYPE_DATE:
			case self::TYPE_DATETIME:
			    break;
			    
			// Unknown type or type not set yet
			default:
				throw new M_DbException(
				    sprintf('Undefined or unkown type %s, cannot set length',$this->_type)
				);
			    break;
		}
	}
	
	/**
	 * Set the collation for this column
	 *
	 * @access public
	 * @param str $collate
	 * @return void
	 */
	public function setCollate($collate) {
	    switch ($this->_type) {
	        case self::TYPE_VARCHAR:
	        case self::TYPE_TEXT:
        	    $this->_collate = $collate;
	            break;
	        default:
	            throw new M_DbException(
	                sprintf('Cannot specify collate for type %s or type undefined',$this->_type)
	            );
	            break;
	    }
	}
	
	/**
	 * Set the charset for this column
	 *
	 * @access public
	 * @param str $charset
	 * @return void
	 */
	public function setCharset($charset) {
	    switch ($this->_type) {
	        case self::TYPE_VARCHAR:
	        case self::TYPE_TEXT:
        	    $this->_charset = $charset;
	            break;
	        default:
	            throw new M_DbException(
	                sprintf('Cannot specify charset for type %s or type undefined',$this->_type)
	            );
	            break;
	    }
	}
	
	/**
	 * Set the comments for this column
	 *
	 * @param str $comments
	 */
	public function setComments($comments) {
	    $this->_comments = (string) $comments;
	}
	
	/**
	 * Set default value
	 *
	 * @access public
	 * @param string $value
	 * 		The default value of the database table column
	 * @return void
	 */
	public function setDefaultValue($value) {
		switch($this->_type) {
			// Cast to floating number:
			case self::TYPE_FLOAT:
				$this->_defaultValue = (float) $value;
				break;
			
			// Cast to integer:
			case self::TYPE_INTEGER:
			case self::TYPE_TINY_INTEGER:
				$this->_defaultValue = (int) $value;
				break;
			
			default:
				$this->_defaultValue = $value;
				break;
		}
	}
	
	/**
	 * Set flag: "Is Auto-Increment?"
	 * 
	 * For more info, read {@link M_DbColumn::isAutoIncrement()}.
	 *
	 * @access public
	 * @param boolean $flag
	 * 		Set to TRUE to enable the option, FALSE to disable.
	 * @return void
	 */
	public function setIsAutoIncrement($flag) {
		$this->_isAutoIncrement = (bool) $flag;
	}
	
	/**
	 * Set flag: "Is Not-Null?"
	 * 
	 * For more info, read {@link M_DbColumn::isNotNull()}.
	 *
	 * @access public
	 * @param boolean $flag
	 * 		Set to TRUE to enable the option, FALSE to disable.
	 * @return void
	 */
	public function setIsNotNull($flag) {
		$this->_isNotNull = (bool) $flag;
	}
	
	/**
	 * Set flag: "Is Null?"
	 * 
	 * For more info, read {@link M_DbColumn::isNotNull()}.
	 *
	 * @access public
	 * @param boolean $flag
	 * 		Set to TRUE to enable the option, FALSE to disable.
	 * @return void
	 */
	public function setIsNull($flag) {
		$this->_isNotNull = (bool) $flag ? false : true;
	}
	
	/**
	 * Set flag: "Is unsigned?"
	 * 
	 * For more info, read {@link M_DbColumn::isUnsigned()}.
	 *
	 * @access public
	 * @param boolean $flag
	 * 		Set to TRUE to enable the option, FALSE to disable.
	 * @return void
	 */
	public function setIsUnsigned($flag) {
		$this->_isUnsigned = (bool) $flag;
	}
}
?>