<?php
// Database-level driver interface
interface MI_Db {
	// Establish connection
	public function connect($host, $user, $pass, $dbname);
	public function close();
	
	// Get connection property
	public function getConnectionProperty($property);
	
	// Transactions:
	public function beginTransaction();
	public function commit();
	public function rollback();
	public function getTransactionCount();
	public function beginSmartTransaction();
	public function completeSmartTransaction();
	public function failSmartTransaction();
	public function hasFailedSmartTransaction();
	
	// Queries:
	public function setFetchMode($mode);
	public function getFetchMode();
	public function select($from = NULL);
	public function query($select);
	public function queryRange($select, $offset, $numberOfRows);
	public function queryOp($sql);
	
	// insert() in most cases will need a unique ID. In some
	// database brands, this problem is easily solved by using
	// the feature "auto_increment". However, this feature is
	// not supported by a lot of database brands. We should use
	// sequences instead!
	// ADOConnection::GenID()
	public function getId($sequence = 'dbseq', $startAt = 1);
	
	// Info about queries
	public function getLastInsertId();
	public function getAffectedRows();
	public function getError();
	
	// Query parsing
	public static function quote($value);
	public static function quoteString($value);
	public static function quoteIn($sql, $args);
	public static function quoteIdentifier($name);
	
	// Data Dictionary
	public function getTables();
	
	/**
	 * Get Table
	 * 
	 * @see M_DbDriverAdo::getTable()
	 * @access public
	 * @param string $name
	 * 		The name of the requested table
	 * @return MI_DbTable
	 */
	public function getTable($name);
	
	/**
	 * Get new table
	 * 
	 * @see M_DbDriverAdo::getNewTable()
	 * @access public
	 * @param string $name
	 * 		The name of the requested table
	 * @return MI_DbTable
	 */
	public function getNewTable();
	public function hasTable($name);
	public function getIndexes($table, $primary = FALSE);
	public function createTable(MI_DbTable $table);
	public function alterTable(MI_DbTable $table);
	public function dropTable(MI_DbTable $table);

	/**
	 * Is cache enabled or not?
	 *
	 * @return bool
	 */
	public function getCacheMode();

	/**
	 * Enable caching of recordsets
	 *
	 * Note: lifetime is set default to 3600 seconds
	 *
	 * @param bool $mode TRUE is on, FALSE is off
	 * @param int $lifetime number of seconds the cache will live
	 * @return M_DbDriver
	 */
	public function setCacheMode($mode, $lifetime = NULL);

	/**
	 * The number of seconds cache of a recordset will live
	 *
	 * @return int
	 */
	public function getCacheLifetime();

	/**
	 * Tell the driver how low the cache needs to live
	 *
	 * @param int $arg
	 * @return M_DbDriver
	 */
	public function setCacheLifetime($arg);

	/**
	 * Set the directory to which the cache files should be saved
	 *
	 * @param M_Directory $directory
	 * @return M_DbDriverAdo
	 */
	public function setCacheDirectory(M_Directory $arg);

	/**
	 * Remove all cahced recordsets
	 *
	 * @return void
	 */
	public function flushCache();
}