<?php
// Support for Countable and Iterator to run through the columns in the table
// NOTE NOTE
// use db::quoteIdentifier to quote table names and field names, to avoid
// conflicts with reserved SQL words.
interface MI_DbTable {
	// 
	public function __construct(MI_Db $db, $name);
	
	// Get name
	public function getName();
	public function setName($name);
	
	// Table info
	public function getComments();
	public function setComments($comments);
	public function getEngine();
	public function setEngine($engine);
	public function getCollate();
	public function setCollate($collate);
	public function getCharset();
	public function setCharset($charset);
	
	// Queries:
	public function getTotalNumberOfRecords();
	public function select();
	public function truncate();
	public function insert(array $data);
	public function insertMultiple(array $data);
	public function update(array $data, $conditions);
	public function delete($conditions);
	
	// Data Dictionary
	public function getColumns();
	public function getColumn($name);
	
	/**
	 * Get primary key
	 * 
	 * @access public
	 * @return MI_DbIndex|boolean
	 */
	public function getPrimaryKey();
	public function getIndexes();
	public function getIndex($name);
	public function hasColumn($name);
	public function addColumn(MI_DbColumn $column);
	public function setPrimaryKey(MI_DbIndex $index);
	public function addIndex(MI_DbIndex $index);
	public function create();
	public function alter();
	public function drop();
}
?>