<?php
/**
 * M_DbQuery class
 * 
 * @package Core
 * @access abstract
 * @todo Sometimes it could be usefull to specify the AS (alias) of a table.
 * at this time it isn't possible to create an inner join to the same table in 
 * the FROM statement
 */
abstract class M_DbQuery extends M_Object implements MI_DbQuery {
	
	/**
	 * Operator onstants
	 */
	const OPERATOR_AND = 'AND';
	const OPERATOR_OR = 'OR';
	
	/**
	 * The database
	 * 
	 * This property stores a reference to the database connection
	 * from which the select has been requested, and in which the
	 * select will be executed.
	 * 
	 * NOTE:
	 * M_DbQuery also uses the database, in order to quote values
	 * and identifiers in the SQL syntax.
	 * 
	 * @access private
	 * @var MI_Db
	 */
	protected $_db;
	
	/**
	 * WHERE
	 * 
	 * This property stores the WHERE expression of the SELECT
	 * statement.
	 * 
	 * @see M_DbQuery::where()
	 * @see M_DbQuery::whereIn()
	 * @see M_DbQuery::whereContains()
	 * @see M_DbQuery::orWhere()
	 * @see M_DbQuery::getWhere()
	 * @access private
	 * @var string
	 */
	protected $_where = '';
	
	/**
	 * ORDER BY
	 * 
	 * This property stores the ORDER BY expression of the SELECT
	 * statement.
	 * 
	 * @see M_DbQuery::order()
	 * @see M_DbQuery::getOrder()
	 * @access private
	 * @var string
	 */
	protected $_order = '';
	
	/**
	 * LIMIT (range)
	 * 
	 * This property stores the range of results that is to be fetched
	 * by the SELECT statement.
	 * 
	 * @see M_DbQuery::limit()
	 * @see M_DbQuery::getLimitOffset()
	 * @see M_DbQuery::getLimitCount()
	 * @see M_Db::queryRange()
	 * @access private
	 * @var string
	 */
	protected $_limit = FALSE;
	
	/**
	 * Counts WHERE blocks which are still open
	 * 
	 * Everytime {@link M_DbQuery::openWhereBlock} is called, this counter will 
	 * be incremented with 1, meaning there is still 1 where block which has 
	 * been opened but has not yet closed.
	 * When one decides to close the where block, this counter will be 
	 * decremented with 1, which will result in the counter being set at 0
	 * Finally this value is checked before executing the query, if it's not
	 * 0 an exception will be thrown
	 * 
	 * @see M_DbQuery::openWhereBlock()
	 * @see M_DbQuery::closeWhereBlock()
	 * @see M_DbQuery::toString()
	 */
	protected $_whereBlockCount = 0;
	
	/**
	 * Constructor
	 * 
	 * NOTE:
	 * You should not use this method directly. Typically, you request
	 * a select object from another object. You can request selects
	 * with the following methods:
	 * 
	 * - {@link MI_Db::select()}
	 * - {@link MI_DbTable::select()}
	 * 
	 * @access public
	 * @param MI_Db $db
	 * 		The database connection in which the select statement
	 * 		should be executed.
	 * @return M_DbQuery
	 */
	public function __construct(MI_Db $db) {
		$this->_db = $db;
	}
	
	/**
	 * Open WHERE block
	 * 
	 * It's possible to open a WHERE block, this way a user can add grouped
	 * conditions. The logical operator can be specified too
	 * 
	 * NOTE: don't forget to close the block afterwards 
	 * with {@link M_DbQuery::closeWhereBlock()}
	 * 
	 * @internal when a query is executed the {@link M_DbQuery::execute()} method
	 * will check if there are still any unclosed/opened blocks left
	 * @example 
	 * 
	 * $query->where( array('product_id = ?',1 ));
	 * $query->openWhereBlock();
	 * $query->where( array('product_id = ?', 2 ));
	 * $query->orWhereContains( 'product_name', 'food');
	 * $query->closeWhereBlock();
	 * 
	 * Will result in:
	 * WHERE product_id = 1 AND ( product_id = 2 OR product_name LIKE '%food%')) 
	 * 
	 * @param str $logical
	 * 			The logical operator (AND/OR/...)
	 * @return M_DbQuery
	 */
	public function openWhereBlock($logical = self::OPERATOR_AND) {
		//$this->_where .= empty($this->_where) ? 'WHERE ( ' : ' '.$logical.' ( ';
		$this->_appendWhere($logical);
		
		//remember a block was opened
		$this->_whereBlockCount++;
		
		return $this;
	}
	
	/**
	 * Close WHERE block
	 * 
	 * When a WHERE block was opened, we can close this block after applying
	 * several conditions to the query.
	 * 
	 * Everytime a block get's closed we remember this by decrementing the 
	 * {@link M_DbQuery::_whereBlockCount} property
	 * 
	 * @return M_DbQuery
	 */
	public function closeWhereBlock()
	{
		$this->_where .= ') ';
		
		//remember a block was closed
		$this->_whereBlockCount--;
		
		return $this;
	}
	
	/**
	 * Add WHERE expression
	 * 
	 * This method will add a WHERE expression to the SELECT statement.
	 * Note that, to add multiple conditions, you call this method
	 * multiple times.
	 * 
	 * NOTE:
	 * Note that you can also use placeholders in the condition, as 
	 * you can in {@link M_Db::query()}.
	 * 
	 * Example 1
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->where('date_publish <= ?', time());
	 * </code>
	 * 
	 * NOTE:
	 * In order to add WHERE conditions in bulk, you can pass in an
	 * array of conditions into this method. In that case, each 
	 * element of the array is in turn another array, containing the
	 * arguments that you would pass into a normal call of this method.
	 * 
	 * Example 2:
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->where(array(
	 *       array('date_publish <= ?', time()),
	 *       array('date_expired > ?',  time())
	 *    ));
	 * </code>
	 * 
	 * @access public
	 * @param string $conditions
	 * 		The WHERE expression/clause
	 * @param ...
	 * 		A variable number of arguments, which is used to do
	 * 		replacements of placeholders in the WHERE clause.
	 * @return M_DbQuery
	 */
	public function where($conditions) {
		//append where string
		$this->_appendWhere(self::OPERATOR_AND);
		
		if(is_array($conditions)) {
			// if the conditions argument is an array, we add the conditions
			// in bulk, using the AND operator.
			$i = 0;
			foreach($conditions as $condition) {
				// if the current condition is also an array, we use the
				// elements of the array to set placeholder values:
				if (!is_array($condition)) {
					throw new M_DbException('condition should be array');
				}
				
				$this->_where .= ($i ++ > 0) ? ' '.self::OPERATOR_AND.' ' : ' ';
				$this->_where .= $this->_db->quoteIn(array_shift($condition), $condition);
			}
		} else {
			// if the conditions argument is not an array, we add the string
			// to the collection of where clauses. Note that we also do 
			// placeholder values, before adding the clause
			$conditions = func_get_args();
			$this->_where .= $this->_db->quoteIn(array_shift($conditions), $conditions);
		}
		
		// close the WHERE clause
		$this->_where .= ' )';
		return $this;
	}
	
	/**
	 * Append {@link M_DbQuery::_where} string
	 * 
	 * This method checks whether the WHERE string should be appended, and if
	 * so whether it should be appended/commenced by a 'WHERE (', or 'OPERATOR ('
	 * string.
	 * 
	 * The only case when this string shouldn't be append is when a whereBlock
	 * has been opened
	 * 
	 * @see M_DbQuery::openWhereBlock()
	 * @param str $logical
	 * 		The logical operator
	 * @return str $append
	 * 		The string which has been appended
	 */
	private function _appendWhere($logical) {
		$append = '';
		
		//check if we need to append 
		$lastChar = substr(M_Helper::trimCharlist($this->_where), -1, 1);
		
		// append WHERE?
		if(empty($this->_where)) {
			$append = 'WHERE ( ';
		}
		//only append ( -> do only when a WHERE block has been opened
		elseif ($lastChar == '(') {
			$this->_where .= ' (';
		}
		//append operator
		else {
			$this->_where .= ' '.$logical. ' ( ';
		}
		
		//append to WHERE string
		$this->_where .= $append;
		
		//return the appended string
		return $append;
	}
	
	/**
	 * Add WHERE ... IN() clause
	 * 
	 * This method adds an IN() clause to the SELECT statement. 
	 * Typically, this is used to match a column against a collection
	 * of values.
	 * 
	 * Example 2, get news items of which attribute 'category' equals 
	 * either 1, 2 or 3
	 * 
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->whereIn('category', array(1, 2, 3));
	 * </code>
	 * 
	 * @access public
	 * @param string $column
	 * 		The column to match the provided values against
	 * @param array $values
	 * 		The collection of values
	 * @param str $logical
	 * 		The logical operator (AND/OR/...)
	 * @param bool $binary
	 * 		Set to TRUE to perform a binary comparison
	 * @return M_DbQuery
	 */
	public function whereIn($column, array $values, $logical = self::OPERATOR_AND, $binary = false) {
		return $this->_whereIn($column, $values, $logical, false, $binary);
	}
	
	/**
	 * Add WHERE ... NOT IN() clause
	 * 
	 * This method adds an NOT IN() clause to the SELECT statement. 
	 * Typically, this is used to match a column against a collection
	 * of values.
	 * 
	 * Example 2, get news items of which attribute 'category' does not equals 
	 * either 1, 2 or 3
	 * 
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->whereNotIn('category', array(1, 2, 3));
	 * </code>
	 * 
	 * @access public
	 * @param string $column
	 * 		The column to match the provided values against
	 * @param array $values
	 * 		The collection of values
	 * @param str $logical
	 * 		The logical operator (AND/OR/...)
	 * @param bool $binary
	 * 		Set to TRUE to perform a binary comparison
	 * @return M_DbQuery
	 */
	public function whereNotIn($column, array $values, $logical = self::OPERATOR_AND, $binary = false) {
		return $this->_whereIn($column, $values, $logical, true, $binary);
	}
	
	/**
	 * Internal function which handles the {@see M_DbQuery::whereIn()} and the
	 * {@see M_DbQuery::whereIn()}
	 *
	 * @param string $column
	 * @param array $values
	 * @param string $logical
	 * @param bool $not
	 * @return M_DbQuery
	 */
	private function _whereIn($column, array $values, $logical = self::OPERATOR_AND, $not = false, $binary = false) {
		// check if array isn't empty
		if (count($values) == 0) {
			throw new M_DbException('Cannot perform select on empty array');
		}
		
		foreach($values as $key => $value) {
			$values[$key] = $this->_db->quote($value);
		}
		
		//not?
		$not = ((bool) $not == true) ? ' NOT' : ''; 
		
		//append where string
		$this->_appendWhere($logical);
		
		// Binary comparison?
		if($binary) {
			$this->_where .= 'BINARY ';
		}
		
		$this->_where .= $this->_db->quoteIdentifier($column) . $not . ' IN ( ';
		$this->_where .= implode(',', $values);
		$this->_where .= ' ) )';
		
		return $this;
	}
	
	/**
	 * Add OR/WHERE ... IN() clause
	 * 
	 * This method uses {@link DbQuery::whereIn()} and adds a logical OR parameter
	 * 
	 * @access public
	 * @param string $column
	 * 		The column to match the provided values against
	 * @param array $values
	 * 		The collection of values
	 * @param bool $binary
	 * 		Set to TRUE to perform a binary comparison
	 * @return M_DbQuery
	 */
	public function orWhereIn($column, array $values, $binary = false) {
		return $this->whereIn($column, $values, self::OPERATOR_OR, $binary);
	}
	
	/**
	 * Add OR/WHERE ... NOT IN() clause
	 * 
	 * This method uses {@link DbQuery::whereNotIn()} and adds a logical OR 
	 * parameter
	 * 
	 * @access public
	 * @param string $column
	 * 		The column to match the provided values against
	 * @param array $values
	 * 		The collection of values
	 * @param bool $binary
	 * 		Set to TRUE to perform a binary comparison
	 * @return M_DbQuery
	 */
	public function orWhereNotIn($column, array $values, $binary = false) {
		return $this->whereNotIn($column, $values, self::OPERATOR_OR, $binary);
	}
	
	/**
	 * Add WHERE ... LIKE ... clause
	 * 
	 * This method adds a LIKE evaluation to the SELECT statement. 
	 * Typically, this is used to match a column against a given 
	 * search value.
	 * 
	 * Example, get news items of which the attribute 'headline' 
	 * contains the word 'multimedium'
	 * 
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->whereContains('headline', 'multimedium');
	 * </code>
	 * 
	 * Example 2, get news items of which the attribute 'headline' 
	 * starts with the word 'multi'
	 * 
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->whereContains('headline', 'multi', 'left');
	 * </code>
	 * 
	 * Example 3, get news items of which the attribute 'headline' 
	 * ends with the word 'ium'
	 * 
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->whereContains('headline', 'ium', 'right');
	 * </code>
	 * 
	 * @access public
	 * @param string $column
	 * 		The column to match the provided values against
	 * @param array $value
	 * 		The search value
	 * @param str $value
	 * 		Value position e.g left = value%
	 * @param str $logical
	 * 		The logical operator (AND/OR/...)
	 * @param bool $binary
	 * 		Set to TRUE to perform a binary comparison
	 * @return M_DbQuery
	 */
	public function whereContains($column, $value, $position = 'middle', $logical = self::OPERATOR_AND, $binary = false) {
		
		//determine position
		switch ( strtolower($position) )
		{
			case 'middle':
				$like = '%' . $value . '%';
				break; 
			case 'left':
				$like = $value . '%';
				break;
			case 'right':
				$like = '%' . $value;
				break;
			default:
				throw new M_Exception('Unsupported parameter 
					position "'.$position.'" used');
		}
		
		//append where string
		$this->_appendWhere($logical);
		
		// Binary comparison?
		if($binary) {
			$this->_where .= 'BINARY ';
		}
		
		$this->_where .= $this->_db->quoteIdentifier($column) . ' LIKE '. $this->_db->quote($like);
		$this->_where .= ' )';
		return $this;
	}
	
	/**
	 * Add OR WHERE ... LIKE ... clause
	 * 
	 * This method uses {@link DbQuery::whereContains()} and adds a logical OR parameter
	 * 
	 * @access public
	 * @param string $column
	 * 		The column to match the provided values against
	 * @param array $value
	 * 		The search value
	 * @param str $value
	 * 		Value position e.g left = value%
	 * @param bool $binary
	 * 		Set to TRUE to perform a binary comparison
	 * @return M_DbQuery
	 */
	public function orWhereContains($column, $value, $position = 'middle', $binary = false) {
		return $this->whereContains($column, $value, $position, self::OPERATOR_OR, $binary);
	}
	
	/**
	 * Add OR WHERE
	 * 
	 * This method does exactly the same as {@link M_DbQuery::where()},
	 * but appends the where condition to the SElECT statement with 
	 * the boolean OR operator instead of the AND operator.
	 * 
	 * Check {@link M_DbQuery::where()} to learn more about how to
	 * use this method.
	 * 
	 * @access public
	 * @param string $conditions
	 * 		The WHERE expression/clause
	 * @param ...
	 * 		A variable number of arguments, which is used to do
	 * 		replacements of placeholders in the WHERE clause.
	 * @return M_DbQuery
	 */
	public function orWhere($conditions) {
		//append where string
		$this->_appendWhere(self::OPERATOR_OR);
		
		if(is_array($conditions)) {
			// if the conditions argument is an array, we add the conditions
			// in bulk, using the AND operator.
			$i = 0;
			foreach($conditions as $condition) {
				// if the current condition is also an array, we use the
				// elements of the array to set placeholder values:
				$this->_where .= ($i ++ > 0) ? ' '.self::OPERATOR_AND.' ' : ' ';
				$this->_where .= $this->_db->quoteIn(array_shift($condition), $condition);
			}
		} else {
			// if the conditions argument is not an array, we add the string
			// to the collection of where clauses. Note that we also do 
			// placeholder values, before adding the clause
			$conditions = func_get_args();
			$this->_where .= $this->_db->quoteIn(array_shift($conditions), $conditions);
		}
		
		// close the WHERE clause
		$this->_where .= ' )';
		return $this;
	}
	
	/**
	 * Set limit
	 * 
	 * This method will define the range of records that should be
	 * included in the SELECT statement's result set. The offset and 
	 * count parameters will be passed to {@link MI_Db::queryRange()}.
	 * 
	 * @access public
	 * @access MI_Db::queryRange()
	 * @param integer $offset
	 * 		The start position of the range
	 * @param integer $numberOfRows
	 * 		The number of rows in the range
	 * @return M_DbQuery
	 */
	public function limit($offset, $numberOfRows) {
		$this->_limit = array($offset, $numberOfRows);
		return $this;
	}
	
	/**
	 * Add sorting
	 * 
	 * This method will add sorting to the SELECT statement. In order
	 * to sort on the value of multiple columns, you call this method
	 * multiple times.
	 * 
	 * Example 1, sort records in the 'news' table first by 'time'
	 * (descending order), then by 'headline' (ascending order).
	 * 
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->order('time', 'DESC');
	 *    $select->order('headline', 'ASC');
	 * </code>
	 * 
	 * @access public
	 * @param string $column
	 * 		The column by which to sort
	 * @param string $order
	 * 		The order in which to sort by the column. Possible values 
	 * 		are 'ASC' and 'DESC'.
	 * @return M_DbQuery
	 */
	public function order($column, $order) {
		$order = strtoupper($order);
		if(!in_array($order, array('ASC', 'DESC'))) {
			throw new M_DbException(sprintf('Unrecognized sort order: %s', $order));
		}
		$this->_order .= empty($this->_order) ? 'ORDER BY ' : ', ';
		$this->_order .= $this->_db->quoteIdentifier($column);
		$this->_order .= ' ' . $order;
		return $this;
	}

	/**
	 * Add sorting (Literal)
	 *
	 * This method will add sorting to the SELECT statement. In order
	 * to sort on the value of multiple columns, you call this method
	 * multiple times.
	 * 
	 * @access public
	 * @param string $conditions
	 * 		The conditions for the order
	 * @param string $order
	 * 		The order in which to sort by the column. Possible values
	 * 		are 'ASC' and 'DESC'.
	 * @return M_DbQuery
	 */
	public function orderLiteral($conditions, $order) {
		$order = strtoupper($order);
		if(!in_array($order, array('ASC', 'DESC'))) {
			throw new M_DbException(sprintf('Unrecognized sort order: %s', $order));
		}
		$this->_order .= empty($this->_order) ? 'ORDER BY ' : ', ';
		$this->_order .= $conditions;
		$this->_order .= ' ' . $order;
		return $this;
	}

	/**
	 * Export to string
	 * 
	 * This is a "magic method", called automatically by PHP when the
	 * {@link M_DbQuery} object is being casted to a string. For 
	 * example, an echo call on the object will print the result of 
	 * this function.
	 * 
	 * Example 1
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    echo $select;
	 * </code>
	 * 
	 * Example 2
	 * <code>
	 *    $config = new M_Config;
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $sql = (string) $select; // saves the result of __toString()
	 * </code>
	 * 
	 * @uses M_DbQuery::toString()
	 * @access public
	 * @return string
	 */
	public function __toString() {
		return $this->toString();
	}
	
	/**
	 * Get database connection
	 * 
	 * This method will return the database connection, as it has been
	 * provided to the constructor of {@link M_DbQuery}.
	 * 
	 * @access public
	 * @return MI_Db
	 */
	public function getDb() {
		return $this->_db;
	}
	
	/**
	 * Get WHERE
	 * 
	 * This method will return the WHERE clause(s) that have been set 
	 * previously with:
	 * 
	 * - {@link M_DbQuery::where()}
	 * - {@link M_DbQuery::whereIn()}
	 * - {@link M_DbQuery::whereContains()}
	 * - {@link M_DbQuery::orWhere()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getWhere() {
		return $this->_where;
	}
	
	/**
	 * Get range offset
	 * 
	 * This method will return the range offset, that has been set
	 * previously with {@link M_DbQuery::limit()}
	 * 
	 * @access public
	 * @return integer
	 */
	public function getLimitOffset() {
		return $this->_limit[0];
	}
	
	/**
	 * Get range count
	 * 
	 * This method will return the number of records in the range, 
	 * that has been set previously with {@link M_DbQuery::limit()}
	 * 
	 * @access public
	 * @return integer
	 */
	public function getLimitCount() {
		return $this->_limit[1];
	}
	
	/**
	 * Get ORDER BY
	 * 
	 * This method will return the sorting that has been set previously
	 * with {@link M_DbQuery::order()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Reset WHERE
	 * 
	 * Will reset back to empty value. For more info, read the 
	 * following documentation:
	 * 
	 * - {@link M_DbQuery::where()}
	 * - {@link M_DbQuery::whereIn()}
	 * - {@link M_DbQuery::whereContains()}
	 * - {@link M_DbQuery::orWhere()}
	 * 
	 * @access public
	 * @return M_DbQuery
	 *		Returns itself, for a fluent programming interface
	 */
	public function resetWhere() {
		$this->_where = '';
		return $this;
	}
	
	/**
	 * Reset ORDER BY
	 * 
	 * Will reset back to empty value. For more info, read the 
	 * following documentation:
	 * 
	 * - {@link M_DbQuery::order()}
	 * - {@link M_DbQuery::getOrder()}
	 * 
	 * @access public
	 * @return M_DbQuery
	 *		Returns itself, for a fluent programming interface
	 */
	public function resetOrder() {
		$this->_order = '';
		return $this;
	}
	
	/**
	 * Reset RANGE
	 * 
	 * Will reset back to empty value. For more info, read the 
	 * following documentation:
	 * 
	 * - {@link M_DbQuery::limit()}
	 * - {@link M_DbQuery::getLimitOffset()}
	 * - {@link M_DbQuery::getLimitCount()}
	 * 
	 * @access public
	 * @return M_DbQuery
	 *		Returns itself, for a fluent programming interface
	 */
	public function resetLimit() {
		$this->_limit = FALSE;
		return $this;
	}
}