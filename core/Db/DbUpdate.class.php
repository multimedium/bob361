<?php
/**
 * M_DbUpdate class
 * 
 * The UPDATE statement has been standardized to a great degree. Nearly
 * every database supports the following:
 * 
 * <code>
 *    UPDATE [tables]
 *    [SET conditions]
 *    [WHERE conditions]
 * </code>
 * 
 * Given the standardization of the UPDATE statement, we use
 * M_DbUpdate - an implementation of {@link MI_DbUpdate} - as a
 * shared implementation for all database drivers.
 * 
 * @package Core
 */
class M_DbUpdate extends M_DbQuery implements MI_DbUpdate {
	
	/**
	 * TABLE
	 * 
	 * This property stores the TABLE expression of the UPDATE
	 * statement.
	 * 
	 * @see M_DbUpdate::table()
	 * @see M_DbSelect::getTable()
	 * @access private
	 * @var string
	 */
	protected $_table = '';

	/**
	 * SET
	 *
	 * This property stores the SET expression of the UPDATE
	 * statement.
	 *
	 * @see M_DbUpdate::set()
	 * @access protected
	 * @var string
	 */
	protected $_set = '';

	/* -- Setters -- */
	
	/**
	 * Set TABLE
	 * 
	 * This method is employed to set the TABLE expression of the
	 * UPDATE statement. By doing so, it sets the database table(s)
	 * where data is being updated.
	 * 
	 * NOTE:
	 * To pass in a selection of tables, you can either pass in a
	 * string (single database table), or an array (collection of
	 * tables).
	 * 
	 * NOTE:
	 * Every call of from() will reset the collection of tables that
	 * may have been set before, in order to set the new selection.
	 * 
	 * @access public
	 * @param string|array $tables
	 * 		The selection of database table(s)
	 * @return M_DbUpdate
	 */
	public function table($tables) {
		if(is_array($tables)) {
			$i = 0;
			$this->_table = '';
			foreach($tables as $table) {
				if($i ++ > 0) {
					$this->_table .= ', ';
				}
				$this->_table .= $this->_db->quoteIdentifier($table);
			}
		} else {
			$this->_table = $this->_db->quoteIdentifier($tables);
		}
		
		return $this;
	}

	/**
	 * Add a SET expression to the query
	 * 
	 * @param string|array $expression
	 * @return M_DbUpdate
	 */
	public function set($expression) {

		if(is_array($expression)) {
			// if the conditions argument is an array, we add the conditions
			// in bulk, using the AND operator.
			$i = 0;
			foreach($expression as $exp) {
				// if the current condition is also an array, we use the
				// elements of the array to set placeholder values:
				if (!is_array($exp)) {
					throw new M_DbException('expression should be array');
				}

				$this->_set .= ($i ++ > 0) ? ', ' : ' ';
				$this->_set .= $this->_db->quoteIn(array_shift($exp), $exp);
			}
		} else {
			// if the conditions argument is not an array, we add the string
			// to the collection of set clauses. Note that we also do
			// placeholder values, before adding the clause
			$output = func_get_args();
			$this->_set .= $this->_db->quoteIn(array_shift($output), $output);
		}

		return $this;
	}

	/**
	 * Set a literal SET clause
	 * 
	 * @param string $arg
	 * @return M_DbUpdate
	 */
	public function setLiteral($arg) {
		$this->_set = $arg;
		return $this;
	}

	/**
	 * Reset the TABLE expression
	 *
	 * @return M_DbUpdate
	 */
	public function resetTable() {
		$this->_table = null;
		return $this;
	}
	
	/* -- Getters -- */

	/**
	 * Get the TABLE expression
	 * 
	 * @return string
	 */
	public function getTable() {
		return $this->_table;
	}
	
	/**
	 * Get the UPDATE statement
	 * 
	 * This method will export the SELECT statement to the correct SQL
	 * expression. The result of this method will be used by 
	 * 
	 * - {@link MI_Db::query()}
	 * - {@link MI_Db::queryRange()}
	 * 
	 * to execute the query.
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		$sql = 'UPDATE ' . $this->_table .
				' SET ' . $this->_set .
				' ' . $this->_where .
				' ' . $this->_order;
		return $sql;
	}

	/* -- Fetchers -- */

	/**
	 * Execute the query
	 * 
	 * @return bool
	 */
	public function execute() {
		if ($this->_limit) {
			throw new M_DbException('Limit not yet supported for DbUpdate');
		}
		else {
			return $this->_db->queryOp($this->toString());
		}
	}

	/**
	 * Get the amount of affected rows
	 * 
	 * @return int
	 */
	public function getAffectedRows() {
		return $this->_db->getAffectedRows();
	}
}