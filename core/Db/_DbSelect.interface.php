<?php
/**
 * Select interface
 * 
 * This interface is used for every type select query
 *
 */
interface MI_DbSelect extends MI_DbQuery {
	
	/* -- SQL syntax -- */

	/**
	 * Get SQL Syntax: DISTINCT
	 *
	 * This method is used to compose the DISTINCT expression. This
	 * method will produce the following syntax:
	 *
	 * <code>
	 *    DISTINCT table.column
	 * </code>
	 *
	 * Example 1, get DISTINCT syntax for the column 'headline' of
	 * the database table 'news'
	 *
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    echo $select->distinct('headline', 'news');
	 * </code>
	 *
	 * Example 1 will produce the following SQL expression:
	 *
	 * <code>
	 *    DISTINCT `news`.`headline`
	 * </code>
	 *
	 * @access public
	 * @param string $column
	 * 		The column to be used in the DISTINCT expression
	 * @param string $table
	 * 		The database table in which the column lives (optional)
	 * @return string
	 */
	public function distinct($column, $table = NULL);

	/**
	 * Get SQL Syntax: COUNT
	 *
	 * This method is used to compose an SQL's COUNT function call.
	 * The following syntax is produced by this method:
	 *
	 * <code>
	 *    COUNT( field )
	 * </code>
	 *
	 * Example 1, get COUNT syntax
	 *
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    echo $select->count('*');
	 * </code>
	 *
	 * Example 1 will produce the following SQL expression:
	 *
	 * <code>
	 *    COUNT(*)
	 * </code>
	 *
	 * @access public
	 * @param string $field
	 * 		The selection of column(s) to be counted
	 * @return string
	 */
	public function count($field);
	
	/* -- Getters -- */

	/**
	 * Get FROM
	 *
	 * This method will return the FROM expression that has been set
	 * previously by {@link M_DbSelect::from()}.
	 *
	 * @access public
	 * @return string
	 */
	public function getFrom();

	/**
	 * Get columns
	 *
	 * This method will return the columns that has been set previously
	 * with {@link M_DbQuery::columns()}.
	 *
	 * @access public
	 * @return string
	 */
	public function getColumns();

	/**
	 * Get JOINS
	 *
	 * This method will return the joins that has been set previously
	 * with:
	 *
	 * - {@link M_DbQuery::joinInner()}
	 * - {@link M_DbQuery::joinLeft()}
	 * - {@link M_DbQuery::joinRight()}
	 *
	 * @access public
	 * @return string
	 */
	public function getJoin();

	/**
	 * Get GROUP
	 *
	 * This method will return the gouping conditions that has been
	 * set previously with {@link M_DbQuery::group()}
	 *
	 * @access public
	 * @return string
	 */
	public function getGroup();

	/**
	 * Get HAVING
	 *
	 * This method will return the HAVING expression that has been
	 * set previously with {@link M_DbQuery::group()}
	 *
	 * @access public
	 * @return string
	 */
	public function getHaving();

	/* -- Setters -- */

	/**
	 * Set FROM
	 *
	 * This method is employed to set the FROM expression of the
	 * SELECT statement. By doing so, it sets the database table(s)
	 * where data is being retrieved from.
	 *
	 * NOTE:
	 * To pass in a selection of tables, you can either pass in a
	 * string (single database table), or an array (collection of
	 * tables).
	 *
	 * NOTE:
	 * Every call of from() will reset the collection of tables that
	 * may have been set before, in order to set the new selection.
	 *
	 * @access public
	 * @param string|array $tables
	 * 		The selection of database table(s)
	 * @return M_DbSelect
	 */
	public function from($table);

	/**
	 * Set columns
	 *
	 * This method will set the collection of column(s) that is to be
	 * fetched by the SELECT statement. The collection that is passed
	 * into this method will affect the elements that are contained
	 * in the final result set.
	 *
	 * Read documentation on {@link M_DbQuery::$_columns} to learn
	 * more about the default selection of columns.
	 *
	 * NOTE:
	 * Every call of columns() will reset the collection of columns
	 * that may have been set before, in order to set the new
	 * selection.
	 *
	 * @access public
	 * @param array $columns
	 * 		The columns that will be selected
	 * @param bool $literal Use quoteIdentifier on every column, this can only
	 * be used when $columns is an array
	 * @return M_DbQuery
	 */
	public function columns(array $columns,$literal = false);

	/**
	 * Add INNER JOIN
	 *
	 * The FROM clause indicates the table or tables from which to
	 * retrieve rows. If you name more than one table, you are
	 * performing a join. This method will add an INNER JOIN to the
	 * SELECT statement.
	 *
	 * @access public
	 * @param string $table
	 * 		The table to be joined in the SELECT statement
	 * @param string $tableAlias
	 * 		The alias for the tablename, if left null this is automatically set
	 * 		to $table
	 * @param string $on
	 * 		The column on which to join the table
	 * @param string $condition
	 * 		This condition is applied to the $on parameter, to find
	 * 		matches where the table can be joined on. Typically, this
	 * 		condition is the name of a column in the joined table.
	 * @return M_DbQuery
	 */
	public function joinInner($table, $tableAlias = null, $on, $condition);

	/**
	 * Add LEFT JOIN
	 *
	 * The FROM clause indicates the table or tables from which to
	 * retrieve rows. If you name more than one table, you are
	 * performing a join. This method will add an LEFT JOIN to the
	 * SELECT statement.
	 *
	 * @access public
	 * @param string $table
	 * 		The table to be joined in the SELECT statement
	 * @param string $tableAlias
	 * 		The alias for the tablename, if left null this is automatically set
	 * 		to $table
	 * @param string $on
	 * 		The column on which to join the table
	 * @param string $condition
	 * 		This condition is applied to the $on parameter, to find
	 * 		matches where the table can be joined on. Typically, this
	 * 		condition is the name of a column in the joined table.
	 * @return M_DbQuery
	 */
	public function joinLeft($table, $tableAlias = null, $on, $condition);

	/**
	 * Add RIGHT JOIN
	 *
	 * The FROM clause indicates the table or tables from which to
	 * retrieve rows. If you name more than one table, you are
	 * performing a join. This method will add an RIGHT JOIN to the
	 * SELECT statement.
	 *
	 * @access public
	 * @param string $table
	 * 		The table to be joined in the SELECT statement
	 * @param string $tableAlias
	 * 		The alias for the tablename, if left null this is automatically set
	 * 		to $table
	 * @param string $on
	 * 		The column on which to join the table
	 * @param string $condition
	 * 		This condition is applied to the $on parameter, to find
	 * 		matches where the table can be joined on. Typically, this
	 * 		condition is the name of a column in the joined table.
	 * @return M_DbQuery
	 */
	public function joinRight($table, $tableAlias = null, $on, $condition);

	/**
	 * Check if a join has already been set
	 *
	 * @param str $table
	 * 		The table to which has been joined, if left empty the total amount
	 * 		of joins is returned
	 * @return int
	 * 		Number of joins to this table
	 */
	public function hasJoin($table = null);

	/**
	 * Count the number of inner joins to a table
	 *
	 * @param str $table
	 * 		The table name
	 * @return int
	 */
	public function hasJoinInner($table = null);

	/**
	 * Count the number of left joins to a table
	 *
	 * @param str $table
	 * 		The table name
	 * @return int
	 */
	public function hasJoinLeft($table = null);

	/**
	 * Count the number of right joins to a table
	 *
	 * @param str $table
	 * 		The table name
	 * @return int
	 */
	public function hasJoinRight($table = null);

	/**
	 * Check if grouping
	 *
	 * Will check if grouping has been defined in the query, with the method
	 * {@link M_DbQuery::group()}
	 *
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if grouping exists in the query, FALSE if not
	 */
	public function hasGroup();

	/**
	 * Add GROUP BY
	 *
	 * This method will add a GROUP BY expression to the SELECT
	 * statement. By doing so, the results of the select query will
	 * be grouped by the provided condition.
	 *
	 * NOTE:
	 * To pass in a grouping condition, you can either pass in a
	 * string (single database column), or an array (collection of
	 * columns).
	 *
	 * Example 1, group news items by 'id'
	 *
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->group('id');
	 * </code>
	 *
	 * Example 2, group news items by 'id', and 'category'
	 *
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->group(array('id', 'category'));
	 * </code>
	 *
	 * @access public
	 * @param string|array $columns
	 * 		The columns by which to group results
	 * @return M_DbQuery
	 */
	public function group($columns);

	/**
	 * Add HAVING clause
	 *
	 * This method is used in exactly the same way(s) as the methods
	 *
	 * - {@link M_DbQuery::where()}
	 * - {@link M_DbQuery::orWhere()}
	 *
	 * @access public
	 * @param string $conditions
	 * 		The HAVING expression/clause
	 * @param ...
	 * 		A variable number of arguments, which is used to do
	 * 		replacements of placeholders in the HAVING clause.
	 * @return M_DbQuery
	 */
	public function having($conditions);

	/* -- Resetters -- */

	/**
	 * Reset from
	 *
	 * Will reset back to empty value. For more info, read the
	 * following documentation:
	 *
	 * - {@link M_DbSelect::from()}
	 * - {@link M_DbSelect::getFrom()}
	 *
	 * @access public
	 * @return void
	 */
	public function resetFrom();

	/**
	 * Reset columns
	 *
	 * Will reset back to empty value. For more info, read the
	 * following documentation:
	 *
	 * - {@link M_DbQuery::columns()}
	 * - {@link M_DbQuery::getColumns()}
	 *
	 * @access public
	 * @return void
	 */
	public function resetColumns();

	/**
	 * Reset GROUP BY
	 *
	 * Will reset back to empty value. For more info, read the
	 * following documentation:
	 *
	 * - {@link M_DbQuery::group()}
	 * - {@link M_DbQuery::getGroup()}
	 *
	 * @access public
	 * @return void
	 */
	public function resetGroup();

	/**
	 * Reset HAVING
	 *
	 * Will reset back to empty value. For more info, read the
	 * following documentation:
	 *
	 * - {@link M_DbQuery::having()}
	 * - {@link M_DbQuery::getHaving()}
	 *
	 * @access public
	 * @return void
	 */
	public function resetHaving();

	/**
	 * Reset join
	 *
	 * Will reset back to empty value. For more info, read the
	 * following documentation:
	 *
	 * - {@link M_DbQuery::joinInner()}
	 * - {@link M_DbQuery::joinLeft()}
	 * - {@link M_DbQuery::joinRight()}
	 *
	 * @access public
	 * @return void
	 */
	public function resetJoin();
	
	/* -- Fetchers -- */

	/**
	 * Get first value of first result
	 *
	 * Shortcut to {@link MI_DbResult::getOne()}.
	 *
	 * This method will execute the SELECT statement, and return the
	 * value of the first column of the first record in the result set.
	 * This method is particularly handy if you're looking for just
	 * one value
	 *
	 * Example 1
	 *
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    echo $select;
	 * </code>
	 *
	 * Note that, if no results have been fetched by the select, this
	 * method will return FALSE!
	 *
	 * @uses MI_DbResult::getOne()
	 * @access public
	 * @return string|integer
	 */
	public function getOne();

	/**
	 * Get All results
	 *
	 * Shortcut to {@link MI_DbResult::getAll()}.
	 *
	 * This method will execute the SELECT statement, and return all
	 * results in an array.
	 *
	 * @uses MI_DbResult::getAll()
	 * @access public
	 * @return array
	 */
	public function getAll();

	/* -- Other -- */

	/**
	 * Merge select
	 *
	 * This method will merge 2 {@link MI_DbSelect} objects into one.
	 * Also, you can specify in which mode the objects should be
	 * merged together:
	 *
	 * {@link M_DbSelect::OVERWRITE}
	 * {@link M_DbSelect::COPY}
	 *
	 * The merging mode defaults to the COPY mode.
	 *
	 * Example 1
	 * <code>
	 *    // Create the first select
	 *    $db = M_Db::getInstance();
	 *    $select1 = $db->select();
	 *    $select1->from('news');
	 *
	 *    // Create the second one
	 *    $db = M_Db::getInstance();
	 *    $select2 = $db->select();
	 *    $select2->where('date_expired < ?', time());
	 *
	 *    // Merge the selects
	 *    $select1->merge($select2, M_DbSelect::COPY);
	 *    echo $select1->toString();
	 * </code>
	 *
	 * Example 1 will output the following:
	 *
	 * <code>
	 *    SELECT * FROM news WHERE ( date_expired < 1221422321 );
	 * </code>
	 *
	 * @access public
	 * @param MI_DbSelect $select
	 * 		The select object to be merged into the current select
	 * 		object.
	 * @param string $mode
	 * 		The mode in which the merge of the select objects should
	 * 		be completed.
	 * @return void
	 */
	public function merge(MI_DbSelect $select);
}