<?php
/**
 * M_DbQueryFilterOrderRandom class
 * 
 * adjust the query-order to a random value
 *
 * @package Core
 * @access public
 * @author Ben Brughmans
 */
class M_DbQueryFilterOrderRandom extends M_DbQueryFilter
{
	
	/**
	 * Apply the filter
	 * 
	 * apply the added column-order.
	 * 
	 * @example 
	 * 
	 * $filter = new M_DbQueryFilterOrder('column1', 'ASC');
	 * $filter->filter($select);
	 * 
	 * Final result: 
	 * ORDER BY `column1` ASC
	 *
	 * @param M_DbSelect $query
	 * @return M_DbSelect $query
	 */
	public function filter( M_DbQuery $query) {
		return $query->orderRandom();
	}
	
	/**
	 * Get fields for this filter, based on every field on which we want
	 * to order. Since this filter doesn't use any field it returns an 
	 * empty ArrayIterator
	 * 
	 * @return ArrayIterator
	 */
	public function getFields() {
		return new ArrayIterator(array());
	}
}