<?php
// Since we're using ADODB under the hood, we write a generic driver,
// instead of writing a driver per database brand.
class M_DbTableAdo extends M_DbTable {
	/**
	 * Columns
	 * 
	 * This property stores the collection of database table columns.
	 * Note that this collection (array) stores objects of the {@link 
	 * MI_DbColumn} interface.
	 * 
	 * This property is populated by
	 * 
	 * - {@link M_DbTableAdo::getColumns()}
	 * - {@link M_DbTableAdo::addColumn()}
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_columns;
	
	/**
	 * Primary key
	 * 
	 * This property stores the {@link MI_DbIndex} object that represents
	 * the primary key of the table. This property is populated by
	 * {@link M_DbTableAdo::getIndexes()}.
	 * 
	 * @access protected
	 * @var MI_DbIndex
	 */
	protected $_primaryKey;
	
	/**
	 * Indexes
	 * 
	 * This property stores the collection of indexes that have been 
	 * set for the table.
	 * 
	 * This property is populated by
	 * 
	 * - {@link M_DbTableAdo::getIndexes()}
	 * - {@link M_DbTableAdo::addIndex()}
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_indexes;
	
	/**
	 * Get columns
	 * 
	 * This method will return an ArrayIterator that has been populated
	 * with objects of the {@link MI_DbColumn} interface, each of them 
	 * representing a column in the database table.
	 * 
	 * NOTE:
	 * The keys of the iterator are the names of the columns.
	 *
	 * @access public
	 * @return ArrayIterator
	 */
	public function getColumns() {
		// If not requested before:
		if(! $this->_columns) {
			// Prepare internal format of collection:
			$this->_columns = array();
			
			// Check if this table exists, if not we cannot read the columns
			// from the database
			if($this->_db->hasTable($this->getName())) {
			
				// Run the SQL to fetch the collection of columns:
				$rs = $this->_db->query(sprintf(
					$this->_db->getConnectionProperty('metaColumnsSQL'), 
					$this->getName()
				));
				
				// If the result set is valid:
				if($rs !== FALSE && count($rs) > 0) {
					// For each of the records in the result set:
					foreach($rs as $row) {
						// Construct a new M_DbColumn object:
						$column = new M_DbColumn($row['Field']);
						
						// Set default value
						$column->setDefaultValue($row['Default']);
						
						// Set options:
						$column->setIsNotNull(($row['Null'] == 'NO'));
						$column->setIsAutoIncrement((strpos($row['Extra'], 'auto_increment') !== FALSE));
						
						// Set type and length:
						$type = strtolower($row['Type']);
						
						// If INTEGER
						if(! strncmp($type, 'int', 3)) {
							$column->setType(M_DbColumn::TYPE_INTEGER);
							$column->setLength(substr($type, 4, -1));
						}
						// If FLOAT
						elseif(! strncmp($type, 'float', 4)) {
							$column->setType(M_DbColumn::TYPE_FLOAT);
						}
						// If VARCHAR
						elseif(! strncmp($type, 'varchar', 7)) {
							$column->setType(M_DbColumn::TYPE_VARCHAR);
							$column->setLength(substr($type, 8, -1));
						}
						// If TEXT
						elseif(! strncmp($type, 'text', 4)) {
							$column->setType(M_DbColumn::TYPE_TEXT);
						}
						// If BLOB
						elseif(! strncmp($type, 'blob', 4)) {
							$column->setType(M_DbColumn::TYPE_BLOB);
						}
						
						// Add the M_DbColumn object to collection:
						$this->_columns[$column->getName()] = $column;
					}
				}
			}
		}
		
		// Return the iterator:
		return new ArrayIterator($this->_columns);
	}
	
	/**
	 * Get column
	 * 
	 * This method will return the {@link MI_DbColumn} object that
	 * represents the requested column.
	 * 
	 * NOTE:
	 * If the requested column could not have been found in the table,
	 * this method will return (boolean) FALSE instead!
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the column
	 * @return MI_DbColumn|boolean
	 */
	public function getColumn($name) {
		// Get the collection of columns:
		$this->getColumns();
		
		// Check if the requested column exists:
		$name = (string) $name;
		if(isset($this->_columns[$name])) {
			return $this->_columns[$name];
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get primary key
	 * 
	 * This method will provide with the primary key of the table. Note
	 * that the primary key is represented by an object of 
	 * {@link MI_DbIndex}.
	 * 
	 * NOTE:
	 * If no primary key is available in the table, this method will
	 * return (boolean) FALSE instead!
	 *
	 * @access public
	 * @return MI_DbIndex
	 */
	public function getPrimaryKey() {
		// Get indexes:
		$this->getIndexes();
		
		// If primary key is available:
		if($this->_primaryKey) {
			return $this->_primaryKey;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get indexes
	 * 
	 * This method will return an ArrayIterator that has been populated
	 * with objects of {@link M_DbColumn}. Note however that the objects
	 * are organized by index name.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getIndexes() {
		// If not requested before:
		if(! $this->_indexes) {
			// Prepare internal format of indexes:
			$this->_indexes = array();
			
			// Get the collection of columns:
			$this->getColumns();
			
			// Ask the database driver for collection of indexes
			// ($db->getIndexes() is a adodb-driver-specific method!)
			foreach($this->_db->getIndexes($this->getName(), TRUE) as $name => $columns) {
				// Create a new M_DbIndex object:
				$dbIndex = new M_DbIndex($name);
				
				// Add the columns to the index:
				foreach($columns as $column) {
					$dbIndex->addColumn($this->_columns[$column]);
				}
				
				// Set the type of the index. If the index is a primary
				// key:
				if($name == 'PRIMARY') {
					// Set type:
					$dbIndex->setType(M_DbIndex::PRIMARY);
					
					// Set table's property that stores primary key:
					$this->_primaryKey = $dbIndex;
				}
				// If the index is not a primary key
				else {
					// Set type:
					$dbIndex->setType(M_DbIndex::INDEX);
					
					// Add the index to collection:
					$this->_indexes[$name] = $dbIndex;
				}
			}
		}
		
		// Return the iterator:
		return new ArrayIterator($this->_indexes);
	}
	
	/**
	 * Get index
	 * 
	 * This method will return the {@link MI_DbIndex} object that
	 * represents the requested index.
	 * 
	 * NOTE:
	 * If the requested index could not have been found in the table,
	 * this method will return (boolean) FALSE instead!
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the index
	 * @return MI_DbIndex|boolean
	 */
	public function getIndex($name) {
		// Get the collection of indexes:
		$this->getIndexes();
		
		// Check if the requested index exists:
		$name = (string) $name;
		if(isset($this->_indexes[$name])) {
			return $this->_indexes[$name];
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get total number of records
	 * 
	 * This method will return the total number of records in the table.
	 * To do so, it will construct a SELECT statement that fetches the
	 * COUNT of entries.
	 *
	 * @access public
	 * @return integer
	 */
	public function getTotalNumberOfRecords() {
		// Compose SELECT statement
		$select = $this->select();
		$select->columns($select->count('*'));
		$rs = $select->execute();
		
		// Run the query, and return the result
		if($rs !== FALSE && count($rs) > 0) {
			return $rs->getFirstValue();
		} else {
			return 0;
		}
	}
	
	/**
	 * Add column
	 * 
	 * This method can be used to add a (new) column to the table. Note
	 * that the column is being represented by an object that implements
	 * {@link MI_DbColumn}.
	 * 
	 * NOTE:
	 * If the column already exists in the table, this method will throw
	 * an exception!
	 * 
	 * @throws M_DbException
	 * @access public
	 * @param MI_DbColumn $column
	 * 		The column to be added to the table
	 * @return integer
	 */
	public function addColumn(MI_DbColumn $column) {
		// Get current collection of columns:
		$this->getColumns();
		
		// Add the new column to the collection:
		$name = $column->getName();
		if(!isset($this->_columns[$name])) {
			$this->_columns[$name] = $column;
		} else {
			throw new M_DbException(sprintf(
				'%s: %s: Database Table Column %s already exists in table %s!',
				__CLASS__,
				__METHOD__,
				$name,
				$this->getName()
			));
		}
	}
	
	/**
	 * Set primary key
	 * 
	 * This method can be used to set the primary key of the table. Note
	 * that the primary key is represented by an object that implements
	 * the {@link MI_DbIndex} interface.
	 * 
	 * @access public
	 * @param MI_DbIndex $index
	 * 		The primary key
	 * @return void
	 */
	public function setPrimaryKey(MI_DbIndex $index) {
		$this->_primaryKey = $index;
	}
	
	/**
	 * Add index
	 * 
	 * This method can be used to add an index to the table. Note that
	 * the index is represented by an object that implements the 
	 * {@link MI_DbIndex} interface.
	 *
	 * @access public
	 * @param MI_DbIndex $index
	 * 		The index to be added to the table
	 * @return void
	 */
	public function addIndex(MI_DbIndex $index) {
		// Get current collection of indexes:
		$this->getIndexes();
		
		// Add the new index to the collection:
		$name = $index->getName();
		if(!isset($this->_indexes[$name])) {
			$this->_indexes[$name] = $index;
		} else {
			throw new M_DbException(sprintf(
				'%s: %s: Database Table Column %s already exists!',
				__CLASS__,
				__METHOD__,
				$name
			));
		}
	}
}
?>