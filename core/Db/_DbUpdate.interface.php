<?php
/**
 * Update interface
 * 
 * This interface is used for every type update query
 *
 */
interface MI_DbUpdate extends MI_DbQuery {
	
	/* -- SQL syntax -- */
	
	public function set($expression);
	
	/* -- Getters -- */
	public function getTable();
	
	/* -- Setters -- */
	public function table($table);
	
	/* -- Resetters -- */
	public function resetTable();
	
	/* -- Fetchers -- */
	
	public function getAffectedRows();

}