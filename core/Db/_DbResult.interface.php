<?php
interface MI_DbResult extends Countable, Iterator {
	public function getAll();
	public function getOne();
	public function getFirstvalue();
	public function getColumnValue($key);
}
?>