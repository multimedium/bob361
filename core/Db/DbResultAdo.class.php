<?php
// The result set directly implements the MI_DbResult
// (no need for an abstract implementation in the case of result sets)
class M_DbResultAdo implements MI_DbResult {
	/**
	 * Record set
	 *
	 * Contains the record set, which is provided by the DB Driver.
	 *
	 * @var ADORecordSet
	 */
	private $_resultset;
	private $_count;
	private $_index;
	
	public function __construct($resultset) {
		$this->_resultset = $resultset;
		$this->_count = $resultset->RecordCount();
		$this->_index = 0;
	}
	
	/**
	 * Get the first column, of the first row
	 * 
	 * @return mixed
	 */ 
	public function getFirstValue() {
		$row = $this->current();

		$key = array_keys((array)$row);
		return $row[$key[0]];
	}
	
	/**
	 * Get the value for a column, of the first row
	 *
	 * @param str $key
	 * @return mixed
	 */
	public function getColumnValue($key) {
	    $value = null;
	    $row = $this->current();
	    if (array_key_exists($key,$row)) {
	        $value = $row[$key];
	    }
	    
	    return $value;
	}
	
	/**
	 * Get the value for a column of all rows
	 *
	 * @param str $key
	 * @return array
	 */
	public function getColumnValues($key) {
		$all = array();
		
		foreach ($this AS $row)
		{
			$all[] = M_Helper::getArrayElement($key, $row);
		}
		
		return $all;
	}
	
	/**
	 * Get the first record
	 * 
	 */ 
	public function getOne() {
		return $this->current();
	}
	
	/**
	 * Download all records
	 * 
	 * @return array
	 */ 
	public function getAll() {
		$all = array();
		
		foreach ($this AS $row)
		{
			$all[] = $row;
		}
		
		return $all;
	}
	
	/**
	 * implementation of Countable
	 * 
	 * @return int
	 */ 
	public final function count() {
		return $this->_count;
	}
	
	// implementation of Iterator
	public final function current() {
		return $this->_resultset->fields;
	}
	
	// implementation of Iterator
	public final function key() {
		return $this->_index;
	}
	
	// implementation of Iterator
	public final function next() {
		$this->_resultset->MoveNext();
		$this->_index += 1;
	}
	
	// implementation of Iterator
	public final function rewind() {
		$this->_resultset->MoveFirst();
		$this->_index = 0;
	}

	// implementation of Iterator
	public final function end() {
		$this->_resultset->MoveLast();
		$this->_index = $this->_count-1;
	}
	
	// implementation of Iterator
	public final function valid() {
		return $this->_index < $this->_count;
	}
}