<?php
// Abstract class to support database-level drivers. This class provides
// with the basic implementation of MI_Db, shared by all driver
// implementations
abstract class M_DbDriver implements MI_Db {
	
	/* -- PROPERTIES -- */

	/**
	 * Enable/disable caching of recordsets
	 *
	 * @var bool
	 */
	protected $_cacheMode = false;

	/**
	 * The lifetime a cache of a recordset will live
	 *
	 * @var int
	 */
	protected $_cacheLifetime = 3600;

	/**
	 * Keep connections alive?
	 * 
	 * This property stores a boolean, indicating whether or not the database
	 * connection should be kept alive. For more information, read the docs on
	 * {@link M_DbDriver::setKeepAlive()}.
	 * 
	 * @access private
	 * @var bool $flag
	 */
	private $_keepAlive = FALSE;
	
	/* -- CONSTRUCTORS -- */
	
	// When all references to the database are removed, we close the 
	// connection. Note that the database will be a singleton instance,
	// so this function will be called at the end of the page request.
	public function __destruct() {
		// This has been commented, because this will cause database
		// queries to fail in other objects' __destruct() method!!!
		// (eg. M_LocaleMessageCatalog)
		//$this->close();
	}
	
	/* -- GETTERS -- */
	
	// Check if a table exists in the database:
	public function hasTable($name) {
		foreach($this->getTables() as $table) {
			if($name == $table->getName()) {
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/**
	 * Quote a value
	 * 
	 * This method will quote a value for secure use in a SQL condition, regardless
	 * of the value's data type. Arrays will also be quoted recursively.
	 * 
	 * @static
	 * @access public
	 * @param mixed $value
	 * @return mixed 
	 */
	public static function quote($value) {
		// If the value is an array, or an iterator:
		if(is_array($value)) {
			// For each of the values in the array:
			foreach($value as $key => $current) {
				// Quote the value:
				$value[$key] = self::quote($current);
			}
			
			// Return the value:
			return $value;
		}
		// NULL values
		elseif(is_null($value)) {
			return 'NULL';
		}
		// All others
		else {
			return '"' . self::quoteString($value) . '"';
		}
	}
	
	// Quote a string value for secure use in a SQL query
	public static function quoteString($string) {
		return addslashes((string)$string);
	}
	
	// This method will insert the placeholder values (note that this
	// method will quote the values, taking their types into account)
	// Note that you can provide a variable number of arguments, by
	// adding parameters to the quoteIn() call. However, you can also
	// choose the all-in-one approach, by providing an array with values
	public static function quoteIn($sql, $args) {
		if(!is_array($args)) {
			$args = array();
			for($i = 1, $n = func_num_args(); $i < $n; $i ++) {
				$args[] = func_get_arg($i);
			}
		}
		
		$n = count($args);
		if($n > 0) {
			return preg_replace_callback(
				'/\?/', 
				function($match) use (& $args) {
					// Using the class name M_DbDriver:: instead of self:: does the
					// trick for both PHP 5.3 and PHP 5.5+
					return M_DbDriver::quote(array_shift($args));
				}, 
				$sql, 
				$n
			);
		} else {
			return $sql;
		}
	}

	/**
	 * Is cache enable or not?
	 *
	 * @return bool
	 */
	public function getCacheMode() {
		return $this->_cacheMode;
	}

	/**
	 * The number of seconds cache of a recordset will live
	 * 
	 * @return int
	 */
	public function getCacheLifetime() {
		return $this->_cacheLifetime;
	}
	
	/**
	 * Get flag: keep alive?
	 * 
	 * @see M_DbDriver::setKeepAlive()
	 * @access public
	 * @return bool
	 */
	public function isKeepingAlive() {
		return $this->_keepAlive;
	}
	
	/* -- SETTERS -- */

	/**
	 * Enable caching of recordsets
	 *
	 * Note: lifetime is set default to 3600 seconds
	 *
	 * @param bool $mode TRUE is on, FALSE is off
	 * @param int $lifetime number of seconds the cache will live
	 * @return M_DbDriverAdo
	 */
	public function setCacheMode($mode, $lifetime = null) {
		$this->_cacheMode = (bool)$mode;

		//set lifetime only is available
		if(!is_null($lifetime)) $this->_cacheLifetime = $lifetime;

		return $this;
	}

	/**
	 * Tell the driver how low the cache needs to live
	 * 
	 * @param int $arg
	 * @return M_DbDriver
	 */
	public function setCacheLifetime($arg) {
		$this->_cacheLifetime = (int)$arg;
		return $this;
	}
	
	/**
	 * Set flag: keep alive?
	 * 
	 * This property allows you to set a boolean, indicating whether or not the 
	 * database connection should be kept alive. Keeping alive a connection is
	 * useful in order to avoid the typical error "MySQL Server has gone away".
	 * 
	 * When connections are kept alive, the database driver checks whether or 
	 * not the connection to the server is working on each query executed. If it 
	 * has gone down, an automatic reconnection is attempted. This is handy in 
	 * scripts that remain idle for a long while, to check whether or not the 
	 * server has closed the connection and reconnect if necessary.
	 * 
	 * By default, connections are not kept alive. If you want connections to
	 * be reestablished automatically, you should provide TRUE to this method.
	 * 
	 * @access public
	 * @param bool $flag
	 * @return M_DbDriver $driver
	 *		Returns itself, for a fluent programming interface
	 */
	public function setKeepAlive($flag) {
		$this->_keepAlive = (bool) $flag;
		return $this;
	}
}