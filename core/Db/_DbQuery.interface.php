<?php
/**
 * Query interface
 * 
 * This interface is used for every type of query: select, update, delete, insert
 *
 */
interface MI_DbQuery {
	
	/* -- Getters -- */
	
	public function getDb();
	public function getOrder();
	public function getWhere();
	public function getLimitOffset();
	public function getLimitCount();
	public function toString();
	public function __toString(); // outputs the final SQL query
	
	/* -- Setters -- */
	
	
	public function order($column, $order);
	public function where($conditions);
	public function whereIn($column, array $values);
	public function whereContains($column, $value, $position = 'middle');
	public function limit($offset, $numberOfRows);
	
	/* -- Resetters -- */
	
	public function resetOrder();
	public function resetWhere();
	public function resetLimit();
	
	/* -- Fetchers -- */
	
	public function execute();
}