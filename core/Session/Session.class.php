<?php
/**
 * M_Session class
 * 
 * The M_Session class wraps PHP's session functions, adding extra
 * functionality and default behaviors to it. Note that M_Session
 * implements the MI_EventDispatcher interface. This enables other
 * classes to listen to events triggered by the M_Session class.
 * 
 * @package Core
 */
class M_Session extends M_EventDispatcher implements MI_EventDispatcher {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Reserved namespace.
	 * 
	 * The M_Session class reserves a {@link M_SessionNamespace} for
	 * internal use. This constant holds the name of that namespace.
	 */
	const NS = '_M_NS';
	
	/* -- PROPERTIES -- */
	
	/**
	 * Singleton instance
	 * 
	 * @see M_Session::getInstance()
	 * @static
	 * @access private
	 * @var M_Session
	 */
	private static $_instance;
	
	/**
	 * Session Lifetime
	 * 
	 * ... Defaulted to 1800 seconds (30 minutes).
	 * 
	 * @see M_Session::setLifetime()
	 * @access private
	 * @var int
	 */
	private $_lifetime = 1800;
	
	/**
	 * Session Flag: started?
	 * 
	 * @see M_Session::start()
	 * @see M_Session::isStarted()
	 * @access private
	 * @var boolean
	 */
	private $_started = FALSE;
	
	/**
	 * Previous Session ID
	 * 
	 * @see M_Session::getPreviousId()
	 * @access private
	 * @var string
	 */
	private $_previousId;
	
	/**
	 * Session Namespace
	 *
	 * @access private
	 * @var M_SessionNamespace
	 */
	private $_namespace;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Private Constructor
	 * 
	 * The signature on the M_Session class constructor is PRIVATE. This way, we 
	 * force the M_Session object to be a singleton, as objects need to be 
	 * constructed with the {@link M_Session::getInstance()} method.
	 *
	 * @access private
	 * @return M_Session
	 */
	private function __construct() {}
	
	/**
	 * Destruct Session
	 * 
	 * Since the M_Session is a singleton object, it will be destructed at the 
	 * end of the page request. We benefit from this, by registering some 
	 * additional information about the session. For example, this method will 
	 * save the current page request. This will make the following methods work:
	 * 
	 * - {@link M_Session::getReferrer()}
	 * - {@link M_Session::getPreviousReferrer()}
	 * 
	 * Additionally, when M_Session is destructed, it dispatches an event to 
	 * notify about the end of the page request, with the event type 
	 * {@link M_SessionEvent::CLOSE}. This way, other objects can respond to 
	 * this.
	 *
	 * @access public
	 * @return void
	 */
	public function __destruct() {
		// Get the current Request URI
		$uri = M_Request::getUri()->toString();
		
		// If a referrer is already stored in the session...
		if(isset($this->_namespace->referrer)) {
			// And, the referrer is NOT the same as the current Request URI
			if($this->_namespace->referrer != $uri) {
				// Then, we'll store that referrer as the "previous referrer". 
				$this->_namespace->previousReferrer = $this->_namespace->referrer;
			}
		}
		
		// Set the new referrer:
		$this->_namespace->referrer = $uri;
		
		// Dispatch an event, to notify other objects:
		$this->dispatchEvent(new M_SessionEvent(M_SessionEvent::CLOSE, $this));
	}
	
	/**
	 * Singleton constructor
	 * 
	 * @access public
	 * @return M_Session
	 */
	public static function getInstance() {
		// If the Singleton instance is not yet instantiated:
		if(self::$_instance == NULL) {
			// Then, we construct the Singleton now:
			self::$_instance = new self;
		}
		
		// Return the Singleton
		return self::$_instance;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set Lifetime
	 * 
	 * This method allows you to set the lifetime of the session, expressed in a
	 * number of seconds. Note that you should call this method after having 
	 * started the session with {@link M_Session::start()}.
	 * 
	 * @access public
	 * @param int $seconds
	 * @return M_Session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setLifetime($seconds, $regenerateIdUponExpiration = TRUE) {
		// Make sure the session has been started:
		if(! $this->isStarted()) {
			// If not the case, we consider it an error in the application. We
			// throw an exception to inform about the error:
			throw new M_SessionException(sprintf(
				'Cannot set lifetime of session; session must be started!'
			));
		}
		
		// Cast the number of seconds provided to an integer:
		$seconds = (int) $seconds;
		
		// The number of seconds must be ZERO or greater:
		if($seconds < 0) {
			$seconds = 0;
		}
		
		// Set the lifetime of the cookie. This function updates the runtime ini 
		// values of the corresponding PHP ini configuration keys.
		session_set_cookie_params($seconds, '/');
		
		// However, the PHP's default settings prove to be unreliable. Also, with
		// the method above, we only affect the lifetime of the cookie that is sent
		// to the client. However, it is the server’s task to invalidate a 
		// session, not the client's.
		
		// Check if the time of last activity was set on a previous occasion,
		// in the current session data. And if so, make sure this time of last 
		// activity is not expired (taking into account the lifetime provided).
		// If expired though:
		if(isset($this->_namespace->lastActivity) && (time() - $this->_namespace->lastActivity > $seconds)) {
			// We destroy the session data:
			$this->destroy();
		}
		
		// Updating the session data with every request does also change the 
		// session file’s modification date so that the session is not removed 
		// by the garbage collector prematurely. We benefit from that, by updating
		// the variable that stored the last activity in the session, with the 
		// current time:
		$this->_namespace->lastActivity = time();
		
		// We also set an additional timestamp to regenerate the session ID 
		// periodically to avoid attacks on sessions like session fixation. Check
		// if this time is already stored; if not:
		if(! isset($this->_namespace->timeCreated)) {
			// Store the time now:
			$this->_namespace->timeCreated = time();
		}
		// If this time is already stored, we make sure it has not expired. If
		// expired though...
		elseif(time() - $this->_namespace->timeCreated > $seconds) {
			// It means that the session was started too long ago. In this case,
			// we might want to regenerate the Session ID. We check if the Session
			// ID is to be regenerated; if so:
			if($regenerateIdUponExpiration) {
				// Regenerate the Session ID:
				$this->regenerateId();
			}
			
			// Update to a new time of creation
			$this->_namespace->timeCreated = time();
		}
		
		// Note that session.gc_maxlifetime should be at least equal to the life 
		// time of our custom expiration handler. session.gc_maxlifetime specifies 
		// the number of seconds after which data will be seen as 'garbage' and 
		// cleaned up. Garbage collection occurs during session start.
		@ini_set('session.gc_maxlifetime', $seconds);
		
		// Return myself
		return $this;
	}
	
	/**
	 * Start the session
	 * 
	 * This will start the session. This method is the equivalent of PHP's 
	 * native session_start() function. However, this method will add some extra 
	 * functionality to the starting of a session. 
	 * 
	 * Note that this method will dispatch the {@link M_SessionEvent::INITIATE} 
	 * if this is the first page request in the session, or {@link M_SessionEvent::REFRESH} 
	 * if this is not the first request.
	 * 
	 * Note that this method should be called only once per page request, and 
	 * that the session has to be started before sending any headers to the 
	 * client.
	 *
	 * @access public
	 * @throws M_SessionException
	 * @return M_Session
	 *		Returns itself, for a fluent programming interface
	 */
	public function start() {
		// TODO: Tie the Session ID to the UserAgent's IP Address, for security
		// (The abstract Auth class shouldn't even try to authenticate, if the
		// IP is different from the one that has been tied to the session 
		// initially)
		
		// If the session has already started earlier;
		if($this->_started) {
			// We do not allow the session to be started more than once in a 
			// single page request. We throw an exception to inform about the
			// error in the application:
			throw new M_SessionException('M_Session has already been started');
		}
		
		// For the session to start, we require that no headers were sent earlier.
		// PHP would warn with a notice about this. So, if headers were sent...
		if(headers_sent()) {
			// We throw an exception to inform about the error:
			throw new M_SessionException(
				'Session cannot start; Headers were already sent to the client!'
			);
		}
			
		// Start the session.
		session_start();

		// Mark the Session Singleton as "started":
		$this->_started = TRUE;

		// Note that we reserve a session namespace for the Session Singleton, 
		// for internal use. By setting the second argument to TRUE, we prevent 
		// other objects from constructing a namespace with the same name:
		$this->_namespace = new M_SessionNamespace(self::NS, TRUE);
		
		// In this namespace, among others, we store the number of Page Requests.
		// If this variable is not yet present:
		if(! isset($this->_namespace->numPageRequests)) {
			// We initiate its value to 0
			$this->_namespace->numPageRequests = 0;
			
			// And, we dispatch an event to notify about the Session being
			// initiated for the first time:
			$this->dispatchEvent(new M_SessionEvent(M_SessionEvent::INITIATE, $this));
		}
		// If the namespace already stores a number of Page Requests...
		else {
			// We dispatch an event, to notify about the Session being refreshed
			// in this page request:
			$this->dispatchEvent(new M_SessionEvent(M_SessionEvent::REFRESH, $this));
		}

		// Increment the number of page requests
		$this->_namespace->numPageRequests += 1;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Destroy the session
	 *
	 * This method will remove all session variables that are being stored in 
	 * the global and persistent $_SESSION variable.
	 * 
	 * @access public
	 * @return M_Session
	 *		Returns itself, for a fluent programming interface
	 */
	public function destroy() {
		$_SESSION = array();
		session_unset();
		session_destroy();
	}
	
	/**
	 * Regenerate the Session ID
	 *
	 * Typically, you regenerate the Session ID in order to avoid attacks on
	 * session fixation (session hijacking). When renewing the Session ID, you
	 * can still get the previous ID with {@link M_Session::getPreviousId()}.
	 * 
	 * Note that this method will also dispatch an event, to notify about the
	 * ID being regenerated: {@link M_SessionEvent::RENEWAL}.
	 * 
	 * @access public
	 * @return M_Session
	 *		Returns itself, for a fluent programming interface
	 */
	public function regenerateId() {
		// Get the current Session ID, and save it the "previous ID" property
		$this->_previousId = session_id();
		
		// regenerate the Session ID
		session_regenerate_id();
		
		// Notify other objects about the Session ID Renewal
		$this->dispatchEvent(
			new M_SessionEvent(
				M_SessionEvent::RENEWAL, 
				$this
			)
		);
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Has session been started?
	 * 
	 * This method will check whether or not the Session has been 
	 * started or not, by the {@link M_Session::start()} method.
	 *
	 * @access public
	 * @return boolean
	 * 		Will return TRUE if the session has been started, FALSE if not.
	 */
	public function isStarted() {
		return $this->_started;
	}
	
	/**
	 * Get the session ID
	 * 
	 * @access public
	 * @return string
	 */
	public function getId() {
		return session_id();
	}
	
	/**
	 * Get previous Session ID
	 *
	 * @see M_Session::regenerateId()
	 * @access public
	 * @return string
	 */
	public function getPreviousId() {
		return $this->_previousId;
	}
	
	/**
	 * Get the number of page requests
	 * 
	 * This method will return the number of page requests in the 
	 * current session.
	 *
	 * @access public
	 * @return integer
	 */
	public function getNumberOfPageRequests() {
		return $this->_namespace->numPageRequests;
	}
	
	// Check whether or not this is the first page request of the visitor
	/**
	 * Session Flag: Is first page request?
	 * 
	 * This method will check whether or not this is the first page
	 * request in the current session. Note that, for this method to
	 * function correctly, you should have started the Session with
	 * the {@link M_Session::start()} method.
	 *
	 * @access public
	 * @throws M_SessionException
	 * @return boolean
	 * 		Will return TRUE if this is the first page request in the
	 * 		current session, or FALSE if not.
	 */
	public function isFirstPageRequest() {
		if($this->isStarted()) {
			return ($this->_namespace->numPageRequests == 1);
		} else {
			throw new M_SessionException('Number of Page Requests cannot be retrieved without starting the Session: M_Session::start()');
		}
	}
	
	/**
	 * Get referrer
	 * 
	 * Since not all browsers send the referrer header, and more 
	 * importantly, the referrer header can be spoofed, the M_Session 
	 * class provides a method to fetch the referrer. Note that this 
	 * method will only work AFTER the first page request in the 
	 * current session.
	 * 
	 * If the referrer is (still) not available, this method will return
	 * (boolean) FALSE.
	 * 
	 * @see M_Session::getPreviousReferrer()
	 * @access public
	 * @return M_Uri|boolean
	 */
	function getReferrer() {
		$uri = $this->_namespace->get('referrer', FALSE);
		if($uri) {
			return new M_Uri($uri);
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get previous referrer
	 * 
	 * This method does the same as {@link M_Session::getReferrer()},
	 * but provides with the referrer to the current page's referrer.
	 * 
	 * @see M_Session::getReferrer()
	 * @access public
	 * @return M_Uri|boolean
	 */
	function getPreviousReferrer() {
		$uri = $this->_namespace->get('previousReferrer', FALSE);
		if($uri) {
			return new M_Uri($uri);
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get Session Namespaces iterator
	 * 
	 * This method will return an iterator of the {@link M_SessionNamespace}
	 * objects that have been constructed. Note, however, that M_Session
	 * class reserves a namespace for internal use. This reserved namespace
	 * will not be included in the iterator collection.
	 *
	 * @access public
	 * @return ArrayIterator
	 */
	public function namespaces() {
		$collection = array();
		
		for($i = 0, $k = array_keys($_SESSION), $n = count($k); $i < $n; $i ++) {
			if($k[$i] != self::NS) {
				array_push($collection, new M_SessionNamespace($k[$i]));
			}
		}
		
		return new ArrayIterator($collection);
	}
}