<?php
/**
 * MI_MailLoggerEntry interface
 * 
 * The MI_MailLoggerEntry interface dictates the interface that must be implemented 
 * by the entries of email loggers (see {@link MI_MailLogger}).
 * 
 * @package Core
 */
interface MI_MailLoggerEntry {
	/**
	 * Set logger
	 * 
	 * Will set the logger that is in charge of saving the entry. Note that the
	 * logger is represented by an object that implements the {@link MI_MailLogger}
	 * interface.
	 * 
	 * @access public
	 * @param MI_MailLogger $logger
	 * @return void
	 */
	public function setLogger(MI_MailLogger $logger);
	
	/**
	 * Set email message
	 * 
	 * Will set the email message that should be contained by the log entry.
	 * 
	 * @access public
	 * @param M_Mail $mail
	 * 		The email message
	 * @return void
	 */
	public function setMail(M_Mail $mail);
	
	/**
	 * Set time
	 * 
	 * Will set the date + time of the log entry
	 * 
	 * @access public
	 * @param M_Date $time
	 * @return void
	 */
	public function setTime(M_Date $time);
	
	/**
	 * Get logger
	 * 
	 * Will provide with the logger that has been set previously with
	 * {@link M_MailLoggerEntry::setLogger()}.
	 * 
	 * @access public
	 * @return MI_MailLogger
	 */
	public function getLogger();
	
	/**
	 * Get email message
	 * 
	 * Will provide with the email message that is contained by the log entry.
	 * 
	 * @access public
	 * @return M_Mail
	 */
	public function getMail();
	
	/**
	 * Get date + time
	 * 
	 * Will provide with the date + time of the log entry. The date + time is
	 * represented by an instance of {@link M_Date}.
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getTime();
	
	/**
	 * Save the entry
	 * 
	 * Will save the log entry to the log. This method will ask the logger object
	 * (previously set with {@link MI_MailLoggerEntry::setLogger()}) to save the
	 * entry to the log.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function save();
}