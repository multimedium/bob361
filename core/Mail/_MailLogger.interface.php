<?php
/**
 * MI_MailLogger interface
 * 
 * The MI_MailLogger interface dictates the interface that must be implemented
 * by email loggers.
 * 
 * @package Core
 */
interface MI_MailLogger {
	/**
	 * Get logged email messages
	 * 
	 * Will provide with the collection of logged email messages. The 
	 * collection is represented by an instance of ArrayIterator. Each 
	 * element in the iterator is an object that implements the 
	 * {@link MI_MailLoggerEntry} interface.
	 * 
	 * Example 1, get the recipients of the email messages that have 
	 * been logged:
	 * <code>
	 *    // We assume that M_MailLogger is a class that implements the 
	 *    // MI_MailLogger interface
	 *    $logger = new M_MailLogger;
	 *    
	 *    // We ask the logger for the log entries:
	 *    foreach($logger->getLogEntries() as $entry) {
	 *       // Get the recipient of the current entry:
	 *       echo $entry->getMail()->getRecipientsString() . '<br>';
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @param string $context
	 * 		The context of the log entries
	 * @return ArrayIterator
	 */
	public function getEntries($context = NULL);
	
	/**
	 * Add an entry
	 * 
	 * Will add/save an entry to the log.
	 * 
	 * @access public
	 * @param MI_MailLoggerEntry $entry
	 * 		The entry to be saved to the log
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function addEntry(MI_MailLoggerEntry $entry);
	
	/**
	 * Get new entry
	 * 
	 * Will provide with a new (empty) log entry. Mainly used by a mailer, to
	 * log an email message.
	 * 
	 * @access public
	 * @return MI_MailLoggerEntry
	 */
	public function getNewEntry();
}