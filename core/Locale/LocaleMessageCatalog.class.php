<?php
/**
 * M_LocaleMessageCatalog class
 * 
 * This class is used to retrieve translated strings from the locale's message 
 * catalog.
 * 
 * @package Core
 */
class M_LocaleMessageCatalog {
	/**
	 * Import mode
	 * 
	 * This constant can be used to address the mode in which a Portable
	 * Object (object of {@link M_LocalePortableObject}) should be 
	 * copied into the M_LocaleMessageCatalog. This particular constant
	 * is used to merge a Portable Object with the already existing
	 * strings of the message catalog.
	 * 
	 * @see {@link M_LocaleMessageCatalog::setPortableObject()}
	 */
	const PO_MERGE = 'merge';
	
	/**
	 * Import mode
	 * 
	 * This constant can be used to address the mode in which a Portable
	 * Object (object of {@link M_LocalePortableObject}) should be 
	 * copied into the M_LocaleMessageCatalog. This particular constant
	 * is used to do the following:
	 * 
	 * 1) Empty the message catalog
	 * 2) Copy the strings of the Portable object into the catalog
	 * 
	 * @see {@link M_LocaleMessageCatalog::setPortableObject()}
	 */
	const PO_EMPTY_AND_COPY = 'truncate-copy';
	
	/**
	 * The singleton instances
	 * 
	 * A singleton object is created per locale. This property
	 * stores the constructed singleton objects.
	 *
	 * @access private
	 * @var array
	 */
	private static $_instances = array();
	
	/**
	 * The installed locales
	 * 
	 * This property stores information about the locales for which
	 * a catalog has been installed.
	 *
	 * @access private
	 * @var array
	 */
	private static $_installed = NULL;
	
	/**
	 * The cache object
	 * 
	 * A cache object is attached to the M_LocaleMessageCatalog class 
	 * (statically, not per object), which is in charge of reading 
	 * and writing the catalog from and to cache memory.
	 * 
	 * @access private
	 * @var string
	 */
	private static $_cache = NULL;
	
	/**
	 * The locale
	 * 
	 * This property holds the locale name represented by the
	 * M_LocaleMessageCatalog object.
	 *
	 * @access private
	 * @var string
	 */
	private $_locale;
	
	/**
	 * The collection of translated strings
	 * 
	 * @access private
	 * @var array
	 */
	private $_strings = NULL;
	
	/**
	 * Retrieved from cache?
	 * 
	 * This property (boolean) tells whether or not the translated 
	 * strings have been fetched from cache (TRUE) or not (FALSE)
	 * 
	 * @access private
	 * @var boolean
	 */
	private $_stringsAreFromCache = FALSE;
	
	/**
	 * Number of plural forms
	 * 
	 * This property stores the number of plural forms that is defined
	 * by the Message Catalog. For more information about plural forms:
	 * 
	 * http://www.gnu.org/software/gettext/manual/gettext.html#Plural-forms
	 * 
	 * @access private
	 * @var integer
	 */
	private $_numberOfPluralForms = NULL;
	
	/**
	 * Plural formula
	 * 
	 * This property stores the plural formula that is defined by the 
	 * Message Catalog. For more information about plural forms:
	 * 
	 * http://www.gnu.org/software/gettext/manual/gettext.html#Plural-forms
	 * 
	 * @access private
	 * @var string
	 */
	private $_pluralFormula = NULL;
	
	/**
	 * (Private) Constructor
	 * 
	 * The signature on the M_LocaleMessageCatalog class constructor 
	 * is PRIVATE. This way, we force the M_LocaleMessageCatalog object 
	 * to be a singleton (per locale), as objects can only be 
	 * constructed by the class itself.
	 * 
	 * In the public interface, the constructor is:
	 * 
	 * <code>
	 *    $catalog = M_LocaleMessageCatalog::getInstance();
	 * </code>
	 *
	 * @access private
	 * @param string $locale
	 * 		The locale name
	 * @return M_LocaleMessageCatalog
	 */
	private function __construct($locale) {
		$this->_locale = $locale;
	}
	
	/**
	 * Destructor
	 * 
	 * Before destructing the message catalog, all changes to the 
	 * catalog (if any) are saved back to the database. This method is 
	 * in saving the changes made by:
	 * 
	 * - {@link M_LocaleMessageCatalog::setNumberOfPluralForms()},
	 * - {@link M_LocaleMessageCatalog::setPluralFormula()}
	 * 
	 * By delaying the save to database, we reduce the number of SQL
	 * queries needed. As many calls to the methods listed above will 
	 * only trigger a single SQL query to save all of the accumulated 
	 * changes to the database.
	 * 
	 * @access public
	 * @return void
	 */
	public function __destruct() {
		if($this->_numberOfPluralForms != NULL || $this->_pluralFormula != NULL) {
			$atts = array();
			if($this->_numberOfPluralForms != NULL) {
				$atts['nplurals'] = $this->_numberOfPluralForms;
			}
			if($this->_pluralFormula != NULL) {
				$atts['plural_formula'] = $this->_pluralFormula;
			}
			
			$db = M_Db::getInstance();
			$table = $db->getTable('locale');
			$table->update($atts, $db->quoteIdentifier('locale') . ' = ?', $this->_locale);
		}
	}
	
	/**
	 * Singleton constructor
	 * 
	 * NOTE:
	 * If the locale is not provided, {@link M_LocaleMessageCatalog}
	 * will use the locale name that has been set in the locale category
	 * {@link M_Locale::LC_MESSAGES}. For more info about locale categories,
	 * read the docs on {@link M_Locale::getCategory()}.
	 * 
	 * @see M_LocaleMessageCatalog::__construct()
	 * @access public
	 * @param string $locale
	 * 		The locale name
	 * @return M_LocaleMessageCatalog
	 */
	public static function getInstance($locale = NULL) {
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
		}
		if(!isset(self::$_instances[$locale])) {
			if(self::isInstalled($locale)) {
				self::$_instances[$locale] = new self($locale);
			} else {
				throw new M_LocaleException(sprintf(
					'%s: Unsupported locale "%s"',
					__CLASS__,
					$locale
				));
			}
		}
		return self::$_instances[$locale];
	}
	
	/**
	 * Get installed locales
	 * 
	 * This method will return the collection of locales for which a 
	 * message catalog has been installed. For more information on
	 * locale names, read {@link M_Locale::getNameParts()}.
	 * 
	 * @static
	 * @access public
	 * @return array
	 */
	public static function getInstalled() {
		return array_keys(self::_getInstalledCatalogs());
	}
	
	/**
	 * Get installed locales
	 * 
	 * This method will return the collection of locales for which a 
	 * message catalog has been installed. So, in a way, it does the exact 
	 * same thing as {@link M_LocaleMessageCatalog::getInstalled()}. However,
	 * this method will return an associative array. For example:
	 * 
	 * <code>
	 *    Array (
	 *       en => English,
	 *       nl => Dutch,
	 *       de => German
	 *    )
	 * </code>
	 * 
	 * To fetch the locales' display names, this method will make use of
	 * {@link M_Locale::getLanguageDisplayName()}
	 * 
	 * @static
	 * @see M_LocaleMessageCatalog::getInstalled()
	 * @see M_Locale::getLanguageDisplayName()
	 * @access public
	 * @param string $locale
	 * 		The locale in which the locales' display names should be retrieved
	 * @return array
	 */
	public static function getInstalledWithDisplayNames($locale = NULL) {
		// Prepare empty iterator (output)
		$out = array();
		
		// For each of the installed locales:
		foreach(self::getInstalled() as $currentLocale) {
			// Add the locale to the output, with display name:
			$out[$currentLocale] = M_Locale::getLanguageDisplayName($currentLocale, $locale);
		}
		
		// Return the iterator:
		return new ArrayIterator($out);
	}
	
	/**
	 * Is locale installed?
	 * 
	 * This method will use {@link M_LocaleMessageCatalog::getInstalled()}
	 * to check if the requested locale is installed.
	 * 
	 * NOTE:
	 * This method will return (boolean) TRUE if the locale has been
	 * installed, FALSE if not.
	 * 
	 * @static
	 * @access public
	 * @param string $locale
	 * @return boolean
	 */
	public static function isInstalled($locale) {
		return in_array($locale, self::getInstalled());
	}
	
	/**
	 * Install a locale
	 * 
	 * @static
	 * @access public
	 * @param string $locale
	 * @return bool $flag
	 * 		Returns TRUE if successfully installed, FALSE if not
	 */
	public static function installLocale($locale, $numberOfPlurals, $pluralFormulaInC) {
		// If not yet installed:
		if(! self::isInstalled($locale)) {
			// Install now:
			$db = M_Db::getInstance();
			
			// First, install database table (if not done yet)
			if(! $db->hasTable('locale')) {
				self::_installDbTableLocale();
			}
			
			// Install the locale:
			$rs = $db->getTable('locale')->insert(array(
				'locale' => (string) $locale
			));
			
			// If successful:
			if($rs) {
				// Add in internal copy of installed locales:
				self::$_installed[(string) $locale] = array(
					'locale'         => (string) $locale,
					'nplurals'       => (int) $numberOfPlurals,
					'plural_formula' => ''
				);
				
				// Ask for the singleton instance:
				$c = self::getInstance((string) $locale);
				$c->setNumberOfPluralForms((int) $numberOfPlurals);
				$c->setPluralFormulaInC($pluralFormulaInC);
				return TRUE;
			}
		}
		
		// If still here, something went wrong
		return FALSE;
	}
	
	/**
	 * Get locale name
	 * 
	 * This method will return the locale to which the Message Catalog
	 * belongs. The M_LocaleMessageCatalog has been constructed with 
	 * this locale name: see {@link M_LocaleMessageCatalog::getInstance()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getLocaleName() {
		return $this->_locale;
	}
	
	/**
	 * Set cache object
	 * 
	 * A {@link M_CacheMemory} object is attached to the M_LocaleMessageCatalog
	 * (statically, not per object), which is in charge of reading and 
	 * writing the stored cache of message catalogs.
	 * 
	 * By attaching an {@link M_CacheMemory} object through the public API,
	 * we promote "polymorphism" (expensive word for flexibility in how
	 * cache is handled). This way, cache may be stored in the database,
	 * or in PHP cache files. The M_LocaleMessageCatalog class does not need 
	 * to worry about which type of cache is being used, it simply talks 
	 * to the cache object through its public interface.
	 * 
	 * Note that, if you do not provide the M_LocaleMessageCatalog class with 
	 * a cache object, the M_LocaleMessageCatalog class will not try to fetch 
	 * the message catalog from cache, nor will it try to store in cache. 
	 * If you prefer not to store the catalog in cache memory, you can use the 
	 * {@link M_LocaleData::removeCacheObject()} method do remove the 
	 * previously attached cache object.
	 * 
	 * @access public
	 * @param M_CacheMemory $cache
	 * 		The constructed cache object
	 * @return void
	 */
	public static function setCacheObject(M_CacheMemory $cache) {
		self::$_cache = $cache;
	}
	
	/**
	 * Get cache object
	 * 
	 * This method will return the {@link M_CacheMemory} object that has
	 * been attached previously with {@link M_LocaleData::setCacheObject()}.
	 *
	 * NOTE:
	 * Note that, if no cache object has been set before, this method
	 * will return NULL.
	 * 
	 * @access public
	 * @return M_CacheMemory|NULL
	 */
	public static function getCacheObject() {
		return self::$_cache;
	}
	
	/**
	 * Remove cache object
	 * 
	 * This method will remove the cache object, previously attached
	 * to the class with the {@link M_LocaleData::setCacheObject()} 
	 * method.
	 *
	 * NOTE:
	 * Note that, if you do not provide the M_LocaleMessageCatalog class 
	 * with a cache object, the M_LocaleMessageCatalog class will not try 
	 * to fetch the catalog from cache, nor will it try to store in 
	 * cache.
	 * 
	 * @access public
	 * @return void
	 */
	public static function removeCacheObject() {
		self::$_cache = NULL;
	}
	
	/**
	 * Get translated string
	 * 
	 * This method will look for the translated version of the provided 
	 * string. You can also use the function {@link t()} as a shortcut.
	 * 
	 * @access public
	 * @param string $string
	 * 		The untranslated string
	 * @param array $args
	 * 		String arguments. The keys of this array will be used to do 
	 * 		replacements in the translated string: 
	 * 		{@link M_LocaleMessageCatalog::insertPlaceholderValues()}
	 * @return string
	 */
	public function getText($msgid, array $args = array()) {
		return $this->insertPlaceholderValues($this->getTranslatedString($msgid), $args);
	}
	
	/**
	 * Get plural form
	 * 
	 * Before the time anybody thought about internationalization (and, 
	 * sadly, even afterwards) one can often find code similar to the 
	 * following:
	 * 
	 * <code>
	 *    printf("%d file%s deleted", $n, $n == 1 ? "" : "s");
	 * </code>
	 * 
	 * After the first complaints from people internationalizing the 
	 * code people either completely avoided formulations like this or 
	 * used strings like "file(s)". Both look unnatural and should be 
	 * avoided. First tries to solve the problem correctly looked like 
	 * this:
	 * 
	 * <code>
	 *    if (n == 1) {
	 *       printf ("%d file deleted", $n);
	 *    } else {
	 *       printf ("%d files deleted", $n);
	 *    }
	 * </code>
	 * 
	 * But this does not solve the problem. It helps languages where 
	 * the plural form of a noun is not simply constructed by adding an 
	 * "s" but that is all. Once again people fell into the trap of 
	 * believing the rules their language is using are universal. But 
	 * the handling of plural forms differs widely between the language 
	 * families. For example, Rafal Maszkowski <rzm@mat.uni.torun.pl> 
	 * reports:
	 * 
	 * In Polish we use e.g. plik (file) this way:
	 * 
	 * <code>
	 *    1 plik
	 *    2,3,4 pliki
	 *    5-21 pliko'w
	 *    22-24 pliki
	 *    25-31 pliko'w
	 * </code>
	 * 
	 * There are two things which can differ between languages (and 
	 * even inside language families):
	 * 
	 * 1) The form how plural forms are built differs.
	 * This is a problem with languages which have many irregularities. 
	 * German, for instance, is a drastic case. Though English and 
	 * German are part of the same language family (Germanic), the 
	 * almost regular forming of plural noun forms (appending an "s") 
	 * is hardly found in German.
	 * 
	 * 2) The number of plural forms differ.
	 * This is somewhat surprising for those who only have experiences 
	 * with Romanic and Germanic languages since here the number is 
	 * the same (there are two). But other language families have only 
	 * one form or many forms.
	 * 
	 * The consequence of this is that application writers should not 
	 * try to solve the problem in their code. This would be localization 
	 * since it is only usable for certain, hardcoded language 
	 * environments.
	 * 
	 * This method accepts instead of the one key string two strings 
	 * and a numerical argument. The idea behind this is that using 
	 * the numerical argument and the first string as a key, the 
	 * implementation can select using rules specified by the translator 
	 * the right plural form. The two string arguments then will be 
	 * used to provide a return value in case no translation is found 
	 * (similar to the normal gettext behavior). In this case the rules 
	 * for Germanic language is used and it is assumed that the first 
	 * string argument is the singular form, the second the plural 
	 * form.
	 * 
	 * This has the consequence that programs without language catalogs 
	 * can display the correct strings only if the program itself is 
	 * written using a Germanic language. This is a limitation but 
	 * since the GNU C library (as well as the GNU gettext package) 
	 * are written as part of the GNU package and the coding standards 
	 * for the GNU project require program being written in English, 
	 * this solution nevertheless fulfills its purpose. 
	 * 
	 * IMPORTANT NOTE:
	 * The @count variable is always made available in translated 
	 * string in plural.
	 * 
	 * @see M_LocaleMessageCatalog::hasPluralFormula()
	 * @access public
	 * @param string $msgid1
	 * 		The untranslated string (singular form)
	 * @param string $msgid2
	 * 		The untranslated string (plural form)
	 * @param integer $n
	 * 		The numerical argument; or count of items, on which to 
	 * 		apply the plural formula of the locale. In other words, 
	 * 		this is the input of the Plural Formula. The output of the 
	 * 		formula defines which plural form is returned. To learn 
	 * 		more, read {@link M_LocaleMessageCatalog::hasPluralFormula()}.
	 * @param array $args
	 * 		String arguments. The keys of this array will be used to 
	 * 		do replacements in the translated string: 
	 * 		{@link M_LocaleMessageCatalog::insertPlaceholderValues()}
	 * @return string
	 */
	public function getPluralText($msgid1, $msgid2, $n, array $args = array()) {
		// We make the @count placeholder value available in the translated string:
		$number = new M_Number($n);
		$args['@count'] = $number->toString();
		
		// If the message catalog does not have a plural formula, we use the rules 
		// for Germanic language, which means that we assume that the first string 
		// argument is the singular form (n == 1), and the second one the plural 
		// form (n != 1).
		if($this->hasPluralFormula()) {
			$nplurals = $this->getNumberOfPluralForms();
			$nplural  = @eval('return (int)(' . $this->getPluralFormula().');');
		} else {
			// default (germanic language) formula
			$nplurals = 2;
			$nplural  = $n == 1 ? 0 : 1;
		}
		
		// Now, we have the N result of the plural formula. The resulting value 
		// then must be greater or equal to zero and smaller than the value given 
		// as the value of 'nplurals' in the locale.
		$c = ($nplural >= 0 && $nplural < $nplurals);
		if(!$c) {
			$nplural = 0;
		}
		
		// get the translated text:
		if($nplural == 0) {
			// If N is 0 (ZERO), we return msgstr[0]
			// msgstr[0] is stored with the msgid1 (singlar) string:
			return $this->insertPlaceholderValues($this->getTranslatedString($msgid1), $args);
		} else {
			// If N > 0
			// msgstr[N] is stored with the msgid2 (plural) string:
			$msgstr = $this->getTranslatedString($msgid2);
			
			// The (plural) string is represented in an array. But, if the
			// string is not translated, the original (untranslated) is returned.
			// So it might be a string instead of an array:
			if(!is_array($msgstr)) {
				return $this->insertPlaceholderValues($msgstr, $args);
			}
			// if the plural string is represented in an array, we return
			// the requested plural form:
			else {
				return $this->insertPlaceholderValues($msgstr[$nplural], $args);
			}
		}
	}
	
	/**
	 * Get collection of texts, summed up
	 * 
	 * This method can be used in order to create an inline and translated text,
	 * with a list of strings. Consider the following example, where we have a 
	 * collection of colors.
	 *
	 * <code>
	 *    $colors = Array(
	 *       0 => 'Green',
	 *       1 => 'Brown',
	 *       2 => 'Blue',
	 *       3 => 'Yellow'
	 *    );
	 * </code>
	 *
	 * Let's say that, in our application, we want to create a text with this
	 * collection, summing up all of the colors in our collection. In order to
	 * do so, we could write the following code:
	 *
	 * <code>
	 *    // Define the collection of colors
	 *    $colors = Array(
	 *       0 => 'Green',
	 *       1 => 'Brown',
	 *       2 => 'Blue',
	 *       3 => 'Yellow'
	 *    );
	 * 
	 *    // Get the catalog, and sum up the collection of colors:
	 *    echo M_LocaleMessageCatalog::getInstance()->getTextsSummedUp($colors);
	 * </code>
	 *
	 * In our example, the output would be as following:
	 *
	 * <code>
	 *    "Green, Brown, Blue and Yellow"
	 * </code>
	 *
	 * @access public
	 * @param array $strings
	 *		The strings to be summed up
	 * @param string $andGlue
	 *		The final "and glue" to be used for summing up. Accepted values are
	 *		'AND' or 'OR'. By default, 'AND' is used.
	 * @param string $prepend
	 *		Optionally, the string to be prepended to each of the strings in the resulting
	 *		text.
	 * @param string $append
	 *		The string to be appended to each of the strings in the resulting
	 *		text.
	 * @return string $text
	 *		The translated text, in which the provided collection of items has 
	 *		been summed up.
	 */
	public function getTextsSummedUp(array $strings, $andGlue = 'AND', $prepend = '', $append = '') {
		// We prepare the output string:
		$out = '';
		
		// Get the "comma glue", and the "and glue":
		// This is a translated string in the message catalog, with a predefined
		// format. The ¤ character is used to split up the three different parts
		// in the string:
		// - the "comma glue" (0)
		// - the "and glue" (1)
		// - the "or glue" (2)
		// Note that whitespaces are prepended and/or appended to each part. These 
		// spaces will be maintained when summing up items, and allows translators
		// to define how spaces are added to comma's etc.
		$gluesFormat = $this->getText(', ¤ and ¤ or ');
		
		// Extract the parts:
		$glues = explode('¤', $gluesFormat);
		
		// We should have obtained exactly 3 parts from the translated string 
		// with glues:
		if(count($glues) != 3) {
			// If that is not the case, we throw an exception to inform about
			// the error:
			throw new M_LocaleException(sprintf(
				'Cannot get parts for summed up texts in Message Catalog. Exactly ' . 
				'3 parts should be present in the translated version of "%s". ' . 
				'Instead, %s parts were encountered in "%s"',
				', ¤ and ¤ or ',
				count($glues),
				$gluesFormat
			));
		}
		
		// Now, we determine which "and glue" is to be used for summing up. This
		// glue might be the "and" part, or the "or" part:
		$and = (strtolower($andGlue) == 'and') ? $glues[1] : $glues[2];
		
		// And, we determine the "comma glue"
		$comma = $glues[0];
		
		// We prepare some working variables:
		$n = count($strings);
		$i = 0;

		// For each of the strings in the provided collection
		foreach($strings as $string) {
			// Clean up the current string
			$clean = trim($string);
			
			// If the current string is not empty
			if($clean != '') {
				// If this is not the first item we are going to append to the 
				// output string, then we will need to add "glue" first:
				if($i ++ > 0) {
					// The glue may be the "comma glue" or the "and glue". The 
					// "and glue" is prepended to the last item in the list. So, 
					// we check whether or not this is the last item:
					if($i == $n) {
						// Yes, it is! Then add the "and glue" to the output first:
						$out .= $and;
					}
					// This is not the last item in the list:
					else {
						// Then add the "comma glue":
						$out .= $comma;
					}
				}

				// Add the current item to the list:
				$out .= $prepend . $string . $append;
			}
		}
		
		// Return the output string:
		return $out;
	}
	
	/**
	 * Get translated string
	 * 
	 * This method will look up translations of a given untranslated 
	 * string from the message catalog. Typically, this method is only 
	 * used internally by the {@link M_LocaleMessageCatalog::getText()} 
	 * and {@link M_LocaleMessageCatalog::getPluralText()} methods.
	 * 
	 * NOTE:
	 * This method will read the message catalog, only if not already 
	 * stored in cache. To learn more on how to use cache in the 
	 * message catalog, read the documentation on 
	 * 
	 * - {@link M_LocaleMessageCatalog::setCacheObject()}.
	 * - {@link M_LocaleMessageCatalog::getCacheObject()}.
	 * - {@link M_LocaleMessageCatalog::removeCacheObject()}.
	 * 
	 * @access public
	 * @param string $msgid
	 * 		The untranslated string
	 * @return string
	 */
	public function getTranslatedString($msgid) {
		// Empty strings cannot be translated:
		if(! $msgid) {
			return '';
		}

		// Initiate the collection of strings
		$this->_initStringsCollection();
		
		// Check if an entry has been created for the requested 
		// untranslated-string:
		if(!array_key_exists($msgid, $this->_strings)) {
			
			// Before registering the untranslated string, we check
			// if it already exists in the database (only if we have
			// retrieved the strings from cache)
			if($this->_stringsAreFromCache) {
				// Get the database connection:
				$db = M_Db::getInstance();
				
				// Compose the query that fetches the untranslated string from the db
				$sql  = 'SELECT ' . $db->quoteIdentifier('id');
				$sql .= ' FROM ' . $db->quoteIdentifier('locale_untranslated');
				$sql .= ' WHERE ' . $db->quoteIdentifier('untranslated') . ' = ?';
				
				// Fetch the result:
				$rs = $db->query($sql, $msgid);
				
				// If the untranslated string does not exist, we register
				// it now:
				if($rs !== FALSE && count($rs) == 0) {
					$this->_registerUntranslatedString($msgid, NULL, '', M_Request::getUriString());
				}
				// We need to remove cache as well, also if the untranslated
				// string is already available, to avoid this from happening
				// again :)
				else {
					$cache = self::getCacheObject();
					if($cache) {
						$cache->remove('locale.' . $this->_locale);
					}

					$this->_strings[$msgid] = TRUE;
				}
			}
			// If we have NOT retrieved the strings FROM CACHE, we 
			// register the untranslated string in the database:
			else {
				$this->_registerUntranslatedString($msgid, NULL, '', M_Request::getUriString());
			}
		}
		// return the translated text, if available. If no translation 
		// is available for the given string, we simply return the 
		// original string.
		return ($this->_strings[$msgid] === TRUE ? $msgid : $this->_strings[$msgid]);
	}
	
	/**
	 * Get Iterator
	 * 
	 * CAUTION:
	 * This method will NOT retrieve the strings from cache memory, but
	 * will collect the strings from the database. You should keep this
	 * in mind wherever you use this function, for performance of your
	 * script.
	 * 
	 * Typically, this method is called to export the locale message
	 * catalog to a Portable Object file. The following example code
	 * illustrates how you would go about that:
	 * 
	 * <code>
	 *    $po = new M_LocalePortableObject;
	 *    $po->setMessageCatalog(M_LocaleMessageCatalog::getInstance('en'));
	 * </code>
	 * 
	 * {@link M_LocalePortableObject} will then ask for the collection 
	 * of strings that is contained in the message catalog, by calling
	 * {@link M_LocaleMessageCatalog::getIterator()}.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getIterator() {
		$out = array();
		
		// We compose the SQL Query that will provide us with the 
		// complete collection of untranslated strings, their
		// corresponding translations)
		$sql  = 'SELECT';
		$sql .=  ' u.id';
		$sql .= ', u.untranslated';
		$sql .= ', u.comments';
		$sql .= ', u.references';
		$sql .= ', u.plural';
		$sql .= ', t.nplural';
		$sql .= ', t.translated';
		$sql .= ' FROM locale_untranslated u';
		$sql .= ' LEFT JOIN locale_translated t';
		$sql .= ' ON u.id = t.id';
		$sql .= ' AND t.language = ?';
		$sql .= ' ORDER BY u.plural ASC';
		
		// We run the SQL Query
		$rs = M_Db::getInstance()->query($sql, $this->_locale);
		
		// If the result set is valid, and it contains at least 1 record
		if($rs !== FALSE && count($rs) > 0) {
			// For each of the strings in the result set:
			foreach($rs as $row) {
				// Get the ID of the current string in the database:
				$id = (int) $row['id'];
				
				// If the current string is a plural form of another 
				// string:
				if($row['plural'] > 0) {
					// Get the ID of the singular form of the string:
					$id = (int) $row['plural'];
					
					// We look for the singular form of the string. Note
					// that, if the singular cannot be found, we throw
					// an exception, informing about the corrupted message
					// catalog!
					if(!isset($out[$id])) {
						throw new M_LocaleException(sprintf(
							'%s: Cannot create iterator; catalog has been corrupted',
							__CLASS__
						));
					}
					
					// We set the untranslated plural form, and the
					// corresponding translation:
					$out[$id]->setUntranslatedPluralString($row['untranslated']);
					$out[$id]->setTranslatedPluralString((int) $row['nplural'], $row['translated']);
				}
				// If the current string is a regular string (in other
				// words: in singular form)
				else {
					$out[$id] = new M_LocaleMessage($row['untranslated']);
					$out[$id]->setTranslatedString($row['translated']);
					
					// Comments:
					if(!empty($row['comments'])) {
						$out[$id]->setComment(M_LocaleMessage::TRANSLATOR_COMMENT, $row['comments']);
					}
					
					if(!empty($row['references'])) {
						$out[$id]->setComment(M_LocaleMessage::REFERENCE, $row['references']);
					}
				}
			}
		}
		
		// Return the collection of translated strings
		return new ArrayIterator($out);
	}
	
	/**
	 * Set Portable Object
	 * 
	 * This method can be used to import a Portable Object into the
	 * message catalog. M_LocaleMessageCatalog will ask the Portable
	 * Object for its collection of translated strings (by calling the
	 * method {@link M_LocalePortableObject::getIterator()}), in order
	 * to import that collection into the message catalog.
	 * 
	 * Also, this method will copy the Plural Formula into the message
	 * catalog. Note that the plural formula will be parsed into a PHP
	 * Syntax Formula, to increase performance. For more info, read
	 * {@link M_LocaleMessageCatalog::getPluralFormula()}
	 * 
	 * @access public
	 * @param M_LocalePortableObject $po
	 * 		The Portable Object to be imported into the message catalog
	 * @return void
	 */
	public function setPortableObject(M_LocalePortableObject $po, $mode) {
		$strings = array();
		
		// Get the database connection 
		$db = M_Db::getInstance();
		
		// We compose the SQL Query that will provide us with the 
		// complete collection of untranslated strings (and their
		// corresponding translations, in the selected locale)
		$sql  = 'SELECT';
		$sql .=  ' u.id';
		$sql .= ', u.untranslated';
		$sql .= ' FROM locale_untranslated u';
		
		// Run the SQL
		$rs = $db->query($sql);
		
		// If the result set is valid, and it contains at least 1
		// record, we bind the untranslated strings to their corresponding
		// ID's in the database:
		if($rs !== FALSE && count($rs) > 0) {
			// For each of the translated strings
			foreach($rs as $row) {
				$strings[$row['untranslated']] = $row['id'];
			}
		}
		
		// Now, we ask the Portable Object for an iterator of the 
		// translated strings. This will provide us with a collection
		// of M_LocaleMessage objects:
		foreach($po->getIterator() as $string) {
			// Get the untranslated string:
			$untranslated = $string->getUntranslatedString();
			
			// If the untranslated string has not yet been registered
			// in the message catalog:
			if(!isset($strings[$untranslated])) {
				// We register the untranslated string now, and get the
				// ID of the string in the database:
				$id  = $this->_registerUntranslatedString($untranslated);
			}
			// If the untranslated string has already been registered:
			else {
				// Get the ID of the string in the database:
				$id = $strings[$untranslated];
				
				// Delete previous translated string:
				$db->queryOp('DELETE FROM locale_translated WHERE id = ? AND language = ?', $id, $this->_locale);
			}
			
			// Note that we only insert non-empty strings.
			$translated = $string->getTranslatedString();
			if(!empty($translated)) {
				// Run the query:
				$db->queryOp(
					'INSERT INTO locale_translated (id, language, translated) VALUES (?, ?, ?)',
					$id,
					$this->_locale,
					$translated
				);
			}
			
			// If the current string has a plural form:
			if($string->hasPluralForm()) {
				// Get the untranslated plural string:
				$untranslated = $string->getUntranslatedPluralString();
				
				// If the untranslated plural has not yet been registered
				// in the message catalog:
				if(!isset($strings[$untranslated])) {
					// We register the untranslated plural now, and get the
					// ID of the string in the database:
					$pId  = $this->_registerUntranslatedString($untranslated, $id);
				}
				// If the untranslated plural has already been registered
				// in the message catalog
				else {
					// Get the ID of the string in the database:
					$pId = $strings[$untranslated];
					
					// Delete previous translated strings:
					$db->queryOp('DELETE FROM locale_translated WHERE id = ? AND language = ?', $pId, $this->_locale);
				}
				
				// Note that we only insert non-empty strings.
				for($i = 0, $j = 1, $n = $string->getNumberOfPluralForms(); $i < $n; $i ++, $j ++) {
					$translated = $string->getTranslatedPluralString($j);
					if(!empty($translated)) {
						// Run the query:
						$db->queryOp(
							'INSERT INTO locale_translated (id, language, nplural, translated) VALUES (?, ?, ?, ?)',
							$pId,
							$this->_locale,
							$j,
							$translated
						);
					}
				}
			}
			// End if plural form
		}
		// End for each string
		
		// If the portable object comes with a definition of plural
		// forms:
		if($po->hasPluralFormula()) {
			// We also import the plural formula into the message catalog:
			$this->setNumberOfPluralForms($po->getNumberOfPluralForms());
			$this->setPluralFormulaInC($po->getPluralFormula());
		}
	}
	
	/**
	 * Has plural formula?
	 * 
	 * Read intro on plural forms at {@link M_LocaleMessageCatalog::getPluralText()}
	 * 
	 * The information about the plural form selection has to be stored in the 
	 * header entry of the PO file (the one with the empty msgid string). Read 
	 * more about PO files in the documentation of {@link M_LocalePortableObject}. 
	 * The plural form information looks like this:
	 * 
	 * <code>
	 *    Plural-Forms: nplurals=2; plural=n == 1 ? 0 : 1;
	 * </code>
	 * 
	 * The nplurals value must be a decimal number which specifies how many 
	 * different plural forms exist for this language. The string following 
	 * plural is an expression which is using the C language syntax. Exceptions 
	 * are that no negative numbers are allowed, numbers must be decimal, 
	 * and the only variable allowed is n. Spaces are allowed in the expression, 
	 * but backslash-newlines are not. This expression will be evaluated 
	 * whenever the method {@link M_LocaleMessageCatalog::getPluralText()} is
	 * called. The numeric value passed to that method is then substituted 
	 * for all uses of the variable n in the expression. The resulting value 
	 * then must be greater or equal to zero and smaller than the value given 
	 * as the value of nplurals.
	 * 
	 * This method will check if the locale's message catalog has a plural 
	 * formula. The return value is TRUE if the message catalog has a plural
	 * formula, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function hasPluralFormula() {
		return ($this->getPluralFormula() != NULL && $this->getNumberOfPluralForms() != NULL);
	}
	
	/**
	 * Get plural formula
	 * 
	 * This method will extract the formula from the information about the
	 * plural form selection. Learn more: {@link M_LocaleMessageCatalog::hasPluralFormula()}.
	 * Note that this method will parse the formula, and return the PHP version,
	 * which can be eval()'ed.
	 * 
	 * @access public
	 * @return string
	 */
	public function getPluralFormula() {
		if($this->_pluralFormula != NULL) {
			return $this->_pluralFormula;
		} else {
			$tmp = self::$_installed[$this->_locale]['plural_formula'];
			if($tmp) {
				return $tmp;
			} else {
				return NULL;
			}
		}
	}
	
	/**
	 * Get number of plural forms
	 * 
	 * This method will extract the value of nplurals from the information 
	 * about the plural form selection. Learn more: 
	 * {@link M_LocaleMessageCatalog::hasPluralFormula()}.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNumberOfPluralForms() {
		if($this->_numberOfPluralForms != NULL) {
			return $this->_numberOfPluralForms;
		} else {
			$tmp = self::$_installed[$this->_locale]['nplurals'];
			if($tmp) {
				return $tmp;
			} else {
				return NULL;
			}
		}
	}
	
	/**
	 * Do argument replacements in (translated) string
	 * 
	 * Example 1
	 * <code>
	 *    echo M_LocaleMessageCatalog::insertPlaceholderValues(
	 *       "Welcome, @username!",
	 *       array(
	 *          '@username' => 'Tom Bauwens'
	 *       )
	 *    );
	 * </code>
	 * 
	 * Example 1 will generate the following output:
	 * 
	 * <code>
	 *    Welcome, Tom Bauwens!
	 * </code>
	 * 
	 * This method is used to do placeholder replacements in the
	 * translated strings, by methods such as:
	 * 
	 * - {@link M_LocaleMessageCatalog::getText()}
	 * - {@link M_LocaleMessageCatalog::getPluralText()}
	 * 
	 * @access public
	 * @param string $string
	 * 		The string to do replacements in
	 * @param array $args
	 * 		String arguments. The keys of this array will be used 
	 * 		to do replacements in the original string.
	 * @return string
	 */
	public static function insertPlaceholderValues($string, array $args) {
		return strtr($string, $args);
	}
	
	/**
	 * 
	 * @see M_LocaleMessageCatalog::__destruct()
	 */
	public function setNumberOfPluralForms($number) {
		$this->_numberOfPluralForms = (int) $number;
	}
	
	/**
	 * 
	 * NOTE:
	 * Expects PHP Syntax
	 * 
	 * @see M_LocaleMessageCatalog::__destruct()
	 */
	public function setPluralFormula($formula) {
		// Remove ; at the end of the line
		$formula = rtrim($formula, ';');
		
		// The parsed formula will be saved to $out
		$out = '';
		$open = 0;
		
		// The current context is stored in $context
		$context = '';
		$ternary = '';
		
		// For each of the characters in the formula
		for($i = 0, $n = strlen($formula); $i < $n; $i ++) {
			// $char is the current character:
			$char = strtolower($formula{$i});
			
			// Check the current character in the formula:
			switch($char) {
				// Variable
				case '$':
					// The only available variable is $n, so we make 
					// sure that the next character is 'n'
					if(strtolower($formula{++ $i}) != 'n') {
						// If that's not the case, we throw an exception!
						throw new M_LocaleException(sprintf(
							'%s: Plural Formula: Only the $n variable is allowed!',
							__CLASS__
						));
					}
					// Add the variable to the formula:
					else {
						$out .= '$n ';
						$context = 'var';
					}
					break;
				
				// Comparison operators (the ones that only can come in
				// pairs!)
				case '=':
				case '!':
					// The comparison operator is allowed after a variable
					// or a value (integer).
					if($context == 'var' || $context == 'int') {
						// The next character must be =, for this to be a
						// valid comparison operator
						if(strtolower($formula{++ $i}) == '=') {
							$out .= $char . '= ';
							$context = 'com';
						}
						// If the comparison editor is not complete
						else {
							throw new M_LocaleException(sprintf(
								'%s: Plural Formula: Incomplete comparison operator "%s"',
								__CLASS__,
								$char
							));
						}
					}
					// If the comparison operator is being used in the wrong
					// context:
					else {
						// If that's not the case, we throw an exception!
						throw new M_LocaleException(sprintf(
							'%s: Plural Formula: Unexpected comparison operator "%s"',
							__CLASS__,
							$char
						));
					}
					break;
				
				// Comparison operators (OPTIONALLY in pairs!)
				case '>':
				case '<':
					// The comparison operator is allowed after a variable
					// or a value (integer).
					if($context == 'var' || $context == 'int') {
						// The next character can be =
						if(strtolower($formula{++ $i}) == '=') {
							$out .= $char . '=';
							$context = 'com';
						}
						// If the comparison operator is one character
						// only:
						else {
							$out .= $char;
							$context = 'com';
							$i -= 1;
						}
					}
					// If the comparison editor is being used in the wrong
					// context:
					else {
						// If that's not the case, we throw an exception!
						throw new M_LocaleException(sprintf(
							'%s: Plural Formula: Unexpected comparison operator "%s"',
							__CLASS__,
							$char
						));
					}
					break;
				
				// Logical operators (eg && ||)
				case '&':
				case '|':
					// The logical operator is allowed after a variable
					// or a value (integer).
					if($context == 'var' || $context == 'int') {
						// The next character must be the same, for this 
						// to be a valid logical operator
						if(strtolower($formula{++ $i}) == $char) {
							$out .= $char . $char . ' ';
							$context = 'log';
						}
						// If the comparison editor is not complete
						else {
							throw new M_LocaleException(sprintf(
								'%s: Plural Formula: Incomplete logical operator "%s"',
								__CLASS__,
								$char
							));
						}
					}
					// If the comparison operator is being used in the wrong
					// context:
					else {
						// If that's not the case, we throw an exception!
						throw new M_LocaleException(sprintf(
							'%s: Plural Formula: Unexpected logical operator "%s"',
							__CLASS__,
							$char
						));
					}
					break;
				
				// Numerical values
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					// Numerical values are allowed in comparison context,
					// or as a result in ternary condition. (Note that 
					// we also allow the formula to start with numerical 
					// values: $context == '')
					if($context == '' || $context == 'com' || $context == 'int' || $context == 'ter') {
						$out .= $char;
						$context = 'int';
					}
					// If the numeric value is not being used in the
					// right context:
					else {
						// If that's not the case, we throw an exception!
						throw new M_LocaleException(sprintf(
							'%s: Plural Formula: Unexpected numerical value',
							__CLASS__
						));
					}
					break;
				
				// Ternary Condition operator ?
				case '?':
					// '?' can only be used after variable or value
					// context! Also, it should not be used directly in
					// ternary context
					if(($context == 'var' || $context == 'int') && $ternary == '') {
						$out .= ' ? (';
						$context = 'ter';
						$ternary = '?';
					}
					// If the ternary operator is not being used in the
					// correct context
					else {
						throw new M_LocaleException(sprintf(
							'%s: Plural Formula: Unexpected ternary operator "?"',
							__CLASS__
						));
					}
					break;
				
				// Ternary Condition operator :
				case ':':
					// ':' Can only come after ternary context '?', and
					// should not be used directly in ternary context
					if($context != 'ter' && $ternary == '?') {
						$out .= ') : (';
						$context = 'ter';
						$ternary = '';
						$open += 1;
					}
					// If the ternary context is not correct, we throw
					// an exception!
					else {
						throw new M_LocaleException(sprintf(
							'%s: Plural Formula: Unexpected ternary operator ":"',
							__CLASS__
						));
					}
					break;
				
				// Grouping
				case '(':
				case ')':
					$out .= $char;
					break;
				
				// white-space characters:
				case ' ':
					// We profit from the loop to eliminate redundant
					// white-spaces from the formula:
					break;
				
				// We refuse all other characters!
				// (we wouldn't want the plural formula to become a
				// Remote Executable Code Injection vector)
				default:
					throw new M_LocaleException(sprintf(
						'%s: Plural Formula: Unrecognized operator: %s',
						__CLASS__,
						$char
					));
			}
		}
		
		// Double-check: Ternary conditions should have been closed!
		if($ternary == '?') {
			throw new M_LocaleException(sprintf(
				'%s: Plural Formula: Ternary condition has not been closed; Missing ":" block',
				__CLASS__
			));
		}
		
		// Close opened groups:
		for($i = 0; $i < $open; $i ++) {
			$out .= ')';
		}
		
		// No need to group integer values
		$out = preg_replace('/\(([0-9]+)\)/', '$1', $out);
		
		// Test the formula :)
		$n = 0;
		if(@eval('return (int)(' . $out.');')) {
			$this->_pluralFormula = $out;
		} else {
			throw new M_LocaleException(sprintf(
				'%s: Plural Formula: Parse Error',
				__CLASS__
			));
		}
		
		// Set the parsed formula as object property. This will trigger
		// an SQL Update Query at the end of page request!
		$this->_pluralFormula = $out;
	}
	
	/**
	 * 
	 */
	public function setPluralFormulaInC($formula) {
		$this->setPluralFormula(str_replace('n', '$n', strtolower($formula)));
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Get installed catalogs
	 * 
	 * Will provide with the collection of installed message catalogs. Used by 
	 * 
	 * - {@link M_LocaleMessageCatalog::getInstalled()}
	 * - {@link M_LocaleMessageCatalog::isInstalled()}
	 * - {@link M_LocaleMessageCatalog::getInstance()}
	 * 
	 * to check for supported message catalogs.
	 * 
	 * @access private
	 * @return array
	 */
	private static function _getInstalledCatalogs() {
		// If not requested before:
		if(self::$_installed == NULL) {
			// Initiate the collection of catalogs:
			self::$_installed = array();
			
			// Select the catalogs from the db
			try {
				$select = M_Db::getInstance()->select('locale')->order('priority','DESC');
				$rs = $select->execute();
				
				// populate the collection:
				if($rs !== FALSE && count($rs) > 0) {
					foreach($rs as $row) {
						self::$_installed[$row['locale']] = $row;
					}
				}
			}
			// If an error occurred, we assume that the database table does not
			// yet exist. We create the database table now:
			catch(Exception $e) {
				self::_installDbTableLocale();
			}
		}
		
		// return the collection of supported locales:
		return self::$_installed;
	}
	
	/**
	 * Initiate collection of translated strings
	 * 
	 * Will initiate the collection of translated strings, from the message catalog
	 * database. Will first try to fetch the translated strings from cache memory:
	 * 
	 * - {@link M_LocaleMessageCatalog::setCacheObject()}
	 * - {@link M_LocaleMessageCatalog::getCacheObject()}
	 * 
	 * @access private
	 * @return void
	 */
	private function _initStringsCollection() {
		// If not requested before:
		if($this->_strings == NULL) {
			// Before fetching the translated strings from the message catalog database,
			// we check if the translated strings are already available in cache memory:
			$cache = self::getCacheObject();
			$str   = FALSE;
			if($cache) {
				$str = $cache->read('locale.' . $this->_locale);
			}
			
			// If the strings are not available in cache memory, we will have to fetch
			// them from the message catalog database:
			if(!$str) {
				// Initiate the empty array now, which will be populated with translations.
				$this->_strings = array();
				
				// Compose the SQL to fetch the translated strings from the database. Note
				// that we add the language comparison to the JOIN statement, so that NULL
				// records (untranslated strings that have not been translated) are included
				// in the result set.
				$sql  = 'SELECT';
				$sql .=  ' t.translated';
				$sql .= ', u.untranslated';
				$sql .= ', t.nplural';
				$sql .= ' FROM locale_untranslated u';
				$sql .= ' LEFT JOIN locale_translated t';
				$sql .= ' ON u.id = t.id';
				$sql .= ' AND t.language = ?';
				
				// Try to run the SQL
				try {
					$rs = M_Db::getInstance()->query($sql, $this->_locale);
					
					// If the result set is valid:
					if($rs !== FALSE && count($rs) > 0) {
						// For each of the translated strings we receive from the
						// database query result set:
						foreach($rs as $row) {
							// Note that UNtranslated strings are set to (boolean) 
							// TRUE. This way, we know that the untranslated string 
							// has been registered in the message catalog database, 
							// even though a translation is not yet available.
							if(empty($row['translated'])) {
								// translation not available:
								$this->_strings[$row['untranslated']] = TRUE;
							} else {
								if($row['nplural'] > 0) {
									// Plural strings are represented in an array. The correct plural form
									// is fetched from this array, using the plural formula of the message
									// catalog. The plural formula returns the numerical index of this array:
									if(!isset($str[$row['untranslated']])) {
										$str[$row['untranslated']] = array();
									}
									
									$this->_strings[$row['untranslated']][$row['nplural']] = $row['translated'];
								}
								else {
									$this->_strings[$row['untranslated']] = $row['translated'];
								}
							}
						}
					}
				}
				// If an error occurred, we assume that the database tables do not
				// yet exist. We create the database tables now:
				catch(Exception $e) {
					$this->_installDbTableLocaleTranslatedStrings();
					$this->_installDbTableLocaleUntranslatedStrings();
				}
				
				// Now we have populated the message catalog with translated strings. In order to
				// speed up future requests from the message catalog, we cache the array of
				// translated strings.
				if($cache) {
					$cache->write('locale.' . $this->_locale, $this->_strings);
				}
			}
			// If the translated strings are still in cache memory:
			else {
				$this->_strings = $str;
				$this->_stringsAreFromCache = TRUE;
			}
		}
	}
	
	/**
	 * Register an untranslated string
	 * 
	 * This method will create an entry for an untranslated string in 
	 * the message catalog. This method does NOT create the corresponding 
	 * translation(s) of the untranslated string.
	 * 
	 * This implementation of M_LocaleMessageCatalog is database-powered. 
	 * This private method will return the ID of the database entry 
	 * that has been created for the untranslated string.
	 * 
	 * NOTE:
	 * This method will remove the message catalog that has been stored 
	 * in cache. To learn more on how to use cache in the catalog, read 
	 * the docs on {@link M_LocaleMessageCatalog::setCacheObject()}.
	 * 
	 * @access private
	 * @param string $msgid
	 * 		The untranslated string
	 * @return string
	 */
	private function _registerUntranslatedString($string, $pluralOf = NULL, $comments = '', $references = '') {
		// Get the database where the catalog is being stored:
		$db = M_Db::getInstance();
		
		// Compose the SQL to register the untranslated string
		$sql  = 'INSERT INTO locale_untranslated';
		$sql .= ' (';
		$sql .=    $db->quoteIdentifier('untranslated') .', ';
		$sql .=    $db->quoteIdentifier('references') .', ';
		$sql .=    $db->quoteIdentifier('comments');
		if($pluralOf) {
			$sql .= ', ' . $db->quoteIdentifier('plural');
		}
		$sql .= ') VALUES (?, ?, ?';
		if($pluralOf) {
			$sql .= ', ?';
		}
		$sql .= ')';
		
		// Compose the collection of arguments for the query
		if($pluralOf) {
			$db->queryOp($sql, $string, $references, $comments, $pluralOf);
		} else {
			$db->queryOp($sql, $string, $references, $comments);
		}
		
		// For now, we set the value of the untranslated string to 
		// TRUE, so we know that the untranslated string has been 
		// registered. At the same time, it indicates that no 
		// translation is available for the string.
		$this->_strings[$string] = TRUE;
		
		// We remove the message catalog from cache memory. This 
		// will cause the message catalog to rebuild the cache
		// at a later request.
		$cache = self::getCacheObject();
		if($cache) {
			$cache->remove('locale.' . $this->_locale);
		}
		
		// We return the ID of the new entry:
		return $db->getLastInsertId();
	}
	
	/**
	 * Install database table (first run)
	 * 
	 * When {@link M_LocaleMessageCatalog} is being used for the first time, the 
	 * database table is installed automatically. This method is in charge of 
	 * installing the database table that stores supported locales.
	 * 
	 * @access private
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	private static function _installDbTableLocale() {
		// Get the database where the catalog should be stored:
		$db = M_Db::getInstance();
		
		// Ask the database connection for a new table
		$table = $db->getNewTable();
		$table->setName('locale');
		
		// Create the fields in the new table:
		$columnLocale = new M_DbColumn('locale');
		$columnLocale->setType(M_DbColumn::TYPE_VARCHAR);
		$columnLocale->setLength(8);
		$table->addColumn($columnLocale);
		
		$columnNplurals = new M_DbColumn('nplurals');
		$columnNplurals->setType(M_DbColumn::TYPE_INTEGER);
		$columnNplurals->setLength(11);
		$table->addColumn($columnNplurals);
		   
		$columnNpluralFormula = new M_DbColumn('plural_formula');
		$columnNpluralFormula->setType(M_DbColumn::TYPE_VARCHAR);
		$columnNpluralFormula->setLength(64);
		$table->addColumn($columnNpluralFormula);
		
		$columnPriority = new M_DbColumn('priority');
		$columnPriority->setType(M_DbColumn::TYPE_TINY_INTEGER);
		$columnPriority->setLength(2);
		$table->addColumn($columnPriority);

		$indexPrimary = new M_DbIndex('primary');
		$indexPrimary->addColumn($columnLocale);
		$indexPrimary->setType(M_DbIndex::PRIMARY);
		
		$table->setPrimaryKey($indexPrimary);
		$table->setComments('Installed locale message catalogs');
		$table->create();
	}
	
	/**
	 * Install database table (first run)
	 * 
	 * When {@link M_LocaleMessageCatalog} is being used for the first time, the 
	 * database table is installed automatically. This method is in charge of 
	 * installing the database table that stores translated strings.
	 * 
	 * @access private
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	private function _installDbTableLocaleTranslatedStrings() {
		// Get the database where the catalog should be stored:
		$db = M_Db::getInstance();
		
		// Ask the database connection for a new table
		$table = $db->getNewTable();
		$table->setName('locale_translated');
		
		// Create the fields in the new table:
		$columnId = new M_DbColumn('id');
		$columnId->setType(M_DbColumn::TYPE_INTEGER);
		$columnId->setLength(11);
		$table->addColumn($columnId);
		
		$columnLanguage = new M_DbColumn('language');
		$columnLanguage->setType(M_DbColumn::TYPE_VARCHAR);
		$columnLanguage->setLength(12);
		$table->addColumn($columnLanguage);
		
		$columnNplural = new M_DbColumn('nplural');
		$columnNplural->setType(M_DbColumn::TYPE_INTEGER);
		$columnNplural->setLength(11);
		$columnNplural->setComments('The plural n (N value returned by the plural formula).');
		$table->addColumn($columnNplural);
		
		$columnTranslated = new M_DbColumn('translated');
		$columnTranslated->setType(M_DbColumn::TYPE_TEXT);
		$table->addColumn($columnTranslated);
		
		$indexPrimary = new M_DbIndex('primary');
		$indexPrimary->addColumn($columnId);
		$indexPrimary->addColumn($columnLanguage);
		$indexPrimary->addColumn($columnNplural);
		$indexPrimary->setType(M_DbIndex::PRIMARY);
		
		$table->setPrimaryKey($indexPrimary);
		$table->setComments('Translated locale date');
		$table->create();
	}
	
	/**
	 * Install database table (first run)
	 * 
	 * When {@link M_LocaleMessageCatalog} is being used for the first time, the 
	 * database table is installed automatically. This method is in charge of 
	 * installing the database table that stores untranslated strings.
	 * 
	 * @access private
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	private function _installDbTableLocaleUntranslatedStrings() {
		// Get the database where the catalog should be stored:
		$db = M_Db::getInstance();
		
		// Ask the database connection for a new table
		$table = $db->getNewTable();
		$table->setName('locale_untranslated');
		
		// Create the fields in the new table:
		$columnId = new M_DbColumn('id');
		$columnId->setType(M_DbColumn::TYPE_INTEGER);
		$columnId->setLength(11);
		$columnId->setIsAutoIncrement(true);
		$table->addColumn($columnId);
		
		$columnRefs = new M_DbColumn('references');
		$columnRefs->setType(M_DbColumn::TYPE_VARCHAR);
		$columnRefs->setLength(255);
		$table->addColumn($columnRefs);
		
		$columnUntranslated = new M_DbColumn('untranslated');
		$columnUntranslated->setType(M_DbColumn::TYPE_TEXT);
		$table->addColumn($columnUntranslated);
		
		$columnComments = new M_DbColumn('comments');
		$columnComments->setType(M_DbColumn::TYPE_VARCHAR);
		$columnComments->setLength(255);
		$table->addColumn($columnComments);
		
		$columnPlural = new M_DbColumn('plural');
		$columnPlural->setType(M_DbColumn::TYPE_INTEGER);
		$columnPlural->setLength(11);
		$table->addColumn($columnPlural);
		
		$indexPrimary = new M_DbIndex('primary');
		$indexPrimary->addColumn($columnId);
		$indexPrimary->setType(M_DbIndex::PRIMARY);
		
		$indexPlural = new M_DbIndex('plural');
		$indexPlural->addColumn($columnPlural);
		$indexPlural->setType(M_DbIndex::INDEX);
		
		$table->setPrimaryKey($indexPrimary);
		$table->addIndex($indexPlural);
		$table->setComments('Untranslated locale date');
		$table->create();
	}
}

/**
 * Shortcut for {@link M_LocaleMessageCatalog::getText()}
 * 
 * Typically, the gettext specification includes a function
 * named _ (underscore) to remove code clutter.
 * 
 * The locale will be defaulted to the locale that has been set 
 * in the locale category LC_MESSAGES (see {@link M_Locale::getCategory()} 
 * for more info)
 * 
 * @param string $string
 * 		The untranslated string
 * @param array $args
 * 		String arguments. The keys of this array will be used to do 
 * 		replacements in the translated string: 
 * 		{@link M_LocaleMessageCatalog::insertPlaceholderValues()}
 * @return string
 */
function t($string, array $args = array()) {
	$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
	if(!$locale) {
		return M_LocaleMessageCatalog::insertPlaceholderValues($string, $args);
	} else {
		$catalog = M_LocaleMessageCatalog::getInstance($locale);
		return $catalog->getText($string, $args);
	}
}

/**
 * Shortcut for {@link M_LocaleMessageCatalog::getPluralText()}
 * 
 * Typically, the gettext specification includes a function
 * named _ (underscore) to remove code clutter.
 * 
 * The locale will be defaulted to the locale that has been set 
 * in the locale category LC_MESSAGES (see {@link M_Locale::getCategory()} 
 * for more info)
 * 
 * @param string $string
 * 		The untranslated string in singular form
 * @param string $string
 * 		The untranslated string in plural form
 * @param int $n
 *		The number on which the locale decides to show plural or singular form
 * @param array $args
 * 		String arguments. The keys of this array will be used to do 
 * 		replacements in the translated string: 
 * 		{@link M_LocaleMessageCatalog::insertPlaceholderValues()}
 * @return string
 */
function p($msgid1, $msgid2, $n, array $args = array()) {
	$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
	if(!$locale) {
		if($n == 1) {
			return M_LocaleMessageCatalog::insertPlaceholderValues($msgid1, $args);
		} else {
			$args['@count'] = $n;
			return M_LocaleMessageCatalog::insertPlaceholderValues($msgid2, $args);
		}
	} else {
		$catalog = M_LocaleMessageCatalog::getInstance($locale);
		return $catalog->getPluralText($msgid1, $msgid2, $n, $args);
	}
}
?>