<?php
/**
 * M_LocaleStopwordsCatalog class
 * 
 * This class is used to retrieve stopword-strings from the locale's stopword 
 * catalog. Using this list one can e.g.
 * remove stopwords (words that are mostly meaningless).
 * 
 * @package Core
 */
class M_LocaleStopwordsCatalog {
	
	/**
	 * The singleton instances
	 * 
	 * A singleton object is created per locale. This property
	 * stores the constructed singleton objects.
	 *
	 * @access private
	 * @var array
	 */
	private static $_instances = array();
	
	/**
	 * The installed locales
	 * 
	 * This property stores information about the locales for which
	 * a catalog has been installed.
	 *
	 * @access private
	 * @var array
	 */
	private static $_installed = NULL;
	
	/**
	 * (Private) Constructor
	 * 
	 * The signature on the M_LocaleStopwordsCatalog class constructor 
	 * is PRIVATE. This way, we force the M_LocaleStopwordsCatalog object 
	 * to be a singleton (per locale), as objects can only be 
	 * constructed by the class itself.
	 * 
	 * In the public interface, the constructor is:
	 * 
	 * <code>
	 *    $catalog = M_LocaleStopwordsCatalog::getInstance();
	 * </code>
	 *
	 * @access private
	 * @param string $locale
	 * 		The locale name
	 * @return M_LocaleStopwordsCatalog
	 */
	private function __construct($locale) {
		$this->_locale = $locale;
	}
	
	/**
	 * Holds the list of stopwords for this locale
	 *
	 * @var ArrayIterator
	 */
	private static $_iterator;
	
	/**
	 * Singleton constructor
	 * 
	 * NOTE:
	 * If the locale is not provided, {@link M_LocaleStopwordsCatalog}
	 * will use the locale name that has been set in the locale category
	 * {@link M_Locale::LC_MESSAGES}. For more info about locale categories,
	 * read the docs on {@link M_Locale::getCategory()}.
	 * 
	 * @see M_LocaleStopwordsCatalog::__construct()
	 * @access public
	 * @param string $locale
	 * 		The locale name
	 * @return M_LocaleStopwordsCatalog
	 */
	public static function getInstance($locale = NULL) {
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
		}
		if(!isset(self::$_instances[$locale])) {
			if(self::isInstalled($locale)) {
				self::$_instances[$locale] = new self($locale);
			} else {
				throw new M_LocaleException(sprintf(
					'%s: Unsupported locale "%s"',
					__CLASS__,
					$locale
				));
			}
		}
		return self::$_instances[$locale];
	}
	
	/**
	 * Is locale installed?
	 * 
	 * This method will use {@link M_LocaleStopwordsCatalog::getInstalled()}
	 * to check if the requested locale is installed.
	 * 
	 * NOTE:
	 * This method will return (boolean) TRUE if the locale has been
	 * installed, FALSE if not.
	 * 
	 * @static
	 * @access public
	 * @param string $locale
	 * @return boolean
	 */
	public static function isInstalled($locale) {
		return in_array($locale, self::getInstalled());
	}
	
	/**
	 * Get installed locales
	 * 
	 * This method will return the collection of locales for which a 
	 * message catalog has been installed. For more information on
	 * locale names, read {@link M_Locale::getNameParts()}.
	 * 
	 * @static
	 * @access public
	 * @return array
	 */
	public static function getInstalled() {
		return array_keys(self::_getInstalledCatalogs());
	}
	
	/**
	 * Get locale name
	 * 
	 * This method will return the locale to which the Stopwords Catalog
	 * belongs. The M_LocaleStopwordsCatalog has been constructed with 
	 * this locale name: see {@link M_LocaleStopwordsCatalog::getInstance()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getLocaleName() {
		return $this->_locale;
	}
	
	/**
	 * Get Iterator
	 * 
	 * Typically, this method is called to get a list of available stopwords for
	 * the active locale.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getIterator() {
		if (!is_null(self::$_iterator)) return self::$_iterator;
		$out = array();
		
		$file = self::_getCatalogFile($this->_locale);
		if (($handle = fopen($file->getPath(), "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		        $out[] = $data[0];
		    }
	    	fclose($handle);
		}	
		
		// Return the collection of translated strings
		return new ArrayIterator($out);
	}
	
	
	
	/* -- PRIVATE -- */
	
	/**
	 * Get installed catalogs
	 * 
	 * Will provide with the collection of installed stopwords catalogs. Used by 
	 * 
	 * - {@link M_LocaleStopwordsCatalog::getInstalled()}
	 * - {@link M_LocaleStopwordsCatalog::isInstalled()}
	 * - {@link M_LocaleStopwordsCatalog::getInstance()}
	 * 
	 * to check for supported stopwords catalogs.
	 * 
	 * @access private
	 * @return array
	 */
	private function _getInstalledCatalogs() {
		// If not requested before:
		if(self::$_installed == NULL) {
			// Initiate the collection of catalogs:
			self::$_installed = array();
			
			// Select the catalogs from the db
			try {
				$select = M_Db::getInstance()->select('locale');
				$rs = $select->execute();
				
				// populate the collection:
				if($rs !== FALSE && count($rs) > 0) {
					foreach($rs as $row) {
						//check if the stopwords file exists
						if (self::_getCatalogFile($row['locale'])->exists()) {
							self::$_installed[$row['locale']] = $row;
						}
					}
				}
			}
			// If an error occurred, we assume that the database table does not
			// yet exist. We create the database table now:
			catch(Exception $e) {
				self::_installDbTableLocale();
			}
		}
		
		// return the collection of supported locales:
		return self::$_installed;
	}
	
	
	
	/**
	 * Get the directory where stopword catalogs are stored
	 *
	 * @return M_Directory
	 */
	private function _getCatalogDirectory() {
		return new M_Directory(M_Loader::getRelative(
			'core/'.FOLDER_THIRDPARTY.'/localestopwords')
		);
	}
	
	/**
	 * Get the file which holds the stopwords
	 *
	 * @param string $locale
	 * @return M_File
	 */
	private function _getCatalogFile($locale) {
		return new M_File(
			self::_getCatalogDirectory()->getPath().'/'.$locale.'.csv'
		);
	}
}