<?php
/**
 * M_LocaleData class
 * 
 * M_LocaleData stores the cultural conventions of a given locale. This data 
 * includes the format of dates, time, numbers, VAT numbers, etc.
 * 
 * M_LocaleData is used by locale-dependent classes, such as {@link M_Date},
 * and {@link M_Number}, in order to get data and properties that are specific
 * to a given locale.
 * 
 * @package Core
 */
class M_LocaleData {
	/**
	 * The singleton instances
	 * 
	 * A singleton object is created per locale. This property
	 * stores the constructed singleton objects.
	 *
	 * @access private
	 * @var array
	 */
	private static $_instances = array();
	
	/**
	 * The cache object
	 * 
	 * A cache object is attached to the M_LocaleData class (statically,
	 * not per object), which is in charge of reading and writing the
	 * stored cache of locale data.
	 * 
	 * @access private
	 * @var string
	 */
	private static $_cache = NULL;
	
	/**
	 * The locale
	 * 
	 * This property holds the locale name represented by the
	 * M_LocaleData object.
	 *
	 * @access private
	 * @var string
	 */
	private $_locale;
	
	/**
	 * The locale data attributes
	 * 
	 * This property holds the attributes of the locale data.
	 *
	 * @access private
	 * @var array
	 */
	private $_data = array();
	
	/**
	 * (Private) Constructor
	 * 
	 * The signature on the M_LocaleData class constructor is PRIVATE.
	 * This way, we force the M_LocaleData object to be a singleton
	 * (per locale), as objects can only be constructed by the class
	 * itself. In the public interface, the constructor is the
	 * {@link M_LocaleData::getInstance()} method.
	 *
	 * @access private
	 * @param string $locale
	 * 		The locale name
	 * @return M_LocaleData
	 */
	private function __construct($locale) {
		$this->_locale = $locale;
	}
	
	/**
	 * Singleton constructor
	 * 
	 * Since the M_LocaleData constructor is private, objects can 
	 * only be constructed by the M_LocaleData class itself. This 
	 * method will check if an object has been constructed before. 
	 * If so, it will return that object, if not, it will 
	 * construct the singleton object.
	 * 
	 * NOTE:
	 * If the locale is not provided, {@link M_LocaleMessageCatalog}
	 * will use the locale name that has been set in the locale category
	 * {@link M_Locale::LANG}. For more info about locale categories,
	 * read the docs on {@link M_Locale::getCategory()}.
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale name
	 * @return M_LocaleData
	 */
	public static function getInstance($locale = NULL) {
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LANG);
		}
		if(!isset(self::$_instances[$locale])) {
			self::$_instances[$locale] = new self($locale);
		}
		return self::$_instances[$locale];
	}
	
	/**
	 * Set cache object
	 * 
	 * A {@link M_CacheMemory} object is attached to the M_LocaleData class
	 * (statically, not per object), which is in charge of reading and 
	 * writing the stored cache of locale data.
	 * 
	 * By attaching an {@link M_CacheMemory} object through the public API,
	 * we promote "polymorphism" (expensive word for flexibility in how
	 * cache is handled). This way, cache may be stored in the database,
	 * or in PHP cache files. The M_LocaleData class does not need to worry
	 * about which type of cache is being used, it simply talks to the
	 * cache object through its public interface.
	 * 
	 * Note that, if you do not provide the M_LocaleData class with a 
	 * cache object, the M_LocaleData class will not try to fetch the data
	 * from cache, nor will it try to store in cache. If you prefer not
	 * to store the locale data in cache memory, you can use the 
	 * {@link M_LocaleData::removeCacheObject()} method do remove the 
	 * previously attached cache object.
	 * 
	 * @access public
	 * @param M_CacheMemory $cache
	 * 		The constructed cache object
	 * @return void
	 */
	public function setCacheObject(M_CacheMemory $cache) {
		self::$_cache = $cache;
	}
	
	/**
	 * Get cache object
	 * 
	 * This method will return the {@link M_CacheMemory} object that has
	 * been attached previously with {@link M_LocaleData::setCacheObject()}.
	 *
	 * NOTE:
	 * Note that, if no cache object has been set before, this method
	 * will return NULL.
	 * 
	 * @access public
	 * @return CacheMemory|NULL
	 */
	public function getCacheObject() {
		return self::$_cache;
	}
	
	/**
	 * Remove cache object
	 * 
	 * This method will remove the cache object, previously attached
	 * to the class with the {@link M_LocaleData::setCacheObject()} 
	 * method.
	 *
	 * NOTE:
	 * Note that, if you do not provide the M_LocaleData class with a 
	 * cache object, the M_LocaleData class will not try to fetch the 
	 * data from cache, nor will it try to store in cache.
	 * 
	 * @access public
	 * @return void
	 */
	public function removeCacheObject() {
		self::$_cache = NULL;
	}
	
	/**
	 * Get date format
	 * 
	 * This method will return a string that describes the format
	 * in which dates should be rendered in the locale that is 
	 * represented by the M_LocaleData object. Note that you can 
	 * request various types of formats from this method:
	 * 
	 * - full
	 * - long
	 * - medium
	 * - short
	 * 
	 * Example 1, medium date format string in Dutch
	 * <code>
	 *    $data = new M_LocaleData('nl');
	 *    echo $data->getDateFormat('medium');
	 * </code>
	 * 
	 * Example 1 would output the following string that describes the
	 * date format in the requested locale.
	 * 
	 * <code>
	 *    d MMM yyyy
	 * </code>
	 * 
	 * Extracted from CLDR specifications:
	 * 
	 * The Date Field Symbol Table illustrated at
	 * 
	 * http://www.unicode.org/reports/tr35/#Date_Field_Symbol_Table
	 * 
	 * contains the characters used in patterns to show the appropriate 
	 * formats for a given locale, such as yyyy for the year. Characters 
	 * may be used multiple times. For example, if y is used for the year, 
	 * 'yy' might produce '99', whereas 'yyyy' produces '1999'. For most 
	 * numerical fields, the number of characters specifies the field width. 
	 * For example, if h is the hour, 'h' might produce '5', but 'hh' 
	 * produces '05'. For some characters, the count specifies whether an 
	 * abbreviated or full form should be used, but may have other choices, 
	 * as given below.
	 * 
	 * Two single quotes represents a literal single quote, either 
	 * inside or outside single quotes. Text within single quotes is 
	 * not interpreted in any way (except for two adjacent single quotes).
	 * Otherwise all ASCII letter from a to z and A to Z are reserved as 
	 * syntax characters, and require quoting if they are to represent 
	 * literal characters. In addition, certain ASCII punctuation 
	 * characters may become variable in the future (eg ":" being 
	 * interpreted as the time separator and '/' as a date separator, 
	 * and replaced by respective locale-sensitive characters in display).
	 * 
	 * Example 2, illustration of quotes in a data format string, or
	 * date pattern:
	 * 
	 * <code>
	 *    Pattern  	               | Result (in a particular locale)
	 *    -------------------------|------------------------------------
	 *    hh 'o''clock' a, zzzz    | 12 o'clock PM, Pacific Daylight Time
	 * </code>
	 * 
	 * @access public
	 * @param string $type
	 * 		The type of format. Possible values: 'short', 'medium', 'long', 'full'
	 * @return string
	 */
	public function getDateFormat($format = 'medium') {
		if(!isset($this->_data['calendar'])) {
			$this->_loadData('calendar');
		}
		
		return $this->_data['calendar']['date'][$format];
	}
	
	/**
	 * Get time format
	 * 
	 * Will provide with the time format. For more info, read the docs on
	 * {@link M_LocaleData::getDateFormat()}
	 * 
	 * @access public
	 * @param string $type
	 * 		The type of format. Possible values: 'short', 'medium', 'long', 'full'
	 * @return string
	 */
	public function getTimeFormat($format = 'medium') {
		if(!isset($this->_data['calendar'])) {
			$this->_loadData('calendar');
		}
		
		return $this->_data['calendar']['time'][$format];
	}
	
	/**
	 * Get calendar field
	 * 
	 * This method will return the display name of a calendar field. 
	 * The following calendar fields can be requested from this method:
	 * 
	 * <code>
	 *    era
	 *    year
	 *    month
	 *    week
	 *    day
	 *    weekday
	 *    dayperiod
	 *    hour
	 *    minute
	 *    second
	 *    zone
	 * </code>
	 * 
	 * For example, if you wanted to refer to the calendar field "day"
	 * in dutch, you would write the following code:
	 * 
	 * <code>
	 *    $data = M_LocaleData::getInstance('nl');
	 *    echo $data->getCalendarFieldName('day'); // outputs "Dag"
	 * </code>
	 * 
	 * Also, you might want to get the relative display name of the
	 * calendar field. For example, instead of asking for the display
	 * name of "day", you may want to get the display name of 
	 * "yesterday". To get the relative display name, you can add a
	 * numeric argument to this method call. Consider the following
	 * examples:
	 * 
	 * <code>
	 *    $data = M_LocaleData::getInstance('nl');
	 *    echo $data->getCalendarFieldName('day', -1); // outputs "Gisteren"
	 *    echo $data->getCalendarFieldName('day', 0);  // outputs "Vandaag"
	 *    echo $data->getCalendarFieldName('day', 1);  // outputs "Morgen"
	 * </code>
	 * 
	 * IMPORTANTE NOTE:
	 * If the display name could not have been found, this method will
	 * return (boolean) FALSE instead!
	 * 
	 * @access public
	 * @param string $field
	 * 		The calendar field you want to retrieve the display name of
	 * @param integer $relative
	 * 		The relative index
	 * @return string|boolean
	 */
	public function getCalendarFieldName($field, $relative = NULL) {
		if(!isset($this->_data['calendar'])) {
			$this->_loadData('calendar');
		}
		if(isset($this->_data['calendar']['fields'][$field])) {
			if(is_numeric($relative)) {
				if(is_array($this->_data['calendar']['fields'][$field]) && isset($this->_data['calendar']['fields'][$field]['relative'][$relative])) {
					return $this->_data['calendar']['fields'][$field]['relative'][$relative];
				} else {
					return FALSE;
				}
			} else {
				if(is_array($this->_data['calendar']['fields'][$field])) {
					return (isset($this->_data['calendar']['fields'][$field]['displayName'])
						? $this->_data['calendar']['fields'][$field]['displayName']
						: FALSE
					);
				} else {
					return $this->_data['calendar']['fields'][$field];
				}
			}
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get month's display name
	 * 
	 * This method will return the display name of a given month (1 - 12).
	 * Note that you can request the display name in various formats. The
	 * possible formats are:
	 * 
	 * <code>
	 *    narrow
	 *    abbreviated
	 *    wide
	 * </code>
	 * 
	 * The format will be defaulted to "abbreviated", if not provided to
	 * this method.
	 * 
	 * @access public
	 * @param integer $month
	 * 		The month for which to fetch the display name
	 * @param string $format
	 * 		The format in which the display name should be fetched
	 * @return string
	 */
	public function getMonth($month, $format = NULL) {
		if(!isset($this->_data['calendar'])) {
			$this->_loadData('calendar');
		}
		if(!$format) {
			$format = 'abbreviated';
		}
		if(isset($this->_data['calendar']['month']['format'][$format])) {
			return $this->_data['calendar']['month']['format'][$format][$month];
		}
		if(isset($this->_data['calendar']['month']['stand-alone'][$format])) {
			return $this->_data['calendar']['month']['stand-alone'][$format][$month];
		}
		return $month;
	}
	
	/**
	 * Get day's display name
	 * 
	 * This method will return the display name if a given day of the
	 * week (0 - 6). 0 (ZERO) represents Sunday, and 6 represents
	 * Saturday. Note that you can request the display name in various 
	 * formats. The possible formats are:
	 * 
	 * <code>
	 *    narrow
	 *    abbreviated
	 *    wide
	 * </code>
	 * 
	 * The format will be defaulted to "abbreviated", if not provided to
	 * this method.
	 * 
	 * @access public
	 * @param integer $day
	 * 		The day for which to fetch the display name (0 - 6)
	 * @param string $format
	 * 		The format in which the display name should be fetched
	 * @return string
	 */
	public function getDay($day, $format = NULL) {
		$days = array('sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat');
		if(isset($days[$day])) {
			$day = $days[$day];
			if(!isset($this->_data['calendar'])) {
				$this->_loadData('calendar');
			}
			if(!$format) {
				$format = 'abbreviated';
			}
			if(isset($this->_data['calendar']['day']['format'][$format])) {
				return $this->_data['calendar']['day']['format'][$format][$day];
			}
			if(isset($this->_data['calendar']['day']['stand-alone'][$format])) {
				return $this->_data['calendar']['day']['stand-alone'][$format][$day];
			}
		}
		return $day;
	}
	
	/**
	 * Get Ante/Post Meridiem
	 * 
	 * This method will return the typical AM/PM string in the locale.
	 * 
	 * Example 1, get AM string in dutch
	 * <code>
	 *    $data = M_Locale::getInstance('nl');
	 *    $data->getMeridiem('AM');
	 * </code>
	 * 
	 * Example 1, get PM string in dutch
	 * <code>
	 *    $data = M_Locale::getInstance('nl');
	 *    $data->getMeridiem('AM');
	 * </code>
	 * 
	 * @access public
	 * @param string $ap
	 * 		Ante meridiem (AM), or Post meridiem
	 * @return string
	 */
	public function getMeridiem($ap) {
		$ap = strtoupper($ap);
		if($ap == 'AM' || $ap == 'PM') {
			if(!isset($this->_data['calendar'])) {
				$this->_loadData('calendar');
			}
			if(isset($this->_data['calendar']['ampm'])) {
				return $this->_data['calendar']['ampm'][$ap];
			}
		}
		return $ap;
	}
	
	/**
	 * Get number format
	 * 
	 * This method will return a string that describes the format
	 * in which numbers should be rendered in the locale that is 
	 * represented by the M_LocaleData object. Note that you can 
	 * request the number format in different contexts:
	 * 
	 * - decimal
	 * - scientific
	 * - percent
	 * - currency
	 * 
	 * If the format is not specified to this method, it will be
	 * defaulted to "decimal".
	 * 
	 * Example 1, get number format string in English
	 * <code>
	 *    $data = new M_LocaleData('en');
	 *    echo $data->getNumberFormat();
	 * </code>
	 * 
	 * Example 1 would output the following string that describes the
	 * number format in the requested locale.
	 * 
	 * <code>
	 *    #,##0.###
	 * </code>
	 * 
	 * Read the CLDR specifications on number formatting, at:
	 * - http://www.unicode.org/reports/tr35/#Number_Format_Patterns
     * - http://www.unicode.org/reports/tr35/#NumberElements
     * 
     * to learn more about the illustrated number pattern.
	 * 
	 * @access public
	 * @param string $type
	 * 		The number context
	 * @return string
	 */
	public function getNumberFormat($format = 'decimal') {
		if(!isset($this->_data['numbers'])) {
			$this->_loadData('numbers');
		}
		return $this->_data['numbers'][$format]['default'];
	}
	
	/**
	 * Get number symbols
	 * 
	 * This method will return the number symbols that are used to format
	 * numbers, such as the grouping symbol, decimal symbol, etc.
	 * 
	 * This method will return an associative array, with values for the
	 * following keys:
	 * 
	 * Example 1, the output array in English
	 * <code>
	 *    Array (
	 *       [decimal] => .
	 *       [group] => ,
	 *       [list] => ;
	 *       [percentSign] => %
	 *       [nativeZeroDigit] => 0
	 *       [patternDigit] => #
	 *       [plusSign] => +
	 *       [minusSign] => -
	 *       [exponential] => E
	 *       [perMille] => ‰
	 *       [infinity] => ∞
	 *       [nan] => NaN
	 *    )
	 * </code>
	 * 
	 * Example 1 also illustrates the default values that will be returned
	 * if values are missing in the locale's data.
	 * 
	 * @access public
	 * @return array
	 */
	public function getNumberSymbols() {
		if(!isset($this->_data['numbers'])) {
			$this->_loadData('numbers');
		}
		
		$defaults = array(
			'decimal'         => '.',
			'group'           => ',',
			'list'            => ';',
			'percentSign'     => '%',
			'nativeZeroDigit' => '0',
			'patternDigit'    => '#',
			'plusSign'        => '+',
			'minusSign'       => '-',
			'exponential'     => 'E',
			'perMille'        => '‰',
			'infinity'        => '∞',
			'nan'             => 'NaN'
		);
		
		$output = $this->_data['numbers']['symbols'];
		foreach($defaults as $name => $value) {
			if(!isset($output[$name])) {
				$output[$name] = $value;
			}
		}
		
		return $output;
	}
	
	/**
	 * Get display name
	 * 
	 * This method will provide with the display name of the requested 
	 * name/code. Note that different elements can be requested from 
	 * this method:
	 * 
	 * - language
	 * - territory
	 * - variant
	 * 
	 * Example 1, get the name of Spain in Dutch
	 * <code>
	 *    $data = M_LocaleData::getInstance('nl');
	 *    echo $data->getDisplayName('territory', 'es');
	 * </code>
	 * 
	 * Note that, if the requested display name could not have been
	 * found, this method will return FALSE.
	 * 
	 * @access public
	 * @param string $element
	 * 		The requested element type
	 * @param string $name
	 * 		The name/code of the requested element
	 * @return string|false
	 */
	public function getDisplayName($element, $name) {
		if(in_array($element, array('language', 'territory', 'variant'))) {
			if(!isset($this->_data['dn-' . $element])) {
				$this->_loadData('dn-' . $element);
			}
			if(isset($this->_data['dn-' . $element][$name])) {
				return $this->_data['dn-' . $element][$name];
			}
		}
		return FALSE;
	}
	
	/**
	 * Get code
	 * 
	 * This method will provide with the ISO Code of the requested 
	 * name. Note that the names of different elements can be requested 
	 * from this method:
	 * 
	 * - language
	 * - territory
	 * - variant
	 * 
	 * Example 1, get the code of "Alemania" ("Germany", in Spanish)
	 * 
	 * <code>
	 *    $data = M_LocaleData::getInstance('es');
	 *    echo $data->getIsoCode('territory', 'Alemania');
	 * </code>
	 * 
	 * Example 1 should then return the ISO Code "de", which represents
	 * Germany as a country.
	 * 
	 * NOTE:
	 * Note that, if the requested code could not have been found, 
	 * this method will return FALSE.
	 * 
	 * @access public
	 * @param string $element
	 * 		The requested element type
	 * @param string $name
	 * 		The name of the requested element
	 * @return string|false
	 */
	public function getIsoCode($element, $name) {
		if(in_array($element, array('language', 'territory', 'variant'))) {
			if(!isset($this->_data['dn-' . $element])) {
				$this->_loadData('dn-' . $element);
			}
			$keys = array_keys($this->_data['dn-' . $element], $name);
			if(count($keys) > 0) {
				return $keys[0];
			}
		}
		return FALSE;
	}
	
	/**
	 * Get languages
	 * 
	 * This method will provide with a list of languages that is recognized in the
	 * locale data.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getLanguages() {
		if(!isset($this->_data['dn-language'])) {
			$this->_loadData('dn-language');
		}
		if(isset($this->_data['dn-language'])) {
			return new ArrayIterator($this->_data['dn-language']);
		} else {
			return new ArrayIterator(array());
		}
	}
	
	/**
	 * Get territories
	 * 
	 * This method will provide with a list of territories that is recognized in the
	 * locale data.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getTerritories() {
		if(!isset($this->_data['dn-territory'])) {
			$this->_loadData('dn-territory');
		}
		if(isset($this->_data['dn-territory'])) {
			return new ArrayIterator($this->_data['dn-territory']);
		} else {
			return new ArrayIterator(array());
		}
	}

	/**
	 * Get Currencies
	 *
	 * This method will provide with a list of Currency Codes that is recognized
	 * in the locale data.
	 *
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getCurrencyCodes() {
		// If the information about currencies is not yet available
		if(!isset($this->_data['dn-currency'])) {
			// Then, we load it now:
			$this->_loadData('dn-currency');
		}

		// If currencies could have been found:
		if(isset($this->_data['dn-currency'])) {
			// Then, we return a copy of the currency list
			return new ArrayIterator(array_keys($this->_data['dn-currency']));
		}
		// If not found
		else {
			// Then, we return an empty collection
			return new ArrayIterator(array());
		}
	}
	
	/**
	 * Get Currency display name
	 * 
	 * This method will provide with the display name of a given currency.
	 * Note that you can request the currency in different formats:
	 * 
	 * - narrow/symbol
	 * - wide
	 * 
	 * @access public
	 * @param string $format
	 * 		The format in which you want to get the currency
	 * @param string $currency
	 * 		The currency you want the display name of
	 * @return string
	 */
	public function getCurrencyDisplayName($format, $currency) {
		if(!isset($this->_data['dn-currency'])) {
			$this->_loadData('dn-currency');
		}
		if(isset($this->_data['dn-currency'][$currency])) {
			$names = $this->_data['dn-currency'][$currency];
			if(is_array($names)) {
				switch($format) {
					case 'wide':
						if(isset($names['displayName'])) {
							return $names['displayName'];
						}
					case 'narrow':
					case 'symbol':
					default:
						if(isset($names['symbol'])) {
							return $names['symbol'];
						}
						break;
				}
				return array_shift($names);
			}
		}
		return $currency;
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Load locale data
	 *
	 * This method is for private use only. When locale dependent values
	 * are requested from the M_LocaleData class, it will load the locale
	 * data file, either from cache or with a {@link M_LocaleDataLDML}
	 * object.
	 *
	 * Note that, to improve performance, the locale data is divided into
	 * different categories that are saved into separate cache files.
	 * You need to tell this method which category you want to load.
	 * These categories are for internal use of this class, and are not
	 * related to the locale categories in {@link M_Locale::getCategory()}
	 * in any way.
	 *
	 * @access private
	 * @param string $category
	 * 		The category to be loaded
	 * @return void
	 */
	private function _loadData($category) {
		// If no cache object available, directly load from LDML file(s)
		if(self::$_cache == NULL) {
			$this->_loadLDML();
			return;
		}

		// first, try to load from cache memory
		$data = self::$_cache->read('locale.' . $this->_locale . '.' . $category);
		if(!$data) {
			$this->_loadLDML();
			foreach($this->_data as $name => $values) {
				self::$_cache->write('locale.' . $this->_locale . '.' . $name, $values);
			}
		} else {
			$this->_data[$category] = $data;
		}
	}

	/**
	 * Load LDML file(s)
	 *
	 * This method will load the LDML file(s) of the locale. LDML is an
	 * XML file, its name stands for Locale Data Markup Language. Note
	 * that this method will load the files by the book of CLDR
	 * (Common Locale Data Repository) specifications:
	 *
	 * "The XML format relies on an inheritance model, whereby the
	 * resources are collected into bundles, and the bundles organized
	 * into a tree. Data for the many Spanish locales does not need to be
	 * duplicated across all of the countries having Spanish as a national
	 * language. Instead, common data is collected in the Spanish language
	 * locale, and territory locales only need to supply differences."
	 *
	 * For example, if we set the locale to "nl_BE", this method will load
	 * the following LDML files (if available):
	 *
	 * - nl.xml
	 * - nl_BE.xml
	 *
	 * Learn more about CLDR:
	 * http://unicode.org/cldr/
	 *
	 * Note that this method will return a boolean. If the method returns
	 * TRUE, it means that the LDML files have been loaded successfully.
	 * If the files could not have been loaded, this method returns FALSE.
	 *
	 * @access private
	 * @return boolean
	 */
	private function _loadLDML() {
		// echo '<h1>Load from LDML</h1>';
		$this->_data = array(); //$this->defaultData();
		$locale = '';
		foreach(explode('_', $this->_locale) as $i => $element) {
			if($i > 0) {
				$locale .= '_';
			}
			$locale .= $element;
			$fn = M_Locale::getDataDirectory() . $locale . '.xml';
			if(is_file($fn)) {
				$ldml = new M_LocaleDataLDML($fn);
				$data = array(
					'dn-language'  => $ldml->getDisplayNames('language'),
					'dn-territory' => $ldml->getDisplayNames('territory'),
					'dn-variant'   => $ldml->getDisplayNames('variant'),
					'dn-currency'  => $ldml->getCurrencyDisplayNames(),
					'calendar' => array(
						'fields'   => $ldml->getCalendarFieldNames('gregorian'),
						'month'    => $ldml->getCalendarItemNames('gregorian', 'month'),
						'day'      => $ldml->getCalendarItemNames('gregorian', 'day'),
						'quarter'  => $ldml->getCalendarItemNames('gregorian', 'quarter'),
						'date'     => $ldml->getDateFormats('gregorian', 'date'),
						'time'     => $ldml->getDateFormats('gregorian', 'time'),
						'ampm'     => $ldml->getAmPm()
					),
					'numbers' => array(
						'symbols'    => $ldml->getNumberFormattingSymbols(),
						'decimal'    => $ldml->getNumberFormat('decimal'),
						'scientific' => $ldml->getNumberFormat('scientific'),
						'percent'    => $ldml->getNumberFormat('percent'),
						'currency'   => $ldml->getNumberFormat('currency'),
						'layout'     => $ldml->getDelimiters()
					)
				);
				$this->_data = array_merge($this->_data, $data);
			} else {
				return ($i == 0) ? FALSE : TRUE;
			}
		}

		return TRUE;
	}
}