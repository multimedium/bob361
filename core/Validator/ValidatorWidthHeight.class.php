<?php
/**
 * M_ValidatorWidthHeight
 * 
 * M_ValidatorWidthHeight, a subclass of {@link M_Validator} is used
 * to validate the width and height of a file.
 *
 * Note: this validator will only work for files of which getImageSize can
 * retrieve the width and height.
 *
 * For more information about getImageSize please consult
 * {@link http://be.php.net/manual/en/function.getimagesize.php}
 * 
 * @package Core
 * @author Ben Brughmans
 */
class M_ValidatorWidthHeight extends M_Validator {

	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the width as validator
	 * criteria. Read {@link M_Validator::setCriteria()} to learn more
	 * about validator criteria.
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorWidthHeight;
	 *    $validator->setCriteria(M_ValidatorIsDate::WIDTH, 500);
	 * </code>
	 * 
	 * This criteria sets the width that the tested file
	 * should meet.
	 */
	const WIDTH = 'width';

	/**
	 * Validator Criteria Constant
	 *
	 * This constant is used to address how the width will be validated
	 * criteria. Read {@link M_Validator::setCriteria()} to learn more
	 * about validator criteria.
	 *
	 * Default: LT
	 *
	 * Possible values: EQ, NEQ, LT, LTEQ, GT, GTEQ
	 *
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorWidthHeight;
	 *    $validator->setCriteria(M_ValidatorIsDate::WIDTH, 500);
	 *    $validator->setCriteria(M_ValidatorIsDate::WIDTH_OPERATOR, 'GTEQ');
	 * </code>
	 *
	 * This criteria tells the validator the file should have a width which
	 * should be Greater than or equal to 500
	 */
	const WIDTH_OPERATOR = 'widthOperator';

	/**
	 * Validator Criteria Constant
	 *
	 * This constant is used to address the height as validator
	 * criteria. Read {@link M_Validator::setCriteria()} to learn more
	 * about validator criteria.
	 *
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorWidthHeight;
	 *    $validator->setCriteria(M_ValidatorIsDate::HEIGHT, 300);
	 * </code>
	 *
	 * This criteria sets the height that the tested file
	 * should meet.
	 */
	const HEIGHT = 'height';

	/**
	 * Validator Criteria Constant
	 *
	 * This constant is used to address how the height will be validated
	 * criteria. Read {@link M_Validator::setCriteria()} to learn more
	 * about validator criteria.
	 *
	 * Default: LT
	 *
	 * Possible values: EQ, NEQ, LT, LTEQ, GT, GTEQ
	 *
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorWidthHeight;
	 *    $validator->setCriteria(M_ValidatorIsDate::HEIGHT, 500);
	 *    $validator->setCriteria(M_ValidatorIsDate::HEIGHT_OPERATOR, 'GTEQ');
	 * </code>
	 *
	 * This criteria tells the validator the file should have a width which
	 * should be Greater than or equal to 500
	 */
	const HEIGHT_OPERATOR = 'heightOperator';
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		$size = @getimagesize($subject);

		//if getimagesize doesn't work on this file, we cannot continue
		if (!$size) return FALSE;

		//get criteria
		$width = $this->_getCriteriaValue(self::WIDTH, null);
		$height = $this->_getCriteriaValue(self::HEIGHT, null);

		//check if at least width or height is set
		if (is_null($width) || is_null($height)) {
			throw new M_ValidatorException(
				'Width and height both not set. Please set at least one of them'
			);
		}

		$result = true;
		//validate width
		if ($width) {
			$result = $this->_validateDimension(
				$size[0],
				$width,
				$this->_getCriteriaValue(self::WIDTH_OPERATOR, 'LT')
			);
		}

		//if width was ok, or width was not set: validate height
		if ($height && $result) {
			$result = $this->_validateDimension(
				$size[1],
				$height,
				$this->_getCriteriaValue(self::HEIGHT_OPERATOR, 'LT')
			);
		}

		//return the result
		return $result;
	}

	/**
	 * Validate a given value ($subject) against a dimensiion with a certain operator
	 * 
	 * @param int $dimension
	 * @param string $operator
	 */
	private function _validateDimension($subject, $dimension, $operator) {
		switch ($operator) {
			case 'EQ':
				return $subject == $dimension;
			case 'NEQ':
				return $subject != $dimension;
			case 'LT':
				return $subject < $dimension;
			case 'LTEQ':
				return $subject <= $dimension;
			case 'GT':
				return $subject > $dimension;
			case 'GTEQ':
				return $subject >= $dimension;
			default:
				throw new M_ValidatorException(
					sprintf('Unknown operator %s', $operator)
				);
		}
	}
}