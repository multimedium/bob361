<?php
/**
 * M_Validator
 * 
 * Read an introduction on validators at {@link MI_Validator}.
 * The abstract M_Validator class implements most of the required 
 * interface that is being defined by {@link MI_Validator}.
 * 
 * @package Core
 */
abstract class M_Validator extends M_Object implements MI_Validator {
	/**
	 * Validator criteria
	 * 
	 * To learn more about validator criteria, read:
	 * 
	 * - {@link M_Validator::setCriteria()}
	 * - {@link M_Validator::getCriteria()}
	 * 
	 * @access private
	 * @var array
	 */
	private $_criteria = array();
	
	/**
	 * Error code
	 * 
	 * This property holds the error code that has been generated
	 * by the validator.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_errorCode;
	
	/**
	 * Error criteria
	 * 
	 * This property holds a reference to the criteria that has caused the validator
	 * to judge the validated subject as invalid.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_errorCriteria;
	
	/**
	 * Error message
	 * 
	 * This property holds the error message that has been generated
	 * by the validator.
	 * 
	 * @access private
	 * @var string
	 */
	private $_errorMessage;
	
	/**
	 * Constructor
	 * 
	 * The constructor accepts an associative array with validator
	 * criteria. The keys in this array are used as the criteria 
	 * names.
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorIsDate(array(
	 *       M_ValidatorIsDate::FORMAT => 'Y-m-d'
	 *    ));
	 * </code>
	 * 
	 * Example 2, shows a rewritten copy of Example 1, to illustrate
	 * how the validator criteria in the constructor are being used
	 * under the hood.
	 * 
	 * Example 2
	 * <code>
	 *    $validator = new M_ValidatorIsDate;
	 *    $validator->setCriteria(M_ValidatorIsDate::FORMAT, 'Y-m-d');
	 * </code>
	 * 
	 * To learn more about validator criteria, read the documentation
	 * on the following M_Validator methods:
	 * 
	 * - {@link M_Validator::setCriteria()}
	 * - {@link M_Validator::getCriteria()}
	 * 
	 * @access public
	 * @param array $criteria
	 * 		The validator criteria
	 * @return M_Validator
	 */
	public function __construct(array $criteria = array()) {
		foreach($criteria as $name => $value) {
			$this->setCriteria($name, $value);
		}
	}
	
	/**
	 * Set validator criteria
	 * 
	 * A Validator Criteria is a principle or standard by which the validated 
	 * subject will be judged. In other words, a validator criteria sets the value 
	 * of a specific variable that may influence the validation process.
	 * 
	 * For example, consider a class that checks if a given text (subject) has 
	 * a predetermined minimum and a defined maximum number of words. In our 
	 * example, we call this class M_ValidatorIsNumWords.
	 * 
	 * <code>
	 *    class M_ValidatorIsNumWords extends M_Validator {
	 *    }
	 * </code>
	 * 
	 * In order for this class to be capable of judging the validated subject, 
	 * this validator would need two validator criteria:
	 * 
	 * 1) What should be the minimum number of words?
	 * 2) What should be the maximum number of words?
	 * 
	 * Let's assume that the validator in our example defines two constants, made
	 * available so you can address each of the above-mentioned criteria:
	 * 
	 * <code>
	 *    class M_ValidatorIsNumWords extends M_Validator {
	 *       const MIN = 'min';
	 *       const MAX = 'max';
	 *    }
	 * </code>
	 * 
	 * Now, with this class in mind, assume we wanted to evaluate a given text 
	 * with this validator, to make sure that the text has at least 2 but no more 
	 * than 10 words. We would write the following code to achieve this:
	 * 
	 * Example 1
	 * <code>
	 *    // We construct an instance of the validator
	 *    $validator = new M_ValidatorIsNumWords;
	 *    
	 *    // Set the minimum number of words (2)
	 *    $validator->setCriteria(M_ValidatorIsNumWords::MIN, 2);
	 *    
	 *    // Set the maximum number of words (10)
	 *    $validator->setCriteria(M_ValidatorIsNumWords::MAX, 10);
	 *    
	 *    // Check the text subject
	 *    if($validator->check('Hello world!')) {
	 *       // The validator's check() method returns TRUE, so we can assume
	 *       // that the text respects the given minimum and maximum number of
	 *       // words:
	 *       echo 'Yes, the subject is valid!';
	 *    } else {
	 *       // The validator provides with FALSE as result, so we can assume
	 *       // that the text has less than 2 words, or more than 10 words.
	 *       echo 'Oops, ths subject is not valid!';
	 *    }
	 * </code>
	 * 
	 * The above code would output the following:
	 * 
	 * <code>
	 *    Yes, the subject is valid!
	 * </code>
	 * 
	 * However, you might want to show a different error message, depending on the
	 * validator criteria that caused the subject to be judged as invalid. Let's
	 * assume that you want to show the text "The text must contain at least 2 
	 * words" if the text contains less than 2 words. But, if the text contains 
	 * more than 10 words, you would like to show the following text instead:
	 * "The text must not contain more than 10 words".
	 * 
	 * In order to achieve this, you can set specific error messages for each
	 * of the criteria, with this method. Let's rewrite Example 1 as following:
	 * 
	 * Example 2
	 * <code>
	 *    // We construct an instance of the validator
	 *    $validator = new M_ValidatorIsNumWords;
	 *    
	 *    // Set the minimum number of words (2), and provide with the error
	 *    // message that should be set if the text does not comply with this 
	 *    // requirement
	 *    $validator->setCriteria(M_ValidatorIsNumWords::MIN, 2, t('The text must contain at least 2 words'));
	 *    
	 *    // Set the maximum number of words (10), and provide with the error
	 *    // message that should be set if the text does not comply with this 
	 *    // requirement
	 *    $validator->setCriteria(M_ValidatorIsNumWords::MAX, 10, t('The text must not contain more than 10 words'));
	 *    
	 *    // Check the text subject
	 *    if($validator->check('Hello world!')) {
	 *       // The validator's check() method returns TRUE, so we can assume
	 *       // that the text respects the given minimum and maximum number of
	 *       // words:
	 *       echo 'The text subject is valid!';
	 *    } else {
	 *       // The validator provides with FALSE as result, so we can assume
	 *       // that the text has less than 2 words, or more than 10 words. However,
	 *       // now we can show a specific error message to precisely describe 
	 *       // which of the validator criteria has caused the subject to be judged
	 *       // as invalid:
	 *       echo $validator->getErrorMessage();
	 *    }
	 * </code>
	 * 
	 * Also, you should be able to use {@link M_Validator::getErrorCriteria()} to 
	 * identify the validator criteria that has caused the subject to be judged 
	 * as invalid. Maybe you would want to undertake some additional actions if 
	 * the text from our example contains less than 2 words:
	 * 
	 * <code>
	 *    if($validator->getErrorCriteria() == M_ValidatorIsNumWords::MIN) {
	 *       // ... actions here
	 *    }
	 * </code>
	 * 
	 * You could rewrite as following:
	 * 
	 * <code>
	 *    if($validator->isErrorCriteria(M_ValidatorIsNumWords::MIN)) {
	 *       // ... actions here
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the criteria
	 * @param mixed $value
	 * 		The value of the criteria
	 * @param string $errorMessage
	 * 		The specific error message, for this criteria
	 * @return M_Validator $validator
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setCriteria($name, $value, $errorMessage = NULL) {
		// Set the criteria
		$this->_criteria[(string) $name] = array(
			0 => $value,
			1 => (string) $errorMessage
		);
		
		// Return myself
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get error code
	 * 
	 * The validator may choose to define a set of error codes. By
	 * defining error codes, the validator can make more information
	 * available on why the subject failed to validate.
	 * 
	 * @see M_Validator::ERROR_CODE_FAILED_CRITERIA
	 * @see M_Validator::ERROR_CODE_INVALID
	 * @access public
	 * @return mixed
	 */
	public function getErrorCode() {
		return $this->_errorCode;
	}
	
	/**
	 * Get error criteria
	 * 
	 * You should be able to use {@link M_Validator::getErrorCriteria()} to 
	 * identify the validator criteria that has caused the subject to be judged 
	 * as invalid.
	 * 
	 * @see M_Validator::setCriteria()
	 * @access public
	 * @return mixed
	 */
	public function getErrorCriteria() {
		return $this->_errorCriteria;
	}
	
	/**
	 * Is error criteria?
	 * 
	 * Can be used to check whether or not the validator error criteria matches
	 * the provided one; see {@link M_Validator::getErrorCriteria()}
	 * 
	 * For an example, read the docs on {@link M_Validator::setCriteria()}
	 * 
	 * @see M_Validator::setCriteria()
	 * @see M_Validator::getErrorCriteria()
	 * @access public
	 * @param string $criteria
	 * 		The validator criteria to compare with
	 * @return mixed
	 */
	public function isErrorCriteria($criteria) {
		return ($this->_errorCriteria == $criteria);
	}
	
	/**
	 * Get error messqge
	 * 
	 * Similarly to the error code ({@link M_Validator::getErrorCode()}),
	 * an M_Validator class may also provide with a text that describes
	 * the error.
	 * 
	 * @see M_Validator::setCriteria()
	 * @access public
	 * @return mixed
	 */
	public function getErrorMessage() {
		return $this->_errorMessage;
	}
	
	/**
	 * Has error message?
	 * 
	 * Will tell whether or not the validator carries an error message.
	 * 
	 * @see M_Validator::getErrorMessage()
	 * @access public
	 * @return boolean $flag
	 * 		Returns TRUE if the validator has an error message, FALSE if not
	 */
	public function hasErrorMessage() {
		return ($this->_errorMessage && ! empty($this->_errorMessage));
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Set error code
	 * 
	 * ... Can be used by subclasses of {@link M_Validator} to set the error code
	 * 
	 * @see M_Validator::getErrorCode()
	 * @see M_Validator::_setErrorMessage()
	 * @access protected
	 * @param string $code
	 * @return mixed
	 */
	protected function _setErrorCode($code) {
		$this->_errorCode = (string) $code;
	}
	
	/**
	 * Set error criteria
	 * 
	 * ... Can be used by subclasses of {@link M_Validator} to set the error criteria
	 * 
	 * @see M_Validator::getErrorCriteria()
	 * @see M_Validator::$_errorCriteria()
	 * @access protected
	 * @param string $criteria
	 * @return mixed
	 */
	protected function _setErrorCriteria($criteria) {
		$this->_errorCriteria = (string) $criteria;
	}
	
	/**
	 * Set error message
	 * 
	 * ... Can be used by subclasses of {@link M_Validator} to set the error 
	 * message
	 * 
	 * @see M_Validator::getErrorMessage()
	 * @see M_Validator::_setErrorCode()
	 * @access protected
	 * @param string $text
	 * @return mixed
	 */
	protected function _setErrorMessage($text) {
		$this->_errorMessage = (string) $text;
	}
	
	/**
	 * Get validator criteria
	 * 
	 * This method is used by subclasses of {@link M_Validator}, to fetch the 
	 * value of a given validator criteria. For more info about validator criteria,
	 * read the docs on {@link M_Validator::setCriteria()}. 
	 * 
	 * Note that, if no value could have been found for the requested criteria, 
	 * this method will 
	 * 
	 * - return the default value (second argument to this method), OR
	 * - throw an exception (if the third argument has been set to TRUE).
	 * 
	 * @access protected
	 * @see M_Validator::setCriteria()
	 * @param string $name
	 * 		The name of the criteria
	 * @param mixed $defaultValue
	 * 		The default value, which is returned if no value could
	 * 		have been found for the requested criteria.
	 * @param boolean $throwException
	 * 		If set to TRUE, this method will throw an {@link M_ValidatorException} 
	 * 		if no value could have been found for the requested criteria.
	 * @return mixed
	 */
	protected function _getCriteriaValue($name, $defaultValue, $throwException = FALSE) {
		if(isset($this->_criteria[$name]) && isset($this->_criteria[$name][0])) {
			return $this->_criteria[$name][0];
		} else {
			if(! $throwException) {
				return $defaultValue;
			} else {
				throw new M_ValidatorException(sprintf(
					'%s is missing Validator Criteria "%s"', 
					$this->getClassName(),
					$name
				));
			}
		}
	}
	
	/**
	 * Get error message for validator criteria
	 * 
	 * This method is used by subclasses of {@link M_Validator}, to fetch the 
	 * error message for a given validator criteria. For more info about validator 
	 * criteria, read the docs on {@link M_Validator::setCriteria()}. 
	 * 
	 * Note that, if no error message could have been found for the requested 
	 * criteria, this method will return the default one (second argument to this 
	 * method).
	 * 
	 * @access protected
	 * @see M_Validator::setCriteria()
	 * @param string $name
	 * 		The name of the criteria
	 * @param mixed $defaultErrorMessage
	 * 		The default error message, which is returned if no message could
	 * 		have been found for the requested criteria.
	 * @return mixed
	 */
	protected function _getCriteriaErrorMessage($name, $defaultErrorMessage) {
		if(isset($this->_criteria[$name]) && isset($this->_criteria[$name][1])) {
			return $this->_criteria[$name][1];
		} else {
			return (string) $defaultErrorMessage;
		}
	}
}