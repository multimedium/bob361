<?php
/**
 * M_ValidatorIsNumberOfDecimals
 * 
 * M_ValidatorIsNumberOfDecimals, a subclass of {@link M_Validator}, is used
 * to check if a subject value has no more then the specified number of decimals.
 * 
 * M_ValidatorIsNumberOfDecimals will use {@link M_Number::constructWithString()}
 * to construct a number with the given subject. By using 
 * {@link M_Number}, the validator is made sensitive to locale-dependent 
 * number formatting.
 * 
 * @package Core
 */
class M_ValidatorIsNumberOfDecimals extends M_Validator {

	/* -- CONSTANTS -- */

	/**
	 * Validator Criteria Constant
	 *
	 * This constant is used to address the number of decimals
	 *
	 * @access public
	 * @var string
	 */
	const DECIMALS = 'decimals';

	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the locale name as validator
	 * criteria. Read {@link M_Validator::setCriteria()} to learn more
	 * about validator criteria.
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorIsNumeric;
	 *    $validator->setCriteria(M_ValidatorIsNumberOfDecimals::LOCALE, 'en');
	 * </code>
	 * 
	 * This criteria sets the locale that should be used to evaluate the 
	 * string as a numeric value. The name of the locale will be passed
	 * into {@link M_Number::constructWithString()} in order to try
	 * and construct a number.
	 * 
	 * @see M_Locale
	 * @see M_Number::constructWithString()
	 * @access public
	 * @var string
	 */
	const LOCALE = 'locale';
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		try {
			// (Try to) Construct a number with the string
			$number = M_Number::constructWithString($subject, $this->_getCriteriaValue(self::LOCALE, NULL));
			
			// get validator criteria:
			$decimals = $this->_getCriteriaValue(self::DECIMALS, null);
			
			
			// If the number is lower than the minimum value:
			if(!is_null($decimals)) {
				// determine the factor
				$factor = pow(10, $decimals);
				// calculate the value we want to campare to
				$compareTo = (int) ($number->getNumber() * $factor);
				$compareTo = $compareTo / $factor;
				
				// check if input and compare value are the same
				if ($number->getNumber() != $compareTo) {
					// Then, we set the error message:
					$this->_setErrorMessage(
						// ... to the one that has been provided for this criteria
						$this->_getCriteriaErrorMessage(
							self::DECIMALS,
							t('The number of decimals should not exceed @number', array(
								'@number' => $decimals
							))
						)
					);
					return false;	
				}
			}

			
			// Return success, if the number got constructed and adheres to the
			// defined minimum and maximum
			return TRUE;
		}
		// If failed to construct a number:
		catch(Exception $e) {
			// Is this really a validator error? If we set this message here, it
			// would be shown in forms...
			// $this->setErrorCode($e->getCode());
			// $this->setErrorMessage($e->getMessage());
		}
		
		// Return FALSE if still here
		return FALSE;
	}
}
?>