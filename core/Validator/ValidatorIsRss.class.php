<?php
/**
 * M_ValidatorIsRss
 * 
 * M_ValidatorIsRss, a subclass of {@link M_Validator} is used to validate a 
 * hyperlink to an RSS Feed. This validator will fetch the contents of the provided
 * link, and make sure that a valid RSS feed is being provided by that link.
 * 
 * @package Core
 */
class M_ValidatorIsRss extends M_Validator {
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to set the Minimum Number of Articles as validator
	 * criteria. A value for this criteria is optional, and is used to check the
	 * number of news headlines in the RSS Feed against a required minimum.
	 * 
	 * @see M_Validator::setCriteria()
	 * @var string
	 */
	const MINIMUM_NUMBER_OF_ARTICLES = 'MINIMUM_NUMBER_OF_ARTICLES';
	
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to set the Maximum Number of Articles as validator
	 * criteria. A value for this criteria is optional, and is used to check the
	 * number of news headlines in the RSS Feed against the given maximum.
	 * 
	 * @see M_Validator::setCriteria()
	 * @var string
	 */
	const MAXIMUM_NUMBER_OF_ARTICLES = 'MAXIMUM_NUMBER_OF_ARTICLES';
	
	/**
	 * Evaluate the subject
	 * 
	 * @see MI_Validator::check()
	 * @access public
	 * @param M_Uri|string $subject
	 * 		The subject to be validated. In this case, you may provide a string,
	 * 		or an instance of {@link M_Uri}
	 * @return boolean
	 */
	public function check($subject) {
		// If the subject is an object:
		if(is_object($subject)) {
			// Then it MUST be an instance of M_Uri:
			if(! M_Helper::isInstanceOf($subject, 'M_Uri')) {
				// If not the case, we throw an exception
				throw new M_ValidatorException(
					sprintf(
						'%s cannot validate an instance of %s; expecting an instance of M_Uri',
						__CLASS__,
						get_class($subject)
					)
				);
			}
		}
		// If the subject is not an object
		else {
			// We construct a new M_Uri instance:
			$subject = new M_Uri((string) $subject);
		}
		
		// Include a comment, to indicate the type of variable that is now
		// stored by $subject
		/* @var $subject M_Uri */
		
		// We fetch the contents of the URI, and try to mount an RSS Feed:
		try {
			// If we can mount the RSS
			if(M_FeedRss::constructWithString($subject->getSession()->getContents())) {
				// 
				// We return TRUE, to indicate a valid subject:
				return TRUE;
			}
		}
		// If we failed to mount a feed
		catch(Exception $e) {
			// Is this really a validator error? If we set this message here, it
			// would be shown in forms...
			// $this->setErrorCode($e->getCode());
			// $this->setErrorMessage($e->getMessage());
		}
		
		// If we are still here, we return FALSE (the subject is not valid)
		return FALSE;
	}
}