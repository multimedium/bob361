<?php
/**
 * M_ValidatorIsTime
 *
 * M_ValidatorIsTime, a subclass of {@link M_Validator} is used
 * to validate a given time input.
 *
 * @author Tim Segers
 * @package Core
 */
class M_ValidatorIsTime extends M_Validator {

	/* -- CONSTANTS -- */

	/**
	 * Pattern
	 *
	 * The pattern we have to check on to pass validation.
	 *
	 * Examples:
	 * - 0:20
	 * - 9:00
	 * - 10:25
	 *
	 * @access const
	 */
	const PATTERN = '/^([0-9]|[0-1][0-9]|2[0-3]):([0-5][0-9])$/';

	/* -- MI_VALIDATOR -- */

	/**
	 * Evaluate the subject
	 *
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 *
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {

		// Create a new M_ValidatorIsPattern, which we will use to check the given time
		$validator = new M_ValidatorIsPattern();

		// Set the pattern the validator should check against
		$validator->setCriteria(M_ValidatorIsPattern::PATTERN, self::PATTERN);

		// Check the given time input against the validator
		if ($validator->check($subject)) {
			// If validation succeeded, return TRUE
			return TRUE;
		} else {
			// If validation failed, return FALSE
			return FALSE;
		}
	}
}