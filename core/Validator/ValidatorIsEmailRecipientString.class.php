<?php
/**
 * M_ValidatorIsEmailRecipientString
 * 
 * M_ValidatorIsEmailRecipientString, a subclass of {@link M_Validator} is used
 * to validate email recipients.
 * 
 * @package Core
 */
class M_ValidatorIsEmailRecipientString extends M_Validator {
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		// Try to get the email recipients:
		try {
			M_MailHelper::getRecipientsFromString($subject);
			return TRUE;
		}
		catch(Exception $e) {
		}
		
		// If we are still here, we failed:
		return FALSE;
	}
}