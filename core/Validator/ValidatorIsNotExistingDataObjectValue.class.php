<?php
/**
 * M_ValidatorIsNotExistingDataObjectValue
 * 
 * ... Produces the reversed result of {@link M_ValidatorIsExistingDataObjectValue}
 * 
 * @package Core
 */
class M_ValidatorIsNotExistingDataObjectValue extends M_ValidatorIsExistingDataObjectValue {
	/**
	 * Check subject
	 *
	 * @access public
	 * @param mixed $subject
	 * 		The value to be checked
	 * @return boolean $flag
	 * 		Returns TRUE if the value is unique (that is, if no matching data object
	 * 		can be found with the value), FALSE if not
	 */
	public function check($subject) {
		return (! parent::check($subject));
	}
}