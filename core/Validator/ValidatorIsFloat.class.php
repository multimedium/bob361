<?php
/**
 * M_ValidatorIsFloat
 * 
 * M_ValidatorIsFloat, a subclass of {@link M_Validator}, is used
 * to check if a subject value is a floating number.
 * 
 * Like {@link M_ValidatorIsNumeric}, M_ValidatorIsFloat will also use 
 * {@link M_Number::constructWithString()} to construct a number with 
 * the given test subject. By using {@link M_Number}, the validator is 
 * made sensitive to locale-dependent number formatting.
 * 
 * @package Core
 */
class M_ValidatorIsFloat extends M_Validator {
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the locale name as validator
	 * criteria. Read {@link M_Validator::setCriteria()} to learn more
	 * about validator criteria.
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorIsFloat;
	 *    $validator->setCriteria(M_ValidatorIsFloat::LOCALE, 'en');
	 * </code>
	 * 
	 * This criteria sets the locale that should be used to evaluate the 
	 * string as a numeric value. The name of the locale will be passed
	 * into {@link M_Number::constructWithString()} in order to try
	 * and construct a number.
	 * 
	 * @see M_Locale
	 * @see M_Number::constructWithString()
	 */
	const LOCALE = 'locale';
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		try {
			// Try to construct a number with the string
			$number = M_Number::constructWithString($subject, $this->_getCriteriaValue(self::LOCALE, NULL));
			
			// Check if floating number
			return $number->isFloat();
		}
		// If failed to construct a number:
		catch(Exception $e) {
			// Is this really a validator error? If we set this message here, it
			// would be shown in forms...
			// $this->setErrorCode($e->getCode());
			// $this->setErrorMessage($e->getMessage());
		}
		
		// Return FALSE if still here
		return FALSE;
	}
}