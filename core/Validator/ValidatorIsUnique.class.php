<?php
/**
 * M_ValidatorIsUnique
 * 
 * M_ValidatorIsUnique, a subclass of {@link M_Validator}, is used to check if 
 * a value can be considered a unique value in a given data object's
 * storage space.
 * 
 * @package Core
 */
class M_ValidatorIsUnique extends M_Validator {
	const DB = 'db';
	const TABLE = 'table';
	const COMPARE = 'field';
	const EXCLUDE = 'exclude';
	const EXCLUDE_VALUE = 'excludeValue';
	
	public function check($subject) {
		// Get the database connection
		$dbId = $this->getCriteria(self::VALUE);
		$db = $dbId ? M_Db::getInstance($dbId) : M_Db::getInstance();
		
		// Get elements for the select:
		$table = $db->quoteIdentifier($this->getCriteria(self::TABLE, NULL, TRUE));
		$field = $db->quoteIdentifier($this->getCriteria(self::COMPARE, NULL, TRUE));
		
		// Compose the select to do the uniqueness check
		$select = $db->select();
		$select->from($table);
		$select->where($field . ' = ?', $subject);
		
		// Exclude records?
		$exclude = $this->getCriteria(self::EXCLUDE, NULL);
		$subject = $this->getCriteria(self::EXCLUDE_VALUE, NULL);
		if($exclude && $subject) {
			$select->where($db->quoteIdentifier($exclude) . ' <> ?', $subject);
		}
		
		// Run the SQL Query
		$rs = $select->execute();
		
		// If the result set is valid, and the number of records is
		// 0 (ZERO), it means that the subject is unique. In that
		// case, we return SUCCESS
		return ($rs !== FALSE && count($rs) == 0);
	}
}
?>