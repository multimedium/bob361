<?php
/**
 * M_ValidatorIsEmail
 * 
 * M_ValidatorIsEmail, a subclass of {@link M_Validator} is used
 * to validate an email address.
 * 
 * @package Core
 */
class M_ValidatorIsEmail extends M_Validator {
	
	/**
	 * use regex
	 * 
	 * setting this criteria to true will force the validator
	 * to use the regex for validation in stead of the php built
	 * in filter (which has proven to be unreliable)
	 */
	const USE_REGEX = 'USE_REGEX';
	
	
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		// check if the use of the regex is desireable
		$useRegex = (
			$this->_getCriteriaValue(self::USE_REGEX, FALSE) ||
			! function_exists('filter_var')
		);
		
		if (! $useRegex) { //Introduced in PHP 5.2
			if(filter_var($subject, FILTER_VALIDATE_EMAIL) === FALSE) {
				return false;
			} else {
				return true;
			}
		} else {
			return $this->_checkWithRegEx($subject);
		}
	}

	/* -- PROTECTED FUNCTIONs -- */

	/**
	 * Validate email by regex
	 *
	 * @param string $subject
	 * @return bool
	 */
	protected function _checkWithRegEx($subject) {

		// The maximum length of an e-mail address is 320 octets, per RFC 2821		
		if(strlen($subject) > 320) {
			return false;
		}
		
		// a simple regex to match email adresses. Note that this may allow to much, but better to allow an 
		// address that doesn't exist then to deny an address that does exist
		$regex = '/("[^"]*"|[^@]*)?[\s<]{0,}[.0-9\pL_-]+@([0-9\pL-]+\.)+[0-9a-z]{2,15}[>\s;,]{0,}/im';
		
		// Check the e-mail address with the regex
		return (preg_match($regex, $subject) == 1);
	}

	/**
	 * Check function - Alternate version
	 *
	 * NOTE: this function is currently not in use
	 *
	 * This function is an alternative to the regular expression checking the
	 * email addresses. It is fully functional and working, but is slower in
	 * comparison to the regular expression variant. While testing on 136k
	 * database records the reg ex reached about 3.5 seconds average, while
	 * this function didn't get under 6 seconds. Therefor, this function is
	 * currently not in use.
	 *
	 * @access protected
	 * @param string $subject
	 *		The email address that is to be validated
	 * @return bool
	 */
	protected function checkWithoutRegEx($subject) {
		// First of all, make sure the string is lowercase. Next, we explode
		// on @, to separate username and domain, and to check
		// that there is only 1 @ in the email address
		$elements = explode('@', strtolower($subject));

		// If there is more than one @ in the email address (or none at all),
		// return FALSE
		if(count($elements) != 2) return FALSE;

		// Now we will check the username. The rules we follow for this can be
		// found here: http://en.wikipedia.org/wiki/Email_address. We will check
		// for dots (.), dashes (-) and underscores (_).
		foreach(explode('.', $elements[0]) as $elem) {
			// Make sure the string is not empty: e.g. if there is a dot in the
			// beginning of the string, at the end of the string, or multiple
			// dots in a row in the middle of the string.
			if(! $elem) {
				return FALSE;
			}

			// Make sure every character in the string is an allowed one, e.g.
			// a-z, A-Z, 0-9, - or _
			for($i = 0, $n = strlen($elem); $i < $n; $i++) {
				if(! ((int) strpos(' abcdefghijklmnopqrstuvwxyz-_0123456789', $elem[$i]) > 0)) {
					return FALSE;
				}
			}
		}

		// Before checking the parts of the domain itself, make sure the
		// domain's last part - e.g. the tld - is valid. To be valid, it must
		// present, and be 2-4 characters long. Additionally, make sure there
		// are at least 2 domain elements present, as an email address like
		// example@be is not valid.
		$domainElements = explode('.', $elements[1]);

		// First, check the total count
		if(count($domainElements) < 2) {
			return FALSE;
		}

		// Next, make sure the tld is 2-4 characters long
		$tld = strlen((string) array_pop($domainElements));
		if($tld < 2 || $tld > 4) {
			return FALSE;
		}

		// After the username validated correctly and the tld was valid,
		// we will now check the domain. For more info about this, read
		// http://en.wikipedia.org/wiki/Hostname. First of all we will
		// explode on a dot, to separate all the labels the domain name
		// contains. For example, for the domain example.multimedium.be,
		// we have 3 labels, e.g. example, multimedium and be.
		foreach($domainElements as $label) {
			// Get the number of characters in the label:
			$labelLength = strlen($label);

			// Make sure the label is not empty: e.g. if there is a dot in the
			// beginning of the label, at the end of the label, or multiple
			// dots in a row in the middle of the string. Additionally, every
			// label can only be 1-63 characters long
			if($labelLength == 0 || $labelLength > 63) {
				return FALSE;
			}

			// Make sure there are no hyphens in the beginning or at the end
			// of the label. In the middle this is no problem
			if(trim($label, '-') != $label) {
				return FALSE;
			}

			// Next, make sure every label contains only the valid characters,
			// e.g. a-z (no uppercase!) , 0-9 and hyphens (-)
			for($i = 0, $n = $labelLength; $i < $n; $i++) {
				if(! ((int) strpos(' abcdefghijklmnopqrstuvwxyz-0123456789', $label[$i]) > 0)) {
					return FALSE;
				}
			}
		}

		// If we are still here the check succeeded, so we return TRUE
		return TRUE;
	}
}