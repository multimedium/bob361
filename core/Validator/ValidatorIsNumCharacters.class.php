<?php
/**
 * M_ValidatorIsNumCharacters class
 * 
 * M_ValidatorIsNumCharacters, a subclass of {@link M_Validator} is used
 * to validate a text against a minimum and maximum number of characters.
 * 
 * @package Core
 */
class M_ValidatorIsNumCharacters extends M_Validator {
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the minimum number of characters as 
	 * validator criteria. Read {@link M_Validator::setCriteria()} to 
	 * learn more about validator criteria.
	 * 
	 * Example 1
	 * <code>
	 *    // Set the minimum number of characters to 10
	 *    $validator = new M_ValidatorIsNumCharacters;
	 *    $validator->setCriteria(M_ValidatorIsNumCharacters::MIN, 10);
	 * </code>
	 * 
	 * This criteria sets the minimum number of characters that the tested 
	 * string should contain. In Example 1, the text should have at
	 * least 10 characters, in order to validate successfully.
	 */
	const MIN = 'min';
	
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the maximum number of characters as 
	 * validator criteria. Read {@link M_Validator::setCriteria()} to 
	 * learn more about validator criteria.
	 * 
	 * Example 1
	 * <code>
	 *    // Set the maximum number of characters to 50
	 *    $validator = new M_ValidatorIsNumCharacters;
	 *    $validator->setCriteria(M_ValidatorIsNumCharacters::MAX, 50);
	 * </code>
	 * 
	 * This criteria sets the maximum number of characters that the tested 
	 * string can contain. In Example 1, the text cannot have more
	 * than 50 characters, in order to validate successfully.
	 */
	const MAX = 'max';
	
	/**
	 * Evaluate the subject
	 * 
	 * @see MI_Validator::check()
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean $flag
	 * 		Returns TRUE if the subject is valid, FALSE if not
	 */
	public function check($subject) {
		// Get the length, or number of characters, of the string subject
		$num = strlen($subject);
		
		// get the provided validator criteria:
		$min = $this->_getCriteriaValue(self::MIN, 0);
		$max = $this->_getCriteriaValue(self::MAX, 0);
		
		// If less than the minimum:
		if($min > 0 && $num < $min) {
			// Set the error message...
			$this->_setErrorMessage(
				// ... to the one that has been provided for this criteria
				$this->_getCriteriaErrorMessage(
					self::MIN,
					t('The text should at least be @number characters long', array(
						'@number' => $min
					))
				)
			);
			
			// Return FALSE as result
			return FALSE;
		}
		
		// If more than the maximum
		if($max > 0 && $num > $max) {
			// Set the error message...
			$this->_setErrorMessage(
				// ... to the one that has been provided for this criteria
				$this->_getCriteriaErrorMessage(
					self::MAX,
					t('The text should not be longer than @number characters long', array(
						'@number' => $max
					))
				)
			);
			
			// Return FALSE as result
			return FALSE;
		}
		
		// If still here, we return success (value is considered valid)
		return TRUE;
	}
}