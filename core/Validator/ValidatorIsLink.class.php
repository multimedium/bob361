<?php
/**
 * M_ValidatorIsLink
 * 
 * M_ValidatorIsLink, a subclass of {@link M_Validator} is used to validate a 
 * hyperlink.
 * 
 * @package Core
 */
class M_ValidatorIsLink extends M_Validator {
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		/*
		// Extracted from http://scriptplayground.com/tutorials/php/Simple-Way-to-Validate-Links/
		// Compose the regular expression:
		// Scheme
		$urlRegex  = "^(https?|ftp)\:\/\/";
		
		// User and password (optional)
		$urlRegex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?";
		
		// Hostname or ip
		$urlRegex .= "[a-z0-9+\$_-]+(\.[a-z0-9+\$_-]+)*";
		
		// Port number
		$urlRegex .= "(\:[0-9]{2,5})?";
		
		// Path (optional)
		$urlRegex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?";
		
		// GET Query (optional)
		$urlRegex .= "(\?[a-z+&\$_.-][a-z0-9;:@/&%=+\$_.-]*)?";
		
		// Fragment/Anchor (optional)
		$urlRegex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?\$";
		
		// Check!
		if(eregi($urlRegex, $subject)) {
			return TRUE;
		} else {
			return FALSE;
		} */
		
		// This one works better:
		// (also, preg_match is binary-safe, so that's good)
		if(preg_match('/^(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:\/~\+#]*[\w\-\@?^=%&amp;\/~\+#])?$/i', $subject)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>