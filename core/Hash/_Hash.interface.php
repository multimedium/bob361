<?php
/**
 * MI_Hash interface
 * 
 * MI_Hash dictates the public API that must be implemented by
 * cryptography related core classes.
 * 
 * @author Tim
 * @package Core
 */
interface MI_Hash {
	
	/* -- SETTERS -- */
	
	/**
	 * Set Input
	 * 
	 * Allows for setting the input string that is to be encrypted by the
	 * cryptographer.
	 * 
	 * @see MI_Hash::getInput()
	 * @access public
	 * @param string $input
	 * @return MI_Hash $crypto
	 *		Returns itself, for a fluent programming interface
	 */
	public function setInput($input);
	
	/**
	 * Set Salt
	 * 
	 * Allows for setting the string that is used for salting during encryption.
	 * 
	 * @see MI_Hash::getSalt()
	 * @access public
	 * @param string $salt
	 * @return MI_Hash $crypto
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSalt($salt);
	
	/* -- GETTERS -- */
	
	/**
	 * Get Input
	 * 
	 * Will provide the set input string
	 * 
	 * @see MI_Hash::setInput()
	 * @access public
	 * @return string
	 */
	public function getInput();
	
	/**
	 * Get Salt
	 * 
	 * Will provide the set salt
	 * 
	 * @see MI_Hash::setSalt()
	 * @access public
	 * @return string
	 */
	public function getSalt();
	
	/**
	 * Get Hash
	 * 
	 * Outputs the hashed result of the chryptographic implementation of this
	 * interface, which is typically a string
	 * 
	 * @access public
	 * @return string
	 */
	public function getHash();
}