<?php
/**
 * MI_ChartAxe interface
 * 
 * @package Core
 */
interface MI_ChartAxe {
	
	/**
	 * Set the type of axe
	 * 
	 * @param string $string
	 * @return void
	 */
	public function setType($string);
	
	/**
	 * Get the type of axe
	 *  
	 * @return string $type
	 */
	public function getType();
	
	/**
	 * Set the end range for this axe
	 * 
	 * @param int $range
	 * @return void
	 */
	public function setStartRange($range);
	
	/**
	 * Get the start range for this axe
	 *  
	 * @return int $range
	 */
	public function getStartRange();
	
	/**
	 * Set the end range for this axe
	 * 
	 * @param int $range
	 * @return void
	 */
	public function setEndRange($range);
	
	/**
	 * Get the end range for this axe
	 *  
	 * @return int $range
	 */
	public function getEndRange();
	
}