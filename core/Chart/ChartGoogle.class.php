<?php 

abstract class M_ChartGoogle extends M_Chart {
	
	/**
	 * Generates the Google Chart's size parameter
	 * The google charts API expects this parameter in the following syntax [width]x[height]
	 * 
	 * Consider the following example:
	 * 
	 * Example 1
	 * <code>
	 * 		$chart->setWidth(250);
	 * 		$chart->setHeight(100);
	 * 		
	 * 		// This method will return the string "250x100"
	 * 		$chart->getSize();
	 * </code>
	 * 
	 * @return string $size
	 */
	protected function _getSize() {
		// Get the width and the height
		$width = $this->getWidth();
		$height = $this->getHeight();
		
		// If empty width or height:
		if(empty($width) OR empty($height)) {
			throw new M_Exception('Could not calculate the size: missing width or height of the chart');
		}
		
		// Return the size
		return $width . 'x' . $height;
	}
	
	/**
	 * Get HTML
	 * 
	 * Returns the image of the chart
	 * 
	 * @return string
	 */
	public function getHtml() {
		$uri = $this->_getGoogleChartApiUri()->toString();
		return '<img src="' . htmlentities($uri) . '" alt="' . $this->getTitle() . '" />';
	}
	
	/**
	 * Get the Google Chart Api URL
	 * 
	 * @return M_Uri $uri
	 */
	protected function _getGoogleChartApiUri() {
		// Build up the basic Uri
		$uri = new M_Uri('http://chart.apis.google.com/chart');
		
		// Set the size variable
		$uri->setQueryVariable('chs', $this->_getSize());
		
		// Set the data variable
		$uri->setQueryVariable('chd', $this->_generateGoogleDataString());
		
		// Set the scale variable
		$uri->setQueryVariable('chds', $this->_generateGoogleScaleString());
		
		// Return the Uri
		return $uri;
	}
	
	/**
	 * Gerenate Google API data string
	 * 
	 * @return string
	 */
	protected function _generateGoogleDataString() {
		
		// Counter for seperator
		$i = 1;
		// Total count
		$count = $this->getDatasets()->count();
		
		if($count < 1) {
			throw new M_Exception('Could not generate the chart: minimum 1 dataset needed');
		}
		
		$result = '';
		
		// Foreach Dataset:
		foreach($this->getDatasets() as $dataset) {
			
			$result .= $this->_generateGoogleDataStringFromDataset($dataset);
			
			// If this is not the last set:
			if($i != $count) {
				// Add pipeline-seperator
				$result .= '|';
			}
			
			// Count++
			$i++;
		}
		return 't:' . $result;
	}
	
	/**
	 * Generate Google API scale string (min and max value)
	 * 
	 * @return string
	 */
	protected function _generateGoogleScaleString() {
		$scale = array();
		foreach($this->getDatasets() AS $dataSet) {
			/* @var $dataSet M_ChartDataset */
			$scale[] = $dataSet->getMinScale();
			$scale[] = $dataSet->getMaxScale();
		}
		return implode(',', $scale);
	}


	/**
	 * This generates a Dataset string that we can use in the UI
	 * 
	 * @param M_ChartDataset $dataset
	 * @return string
	 */
	protected function _generateGoogleDataStringFromDataset(M_ChartDataset $dataset) {
		
		// Counter for the seperator
		$i = 1;
		// Total count
		$count = $dataset->getData()->count();


		$result = '';
		// Get the data
		foreach($dataset->getData() as $data) {
			
			// Add the value
			$result .= $data->getValue();
			
			// If this is not the last item, add the seperator
			if($i != $count) {
				$result .= ',';
			}
			
			// Count++
			$i++;
			
		}
		
		return $result;
	}
	
	/**
	 * Generate scaling string
	 * 
	 * @return string $string
	 */
	protected function _generateGoogleScalingString() {
		
		// Counter for seperator
		$i = 1;
		$count = $this->getDatasets()->count();
		
		$result = '';
		
		// Foreach dataset
		foreach($this->getDatasets() as $dataset) {
			
			// Get miniumum and maximum scaling
			$result .= $dataset->getMinimumScale() . ',' . $dataset->getMaximumScale();	
			
			// If this is not the last dataset, add seperator
			if($i != $count) {
				$result .= ',';
			}		
			$i++;
		}
	}
	
}