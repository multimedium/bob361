<?php 
/**
 * M_ChartAxe
 * 
 * The Axe contains all the specific data in display a correct axe
 * 
 * @author Ben Brughmans
 *
 */
class M_ChartAxe implements MI_ChartAxe {
	
	/**
	 * Types of axes
	 */
	CONST TYPE_BOTTOM ='x';
	CONST TYPE_LEFT = 'y';
	CONST TYPE_RIGHT = 'right';
	CONST TYPE_TOP = 'top';

	/**
	 * Used to store the different types of axes
	 */
	private $_type;
	
	/**
	 * Holds the labels for this axe
	 * 
	 * @var array
	 */
	private $_labels = array();
	
	/**
	 * Start range on this value
	 * 
	 * @var int
	 */
	private $_startRange;
	
	/**
	 * End range on this value
	 * 
	 * @var int
	 */
	private $_endRange;
	
	/**
	 * Get the start range of this axe
	 * 
	 * @return int $startRange
	 */
	public function getStartRange() {
		return $this->_startRange;
	}
	
	/**
	 * Set the start range for this axe
	 * 
	 * @param int $startRange
	 * @return void
	 */
	public function setStartRange($startRange) {
		$this->_startRange = $startRange;
	}
	
	/**
	 * Get the end range of this axe
	 * 
	 * @return int $endRange
	 */
	public function getEndRange() {
		return $this->_endRange;
	}
	
	/**
	 * Set the end range for this axe
	 * 
	 * @param int $endRange
	 * @return void
	 */
	public function setEndRange($endRange) {
		$this->_endRange = $endRange;
	}
	
	/**
	 * Get the type of this axe
	 * 
	 * @return string $type
	 */
	public function getType() {
		return $this->_type;
	}
	
	/**
	 * Set the type of this axe
	 * 
	 * @param string $type
	 * @return void
	 */
	public function setType($type) {
		$this->_type = $type;
	}
	
	/**
	 * Get a list of labels
	 * 
	 * @return array
	 */
	public function getLabels() {
		return $this->_labels;
	}
	
	/**
	 * Set the labels for this Axe.
	 * 
	 * @param array $labels
	 * @return M_ChartAxe
	 */
	public function setLabels( array $labels) {
		$this->_labels = $labels;
		return $this;
	}
	
	/**
	 * Add a label to the axe
	 * 
	 * @param string $label
	 * @return M_ChartAxe
	 */
	public function addLabel($label) {
		$this->_labels[] = $label;
		return $this;
	}
}