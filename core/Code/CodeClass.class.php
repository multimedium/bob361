<?php
class M_CodeClass {
	protected $_name;
	protected $_parent;
	protected $_interface;
	protected $_comment;
	protected $_properties = array();
	protected $_functions = array();
	
	public function getName() {
		return $this->_name;
	}
	
	public function getParentClassName() {
		return $this->_parent;
	}
	
	public function getInterface() {
		return $this->_interface;
	}
	
	public function getComment() {
		return $this->_comment;
	}
	
	public function getProperties() {
		return new ArrayIterator($this->_properties);
	}
	
	public function getMethods() {
		return new ArrayIterator($this->_functions);
	}
	
	public function toString() {
		// We get the new line characters (including tabs), to start new lines
		// in the class' source code
		$nl = M_CodeHelper::getNewLineWithTab(1, 1);
		
		// We start the code by adding the class comments (if any):
		$out = '';
		if($this->_comment) {
			$out .= $this->_comment->toString(0);
		}
		
		// Open the class:
		$out .= M_CodeHelper::getNewLine();
		$out .= 'class ' . $this->_name;
		
		// Extend from parent (if any)
		if($this->_parent) {
			$out .= ' extends ' . $this->_parent;
		}
		
		// Implement interface (if any)
		if($this->_interface) {
			$out .= ' implements ' . $this->_interface;
		}
		
		// Open the class body
		$out .= ' {';
		
		// For each of the properties in the class:
		$i = 0;
		foreach($this->getProperties() as $i => $property) {
			// Default the access to PROTECTED:
			$tmp = $property->getAccess();
			if(! $tmp) {
				$tmp = M_CodeAccess::TYPE_PROTECTED;
			}
			
			// Add the property to the class' source code:
			$out .= M_CodeHelper::getNewLine() . $this->_getPropertyComment($property)->toString(1);
			$out .= $nl . $tmp . ' $' . M_Helper::ltrimCharlist($property->getName(), '$');
		}

		//add a newline if the class contains at least one property
		if ($i > 0) {
			$out .= $nl;
		}
		
		// For each of the methods in the class
		/* @var $method M_CodeFunction */
		foreach($this->getMethods() as $method) {
			$out .= $nl . M_CodeHelper::getNewLine(1) . $method->toString(1);
		}
		
		$out .= M_CodeHelper::getNewLine();
		$out .= '}';
		
		// Return the final code render:
		return $out;
	}
	
	public function setName($name) {
		$this->_name = (string) $name;
	}
	
	public function setParentClassName($name) {
		$this->_parent = (string) $name;
	}
	
	public function setInterface($name) {
		$this->_interface = (string) $name;
	}
	
	public function setComments(M_CodeComment $comment) {
		$this->_comment = $comment;
	}
	
	public function addProperty(M_CodeVariable $variable) {
		$this->_properties[] = $variable;
	}
	
	public function addMethod(M_CodeFunction $function) {
		$this->_functions[] = $function;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	protected function _getPropertyComment(M_CodeVariable $property) {
		$comment = $property->getComment();
		
		// If no comments have been added to the property, we make them ourselves:
		if(! $comment) {
			$comment = new M_CodeComment;
			$comment->setTitle($property->getName());
			$comment->setAccess($this->_getPropertyAccess($property));
			$comment->setDataType($property->getDataType());
		}
		// if comments have already been added to the property:
		else {
		}
		
		// Return the comments:
		return $comment;
	}
	
	protected function _getMethodComment(M_CodeFunction $method) {
		$comment = $method->getComment();
		
		// If no comments have been added to the method, we make them ourselves:
		if(! $comment) {
			$comment = new M_CodeComment;
			$comment->setTitle($method->getName());
			$comment->setAccess($this->_getMethodAccess($method));
			$comment->setReturnVoid();
			foreach($method->getParameters() as $parameter) {
				$comment->addParameter($parameter, 'Test description');
			}
			if($method->isAbstract()) {
				$comment->setIsAbstract(TRUE);
			}
			if($method->isStatic()) {
				$comment->setIsStatic(TRUE);
			}
		}
		// If comments have already been added:
		else {
		}
		
		// Return the comments:
		return $comment;
	}
	
	protected function _getPropertyAccess(M_CodeVariable $property) {
		// Default the access to PROTECTED:
		$tmp = $property->getAccess();
		if(! $tmp) {
			$tmp = M_CodeAccess::TYPE_PROTECTED;
		}
		return $tmp;
	}
	
	protected function _getMethodAccess(M_CodeFunction $method) {
		// Default the access to PUBLIC:
		$tmp = $method->getAccess();
		if(! $tmp) {
			$tmp = M_CodeAccess::TYPE_PUBLIC;
		}
		return $tmp;
	}
}