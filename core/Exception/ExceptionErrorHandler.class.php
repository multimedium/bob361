<?php
/**
 * M_ExceptionErrorHandler
 * 
 * Used to convert "old school" PHP errors to full-blown exceptions. This allows
 * an application to also log traditional errors, warnings and notices by converting
 * them to comprehensive exceptions. For more info, read {@link MI_ExceptionLogger}
 *
 * @package Core
 */
class M_ExceptionErrorHandler {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Handle errors?
	 * 
	 * @see M_ExceptionErrorHandler::setHandleErrors()
	 * @access private
	 * @var bool
	 */
	private static $_handleErrors = TRUE;
	
	/**
	 * Handle warnings?
	 * 
	 * @see M_ExceptionErrorHandler::setHandleWarnings()
	 * @access private
	 * @var bool
	 */
	private static $_handleWarnings = FALSE;
	
	/**
	 * Handle notices?
	 * 
	 * @see M_ExceptionErrorHandler::setHandleNotices()
	 * @access private
	 * @var bool
	 */
	private static $_handleNotices = FALSE;
	
	/* -- SETTERS -- */
	
	/**
	 * Set as error handler
	 * 
	 * Will initiate the M_ExceptionErrorHandler class as the error handler in PHP
	 * 
	 * @static
	 * @access public
	 * @return void
	 */
	public static function start() {
		set_error_handler(array('M_ExceptionErrorHandler', 'handleError'));
	}
	
	/**
	 * Handle errors?
	 * 
	 * If you want errors to be converted to exceptions, you should provide
	 * (boolean) TRUE as an argument to this method. If you do not want errors 
	 * to be converted to exceptions, provide with (boolean) FALSE instead.
	 * 
	 * @static
	 * @see M_ExceptionErrorHandler::start()
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE if you want errors to be converted to exceptions
	 * @return void
	 */
	public static function setHandleErrors($flag) {
		self::$_handleErrors = (bool) $flag;
	}
	
	/**
	 * Handle warnings?
	 * 
	 * If you want warnings to be converted to exceptions, you should provide
	 * (boolean) TRUE as an argument to this method. If you do not want warnings 
	 * to be converted to exceptions, provide with (boolean) FALSE instead.
	 * 
	 * @static
	 * @see M_ExceptionErrorHandler::start()
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE if you want warnings to be converted to exceptions
	 * @return void
	 */
	public static function setHandleWarnings($flag) {
		self::$_handleWarnings = (bool) $flag;
	}
	
	/**
	 * Handle notices?
	 * 
	 * If you want notices to be converted to exceptions, you should provide
	 * (boolean) TRUE as an argument to this method. If you do not want notices 
	 * to be converted to exceptions, provide with (boolean) FALSE instead.
	 * 
	 * @static
	 * @see M_ExceptionErrorHandler::start()
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE if you want warnings to be converted to exceptions
	 * @return void
	 */
	public static function setHandleNotices($flag) {
		self::$_handleNotices = (bool) $flag;
	}
	
	/**
	 * Unset as error handler
	 * 
	 * On the contrary to {@link M_ExceptionErrorHandler::initialize()}, will cause 
	 * the M_ExceptionErrorHandler to stop handling errors, warnings and/or notices 
	 * that are being caused in PHP
	 * 
	 * @static
	 * @access public
	 * @return void
	 */
	public static function stop() {
		restore_error_handler();
	}
	
	/**
	 * Handle error
	 * 
	 * Is configured as the error handler, by {@link M_ExceptionErrorHandler::start()}
	 * 
	 * @access public
	 * @param integer $errno
	 * @param string $errstr
	 * @param string $errfile
	 * @param integer $errline
	 * @return void
	 */
	public static function handleError($errno, $errstr, $errfile, $errline) {
		// Prepare a boolean, which tells us whether or not to convert to exception
		switch ($errno) {
			// WARNING
			case E_USER_WARNING:
			case E_CORE_WARNING:
			case E_COMPILE_WARNING:
			case E_WARNING:
				$b = self::$_handleWarnings;
				break;
			
			// NOTICE
			case E_USER_NOTICE:
			case E_NOTICE:
			case E_STRICT:
				$b = self::$_handleNotices;
				break;
			
			// ERROR
			case E_USER_ERROR:
			case E_COMPILE_ERROR:
			case E_CORE_ERROR:
			case E_ERROR:
			case E_PARSE:
			case E_RECOVERABLE_ERROR:
			default:
				$b = self::$_handleErrors;
				break;
		}
		
		// If the error/warning/notice is not to be converted into an exception:
		if(! $b) {
			// Execute PHP's internal error handler
			// (by returning FALSE)
			return FALSE;
		}
		
		// Throw an exception
		throw new M_Exception($errstr, $errno, $errfile, $errline);
		
		// Don't execute PHP internal error handler:
		// (returning TRUE will stop PHP from handling the error)
		return TRUE;
	}
}