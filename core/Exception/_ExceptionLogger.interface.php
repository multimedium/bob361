<?php
/**
 * MI_ExceptionLogger interface
 * 
 * In order to log exceptions, you should implement this interface or use a
 * core class such as {@link M_ExceptionLoggerDb}. To start logging exceptions
 * in your application, use {@link M_Exception::setExceptionLogger()}
 * 
 * @package Core
 */
interface MI_ExceptionLogger {
	
	/* -- SETTERS -- */
	
	/**
	 * Log an exception
	 * 
	 * @access public
	 * @param Exception $exception
	 * 		The exception to be logged
	 * @return boolean $flag
	 * 		Returns TRUE if successful, FALSE if not
	 */
	public function logException(Exception $exception);
	
	/**
	 * Clear (remove) exceptions
	 * 
	 * ... will remove the exceptions from logging storage. You may remove exceptions
	 * from a website/application at a given URL, or of a given type (code)
	 * 
	 * @access public
	 * @param M_Uri $uri
	 * 		The website/application that has thrown the exception. This website/
	 * 		application is defined by a URI
	 * @param string $code
	 * 		You can select the type of exceptions, by providing the exception code
	 * @return boolean $flag
	 * 		Returns TRUE if successful, FALSE if not
	 */
	public function clearExceptions(M_Uri $uri = NULL, $code = NULL);
	
	/* -- GETTERS -- */
	
	/**
	 * Get number of exceptions
	 * 
	 * Will return the the total number of exceptions that has been logged by
	 * this logger.
	 * 
	 * Note that you may (optionally) provide with an instance of {@link M_Uri},
	 * in order to get the number of exceptions that has been originated by a
	 * website/application at that given URI.
	 * 
	 * @access public
	 * @param M_Uri $uri
	 * 		The website/application that has thrown the exception. This website/
	 * 		application is defined by a URI
	 * @param string $code
	 * 		You can select the type of exceptions, by providing the exception code
	 * @return integer $number
	 * 		The number of exceptions that has been logged
	 */
	public function getNumberOfExceptions(M_Uri $uri = NULL, $code = NULL);
	
	/**
	 * Get exceptions
	 * 
	 * Will provide with the collection of exceptions that has been logged by
	 * this logger. 
	 * 
	 * Note that you may (optionally) provide with an instance of {@link M_Uri},
	 * in order to get the exceptions that have been originated by a website/
	 * application at that given URI.
	 * 
	 * Note that you may provide with a given maximum, to limit the number of 
	 * exceptions in the collection with. Provide 0 (ZER0) as maximum in order 
	 * to fetch the complete collection.
	 * 
	 * @access public
	 * @param M_Uri $uri
	 * 		The website/application that has thrown the exception(s). This website/
	 * 		application is defined by a URI
	 * @param string $code
	 * 		You can select the type of exceptions, by providing the exception code
	 * @param integer $maximum
	 * @return ArrayIterator $exceptions
	 * 		The collection of {@link Exception} objects
	 */
	public function getExceptions(M_Uri $uri = NULL, $code = NULL, $maximum = 0);
	
	/**
	 * Get exception
	 * 
	 * Will provide with the latest exception that has been logged by
	 * this logger.
	 * 
	 * Note that you may (optionally) provide with an instance of {@link M_Uri},
	 * in order to get the latest exception that has been originated by a website/
	 * application at that given URI.
	 * 
	 * @access public
	 * @param M_Uri $uri
	 * 		The website/application that has thrown the exception. This website/
	 * 		application is defined by a URI
	 * @param string $code
	 * 		You can select the type of exception, by providing the exception code
	 * @return Exception $exception
	 * 		Will return NULL instead, if no exception could have been found
	 */
	public function getException(M_Uri $uri = NULL, $code = NULL);
}