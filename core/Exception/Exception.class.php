<?php
/**
 * M_Exception class
 * 
 * This class is used to throw exceptions in the core and application environment.
 * Also, this class is the mother class to all other exceptions, thrown by core 
 * and Application-Specific API's.
 *
 * @package Core
 */
class M_Exception extends Exception {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Exception logger
	 * 
	 * This (static) property stores the logger, which is in charge of logging 
	 * the exceptions that are thrown in the application.
	 * 
	 * @static
	 * @access private
	 * @return MI_ExceptionLogger
	 */
	private static $_logger;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_Exception
	 */
	public function __construct($message, $code = NULL, $file = NULL, $line = NULL) {
		// Construct the exception
		parent::__construct($message, $code);
		if($file) {
			$this->file = $file;
		}
		if($line) {
			$this->line = $line;
		}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Exception logger
	 * 
	 * @static
	 * @see M_Exception::setExceptionLogger()
	 * @access publoc
	 * @return MI_ExceptionLogger
	 */
	public static function getExceptionLogger() {
		return self::$_logger;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set Exception logger
	 * 
	 * In order to log exceptions, you should implement the {@link MI_ExceptionLogger} 
	 * or use a core class such as {@link M_ExceptionLoggerDb}. To start logging 
	 * exceptions in your application, call this method.
	 * 
	 * @static 
	 * @access public
	 * @param MI_ExceptionLogger $logger
	 * 		The object that is to be used to log the exceptions
	 * @return void
	 */
	public static function setExceptionLogger(MI_ExceptionLogger $logger) {
		self::$_logger = $logger;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Logs the exception
	 * 
	 * Call this function in the handleError method of your pageController,
	 * clientController, ...
	 * 
	 * @access public
	 * @return void
	 */
	public function log() {
		// if a logger has been provided:
		if(self::$_logger) {
			// Log:
			self::$_logger->logException($this);
		}
	}
}