<?php
/**
 * M_ExceptionLogger
 * 
 * ... is an abstract class that implements some basic functionality that is
 * dictated by {@link MI_ExceptionLogger}
 * 
 * @abstract 
 * @package Core
 */
abstract class M_ExceptionLogger extends M_Object implements MI_ExceptionLogger {
	/**
	 * Get exception
	 * 
	 * Will provide with the latest exception that has been logged by
	 * this logger.
	 * 
	 * Note that you may (optionally) provide with an instance of {@link M_Uri},
	 * in order to get the latest exception that has been originated by a website/
	 * application at that given URI.
	 * 
	 * @see MI_ExceptionLogger::getException()
	 * @access public
	 * @param M_Uri $uri
	 * 		The website/application that has thrown the exception. This website/
	 * 		application is defined by a URI
	 * @return Exception $exception
	 * 		Will return NULL instead, if no exception could have been found
	 */
	public function getException(M_Uri $uri = NULL, $code = NULL) {
		return $this->getExceptions($uri, $code, 1)->current();
	}
}