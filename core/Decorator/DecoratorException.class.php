<?php
/**
 * M_DecoratorException class
 * 
 * Exceptions thrown by {@link M_Decorator}.
 *
 * @package Core
 */
class M_DecoratorException extends M_Exception {
}
?>