<?php
/**
 * This file uses the PHPUnit framework to execute test files. It's possible to
 * test core files as well as application files
 * 
 * @example PHPUnit.php?test=DbSelect.test.php&core=true
 * 
 * By adding the core parameter the correct path will automaticcally be used
 * By removing the core parameter, the tests folder in the application enverionment
 * will be used
 */

// Load M_Loader
require_once 'Loader.class.php';

// Set M_Loader as the autoloader
M_Loader::setAsAutoloader();

// Set include path
set_include_path(M_Loader::getAbsolute('PHPUnit'));

// Core testing?
if (M_Request::getVariable('core') !== false)
{
	$testPath = M_Loader::getAbsolute('tests');
}
else
{
	$testPath = M_Loader::getAbsolute( M_Loader::getApplicationFolder()) . '/tests';
}

// Check if file exists
$filePath = $testPath . '/' . M_Request::getVariable('file');
if (!file_exists($filePath))
{
	throw new M_Exception('File does not exist:' . $filePath);
	die;
}

// Pass test argument
$_SERVER['argv'][1] = $filePath;

// Require phpunit
require_once ( 'phpunit.php');