<?php
/**
 * M_Color class
 * 
 * The M_Color class provides with methods to work with colors.
 * It provides tools to exchange colors in different formats
 * (hex, rgb, int), transform colors, etc.
 * 
 * @package Core
 */
class M_Color {
	/**
	 * Constant to address color channel: RED
	 * 
	 * This constant is used to address the red color channel,
	 * and to improve code readability.
	 */
	const RED = 0;
	
	/**
	 * Constant to address color channel: GREEN
	 * 
	 * This constant is used to address the green color channel,
	 * and to improve code readability.
	 */
	const GREEN = 1;
	
	/**
	 * Constant to address color channel: BLUE
	 * 
	 * This constant is used to address the blue color channel,
	 * and to improve code readability.
	 */
	const BLUE = 2;
	
	/**
	 * Constant to address color channel: ALPHA
	 * 
	 * This constant is used to address the ALPHA color channel,
	 * and to improve code readability.
	 */
	const ALPHA = 3;
	
	/**
	 * Constant to address RGBA Matrix
	 * 
	 * This constant is used to address the color transformation matrix
	 * that transforms a color to grayscale. Read the documentation on
	 * {@link M_Color::getRGBAMatrix()} and {@link M_Color::applyRGBAMatrix()}
	 * for more info on RGBA transformation matrixes.
	 */
	const MATRIX_GRAYSCALE = 'grayscale';
	
	/**
	 * Constant to address RGBA Matrix
	 * 
	 * This constant is used to address the color transformation matrix
	 * that transforms a color to sepia. Read the documentation on
	 * {@link M_Color::getRGBAMatrix()} and {@link M_Color::applyRGBAMatrix()}
	 * for more info on RGBA transformation matrixes.
	 */
	const MATRIX_SEPIA = 'sepia';
	
	/**
	 * Constant to address RGBA Matrix
	 * 
	 * This constant is used to address the color transformation matrix
	 * that transforms a color to its negative. Read the documentation on
	 * {@link M_Color::getRGBAMatrix()} and {@link M_Color::applyRGBAMatrix()}
	 * for more info on RGBA transformation matrixes.
	 * 
	 * When applying the color's digital negative, you are "inverting" the
	 * image.
	 */
	const MATRIX_NEGATIVE = 'negative';
	
	/**
	 * Channels
	 * 
	 * This property holds the values for each of the channels: Red, Green
	 * and Blue. The purpose of this property is to define the current
	 * color, and to allow for effects to be applied on each of the channels
	 * of the original color.
	 * 
	 * @access private
	 * @var array
	 */
	private $_channels;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * Note that you do not necessarily have to pass in the color value. 
	 * If not provided, the class will instantiate a M_Color object with 
	 * the channels RGB set to 0, 0, and 0 respectively (=black).
	 * 
	 * If, however, you wish to construct an object that represents a 
	 * given color, you can choose to provide that definition in either of
	 * the following ways:
	 * 
	 * - a string
	 * - an array with numerical values, each ranging from 0 to 255
	 * 
	 * If constructed with a string, the M_Color class expects a 6 character
	 * long hex value: 2 letters ranging from 0 to F for each of the RGB
	 * channels.
	 * 
	 * Example 1, construct with a string:
	 * <code>
	 *    // Construct the pure red color:
	 *    $color = new M_Color('ff0000');
	 * </code>
	 * 
	 * If constructed with an array, the M_Color class expects a numerical
	 * value, ranging from 0 to 255, for each of the RGB Channels.
	 * 
	 * Example 2, construct with array of numerical values:
	 * <code>
	 *    // Construct the pure red color
	 *    $color = new M_Color(array(255, 0, 0));
	 * </code>
	 * 
	 * @access public
	 * @param string|array $color
	 * 		The color definition
	 * @return M_Color
	 */
	public function __construct($color = NULL) {
		if($color != NULL) {
			if(is_array($color)) {
				if(count($color) >= 3) {
					$this->setRGB($color[0], $color[1], $color[2]);
					if(array_key_exists(3, $color)) {
						$this->_channels[self::ALPHA] = $color[3];
					} else {
						$this->_channels[self::ALPHA] = 1;
					}
				}
			} else {
				$this->setHex($color);
				$this->setAlpha(1);
			}
		} else {
			$this->_channels = array(
				self::RED => 0,
				self::GREEN => 0,
				self::BLUE => 0,
				self::ALPHA => 1
			);
		}
	}
	
	/**
	 * Construct random color
	 * 
	 * @static
	 * @access public
	 * @return M_Color
	 */
	public static function constructRandom() {
		return new self(array(
			rand(0, 255),
			rand(0, 255),
			rand(0, 255),
			1
		));
	}
	
	/**
	 * Construct a color with a LONG integer
	 * 
	 * @static
	 * @access public
	 * @param int $long
	 * @return M_Color
	 */
	public static function constructWithLongInteger($long) {
		return new M_Color(array(
			$long % 256,
			($long >> 8) % 256,
			($long >> 16) % 256,
			1
		));
	}
	
	/* -- SETTERS/GETTERS -- */
	
	/**
	 * Set RGB channels
	 * 
	 * This method will set the Red, Green, and Blue channels to the 
	 * values provided. Note that this method expects a numerical value
	 * for each of the channels, ranging from 0 to 255.
	 * 
	 * @access public
	 * @param integer $r
	 * 		The value for the red channel; 0 <= $r <= 255
	 * @param integer $g
	 * 		The value for the green channel; 0 <= $g <= 255
	 * @param integer $b
	 * 		The value for the blue channel; 0 <= $b <= 255
	 * @return void
	 */
	public function setRGB($r, $g, $b) {
		$c = (      is_numeric($r) && $r >= 0 && $r <= 255);
		$c = ($c && is_numeric($g) && $g >= 0 && $g <= 255);
		$c = ($c && is_numeric($b) && $b >= 0 && $b <= 255);
		if(!$c) {
			throw new M_ColorException('Expecting RGB values in the range 0 - 255');
		} else {
			$this->_channels = array(
				self::RED => (int) $r,
				self::GREEN => (int) $g,
				self::BLUE => (int) $b
			);
		}
	}
	
	/**
	 * Set channel
	 * 
	 * This method will set the value of a selected channel. If you
	 * want to set the value of all channels, you should check the 
	 * {@link M_Color::setRGB()} method.
	 * 
	 * Example 1:
	 * <code>
	 *    $color = new M_Color; // Now, it's black
	 *    $color->setChannel(M_Color::RED, 255); // Now, ir's pure red
	 *    $color->setChannel(M_Color::GREEN, 255); // Now, ir's yellow
	 * </code>
	 * 
	 * NOTE:
	 * Note that this method expects a numerical value ranging from
	 * 0 to 255.
	 * 
	 * @access public
	 * @param integer $channel
	 * 		The channel you are changing the value of
	 * @param integer $value
	 * 		The numerical value of the channel, from 0 to 255.
	 * @return void
	 */
	public function setChannel($channel, $value) {
		$this->_channels[$channel] = $value;
	}
	
	/**
	 * Get the RGB Channels
	 * 
	 * This method will return the values of the Red, Green and Blue
	 * channels.
	 * 
	 * @access public
	 * @return array
	 */
	public function getRGB() {
		return $this->_channels;
	}
	
	/**
	 * Get a channel
	 * 
	 * This method will return the value of the requested color channel.
	 * Note that this method will throw an exception, if the requested
	 * channel could not have been found.
	 * 
	 * Example 1:
	 * <code>
	 *    $color = new M_Color('ff0000');
	 *    echo $color->getChannel(M_Color::RED);
	 * </code>
	 * 
	 * Example 1 will print the following value:
	 * <code>
	 *    255
	 * </code>
	 * 
	 * Note that this method will return a number that ranges from 0 to 
	 * 255.
	 * 
	 * @access public
	 * @throw M_ColorException
	 * @param integer $channel
	 * 		The color channel
	 * @return integer
	 */
	public function getChannel($channel) {
		if(isset($this->_channels[$channel])) {
			return $this->_channels[$channel];
		} else {
			throw new M_ColorException(sprintf('Undefined channel "%s"', $channel));
		}
	}
	
	/**
	 * Get RGB channels from integer value
	 * 
	 * This method will extract the RGB channels out of an integer value
	 * that represents a color. Typically, such an integer value is used
	 * by image libraries such as GD(2) to represent a color in the image.
	 * This method uses bitshifting and masking to get the Red, Green and
	 * Blue channel values.
	 * 
	 * The return value of this method is an array, that contains numerical
	 * values for each of the color channels (each ranging from 0 to 255)
	 * 
	 * @access public
	 * @return array
	 */
	public static function getRGBInInteger($int) {
		$r = ($int >> 16) & 0xFF;
		$g = ($int >> 8) & 0xFF;
		$b = $int & 0xFF;
		return array(
			self::RED => $r,
			self::GREEN => $g,
			self::BLUE => $b
		);
	}
	
	/**
	 * Set hex value
	 * 
	 * This method will set the color, based on a hex value. A hex value
	 * is a 6 character long string value that represents the three color
	 * channels. For example, ff0000 represents red:
	 * 
	 * - Red: ff (255)
	 * - Green: 00 (0)
	 * - Blue: 00 (0)
	 * 
	 * Example 1
	 * <code>
	 *    $color = new M_Color; // Now, it's black
	 *    $color->setHex('00ff00'); // Now, it's green
	 * </code>
	 * 
	 * @access public
	 * @param string $string
	 * 		The hex value
	 * @return void
	 */
	public function setHex($string, $alpha = 0) {
		$this->_channels = $this->getRGBFromHex($string);
		$this->setAlpha($alpha);
	}
	
	/**
	 * Get the hex value
	 * 
	 * This method will return the hex value (string) that represents 
	 * the current color.
	 * 
	 * Example 1
	 * <code>
	 *    $color = new M_Color(array(255, 0, 0));
	 *    echo $color->getHex(); // will output "ff0000"
	 * </code>
	 * 
	 * Note that you can choose whether or not to include the typical
	 * # character at the beginning of the hex value. If TRUE is passed
	 * into this method, the # character will be prepended. If FALSE is
	 * passed in, only the 6 character long hex value is returned. By
	 * default, this method does NOT prepend the character.
	 * 
	 * Example 2
	 * <code>
	 *    $color = new M_Color(array(255, 0, 0));
	 *    echo $color->getHex(TRUE); // will output "#ff0000"
	 * </code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getHex($includeAnchor = FALSE) {
		$output = '';
		if($includeAnchor) {
			$output .= '#';
		}
		$output .= str_pad(dechex($this->_channels[self::RED]), 2, '0', STR_PAD_LEFT) . 
		           str_pad(dechex($this->_channels[self::GREEN]), 2, '0', STR_PAD_LEFT) . 
		           str_pad(dechex($this->_channels[self::BLUE]), 2, '0', STR_PAD_LEFT);
		return $output;
	}
	
	/**
	 * Extract RGB values from hex value
	 * 
	 * This method will extract the RGB values (numerical values, ranging 
	 * from 0 to 255) from a given hex value. The method will return an 
	 * array with the numerical values for each of the channels.
	 * 
	 * Note that, if the provided hex value is not a valid hex value,
	 * this method will throw an {@link M_ColorException}.
	 * 
	 * @access public
	 * @throw M_ColorException
	 * @param string $string
	 * 		The hex value
	 * @return array
	 */
	public function getRGBFromHex($string) {
		$match = array();
		
		// If the provided string starts with #, remove it:
		if(! strncmp($string, '#', 1)) {
			$string = substr($string, 1);
		}
		
		// Check for 6 hexadecimal characters:
		if(preg_match('/^([0-9A-F]{6})$/i', trim($string), $match)) {
			// If we find a match, we store the RGB Coords
			return array(
				self::RED => hexdec($match[1]{0} . $match[1]{1}),
				self::GREEN => hexdec($match[1]{2} . $match[1]{3}),
				self::BLUE => hexdec($match[1]{4} . $match[1]{5})
			);
		} else {
			// If not, we throw an exception
			throw new M_ColorException(sprintf('Invalid hex value: "%s"', $string));
		}
	}
	
	/**
	 * Extract hex value from RGB channels
	 * 
	 * This method will extract the hex value from the provided RGB
	 * channels (numerical values, ranging from 0 to 255). The method 
	 * will return a string with the hex value that represents the 
	 * provided color definition.
	 * 
	 * @access public
	 * @param integer $r
	 * 		The value for the red channel; 0 <= $r <= 255
	 * @param integer $g
	 * 		The value for the green channel; 0 <= $g <= 255
	 * @param integer $b
	 * 		The value for the blue channel; 0 <= $b <= 255
	 * @return string
	 */
	public function getHexFromRGB($r, $g, $b) {
		return  str_pad(dechex($r), 2, '0', STR_PAD_LEFT) . 
		        str_pad(dechex($g), 2, '0', STR_PAD_LEFT) . 
		        str_pad(dechex($b), 2, '0', STR_PAD_LEFT);
	}
	
	/**
	 * TODO: get CMYK
	 * 
	 * This method will return the CMYK definition of the color.
	 * 
	 * @access public
	 * @return array
	 */
	public function getCMYK() {
	}
	
	/**
	 * Brighten the color
	 * 
	 * This method will brighten the current color by a given 
	 * percentage/factor
	 * 
	 * @access public
	 * @return void
	 */
	public function brighten($percentage = 0.25) {
		$r = $this->_channels[self::RED]   + (255 * $percentage);
		$g = $this->_channels[self::GREEN] + (255 * $percentage);
		$b = $this->_channels[self::BLUE]  + (255 * $percentage);
		$this->_channels = array(
			self::RED => $r > 255 ? 255 : round($r),
			self::GREEN => $g > 255 ? 255 : round($g),
			self::BLUE => $b > 255 ? 255 : round($b)
		);
		return $this;
	}
	
	/**
	 * is white?
	 * 
	 * This method will check whether or not the color is white. If the
	 * color is white, this method will return TRUE. If the color is not
	 * white, this method will return FALSE.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isWhite() {
		return (
			$this->_channels[self::RED]   == 255 &&
			$this->_channels[self::GREEN] == 255 &&
			$this->_channels[self::BLUE]  == 255
		);
	}
	
	/**
	 * Darken the color
	 * 
	 * This method will darken the current color by a given 
	 * percentage/factor
	 * 
	 * @access public
	 * @return void
	 */
	public function darken($percentage = 0.25) {
		$r = $this->_channels[self::RED]   - (255 * $percentage);
		$g = $this->_channels[self::GREEN] - (255 * $percentage);
		$b = $this->_channels[self::BLUE]  - (255 * $percentage);
		$this->_channels = array(
			self::RED => $r < 0 ? 0 : round($r),
			self::GREEN => $g < 0 ? 0 : round($g),
			self::BLUE => $b < 0 ? 0 : round($b)
		);
		return $this;
	}
	
	/**
	 * is black?
	 * 
	 * This method will check whether or not the color is black. If the
	 * color is black, this method will return TRUE. If the color is not
	 * black, this method will return FALSE.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isBlack() {
		return (
			$this->_channels[self::RED]   == 0 &&
			$this->_channels[self::GREEN] == 0 &&
			$this->_channels[self::BLUE]  == 0
		);
	}
	
	/**
	 * Apply an RGBA Matrix to the color
	 * 
	 * An RGBA Matrix is an array of 20 items for 4 x 5 color 
	 * transform. This method separates the source color into its 
	 * red, green, blue, and alpha components. To calculate the 
	 * result of each of the four channels, the value of each pixel 
	 * in the image is multiplied by the values in the transformation 
	 * matrix. An offset, between -255 and 255, can optionally be 
	 * added to each result (the fifth item in each row of the matrix).
	 * 
	 * For each color value in the matrix array, a value of 1 is equal 
	 * to 100% of that channel being sent to the output, preserving the 
	 * value of the color channel. 
	 * 
	 * Example 1, A matrix that adjusts only the alpha component:
	 * 
	 * <code>
	 *    1 0 0 0 0
	 *    0 1 0 0 0
	 *    0 0 1 0 0
	 *    0 0 0 N 0  (where N is between 0.0 and 1.0)
	 * </code>
	 * 
	 * Example 2, A matrix to convert a pixel to grayscale
	 * 
	 * <code>
	 *    0.308600038290024 0.609399974346161 0.0820000022649765 0 0
	 *    0.308600008487701 0.609399974346161 0.0820000022649765 0 0
	 *    0.308600008487701 0.609399974346161 0.0820000246167183 0 0
	 *    0                 0                 0                  1 0
	 * </code>
	 * 
	 * @access public
	 * @param array $matrix
	 * 		The RGBA matrix, or transformation matrix
	 * @return void
	 */
	public function applyRGBAMatrix(array $matrix) {
		// The result of the formula is the following:
		// redResult   = (a[0]  * srcR) + (a[1]  * srcG) + (a[2]  * srcB) + (a[3]  * srcA) + a[4]
		// greenResult = (a[5]  * srcR) + (a[6]  * srcG) + (a[7]  * srcB) + (a[8]  * srcA) + a[9]
		// blueResult  = (a[10] * srcR) + (a[11] * srcG) + (a[12] * srcB) + (a[13] * srcA) + a[14]
		// alphaResult = (a[15] * srcR) + (a[16] * srcG) + (a[17] * srcB) + (a[18] * srcA) + a[19]
		$r = ($this->_channels[self::RED] * $matrix[0])  + ($this->_channels[self::GREEN] * $matrix[1])  + ($this->_channels[self::BLUE] * $matrix[2])  + $matrix[4];
		$g = ($this->_channels[self::RED] * $matrix[5])  + ($this->_channels[self::GREEN] * $matrix[6])  + ($this->_channels[self::BLUE] * $matrix[7])  + $matrix[9];
		$b = ($this->_channels[self::RED] * $matrix[10]) + ($this->_channels[self::GREEN] * $matrix[11]) + ($this->_channels[self::BLUE] * $matrix[12]) + $matrix[14];
		$this->_channels = array(
			self::RED => ($r < 0 ? 0 : ($r > 255) ? 255 : round($r)),
			self::GREEN => ($g < 0 ? 0 : ($g > 255) ? 255 : round($g)),
			self::BLUE => ($b < 0 ? 0 : ($b > 255) ? 255 : round($b))
		);
	}
	
	/**
	 * Get RGBA Matrix
	 * 
	 * This method is used to fetch a predefined color transformation 
	 * matrix, also called RGBA Matrix. To learn more about RGBA Matrixes,
	 * read the documentation on {@link M_Color::applyRGBAMatrix()}.
	 * 
	 * Example 1, get the RGBA Matrix to convert to grayscale:
	 * <code>
	 *    print_r(M_Color::getRGBAMatrix(M_Color::MATRIX_GRAYSCALE));
	 * </code>
	 * 
	 * Example 1 would output the following array:
	 * <code>
	 *    Array (
	 *       0  => 0.308600038290024,
	 *       1  => 0.609399974346161,
	 *       2  => 0.0820000022649765,
	 *       3  => 0,
	 *       4  => 0,
	 *       5  => 0.308600008487701,
	 *       6  => 0.609399974346161,
	 *       7  => 0.0820000022649765,
	 *       8  => 0,
	 *       9  => 0,
	 *       10 => 0.308600008487701,
	 *       11 => 0.609399974346161,
	 *       12 => 0.0820000246167183,
	 *       13 => 0,
	 *       14 => 0,
	 *       15 => 0,
	 *       16 => 0,
	 *       17 => 0,
	 *       18 => 1,
	 *       19 => 0
	 *    )
	 * </code>
	 * 
	 * which represents the following RGBA Matrix:
	 * 
	 * <code>
	 *    0.308600038290024 0.609399974346161 0.0820000022649765 0 0
	 *    0.308600008487701 0.609399974346161 0.0820000022649765 0 0
	 *    0.308600008487701 0.609399974346161 0.0820000246167183 0 0
	 *    0                 0                 0                  1 0
	 * </code>
	 * 
	 * NOTE:
	 * This method returns the RGBA Matrix in the form of an array. 
	 * This array is in the same format as expected by the method
	 * {@link M_Color::applyRGBAMatrix()}.
	 * 
	 * NOTE:
	 * If the requested matrix could not have been found, this method
	 * will throw a {@link M_ColorException} to inform about the bad
	 * usage of this method :)
	 * 
	 * @access public
	 * @throws M_ColorException
	 * @param string $which
	 * 		The requested RGBA matrix
	 * @return array
	 */
	public function getRGBAMatrix($which) {
		switch($which) {
			case self::MATRIX_GRAYSCALE:
				return array(
					0.308600038290024, 0.609399974346161, 0.0820000022649765, 0, 0,
					0.308600008487701, 0.609399974346161, 0.0820000022649765, 0, 0,
					0.308600008487701, 0.609399974346161, 0.0820000246167183, 0, 0,
					0                , 0                , 0                 , 1, 0
				);
			
			case self::MATRIX_SEPIA:
				return array(
					0.608600038290024, 0.609399974346161, 0.0820000022649765, 0, 45,
					0.608600008487701, 0.609399974346161, 0.0820000022649765, 0, 25,
					0.608600008487701, 0.609399974346161, 0.0820000246167183, 0, 0,
					0                , 0                , 0                 , 1, 0
				);
			
			case self::MATRIX_NEGATIVE:
				return array(
					-1, 1 , 1 , 0, 0,
					1 , -1, 1 , 0, 0,
					1 , 1 , -1, 0, 0,
					0 , 0 , 0 , 1, 0
				);
			
			default:
				throw new M_ColorException(sprintf('Undefined RGBA Matrix "%s"', $which));
		}
	}
	
	/**
	 * Get HSL
	 * 
	 * Will convert the current color to its hue, saturation and lightness
	 * values, returning them as an array that looks like the following:
	 * 
	 * <code>
	 *	array($hue, $saturation, $lightness)
	 * </code>
	 * 
	 * @access public
	 * @return array
	 */
	public function getHsl() {
		// Fetch the RGB values
		$rgb = $this->getRGB();
		
		// Fetch the 3 separate colors
		$red = $rgb[0];
		$green = $rgb[1];
		$blue = $rgb[2];
		
		// Convert them to HSL
		$minColor = min($red, $green, $blue);
		$maxColor = max($red, $green, $blue);
		$deltaMax = $maxColor - $minColor;

		// Calculate the lightness
		$lightness = ($maxColor + $minColor) / 510;

		if(0 == $deltaMax) {
			$hue = 0;
			$saturation = 0;
		} else {
			if(0.5 > $lightness){
				$saturation = $deltaMax / ($maxColor + $minColor);
			} else{
				$saturation = $deltaMax / (510 - $maxColor - $minColor);
			}

			if($maxColor == $red) {
				$hue = ($green - $blue) / (6.0 * $deltaMax);
			} else if($maxColor == $green) {
				$hue = 1 / 3 + ($blue - $red) / (6.0 * $deltaMax);
			} else {
				$hue = 2 / 3 + ($red - $green) / (6.0 * $deltaMax);
			}

			if(0 > $hue) $hue += 1;
			if(1 < $hue) $hue -= 1;
		}
		
		// Return the HSL
		return array($hue, $saturation, $lightness);
	}
	
	/**
	 * Get Hue
	 * 
	 * Will calculate and return the hue of this color, as a value
	 * between 0 and 1
	 * 
	 * @uses Color::getHsl()
	 * @access public
	 * @return float
	 */
	public function getHue() {
		$hsl = $this->getHsl();
		return $hsl[0];
	}
	
	/**
	 * Get Saturation
	 * 
	 * Will calculate and return the saturation of this color, as a value
	 * between 0 and 1
	 * 
	 * @uses Color::getHsl()
	 * @access public
	 * @return float
	 */
	public function getSaturation() {
		$hsl = $this->getHsl();
		return $hsl[1];
	}
	
	/**
	 * Get Lightness
	 * 
	 * Will calculate and return the lightness of this color, as a value
	 * between 0 and 1
	 * 
	 * @uses Color::getHsl()
	 * @access public
	 * @return float
	 */
	public function getLightness() {
		$hsl = $this->getHsl();
		return $hsl[2];
	}
	
	/**
	 * Get Ideal Foreground Color
	 * 
	 * Based on the lightness of the current color, we will determine wheter
	 * the ideal foreground color would be black or white, so it is most
	 * readible, returning that color.
	 * 
	 * @access public
	 * @return M_Color 
	 */
	public function getIdealForegroundColor() {
		// Based on the lightness of the color, the ideal foreground color
		// is either black or white:
		$foregroundColor = $this->getLightness() < 0.495 ? 'ffffff' : '000000';
		
		// Return a new M_Color with that selected color
		return new M_Color($foregroundColor);
	}
	
	/**
	 * Get alpha channel
	 * 
	 * @access public
	 * @return float
	 *		The alpha value, as a float between 0 and 1
	 */
	public function getAlpha() {
		return $this->getChannel(self::ALPHA);
	}
	
	/**
	 * Set alpha channel
	 * 
	 * The alpha, as a floated value between 0 and 1
	 * 
	 * 0 = fully transparent
	 * 1 = fully opaque
	 * @see http://en.wikipedia.org/wiki/RGBA_color_space
	 * 
	 * @access public
	 * @param float $alpha
	 * @return float
	 */
	public function setAlpha($alpha) {
		$this->setChannel(self::ALPHA, $alpha);
	}
}