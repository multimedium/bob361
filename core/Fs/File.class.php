<?php
/**
 * M_File class
 * 
 * M_File is an implementation of {@link MI_FsItemFile} that represents 
 * a file on the "local" file system (server).
 * 
 * @package Core
 */
class M_File extends M_FsItem implements MI_FsItemFile {
	/**
	 * Name
	 * 
	 * This property stores the name of the file.
	 * 
	 * @see M_File::getName()
	 * @see M_File::getBasename()
	 * @see M_File::setName()
	 * @access protected
	 * @var string
	 */
	protected $_name;
	
	/**
	 * Path
	 * 
	 * This property stores the complete path to the file
	 * 
	 * @see M_File::setPath()
	 * @see M_File::getPath()
	 * @access protected
	 * @var string
	 */
	protected $_path;
	
	/**
	 * Extension
	 * 
	 * This property stores the extension of the file
	 *
	 * @see M_File::getFileExtension()
	 * @see M_File::setFileExtension()
	 * @access protected
	 * @var string
	 */
	protected $_extension;
	
	/**
	 * Filename
	 * 
	 * This property stores the filename of the file (without extension)
	 * 
	 * @see M_File::getFilename()
	 * @access protected
	 * @var string
	 */
	protected $_filename;
	
	/**
	 * Get item type
	 * 
	 * @see MI_FsItem::getFsItemType()
	 * @access public
	 * @return string
	 */
	public function getFsItemType() {
		return MI_Fs::TYPE_FILE;
	}
	
	/**
	 * Set path
	 * 
	 * @see MI_FsItem::setPath()
	 * @access public
	 * @param string $path
	 * 		The new path of the item
	 * @return void
	 */
	public function setPath($path) {
		// Store the path
		$this->_path = $path;
		
		// Parse some information about the file:
		$info = pathinfo($path);
		// - Set the extension
		$this->_extension = isset($info['extension']) ? $info['extension'] : '';
		// - Set the name of the file
		$this->_name = $info['basename'];
		
		// pathinfo provides "filename" since version 5,2,0. If the
		// filename has not been provided, we do it ourselves:
		if(isset($info['filename'])) {
			$this->_filename = $info['filename'];
		} else {
			$this->_filename = substr($this->_name, 0, strrpos($this->_name, '.'));
		}
	}
	
	/**
	 * Get path
	 * 
	 * @see MI_FsItem::getPath()
	 * @access public
	 * @return string
	 */
	public function getPath() {
		return $this->_path;
	}
	
	/**
	 * Get name
	 * 
	 * @see MI_FsItem::getName()
	 * @access public
	 * @return string
	 */
	public function getName() {
		return $this->getBasename();
	}
	
	/**
	 * Get basename
	 * 
	 * @see MI_FsItemFile::getBasename()
	 * @see MI_FsItem::getName()
	 * @access public
	 * @return string
	 */
	public function getBasename() {
		return $this->_filename . '.' . $this->_extension;
	}
	
	/**
	 * Check if file exists
	 *
	 * @see MI_FsItem::exists()
	 * @access public
	 * @return bool
	 */
	public function exists() {
		return is_file($this->getPath());
	}
	
	/**
	 * Get extension
	 * 
	 * @see MI_FsItemFile::getFileExtension()
	 * @access public
	 * @return string
	 */
	public function getFileExtension() {
		return $this->_extension;
	}
	
	/**
	 * Get filename
	 * 
	 * @see MI_FsItemFile::getFilename()
	 * @access public
	 * @return string
	 */
	public function getFilename() {
		return $this->_filename;
	}
	
	/**
	 * Get directory
	 * 
	 * @see MI_FsItemFile::getDirectory()
	 * @access public
	 * @return string
	 */
	public function getDirectory() {
		return rtrim(dirname($this->_path), " \t\n\r\0\x0B/\\");
	}
	
	/**
	 * Get directory + filename
	 * 
	 * @see MI_FsItemFile::getDirectoryAndFilename()
	 * @access public
	 * @return string
	 */
	public function getDirectoryAndFilename() {
		return $this->getDirectory() . '/' . $this->getFilename();
	}
	
	/**
	 * Get size
	 *
	 * @see MI_FsItem::getSize()
	 * @access public
	 * @return integer
	 */
	public function getSize() {
		if($this->exists()) {
			return filesize($this->getPath());
		} else {
			throw new M_FsException(sprintf(
				'Cannot get size of %s; cannot locate the file', 
				$this->getPath()
			));
		}
	}
	
	/**
	 * Get last modified date
	 * 
	 * @see MI_FsItem::getLastModifiedDate()
	 * @access public
	 * @return M_Date
	 */
	public function getLastModifiedDate() {
		if($this->exists()) {
			return new M_Date(filemtime($this->getPath()));
		} else {
			throw new M_FsException(sprintf(
				'Cannot get last modified date of %s; cannot locate the file',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Get creation date
	 * 
	 * @see MI_FsItem::getCreationDate()
	 * @access public
	 * @return M_Date
	 */
	public function getCreationDate() {
		return $this->getLastModifiedDate();
	}
	
	/**
	 * Get last access date
	 * 
	 * @see MI_FsItem::getLastAccessDate()
	 * @access public
	 * @return M_Date
	 */
	public function getLastAccessDate() {
		if($this->exists()) {
			return new M_Date(fileatime($this->getPath()));
		} else {
			throw new M_FsException(sprintf(
				'Cannot get last access date of %s; cannot locate the file',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Get contents
	 * 
	 * @see MI_FsItemFile::getContents()
	 * @access public
	 * @return string
	 */
	public function getContents() {
		if($this->exists()) {
			return file_get_contents($this->getPath());
		} else {
			throw new M_FsException(sprintf(
				'Cannot get contents of %s; cannot locate the file',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Get contents, in lines
	 * 
	 * @see MI_FsItemFile::getContentsInLines()
	 * @access public
	 * @return string
	 */
	public function getContentsInLines() {
		if($this->exists()) {
			return file($this->getPath());
		} else {
			throw new M_FsException(sprintf(
				'Cannot get contents of %s; cannot locate the file',
				$this->getPath()
			));
		}
	}

	/**
	 * Parses the file contents as CSV
	 *
	 * @param string $delimiter Field delimiter (default ,)
	 * @param string $enclose Field enclose character (default ")
	 * @param string $escape Set the escape character (default \)
	 * @return array
	 */
	public function getContentsCsv($delimiter = ',' , $enclose = '"', $escape = '\\') {
		$close = false;
		$output = array();
		
		if (($handle = fopen($this->getPath(), "r")) !== FALSE) {

			while (($data = fgetcsv($handle, null, $delimiter, $enclose, $escape)) !== FALSE) {
				$output[] = $data;
			}

			fclose ($handle);
		}

		return $output;
	}
	
	/**
	 * Get items
	 * 
	 * @see MI_FsItem::getItems()
	 * @access public
	 * @return M_IteratorNull
	 */
	public function getItems() {
		return new M_IteratorNull;
	}
	
	/**
	 * Set extension
	 * 
	 * @see MI_FsItemFile::setFileExtension()
	 * @access public
	 * @return bool
	 */
	public function setFileExtension($extension) {
		$this->setName($this->getFilename() . '.' . trim($extension, " \t\n\r\0\x0B."));
	}
	
	/**
	 * Set filename
	 * 
	 * @see MI_FsItemFile::setFilename()
	 * @access public
	 * @param string $name
	 * @return bool
	 */
	public function setFilename($name) {
		$this->setName($name . '.' . $this->getFileExtension());
	}
	
	/**
	 * Set name
	 * 
	 * NOTE:
	 * Will rename the file, if the file exist on the filesystem
	 * 
	 * @see MI_FsItem::setName()
	 * @uses M_File::exists()
	 * @access public
	 * @param string $newName
	 * 		The new name of the item
	 * @return bool
	 */
	public function setName($newName) {
		// Cast the provided name to a string
		$newName = (string) $newName;
		
		// Rename the file, if existing
		if($this->exists()) {
			if(! @rename($this->getPath(), $this->getDirectory() . '/' . $newName)) {
				throw new M_FsException(sprintf(
					'Cannot rename %s; permission denied',
					$this->getPath()
				));
			}
		}
		
		// Parse some information about the file, and set internal variables:
		$info = pathinfo($newName);
		$this->_name = $newName;
		$this->_extension = isset($info['extension']) ? $info['extension'] : '';
		
		// pathinfo provides "filename" since version 5,2,0. If the
		// filename has not been provided, we do it ourselves:
		if(isset($info['filename'])) {
			$this->_filename = $info['filename'];
		} else {
			$this->_filename = substr($this->_name, 0, strrpos($this->_name, '.'));
		}
		
		// Finally, set complete path to the file
		$this->_path = $this->getDirectory() . '/' . $this->_filename;
		if($this->_extension) {
			$this->_path .= '.' . $this->_extension;
		}
	}
	
	/**
	 * Set contents
	 * 
	 * @see MI_FsItemFile::setContents()
	 * @access public
	 * @param string $contents
	 * 		The contents that are to be written to the file
	 * @param string $mode
	 * 		The mode  parameter specifies the type of access you require to the stream.
	 * 		The possible values of this parameter are the same as the one that
	 * 		can be provided to fopen(). For more information, read the docs on
	 * 		{@link http://be.php.net/manual/en/function.fopen.php}
	 * @return bool
	 */
	public function setContents($contents, $mode = 'a') {
		// Open the file:
		$fp = @fopen($this->getPath(), $mode);
		if(! $fp) {
			throw new M_FsException(sprintf(
				'Cannot write to file %s; permission denied',
				$this->getPath()
			));
		}
		
		// Write the contents:
		if(@fwrite($fp, $contents) === FALSE) {
			throw new M_FsException(sprintf(
				'Cannot write to file %s; unknown error',
				$this->getPath()
			));
		}
		
		// Close the file handler:
		fclose($fp);
		
		// Return TRUE (success)
		return TRUE;
	}
	
	/**
	 * Set contents, in lines
	 * 
	 * @see MI_FsItemFile::setContentsInLines()
	 * @access public
	 * @param string $contents
	 * 		The contents that are to be written to the file
	 * @param string $mode
	 * 		The mode  parameter specifies the type of access you require to the stream.
	 * 		The possible values of this parameter are the same as the one that
	 * 		can be provided to fopen(). For more information, read the docs on
	 * 		{@link http://be.php.net/manual/en/function.fopen.php}
	 * @return bool
	 */
	public function setContentsInLines(array $contents, $mode = 'a') {
		return $this->setContents(implode("\r\n", $contents), $mode);
	}
	
	/**
	 * Get Base64 Encoded
	 * 
	 * Will create and return the base64 encoded string containing the
	 * file contents. This may for example be used to create a base64 encoded
	 * string of an image.
	 * 
	 * @access public
	 * @return string
	 */
	public function getBase64Encoded() {
		return base64_encode($this->getContents());
	}
	
	/**
	 * Delete the file
	 * 
	 * @see MI_FsItem::delete()
	 * @access public
	 * @return bool
	 */
	public function delete() {
		if($this->exists()) {
			if(! @unlink($this->getPath())) {
				throw new M_FsException(sprintf(
					'Cannot delete %s; permission denied',
					$this->getPath()
				));
			}
			return TRUE;
		} else {
			throw new M_FsException(sprintf(
				'Cannot delete %s; cannot locate the file',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Copy the file
	 * 
	 * @see MI_FsItem::copy()
	 * @access public
	 * @param MI_FsItemDirectory $to
	 * 		The location to which the item should be copied
	 * @param string $name
	 * 		The (new) (base)name that will be given to the destination (copied)
	 *		item. If not provided, the original name is maintained.
	 * @return bool
	 */
	public function copy(MI_FsItemDirectory $to, $name = null) {
		return $to->place($this, $name);
	}
	
	/**
	 * Rename the basename of this file
	 *
	 * @param string $basename
	 * @return bool
	 */
	public function rename($basename) {
		return $this->move(new M_Directory($this->getDirectory()), $basename);
	}
	
	/**
	 * Move the file
	 * 
	 * @see MI_FsItem::move()
	 * @access public
	 * @param MI_FsItem $to
	 * 		The location to which the item should be moved
	 * @param string
	 * 		Target basename: leave null if you don't want to change the filename
	 * @return bool
	 */
	public function move(MI_FsItemDirectory $to, $basename = null) {
		if($this->copy($to, $basename)) {
			return $this->delete();
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Download this file
	 * 
	 * @access public
	 * @param string $filename
	 * 		The filename under which the file will be presented to the user
	 * @return void
	 * @author Ben Brughmans
	 */
	public function download( $filename = null) {
		$filename = (is_null($filename)) ? $this->getBasename() : $filename;
		M_Header::sendDownload($this, $filename);
	}
	
	/**
	 * Get the MIME-type 
	 * 
	 * Returns a string e.g. image/jpeg
	 * 
	 * @author Ben Brughmans
	 * @return string
	 */
	public function getMimeType() {
		/* Temporary */
		switch(strtolower($this->getFileExtension())) {
			case 'pdf':
				return 'application/pdf';
			
			case 'exe':
				return 'application/octet-stream';
			
			case 'zip':
				return 'application/zip';
			
			case 'doc':
				return 'application/msword';
			
			case 'xls':
				return 'application/vnd.ms-excel';
			
			case 'ppt':
				return 'application/vnd.ms-powerpoint';
			
			case 'gif':
				return 'image/gif';
			
			case 'png':
				return 'image/png';
			
			case 'jpeg':
			case 'jpg':
				return 'image/jpg';
			
			case 'mp3':
				return 'audio/mpeg';
			
			case 'wav':
				return 'audio/x-wav';
			
			case 'mpeg':
			case 'mpg':
			case 'mpe':
				return 'video/mpeg';
			
			case 'mov':
				return 'video/quicktime';
			
			case 'avi':
				return 'video/x-msvideo';
			
			case 'htm':
			case 'html':
				return 'text/html';
			
			case 'xml':
				return 'text/html';
			
			case 'txt':
				return 'application/txt';
			
			// By default, we try to force a download dialog
			default:
				return 'application/force-download';
		}
		/* /Temporary */
		
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		
		$mimetype = finfo_file(
			$finfo, 
			$this->getPath()
		);
		
		finfo_close($finfo);
		
		return $mimetype;
	}
}