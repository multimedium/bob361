<?php
/**
 * M_FsItem class
 * 
 * M_FsItem is an abstract class, that is used to represent items on
 * the local file system ({@link M_Fs}). It provides with shared 
 * implementations for:
 * 
 * - {@link M_Directory}
 * - {@link M_File}
 * 
 * @package Core
 */
abstract class M_FsItem implements MI_FsItem {
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $path
	 * 		The path to the item
	 * @return M_FsItem
	 */
	public function __construct($path = NULL) {
		$this->setPath($path);
	}
	
	/**
	 * Get file system
	 * 
	 * @see MI_FsItem::getFs()
	 * @access public
	 * @return M_Fs
	 */
	public function getFs() {
		return new M_Fs;
	}
	
	/**
	 * Get parent
	 * 
	 * @see MI_FsItem::getParent()
	 * @access public
	 * @return M_Directory
	 */
	public function getParent() {
		// We do not use M_Fs::getItem(), because then the item would have to
		// "physically" exist on the server. Instead, we use M_Directory, because
		// we know it's going to be a directory:
		// return $this->getFs()->getItem(dirname($this->getPath()));
		return new M_Directory(dirname($this->getPath()));
	}
	
	/**
	 * Get owner
	 * 
	 * @see MI_FsItem::getOwner()
	 * @access public
	 * @return M_FsOwner
	 */
	public function getOwner() {
		if($this->exists()) {
			$u = posix_getpwuid(fileowner($this->getPath()));
			$g = posix_getgrgid($u['gid']);
			return new M_FsOwner($u['name'], $g['name']);
		} else {
			throw new M_FsException(sprintf(
				'Cannot get owner of %s; cannot locate the item',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Get permissions
	 * 
	 * @see MI_FsItem::getPermissions()
	 * @access public
	 * @return M_FsPermissions
	 */
	public function getPermissions() {
		if($this->exists()) {
			$octal = octdec(substr(sprintf('%o', fileperms($this->getPath())), -4));
			return M_FsPermissions::constructWithOctal($octal);
		} else {
			throw new M_FsException(sprintf(
				'Cannot get permissions of %s; cannot locate the item',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Is link?
	 * 
	 * @see MI_FsItem::isLink()
	 * @access public
	 * @return bool
	 */
	public function isLink() {
		if($this->exists()) {
			return is_link($this->getPath());
		} else {
			throw new M_FsException(sprintf(
				'Cannot determine if %s is a link; cannot locate the item',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Is this a file?
	 *
	 * @return bool
	 * @access public
	 * @author b.brughmans
	 */
	public function isFile() {
		return ($this->getFsItemType() == M_Fs::TYPE_FILE || $this->getFsItemType() == M_Fs::TYPE_ARCHIVE);
	}
	
	/**
	 * Is this a directory?
	 * 
	 * @return bool
	 * @access public
	 * @author b.brughmans
	 */
	public function isDirectory() {
		return ($this->getFsItemType() == M_Fs::TYPE_DIRECTORY);
	}
	
	/**
	 * Set owner
	 * 
	 * @see MI_FsItem::setOwner()
	 * @access public
	 * @param M_FsOwner $owner
	 * @return bool
	 */
	public function setOwner(M_FsOwner $owner) {
		// If the file or directory exists:
		if($this->exists()) {
			// First, we set the owner of all contained items. For each of the 
			// contained items:
			foreach($this->getItems() as $item) {
				// Set the owner of the contained item first:
				$item->setOwner($owner);
			}
			
			// Set the owner, and return the result:
			return @chown($this->getPath(), $owner->getName());
		}
		// If the file or directory does not exist:
		else {
			// Throw an exception:
			throw new M_FsException(sprintf(
				'Cannot set owner of %s; cannot locate the item',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Set permissions
	 * 
	 * @see MI_FsItem::setPermissions()
	 * @access public
	 * @param M_FsPermissions $permissions
	 * @return bool
	 */
	public function setPermissions(M_FsPermissions $permissions) {
		// If the file or directory exists:
		if($this->exists()) {
			// First, we set the permissions of all contained items. For each of 
			// the contained items:
			foreach($this->getItems() as $item) {
				// Set the owner of the contained item first:
				$item->setPermissions($permissions);
			}
			
			// Set the new permissions, and return the result:
			return @chmod($this->getPath(), $permissions->getOctalValue());
		}
		// If the file or directory does not exist:
		else {
			// Throw an exception:
			throw new M_FsException(sprintf(
				'Cannot set permissions of %s; cannot locate the item',
				$this->getPath()
			));
		}
	}
}