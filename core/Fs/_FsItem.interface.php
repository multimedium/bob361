<?php
/**
 * MI_FsItem interface
 * 
 * The MI_FsItem interface dictates the public API that must be 
 * implemented by the items on a filesystem.
 * 
 * An introduction to COMPOSITE PATTERN
 * (Extracted from "Advanced Actionscript 3 with Design Patterns, 1E")
 * -------------------------------------------------------------------
 * The Composite pattern enables you to elegantly deal with recursive 
 * or hierarchical data structures. There are many examples of hierarchical 
 * data structures, making the Composite pattern very useful. A common 
 * example of such a data structure is one that you encounter every time 
 * you use a computer: the file system. The file system consists of 
 * directories and files. Every directory potentially has contents. The 
 * contents of a directory might be files, but they also might be 
 * directories. In this way, the file system of a computer is organized 
 * in a recursive structure. If you want to represent such a data structure 
 * programmatically, the Composite pattern is ideal to do so.
 * 
 * The Composite pattern has the following elements:
 * 
 * Element interface: An interface for all participating elements. This
 * interface is defined by {@link MI_FsItem}.
 * 
 * Leaf: A class representing terminating elements in the data structure.
 * In the file system example, files are leaf elements because they 
 * don't have child elements. Leaf classes must implement the element 
 * interface.
 * 
 * Composite: A class for the collections in the data structure. In the 
 * file system example, directories are composite elements. Composite 
 * classes must implement the element interface.
 * -------------------------------------------------------------------
 * 
 * Note that both directories and files in a file system have to 
 * implement the same interface. This allows for us to very easily go
 * through the items in a file system, because we know that each and
 * every item on the system implements the same interface, whether it's
 * a directory or a file.
 * 
 * However, aside from the shared interface, we also create specific
 * interfaces (which extends the shared one) for both the leaf and
 * composite elements in the file system:
 * 
 * - {@link M_FsItemFile}
 * - {@link M_FsItemDirectory}
 * - {@link M_FsItemArchive}
 * 
 * @package Core
 */
interface MI_FsItem {
	/**
	 * Get file system
	 * 
	 * Will provide with a reference to the file system on which the
	 * item is located. Note that the filesystem is represented by 
	 * an object that implements the {@link MI_Fs} interface. Currently,
	 * we have 2 implementations of file systems:
	 * 
	 * - {@link M_Fs}
	 * - {@link M_Ftp}
	 * 
	 * @access public
	 * @return MI_Fs
	 */
	public function getFs();
	
	/**
	 * Get item type
	 * 
	 * Will provide with a type indicator of the item. Since the item
	 * can either be a directory or a file, the return value of this
	 * method can be one of the following:
	 * 
	 * - {@link MI_Fs::TYPE_DIRECTORY}
	 * - {@link MI_Fs::TYPE_FILE}
	 * - {@link MI_Fs::TYPE_ARCHIVE}
	 * 
	 * @access public
	 * @return string
	 */
	public function getFsItemType();
	
	/**
	 * Check if item exists
	 * 
	 * You can represent an item on the file system by an object that
	 * implements the {@link MI_FsItem} interface, but that does not
	 * necessarily mean that the item "physically" exists on the file
	 * system.
	 * 
	 * To check whether or not the item exists in the file system, you
	 * can use this method. It returns TRUE if the item exists, FALSE
	 * if not.
	 * 
	 * @access public
	 * @return bool
	 */
	public function exists();
	
	/**
	 * Get parent
	 * 
	 * Will provide with the parent item, which is containing the item
	 * that is being represented by the object. Obviously, this will 
	 * always be a directory (folder), so the return value will be an
	 * object that implements the {@link MI_FsItemDirectory} interface.
	 * 
	 * @access public
	 * @return MI_FsItemDirectory
	 */
	public function getParent();
	
	/**
	 * Get name
	 * 
	 * Will provide with the name of the item
	 * 
	 * @access public
	 * @return string
	 */
	public function getName();
	
	/**
	 * Get path
	 * 
	 * Will provide with the complete path to the item
	 * 
	 * @access public
	 * @return string
	 */
	public function getPath();
	
	/**
	 * Get size
	 * 
	 * Will provide with the size (number of bytes) of the item. Note 
	 * that the size will be the sum of the item's size, and the size
	 * of all contained items.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getSize();
	
	/**
	 * Get items
	 * 
	 * Will provide with the collection of items that is contained by
	 * the item that is being represented by the object. In the case of
	 * a directory, it is pretty clear that this method will return the 
	 * collection of directories and files that live inside the item.
	 * 
	 * However, a file cannot contain items. Typically, if the item is
	 * a file, this method will return a Null Iterator. For more info,
	 * read the docs on {@link M_IteratorNull}.
	 * 
	 * @access public
	 * @return Iterator
	 */
	public function getItems();
	
	/**
	 * Get last modified date
	 * 
	 * Will provide with the date at which the item was last modified, 
	 * represented by an instance of {@link M_Date}.
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getLastModifiedDate();
	
	/**
	 * Get creation date
	 * 
	 * Will provide with the date at which the item was created, 
	 * represented by an instance of {@link M_Date}.
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getCreationDate();
	
	/**
	 * Get last access date
	 * 
	 * Will provide with the date at which the item was last accessed, 
	 * represented by an instance of {@link M_Date}.
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getLastAccessDate();
	
	/**
	 * Get owner
	 * 
	 * Will provide with more information about the item's owner, 
	 * represented by an instance of {@link M_FsOwner}.
	 * 
	 * @access public
	 * @return M_FsOwner
	 */
	public function getOwner();
	
	/**
	 * Get permissions
	 * 
	 * Will provide with more information about the item's permissions,
	 * represented by an instance of {@link M_FsPermissions}.
	 * 
	 * @access public
	 * @return M_FsPermissions
	 */
	public function getPermissions();
	
	/**
	 * Is link?
	 * 
	 * Will check whether or not the item is a symbolic link. A symbolic
	 * links is like a Windows shortcut, or a Mac OS X Alias. Returns
	 * TRUE if the item is a link, FALSE if not.
	 * 
	 * @access public
	 * @return bool
	 */
	public function isLink();
	
	/**
	 * Is file?
	 * 
	 * Will check whether or not the item is a file. Returns TRUE if the item
	 * is a file, FALSE is not
	 * 
	 * @access public
	 * @return bool
	 * @author b.brughmans
	 */
	public function isFile();
	
	/**
	 * Is dir?
	 * 
	 * Will check whether or not the item is a directory. Returns TRUE if the item
	 * is a file, FALSE is not
	 * 
	 * @access public
	 * @return bool
	 * @author b.brughmans
	 */
	public function isDirectory();
	
	/**
	 * Delete the item
	 * 
	 * Will delete the item, and all items that are contained by the
	 * item. Returns TRUE on success, FALSE on failure.
	 * 
	 * @access public
	 * @return bool
	 */
	public function delete();
	
	/**
	 * Copy the item
	 * 
	 * Will copy the item to a specified location. Will return TRUE
	 * on success, FALSE on failure.
	 * 
	 * NOTE:
	 * Note that the target location is represented by an object that
	 * implements the {@link MI_FsItem} interface. This way, we can
	 * copy the item to any (other) file system of our choice.
	 * 
	 * @access public
	 * @param MI_FsItemDirectory $to
	 * 		The location to which the item should be copied
	 * @param string $name
	 * 		The (new) (base)name that will be given to the destination (copied)
	 *		item. If not provided, the original name is maintained.
	 * @return bool
	 */
	public function copy(MI_FsItemDirectory $to, $name = null);
	
	/**
	 * Move the item
	 * 
	 * Copies the item to the specified target location, and then 
	 * deletes the original copy (if copied successfully). Will return
	 * TRUE on success, FALSE on failure.
	 * 
	 * NOTE:
	 * Note that the target location is represented by an object that
	 * implements the {@link MI_FsItem} interface. This way, we can
	 * move the item to any (other) file system of our choice.
	 * 
	 * @access public
	 * @param MI_FsItemDirectory $to
	 * 		The location to which the item should be moved
	 * @return bool
	 */
	public function move(MI_FsItemDirectory $to);
	
	/**
	 * Set name
	 * 
	 * Will rename the item. Will return TRUE on success, FALSE on
	 * failure.
	 * 
	 * @access public
	 * @param string $newName
	 * 		The new name of the item
	 * @return bool
	 */
	public function setName($newName);
	
	/**
	 * Set path
	 * 
	 * Can be used to point the item to another item on the file 
	 * system. Of course, if the types do not match, an exception will
	 * be thrown! (for example, you cannot point a file object to a 
	 * directory)
	 *
	 * @access public
	 * @param string $path
	 * 		The new path of the item
	 * @return void
	 */
	public function setPath($path);
	
	/**
	 * Set owner
	 * 
	 * Will set with the item's owner. The owner is represented by an instance 
	 * of {@link M_FsOwner}.
	 * 
	 * Will return TRUE on success, FALSE on failure.
	 * 
	 * @access public
	 * @param M_FsOwner $owner
	 * @return bool
	 */
	public function setOwner(M_FsOwner $owner);
	
	/**
	 * Set permissions
	 * 
	 * Will set the permissions of an item on the file system. The 
	 * description of the permissions are provided by an instance of
	 * {@link M_FsPermissions}.
	 * 
	 * Will return TRUE on success, FALSE on failure.
	 * 
	 * @access public
	 * @param M_FsPermissions $permissions
	 * 		The permissions that should be applied to the item
	 * @return bool
	 */
	public function setPermissions(M_FsPermissions $permissions);
}