<?php
/**
 * MI_Fs interface
 * 
 * This interface dictates what public API must be implemented by 
 * filesystems. Currently, the following filesystems implement the
 * MI_Fs interface:
 * 
 * - {@link M_Fs}
 * - {@link M_Ftp}
 * 
 * @package Core
 */
interface MI_Fs {
	/**
	 * Filesystem item type
	 * 
	 * This constant can be used to indicate the type of an item on the
	 * filesystem. This particular constant is used to address the file
	 * type.
	 */
	const TYPE_FILE = 'file';
	
	/**
	 * Filesystem item type
	 * 
	 * This constant can be used to indicate the type of an item on the
	 * filesystem. This particular constant is used to address the folder
	 * type.
	 */
	const TYPE_DIRECTORY = 'dir';
	
	/**
	 * Filesystem item type
	 * 
	 * This constant can be used to indicate the type of an item on the
	 * filesystem. This particular constant is used to address the archive
	 * type.
	 */
	const TYPE_ARCHIVE = 'archive';
	
	/**
	 * Get type
	 * 
	 * Will provide with the type of the filesystem. In currently 
	 * implemented filesystems, the return value of this method is
	 * "Local" in {@link M_Fs} and "FTP" in {@link M_Ftp}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getType();
	
	/**
	 * Get OS
	 * 
	 * Will provide with information about the OS that is running the
	 * filesystem. Typically, this method will release information
	 * about the OS that has been installed on the machine where the
	 * files are located.
	 * 
	 * Example 1, get the OS of the server on which the app is running
	 * <code>
	 *    $fs = new M_Fs();
	 *    echo $fs->getOS();
	 * </code>
	 * 
	 * Example 3, get the OS that is running on a remote FTP Server
	 * <code>
	 *    $ftp = new M_Ftp(M_Uri('ftp://username:password@ftp.my-domain.com:21'));
	 *    $ftp->connect();
	 *    
	 *    echo $ftp->getOS();
	 * </code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getOS();
	
	/**
	 * Get temporary folder
	 * 
	 * Will return an object, that implements the {@link MI_FsItemDirectory} 
	 * interface, where temporary files are located.
	 * 
	 * @access public
	 * @return MI_FsItemDirectory
	 */
	public static function getTemporaryDirectory();
	
	/**
	 * Get unique temporary folder
	 * 
	 * Will return an object, that implements the {@link MI_FsItemDirectory}
	 * interface, where temporary files are located. In addition to what the
	 * {@link MI_Fs::getTemporaryDirectory()} method already does, this method
	 * creates a UNIQUE folder inside the temporary folder.
	 * 
	 * @access public
	 * @return MI_FsItemDirectory
	 */
	public static function getUniqueTemporaryDirectory();
	
	/**
	 * Get an item
	 * 
	 * Will provide with an item on the filesystem, given the path to
	 * the requested item. Note that the result will be a filesystem 
	 * item, and can be either a directory (folder) or a file. For more
	 * info, read the docs on:
	 * 
	 * - {@link MI_FsItem}
	 * - {@link MI_FsItemDirectory}
	 * - {@link MI_FsItemFile}
	 *
	 * @access public
	 * @param string $path
	 * @return MI_FsItem
	 */
	public function getItem($path);
}