<?php
/**
 * M_FsPersmissions
 * 
 * Used to describe permissions/rights of an item on a filesystem. To
 * learn more about filesystems and its items, read the docs on
 * 
 * - {@link MI_Fs}
 * - {@link MI_FsItem}
 * - {@link MI_FsItemDirectory}
 * - {@link MI_FsItemFile}
 * 
 * @package Core
 */
class M_FsPermissions {
	/**
	 * Permissions string
	 * 
	 * This property stores the permissions string. For more info
	 * on permissions strings, read the documentation on 
	 * {@link M_FsPermissions::constructWithString()}
	 * 
	 * @access private
	 * @var string
	 */
	private $_perm;
	
	/**
	 * Constructor
	 * 
	 * NOTE:
	 * The permissions object will be initiated with the following
	 * definition of permissions:
	 * 
	 * <code>
	 *    -rw-r--r-- (644)
	 * </code>
	 * 
	 * For more information, read the docs on:
	 * 
	 * - {@link M_FsPermissions::constructWithString()}
	 * - {@link M_FsPermissions::constructWithOctal()}
	 * 
	 * @access public
	 * @return M_FsPermissions
	 */
	public function __construct() {
		$this->_perm = '-rw-r--r--'; // default to 644
	}
	
	/* -- Static -- */
	
	/**
	 * Construct from Permissions String
	 * 
	 * Each file comes with a permission string. Some examples are:
	 * 
	 * <code>
	 *    -rw-rw-rw-
	 *    -rw-r--r--
	 *    -r--------
	 * </code>
	 * 
	 * The first letter on the left is not really a permission but it 
	 * is included in the permissions anyway. It is either d, l or -:
	 * 
	 * <code>d</code>
	 * means the item is a directory
	 * 
	 * <code>l</code>
	 * means the item is a link (like a Windows shortcut or a Mac alias, 
	 * not the actual file but just a link to the actual file)
	 * 
	 * <code>-</code>
	 * means the file is a regular file (in other words it's NOT a 
	 * directory)
	 * 
	 * The remaining 9 bits are divided into triads (groups of 3). 
	 * The left-most triad is for the file's owner (user), the middle 
	 * triad is for the file's group, the right-most triad is for 
	 * others. Others means all those who are neither the file's owner 
	 * nor in the file's group, in other words the rest of the users 
	 * on the system.
	 * 
	 * Since owner and others both start with letter o, we refer to the 
	 * owner as user so that we can refer to the 3 groups with 3 letters, 
	 * for brevity, as follows:
	 * 
	 * <code>
	 *    u = owner
	 *    g = group
	 *    o = others 
	 * </code>
	 * 
	 * Now we can show the relationship between the triads and the 
	 * people this way:
	 * 
	 * <code>
	 *       u   g   o
	 *    d|---|---|---
	 * </code>
	 * 
	 * Each triad designates 3 permissions in the following order from 
	 * left to right:
	 * 
	 * <code>r</code>
	 * = read = can read the file or directory
	 * 
	 * <code>w</code>
	 * = write = can write to the file or directory, if the file is 
	 * a directory then can create and delete files in that directory
	 * 
	 * <code>x</code>
	 * = executable = can execute it if it's an executable file, if 
	 * it's a directory then can view the contents of that directory 
	 * 
	 * The bits in the triads are either "on" (an r, w or x) or "off" 
	 * (a -).
	 * 
	 * Some examples:
	 * 
	 * <code>-rw-rw-rw-</code>
	 * means everybody can read and write to the file, not very secure 
	 * but you might find a use for it
	 * 
	 * <code>-rw-r-r-</code>
	 * means everybody can read the file but only the owner can write 
	 * to it, more secure but not what you would want for a file 
	 * containing a password
	 * 
	 * <code>-r--------</code>
	 * means only the owner can read and NOBODY can write, very secure, 
	 * so secure even the owner must jump through some hoops just to 
	 * write to it (you have become root and give yourself write 
	 * permission first) but if you need to hide something important 
	 * this helps
	 * 
	 * <code>-rw-rw----</code>
	 * means the owner and members of the group can read and write, 
	 * good for files that a team collaborates on, the administrator 
	 * simply gives membership in the group to trusted users, the rest 
	 * of the world is blocked 
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The permissions string with which to construct a permissions
	 * 		object.
	 * @return M_FsPermissions
	 */
	public static function constructWithString($string) {
		// Get the permissions string
		$string = strtolower((string) $string);
		
		// The permissions string should have exactly 10 characters:
		if(strlen($string) == 10) {
			// The first character (file type) must be:
			// - denotes a regular file
			// d denotes a directory
			// b denotes a block special file
			// c denotes a character special file
			// l denotes a symbolic link
			// p denotes a named pipe
			// s denotes a domain socket
			if(! in_array($string{0}, array('-', 'd', 'b', 'c', 'l', 'p', 's'))) {
				throw new M_FsException('First character in permissions string must be - d b c l p s');
			}
			
			// All other characters must be - r w or x, depending on
			// the position of the character
			for($i = 1; $i < 10; $i ++) {
				if($i == 1 || $i == 4 || $i == 7) {
					$char = 'r';
				} elseif($i == 2 || $i == 5 || $i == 8) {
					$char = 'w';
				} else {
					$char = 'x';
				}
				if(! in_array($string{$i}, array('-', $char))) {
					throw new M_FsException(sprintf(
						'Could not parse permissions string. Unrecognized "%s" at position %d',
						$string{$i},
						$i
					));
				}
			}
		}
		// Oops, the permissions string does not have 10 chars!!
		else {
			throw new M_FsException('Could not parse permissions string; should be 10 chars long');
		}
		
		// Construct a new permissions object:
		$object = new self;
		$object->setIsReadableByOwner(($string{1} == 'r'));
		$object->setIsWritableByOwner(($string{2} == 'w'));
		$object->setIsExecutableByOwner(($string{3} == 'x'));
		$object->setIsReadableByGroup(($string{4} == 'r'));
		$object->setIsWritableByGroup(($string{5} == 'w'));
		$object->setIsExecutableByGroup(($string{6} == 'x'));
		$object->setIsReadableByOthers(($string{7} == 'r'));
		$object->setIsWritableByOthers(($string{8} == 'w'));
		$object->setIsExecutableByOthers(($string{9} == 'x'));
		return $object;
	}
	
	/**
	 * Construct from octal value
	 * 
	 * In order to fully understand the octal value, you should first
	 * read the intro at {@link M_FsPermissions::constructWithString()}.
	 * 
	 * Another common method for representing permissions is octal 
	 * notation. Octal notation consists of a three- or four-digit 
	 * base-8 value.
	 * 
	 * With three-digit octal notation, each numeral represents a 
	 * different component of the permission set:
	 * 
	 * - user class,
	 * - group class, and 
	 * - "others" class respectively.
	 * 
	 * Each of these digits is the sum of its component bits. As a result, 
	 * specific bits add to the sum as it is represented by a numeral:
	 * 
	 * The read bit adds 4 to its total (in binary 100),
	 * The write bit adds 2 to its total (in binary 010), and
	 * The execute bit adds 1 to its total (in binary 001).
	 * 
	 * These values never produce ambiguous combinations; each sum 
	 * represents a specific set of permissions.
	 * 
	 * These are the examples from the Symbolic notation section given 
	 * in octal notation:
	 * 
	 * "-rwxr-xr-x" would be represented as 755 in three-digit octal.
	 * "-rw-rw-r--" would be represented as 664 in three-digit octal.
	 * "-r-x------" would be represented as 500 in three-digit octal.
	 * 
	 * @access public
	 * @param integer $octal
	 * 		The permissions, expressed in an octal notation
	 * @return M_FsPermissions
	 */
	public static function constructWithOctal($octal) {
		// We download the octal value as a string:
		$octalInString = '0' . ((string) decoct($octal)*1);
		
		// We expect the octal value to have been provided in 4 digits
		// (eg 0777, 0644, etc...)
		if(strlen($octalInString) != 4) {
			throw new M_FsException(sprintf(
				'%s::%s() expects an octal value of 4 digits; eg 0777, 0644',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// For each of the numbers in the octal notation:
		$string = '----------';
		for($i = 1, $j = 0; $i < 4; $i ++, $j += 3) {
			$number = (int) $octalInString{$i};
			if($number >= 0 && $number < 8) {
				$perms  = array(0, 0, 0);
				if($number >= 4) {
					$string{1 + $j} = 'r';
					$number -= 4;
				}
				if($number >= 2) {
					$string{2 + $j} = 'w';
					$number -= 2;
				}
				if($number > 0) {
					$string{3 + $j} = 'x';
				}
			} else {
				throw new M_FsException(sprintf(
					'Invalid octal notation for file permission: %s',
					$octalInString
				));
			}
		}
		
		// Construct a new permissions object:
		$object = new self;
		$object->setIsReadableByOwner(($string{1} == 'r'));
		$object->setIsWritableByOwner(($string{2} == 'w'));
		$object->setIsExecutableByOwner(($string{3} == 'x'));
		$object->setIsReadableByGroup(($string{4} == 'r'));
		$object->setIsWritableByGroup(($string{5} == 'w'));
		$object->setIsExecutableByGroup(($string{6} == 'x'));
		$object->setIsReadableByOthers(($string{7} == 'r'));
		$object->setIsWritableByOthers(($string{8} == 'w'));
		$object->setIsExecutableByOthers(($string{9} == 'x'));
		return $object;
	}
	
	/* -- Getters -- */
	
	/**
	 * Get octal notation
	 * 
	 * Will provide with an octal (numeric) representation of the 
	 * permissions.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getOctalValue() {
		$return = 0;
		@eval('$return = ' . $this->getOctalValueInString() . ';');
		return $return;
	}
	
	/**
	 * Get octal value, in string
	 * 
	 * @access public
	 * @return string
	 */
	public function getOctalValueInString() {
		$octalInString = '0';
		for($i = 1, $j = 0; $i < 4; $i ++, $j += 3) {
			$number = 0;
			if($this->_perm{1 + $j} == 'r') {
				$number += 4;
			}
			if($this->_perm{2 + $j} == 'w') {
				$number += 2;
			}
			if($this->_perm{3 + $j} == 'x') {
				$number += 1;
			}
			$octalInString .= (string) $number;
		}
		return $octalInString;
	}
	
	/**
	 * Get string representation
	 * 
	 * Will provide with the string representation of the permissions.
	 * For more info on permission strings, read the documentation on
	 * {@link M_FsPermissions::constructWithString()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getStringValue() {
		return $this->_perm;
	}
	
	/**
	 * Get flag: is readable by owner?
	 * 
	 * @access public
	 * @return bool
	 */
	public function isReadableByOwner() {
		return ($this->_perm{1} == 'r');
	}
	
	/**
	 * Get flag: is writable by owner?
	 * 
	 * @access public
	 * @return bool
	 */
	public function isWritableByOwner() {
		return ($this->_perm{2} == 'w');
	}
	
	/**
	 * Get flag: is executable by owner?
	 * 
	 * @access public
	 * @return bool
	 */
	public function isExecutableByOwner() {
		return ($this->_perm{3} == 'x');
	}
	
	/**
	 * Get flag: is readable by group?
	 * 
	 * @access public
	 * @return bool
	 */
	public function isReadableByGroup() {
		return ($this->_perm{4} == 'r');
	}
	
	/**
	 * Get flag: is writable by group?
	 * 
	 * @access public
	 * @return bool
	 */
	public function isWritableByGroup() {
		return ($this->_perm{5} == 'w');
	}
	
	/**
	 * Get flag: is executable by group?
	 * 
	 * @access public
	 * @return bool
	 */
	public function isExecutableByGroup() {
		return ($this->_perm{6} == 'x');
	}
	
	/**
	 * Get flag: is readable by others?
	 * 
	 * @access public
	 * @return bool
	 */
	public function isReadableByOthers() {
		return ($this->_perm{7} == 'r');
	}
	
	/**
	 * Get flag: is writable by others?
	 * 
	 * @access public
	 * @return bool
	 */
	public function isWritableByOthers() {
		return ($this->_perm{8} == 'w');
	}
	
	/**
	 * Get flag: is executable by others?
	 * 
	 * @access public
	 * @return bool
	 */
	public function isExecutableByOthers() {
		return ($this->_perm{9} == 'x');
	}
	
	/**
	 * Set flag: is readable by owner?
	 *
	 * @access public
	 * @param boolean $flag
	 * @return void
	 */
	public function setIsReadableByOwner($flag) {
		$this->_perm{1} = $flag ? 'r' : '-';
	}
	
	/**
	 * Set flag: is writable by owner?
	 *
	 * @access public
	 * @param boolean $flag
	 * @return void
	 */
	public function setIsWritableByOwner($flag) {
		$this->_perm{2} = $flag ? 'w' : '-';
	}
	
	/**
	 * Set flag: is executable by owner?
	 *
	 * @access public
	 * @param boolean $flag
	 * @return void
	 */
	public function setIsExecutableByOwner($flag) {
		$this->_perm{3} = $flag ? 'x' : '-';
	}
	
	/**
	 * Set flag: is readable by group?
	 *
	 * @access public
	 * @param boolean $flag
	 * @return void
	 */
	public function setIsReadableByGroup($flag) {
		$this->_perm{4} = $flag ? 'r' : '-';
	}
	
	/**
	 * Set flag: is writable by group?
	 *
	 * @access public
	 * @param boolean $flag
	 * @return void
	 */
	public function setIsWritableByGroup($flag) {
		$this->_perm{5} = $flag ? 'w' : '-';
	}
	
	/**
	 * Set flag: is executable by group?
	 *
	 * @access public
	 * @param boolean $flag
	 * @return void
	 */
	public function setIsExecutableByGroup($flag) {
		$this->_perm{6} = $flag ? 'x' : '-';
	}
	
	/**
	 * Set flag: is readable by others?
	 *
	 * @access public
	 * @param boolean $flag
	 * @return void
	 */
	public function setIsReadableByOthers($flag) {
		$this->_perm{7} = $flag ? 'r' : '-';
	}
	
	/**
	 * Set flag: is writable by others?
	 *
	 * @access public
	 * @param boolean $flag
	 * @return void
	 */
	public function setIsWritableByOthers($flag) {
		$this->_perm{8} = $flag ? 'w' : '-';
	}
	
	/**
	 * Set flag: is executable by others?
	 *
	 * @access public
	 * @param boolean $flag
	 * @return void
	 */
	public function setIsExecutableByOthers($flag) {
		$this->_perm{9} = $flag ? 'x' : '-';
	}
}