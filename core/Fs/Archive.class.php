<?php
M_Loader::loadCoreClass('M_Archiver');

class M_Archive extends M_File implements MI_FsItemArchive {
	
	/**
	 * @var int
	 */
	protected $_sizeUncompressed;

	/**
	 * construct
	 * 
	 * @param str $path
	 * @return void
	 */
	public function __construct($path) {
		$this->setPath($path);
		
		//check file extension
		if ($this->hasValidFileExtension() == false) {
			throw new M_Exception(
				sprintf(
					'Cannot create M_Archive for file extension %s', 
					$this->getFileExtension()
				)
			);
		}
	}
	
	/**
	 * Return the supported file extensions
	 * 
	 * @return array
	 */
	public static function getFileExtensions() {
		return array('zip');
	}
	
	/**
	 * Check if this archive has a valid file extension
	 * 
	 * @return bool
	 */
	public function hasValidFileExtension() {
		return in_array(
			strtolower($this->getFileExtension()), 
			$this->getFileExtensions()
		);
	}

	/**
	 * Item type (file/directory/archive)
	 * 
	 * @return str
	 */
	public function getFsItemType() {
		return MI_Fs::TYPE_ARCHIVE;
	}
	
	/**
	 * Get the size of the archive
	 * 
	 * NOTE: if the archive doesn't exist, it will be compressed in order to
	 * calculate the size. Afterwards the archive will be deleted
	 * 
	 * @return int
	 */
	public function getSize() {
		//if the archive already exists we can return the size easily
		if ($this->exists()) {
			return $this->getSize();
		}
		
		//if not we need to compress
		else {
			$this->compress();
			$size = $this->getSize();
			$this->delete();
			
			return $size;
		}
	}
	
	/**
	 * Compress using {@link M_Archiver::compress()}
	 *
	 * @return bool
	 */
	public function compress() {
		$archiver = new M_Archiver();
		return $archiver->compress($this);
	}
	
	/**
	 * Get the items of this archive
	 * 
	 * Extract the archive (when it exists) and return the contents of this 
	 * directory. These can be instances of {@see M_File} or {@see M_Directory}
	 * 
	 * @return M_ArrayIterator
	 */
	public function getItems() {
		//if the archive already exists we need to extract before we can return
		//the items
		if ($this->exists()) {
			//extract
			$tmpDir = M_Archiver::extract($this);
			return $tmpDir->getItems();
		}
		//if the archive does not exist yet, we return an empty M_ArrayIterator
		else {
			return new M_ArrayIterator();
		}
	}
	
	/**
	 * Extract the archive to a directory
	 *
	 * @param MI_FsItemDirectory|null $directory
	 * @return MI_FsItemDirectory
	 */
	public function extract($directory = null) {
		//check if a valid directory is provided
		if (!is_null($directory) && $directory instanceof MI_FsItemDirectory == false) {
			throw new M_ArchiveException('Invalid argument passed when trying to extract archive');
		}
		
		$archiver = new M_Archiver();
		return $archiver->extract($this, $directory);
	}
	
	/**
	 * Get the total size of all items in archive when it would be uncompressed
	 * 
	 * @param bool $recalculate
	 * 		Force the size to be recalculated
	 * @return int
	 */
	public function getSizeUncompressed($recalculate = false) {
		
		//calculate when size hasn't been calculated yet, or when users forces
		//to calculate again
		if (is_null($this->_sizeUncompressed) || $recalculate === true) {
			//if the archive allready exists, we can use 
			//{@link M_Archiver::getSizeUncompressed()} to calculate the 
			//uncompressed file size
			if ($this->exists()) {
				$this->_sizeUncompressed = M_Archiver::getSizeUncompressed(
					$this
				);
			}
		}
		
		return $this->_sizeUncompressed;
	}
}