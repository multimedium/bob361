<?php
/**
 * M_FtpItem class
 * 
 * M_FtpItem is an abstract class, that is used to represent items on
 * an FTP Server ({@link M_Ftp}). It provides with shared implementations 
 * for:
 * 
 * - {@link M_FtpItemDirectory}
 * - {@link M_FtpItemFile}
 * 
 * @package Core
 */
abstract class M_FtpItem implements MI_FsItem {
	/**
	 * FTP Server
	 * 
	 * This property stores a reference to the FTP Server (or file
	 * system) on which the item is located
	 *
	 * @access protected
	 * @var M_Ftp
	 */
	protected $_ftp;
	
	/**
	 * Path to item
	 *
	 * This property stores the complete path to the item on the FTP
	 * server.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_path;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param M_Ftp $ftp
	 * 		The FTP (or file system)
	 * @param string $path
	 * 		The path to the item on the file system
	 * @return M_FtpItem
	 * @todo Replace M_Fs by an M_FsFtp interface
	 */
	public function __construct(MI_Fs $ftp, $path) {
		$this->_ftp  = $ftp;
		$this->setPath((string) $path);
	}
	
	
	/**
	 * Get filesystem
	 * 
	 * In the case of an {@link M_FtpItem}, this method will provide
	 * with the {@link M_Ftp} on which the item is located.
	 * 
	 * @access public
	 * @return M_Ftp
	 */
	public function getFs() {
		return $this->_ftp;
	}
	
	/**
	 * Check if exists
	 * 
	 * @access public
	 * @return bool
	 */
	public function exists() {
		return $this->getFs()->isItemOnServer($this);
	}
	
	/**
	 * Get parent item
	 * 
	 * @access public
	 * @return M_FtpItemDirectory
	 */
	public function getParent() {
		return new M_FtpItemDirectory($this->getFs(), dirname($this->getPath()));
	}
	
	/**
	 * Get name
	 * 
	 * @access public
	 * @return string
	 */
	public function getName() {
		return basename($this->getPath());
	}
	
	/**
	 * Get (complete) path to item
	 * 
	 * @access public
	 * @return string
	 */
	public function getPath() {
		return $this->_path;
	}
	
	/**
	 * Get size
	 * 
	 * @see M_Ftp::getSizeOfItem()
	 * @access public
	 * @return integer
	 */
	public function getSize() {
		return $this->getFs()->getSizeOfItem($this);
	}
	
	/**
	 * Get size in string format
	 * 
	 * @see M_FsHelper::getFileSizeString()
	 * @access public
	 * @return string
	 */
	public function getSizeString() {
		return M_FsHelper::getFileSizeString($this->getSize());
	}
	
	/**
	 * Get last modified date
	 * 
	 * @see M_Ftp::getLastModifiedDateOfItem()
	 * @access public
	 * @return M_Date
	 */
	public function getLastModifiedDate() {
		return $this->getFs()->getLastModifiedDateOfItem($this);
	}
	
	/**
	 * Get creation date
	 * 
	 * NOTE:
	 * In the case of an {@link M_FtpItem}, we cannot determine the
	 * creation date of an item, and we return the same result that
	 * is obtained by {@link M_FtpItem::getLastModifiedDate()}.
	 * 
	 * @see M_FtpItem::getLastModifiedDate()
	 * @access public
	 * @return M_Date
	 */
	public function getCreationDate() {
		return $this->getLastModifiedDate();
	}
	
	/**
	 * Get last-access date
	 * 
	 * NOTE:
	 * In the case of an {@link M_FtpItem}, we cannot determine the
	 * last-access date of an item, and we return the same result that
	 * is obtained by {@link M_FtpItem::getLastModifiedDate()}.
	 * 
	 * @see M_FtpItem::getLastModifiedDate()
	 * @access public
	 * @return M_Date
	 */
	public function getLastAccessDate() {
		return $this->getLastModifiedDate();
	}
	
	/**
	 * Get permissions
	 * 
	 * @see M_Ftp::getPermissionsOfItem()
	 * @access public
	 * @return M_FsPermissions
	 */
	public function getPermissions() {
		return $this->getFs()->getPermissionsOfItem($this);
	}
	
	/**
	 * Get owner name
	 * 
	 * @see M_Ftp::getOwnerOfItem()
	 * @access public
	 * @return string
	 */
	public function getOwner() {
		return $this->getFs()->getOwnerOfItem($this);
	}
	
	/**
	 * Set owner
	 * 
	 * Will set with the item's owner. The owner is represented by an instance 
	 * of {@link M_FsOwner}.
	 * 
	 * Will return TRUE on success, FALSE on failure.
	 * 
	 * @access public
	 * @param M_FsOwner $owner
	 * @return bool
	 */
	public function setOwner(M_FsOwner $owner) {
		return $this->getFs()->setOwnerOfItem($owner, $this);
	}
	
	/**
	 * Copy the item
	 *
	 * @see M_Ftp::copyItem()
	 * @access public
	 * @param MI_FsItemDirectory $to
	 * 		The target location of the copied item
	 * @param string $name
	 * 		The (new) (base)name that will be given to the destination (copied)
	 *		item. If not provided, the original name is maintained.
	 * @return bool
	 */
	public function copy(MI_FsItemDirectory $to, $name = null) {
		return $to->place($this, $name);
	}
	
	/**
	 * Move the item
	 * 
	 * Copies the item to the specified target location, and then deletes
	 * the original copy.
	 * 
	 * @see M_FtpItem::copy()
	 * @see M_FtpItem::delete()
	 * @access public
	 * @param MI_FsItem $to
	 * 		The target location of the moved item
	 * @return bool
	 */
	public function move(MI_FsItemDirectory $to) {
		// If the item has been copied successfully
		if($this->copy($to)) {
			// Delete the original, and return result
			return $this->delete();
		}
		
		// If we're still here, we could not copy the original
		return FALSE;
	}
	
	/**
	 * Delete the item
	 * 
	 * @see M_Ftp::deleteItem()
	 * @access public
	 * @return bool
	 */
	public function delete() {
		return $this->getFs()->deleteItem($this);
	}
	
	/**
	 * Set the name of the item
	 * 
	 * @see M_Ftp::renameItem()
	 * @access public
	 * @param string $newName
	 * 		The new name of the item
	 * @return bool
	 */
	public function setName($newName) {
		return $this->getFs()->renameItem($this, $newName);
	}
	
	/**
	 * Set the path to the item
	 * 
	 * @access public
	 * @param string $path
	 * 		The path to the item
	 * @return void
	 */
	public function setPath($path) {
		$this->_path = $path;
	}
	
	/**
	 * Set permissions of the item
	 * 
	 * @access public
	 * @param M_FsPermissions $permissions
	 * 		The new permissions definition for the item
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function setPermissions(M_FsPermissions $permissions) {
		return $this->getFs()->setPermissionsOfItem($this, $permissions);
	}
	
	/**
	 * Check if an item is a link
	 * 
	 * Will check whether or not a given item is a symbolic link. Returns
	 * TRUE if the item is a link, FALSE if not.
	 * 
	 * @access public
	 * @return bool
	 */
	public function isLink() {
		return $this->getFs()->isItemLink($this);
	}
	
	/**
	 * Is this a file?
	 *
	 * @return bool
	 * @access public
	 * @author b.brughmans
	 */
	public function isFile() {
		return $this->getFs()->isFile($this);
	}
	
	/**
	 * Is this a directory?
	 * 
	 * @return bool
	 * @access public
	 * @author b.brughmans
	 */
	public function isDirectory() {
		return $this->getFs()->isDirectory($this);
	}
}