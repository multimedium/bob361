<?php
// SFTP Connection
// (An SFTP Connection is treated as a filesystem)
class M_Sftp implements MI_Fs {
	/**
	 * Connection URL
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_uri;
	
	/**
	 * SFTP Stream
	 *
	 * @access private
	 * @var Net_SFTP
	 */
	private $_sftp;
	
	/**
	 * Private key
	 *
	 * @access private
	 * @var M_File
	 */
	private $_privateKey;
	
	/**
	 * Private key: passphrase
	 * 
	 * @var string 
	 */
	private $_privateKeyPassphrase;
	
	/* -- Implementation specific to M_SFtp -- */
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param M_Uri $uri
	 * @return M_Ftp
	 */
	public function __construct(M_Uri $uri = NULL) {
		if($uri) {
			$this->setConnectionUrl($uri);
		}
		M_Loader::addIncludePath('core/_thirdparty/phpseclib');
		M_Loader::loadRelative('core/_thirdparty/phpseclib/Net/SFTP.php');
		M_Loader::loadRelative('core/_thirdparty/phpseclib/Crypt/RSA.php');
	}
	
	/**
	 * Destructor
	 * 
	 * When all references to this object are removed, the class will
	 * automatically close the open connection to the SFTP Server.
	 * 
	 * @access public
	 * @return void
	 */
	public function __destruct() {
		if($this->_sftp) {
			$this->_sftp->disconnect();
		}
	}
	
	/**
	 * Get Connection URL
	 *
	 * @access public
	 * @return M_Uri|NULL
	 */
	public function getConnectionUrl() {
		return $this->_uri;
	}
	
	/**
	 * Get private key
	 * 
	 * @return M_File|NULL
	 */
	public function getPrivateKey() {
		return $this->_privateKey;
	}
	
	/**
	 * Get private key passphrase
	 * 
	 * @return string|NULL
	 */
	public function getPrivateKeyPassphrase() {
		return $this->_privateKeyPassphrase;
	}
	
	/**
	 * Set Connection URL
	 *
	 * @access public
	 * @param M_Uri $uri
	 * @return void
	 */
	public function setConnectionUrl(M_Uri $uri) {
		$this->_uri = $uri;
	}
	
	/**
	 * Set private key
	 * 
	 * @param M_File $privateKey
	 */
	public function setPrivateKey(M_File $privateKey, $passphrase = null) {
		$this->_privateKey = $privateKey;
		$this->_privateKeyPassphrase = (string)$passphrase;
	}
	
	/**
	 * Connect to the SFTP Server
	 * 
	 * @throws M_FsException
	 * @access public
	 * @return void
	 */
	public function connect() {
		if(!$this->_sftp) {
			if(! $this->getConnectionUrl()) {
				throw new M_FsException('Cannot connect to SFTP Server; Missing Connection URL');
			}

			// Get port for connection (default to 22)
			$port = $this->getConnectionUrl()->getPort();
			if(! $port) {
				$port = 22;
			}

			$sftp = new Net_SFTP($this->getConnectionUrl()->getHost(), $port);

			if(! $sftp) {
				throw new M_FsException(sprintf('Could not connect to SFTP %s', $this->_uri->getHostWithPort()));
			}

			// If username and password has been given in the Connection
			// URL, we login to the SFTP Server
			$username = $this->getConnectionUrl()->getUsername();
			$password = $this->getConnectionUrl()->getPassword();
			if($username && $password) {
				if(! $sftp->login($username, $password)) {
					throw new M_FsException(sprintf(
						'Cannot login to SFTP %s with username %s and password %s',
						$this->getConnectionUrl()->getHostWithPort(),
						$this->getConnectionUrl()->getUsername(),
						$this->getConnectionUrl()->getPassword()
					));
				}
				
			// If username and private key has been given, we login to the SFTP
			// server
			}elseif($username && $this->getPrivateKey() && $this->getPrivateKey()->exists()) {
				
				// Create the key
				
				$key = new Crypt_RSA();
				if($this->getPrivateKeyPassphrase()) {
					$key->setPassword($this->getPrivateKeyPassphrase());
				}
				
				// Load the key in the crypt object
				if(! $key->loadKey($this->getPrivateKey()->getContents())) {
					throw new M_FsException(sprintf(
						'Cannot login to SFTP, the private key can not be loaded'
					));
				}
				
				// Try to login
				if(! $sftp->login($username, $key)) {
					throw new M_FsException(sprintf(
						'Cannot login to SFTP %s with username %s and the private key',
						$this->getConnectionUrl()->getHostWithPort(),
						$this->getConnectionUrl()->getUsername()
					));
				}
			}

			// Store a reference to the stream:
			$this->_sftp = $sftp;
		}
		
		return $this->_sftp;
	}
	
	/**
	 * Disconnect
	 * 
	 */
	public function disconnect() {
		$this->_getSFTP()->disconnect();
	}
	
	/**
	 * Download a file
	 * 
	 * @param M_FtpItemFile $file
	 * @return void
	 */
	public function download(M_FtpItemFile $file) {
		//we stop the output buffering start in the controller dispatcher.
		//the dispatcher buffers all output and displays it when all scripts
		//are processed. We don't want to let the dispatcher wait for this 
		//download
		ob_end_flush();
		
		$header = new M_Header();
		$header->sendContentTypeByFile($file);
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $file->getSize() ."; "); 
		$hostfile = fopen($this->_uri->toString(). $file->getPath(), 'r');
		while(!feof($hostfile)) {
			echo fread($hostfile, 1024);
			flush();
		}
		   
		fclose($hostfile);
	}
	
	/**
	 * Get size
	 * 
	 * Get the size, in bytes, of a given item on the SFTP Server.
	 * 
	 * NOTE:
	 * If you ask for the size of a folder, this method will sum up
	 * the sizes of all contained items.
	 *
	 * @access public
	 * @param M_FtpItem $item
	 * @return integer
	 */
	public function getSizeOfItem(M_FtpItem $item) {
		// We start with a size of 0
		$size = 0;
		
		// For each of the contained items, we add the size of the 
		// contained item to the total size:
		foreach($item->getItems() as $containedItem) {
			$size += $this->getSizeOfItem($containedItem);
		}
		
		// If the item is a file, we add the size of the item:
		if($item->getFsItemType() == MI_Fs::TYPE_FILE) {
			$size += $this->_getSFTP()->size($item->getPath());
		}
		
		// Return final size:
		return $size;
	}
	
	/**
	 * Delete an item
	 * 
	 * Will delete a given item from the SFTP Server.
	 * 
	 * NOTE:
	 * If the given item is a folder, this method will also delete all
	 * contained items in that folder.
	 *
	 * @access public
	 * @param M_FtpItem $item
	 * @return bool
	 */
	public function deleteItem(M_FtpItem $item) {
		// For each of the contained items:
		foreach($item->getItems() as $containedItem) {
			// Delete the contained item:
			$this->deleteItem($containedItem);
		}
		
		// Delete the item:
		if($item->getFsItemType() == MI_Fs::TYPE_DIRECTORY) {
			return $this->_getSFTP()->rmdir($item->getPath());
		} else {
			return $this->_getSFTP()->delete($item->getPath());
		}
	}
	
	/**
	 * Copy an item
	 * 
	 * Will copy an item from the SFTP Server to a given file.
	 * 
	 * @access public
	 * @param M_FtpItem $item
	 * 		The item (from the SFTP Server) to be copied
	 * @param MI_FsItem $to
	 * 		The target location of the copy. Note that we use the 
	 * 		interface, so the item can be located on any other filesystem.
	 * 		This way, you can copy from SFTP to local, from SFTP to other 
	 * 		FTP, etc...
	 * 		The target location can be a {@link M_FsFile} or a {@link M_FsDirectory},
	 * 		whenever a directory is passed: the destination file will be stored
	 * 		with the same filename as the source-file
	 * @return bool
	 */
	public function copyItem(M_FtpItem $item, MI_FsItem $to) {
		// copy from sftp to local
		if(M_Helper::isInstanceOf($to, 'M_Directory') || M_Helper::isInstanceOf($to, 'M_File')) {
			// Compose the destination path:
			$toPath = $item->getName();

			// if destination is a directory: prepend it to the destination path
			if (M_Helper::isInstanceOf($to, 'M_Directory')) {
				$toPath = $to->getPath().'/'.$toPath;
			}

			// Download the item to the local copy:
			return $this->_getSFTP()->get($item->getPath(), $toPath);
		}
		// From SFTP to SFTP
		else {
			throw new M_FsException('Cannot copy from (S)FTP to (S)FTP, not yet implemented');
		}
	}
	
	/**
	 * Rename item
	 *
	 * @access public
	 * @param M_FtpItem $item
	 * 		The item to be renamed
	 * @param string $newName
	 * 		The new name of the item
	 * @return bool
	 */
	public function renameItem(M_FtpItem $item, $newName) {
		return $this->_getSFTP()->rename($item->getPath(), (string) $newName);
	}
	
	/**
	 * Get permissions of item
	 * 
	 * @access public
	 * @param M_FtpItem $item
	 * @return M_FsPermissions
	 */
	public function getPermissionsOfItem(M_FtpItem $item) {
		// Get the raw info of the item:
		$raw = $this->_getRawInfoOfItem($item->getPath());
		
		// Construct an instance with permissions' info
		return M_FsPermissions::constructWithString($raw['chmod']);
	}
	
	/**
	 * Get owner of item
	 * 
	 * @access public
	 * @param M_FtpItem $item
	 * @return string
	 */
	public function getOwnerOfItem(M_FtpItem $item) {
		// Throw an exception:
		throw new M_Exception('Owner is not implemented in SFTP');
		
		// Get the raw info of the item:
		/*$raw = $this->_getRawInfoOfItem($item->getPath());
		
		// return the owner name:
		return $raw['owner'];*/
	}
	
	/**
	 * Is a given item a link?
	 * 
	 * Will tell if a given item on the SFTP Server is a link. Returns
	 * TRUE if the item is a link, FALSE if not
	 * 
	 * @access public
	 * @param M_FtpItem $item
	 * @return bool
	 */
	public function isItemLink(M_FtpItem $item) {
		// Get the raw info of the item:
		$raw = $this->_getRawInfoOfItem($item->getPath());
		
		// return the is-link flag
		return $raw['is link'];
	}
	
	/**
	 * Is a given item a file?
	 *
	 * @param M_FtpItem $item
	 * @return bool
	 * @author b.brughmans
	 */
	public function isFile(M_FtpItem $item) {
		return ($item->getFsItemType() == M_Fs::TYPE_FILE || $item->getFsItemType() == M_Fs::TYPE_ARCHIVE);
	}
	
	/**
	 * Is a given item a directory?
	 *
	 * @param M_FtpItem $item
	 * @return bool
	 * @author b.brughmans
	 */
	public function isDirectory(M_FtpItem $item) {
		return ($item->getFsItemType() == M_Fs::TYPE_DIRECTORY);
	}
	
	
	/**
	 * Is item existing?
	 * 
	 * Will check if a given item exists on the SFTP Server. Returns
	 * TRUE if the item exists, FALSE if not.
	 * 
	 * @access public
	 * @param M_FtpItem $item
	 * @return bool
	 */
	public function isItemOnServer(M_FtpItem $item) {
		// We try to get the raw info on the item:
		try {
			$this->_getRawInfoOfItem($item->getPath());
			return TRUE;
		} catch(Exception $e) {
			return FALSE;
		}
	}
	
	/**
	 * Get items in directory
	 * 
	 * @access public
	 * @param string $path
	 * @return ArrayIterator
	 */
	public function getItemsInDirectory(M_FtpItemDirectory $directory) {
		// Get raw info on the directory:
		// $raw = $this->_getRawInfoOfItem($directory->getPath());
		// The return value:
		$out = array();
		// For each of the items in the directory:
		foreach($this->_getRawList($directory->getPath()) as $item) {
			// If the current item is a directory, we construct an instance
			// of M_FtpItemDirectory
			if($this->_getItemType($item['path']) == MI_Fs::TYPE_DIRECTORY) {
				$out[] = new M_FtpItemDirectory($this, $item['path']);
			} else {
				$out[] = new M_FtpItemFile($this, $item['path']);
			}
		}
		
		// return the final collection of items:
		return new ArrayIterator($out);
	}
	
	/**
	 * Make a directory
	 * 
	 * Will attempt to create a directory on the SFTP Server.
	 * 
	 * @access public
	 * @param M_FtpItemDirectory $directory
	 * @return bool
	 */
	public function makeDirectory(M_FtpItemDirectory $directory) {
		// If the directory does not yet exist:
		if(! $this->isItemOnServer($directory)) {
			$dir  = explode('/', trim($directory->getPath(), '/'));
			$path = '';
			for($i = 0, $n = count($dir); $i < $n; $i ++) {
				$path .= '/' . $dir[$i];
				if(! $this->_getSFTP()->chdir($path)) {
					if(! $this->_getSFTP()->mkdir($path)) {
						return FALSE;
					}
				} else {
					// dir already exists, continue to next one...
				}
			}
		}
		return TRUE;
	}
	
	/**
	 * Set permissions of item
	 * 
	 * Will (try to) set the permissions of a given item on the SFTP
	 * server. Will return TRUE on success.
	 *
	 * @throws M_FsException
	 * @access public
	 * @param M_FtpItem $item
	 * 		The item of which to set the permissions
	 * @param M_FsPermissions $permissions
	 * 		The permissions
	 * @return bool
	 */
	public function setPermissionsOfItem(M_FtpItem $item, M_FsPermissions $permissions) {
		// Apply the permissions recursively
		foreach($item->getItems() as $containedItem) {
			$this->setPermissionsOfItem($containedItem, $permissions);
		}
		
		// Apply permissions on item:
		if($this->_getSFTP()->chmod($permissions->getOctalValue(), $item->getPath())) {
			return TRUE;
		} else {
			throw new M_FsException(sprintf(
				'Could not set permissions of %s on SFTP %s',
				$item->getPath(),
				$this->_uri->getHostWithPort()
			));
		}
	}
	
	/* -- MI_Fs implementations -- */
	
	/**
	 * Get type of file system
	 *
	 * @access public
	 * @return string
	 */
	public function getType() {
		return 'SFTP';
	}
	
	/**
	 * Get OS
	 * 
	 * Will provide with information about the OS that is running the
	 * filesystem. Typically, this method will release information
	 * about the OS that has been installed on the machine where the
	 * files are located.
	 * 
	 * Example 1, get the OS of the server on which the app is running
	 * <code>
	 *    $fs = new M_Fs();
	 *    echo $fs->getOS();
	 * </code>
	 * 
	 * Example 3, get the OS that is running on a remote SFTP Server
	 * <code>
	 *    $ftp = new M_Ftp(M_Uri('ftp://username:password@ftp.my-domain.com:21'));
	 *    $ftp->connect();
	 *    
	 *    echo $ftp->getOS();
	 * </code>
	 * 
	 * @access public
	 * @return string
	 * @todo implement
	 */
	public function getOS() {
		throw new M_Exception('getOs() not implemented');
	}
	
	/**
	 * Get temporary folder
	 * 
	 * Will return an object, that implements the {@link MI_FsItemDirectory} 
	 * interface, where temporary files are located.
	 * 
	 * @access public
	 * @return MI_FsItemDirectory
	 * @todo implement
	 */
	public static function getTemporaryDirectory() {
		
	}
	
	/**
	 * Get unique temporary folder
	 * 
	 * Will return an object, that implements the {@link MI_FsItemDirectory}
	 * interface, where temporary files are located. In addition to what the
	 * {@link MI_Fs::getTemporaryDirectory()} method already does, this method
	 * creates a UNIQUE folder inside the temporary folder.
	 * 
	 * @access public
	 * @return MI_FsItemDirectory
	 * @todo implement
	 */
	public static function getUniqueTemporaryDirectory() {
		
	}
	
	/**
	 * Get item
	 * 
	 * Will return 
	 * 
	 * @access public
	 * @param string $path
	 * 		The path to the item
	 * @return M_FtpItem
	 */
	public function getItem($path) {
		//root folder?
		if (M_Helper::ltrimCharlist($path, '/') == "") {
			return new M_FtpItemDirectory($this, "");
		}
		
		// Get the raw info of the item:
		$raw = $this->_getRawInfoOfItem($path);
		
		// If the item is a directory, we return an instance of
		// M_FtpItemDirectory. If not, we return an instance of 
		// M_FtpItemFile
		if($this->_getItemType($raw['path']) == MI_Fs::TYPE_DIRECTORY) {
			return new M_FtpItemDirectory($this, $raw['path']);
		} else {
			return new M_FtpItemFile($this, $raw['path']);
		}
	}

	/**
	 * Truncate directory
	 *
	 * Delete all items (files and subfolders) from a directory
	 *
	 * @access public
	 * @author Ben Brughmans
	 * @param M_FtpItemDirectory $dir
	 * @return bool
	 */
	public function truncate(M_FtpItemDirectory $dir) {
		try {
			foreach($dir->getItems() AS $item) {
				//if an item cannot be deleted: stop
				if (!$item->delete()) return false;
			}
			return true;
		}
		//if something went wrong: log and stop
		catch( M_FsException $e) {
			$e->log();
			return false;
		}
	}

	/**
	 * Upload an item (file/directory)
	 *
	 * @access public
	 * @param M_FtpItemDirectory $directory
	 *		The location to which the file/directory should be uploaded
	 * @param MI_FsItem $item
	 *		The item (file/directory) that is to be uploaded
	 * @return bool $flag
	 *		Returns TRUE on success, FALSE on failure
	 */
	public function upload(M_FtpItemDirectory $directory, MI_FsItem $item) {
		// If the item is a directory:
		if($item->isDirectory()) {
			// Then, we need to loop all of the items in the provided directory,
			// and upload each of those items separately:
		}
		// If the item is a file:
		else {
			// We create a local copy of the file, in the temporary directory.
			// First of all, get the temporary directory:
			$tempDirectory = M_Fs::getTemporaryDirectory();
			
			// This way, we can assume that we have a local file (which was not 
			// necessarily the case), which is easily uploaded to the SFTP server.
			if(! $item->copy($tempDirectory)) {
				// If we could not copy the file, then we throw an exception in
				// order to inform about the error
				throw new M_FsException(sprintf(
					'Cannot create a local copy of %s at %s, in order to upload ' .
					'the item to sftp directory %s',
					$item->getPath(),
					$tempDirectory->getPath(),
					$directory->getPath()
				));
			}

			// We compose the path to the local file:
			$local = $tempDirectory->getPath() . '/' . $item->getBasename();

			// Compose the path to the remote file:
			$path  = $directory->getPath() . '/';
			$path .= $item->getName();
			
			// Now, we upload the local file to the SFTP server:
			$rs = $this->_getSFTP()->put($path, $local, NET_SFTP_LOCAL_FILE);

			// If we failed to upload the file:
			if(! $rs) {
				// Then, we throw an exception
				throw new M_FsException(sprintf(
					'Could not upload item %s to %s; Permissions?',
					$item->getPath(),
					$path
				));
			}

			// Return TRUE, if still here
			return TRUE;
		}
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Get SFTP Stream
	 *
	 * Will provide with the SFTP Stream that has been created when
	 * connecting to the server. Note that an exception will be thrown
	 * if the stream is not available (e.g. if not yet connected)
	 * 
	 * @access private
	 * @return resource
	 */
	private function _getSFTP() {
		// If the stream is not available, it means that we have not 
		// yet connected to the SFTP, or that our attempt to connect
		// has failed. In that case, we throw an exception, to inform
		// about the error:
		if(! $this->_sftp) {
			throw new M_FsException('Not connected to SFTP');
		}
		
		// Return the stream:
		return $this->_sftp;
	}
	
	/**
	 * Get raw list
	 * 
	 * The return value is an array, of which each of the elements in
	 * turn are again arrays. Each of the elements contain the following
	 * keys:
	 * 
	 * <code>
	 *    chmod
	 *    num     
	 *    owner
	 *    group
	 *    size
	 *    month
	 *    day
	 *    time
	 *    name
	 * </code>
	 * 
	 * @access private
	 * @param string $path
	 * 		The complete path to the directory of which to get a list
	 * 		of contained items
	 * @return array
	 */
	private function _getRawList($path) {
		// echo 'raw list request on ' . $path . '<br>';
		// The return value is an array that contains raw data of the
		// files...
		$raw = array();

		//check the path: when no slash is found, we prepend the path with a /
		//otherwise it won't be found
		if (strlen($path) > 0 && $path{0} != '/') $path = '/'.$path;
	
		// Get a raw list of files in the directory, to determine the
		// type of the requested item (file/directory)
		// TODO: Should we use ftp_systype() to correspondingly parse the raw list?
		foreach($this->_getSFTP()->rawlist($path) as $name => $temp) {
			// Prepare an associative array of the current item, and 
			// add it to the raw data array
			if($name != 'total' && $name != '.' && $name != '..') {
				
				$date = new M_Date($temp['mtime']);
				$raw[] = array(
					'path'    => rtrim($path, " \t\n\r\0\x0B/") . '/' . $name,
					//'chmod'   => $temp['persmissions'],
					//'num'     => $temp[1],
					//'owner'   => $temp[2],
					//'group'   => $temp[3],
					'size'    => $temp['size'],
					'month'   => $date->toString('MMM', 'en'),
					'day'     => $date->toString('dd'),
					'time'    => $date->toString('HH:mm'),
					'name'    => $name,
					'is link' => FALSE,
					'mtime'	  => $temp['mtime']
				);
			}
		}

        // Sort the items from old to new:
		usort($raw, function($a, $b) {
			return $a['mtime'] > $b['mtime'];
		});
		
		// Return the RAW Data:
		return $raw;
	}
	
	/**
	 * Get raw info of item
	 *
	 * Will provide with an array of raw data about the requested item.
	 * This array will contain the following keys:
	 * 
	 * <code>
	 *    chmod
	 *    num     
	 *    owner
	 *    group
	 *    size
	 *    month
	 *    day
	 *    time
	 *    name
	 * </code>
	 * 
	 * NOTE:
	 * This method will throw an exception, if the item could not have
	 * been found on the SFTP server.
	 * 
	 * @throws M_FsException
	 * @access private
	 * @param string $path
	 * 		The complete path to the item
	 * @return array
	 */
	private function _getRawInfoOfItem($path) {
		// Get the name of the item:
		$name = basename($path);
		
		// For each of the items in the directory where the item is
		// located:
		foreach($this->_getRawList(dirname($path)) as $item) {
			// If the current item is the one we are looking for:
			if($item['name'] == $name) {
				// return the raw info of the item:
				return $item;
			}
		}
		throw new M_FsException(sprintf('Cannot find item %s on SFTP %s', $path, $this->_uri->getHostWithPort()));
	}
	
	/**
	 * Get type of item
	 * 
	 * Will tell whether the given item on the SFTP server is a file or 
	 * a directory.
	 *
	 * @access private
	 * @param string $path
	 * 		The complete path to the item on the remote server
	 * @return string
	 */
	private function _getItemType($path) {
		$type = M_Helper::getArrayElement('type', $this->_getSFTP()->stat($path));
		if($type == 2) {
			return MI_Fs::TYPE_DIRECTORY;
		} elseif($type == 1) {
			return MI_Fs::TYPE_FILE;
		} else {
			throw new M_Exception('Could not recognize the type of the item');
		}
	}
}