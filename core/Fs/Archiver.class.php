<?php
// Load Zip lib
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . FOLDER_THIRDPARTY . DIRECTORY_SEPARATOR . 'pclzip-2-6' . DIRECTORY_SEPARATOR . 'pclzip.lib.php';

/**
 * This class is used to create or extract archives. 
 * 
 * It's possible to add files and/or directories to a new archive, extract 
 * existing archives, etc...
 * 
 * Supports following extensions:
 */

class M_Archiver {
	
	/**
	 * @var array
	 */
	protected $_items = array();
	
	/**
	 * Get items
	 * 
	 * @see _items
	 * @return ArrayIterator
	 */
	public function getItems() {
		return new ArrayIterator($this->_items);
	}
	
	/**
	 * add an item to the archive
	 * 
	 * NOTE: the items will only be added fysically to the archive when 
	 * {@link M_Archiver::compress()} is called—
	 * 
	 * @param MI_FsItem $item
	 * @return M_Archiver
	 */
	public function addItem( MI_FsItem $item) {
		$this->_items[] = $item;
		return $this;
	}
	
	/**
	 * set the items to archive, all previous added items will get lost
	 *
	 * @param ArrayIterator $items
	 * @return M_Archiver
	 */
	public function setItems(ArrayIterator $items) {
		$this->_items = $items->getArrayCopy();
		return $this;
	}
	
	/**
	 * compress the items
	 * 
	 * @param $archive
	 * @param bool|string $removePath
	 * 		When set to TRUE the absolute path will be removed, if a string is 
	 * 		given: this string will be removed
	 * @return bool
	 * 		Success/failure
	 * 
	 * @todo: what with $result array (contains file info)?
	 */
	public function compress( M_Archive $archive, $removePath = false) {
		//nothing to compress
		if (count($this->_items) == 0) {
			throw new M_ArchiveException('Nothing to compress');
		}

		//begin new zip object
		$zip = new PclZip($archive->getPath());
			
		//loop through every item we want to add to the zip file
		$itemArray = array();
		foreach($this->_items AS $item) {
			$itemArray[] = $item->getPath();
		}

		//remove absolute path by default
		if ($removePath === true) {
			$removePath = M_Loader::getAbsolute('/');
		}
		
		//add to existing archive
		
		if ($archive->exists()) {
			$zip->add(
				$itemArray, 
				PCLZIP_OPT_REMOVE_PATH, 
				$removePath
			);
		}
		//create new archive
		else {
			$zip->create(
				$itemArray, 
				PCLZIP_OPT_REMOVE_PATH, 
				$removePath
			);
		}
		
		//failed
		if ($zip->error_code) {
			throw new M_ArchiveException($zip->errorInfo(), $zip->error_code);
		}
		
		return true;
	}
	
	/**
	 * Get uncompressed size for an archive
	 * 
	 * @param M_Archive $archive
	 * @return int
	 */
	public function getSizeUncompressed( M_Archive $archive ) {
		if ($archive->exists() == false) {
			$size = 0;
			/* @var $item MI_FsItem */
			foreach ($this->_items AS $item) {
				$size += $item->getSize();
			}
			return $size;
		}
		else {
			$zip = new PclZip($archive->getPath());
			
			return self::_calculateSizeUncompressedByContentList(
				$zip->listContent()
			);
		}
	}
	
	/**
	 * @var bool
	 */
	protected $_optionExtractRemoveAllPaths;
	
	/**
	 * @var bool
	 */
	protected $_optionExtractRemoveHiddenMacFiles = false;
	
	/**
	 * @return bool
	 */
	public function getOptionExtractRemoveHiddenMacFiles() {
		return $this->_optionExtractRemoveHiddenMacFiles;
	}
	
	/**
	 * This setting will remove any hidden mac files when extraction an archive
	 * 
	 * @param bool $_optionExtractRemoveHiddenMacFiles
	 */
	public function setOptionExtractRemoveHiddenMacFiles($_optionExtractRemoveHiddenMacFiles) {
		$this->_optionExtractRemoveHiddenMacFiles = $_optionExtractRemoveHiddenMacFiles;
	}
	
	/**
	 * This setting will remove any paths from the archive
	 * 
	 * Example:
	 * 
	 * archive contains 3 files: "test.txt", "files/file.png" and "data/doc/word.doc"
	 * extracting the archive will produce the files without any folder
	 * 	- text.txt
	 *  - file.png
	 *  - word.doc
	 * 
	 * @param bool $_optionExtractRemovePath
	 */
	public function setOptionExtractRemovePath($_optionExtractRemovePath) {
		$this->_optionExtractRemoveAllPaths = $_optionExtractRemovePath;
	}
	
	/**
	 * @return bool
	 */
	public function getOptionExtractRemoveAllPaths() {
		return $this->_optionExtractRemoveAllPaths;
	}
	
	/**
	 * Extract this archive to a directory
	 * 
	 * Note: it's designated to use a try {} catch statement when extracting
	 * an Archive. When the extracting fails an {@see M_ArchiveException} will be
	 * thrown
	 * 
	 * @param M_Archive $archive
	 * 			Archive which needs to be extracted
	 * @param MI_FsItemDirectory|null $directory
	 * 			The destination directory, leave empty to use tmp directory
	 * @return MI_FsItemDirectory 
	 * 			The directory in which the files will be extracted, if something
	 * 			goes wrong an exception will be thrown
	 */
	public function extract(M_Archive $archive, MI_FsItemDirectory $directory = null) {
		if ($archive->exists() == false) {
			throw new M_ArchiveException('Cannot extract inexistant archive '.
				$this->getDirectoryAndFilename()
			);
		}
		
		//No directory provided, we use a temporary directory
		if (is_null($directory)) {
			$directory = M_Fs::getUniqueTemporaryDirectory();
		}
		
		/* @var $directory MI_FsItemDirectory */
		if ($directory->exists() == false) $directory->make();
			
		//extract to tmp folder
		$zip = new PclZip($archive->getPath());
		
		$callback = new M_Callback();
		$callback->setObject($zip);
		$callback->setFunctionName('extract');
		
		//remove all hidden mac os files?
		//if ($this->getOptionExtractRemoveHiddenMacFiles()) {
		//	$callback->addArgument(PCLZIP_OPT_BY_PREG);
		//	$callback->addArgument("^[^._]{3}.*/");
		//}
		
		//remove all paths?
		//if ($this->getOptionExtractRemoveAllPaths()) {
		//	$callback->addArgument(PCLZIP_OPT_REMOVE_ALL_PATH);
		//}
		
		//if not: we remove the root path by default
		//else {
			$callback->addArgument(PCLZIP_OPT_PATH);
			$callback->addArgument($directory->getPath());
		//}

		$result = $callback->run();

		//if archive has been extracted successfully
		if (is_array($result)) {
			return $directory;
		}

		//some error has occurred
		else {
			throw new M_ArchiveException(
				sprintf(
					'Archive could not be extracted to %s', 
					$directory->getPath()
				)
			); 
		}
	}
	
	/**
	 * Calculate the total uncompressed size of an archive by content list
	 * 
	 * The contentlist array is generated by {@link PclZip} and has following
	 * structure:
	 * 
	 * Array
	 *	(
	 *	    [0] => Array
	 *	        (
	 *	            [filename] => index.php
	 *	            [stored_filename] => index.php
	 *	            [size] => 937
	 *	            [compressed_size] => 482
	 *	            [mtime] => 1236590308
	 *	            [comment] => 
	 *	            [folder] => 
	 *	            [index] => 0
	 *	            [status] => ok
	 *	            [crc] => 720283048
	 *	        )
	 *	)
	 * 
	 * @param array $contentList
	 * @return int
	 */
	private function _calculateSizeUncompressedByContentList( array $contentList) {
		$size = 0;
		
		foreach( $contentList as $value ) {
			$size += $value['size'];
		}
		
		return $size;
	}
	
	/**
	 * Calculate the total compressed size of an archive by content list
	 * 
	 * @see {M_Archiver::_calculateSizeUncompressedByContents()}
	 * @param array $contentList
	 * @return int
	 */
	private function _calculateSizeCompressedByContentList( array $contentList) {
		$size = 0;
		
		foreach( $contentList as $value ) {
			$size += $value['compressed_size'];
		}
		
		return $size;
	}
}