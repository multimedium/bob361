<?php
class M_ContactEmail {
	const TYPE_WORK = 'work';
	const TYPE_PRIVATE = 'private';
	
	protected $_email;
	protected $_name;
	protected $_type;
	
	public function __construct($email, $type = self::TYPE_WORK) {
		$this->setEmail($email);
		$this->setType($type);
	}
	
	public function setEmail($email) {
		$validator = new M_ValidatorIsEmail;
		if(! $validator->check($email)) {
			throw new M_ContactException(sprintf(
				'Cannot set Contact Email Address to %s; invalid email address',
				$email
			));
		}
		$this->_email = (string) $email;
	}
	
	public function setName($name) {
		$this->_name = (string) $name;
	}
	
	public function setType($type) {
		$this->_type = (string) $type;
	}
	
	public function getEmail() {
		return $this->_email;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function getType() {
		return $this->_type;
	}
}