<?php
/**
 * M_ContactAddress
 * 
 * @package Core
 */
class M_ContactAddress extends M_Object {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Address type: work
	 * 
	 * @var string
	 */
	const TYPE_WORK = 'work';
	
	/**
	 * Address type: private
	 * 
	 * @var string
	 */
	const TYPE_PRIVATE = 'private';
	
	/* -- PROPERTIES -- */

	/**
	 * Holds a name for the address
	 * 
	 * @see M_ContactAddress::setName()
	 * @see M_ContactAddress::getName()
	 * @access private
	 * @var string
	 */
	private $_name;

	/**
	 * Holds the complete street address (name + number + box number)
	 * 
	 * @see M_ContactAddress::setStreetAddress()
	 * @see M_ContactAddress::getStreetAddress()
	 * @access private
	 * @var string
	 */
	private $_streetAddress;
	
	/**
	 * Holds the street name
	 * 
	 * @see M_ContactAddress::setStreetName()
	 * @see M_ContactAddress::getStreetName()
	 * @access private
	 * @var string
	 */
	private $_streetName;

	/**
	 * Holds the street number
	 * 
	 * @see M_ContactAddress::setStreetNumber()
	 * @see M_ContactAddress::getStreetNumber()
	 * @access private
	 * @var string
	 */
	private $_streetNumber;

	/**
	 * Holds the box number
	 * 
	 * @see M_ContactAddress::setBoxNumber()
	 * @see M_ContactAddress::getBoxNumber()
	 * @access private
	 * @var string
	 */
	private $_boxNumber;

	/**
	 * Holds the city
	 * 
	 * @see M_ContactAddress::setCity()
	 * @see M_ContactAddress::getCity()
	 * @access private
	 * @var string
	 */
	private $_city;

	/**
	 * Holds the postal code
	 * 
	 * @see M_ContactAddress::setPostalCode()
	 * @see M_ContactAddress::getPostalCode()
	 * @access private
	 * @var string
	 */
	private $_postalCode;

	/**
	 * Holds the region
	 * 
	 * @see M_ContactAddress::setRegion()
	 * @see M_ContactAddress::getRegion()
	 * @access private
	 * @var string
	 */
	private $_region;

	/**
	 * Holds the country
	 * 
	 * @see M_ContactAddress::setCountry()
	 * @see M_ContactAddress::getCountry()
	 * @access private
	 * @var string
	 */
	private $_country;

	/**
	 * Holds the country as ISO code
	 * 
	 * @see M_ContactAddress::setCountryISO()
	 * @see M_ContactAddress::getCountryISO()
	 * @access private
	 * @var string
	 */
	private $_countryISO;

	/**
	 * Set the type of address
	 * 
	 * @see M_ContactAddress::TYPE_WORK
	 * @see M_ContactAddress::TYPE_PRIVATE
	 * @see M_ContactAddress::setType()
	 * @see M_ContactAddress::getType()
	 * @access private
	 * @var string
	 */
	private $_type;

	/**
	 * Latitude for this address
	 * 
	 * @see M_ContactAddress::setLatitude()
	 * @see M_ContactAddress::getLatitude()
	 * @access private
	 * @var string
	 */
	protected $_latitude;

	/**
	 * Longitude for this address
	 * 
	 * @see M_ContactAddress::setLongitude()
	 * @see M_ContactAddress::getLongitude()
	 * @access private
	 * @var string
	 */
	protected $_longitude;

	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $streetAddress
	 * @param string $city
	 * @param string $postalCode
	 * @param string $region
	 * @param string $country
	 * @return M_ContactAddress
	 */
	public function __construct($streetAddress = NULL, $city = NULL, $postalCode = NULL, $region = NULL, $country = NULL) {
		if($streetAddress) {
			$this->setStreetAddress($streetAddress);
		}
		if($city) {
			$this->setCity($city);
		}
		if($postalCode) {
			$this->setPostalCode($postalCode);
		}
		if($region) {
			$this->setRegion($region);
		}
		if($country) {
			$this->setCountry($country);
		}
	}

	/* -- SETTERS -- */

	/**
	 * Set name/title of the address
	 *
	 * @access public
	 * @param string $name
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setName($name) {
		$this->_name = (string) $name;
		return $this;
	}

	/**
	 * Set the street address
	 *
	 * @access public
	 * @param string $address
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setStreetAddress($address) {
		$this->_streetAddress = (string) $address;
		return $this;
	}

	/**
	 * Set the street name
	 *
	 * @access public
	 * @param string $streetName
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setStreetName($streetName) {
		$this->_streetName = (string) $streetName;
		return $this;
	}
	
	/**
	 * Set the street number
	 *
	 * @access public
	 * @param string $streetNumber
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setStreetNumber($streetNumber) {
		$this->_streetNumber = (string) $streetNumber;
		return $this;
	}
	
	/**
	 * Set the box number
	 *
	 * @access public
	 * @param string $boxNumber
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setBoxNumber($boxNumber) {
		$this->_boxNumber = (string) $boxNumber;
		return $this;
	}
	
	/**
	 * Set the city
	 *
	 * @access public
	 * @param string $city
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCity($city) {
		$this->_city = (string) $city;
		return $this;
	}
	
	/**
	 * Set the postalcode
	 *
	 * @access public
	 * @param string $postalCode
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPostalCode($postalCode) {
		$this->_postalCode = (string) $postalCode;
		return $this;
	}
	
	/**
	 * Set the region
	 *
	 * @access public
	 * @param string $region
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setRegion($region) {
		$this->_region = (string) $region;
		return $this;
	}
	
	/**
	 * Set the country
	 *
	 * @access public
	 * @param string $country
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCountry($country) {
		$this->_country = (string) $country;
		return $this;
	}
	
	/**
	 * Set the country ISO
	 *
	 * @access public
	 * @param string $countryISO
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCountryISO($countryISO) {
		$this->_countryISO = (string) $countryISO;
		return $this;
	}
	
	/**
	 * Set the addresss-type
	 *
	 * @access public
	 * @param string $type
	 * @see M_ContactAddress::TYPE_PRIVATE
	 * @see M_ContactAddress::TYPE_WORK
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setType($type) {
		$this->_type = (string) $type;
		return $this;
	}

	/**
	 * Set Latitude
	 *
	 * @access public
	 * @param string $arg
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setLatitude($arg) {
		$this->_latitude = $arg;
		return $this;
	}
	
	/**
	 * Set Longitude
	 *
	 * @access public
	 * @param string $arg
	 * @return M_ContactAddress
	 *		Returns itself, for a fluent programming interface
	 */
	public function setLongitude($arg) {
		$this->_longitude = $arg;
		return $this;
	}

	/* -- GETTERS -- */

	/**
	 * Get Name
	 *
	 * @see M_ContactAddress::setName()
	 * @access public
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * Get Latitude
	 *
	 * @see M_ContactAddress::getLatitude()
	 * @access public
	 * @return string
	 */
	public function getLatitude() {
		return $this->_latitude;
	}

	/**
	 * Get Longitude
	 *
	 * @see M_ContactAddress::setLongitude()
	 * @access public
	 * @return string
	 */
	public function getLongitude() {
		return $this->_longitude;
	}

	/**
	 * Get the streetAddress
	 * 
	 * In some cases the streetname, streetnumber and box number are stored in
	 * one field: the street-address. In other cases these data is stored
	 * separately in 3 fields. When this is the case we return the combination
	 * of {@link M_ContactAddress::getStreetName()},
	 * {@link M_ContactAddress::getStreetNumber()} and
	 * {@link M_ContactAddress::getBoxNumber()}
	 * 
	 * @see M_ContactAddress::setStreetAddress()
	 * @access public
	 * @return string
	 */
	public function getStreetAddress() {
		$streetAddress = array();
		//a specific street address has been set
		if($this->_streetAddress) {
			$streetAddress[] = $this->_streetAddress;
		}
		//return combination of streetname, streetnumber and box number
		else {
			if ($this->getStreetName()) $streetAddress[] = $this->getStreetName();
			if ($this->getStreetNumber()) $streetAddress[] = $this->getStreetNumber();
			if ($this->getBoxNumber()) $streetAddress[] = $this->getBoxNumber();
		}
		
		return implode(' ', $streetAddress);
	}
	
	/**
	 * Get Streetname
	 * 
	 * @see M_ContactAddress::setStreetName()
	 * @access public
	 * @return string
	 */
	public function getStreetName() {
		return $this->_streetName;
	}
	
	/**
	 * Get streetnumber
	 * 
	 * @see M_ContactAddress::setStreetNumber()
	 * @access public
	 * @return string
	 */
	public function getStreetNumber() {
		return $this->_streetNumber;
	}
	
	
	/**
	 * Get box number
	 * 
	 * @see M_ContactAddress::setBoxNumber()
	 * @access public
	 * @return string
	 */
	public function getBoxNumber() {
		return $this->_boxNumber;
	}
	
	/**
	 * Get city
	 * 
	 * @see M_ContactAddress::setCity()
	 * @access public
	 * @return string
	 */
	public function getCity() {
		return $this->_city;
	}
	
	/**
	 * Get postalcode
	 * 
	 * @see M_ContactAddress::setPostalCode()
	 * @access public
	 * @return string
	 */
	public function getPostalCode() {
		return $this->_postalCode;
	}
	
	/**
	 * Get region
	 * 
	 * @see M_ContactAddress::setRegion()
	 * @access public
	 * @return string
	 */
	public function getRegion() {
		return $this->_region;
	}
	
	/**
	 * Get country
	 * 
	 * @see M_ContactAddress::setCountry()
	 * @access public
	 * @return string
	 */
	public function getCountry() {
		return $this->_country;
	}
	
	/**
	 * Get country ISO
	 * 
	 * @see M_ContactAddress::setCountryISO()
	 * @access public
	 * @return string
	 */
	public function getCountryISO() {
		return $this->_countryISO;
	}
	
	/**
	 * Get type
	 * 
	 * @see M_ContactAddress::setType()
	 * @access public
	 * @return string
	 */
	public function getType() {
		return $this->_type;
	}
	
	/**
	 * Get address string
	 * 
	 * @todo: Should the string be localized?
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// Output
		$t = array();
		
		// If a street address (including house number and box number) is known
		if($this->getStreetAddress()) {
			// Then, we add the street address to the output:
			$t[] = $this->getStreetAddress();
		}
		
		// If a city name is known
		if($this->getCity()) {
			// And also a postal code has been defined:
			if($this->getPostalCode()) {
				// Then, we add the postal code with the city name
				$t[] = $this->getPostalCode() . ' ' . $this->getCity();
			}
			// If the postal code has not been defined
			else {
				// Then, we only add the city name
				$t[] = $this->getCity();
			}
		}
		
		// If a region is known:
		if($this->getRegion()) {
			// Then, we add the region string to the output
			$t[] = $this->getRegion();
		}
		
		// If a country is known:
		if($this->getCountry()) {
			// Then, we add the country string to the output
			$t[] = $this->getCountry();
		}
		
		// Return the address string
		return implode(', ', $t);
	}

	/**
	 * Get this address as string
	 *
	 * @see M_ContactAddress::toString();
	 * @author Ben Brughmans
	 */
	public function __toString() {
		return $this->toString();
	}
}