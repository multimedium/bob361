<?php
/**
 * M_OneTimePasswordServer
 *
 * @package Core
 */
class M_OneTimePasswordServer extends M_OneTimePasswordSystem {

	/* -- PROPERTIES -- */

	/**
	 * @var MI_OneTimePasswordServerStorage
	 */
	private $_storage;

	/* -- CONSTRUCTOR -- */

	/**
	 * One-Time Password Server Constructor
	 *
	 * @uses M_OneTimePasswordServer::setHashFunction()
	 * @uses M_OneTimePasswordServer::setSeed()
	 * @uses M_OneTimePasswordServer::setSequenceNumber()
	 * @access protected
	 * @param string $hashFunction
	 *		The hash function that is to be used by the server to send the
	 *		challenge to the client, or one-time password generator
	 * @param string $seed
	 *		The seed that is to be used by the server to send the
	 *		challenge to the client, or one-time password generator
	 * @param string $sequenceNumber
	 *		The sequence number that is to be used by the server to send the
	 *		challenge to the client, or one-time password generator
	 * @return M_OneTimePasswordServer
	 */
	protected function  __construct($hashFunction = NULL, $seed = NULL, $sequenceNumber = NULL) {
		// If the hash function has been provided
		if($hashFunction) {
			// Then, we set the hash function in the server object:
			$this->_setHashFunction($hashFunction);
		}

		// If the seed has been provided
		if($seed) {
			// Then, we set the seed in the server object
			$this->_setSeed($seed);
		}

		// If the sequence number has been provided
		if($sequenceNumber) {
			// Then, we provide the server object with the sequence number
			$this->_setSequenceNumber($sequenceNumber);
		}
	}

	/**
	 * Construct with {@link MI_OneTimePasswordServerStorage} instance
	 *
	 * @access public
	 * @return M_OneTimePasswordServer
	 */
	public static function constructWithStorage(MI_OneTimePasswordServerStorage $storage) {
		// First of all, we construct a new server object
		$server = new self(
			// The hash function that is to be used by the server to send the
			// challenge to the client, or one-time password generator
			$storage->getHashFunction(),
			// The seed that is to be used by the server to send the challenge
			// to the client, or one-time password generator
			$storage->getSeed(),
			// The sequence number that is to be used by the server to send the
			// challenge to the client, or one-time password generator
			$storage->getSequenceNumber()
		);

		// Also, set the internal reference to the storage object:
		$server->_storage = $storage;

		// Return the constructed server object:
		return $server;
	}

	/**
	 *
	 */
	public function setOneTimePassword($oneTimePassword) {
		// We need to verify whether or not the provided password is the inverted
		// hash of the previous password. In order to check this, we hash the
		// the provided password and compare it to the previous one.
		$previous = $this->getStorage()->getOneTimePassword();

		// If no previous one-time password is available:
		if(! $previous) {
			// Then we cannot compare to any value! We throw an exception, to
			// inform about the error:
			throw new M_OneTimePasswordException(sprintf(
				'The One-Time Password Server object cannot find the previous ' .
				'one-time password, and can therefore not validte the provided ' .
				'password!'
			));
		}

		// Now, we compare the result of the hash function on the provided
		// one-time password with the previous password. Note that we will compare
		// bitstrings. First, we get the bitstring fot the provided password:
		$bits1 = $this->_getBitStringFromHash($this->_getHashFromOneTimePassword($oneTimePassword));
		
		// Then, we get the bitstring for the previous password:
		$bits2 = $this->_getBitStringFromOneTimePassword($previous);

		// These two bitstrings should now be a perfect match!
		if($bits1 == $bits2) {
			// If they do not match, then we have an error!
			throw new M_OneTimePasswordException(sprintf(
				'The One-Time Password is not valid, and does not form part of ' .
				'One-Time Password Sequence.'
			));
		}

		// Return TRUE if still here
		return TRUE;
	}
	
	/* -- GETTERS -- */

	/**
	 * @return MI_OneTimePasswordServerStorage
	 */
	public function getStorage() {
		return $this->_storage;
	}

	/**
	 * Get the challenge
	 *
	 * @access public
	 * @return string
	 */
	public function getChallenge() {
		// We make sure that a hash function has been defined for the server:
		if(! $this->getHashFunction()) {
			// If no hash function has been defined, then the server object is
			// unable to compose a challenge string for the generator. In this
			// case, we throw an exception to inform about the error:
			throw new M_OneTimePasswordException(sprintf(
				'The One-Time Password Server object cannot create a challenge. ' .
				'Missing the definition of the hash function!'
			));
		}

		// We make sure that a seed has been defined for the server:
		if(! $this->getSeed()) {
			// If no seed has been defined, then the server object is unable to
			// compose a challenge string for the generator. In this case, we
			// throw an exception to inform about the error:
			throw new M_OneTimePasswordException(sprintf(
				'The One-Time Password Server object cannot create a challenge. ' .
				'Missing the definition of the seed!'
			));
		}

		// We make sure that a sequence number has been defined for the server:
		if(! $this->getSequenceNumber()) {
			// If no sequence number has been defined, then the server object is
			// unable to compose a challenge string for the generator. In this
			// case, we throw an exception to inform about the error:
			throw new M_OneTimePasswordException(sprintf(
				'The One-Time Password Server object cannot create a challenge. ' .
				'Missing the definition of the sequence number!'
			));
		}

		// Return the challenge string:
		return strtr(
			'otp-@hash @sequence @seed',
			array(
				'@hash'     => $this->getHashFunction(),
				'@seed'     => $this->getSeed(),
				'@sequence' => $this->getSequenceNumber()
			)
		);
	}
}