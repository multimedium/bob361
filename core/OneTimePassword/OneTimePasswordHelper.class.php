<?php
/**
 * M_OneTimePasswordHelper
 *
 * @package Core
 */
class M_OneTimePasswordHelper extends M_Object {
	/**
	 * Get supported hash algorithms
	 *
	 * The RFC-2289 Specification mentions the algorithms sha1, md5 and md4 for
	 * the digesting of one-time passwords. However, other algorithms may be added
	 * to this list, and each implementation chooses which hashing algorithms
	 * it supports.
	 *
	 * Since MD4 and MD5 have been proven not to be secure anymore, because of
	 * the probability of collisions, we have chosen not to support these algorithms
	 * in our implementation of the One-Time Password system. Instead, we will
	 * typically rely on the safety of a sha1() digest.
	 *
	 * This method will provide with the list of algorighms that is currently
	 * being supported by our One-Time Password system.
	 *
	 * @static
	 * @access public
	 * @return array $algorithms
	 *		The list of supported algorithms
	 */
	public static function getSupportedHashFunctions() {
		return array('sha1');
	}

	/**
	 * Get random seed
	 *
	 * This method will provide with a random seed, that can be used in a
	 * challenge sent by the One-Time Password Server object. The result of this
	 * method complies with the RFC 2289 specifications. For more information
	 * about the format in which a seed is expected, read the documentation on
	 * {@link M_OneTimePasswordHelper::isValidSeed()}
	 *
	 * @see M_OneTimePasswordHelper::isValidSeed()
	 * @static
	 * @access public
	 * @param array $excludeSeeds
	 *		By adding a seed to the secret pass-phrase, the pass-phrase can safely
	 *		be used on multiple clients, or one-time password generators. In order
	 *		to make sure that no duplicate seeds are created, you can provide a
	 *		list of seeds that can no longer be used to this method. This method
	 *		will then make sure that is returns a seed that is not yet in use.
	 * @return string $seed
	 */
	public static function getRandomSeed(array $excludeSeeds = array()) {
		// We compose the collection of characters that are allowed in a seed:
		$chars = array(
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c',
			'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
			'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
		);

		// For randomization, we shuffle the collection of characters:
		shuffle($chars);

		// We determine the (random) length of the seed. Note that, according
		// to the RFC 2289 specifications, the seed must not be longer than 16
		// characters. Also, our implementation creates seeds with a minimum
		// of 8 characters:
		$length = rand(8, 16);

		// We initiate the seed string:
		$seed = '';

		// Now, we compose a seed. While our seed is shorter than the randomly
		// determined length:
		for($i = 0; $i < $length; $i ++) {
			// We add the next character to the seed:
			$seed .= $chars[$i];
		}

		// While the random seed that has been generated by this method is present
		// in the list of seeds to be excluded:
		while(in_array($seed, $excludeSeeds)) {
			// We generate another random seed:
			$seed = self::getRandomSeed($excludeSeeds);
		}

		// We return the seed
		return $seed;
	}

	/**
	 * Validate a hashing algorithm
	 *
	 * The OTP system generator passes the user's secret pass-phrase, along with
	 * a seed received from the server as part of the challenge, through multiple
	 * iterations of a secure hash function to produce a one-time password.
	 *
	 * This method will tell if a given hash function is supported by this
	 * implementation of the One-Time Password system.
	 *
	 * @access public
	 * @param string $hashFunction
	 * @return bool $flag
	 *		Returns TRUE if the hash function is supported, FALSE if not
	 */
	public static function isValidHashFunction($hashFunction) {
		return in_array($hashFunction, self::getSupportedHashFunctions());
	}

	/**
	 * Validate an alternative dictionary
	 *
	 * In addition to accepting six-word and hexadecimal encodings of the 64 
	 * bit one-time password, servers SHOULD accept an alternate dictionary 
	 * encoding.  The six words in this encoding MUST not overlap the set of 
	 * words in the standard dictionary.
	 * 
	 * The purpose of alternative dictionary encoding of the OTP one-time
	 * password is to allow the use of language specific or friendly words. As
	 * case translation is not always well defined, the alternative dictionary
	 * encoding is case sensitive.  Servers SHOULD accept this encoding in
	 * addition to the standard 6-word and hexadecimal encodings.
	 *
	 * The standard 6-word encoding uses the placement of a word in the dictionary
	 * to represent an 11-bit number. The 64-bit one-time password can then be
	 * represented by six words.
	 *
	 * The alternative dictionary MUST:
	 *
	 * - contain 2048 words
	 * - words with no less than 1 letter, and no more than 4 letters
	 * - not contain words from the default dictionary
	 *
	 * @uses M_OneTimePasswordHelper::getDefaultDictionary()
	 * @static
	 * @access public
	 * @param array $dictionary
	 *		The alternative dictionary to be validated
	 * @return bool $flag
	 *		Returns TRUE if the alternative dictionary is considered valid, FALSE
	 *		if not.
	 */
	public static function isValidAlternativeDictionary(array $dictionary) {
		// First of all, the dictionary must contain 2048 entries:
		if(count($dictionary) != 2048) {
			// If it does not, we return FALSE to indicate an invalid dictionary
			return false;
		}

		// We load default RFC 2289 dictionary, for comparison. The words in the
		// alternative dictionary MUST not overlap the set of words in the default
		// dictionary!
		$defaultDictionary = self::getDefaultDictionary();

		// For each of the words in the provided, alternative, dictionary
		foreach($dictionary as $word) {
			// Each and every word in the alternative dictionary must contain
			// only 1 to 4 letters:
			if(! preg_match('/^[a-zA-Z]{1,4}$/', $word)) {
				// If the word is larger, or it contains forbidden characters,
				// we return FALSE to indicate an invalid dictionary.
				return false;
			}

			// The word must not be in the default dictionary:
			if(in_array(strtoupper($word), $defaultDictionary)) {
				// If the word is already present in the default dictionary,
				// we return FALSE to indicate an invalid dictionary.
				return false;
			}
		}

		// If we are still here, then we return TRUE, to indicate a valid
		// alternative dictionary:
		return true;
	}

	/**
	 * Validate a sequence number
	 *
	 * The sequence number and seed together constitute a larger unit of data
	 * called the challenge. The challenge gives the generator the parameters it
	 * needs to calculate the correct one-time password from the secret
	 * pass-phrase. The challenge MUST be in a standard syntax so that automated
	 * generators can recognize the challenge in context and extract these
	 * parameters. The syntax of the challenge is:
	 *
	 * otp-<algorithm identifier> <sequence integer> <seed>
	 *
	 * @see M_OneTimePasswordHelper::isValidSeed()
	 * @static
	 * @access public
	 * @param string $sequenceNumber
	 *		The sequence number to be validated
	 * @return bool $flag
	 *		Returns TRUE if the sequence number is valid, FALSE if not
	 */
	public static function isValidSequenceNumber($sequenceNumber) {
		return (is_numeric($sequenceNumber) && preg_match('/^[0-9]+$/', $sequenceNumber));
	}

	/**
	 * Validate a seed
	 *
	 * The seed is provided by the One-Time Password Server, as part of the
	 * challenge. The pass-phrase is concatenated with a seed that is transmitted
	 * from the server in clear text. This non-secret seed allows clients to use
	 * the same secret pass-phrase on multiple machines (using different seeds)
	 * and to safely recycle their secret pass-phrases by changing the seed.
	 *
	 * The seed MUST consist of purely alphanumeric characters and MUST be of
	 * one to 16 characters in length. The seed is a string of characters that
	 * MUST not contain any blanks and SHOULD consist of strictly alphanumeric
	 * characters from the ISO-646 Invariant Code Set. The seed MUST be case
	 * insensitive and MUST be internally converted to lower case before it is
	 * processed.
	 *
	 * @static
	 * @access public
	 * @param string $seed
	 *		The seed to be validated
	 * @return bool $flag
	 *		Returns TRUE if the seed is valid, FALSE if not
	 */
	public static function isValidSeed($seed) {
		return (is_string($seed) && preg_match('/^[0-9a-z]{1,16}$/', $seed));
	}

	/**
	 * Get dictionary for Converting Between 6-Word and Binary Formats
	 *
	 * The one-time password may be converted to, and all servers must be capable
	 * of accepting it as, a sequence of six short (1 to 4 letter) easily typed
	 * words that only use characters from ISO-646 IVCS. Each word is chosen
	 * from a dictionary of 2048 words; at 11 bits per word, all one-time passwords
	 * may be encoded.
	 *
	 * This method will provide with the described collection of 1 to 4 letter
	 * words that can be used to convert one time passwords.
	 *
	 * @static
	 * @access public
	 * @return array
	 */
	public static function getDefaultDictionary() {
		return array(
			'A',     'ABE',   'ACE',   'ACT',   'AD',    'ADA',   'ADD',
			'AGO',   'AID',   'AIM',   'AIR',   'ALL',   'ALP',   'AM',    'AMY',
			'AN',    'ANA',   'AND',   'ANN',   'ANT',   'ANY',   'APE',   'APS',
			'APT',   'ARC',   'ARE',   'ARK',   'ARM',   'ART',   'AS',    'ASH',
			'ASK',   'AT',    'ATE',   'AUG',   'AUK',   'AVE',   'AWE',   'AWK',
			'AWL',   'AWN',   'AX',    'AYE',   'BAD',   'BAG',   'BAH',   'BAM',
			'BAN',   'BAR',   'BAT',   'BAY',   'BE',    'BED',   'BEE',   'BEG',
			'BEN',   'BET',   'BEY',   'BIB',   'BID',   'BIG',   'BIN',   'BIT',
			'BOB',   'BOG',   'BON',   'BOO',   'BOP',   'BOW',   'BOY',   'BUB',
			'BUD',   'BUG',   'BUM',   'BUN',   'BUS',   'BUT',   'BUY',   'BY',
			'BYE',   'CAB',   'CAL',   'CAM',   'CAN',   'CAP',   'CAR',   'CAT',
			'CAW',   'COD',   'COG',   'COL',   'CON',   'COO',   'COP',   'COT',
			'COW',   'COY',   'CRY',   'CUB',   'CUE',   'CUP',   'CUR',   'CUT',
			'DAB',   'DAD',   'DAM',   'DAN',   'DAR',   'DAY',   'DEE',   'DEL',
			'DEN',   'DES',   'DEW',   'DID',   'DIE',   'DIG',   'DIN',   'DIP',
			'DO',    'DOE',   'DOG',   'DON',   'DOT',   'DOW',   'DRY',   'DUB',
			'DUD',   'DUE',   'DUG',   'DUN',   'EAR',   'EAT',   'ED',    'EEL',
			'EGG',   'EGO',   'ELI',   'ELK',   'ELM',   'ELY',   'EM',    'END',
			'EST',   'ETC',   'EVA',   'EVE',   'EWE',   'EYE',   'FAD',   'FAN',
			'FAR',   'FAT',   'FAY',   'FED',   'FEE',   'FEW',   'FIB',   'FIG',
			'FIN',   'FIR',   'FIT',   'FLO',   'FLY',   'FOE',   'FOG',   'FOR',
			'FRY',   'FUM',   'FUN',   'FUR',   'GAB',   'GAD',   'GAG',   'GAL',
			'GAM',   'GAP',   'GAS',   'GAY',   'GEE',   'GEL',   'GEM',   'GET',
			'GIG',   'GIL',   'GIN',   'GO',    'GOT',   'GUM',   'GUN',   'GUS',
			'GUT',   'GUY',   'GYM',   'GYP',   'HA',    'HAD',   'HAL',   'HAM',
			'HAN',   'HAP',   'HAS',   'HAT',   'HAW',   'HAY',   'HE',    'HEM',
			'HEN',   'HER',   'HEW',   'HEY',   'HI',    'HID',   'HIM',   'HIP',
			'HIS',   'HIT',   'HO',    'HOB',   'HOC',   'HOE',   'HOG',   'HOP',
			'HOT',   'HOW',   'HUB',   'HUE',   'HUG',   'HUH',   'HUM',   'HUT',
			'I',     'ICY',   'IDA',   'IF',    'IKE',   'ILL',   'INK',   'INN',
			'IO',    'ION',   'IQ',    'IRA',   'IRE',   'IRK',   'IS',    'IT',
			'ITS',   'IVY',   'JAB',   'JAG',   'JAM',   'JAN',   'JAR',   'JAW',
			'JAY',   'JET',   'JIG',   'JIM',   'JO',    'JOB',   'JOE',   'JOG',
			'JOT',   'JOY',   'JUG',   'JUT',   'KAY',   'KEG',   'KEN',   'KEY',
			'KID',   'KIM',   'KIN',   'KIT',   'LA',    'LAB',   'LAC',   'LAD',
			'LAG',   'LAM',   'LAP',   'LAW',   'LAY',   'LEA',   'LED',   'LEE',
			'LEG',   'LEN',   'LEO',   'LET',   'LEW',   'LID',   'LIE',   'LIN',
			'LIP',   'LIT',   'LO',    'LOB',   'LOG',   'LOP',   'LOS',   'LOT',
			'LOU',   'LOW',   'LOY',   'LUG',   'LYE',   'MA',    'MAC',   'MAD',
			'MAE',   'MAN',   'MAO',   'MAP',   'MAT',   'MAW',   'MAY',   'ME',
			'MEG',   'MEL',   'MEN',   'MET',   'MEW',   'MID',   'MIN',   'MIT',
			'MOB',   'MOD',   'MOE',   'MOO',   'MOP',   'MOS',   'MOT',   'MOW',
			'MUD',   'MUG',   'MUM',   'MY',    'NAB',   'NAG',   'NAN',   'NAP',
			'NAT',   'NAY',   'NE',    'NED',   'NEE',   'NET',   'NEW',   'NIB',
			'NIL',   'NIP',   'NIT',   'NO',    'NOB',   'NOD',   'NON',   'NOR',
			'NOT',   'NOV',   'NOW',   'NU',    'NUN',   'NUT',   'O',     'OAF',
			'OAK',   'OAR',   'OAT',   'ODD',   'ODE',   'OF',    'OFF',   'OFT',
			'OH',    'OIL',   'OK',    'OLD',   'ON',    'ONE',   'OR',    'ORB',
			'ORE',   'ORR',   'OS',    'OTT',   'OUR',   'OUT',   'OVA',   'OW',
			'OWE',   'OWL',   'OWN',   'OX',    'PA',    'PAD',   'PAL',   'PAM',
			'PAN',   'PAP',   'PAR',   'PAT',   'PAW',   'PAY',   'PEA',   'PEG',
			'PEN',   'PEP',   'PER',   'PET',   'PEW',   'PHI',   'PI',    'PIE',
			'PIN',   'PIT',   'PLY',   'PO',    'POD',   'POE',   'POP',   'POT',
			'POW',   'PRO',   'PRY',   'PUB',   'PUG',   'PUN',   'PUP',   'PUT',
			'QUO',   'RAG',   'RAM',   'RAN',   'RAP',   'RAT',   'RAW',   'RAY',
			'REB',   'RED',   'REP',   'RET',   'RIB',   'RID',   'RIG',   'RIM',
			'RIO',   'RIP',   'ROB',   'ROD',   'ROE',   'RON',   'ROT',   'ROW',
			'ROY',   'RUB',   'RUE',   'RUG',   'RUM',   'RUN',   'RYE',   'SAC',
			'SAD',   'SAG',   'SAL',   'SAM',   'SAN',   'SAP',   'SAT',   'SAW',
			'SAY',   'SEA',   'SEC',   'SEE',   'SEN',   'SET',   'SEW',   'SHE',
			'SHY',   'SIN',   'SIP',   'SIR',   'SIS',   'SIT',   'SKI',   'SKY',
			'SLY',   'SO',    'SOB',   'SOD',   'SON',   'SOP',   'SOW',   'SOY',
			'SPA',   'SPY',   'SUB',   'SUD',   'SUE',   'SUM',   'SUN',   'SUP',
			'TAB',   'TAD',   'TAG',   'TAN',   'TAP',   'TAR',   'TEA',   'TED',
			'TEE',   'TEN',   'THE',   'THY',   'TIC',   'TIE',   'TIM',   'TIN',
			'TIP',   'TO',    'TOE',   'TOG',   'TOM',   'TON',   'TOO',   'TOP',
			'TOW',   'TOY',   'TRY',   'TUB',   'TUG',   'TUM',   'TUN',   'TWO',
			'UN',    'UP',    'US',    'USE',   'VAN',   'VAT',   'VET',   'VIE',
			'WAD',   'WAG',   'WAR',   'WAS',   'WAY',   'WE',    'WEB',   'WED',
			'WEE',   'WET',   'WHO',   'WHY',   'WIN',   'WIT',   'WOK',   'WON',
			'WOO',   'WOW',   'WRY',   'WU',    'YAM',   'YAP',   'YAW',   'YE',
			'YEA',   'YES',   'YET',   'YOU',   'ABED',  'ABEL',  'ABET',  'ABLE',
			'ABUT',  'ACHE',  'ACID',  'ACME',  'ACRE',  'ACTA',  'ACTS',  'ADAM',
			'ADDS',  'ADEN',  'AFAR',  'AFRO',  'AGEE',  'AHEM',  'AHOY',  'AIDA',
			'AIDE',  'AIDS',  'AIRY',  'AJAR',  'AKIN',  'ALAN',  'ALEC',  'ALGA',
			'ALIA',  'ALLY',  'ALMA',  'ALOE',  'ALSO',  'ALTO',  'ALUM',  'ALVA',
			'AMEN',  'AMES',  'AMID',  'AMMO',  'AMOK',  'AMOS',  'AMRA',  'ANDY',
			'ANEW',  'ANNA',  'ANNE',  'ANTE',  'ANTI',  'AQUA',  'ARAB',  'ARCH',
			'AREA',  'ARGO',  'ARID',  'ARMY',  'ARTS',  'ARTY',  'ASIA',  'ASKS',
			'ATOM',  'AUNT',  'AURA',  'AUTO',  'AVER',  'AVID',  'AVIS',  'AVON',
			'AVOW',  'AWAY',  'AWRY',  'BABE',  'BABY',  'BACH',  'BACK',  'BADE',
			'BAIL',  'BAIT',  'BAKE',  'BALD',  'BALE',  'BALI',  'BALK',  'BALL',
			'BALM',  'BAND',  'BANE',  'BANG',  'BANK',  'BARB',  'BARD',  'BARE',
			'BARK',  'BARN',  'BARR',  'BASE',  'BASH',  'BASK',  'BASS',  'BATE',
			'BATH',  'BAWD',  'BAWL',  'BEAD',  'BEAK',  'BEAM',  'BEAN',  'BEAR',
			'BEAT',  'BEAU',  'BECK',  'BEEF',  'BEEN',  'BEER',  'BEET',  'BELA',
			'BELL',  'BELT',  'BEND',  'BENT',  'BERG',  'BERN',  'BERT',  'BESS',
			'BEST',  'BETA',  'BETH',  'BHOY',  'BIAS',  'BIDE',  'BIEN',  'BILE',
			'BILK',  'BILL',  'BIND',  'BING',  'BIRD',  'BITE',  'BITS',  'BLAB',
			'BLAT',  'BLED',  'BLEW',  'BLOB',  'BLOC',  'BLOT',  'BLOW',  'BLUE',
			'BLUM',  'BLUR',  'BOAR',  'BOAT',  'BOCA',  'BOCK',  'BODE',  'BODY',
			'BOGY',  'BOHR',  'BOIL',  'BOLD',  'BOLO',  'BOLT',  'BOMB',  'BONA',
			'BOND',  'BONE',  'BONG',  'BONN',  'BONY',  'BOOK',  'BOOM',  'BOON',
			'BOOT',  'BORE',  'BORG',  'BORN',  'BOSE',  'BOSS',  'BOTH',  'BOUT',
			'BOWL',  'BOYD',  'BRAD',  'BRAE',  'BRAG',  'BRAN',  'BRAY',  'BRED',
			'BREW',  'BRIG',  'BRIM',  'BROW',  'BUCK',  'BUDD',  'BUFF',  'BULB',
			'BULK',  'BULL',  'BUNK',  'BUNT',  'BUOY',  'BURG',  'BURL',  'BURN',
			'BURR',  'BURT',  'BURY',  'BUSH',  'BUSS',  'BUST',  'BUSY',  'BYTE',
			'CADY',  'CAFE',  'CAGE',  'CAIN',  'CAKE',  'CALF',  'CALL',  'CALM',
			'CAME',  'CANE',  'CANT',  'CARD',  'CARE',  'CARL',  'CARR',  'CART',
			'CASE',  'CASH',  'CASK',  'CAST',  'CAVE',  'CEIL',  'CELL',  'CENT',
			'CERN',  'CHAD',  'CHAR',  'CHAT',  'CHAW',  'CHEF',  'CHEN',  'CHEW',
			'CHIC',  'CHIN',  'CHOU',  'CHOW',  'CHUB',  'CHUG',  'CHUM',  'CITE',
			'CITY',  'CLAD',  'CLAM',  'CLAN',  'CLAW',  'CLAY',  'CLOD',  'CLOG',
			'CLOT',  'CLUB',  'CLUE',  'COAL',  'COAT',  'COCA',  'COCK',  'COCO',
			'CODA',  'CODE',  'CODY',  'COED',  'COIL',  'COIN',  'COKE',  'COLA',
			'COLD',  'COLT',  'COMA',  'COMB',  'COME',  'COOK',  'COOL',  'COON',
			'COOT',  'CORD',  'CORE',  'CORK',  'CORN',  'COST',  'COVE',  'COWL',
			'CRAB',  'CRAG',  'CRAM',  'CRAY',  'CREW',  'CRIB',  'CROW',  'CRUD',
			'CUBA',  'CUBE',  'CUFF',  'CULL',  'CULT',  'CUNY',  'CURB',  'CURD',
			'CURE',  'CURL',  'CURT',  'CUTS',  'DADE',  'DALE',  'DAME',  'DANA',
			'DANE',  'DANG',  'DANK',  'DARE',  'DARK',  'DARN',  'DART',  'DASH',
			'DATA',  'DATE',  'DAVE',  'DAVY',  'DAWN',  'DAYS',  'DEAD',  'DEAF',
			'DEAL',  'DEAN',  'DEAR',  'DEBT',  'DECK',  'DEED',  'DEEM',  'DEER',
			'DEFT',  'DEFY',  'DELL',  'DENT',  'DENY',  'DESK',  'DIAL',  'DICE',
			'DIED',  'DIET',  'DIME',  'DINE',  'DING',  'DINT',  'DIRE',  'DIRT',
			'DISC',  'DISH',  'DISK',  'DIVE',  'DOCK',  'DOES',  'DOLE',  'DOLL',
			'DOLT',  'DOME',  'DONE',  'DOOM',  'DOOR',  'DORA',  'DOSE',  'DOTE',
			'DOUG',  'DOUR',  'DOVE',  'DOWN',  'DRAB',  'DRAG',  'DRAM',  'DRAW',
			'DREW',  'DRUB',  'DRUG',  'DRUM',  'DUAL',  'DUCK',  'DUCT',  'DUEL',
			'DUET',  'DUKE',  'DULL',  'DUMB',  'DUNE',  'DUNK',  'DUSK',  'DUST',
			'DUTY',  'EACH',  'EARL',  'EARN',  'EASE',  'EAST',  'EASY',  'EBEN',
			'ECHO',  'EDDY',  'EDEN',  'EDGE',  'EDGY',  'EDIT',  'EDNA',  'EGAN',
			'ELAN',  'ELBA',  'ELLA',  'ELSE',  'EMIL',  'EMIT',  'EMMA',  'ENDS',
			'ERIC',  'EROS',  'EVEN',  'EVER',  'EVIL',  'EYED',  'FACE',  'FACT',
			'FADE',  'FAIL',  'FAIN',  'FAIR',  'FAKE',  'FALL',  'FAME',  'FANG',
			'FARM',  'FAST',  'FATE',  'FAWN',  'FEAR',  'FEAT',  'FEED',  'FEEL',
			'FEET',  'FELL',  'FELT',  'FEND',  'FERN',  'FEST',  'FEUD',  'FIEF',
			'FIGS',  'FILE',  'FILL',  'FILM',  'FIND',  'FINE',  'FINK',  'FIRE',
			'FIRM',  'FISH',  'FISK',  'FIST',  'FITS',  'FIVE',  'FLAG',  'FLAK',
			'FLAM',  'FLAT',  'FLAW',  'FLEA',  'FLED',  'FLEW',  'FLIT',  'FLOC',
			'FLOG',  'FLOW',  'FLUB',  'FLUE',  'FOAL',  'FOAM',  'FOGY',  'FOIL',
			'FOLD',  'FOLK',  'FOND',  'FONT',  'FOOD',  'FOOL',  'FOOT',  'FORD',
			'FORE',  'FORK',  'FORM',  'FORT',  'FOSS',  'FOUL',  'FOUR',  'FOWL',
			'FRAU',  'FRAY',  'FRED',  'FREE',  'FRET',  'FREY',  'FROG',  'FROM',
			'FUEL',  'FULL',  'FUME',  'FUND',  'FUNK',  'FURY',  'FUSE',  'FUSS',
			'GAFF',  'GAGE',  'GAIL',  'GAIN',  'GAIT',  'GALA',  'GALE',  'GALL',
			'GALT',  'GAME',  'GANG',  'GARB',  'GARY',  'GASH',  'GATE',  'GAUL',
			'GAUR',  'GAVE',  'GAWK',  'GEAR',  'GELD',  'GENE',  'GENT',  'GERM',
			'GETS',  'GIBE',  'GIFT',  'GILD',  'GILL',  'GILT',  'GINA',  'GIRD',
			'GIRL',  'GIST',  'GIVE',  'GLAD',  'GLEE',  'GLEN',  'GLIB',  'GLOB',
			'GLOM',  'GLOW',  'GLUE',  'GLUM',  'GLUT',  'GOAD',  'GOAL',  'GOAT',
			'GOER',  'GOES',  'GOLD',  'GOLF',  'GONE',  'GONG',  'GOOD',  'GOOF',
			'GORE',  'GORY',  'GOSH',  'GOUT',  'GOWN',  'GRAB',  'GRAD',  'GRAY',
			'GREG',  'GREW',  'GREY',  'GRID',  'GRIM',  'GRIN',  'GRIT',  'GROW',
			'GRUB',  'GULF',  'GULL',  'GUNK',  'GURU',  'GUSH',  'GUST',  'GWEN',
			'GWYN',  'HAAG',  'HAAS',  'HACK',  'HAIL',  'HAIR',  'HALE',  'HALF',
			'HALL',  'HALO',  'HALT',  'HAND',  'HANG',  'HANK',  'HANS',  'HARD',
			'HARK',  'HARM',  'HART',  'HASH',  'HAST',  'HATE',  'HATH',  'HAUL',
			'HAVE',  'HAWK',  'HAYS',  'HEAD',  'HEAL',  'HEAR',  'HEAT',  'HEBE',
			'HECK',  'HEED',  'HEEL',  'HEFT',  'HELD',  'HELL',  'HELM',  'HERB',
			'HERD',  'HERE',  'HERO',  'HERS',  'HESS',  'HEWN',  'HICK',  'HIDE',
			'HIGH',  'HIKE',  'HILL',  'HILT',  'HIND',  'HINT',  'HIRE',  'HISS',
			'HIVE',  'HOBO',  'HOCK',  'HOFF',  'HOLD',  'HOLE',  'HOLM',  'HOLT',
			'HOME',  'HONE',  'HONK',  'HOOD',  'HOOF',  'HOOK',  'HOOT',  'HORN',
			'HOSE',  'HOST',  'HOUR',  'HOVE',  'HOWE',  'HOWL',  'HOYT',  'HUCK',
			'HUED',  'HUFF',  'HUGE',  'HUGH',  'HUGO',  'HULK',  'HULL',  'HUNK',
			'HUNT',  'HURD',  'HURL',  'HURT',  'HUSH',  'HYDE',  'HYMN',  'IBIS',
			'ICON',  'IDEA',  'IDLE',  'IFFY',  'INCA',  'INCH',  'INTO',  'IONS',
			'IOTA',  'IOWA',  'IRIS',  'IRMA',  'IRON',  'ISLE',  'ITCH',  'ITEM',
			'IVAN',  'JACK',  'JADE',  'JAIL',  'JAKE',  'JANE',  'JAVA',  'JEAN',
			'JEFF',  'JERK',  'JESS',  'JEST',  'JIBE',  'JILL',  'JILT',  'JIVE',
			'JOAN',  'JOBS',  'JOCK',  'JOEL',  'JOEY',  'JOHN',  'JOIN',  'JOKE',
			'JOLT',  'JOVE',  'JUDD',  'JUDE',  'JUDO',  'JUDY',  'JUJU',  'JUKE',
			'JULY',  'JUNE',  'JUNK',  'JUNO',  'JURY',  'JUST',  'JUTE',  'KAHN',
			'KALE',  'KANE',  'KANT',  'KARL',  'KATE',  'KEEL',  'KEEN',  'KENO',
			'KENT',  'KERN',  'KERR',  'KEYS',  'KICK',  'KILL',  'KIND',  'KING',
			'KIRK',  'KISS',  'KITE',  'KLAN',  'KNEE',  'KNEW',  'KNIT',  'KNOB',
			'KNOT',  'KNOW',  'KOCH',  'KONG',  'KUDO',  'KURD',  'KURT',  'KYLE',
			'LACE',  'LACK',  'LACY',  'LADY',  'LAID',  'LAIN',  'LAIR',  'LAKE',
			'LAMB',  'LAME',  'LAND',  'LANE',  'LANG',  'LARD',  'LARK',  'LASS',
			'LAST',  'LATE',  'LAUD',  'LAVA',  'LAWN',  'LAWS',  'LAYS',  'LEAD',
			'LEAF',  'LEAK',  'LEAN',  'LEAR',  'LEEK',  'LEER',  'LEFT',  'LEND',
			'LENS',  'LENT',  'LEON',  'LESK',  'LESS',  'LEST',  'LETS',  'LIAR',
			'LICE',  'LICK',  'LIED',  'LIEN',  'LIES',  'LIEU',  'LIFE',  'LIFT',
			'LIKE',  'LILA',  'LILT',  'LILY',  'LIMA',  'LIMB',  'LIME',  'LIND',
			'LINE',  'LINK',  'LINT',  'LION',  'LISA',  'LIST',  'LIVE',  'LOAD',
			'LOAF',  'LOAM',  'LOAN',  'LOCK',  'LOFT',  'LOGE',  'LOIS',  'LOLA',
			'LONE',  'LONG',  'LOOK',  'LOON',  'LOOT',  'LORD',  'LORE',  'LOSE',
			'LOSS',  'LOST',  'LOUD',  'LOVE',  'LOWE',  'LUCK',  'LUCY',  'LUGE',
			'LUKE',  'LULU',  'LUND',  'LUNG',  'LURA',  'LURE',  'LURK',  'LUSH',
			'LUST',  'LYLE',  'LYNN',  'LYON',  'LYRA',  'MACE',  'MADE',  'MAGI',
			'MAID',  'MAIL',  'MAIN',  'MAKE',  'MALE',  'MALI',  'MALL',  'MALT',
			'MANA',  'MANN',  'MANY',  'MARC',  'MARE',  'MARK',  'MARS',  'MART',
			'MARY',  'MASH',  'MASK',  'MASS',  'MAST',  'MATE',  'MATH',  'MAUL',
			'MAYO',  'MEAD',  'MEAL',  'MEAN',  'MEAT',  'MEEK',  'MEET',  'MELD',
			'MELT',  'MEMO',  'MEND',  'MENU',  'MERT',  'MESH',  'MESS',  'MICE',
			'MIKE',  'MILD',  'MILE',  'MILK',  'MILL',  'MILT',  'MIMI',  'MIND',
			'MINE',  'MINI',  'MINK',  'MINT',  'MIRE',  'MISS',  'MIST',  'MITE',
			'MITT',  'MOAN',  'MOAT',  'MOCK',  'MODE',  'MOLD',  'MOLE',  'MOLL',
			'MOLT',  'MONA',  'MONK',  'MONT',  'MOOD',  'MOON',  'MOOR',  'MOOT',
			'MORE',  'MORN',  'MORT',  'MOSS',  'MOST',  'MOTH',  'MOVE',  'MUCH',
			'MUCK',  'MUDD',  'MUFF',  'MULE',  'MULL',  'MURK',  'MUSH',  'MUST',
			'MUTE',  'MUTT',  'MYRA',  'MYTH',  'NAGY',  'NAIL',  'NAIR',  'NAME',
			'NARY',  'NASH',  'NAVE',  'NAVY',  'NEAL',  'NEAR',  'NEAT',  'NECK',
			'NEED',  'NEIL',  'NELL',  'NEON',  'NERO',  'NESS',  'NEST',  'NEWS',
			'NEWT',  'NIBS',  'NICE',  'NICK',  'NILE',  'NINA',  'NINE',  'NOAH',
			'NODE',  'NOEL',  'NOLL',  'NONE',  'NOOK',  'NOON',  'NORM',  'NOSE',
			'NOTE',  'NOUN',  'NOVA',  'NUDE',  'NULL',  'NUMB',  'OATH',  'OBEY',
			'OBOE',  'ODIN',  'OHIO',  'OILY',  'OINT',  'OKAY',  'OLAF',  'OLDY',
			'OLGA',  'OLIN',  'OMAN',  'OMEN',  'OMIT',  'ONCE',  'ONES',  'ONLY',
			'ONTO',  'ONUS',  'ORAL',  'ORGY',  'OSLO',  'OTIS',  'OTTO',  'OUCH',
			'OUST',  'OUTS',  'OVAL',  'OVEN',  'OVER',  'OWLY',  'OWNS',  'QUAD',
			'QUIT',  'QUOD',  'RACE',  'RACK',  'RACY',  'RAFT',  'RAGE',  'RAID',
			'RAIL',  'RAIN',  'RAKE',  'RANK',  'RANT',  'RARE',  'RASH',  'RATE',
			'RAVE',  'RAYS',  'READ',  'REAL',  'REAM',  'REAR',  'RECK',  'REED',
			'REEF',  'REEK',  'REEL',  'REID',  'REIN',  'RENA',  'REND',  'RENT',
			'REST',  'RICE',  'RICH',  'RICK',  'RIDE',  'RIFT',  'RILL',  'RIME',
			'RING',  'RINK',  'RISE',  'RISK',  'RITE',  'ROAD',  'ROAM',  'ROAR',
			'ROBE',  'ROCK',  'RODE',  'ROIL',  'ROLL',  'ROME',  'ROOD',  'ROOF',
			'ROOK',  'ROOM',  'ROOT',  'ROSA',  'ROSE',  'ROSS',  'ROSY',  'ROTH',
			'ROUT',  'ROVE',  'ROWE',  'ROWS',  'RUBE',  'RUBY',  'RUDE',  'RUDY',
			'RUIN',  'RULE',  'RUNG',  'RUNS',  'RUNT',  'RUSE',  'RUSH',  'RUSK',
			'RUSS',  'RUST',  'RUTH',  'SACK',  'SAFE',  'SAGE',  'SAID',  'SAIL',
			'SALE',  'SALK',  'SALT',  'SAME',  'SAND',  'SANE',  'SANG',  'SANK',
			'SARA',  'SAUL',  'SAVE',  'SAYS',  'SCAN',  'SCAR',  'SCAT',  'SCOT',
			'SEAL',  'SEAM',  'SEAR',  'SEAT',  'SEED',  'SEEK',  'SEEM',  'SEEN',
			'SEES',  'SELF',  'SELL',  'SEND',  'SENT',  'SETS',  'SEWN',  'SHAG',
			'SHAM',  'SHAW',  'SHAY',  'SHED',  'SHIM',  'SHIN',  'SHOD',  'SHOE',
			'SHOT',  'SHOW',  'SHUN',  'SHUT',  'SICK',  'SIDE',  'SIFT',  'SIGH',
			'SIGN',  'SILK',  'SILL',  'SILO',  'SILT',  'SINE',  'SING',  'SINK',
			'SIRE',  'SITE',  'SITS',  'SITU',  'SKAT',  'SKEW',  'SKID',  'SKIM',
			'SKIN',  'SKIT',  'SLAB',  'SLAM',  'SLAT',  'SLAY',  'SLED',  'SLEW',
			'SLID',  'SLIM',  'SLIT',  'SLOB',  'SLOG',  'SLOT',  'SLOW',  'SLUG',
			'SLUM',  'SLUR',  'SMOG',  'SMUG',  'SNAG',  'SNOB',  'SNOW',  'SNUB',
			'SNUG',  'SOAK',  'SOAR',  'SOCK',  'SODA',  'SOFA',  'SOFT',  'SOIL',
			'SOLD',  'SOME',  'SONG',  'SOON',  'SOOT',  'SORE',  'SORT',  'SOUL',
			'SOUR',  'SOWN',  'STAB',  'STAG',  'STAN',  'STAR',  'STAY',  'STEM',
			'STEW',  'STIR',  'STOW',  'STUB',  'STUN',  'SUCH',  'SUDS',  'SUIT',
			'SULK',  'SUMS',  'SUNG',  'SUNK',  'SURE',  'SURF',  'SWAB',  'SWAG',
			'SWAM',  'SWAN',  'SWAT',  'SWAY',  'SWIM',  'SWUM',  'TACK',  'TACT',
			'TAIL',  'TAKE',  'TALE',  'TALK',  'TALL',  'TANK',  'TASK',  'TATE',
			'TAUT',  'TEAL',  'TEAM',  'TEAR',  'TECH',  'TEEM',  'TEEN',  'TEET',
			'TELL',  'TEND',  'TENT',  'TERM',  'TERN',  'TESS',  'TEST',  'THAN',
			'THAT',  'THEE',  'THEM',  'THEN',  'THEY',  'THIN',  'THIS',  'THUD',
			'THUG',  'TICK',  'TIDE',  'TIDY',  'TIED',  'TIER',  'TILE',  'TILL',
			'TILT',  'TIME',  'TINA',  'TINE',  'TINT',  'TINY',  'TIRE',  'TOAD',
			'TOGO',  'TOIL',  'TOLD',  'TOLL',  'TONE',  'TONG',  'TONY',  'TOOK',
			'TOOL',  'TOOT',  'TORE',  'TORN',  'TOTE',  'TOUR',  'TOUT',  'TOWN',
			'TRAG',  'TRAM',  'TRAY',  'TREE',  'TREK',  'TRIG',  'TRIM',  'TRIO',
			'TROD',  'TROT',  'TROY',  'TRUE',  'TUBA',  'TUBE',  'TUCK',  'TUFT',
			'TUNA',  'TUNE',  'TUNG',  'TURF',  'TURN',  'TUSK',  'TWIG',  'TWIN',
			'TWIT',  'ULAN',  'UNIT',  'URGE',  'USED',  'USER',  'USES',  'UTAH',
			'VAIL',  'VAIN',  'VALE',  'VARY',  'VASE',  'VAST',  'VEAL',  'VEDA',
			'VEIL',  'VEIN',  'VEND',  'VENT',  'VERB',  'VERY',  'VETO',  'VICE',
			'VIEW',  'VINE',  'VISE',  'VOID',  'VOLT',  'VOTE',  'WACK',  'WADE',
			'WAGE',  'WAIL',  'WAIT',  'WAKE',  'WALE',  'WALK',  'WALL',  'WALT',
			'WAND',  'WANE',  'WANG',  'WANT',  'WARD',  'WARM',  'WARN',  'WART',
			'WASH',  'WAST',  'WATS',  'WATT',  'WAVE',  'WAVY',  'WAYS',  'WEAK',
			'WEAL',  'WEAN',  'WEAR',  'WEED',  'WEEK',  'WEIR',  'WELD',  'WELL',
			'WELT',  'WENT',  'WERE',  'WERT',  'WEST',  'WHAM',  'WHAT',  'WHEE',
			'WHEN',  'WHET',  'WHOA',  'WHOM',  'WICK',  'WIFE',  'WILD',  'WILL',
			'WIND',  'WINE',  'WING',  'WINK',  'WINO',  'WIRE',  'WISE',  'WISH',
			'WITH',  'WOLF',  'WONT',  'WOOD',  'WOOL',  'WORD',  'WORE',  'WORK',
			'WORM',  'WORN',  'WOVE',  'WRIT',  'WYNN',  'YALE',  'YANG',  'YANK',
			'YARD',  'YARN',  'YAWL',  'YAWN',  'YEAH',  'YEAR',  'YELL',  'YOGA',
			'YOKE'
		);
	}
	
	/**
	 * Get One-Time-Password Challenge, given the components of it
	 * 
	 * @access public
	 * @param string $hashAlgorithm
	 * @param integer $sequenceNumber
	 * @param string $seed
	 * @return string
	 */
	public static function getOtpChallenge($hashAlgorithm, $sequenceNumber, $seed) {
		return 'otp-' . $hashAlgorithm . ' ' . $sequenceNumber . ' ' . $seed;
	}
}