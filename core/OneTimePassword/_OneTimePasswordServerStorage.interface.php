<?php
/**
 * MI_OneTimePasswordServerStorage
 *
 * @package Core
 */
interface MI_OneTimePasswordServerStorage {
	public function setSeed($seed);
	public function initialize($hashFunction, $sequenceNumber);
	public function setOneTimePassword($oneTimePassword);
	public function setIdentity($identity);

	/* @return M_OneTimePasswordServer */
	public function getServer();

	public function getSeed();
	public function getSequenceNumber();
	public function getHashFunction();
	public function getOneTimePassword();
	public function getIdentity();
}