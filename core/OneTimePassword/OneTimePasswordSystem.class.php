<?php
/**
 * M_OneTimePassword
 *
 * @package Core
 */
abstract class M_OneTimePasswordSystem extends M_Object {

	/* -- PROPERTIES -- */

	/**
	 * Seed
	 *
	 * @see M_OneTimePasswordSystem::getSeed()
	 * @see M_OneTimePasswordSystem::_setSeed()
	 * @access private
	 * @var string
	 */
	private $_seed;

	/**
	 * Algorithm
	 *
	 * @see M_OneTimePasswordSystem::getHashFunction()
	 * @see M_OneTimePasswordSystem::_setHashFunction()
	 * @access private
	 * @var string
	 */
	private $_hashFunction;

	/**
	 * Sequence number
	 *
	 * @see M_OneTimePasswordSystem::getSequenceNumber()
	 * @see M_OneTimePasswordSystem::_setSequenceNumber()
	 * @access private
	 * @var integer
	 */
	private $_sequenceNumber;

	/**
	 * Alternative dictionary of words
	 *
	 * This property stores an alternative dictionary of words, and is populated
	 * by {@link M_OneTimePasswordSystem::setAlternativeDictionary()}.
	 *
	 * @see M_OneTimePasswordSystem::setAlternativeDictionary()
	 * @see M_OneTimePasswordHelper::isValidAlternativeDictionary()
	 * @access private
	 * @var array
	 */
	private $_dictionary;
	
	/* -- SETTERS -- */

	/**
	 * Set alternative dictionary of words
	 *
	 * In addition to accepting six-word and hexadecimal encodings of the 64
	 * bit one-time password, servers SHOULD accept an alternate dictionary
	 * encoding from the generators. The six words in this encoding MUST not
	 * overlap the set of words in the standard dictionary.
	 *
	 * The purpose of alternative dictionary encoding of the OTP one-time
	 * password is to allow the use of language specific or friendly words. As
	 * case translation is not always well defined, the alternative dictionary
	 * encoding is case sensitive.  Servers SHOULD accept this encoding in
	 * addition to the standard 6-word and hexadecimal encodings.
	 *
	 * NOTE:
	 * This method will comply with the RFC 2289 specifications, and make sure
	 * that the provided dictionary adheres to the requirements. If the dictionary
	 * does not, an exception will be thrown.
	 *
	 * NOTE:
	 * Of course, both the One-Time Password Generator and the One-Time Password
	 * Server should be using exactly the same dictionary!
	 *
	 * @see M_OneTimePasswordHelper::isValidAlternativeDictionary()
	 * @access public
	 * @param array $dictionary
	 * @return M_OneTimePassword $otp
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAlternativeDictionary(array $dictionary) {
		// Before setting the alternative dictionary, we make sure that the
		// dictionary complies with the specifications of RFC 2289
		if(! M_OneTimePasswordHelper::isValidAlternativeDictionary($dictionary)) {
			// If it does not, we throw an exception
			throw new M_OneTimePasswordException(sprintf(
				'The provided alternative dictionary cannot be used. Please check ' .
				'the RFC 2289 specifications, for more information.'
			));
		}

		// Set the dictionary:
		$this->_dictionary = $dictionary;

		// Returns itself:
		return $this;
	}

	/* -- GETTERS -- */
	
	/**
	 * Get Seed
	 *
	 * The seed is provided by the One-Time Password Server, as part of the
	 * challenge. This method will return the seed that has been provided to
	 * this object.
	 *
	 * @see M_OneTimePasswordGenerator::setChallenge()
	 * @see M_OneTimePasswordServer::setSeed()
	 * @access public
	 * @return string $seed
	 */
	public function getSeed() {
		return $this->_seed;
	}

	/**
	 * Get Algorithm, or hash function
	 *
	 * The algorithm is provided by the One-Time Password Server, as part of the
	 * challenge. This method will return the algorithm that has been provided
	 * to this object.
	 *
	 * @see M_OneTimePasswordGenerator::setChallenge()
	 * @see M_OneTimePasswordServer::setHashFunction()
	 * @access public
	 * @return string $algorithm
	 */
	public function getHashFunction() {
		return $this->_hashFunction;
	}

	/**
	 * Get Sequence number
	 *
	 * The sequence number is provided by the One-Time Password Server, as part
	 * of the challenge. This method will return the sequence that has been
	 * provided to this object.
	 *
	 * @see M_OneTimePasswordGenerator::setChallenge()
	 * @see M_OneTimePasswordServer::setSequenceNumber()
	 * @access public
	 * @return integer $sequenceNumber
	 */
	public function getSequenceNumber() {
		return $this->_sequenceNumber;
	}

	/**
	 * Get dictionary of words
	 *
	 * Will return the dictionary of words that is being used by this generator
	 * object, or server object, to represent the one-time password in six short
	 * 1 to 4 letter dictionary words.
	 *
	 * NOTE:
	 * If no alternative dictionary has been provided to the object, this method
	 * will return the default dictionary instead, as defined by the RFC 2289
	 * specifications.
	 *
	 * @see M_OneTimePasswordSystem::setAlternativeDictionary()
	 * @see M_OneTimePasswordHelper::getDefaultDictionary()
	 * @access public
	 * @return array $dictionary
	 *		The dictionary that is being used by this object
	 */
	public function getDictionary() {
		// If an alternative dictionary has been provided to the generator:
		if($this->_dictionary) {
			// Then, we return that dictionary
			return $this->_dictionary;
		}

		// If not, we return the default one:
		return M_OneTimePasswordHelper::getDefaultDictionary();
	}

	/* -- PROTECTED/PRIVATE -- */

	/**
	 * Set hash function
	 *
	 * @throws M_OneTimePasswordException
	 * @access protected
	 * @param string $hashFunction
	 *		The hash function that is to be used by the server to send the
	 *		challenge to the one-time password generator, or by the one-time
	 *		password generator to calculate a new password.
	 * @return M_OneTimePassword $otp
	 *		Returns itself, for a fluent programming interface
	 */
	protected function _setHashFunction($hashFunction) {
		// Before setting the provided hash function, we make sure that this
		// function is being supported by our implementation of the OTP system:
		if(! M_OneTimePasswordHelper::isValidHashFunction($hashFunction)) {
			// If the function is not supported, then we throw an exception to
			// inform about the error:
			throw new M_OneTimePasswordException(sprintf(
				'The One-Time Password system does not support hash function "%s"'.
				'Supported hash functions are: %s',
				$hashFunction,
				implode(', ', M_OneTimePasswordHelper::getSupportedHashFunctions())
			));
		}

		// Set the hash function:
		$this->_hashFunction = $hashFunction;

		// Returns itself:
		return $this;
	}

	/**
	 * Set seed
	 *
	 * @throws M_OneTimePasswordException
	 * @access protected
	 * @param string $seed
	 *		The seed that is to be used by the server to send the challenge to
	 *		the one-time password generator, or by the one-time password generator
	 *		to calculate a new password.
	 * @return M_OneTimePassword $otp
	 *		Returns itself, for a fluent programming interface
	 */
	protected function _setSeed($seed) {
		// We make sure that the provided seed adheres to the RFC 2289 specs:
		if(! M_OneTimePasswordHelper::isValidSeed($seed)) {
			// We throw an exception, to inform about the error, if the provided
			// seed does not comply:
			throw new M_OneTimePasswordException(sprintf(
				'The One-Time Password object cannot accept the seed "%s". ' .
				'This seed does not comply with the RFC 2289 specifications!',
				$seed
			));
		}

		// Set the seed:
		$this->_seed = $seed;

		// Returns itself:
		return $this;
	}

	/**
	 * Set sequence number
	 *
	 * @throws M_OneTimePasswordException
	 * @access protected
	 * @param integer $sequenceNumber
	 *		The sequence number that is to be used by the server to send the
	 *		challenge to the one-time password generator, or by the one-time
	 *		password generator to calculate a new password.
	 * @return M_OneTimePasswordServer $server
	 *		Returns itself, for a fluent programming interface
	 */
	protected function _setSequenceNumber($sequenceNumber) {
		// We make sure that the provided sequence number adheres to the RFC
		// 2289 specs:
		if(! M_OneTimePasswordHelper::isValidSequenceNumber($sequenceNumber)) {
			// We throw an exception, to inform about the error, if the provided
			// sequence number does not comply:
			throw new M_OneTimePasswordException(sprintf(
				'The One-Time Password object cannot accept the sequence ' .
				'number "%s". This sequence does not comply with the RFC 2289 ' .
				'specifications!',
				$sequenceNumber
			));
		}

		// Set the sequence number:
		$this->_sequenceNumber = $sequenceNumber;

		// Returns itself:
		return $this;
	}

	/**
	 *
	 */
	protected function _isOneTimePasswordInDictionaryWords($otp) {
		// In order to check whether or not the provided One-Time Password is
		// in the format of Dictionary Words, we will match the provided password
		// against a pattern.

		// First, we compose the piece of the pattern that matches the words in
		// the password. As you can see, we are looking for 1 to 4 letter words,
		// as specified in the RFC2289 specifications:
		$word = '[a-zA-Z]{1,4}';

		// Then, we also define the pattern that matches the whitespaces between
		// each word. Note that we allow more than one whitespace to be present
		// between the words...
		$whitespace = '\s+';

		// Now, we compose the complete pattern, recycling the two subpatterns
		// we have created:
		$pattern =
			// Word 1
			$word . $whitespace .
			// Word 2
			$word . $whitespace .
			// Word 3
			$word . $whitespace .
			// Word 4
			$word . $whitespace .
			// Word 5
			$word . $whitespace .
			// Word 6
			$word;

		// Now, we test the provided password against our pattern. Note that we
		// add optional whitespace at the beginning and at the end of the
		// pasword...
		return (preg_match('/^\s*'. $pattern .'\s*$/', $otp) == 1);
	}

	/**
	 *
	 */
	protected function _isOneTimePasswordInBits($otp) {
		// If the password is provided in bits, then we expect a string with a
		// representation of exactly 64 bits. Note that we do NOT allow whitespaces
		// whatsoever!
		return (preg_match('/^[01]{64}$/', $otp) == 1);
	}

	/**
	 *
	 */
	protected function _isOneTimePasswordInDecimals() {
		// In order to check whether or not the provided One-Time Password is
		// in the format of decimal numbers, we will match the provided password
		// against a predetermined pattern.

		// First, we compose the piece of the pattern that matches the numbers in
		// the password. As you can see, we are looking for 1 to 4 digit numbers:
		$number = '[0-9]{1,4}';

		// Then, we also define the pattern that matches the whitespaces between
		// each number. Note that we allow more than one whitespace to be present
		// between the numbers...
		$whitespace = '\s+';

		// Now, we compose the complete pattern, recycling the two subpatterns
		// we have created:
		$pattern =
			// Number 1
			$number . $whitespace .
			// Number 2
			$number . $whitespace .
			// Number 3
			$number . $whitespace .
			// Number 4
			$number . $whitespace .
			// Number 5
			$number . $whitespace .
			// Number 6
			$number;

		// Now, we test the provided password against our pattern. Note that we
		// add optional whitespace at the beginning and at the end of the
		// pasword...
		return (preg_match('/^\s*'. $pattern .'\s*$/', $otp) == 1);
	}

	/**
	 *
	 */
	protected function _isOneTimePasswordHexadecimal($otp) {
		// In order to check whether or not the provided One-Time Password is
		// in hexadecimal format, we will match the provided password against a
		// predetermined pattern. Before we do so, we remove the redundant white-
		// spaces from the provided one-time password
		$otp = preg_replace('/\s+/', '', $otp);

		// If the password is provided in hexadecimal format, then we expect a
		// string with exactly 16 characters:
		return (preg_match('/^[0-9a-fA-F]{16}$/', $otp) == 1);
	}

	/**
	 *
	 */
	protected function _getBitStringFromDictionaryWords($phrase) {
		// We prepare the string, containing the representation in bits:
		$bits = '';

		// We get the dictionary:
		$dictionary = $this->getDictionary();

		// We split up the provided string of words into an array of words:
		$words = preg_split('/[\s]+/m', M_Helper::trimCharlist($phrase));

		// The array of words should contain at least 6 elements!
		if(count($words) != 6) {
			// If that's not the case, then we return FALSE
			return FALSE;
		}

		// For each of the words:
		foreach($words as $word) {
			// We check if the word is present in the dictionary of words and,
			// while we're at it, get the key at which the word is stored in the
			// dictionary.
			$number = array_search($word, $dictionary);

			// If the word could not be found in the dictionary, then the provided
			// phrase is not a valid one-time password.
			if($number === FALSE) {
				// In that case, we return FALSE
				return FALSE;
			}

			// The key at which the current word is stored, is the decimal equivalent
			// of a 11-bit chunk of the one-time password. So, we convert that
			// decimal number to a string that contains the binary representation.
			// Please note that we make sure to add 11-character long bit strings
			// each time:
			$bits .= str_pad(decbin($number), 11, '0', STR_PAD_LEFT);
		}

		// At this point, we should have accumulated a string containing a
		// representation of 66 bits. The last 2 bits in this string store the
		// two-bit checksum. So, in order to check the validity of the current
		// bit string, we compare that checksum to the one we would calculate
		// for the first 64 bits:
		$bits64 = substr($bits, 0, 64);
		$bitsCS = substr($bits, 0, 2);
		if($this->_getBitStringChecksum($bits64) != $bitsCS) {
			// If they don't match, then the bitstring is not valid:
			return FALSE;
		}

		// If we're still here, then everything went perfect! :) We return the
		// chunk of 64 bits, representing the one-time password
		return $bits64;
	}

	protected function _getBitStringFromDecimals($decimals) {
		// We prepare the string, containing the representation in bits:
		$bits = '';

		// We split up the provided string of decimals into an array of numbers:
		$numbers = preg_split('/[\s]+/m', M_Helper::trimCharlist($decimals));

		// The array of numbers should contain at least 6 elements!
		if(count($numbers) != 6) {
			// If that's not the case, then we return FALSE
			return FALSE;
		}

		// For each of the numbers:
		foreach($numbers as $number) {
			// The number is used as a key to look up words in the dictionary.
			// Since the dictionary contains 2048 words, we know that the lowest
			// value for the current number is 0, and the highest possible value
			// is 2047:
			$c = (is_numeric($number) && $number >= 0 && $number < 2048);

			// If that is not the case:
			if(! $c) {
				// Then, we return FALSE
				return FALSE;
			}

			// The current number is the decimal equivalent of an 11-bit chunk
			// of the one-time password. So, we convert that decimal number to a
			// string that contains the binary representation. Please note that
			// we make sure to add 11-character long bit strings each time:
			$bits .= str_pad(decbin($number), 11, '0', STR_PAD_LEFT);
		}

		// At this point, we should have accumulated a string containing a
		// representation of 66 bits. The last 2 bits in this string store the
		// two-bit checksum. So, in order to check the validity of the current
		// bit string, we compare that checksum to the one we would calculate
		// for the first 64 bits:
		$bits64 = substr($bits, 0, 64);
		$bitsCS = substr($bits, 0, 2);
		if($this->_getBitStringChecksum($bits64) != $bitsCS) {
			// If they don't match, then the bitstring is not valid:
			return FALSE;
		}

		// If we're still here, then everything went perfect! :) We return the
		// chunk of 64 bits, representing the one-time password
		return $bits64;
	}

	/**
	 *
	 */
	protected function _getBitStringFromOneTimePassword($oneTimePassword) {
		// RFC 2289 says:
		// ... "As there is a remote possibility that a hexadecimal encoding of
		// a one-time password will look like a valid six-word standard dictionary
		// encoding, all implementations MUST use the following scheme. If a
		// six-word encoded one-time password is valid, it is accepted. Otherwise,
		// if the one-time password can be interpreted as hexadecimal, and with
		// that decoding it is valid, then it is accepted." ...

		// If the password looks like a six-word dictionary encoding
		if($this->_isOneTimePasswordInDictionaryWords($oneTimePassword)) {
			// Then, we try to decode the six-word dictionary encoding into a
			// string with a representation of 64 bits:
			$bitString = $this->_getBitStringFromDictionaryWords($oneTimePassword);

			// If we have successfully parsed the dictionary words into a 64-bit
			// representation
			if($bitString) {
				// Then we return the string with the 64-bit representation.
				return $bitString;
			}
		}

		// If we fail in doing so, then we will try to interpret the one-time
		// password as a hexadecimal value, in compliance with the specifications
		// of RFC 2289. We check if the password looks like a hexadecimal
		// value:
		if($this->_isOneTimePasswordHexadecimal($oneTimePassword)) {
			// If it does look like hexadecimal, then we try to decode the
			// hexadecimal encoding into a string with a representation of
			// 64 bits:
			$bitString = $this->_getBitStringFromHexadecimal($oneTimePassword);

			// If we have successfully parsed the hexadecimal value into a 64-bit
			// representation
			if($bitString) {
				// Then we return the string with the 64-bit representation.
				return $bitString;
			}
		}

		// If we are still here, then the password is not encoded in the the
		// six-word dictionary format, and is not being represented in hexadecimal
		// format. So, now we check if the password is being provided as a set of
		// decimal numbers:
		if($this->_isOneTimePasswordInDecimals($oneTimePassword)) {
			// If it does look like decimals, then we try to decode the decimal
			// encoding into a string with a representation of 64 bits:
			$bitString = $this->_getBitStringFromDecimals($oneTimePassword);

			// If we have successfully parsed the decimals into a 64-bit
			// representation
			if($bitString) {
				// Then we return the string with the 64-bit representation.
				return $bitString;
			}
		}

		// If we are still here, then the only possible format remaining is the
		// string with a representation of 64 bits. We check if the provided
		// password matches that format:
		if($this->_isOneTimePasswordInBits($oneTimePassword)) {
			// If it does look like bits, then we return the string with the
			// 64-bit representation.
			return $oneTimePassword;
		}

		// If we are still here, then we could not interpret the one-time password
		// in any of the possible formats. So, we return FALSE for failure:
		return FALSE;
	}

	/**
	 * Get representation in bits
	 *
	 * Will render a representation in bits, of the one-time password. The result
	 * of this method is a string that represents the bits. An example of an
	 * outputted string is
	 *
	 * 1010001101110011001101001010001101110011001101001010001101110011
	 *
	 * As you can see, this method will be producing a string of 64 bits. This
	 * bit representation can be used to convert the one-time password to a
	 * collection of words from the dictionary, and to calculate the checksum
	 * of bits before converting to words.
	 *
	 * @access protected
	 * @param string $hex
	 *		The hexadecimal representation of the one-time password that is to
	 *		be converted to a representation of bits.
	 * @return string $bits
	 */
	protected function _getBitStringFromHexadecimal($hex) {
		// We prepare the string, containing the representation in bits:
		$bits = '';

		// We clean up the hexadecimal value:
		$hex = $this->_getHexadecimalClean($hex);

		// If we could not retrieve a clean hexadecimal value:
		if(! $hex) {
			// Then, we return FALSE for failure
			return FALSE;
		}

		// For each of the characters in the hexadecimal string:
		for($i = 0, $n = strlen($hex); $i < $n; $i ++) {
			// We convert the hexadecimal character to a string of bits. And,
			// we make sure that it the string of bits is 4 characters long:
			$bits .= str_pad(base_convert($hex{$i}, 16, 2), 4, '0', STR_PAD_LEFT);
		}

		// Return the bits:
		return $bits;
	}

	/**
	 *
	 */
	protected function _getBitStringFromHash($hash) {
		// Convert binary hash into hexadecimal representation, and convert that
		// to the requested bit string:
		return $this->_getBitStringFromHexadecimal(bin2hex($hash));
	}
	
	/**
	 * Get checksum of bits
	 *
	 * The representation in bits of the one-time password is one of 64 bits. The
	 * bits can be used to look up words from the dictionary, in order to create
	 * a word representation of the password. Each word is chosen from a dictionary
	 * of 2048 words; at 11 bits per word, all one-time passwords may be encoded.
	 *
	 * The two extra bits in this encoding are used to store a checksum. The 64
	 * bits of key are broken down into pairs of bits, then these pairs are summed
	 * together. The two least significant bits of this sum are encoded in the
	 * last two bits of the six word sequence with the least significant bit of
	 * the sum as the last bit encoded. All OTP generators MUST calculate this
	 * checksum and all OTP servers MUST verify this checksum explicitly as part
	 * of the operation of decoding this representation of the one-time password.
	 *
	 * @access protected
	 * @param string $bitString
	 *		The representation in bits of the one-time password is one of 64 bits,
	 *		see {@link M_OneTimePasswordSystem::_getBits()}
	 * @return integer $checksum
	 */
	protected function _getBitStringChecksum($bitString) {
		// Checksum
		$checksum = 0;

		// For each pair of bits:
		for($i = 0; $i < 64; $i += 2) {
			// Sum the bits to the checksum
			$checksum += $bitString{$i + 1} + (2 * $bitString{$i});
		}

		// Create a string of 2 checksum bits
		return (fmod(floor($checksum / 2), 2) . '' . fmod($checksum, 2));
	}

	/**
	 *
	 */
	protected function _getHexadecimalFromBitString($bitString) {
		// The output
		$hex = '';

		// We run through the bit string in chunks of 4 bits. Dividing 64 by 4,
		// we know that we will need 16 loops to do so:
		for($i = 0; $i < 16; $i ++) {
			// Each chunk of 4 bits can be converted to its hexadecimal equivalent:
			$hex .= dechex(bindec(substr($bitString, ($i * 4), 4)));
		}

		// Return the hexadecimal string:
		return $hex;
	}

	/**
	 *
	 */
	protected function _getHexadecimalClean($hex) {
		// We remove all whitespace from the provided hexadecimal string:
		$hex = preg_replace('/[\s]+/m', '', $hex);

		// At this point, the hexadecimal string should be 16 characters long.
		// If the hexadecimal string is 18 characters long:
		if(strlen($hex) == 18) {
			// Then, maybe the hex has been provided with the 0x prefix. Eg,
			// the hex e64b3c10c2ff6216 could also be written as 0xe64b3c10c2ff6216
			if(strtolower(substr($hex, 0, 2)) == '0x') {
				// Then, remove the prefix
				$hex = substr($hex, 2);
			}
		}

		// Now, the hex string should really be 16 characters long.
		if(strlen($hex) != 16) {
			// If that is not the case, we return FALSE instead:
			return FALSE;
		}

		// If still here, the hex is OK:
		return $hex;
	}

	/**
	 * Get Hash
	 *
	 * Will produce a hash from the provided data, with the algorithm that has
	 * been specified to this object.
	 *
	 * @uses M_OneTimePasswordSystem::_getSha1()
	 * @uses M_OneTimePasswordSystem::_getMd5()
	 * @access protected
	 * @param string $data
	 *		The data to be hashed
	 * @return mixed $hash
	 *		The hash/digest, in raw binary format
	 */
	protected function _getHash($data) {
		// Depending on the algorithm that has been requested from this object:
		switch($this->getHashFunction()) {
			// SHA1:
			case 'sha1':
				return $this->_getSha1($data);

			// MD5:
			case 'md5':
				return $this->_getMd5($data);

			// Others (unsupported)
			default:
				// In theory, this function should never get to this point. When
				// an unsupported hash function is requested from the object, an
				// exception will already be thrown. However, we include this
				// default case, for objects that have not yet specified the
				// hash function to be used:
				throw new M_OneTimePasswordException(sprintf(
					'%s cannot calculate a hash. The hash function "%s" is ' .
					'either not supported, or undefined.',
					$this->getClassName(),
					$this->getHashFunction()
				));
		}
	}

	/**
	 * Get Hash, from one-time password
	 *
	 * Will convert a given one-time password back to the original, binary,
	 * hash format. Then, it will apply the hash function to the result and return
	 * the resulting binary hash.
	 *
	 * @access protected
	 * @param string $oneTimePassword
	 *		The one-time password, in a six-word dictionary encoding, in hexadecimal
	 *		format, in decimals, or as a string representation of 64 bits.
	 * @return mixed
	 */
	protected function _getHashFromOneTimePassword($oneTimePassword) {
		// First, we convert the one-time password to a string representation of
		// 64 bits:
		$bitString = $this->_getBitStringFromOneTimePassword($oneTimePassword);

		// If we failed to produce the bit string:
		if(! $bitString) {
			// Then we throw an exception, to inform about the error:
			throw new M_OneTimePasswordException(sprintf(
				'The One-Time Password System object cannot parse the password ' .
				'"%s"; corrupted format?',
				$oneTimePassword
			));
		}

		// Now that we have the bit string, we convert it to its equivalent in
		// hexadecimal format. This format can be easily converted back to the
		// original binary format. This binary format is the original binary data
		// of the password, and should be used as an argument to the hash function:
		return $this->_getHash(pack("H*" , $this->_getHexadecimalFromBitString($bitString)));
	}

	/**
	 * Get SHA1 Hash
	 *
	 * @access protected
	 * @param string $data
	 *		The data to be hashed
	 * @return mixed $sha1
	 *		The SHA1 digest, in raw binary format
	 */
	protected function _getSha1($data) {
		// We get the binary value of the hash
		$binary = pack('H*', sha1($data));

		// RFC 2289: The result of the secure hash is reduced to 64 bits using
		// the function dependent algorithm:
		$temp  = substr($binary, 0, 4) ^ substr($binary, 8, 4);
		$temp  = $temp ^ substr($binary, 16, 4);
		$temp .= substr($binary, 4, 4) ^ substr($binary, 12, 4);
		$hash = strrev(substr($temp, 0, 4)).strrev(substr($temp, 4, 4));

		// Return the hash
		return $hash;
	}

	/**
	 * Get MD5 Hash
	 *
	 * @access protected
	 * @param string $data
	 *		The data to be hashed
	 * @return mixed $md5
	 *		The MD5 digest, in raw binary format
	 */
	protected function _getMd5($data) {
		// We get the binary value of the hash
		$binary = pack('H*', md5($data));

		// RFC 2289: The result of the secure hash is reduced to 64 bits using
		// the function dependent algorithm:
		$hash = substr($binary, 0, 8) ^ substr($binary, 8, 8);

		// Return the hash
		return $hash;
	}
}