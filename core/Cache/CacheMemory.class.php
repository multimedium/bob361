<?php
/**
 * M_CacheMemory
 * 
 * The M_CacheMemory class helps to improve the overall performance
 * of heavy-duty page requests, by saving any kind of data to cache 
 * memory. Typically, the M_CacheMemory class is used to reduce the 
 * number of SQL queries in a page request, by saving the results to 
 * cache memory. Once saved to cache memory, time consuming queries 
 * can be skipped completely, by reading directly from the previously 
 * cache'd results.
 * 
 * The M_CacheMemory is an abstract class, providing with basic 
 * functionality, shared by all cache memory adapters. Concrete 
 * subclasses of M_CacheMemory add the read() and write() method. This
 * way, other classes can interface with a M_CacheMemory object, whether
 * the data is being stored into a PHP file, database table, or any
 * other data source for that matter.
 *
 * @package Core
 */
abstract class M_CacheMemory {
	/**
	 * Constant to express unlimited lifetime
	 *
	 * This constant can be used to express unlimited lifetime. This
	 * is used to store data into cache memory that never expires.
	 */
	const LIFETIME_UNLIMITED = 0;
	
	/**
	 * The lifetime of the data in cache
	 * 
	 * The data saved in cache memory has a given lifetime. It will
	 * live in cache memory for as long as specified by this property.
	 * Note that the lifetime is expressed in a number of seconds.
	 *
	 * @access private
	 * @var integer
	 */
	protected $_lifetime = self::LIFETIME_UNLIMITED;
	
	/**
	 * The location of cache memory
	 * 
	 * This property holds the location where cache'd data lives. If
	 * the data is stored into cache files, this property would hold
	 * the directory where cache files are stored. If the cache is
	 * stored in the database, this property would hold the database
	 * table object.
	 * 
	 * @access private
	 * @var mixed
	 */
	protected $_location;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_CacheMemory
	 */
	public function __construct($location = NULL, $lifetime = NULL) {
		if($location) {
			$this->_location = $location;
		}
		if($lifetime) {
			$this->_lifetime = $lifetime;
		}
	}
	
	/**
	 * Set the lifetime
	 * 
	 * Set the lifetime of the cache'd data. This method is a setter 
	 * for the {@link M_CacheMemory::$lifetime} property.
	 *
	 * @access public
	 * @param integer $lifetime
	 * 		The lifetime of the cache memory. Note that this lifetime
	 * 		is expressed in a number of seconds.
	 * @return void
	 */
	public function setLifetime($lifetime) {
		$this->_lifetime = $lifetime;
	}
	
	/**
	 * Set the location of cache memory
	 * 
	 * This method will set the location where cache'd data lives. If
	 * the data is stored into cache files, this method would accept
	 * the directory where cache files are located. If the cache is
	 * stored in the database, this method would accept the database
	 * table object.
	 * 
	 * @access public
	 * @param string $location
	 * 		The location of the cache memory
	 * @return void
	 */
	public function setLocation($location) {
		$this->_location = $location;
	}
	
	/**
	 * Read from cache
	 * 
	 * This method will try to read data from cache memory. Note that
	 * this method will return NULL, if no data has been found in cache
	 * memory.
	 * 
	 * @access public
	 * @abstract
	 * @param string $dataSetId
	 * 		The ID of the data in cache memory. Each set of data is 
	 * 		marked with an ID when written to cache. The same ID is
	 * 		used to retrieve the data from cache.
	 * @return mixed
	 */
	abstract protected function read($dataSetId);
	
	/**
	 * Write data to cache memory
	 * 
	 * This method will save data to cache memory. Note that a unique
	 * ID (string) is being assigned to the data set, so it can be
	 * retrieved with the {@link M_CacheMemory::read()} method later on.
	 *
	 * @access public
	 * @abstract
	 * @param string $dataSetId
	 * 		The ID of the data in cache memory.
	 * @param mixed $data
	 * 		The data to be saved to cache memory
	 * @return boolean
	 * 		Will return TRUE on success, FALSE on error
	 */
	abstract protected function write($dataSetId, $data);
	
	/**
	 * Remove data from cache memory
	 * 
	 * This method will remove data from cache memory. Note that a 
	 * unique ID (string) is being assigned to the data set, to identify
	 * the data set that has been cache'd before.
	 *
	 * NOTE:
	 * If you ommit the ID, all cache will be removed!
	 *
	 * @access public
	 * @abstract
	 * @param string $dataSetId
	 * 		The ID of the data in cache memory.
	 * @return boolean
	 * 		Will return TRUE on success, FALSE on error
	 */
	abstract protected function remove($dataSetId = NULL);
}
?>