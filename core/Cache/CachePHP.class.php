<?php
/**
 * M_CachePHP class
 * 
 * This concrete subclass of M_CacheMemory handles the cache memory
 * that is saved into PHP files (*.cache.php).
 *
 * @package Core
 */
class M_CachePHP extends M_CacheMemory {
	
	const CACHE_VAR_EXPORT = 'var_export';
	const CACHE_SERIALIZE = 'serialize';
	
	/**
	 * @var string
	 */
	private $_cacheMode = self::CACHE_VAR_EXPORT;
	
	/**
	 * Set the cache mode
	 * 
	 * Choose in which mode you want to cache data: serialize, or var_export
	 * (use class constants to set this mode)
	 * 
	 * @param string $arg
	 * @return M_CachePHP
	 */
	public function setCacheMode($arg) {
		$this->_cacheMode = $arg;
		return $this;
	}
	
	/**
	 * Get the cache mode
	 * 
	 * @return string
	 */
	public function getCacheMode() {
		return $this->_cacheMode;
	}
	
	/**
	 * Read from cache
	 * 
	 * Overwrites the {@link M_CacheMemory::read()} method.
	 *
	 * @access public
	 * @abstract
	 * @param string $dataSetId
	 * 		The ID of the data in cache memory. Each set of data is 
	 * 		marked with an ID when written to cache. The same ID is
	 * 		used to retrieve the data from cache.
	 * @return mixed
	 */
	public function read($dataSetId) {
		$filename = $this->_getFilename($dataSetId);
		if(!is_file($filename)) {
			return NULL;
		}
		
		$data = NULL;
		require($filename);
		
		//if the cache mode is "serialize", we need to remove the slashes
		//from the $data string
		if($this->getCacheMode() == self::CACHE_SERIALIZE) {
			$data = unserialize(stripslashes($data));
		}
		
		return $data;
	}
	
	/**
	 * Write data to cache memory
	 * 
	 * Overrides the {@link M_CacheMemory::write()} method.
	 *
	 * @access public
	 * @abstract
	 * @param string $dataSetId
	 * 		The ID of the data in cache memory.
	 * @param mixed $data
	 * 		The data to be saved to cache memory
	 * @return boolean
	 * 		Will return TRUE on success, FALSE on error
	 */
	public function write($dataSetId, $data) {
		$source  = '<?php ';
		$source .= '$c1 = ($this->_lifetime == M_CacheMemory::LIFETIME_UNLIMITED); ';
		$source .= '$c2 = (('. time() .' + $this->_lifetime) > time()); ';
		$source .= 'if($c1 || $c2) { ';
		
		//generate the data to be cached, depending on the cache mode
		if ($this->getCacheMode() == self::CACHE_VAR_EXPORT) {
			$cacheData = var_export($data, true);
		}else {
			$cacheData = "'".addslashes(serialize($data))."'";
		}
		//remove $data variable from memory
		unset($data);
		
		$source .=    '$data = ' . $cacheData . '; ';
		$source .= '} else { ';
		$source .=    '$data = NULL; ';
		$source .= '} ';
		$source .= '?>';
		
		$fp = @fopen($this->_getFilename($dataSetId), 'w');
		if($fp !== FALSE) {
			if(fwrite($fp, $source) !== FALSE) {
				fclose($fp);
				return TRUE;
			}
		}else {
			throw new M_Exception(sprintf(
				'Could not write cache for %s to %s',
				$dataSetId,
				$this->_getFilename($dataSetId))
			);
		}
		
		return FALSE;
	}
	
	/**
	 * Remove data from cache memory
	 * 
	 * Overrides the {@link M_CacheMemory::remove()} method.
	 *
	 * @access public
	 * @abstract
	 * @param string $dataSetId
	 * 		The ID of the data in cache memory.
	 * @return boolean
	 * 		Will return TRUE on success, FALSE on error
	 */
	public function remove($dataSetId = NULL) {
		if($dataSetId) {
			if(@unlink($this->_getFilename($dataSetId))) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			$dir = new M_Directory($this->_location);
			foreach($dir->getItems() as $item) {
				/* @var $item MI_FsItemFile */
				if($item->isFile()) {
					$item->delete();
				}
			}
		}
	}
	
	/**
	 * Get the time at which this this cache was created
	 * 
	 * @param string $dataSetId
	 * @return boolean
	 */
	public function getCacheCreationDate($dataSetId = NULL) {
		$file = new M_File(
			$this->_getFilename($dataSetId)
		);
		
		if($file->exists()) {
			return $file->getCreationDate();
		}
		
		return false;
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Get PHP filename (dataSetId.cache.php)
	 *
	 * @access private
	 * @param string $name
	 * 		The name; or the ID of the data in cache memory
	 * @return string
	 * 		The filename
	 */
	private function _getFilename($name) {
		return rtrim($this->_location, " \t\n\r\0\x0B/\\") . '/' . str_replace(
			array('*', '"', '/', '\\', '[', ']', ':', ';', '|', '=', ','),
			'-',
			$name
		) . '.cache.php';
	}
}