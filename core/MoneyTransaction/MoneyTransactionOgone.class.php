<?php
/**
 * M_MoneyTransactionOgone class
 * 
 * M_MoneyTransactionOgone is a concrete implementation of the abstract
 * {@link M_MoneyTransaction}, and can be used to handle online payment 
 * with OGONE.
 * 
 * Status codes:
 * 
 * 
 * @package Core
 */
class M_MoneyTransactionOgone extends M_MoneyTransaction {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_IMCOMPLETE_INVALID = 0;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_CANCELLED_BY_CLIENT = 1;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_AUTHORIZATION_REFUSED = 2;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_ORDER_STORED = 4;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_WAITING_CLIENT_PAYMENT = 41;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_AUTHORIZED = 5;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_AUTHORIZATION_WAITING = 51;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_AUTHORIZATION_NOT_KNOWN = 52;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_STANDBY = 55;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_AUTHORIZATION_TO_GET_MANUALLY = 59;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_AUTHORIZED_AND_CANCELLED = 6;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_AUTHOR_DELETION_WAITING = 61;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_AUTHOR_DELETION_UNCERTAIN = 62;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_AUTHOR_DELETION_REFUSED = 63;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_PAYMENT_DELETED = 7;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_PAYMENT_DELETION_PENDING = 71;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_PAYMENT_DELETION_UNCERTAIN = 72;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_PAYMENT_DELETION_REFUSED = 73;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_DELETION_PROCESSED_BY_MERCHANT = 75;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_REFUND = 8;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_REFUND_PENDING = 81;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_REFUND_UNCERTAIN = 82;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_REFUND_REFUSED = 83;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_PAYMENT_DECLINED_BY_ACQUIRER = 84;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_REFUND_PROCESSED_BY_MERCHANT = 85;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_PAYMENT_REQUESTED = 9;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_PAYMENT_PROCESSING = 91;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_PAYMENT_UNCERTAIN = 92;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_PAYMENT_REFUSED = 93;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_REFUND_DECLINED_BY_ACQUIRER = 94;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_PAYMENT_PROCESSED_BY_MERCHANT = 95;
	
	/**
	 * Status code
	 * 
	 * This constant is used to address a specific status of the transaction. For
	 * a complete overview of possible statusses, please consult Ogone.
	 * 
	 * @see M_MoneyTransaction::getStatus()
	 * @var integer
	 */
	const STATUS_BEING_PROCESSED = 99;
	
	/* -- PROPERTIES -- */

	/**
	 * Template page
	 *
	 * @see M_MoneyTransactionOgone::setTemplatePage()
	 * @access protected
	 * @var array
	 */
	protected $_template;

	/**
	 * Presentation variables
	 * 
	 * This property stores some presentation variables, such as the
	 * background color, text color, etc...
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_presentation = array();
	
	/* -- SETTERS -- */
	
	/**
	 * Add transaction definition
	 * 
	 * This method extends {@link M_MoneyTransaction::set()}, in order
	 * to add definitions to the {@link M_MoneyTransaction::factory()}
	 * method. This method adds the following definition keys:
	 * 
	 * <code>bgColor</code>
	 * 
	 * Read {@link M_MoneyTransactionOgone::setBackgroundColor()} for
	 * more information about this definition.
	 * 
	 * <code>tableBgColor</code>
	 * 
	 * Read {@link M_MoneyTransactionOgone::setTableBackgroundColor()} 
	 * for more information about this definition.
	 * 
	 * <code>textColor</code>
	 * 
	 * Read {@link M_MoneyTransactionOgone::setTextColor()} for more 
	 * information about this definition.
	 * 
	 * <code>fontFamily</code>
	 * 
	 * Read {@link M_MoneyTransactionOgone::setFontFamily()} for
	 * more information about this definition.
	 * 
	 * <code>buttonBgColor</code>
	 * 
	 * Read {@link M_MoneyTransactionOgone::setButtonBackgroundColor()} 
	 * for more information about this definition.
	 * 
	 * <code>buttonTextColor</code>
	 * 
	 * Read {@link M_MoneyTransactionOgone::setButtonTextColor()} 
	 * for more information about this definition.
	 * 
	 * @access public
	 * @throws M_MoneyTransactionException
	 * @param string $spec
	 * 		The transaction definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @return void 
	 */
	public function set($spec, $definition) {
		switch(strtolower($spec)) {
			case 'bgcolor':
				$this->setBackgroundColor($definition);
				break;
			
			case 'tablebgcolor':
				$this->setTableBackgroundColor($definition);
				break;
			
			case 'textcolor':
				$this->setTextColor($definition);
				break;
			
			case 'font':
			case 'fontfamily':
				$this->setFontFamily($definition);
				break;
			
			case 'buttonbgcolor':
				$this->setButtonBackgroundColor($definition);
				break;

			case 'buttontextcolor':
				$this->setButtonTextColor($definition);
				break;

			case 'template':
				$this->setTemplatePage(new M_Uri((string) $definition));
				break;
			
			default:
				parent::set($spec, $definition);
				break;
		}
	}

	/**
	 * Set template page
	 *
	 * Set the URL of the merchant's dynamic template page:
	 *
	 * - the page must be hosted at the merchant‟s end,
	 * - The URL must be absolute (contain the full path),
	 * - Do not specify any ports in your URL, Ogone only accepts ports 443 and 80.
	 *   Any component included in the template page must also have an absolute URL.
	 *
	 * @access public
	 * @param M_Uri $uri
	 * @return void
	 */
	public function setTemplatePage(M_Uri $uri) {
		$this->_template = $uri;
	}
	
	/**
	 * Set background color
	 * 
	 * This method can be used to set the background color of the
	 * page(s) that are rendered by Ogone; for example the payment
	 * form's page.
	 * 
	 * NOTE:
	 * This method is part of {@link M_MoneyTransactionOgone}, because 
	 * not all providers support this.
	 * 
	 * NOTE:
	 * This method will accept either:
	 * 
	 * - an {@link M_Color} object, 
	 * - a 6 character long hex string, or
	 * - an array with R, G and B channel
	 * 
	 * @access public
	 * @param M_Color|string|array $color
	 * @return void
	 */
	public function setBackgroundColor($color) {
		if($color instanceof M_Color) {
			$this->_presentation['bg'] = $color->getHex(TRUE);
		} else {
			$color = new M_Color($color);
			$this->_presentation['bg'] = $color->getHex(TRUE);
		}
	}
	
	/**
	 * Set table background color
	 * 
	 * This method can be used to set the background color of the
	 * table(s) that are rendered by Ogone.
	 * 
	 * NOTE:
	 * This method is part of {@link M_MoneyTransactionOgone}, because 
	 * not all providers support this.
	 * 
	 * NOTE:
	 * This method will accept either:
	 * 
	 * - an {@link M_Color} object, 
	 * - a 6 character long hex string, or
	 * - an array with R, G and B channel
	 * 
	 * @access public
	 * @param M_Color|string|array $color
	 * @return void
	 */
	public function setTableBackgroundColor($color) {
		if($color instanceof M_Color) {
			$this->_presentation['tblbg'] = $color->getHex(TRUE);
		} else {
			$color = new M_Color($color);
			$this->_presentation['tblbg'] = $color->getHex(TRUE);
		}
	}
	
	/**
	 * Set text color
	 * 
	 * This method can be used to set the color of the text in the
	 * page(s) that are rendered by Ogone; for example the payment
	 * form's page.
	 * 
	 * NOTE:
	 * This method is part of {@link M_MoneyTransactionOgone}, because 
	 * not all providers support this.
	 * 
	 * NOTE:
	 * This method will accept either:
	 * 
	 * - an {@link M_Color} object, 
	 * - a 6 character long hex string, or
	 * - an array with R, G and B channel
	 * 
	 * @access public
	 * @return void
	 */
	public function setTextColor($color) {
		if($color instanceof M_Color) {
			$this->_presentation['fg'] = $color->getHex(TRUE);
		} else {
			$color = new M_Color($color);
			$this->_presentation['fg'] = $color->getHex(TRUE);
		}
	}
	
	/**
	 * Set font
	 * 
	 * This method can be used to set the font (style) of the text in 
	 * the page(s) that are rendered by Ogone; for example the payment
	 * form's page.
	 * 
	 * NOTE:
	 * This method is part of {@link M_MoneyTransactionOgone}, because 
	 * not all providers support this.
	 * 
	 * @access public
	 * @param string $font
	 * 		The font family to be used, eg. "Arial"
	 * @return void
	 */
	public function setFontFamily($font) {
		$this->_presentation['font'] = (string) $font;
	}
	
	/**
	 * Set button background color
	 * 
	 * This method can be used to set the background color of buttons 
	 * that are presented on the page(s) that are rendered by Ogone; 
	 * for example the payment form's page.
	 * 
	 * NOTE:
	 * This method is part of {@link M_MoneyTransactionOgone}, because 
	 * not all providers support this.
	 * 
	 * NOTE:
	 * This method will accept either:
	 * 
	 * - an {@link M_Color} object, 
	 * - a 6 character long hex string, or
	 * - an array with R, G and B channel
	 * 
	 * @access public
	 * @return void
	 */
	public function setButtonBackgroundColor($color) {
		if($color instanceof M_Color) {
			$this->_presentation['bttnbg'] = $color->getHex(TRUE);
		} else {
			$color = new M_Color($color);
			$this->_presentation['bttnbg'] = $color->getHex(TRUE);
		}
	}
	
	/**
	 * Set button text color
	 * 
	 * This method can be used to set the text color on buttons 
	 * of Ogone page(s); for example the payment form's page.
	 * 
	 * NOTE:
	 * This method is part of {@link M_MoneyTransactionOgone}, because 
	 * not all providers support this.
	 * 
	 * NOTE:
	 * This method will accept either:
	 * 
	 * - an {@link M_Color} object, 
	 * - a 6 character long hex string, or
	 * - an array with R, G and B channel
	 * 
	 * @access public
	 * @return void
	 */
	public function setButtonTextColor($color) {
		if($color instanceof M_Color) {
			$this->_presentation['bttnfg'] = $color->getHex(TRUE);
		} else {
			$color = new M_Color($color);
			$this->_presentation['bttnfg'] = $color->getHex(TRUE);
		}
	}
	
	/* -- Support for M_MoneyTransaction -- */
	
	/**
	 * Is Payment Authorized?
	 * 
	 * Will use {@link M_MoneyTransaction::getStatus()} to check whether or not
	 * the payment has been authorized.
	 * 
	 * NOTE:
	 * All subclasses of {@link M_MoneyTransaction} should implement
	 * this method. The response (payment feedback) varies from provider 
	 * to provider, and each requires a specific implementation.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the payment has been authorized, FALSE if not
	 */
	public function isPaymentAuthorized() {
		return ((int) $this->getStatus() == self::STATUS_AUTHORIZED);
	}
	
	/**
	 * Is Payment Confirmed?
	 * 
	 * Will use {@link M_MoneyTransaction::getStatus()} to check whether or not
	 * the payment has been confirmed.
	 * 
	 * NOTE:
	 * All subclasses of {@link M_MoneyTransaction} should implement
	 * this method. The response (payment feedback) varies from provider 
	 * to provider, and each requires a specific implementation.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the payment has been confirmed, FALSE if not
	 */
	public function isPaymentConfirmed() {
		return ((int) $this->getStatus() == self::STATUS_PAYMENT_REQUESTED);		
	}
	
	/**
	 * Is Payment Ok?
	 * 
	 * Will use {@link M_MoneyTransaction::isPaymentAuthorized()} and 
	 * {@link M_MoneyTransaction::isPaymentConfirmed()} to check whether or not
	 * the payment has been confirmed and/or authorized.
	 * 
	 * NOTE:
	 * All subclasses of {@link M_MoneyTransaction} should implement
	 * this method. The response (payment feedback) varies from provider 
	 * to provider, and each requires a specific implementation.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the payment is OK, FALSE if not
	 */
	public function isPaymentOk() {
		
		if($this->isPaymentAuthorized())  {
			return TRUE;
		}
		
		if($this->isPaymentConfirmed()) {
			return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Get form
	 * 
	 * This method provides with an implementation for the abstract
	 * {@link M_MoneyTransaction::getForm()}.
	 * 
	 * NOTE:
	 * All subclasses of {@link M_MoneyTransaction} should implement
	 * this method. The collection of hidden fields that need to be
	 * included in a payment form vary from provider to provider.
	 * 
	 * @access public
	 * @return M_Form
	 */
	public function getForm() {
		// Form variables:
		$vars = $this->_getFormVariables();
		$vars['SHASign'] = $this->_getSha1Signature();
		
		// Mount the basic form:
		$form = M_Form::factory('trans-ogone', new M_Config(array(
			'type' => 'callback',
			'variables' => $vars,
			'properties' => array(
				'action' => 'https://secure.ogone.com/ncol/'. ($this->isTestingAccount() ? 'test' : 'prod') .'/orderstandard.asp'
			)
		)));
		
		// return the final form:
		return $form;
	}
	
	/**
	 * Import response
	 * 
	 * This method provides with an implementation for the abstract
	 * {@link M_MoneyTransaction::importResponse()}.
	 * 
	 * NOTE:
	 * All subclasses of {@link M_MoneyTransaction} should implement
	 * this method. The response (payment feedback) varies from provider 
	 * to provider, and each requires a specific implementation.
	 * 
	 * NOTE:
	 * This specific implementation uses the Private Key Out to check
	 * the response that has been received from Ogone. For more info,
	 * read {@link M_MoneyTransaction::setPrivateKeyOut()}.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function importResponse($requestMethod, array $requestVariables) {
		// In the feedback from ogone, we expect the following request
		// variables:
		if(strtoupper($requestMethod) == 'GET' || strtoupper($requestMethod) == 'POST') {
			// We convert all parameter names to uppercase, to avoid confusions
			// (Ogone communicates about uppercase parameters, but for some values
			// they use lowercased or camel-cased names)
			$requestVariables = array_change_key_case($requestVariables, CASE_UPPER);

			// In the feedback from ogone, we expect the following request
			// variables to be present:
			$orderId    = isset($requestVariables['ORDERID'])    ? $requestVariables['ORDERID']    : FALSE;
			$currency   = isset($requestVariables['CURRENCY'])   ? $requestVariables['CURRENCY']   : FALSE;
			$amount     = isset($requestVariables['AMOUNT'])     ? $requestVariables['AMOUNT']     : FALSE;
			$pm         = isset($requestVariables['PM'])         ? $requestVariables['PM']         : FALSE;
			//$acceptance = isset($requestVariables['ACCEPTANCE']) ? $requestVariables['ACCEPTANCE'] : FALSE;
			$status     = isset($requestVariables['STATUS'])     ? $requestVariables['STATUS']     : FALSE;
			//$cardno     = isset($requestVariables['CARDNO'])     ? $requestVariables['CARDNO']     : FALSE;
			$payid      = isset($requestVariables['PAYID'])      ? $requestVariables['PAYID']      : FALSE;
			$ncerror    = isset($requestVariables['NCERROR'])    ? $requestVariables['NCERROR']    : FALSE;
			$brand      = isset($requestVariables['BRAND'])      ? $requestVariables['BRAND']      : FALSE;
			$shasign    = isset($requestVariables['SHASIGN'])    ? $requestVariables['SHASIGN']    : FALSE;
			
			// We make sure we have received all of the above parameters:
			if($orderId !== FALSE && $currency !== FALSE && $amount !== FALSE && $pm !== FALSE && 
			// $acceptance !== FALSE && 
			$status !== FALSE && 
			// $cardno !== FALSE && 
			$payid !== FALSE && 
			$ncerror !== FALSE && $brand !== FALSE && $shasign !== FALSE) {
				// If we got in here, all parameters have been provided! :)
				// Ogone has provided us with a sha1 signature, that helps
				// us to verify that the parameters have not been tampered 
				// with. It provides with a sha1 hash of a string that 
				// contains all of the above variables:
				$parameters = array(
					'AMOUNT'     => $amount,
					'BRAND'      => $brand,
					// 'CARDNO'     => $cardno,
					'CURRENCY'   => $currency,
					'NCERROR'    => $ncerror,
					'ORDERID'    => $orderId,
					'PAYID'      => $payid,
					'PM'         => $pm,
					'STATUS'     => $status
				);

				// Optional parameters:
				$optional = array(
					'ACCEPTANCE',
					'CARDNO',
					'AAVADDRESS',             'AAVCHECK',
					'AAVZIP',                 'ALIAS',
					'CCCTY',                  'CN',
					'COMPLUS',                'CVCCHECK',
					'DCC_COMMPERCENTAGE',     'DCC_CONVAMOUNT',
					'DCC_CONVCCY',            'DCC_EXCHRATE',
					'DCC_EXCHRATESOURCE',     'DCC_EXCHRATETS',
					'DCC_INDICATOR',          'DCC_MARGINPERCENTAGE',
					'DCC_VALIDHOUS',          'DIGESTCARDNO',
					'ECI',                    'ED',
					'ENCCARDNO',              'IP',
					'IPCTY',                  'NBREMAILUSAGE',
					'NBRIPUSAGE',             'NBRIPUSAGE_ALLTX',
					'NBRUSAGE',               'SCO_CATEGORY',
					'SCORING',                'TRXDATE',
					'VC'
				);

				// For each of the optional parameters:
				foreach($optional as $name) {
					// If the parameter is provided
					if(isset($requestVariables[$name]) && ! empty($requestVariables[$name])) {
						// Then, we add it to the mix of present variables:
						$parameters[$name] = $requestVariables[$name];
					}
				}

				// In order to correctly compose the SHA signature, we need to order
				// the parameters alphabetically, by the name of the parameter:
				ksort($parameters);

				// Start composing the signature
				$signature  = '';
				
				// For each of the parameters:
				foreach($parameters as $name => $value) {
					// We include the parameter in the parameter=value format,
					// and append the Private OUT Key:
					$signature .= $name . '=' . $value . $this->getPrivateKeyOut();
				}
				
				// For some reason, ogone provides us with an uppercased
				// hash. We compare our hash (uppercased) against the hash
				// that has been provided by ogone.
				if(strtoupper(sha1($signature)) == $shasign) {
					// If we have a match, we set the internals of the
					// transaction object:
					$this->setOrderNumber($orderId);
					$this->setCurrency($currency);
					$this->setAmount($amount);
					$this->setPaymentMethod($pm);
					$this->setPaymentMethodBrand($brand);
					
					// The card number
					if(isset($requestVariables['CARDNO'])) {
						$this->setPaymentCardNumber($requestVariables['CARDNO']);
					}
					
					// The card holder name
					if(isset($requestVariables['CN'])) {
						$this->setPaymentCardHolderName($requestVariables['CN']);
					}
					
					// The card's expiration month and year
					if(isset($requestVariables['ED'])) {
						$this->setPaymentCardExpirationMonthAndYear($requestVariables['ED']);
					}
					
					// Set the status of the transaction:
					$this->_setStatus($requestVariables['STATUS']);
					
					// And we return TRUE (success)
					return TRUE;
				}
			}
		}
		
		// We return FALSE (failure), if we're still here
		return FALSE;
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Get SHA1 Signature
	 * 
	 * This method will return the SHA1 signature that should be used 
	 * to start the online payment. More specifically, this method will 
	 * concatenate a collection of values that are related to the 
	 * payment and the merchant's account, and encrypt it with the SHA1 
	 * algorithm. To do so, it makes use of the Private Key In. Read
	 * {@link M_MoneyTransaction::setPrivateKeyIn()} for more info 
	 * about the Private Key In. 
	 * 
	 * Ogone will do the same thing at their server. Their hash must
	 * match the result of this method, in order for the payment to be
	 * accepted.
	 * 
	 * This method collects the values of the following (ogone-specific)
	 * fields, in order to create the SHA1-encrypted hash:
	 * 
	 * - Order ID, see {@link M_MoneyTransaction::setOrderNumber()}
	 * - Amount, see {@link M_MoneyTransaction::setAmount()}
	 * - Currency, see {@link M_MoneyTransaction::setCurrency()}
	 * - PSPID, see {@link M_MoneyTransaction::setMerchantCode()}
	 * - SHA1 Signature, see {@link M_MoneyTransaction::setPrivateKeyIn()}
	 * 
	 * @access private
	 * @return string
	 */
	private function _getSha1Signature() {
		// Output
		$out = '';

		// Each parameter must be included in the SHA string, in the parameter=value
		// format. Each of the parameters is then separated by the SHA-IN string.
		// So, for each of the parameters:
		foreach($this->_getFormVariables() as $name => $value) {
			// Note that Ogone expects the parameter name to be uppercased
			$out .= strtoupper($name) . '=' . $value . $this->getPrivateKeyIn();
		}

		// Return the values, separated with SHA-IN, and encrypted
		return strtoupper(sha1($out));
	}

	/**
	 * Get form variables
	 *
	 * This method provides with the form variables that are needed in the payment
	 * form: {@link M_MoneyTransactionOgone::getForm()}. Also, the result of
	 * this method is used to compose the SHA-IN Signature:
	 * {@link M_MoneyTransactionOgone::_getSha1Signature()}
	 *
	 * @access public
	 * @return array
	 */
	private function _getFormVariables() {
		// Basic variables:
		$vars = array(
			'orderID'  => $this->getOrderNumber(),
			'amount'   => (int) (string) (round($this->getAmount(), 2) * 100),
			'currency' => $this->getCurrency(),
			'PSPID'    => $this->getMerchantCode(),
			'language' => $this->getLocaleName(),
		);
		
		// Add the payment method / brand
		if($this->getPaymentMethod()) {
			$vars['PM']	= $this->getPaymentMethod();
		}
		if($this->getPaymentMethodBrand()) {
			$vars['BRAND'] = $this->getPaymentMethodBrand();
		}

		// If a page title has been provided for the payment form, we
		// add that property to the form:
		$tmp = $this->getPageTitle();
		if($tmp) {
			$vars['TITLE'] = $tmp;
		}

		// If redirect URL for successful outcome has been set, we
		// add that property to the form:
		$tmp = $this->getRedirectUrl(M_MoneyTransaction::SUCCESS);
		if($tmp) {
			$vars['ACCEPTURL'] = $tmp;
		}

		// If redirect URL for failed outcome has been set, we
		// add that property to the form:
		$tmp = $this->getRedirectUrl(M_MoneyTransaction::FAILURE);
		if($tmp) {
			$vars['declineurl'] = $tmp;
		}

		// If redirect URL for cancelled payment has been set] = we
		// add that property to the form:
		$tmp = $this->getRedirectUrl(M_MoneyTransaction::CANCEL);
		if($tmp) {
			$vars['cancelurl'] = $tmp;
		}

		// Presentation variables:
		// The background color of the page (if any)
		if(isset($this->_presentation['bg'])) {
			$vars['BGCOLOR'] = $this->_presentation['bg'];
		}

		// The background color of tables in the page
		if(isset($this->_presentation['tblbg'])) {
			$vars['TBLBGCOLOR'] = $this->_presentation['tblbg'];
		}

		// The color of texts
		if(isset($this->_presentation['fg'])) {
			$vars['TXTCOLOR'] = $this->_presentation['fg'];
			$vars['TBLTXTCOLOR'] = $this->_presentation['fg'];
		}

		// The background color of buttons in the payment page
		if(isset($this->_presentation['bttnbg'])) {
			$vars['BUTTONBGCOLOR'] = $this->_presentation['bttnbg'];
		}

		// The color of texts in buttons] = in the payment page:
		if(isset($this->_presentation['bttnfg'])) {
			$vars['BUTTONTXTCOLOR'] = $this->_presentation['bttnfg'];
		}

		// The font that should be used by the payment page:
		if(isset($this->_presentation['font'])) {
			$vars['FONTTYPE'] = $this->_presentation['font'];
		}

		// The template page:
		// (the template page is a dynamic page that renders the HTML for the payment
		// page. Images and other resources are hosted at Ogone though...)
		if($this->_template) {
			// Set the URL to the template page:
			$vars['TP'] = $this->_template->toString();
		}

		// In order to correctly compose the SHA signature, we need to order the
		// parameters alphabetically, by the name of the parameter:
		// (case-insensitive sort)
		uksort($vars, "strnatcasecmp");
		
		// return the final form:
		return $vars;
	}
}
?>