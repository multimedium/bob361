<?php
/**
 * M_ApplicationModule class
 * 
 * M_ApplicationModule can be used to get information about a module
 * of the installed application. Also, it can be used to install/uninstall
 * a given module.
 * 
 * @todo: RELEASE NOTES IN info.xml
 * 
 * @package Core
 */
class M_ApplicationModule {
	/**
	 * Module ID
	 * 
	 * This property stores the ID of the module. The ID of a module
	 * equals the folder name in which it has been installed in the
	 * application's module folder.
	 *
	 * @access private
	 * @var string
	 */
	private $_id;
	
	/**
	 * Module XML File
	 *
	 * This property stores the SimpleXMLElement objects that help the
	 * {@link M_ApplicationModule} class in reading the modules' info.xml
	 * files.
	 * 
	 * @static
	 * @access private
	 * @var array
	 */
	private static $_xml = array();
	
	/**
	 * The modules' configs
	 *
	 * This property stores the configuration objects of the modules, and is 
	 * populated by {@link M_ApplicationModule::getConfig()}
	 * 
	 * @static
	 * @access private
	 * @var array
	 */
	private static $_config = array();
	
	/**
	 * Constructor
	 * 
	 * To construct an instance of {@link M_ApplicationModule}, you need
	 * to provide the ID of the module. For more information on the ID
	 * of the module, please read {@link M_ApplicationModule::$_id}.
	 *
	 * @access public
	 * @param string $id
	 * 		The Module's ID
	 * @return M_ApplicationModule
	 */
	public function __construct($id) {
		$this->_id = strtolower($id);
	}
	
	/**
	 * Get the ID
	 * 
	 * This method will provide with the Module's ID. For more info 
	 * on the ID of the module, read {@link M_ApplicationModule::$_id}.
	 *
	 * @access public
	 * @return string
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Get the name of the module
	 * 
	 * This method will provide with the name of the module. Do not 
	 * confuse this value with the ID of the module. The name of the
	 * module is fetched from the module's info.xml file.
	 *
	 * @access public
	 * @return string
	 */
	public function getName() {
		return (string) $this->_getInfoXmlFile()->name;
	}
	
	/**
	 * Get the version of the module
	 *
	 * @access public
	 * @return M_ApplicationVersion
	 */
	public function getVersion() {
		return M_ApplicationVersion::constructWithString((string) $this->_getInfoXmlFile()->version);
	}
	
	/**
	 * Get release notes
	 * 
	 * Will provide with a collection of release notes. Each of the release notes 
	 * is represented by an instance of {@link M_ApplicationReleaseNote}.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getReleaseNotes() {
		// Return value
		$out = array();
		
		// If release notes are available on the module:
		$node = $this->_getInfoXmlFile()->releases;
		if($node) {
			// For each of the release notes:
			$children = $node->children();
			if(count($children) > 0) {
				foreach($children as $child) {
					// Make sure a version number has been given:
					if(! isset($child['version'])) {
						throw new M_ApplicationException(sprintf(
							'Missing Version Number in Release Note "%s"',
							(string) $child
						));
					}
					
					// Construct a Release note:
					$note = new M_ApplicationReleaseNote;
					$note->setVersion(M_ApplicationVersion::constructWithString($child['version']));
					$note->setDescription((string) $child);
					
					// Add the release note to the output:
					$out[] = $note;
				}
			}
		}
		
		// Return the collection of release notes:
		return new ArrayIterator($out);
	}
	
	/**
	 * Get the description of the module
	 *
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return (string) $this->_getInfoXmlFile()->description;
	}
	
	/**
	 * Get dependencies
	 * 
	 * This method will inform about other application modules upon 
	 * which this module depends in order to function properly. Note 
	 * that this method returns an iterator of {@link M_ApplicationModule}
	 * objects.
	 * 
	 * Note:
	 * The dependencies of the module are defined in the module's
	 * info.xml file. To define dependencies, check out Example 1 and 2.
	 * 
	 * Example 1, illustrates how the module 'user' is defined as 
	 * depending on one other module:
	 * <code>
	 *    <info>
	 *       <name>User Management</name>
	 *       <dependencies>authentication</dependencies>
	 *    </info>
	 * </code>
	 * 
	 * Example 2, illustrates how the module 'admin' is defined as 
	 * depending on more than one other module:
	 * <code>
	 *    <info>
	 *       <name>Admin Package</name>
	 *       <dependencies>
	 *          <module>authentication</module>
	 *          <module>user</module>
	 *       </dependencies>
	 *    </info>
	 * </code>
	 *
	 * @access public
	 * @return ArrayIterator
	 */
	public function getDependencies() {
		// The return value:
		$out = array();
		
		// We check if a dependency node exists
		if ($this->_getInfoXmlFile()->dependencies == false) {
			return $out;	
		}

		// If so, this module may depend on more than one module. 
		$c = $this->_getInfoXmlFile()->dependencies->children();
		$n = count($c);
		
		// If so, we do not refer to a collection of modules:
		if($n == 0) {
			$c = array($this->_getInfoXmlFile()->dependencies);
			
		}
		
		// If so, go through the collection of modules:
		foreach($c as $module) {
			// Add the module to the return value:
			$out[] = new self((string) $module);
		}
		
		// Return the final value:
		return new ArrayIterator($out);
	}
	
	/**
	 * Get Data Object Names
	 * 
	 * This method informs about which data objects are made available
	 * in the module. Note that this method returns an iterator of Data
	 * Object names, of which the keys are the Data Objects' ID's.
	 * 
	 * Note:
	 * The data objects of a module are defined in the module's
	 * info.xml file. To define data objects, check out Example 1.
	 * 
	 * Example 1
	 * <code>
	 *    <info>
	 *       <name>Portfolio</name>
	 *       <dataObject>
	 *          <portfolio>Portfolio</portfolio>
	 *          <category>PortfolioCategory</category>
	 *       </dataObject>
	 *    </info>
	 * </code>
	 * 
	 * In Example 1, we have defined two different data objects of a
	 * module called 'Portfolio'. Note that the names of the dataObject
	 * child nodes are used as the ID of the data objects, while the 
	 * values are being used as the class names of the data objects.
	 * Typically, these classes are subclasses of {@link M_DataObject}.
	 * 
	 * Example 2, get the mappers for each data object in module 'Portfolio'
	 * <code>
	 *    $module = new M_ApplicationModule('portfolio');
	 *    foreach($module->getDataObjectNames() as $id => $name) {
	 *       $module->getDataObjectMapper($id);
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getDataObjectNames() {
		$out = array();
		if(isset($this->_getInfoXmlFile()->dataObject)) {
			foreach($this->_getInfoXmlFile()->dataObject->children() as $id => $name) {
				$out[(string) $id] = (string) $name;
			}
		}
		return new ArrayIterator($out);
	}
	
	/**
	 * Get ID of DataObject, based on object
	 * 
	 * @uses ApplicationModule::getDataObjectIdOfClassName()
	 * @access public
	 * @param M_DataObject $object
	 * @return string
	 */
	public function getDataObjectIdOfObject(M_DataObject $object) {
		return $this->getDataObjectIdOfClassName(get_class($object));
	}
	
	/**
	 * Get ID of DataObject, based on class name
	 * 
	 * This method will provide with the ID of a data object, which is being 
	 * identified by the corresponding class name.
	 * 
	 * Example 1, the module's info.xml
	 * <code>
	 *    <info>
	 *       <name>Portfolio</name>
	 *       <dataObject>
	 *          <portfolio>Portfolio</portfolio>
	 *          <category>PortfolioCategory</category>
	 *       </dataObject>
	 *    </info>
	 * </code>
	 * 
	 * Example 1, get the ID of PortfolioCategory:
	 * <code>
	 *    // Get the ID of the object:
	 *    $module = new M_ApplicationModule('portfolio');
	 *    $module->getDataObjectIdOfClassName('PortfolioCategory'); // outputs "category"
	 * </code>
	 * 
	 * NOTE:
	 * This method will return NULL, if the Data Object could not have been 
	 * identified.
	 * 
	 * @access public
	 * @param M_DataObject $object
	 * @return string
	 */
	public function getDataObjectIdOfClassName($dataObjectClassName) {
		// Cast the provided class name to a string
		$dataObjectClassName = (string) $dataObjectClassName;
		
		// If data objects are defined:
		if(isset($this->_getInfoXmlFile()->dataObject)) {
			// For each of the data objects:
			foreach($this->_getInfoXmlFile()->dataObject->children() as $currentId => $currentName) {
				// If the current one matches:
				if($dataObjectClassName == $currentName) {
					// Return the DataObject ID
					return $currentId;
				}
			}
		}
		
		// Return NULL, if still here
		return NULL;
	}
	
	/**
	 * Get collection of dataObjects for this module
	 *
	 * @see {M_ApplicationModule::getDataObjectNames()}
	 * @return ArrayIterator
	 * 		Collection of M_DataObjects
	 */
	public function getDataObjects() {
		// If so, go through the collection of dataObjects:
		foreach($this->getDataObjectNames() as $dataObjectName) {
			// Compose the class name:
			$className = (string) $dataObjectName;
			
			// load the object
			M_Loader::loadDataObject($className, $this->_id);
			
			// Add the module to the return value:
			$out[] = new $className;
		}
		
		// Return the final value:
		return new ArrayIterator($out);
	}
	
	/**
	 * Get Data Object class name
	 * 
	 * This method will provide with the class name of a given data
	 * object. Note that the data object is being identified by the ID
	 * with which it has been defined in the module. For more info on
	 * how data objects are defined in a module, read the documentation
	 * on {@link M_ApplicationModule::getDataObjectNames()}.
	 *
	 * @access public
	 * @param string $id
	 * 		The ID of the data object
	 * @return string
	 */
	public function getDataObjectClassName($id) {
		if(isset($this->_getInfoXmlFile()->dataObject) && $this->_getInfoXmlFile()->dataObject->$id) {
			return (string) $this->_getInfoXmlFile()->dataObject->$id;
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get Data Object
	 * 
	 * Same as {@link M_ApplicationModule::getDataObjectClassName()}, but will 
	 * return a constructed object instead of the object's class name.
	 *
	 * @access public
	 * @param string $id
	 * 		The ID of the data object
	 * @return string
	 */
	public function getDataObject($id) {
		$className = $this->getDataObjectClassName($id);
		M_Loader::loadModel($className, $this->_id);
		return new $className;
	}
	
	/**
	 * Get Data Object's Mapper class name
	 * 
	 * Same as {@link M_ApplicationModule::getDataObjectClassName()},
	 * but will return the class name of the data object's mapper.
	 *
	 * @access public
	 * @param string $id
	 * 		The ID of the data object
	 * @return string
	 */
	public function getDataObjectMapperClassName($id) {
		return $this->getDataObjectClassName($id) . M_DataObjectMapper::MAPPER_SUFFIX;
	}
	
	/**
	 * Get Data Object's Mapper
	 * 
	 * Same as {@link M_ApplicationModule::getDataObjectMapperClassName()},
	 * but will return a constructed mapper object instead of the mapper's
	 * class name.
	 *
	 * @access public
	 * @param string $id
	 * 		The ID of the data object
	 * @return M_DataObjectMapper
	 */
	public function getDataObjectMapper($id) {
		$className = $this->getDataObjectMapperClassName($id);
		M_Loader::loadModel($className, $this->_id);
		return new $className;
	}
	
	/**
	 * Get config
	 * 
	 * This method will provide with the configuration of the module.
	 * Note that this configuration is being represented by an instance
	 * of {@link M_Config}.
	 * 
	 * NOTE:
	 * The module's configuration settings is fetched from the application's
	 * configuration; {@link M_ApplicationConfig::getConfig()}. If no configuration 
	 * is available yet, the module's default configuration - which is provided 
	 * by {@link M_ApplicationModule::getConfigDefault()} - will be set in the 
	 * app's config: {@link M_ApplicationConfig::setConfig()}
	 * 
	 * @access public
	 * @return M_Config
	 */
	public function getConfig() {
		// If not requested before:
		if(! isset(self::$_config[$this->_id])) {
			// Camel-case the Module ID:
			$moduleIdCamelCased = M_Helper::getCamelCasedString($this->_id);
			
			// Get the application's configuration namespace:
			$config = M_ApplicationConfig::getInstance();
			
			// Check if the module's configuration is available in the application's
			// configuration object:
			if(! $config->hasConfig($moduleIdCamelCased)) {
				// If no configuration of the module has been made available in the
				// application's configuration namespace, we add the default 
				// configuration of this module:
				$config->setConfig($moduleIdCamelCased, $this->getConfigDefault());
			}
			
			// Get the final configuration of the module:
			self::$_config[$this->_id] = $config->getConfig($moduleIdCamelCased);
			
			// We make sure that the module's namespace in the application contains
			// an instance of MI_Config. If that's not the case, we return an empty
			// MI_Config instance.
			if(! M_Helper::isInstanceOf(self::$_config[$this->_id], 'MI_Config')) {
				self::$_config[$this->_id] = new M_Config();
			}
		}
		
		// Return the config:
		return self::$_config[$this->_id];
	}
	
	/**
	 * Get default config
	 * 
	 * This method will provide with the default configuration of the module.
	 * Note that this configuration is being represented by an instance
	 * of {@link M_Config}.
	 * 
	 * NOTE:
	 * The configuration is fetched automatically from the module. To
	 * add a configuration to a module, you should create an XML file
	 * in the root folder of that module (where info.xml is also located).
	 * That XML file should be named config.xml:
	 * 
	 * <code>/application/modules/[my-module]/config.xml</code>
	 * 
	 * NOTE:
	 * If no configuration is available, this method will return an 
	 * empty {@link M_Config} instance.
	 * 
	 * @see M_ApplicationModule::getConfig()
	 * @access public
	 * @return M_ConfigXml
	 */
	public function getConfigDefault() {
		// Construct the config file
		$file = new M_File(
			M_Loader::getAbsolute(
				M_Loader::getModulePath($this->_id) . '/config.xml'
			)
		);
		
		// If the file exists:
		if($file->exists()) {
			// we mount an MI_Config instance:
			$config = new M_ConfigXml($file->getPath());
			
			// Download the M_Config from the root node:
			if(M_Helper::isInstanceOf($config->current(), 'MI_Config')) {
				return $config->current();
			}
		}
		
		// If we're still here, it means that we could not mount a config object.
		// we create an empty config object:
		return new M_Config();
	}
	
	/**
	 * Check if installed
	 *
	 * This method will check if the module has been installed in the
	 * application. Note that the return value of this methos is also
	 * used by {@link M_Application::isModuleInstalled()}.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isInstalled() {
		return is_dir(M_Loader::getAbsolute(M_Loader::getModulePath($this->_id)));
	}
	
	/**
	 * Install the module
	 * 
	 * This method will install the module, so it can be used in the
	 * application. Note that, by installing a module, the following 
	 * tasks are performed:
	 * 
	 * <strong>Install Database</strong>
	 * If the module defines (a) DataObjectMapper class(es), the necessary
	 * tables will be created in the database, in order to support the
	 * object mapper.
	 * 
	 * <strong>Run installer</strong>
	 * If the module comes with an installer, it will be executed in
	 * order to perform additional module installation tasks. Note
	 * that {@link M_ApplicationModule} will look for an installer
	 * class in the module's root folder.
	 * 
	 * @uses M_Console
	 * @access public
	 * @return void
	 */
	public function install() {
		// For each of the data objects in the module
		foreach($this->getDataObjectNames() as $id => $name) {
			// Get the mapper for the current data object:
			$mapper = $this->getDataObjectMapper($id);

			// Now, we create a Data Object Database Factory with the
			// mapper we have constructed:
			$factory = new M_DataObjectDbFactory($mapper);
			
			// The factory will tell us about the database table(s)
			// that need to be installed in order to support the new
			// data object. For each of the table(s):
			/* @var $table M_DbTable */
			foreach($factory->getTables() as $table) {
				// Create the table in the database:
				$res = $table->create();
			}
		}
		
		// Now, we check if the module provides with an installer.
		$installer = $this->_getInstallController();

		// If so:
		if($installer) {
			// If the class implements the "install" method:
			if(method_exists($installer, 'install')) {
				// We run it:
				$installer->install();
			}
		}
	}
	
	/**
	 * Uninstall module
	 * 
	 * @todo uninstall a module
	 *
	 */
	public function uninstall() {
		// - undo changes made to system by of install()
		// - run uninstaller, if module provides with one
	}
	
	protected function _getInfoXmlFile() {
		// If the XML file has not been requested before:
		if(!isset(self::$_xml[$this->_id])) {
			// We compose the complete (absolute) path to the module's
			// info.xml file. With this path, we construct an M_File object.
			$file = new M_File(M_Loader::getAbsolute(M_Loader::getModulePath($this->_id) . '/info.xml'));
			
			// If the file cannot be found, we throw an exception to
			// inform about the failure to fetch module info.
			if(!$file->exists()) {
				throw new M_ApplicationException(sprintf(
					'%s: Cannot load %s',
					__CLASS__,
					$file->getPath()
				));
			}
			
			// If the file does exist, we try to parse the XML file into
			// an instance of SimpleXmlElement:
			try {
				// Load & Parse the file
				self::$_xml[$this->_id] = @simplexml_load_file($file->getPath());
				
				// Validate required fields:
				$hasName        = isset(self::$_xml[$this->_id]->name);
				$hasVersion     = isset(self::$_xml[$this->_id]->version);
				$hasDescription = isset(self::$_xml[$this->_id]->description);
				if(! $hasName || ! $hasVersion || ! $hasDescription) {
					throw new M_ApplicationException('Missing required "name", "version" or "description"');
				}
			}
			// If anything goes wrong, we throw an exception to inform
			// about the failure to fetch module info.
			catch(Exception $e) {
				throw new M_ApplicationException(sprintf(
					'%s: Could not parse info.xml of module %s (%s)',
					__CLASS__,
					$this->_id,
					$e->getMessage()
				));
			}
		}
		
		// We return the XML object:
		return self::$_xml[$this->_id];
	}
	
	protected function _getInstallController() {
		// Compose the class name of the installer:
		$className = 'Install' . M_Helper::getCamelCasedString($this->_id) . 'Controller';

		// We compose the complete (absolute) path to the module's
		// installer class. With this path, we construct an M_File object.
		$file = new M_File(
			M_Loader::getAbsolute(
				M_Loader::getModulePath($this->_id) . DIRECTORY_SEPARATOR . 
				M_Loader::getControllersFolder() . DIRECTORY_SEPARATOR . 
				$className . '.class.php'
			)
		);
		
		// If the file exists:
		if($file->exists()) {
			// We load the class into the application:
			M_Loader::loadController($className, $this->_id);
			
			// We construct the controller, and return it as a result of 
			// this method:
			return new $className;
		}
		// If the file does not exist:
		else {
			return NULL;
		}
	}
}