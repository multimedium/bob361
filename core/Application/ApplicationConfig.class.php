<?php
/**
 * M_ApplicationConfig class
 * 
 * M_ApplicationConfig is used by the application to organize configuration 
 * files in the app. It is used by classes such as {@link M_ApplicationModule}
 * to save the configuration values into one central place.
 * 
 * Also, this class may be used by other modules, to group configuration values
 * from different {@link MI_Config} instances into one central config file. In order
 * to group configuration values, you can create a singleton object of this
 * class and define a namespace for it. You can then add {@link MI_Config} instances
 * to this singleton object with {@link M_ApplicationConfig::setConfig()}.
 * 
 * @package Core
 */
class M_ApplicationConfig extends M_Object {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Singleton instances
	 * 
	 * This property stores the singleton objects that are being dispatched by 
	 * the singleton constructor {@link M_ApplicationConfig::getInstance()}
	 * 
	 * @static
	 * @access public
	 * @var array
	 */
	private static $_instances = array();
	
	/**
	 * Config directory
	 * 
	 * This property stores the directory where configuration file(s) should
	 * be maintained.
	 * 
	 * @static
	 * @see M_ApplicationConfig::setConfigDirectory()
	 * @see M_ApplicationConfig::getConfigDirectory()
	 * @access private
	 * @var string
	 */
	private static $_configDirectory;
	
	/**
	 * The config object
	 * 
	 * This property stores an {@link MI_Config} instance, which stores the
	 * configuration values. This configuration object is then used to write the
	 * config values to a file in {@link M_ApplicationConfig::$_configDirectory}
	 * 
	 * @access private
	 * @var MI_Config
	 */
	private $_config;
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * (Private) Constructor
	 * 
	 * Note that the signature on the M_ApplicationConfig class constructor 
	 * is PRIVATE. This means that you cannot directly instantiate an object of
	 * this class, as objects can only be constructed by the class itself.
	 * 
	 * If you want to construct an object of this class, you should use the 
	 * following (singleton) constructor instead:
	 * 
	 * <code>
	 *    $appConfig = M_ApplicationConfig::getInstance();
	 * </code>
	 *
	 * @access private
	 * @param string $namespace
	 * @return M_ApplicationConfig
	 */
	private function __construct($namespace) {
		// Compose the full path to the configuration file:
		// (Note that we use XML files to store configuration values)
		$file = new M_File(self::getConfigDirectory() . '/' . $namespace . '.xml');
		
		// We check if the config file exists:
		if(! $file->exists()) {
			// If it does not, we attempt to create it:
			// (We use XML to store configuration values, so we create the config
			// with an instance of M_ConfigXml)
			$this->_config = new M_ConfigXmlString(
				// Initial XML Configuration Values:
				'<?xml version="1.0" encoding="UTF-8"?><config></config>',
				// The source; the file where the XML should be saved to
				$file->getPath()
			);
			
			// We define the config as having changed, so it is written to file
			$this->_config->setIsChanged(TRUE);
		}
		// If the configuration file already exists
		else {
			// we read it with the M_ConfigXml class
			$this->_config = new M_ConfigXml($file->getPath());
		}
	}
	
	/**
	 * Destructor
	 * 
	 * Before destructing the object, all changes to the configuration under the same
	 * namespace are saved back to the config file.
	 * 
	 * By delaying the save to the config file until the destruction of the object, 
	 * we reduce the number of write operations that are performed on the file
	 * considerably.
	 * 
	 * @access public
	 * @return void
	 */
	public function __destruct() {
		try {
			if($this->_config->isChanged()) {
				$this->_config->save();
			}
		} catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	
	/* -- SINGLETON CONSTRUCTOR -- */
	
	/**
	 * Singleton constructor
	 * 
	 * NOTE:
	 * By default, the "Application" namespace is used
	 * 
	 * @see M_ApplicationConfig::__construct()
	 * @access public
	 * @param string $namespace
	 * @return M_ApplicationConfig
	 */
	public static function getInstance($namespace = 'Application') {
		// If the singleton object does not yet exist:
		if(!isset(self::$_instances[$namespace])) {
			// We construct the singleton now:
			self::$_instances[$namespace] = new self($namespace);
		}
		
		// Return the singleton
		return self::$_instances[$namespace];
	}
	
	/* -- STATIC SETTERS -- */
	
	/**
	 * Set config directory
	 * 
	 * Will set the path to the directory where config files are located. This 
	 * directory will be used to read from configuration files, and to write to 
	 * the configuration file(s).
	 * 
	 * NOTE:
	 * This method is to be called statically, and will affect the location of 
	 * ALL configuration files in the application
	 * 
	 * @static
	 * @see M_ApplicationConfig::getConfigDirectory()
	 * @access public
	 * @param string $directoryPath
	 * @return void
	 */
	public static function setConfigDirectory($directoryPath) {
		self::$_configDirectory = M_Helper::trimCharlist((string) $directoryPath, '/');
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Add configuration value(s)
	 * 
	 * This method can be used to set/add configuration values in the object,
	 * under the namespace with which the {@link M_ApplicationConfig} singleton
	 * object has been constructed: {@link M_ApplicationConfig::getInstance()}.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name with which to save the configuration value(s)
	 * @param MI_Config $config
	 * 		The configuration value(s)
	 * @return void
	 */
	public function setConfig($name, MI_Config $config) {
		// Make root node, if not already done
		if(! M_Helper::isInstanceOf($this->_config->config, 'MI_Config')) {
			$this->_config->config = new M_Config;
		}
		
		// Set the value
		$this->_config->config->set((string) $name, $config);
	}
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get config directory
	 * 
	 * Will provide with the path to the directory where config files are located. 
	 * This directory is used to read from configuration files, and to write to 
	 * the configuration file(s).
	 * 
	 * If not set previously with {@link M_ApplicationModule::setConfigDirectory()},
	 * this method will return the following (absolute) path by default:
	 * 
	 * <code>
	 *    M_Loader::getAbsolute('files/config');
	 * </code>
	 * 
	 * @static
	 * @see M_ApplicationConfig::setConfigDirectory()
	 * @access public
	 * @param string $directoryPath
	 * @return void
	 */
	public static function getConfigDirectory() {
		if(self::$_configDirectory) {
			return self::$_configDirectory;
		} else {
			return M_Loader::getAbsolute('files/config');
		}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Check if a config is available
	 * 
	 * Will check if a configuration value(s) is available under a given name.
	 * 
	 * @see M_ApplicationConfig::setConfig()
	 * @access public
	 * @param string $name
	 * 		The name with which the configuration value(s) have been saved
	 * @return bool $flag
	 * 		Returns TRUE if configuration value(s) is(are) available
	 */
	public function hasConfig($name) {
		$name = (string) $name;
		return isset($this->_config->config->$name);
	}
	
	/**
	 * Get configuration
	 * 
	 * This method can be used to get the configuration that has been saved with
	 * a given name, or to retrieve all configuration values that have been grouped
	 * under the object's namespace: {@link M_ApplicationConfig::getInstance()}
	 * 
	 * NOTE:
	 * If no configuration value(s) could have been found for the requested name,
	 * this method will return NULL instead!
	 * 
	 * @see M_ApplicationConfig::setConfig()
	 * @see M_ApplicationConfig::hasConfig()
	 * @access public
	 * @param string $name
	 * 		The name with which the configuration value(s) have been saved
	 * @return MI_Config
	 */
	public function getConfig($name = NULL) {
		if($name) {
			$name = (string) $name;
			if(isset($this->_config->config->$name)) {
				return $this->_config->config->$name;
			} else {
				return NULL;
			}
		} else {
			return $this->_config->config;
		}
	}
}