<?php
/**
 * M_ObjectSingleton class
 * 
 * The M_ObjectSingleton class is the superclass for all Core classes
 * that dispatch Singleton objects. It provides with the basic 
 * extensions that we expect from all Core Singleton objects.
 * 
 * @package Core
 */
abstract class M_ObjectSingleton extends M_Object {
	/*
	 * Singleton object
	 * 
	 * This property stores a singleton object, for each of the (sub)classes it
	 * has been requested from.
	 * 
	 * @static
	 * @access private
	 * @var array
	 *
	private static $_instances = array();
	
	/*
	 * Constructor
	 * 
	 * A singleton object may be constructed only by the class itself. 
	 * In order to prevent others from constructing an object from the 
	 * singleton class, we make the constructor protected!
	 * 
	 * To obtain the singleton object, you should use the method
	 * {@link M_ObjectSingleton::getInstance()} instead.
	 * 
	 * @access protected
	 * @return M_ObjectSingleton
	 *
	protected function __construct() {
	}
	
	/*
	 * Singleton constructor
	 * 
	 * Provides with the singleton object.
	 * 
	 * @access public
	 * @return M_ObjectSingleton
	 *
	public static function getInstance() {
		// Get the class name:
		$className = get_called_class();
		
		// We implement the lazy nature of a singleton object, and do not 
		// instantiate the singleton instance until requested:
		if(! isset(self::$_instances[$className])) {
			// Instantiate the instance:
			self::$_instances[$className] = new $className;
		}
		
		// Return the singleton instance:
		return self::$_instances[$className];
	}
	 */
	
	/**
	 * Clone the object
	 * 
	 * A singleton object cannot be cloned, since the singleton object 
	 * is the only object that is allowed to exist. In order to prevent
	 * singleton objects from being cloned, we throw an exception!
	 * 
	 * NOTE (php.net)
	 * An object copy is created by using the clone keyword (which 
	 * calls the object's __clone() method if possible). An object's 
	 * __clone() method cannot be called directly.
	 * 
	 * @access public
	 * @return void
	 */
	public function __clone() {
		throw new M_ObjectException(
			'Cannot clone a SINGLETON Object (%s)!',
			$this->getClassName()
		);
	}
}

/**
 * Get called class
 * 
 * The "Late Static Binding" class name; Gets the name of the class the static 
 * method is called in.
 * 
 * PHP offers a built-in get_called_class() function as of 5.3.0. In case we
 * are running a prior version of PHP, we provide with our own implementation
 * of get_class_name().
 * 
 * We use this function in order to provide with an abstract superclass for
 * singleton objects in the application.
 * 
 * @access public
 * @return string
 *
if(! function_exists('get_called_class')) {
	function get_called_class() {
		$bt = debug_backtrace();
		$lines = file($bt[1]['file']);
		$matches = array();
		preg_match('/([a-zA-Z0-9\_]+)::'.$bt[1]['function'].'/',  $lines[$bt[1]['line']-1], $matches);
		return $matches[1];
	}
}
 */