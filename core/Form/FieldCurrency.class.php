<?php
/**
 * M_FieldCurrency class
 * 
 * M_FieldCurrency, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldText}
 * - {@link M_FieldNumeric}
 * 
 * handles the input control that allows users to introduce a 
 * currency.
 * 
 * @package Core
 */
class M_FieldCurrency extends M_FieldNumeric {
	
	/**
	 * Supported currencies
	 *
	 */
	const CURRENCY_EURO = 'EUR';
	const CURRENCY_DOLLAR = 'USD';

	/**
	 * Current currency, default: euro
	 *
	 * @var string
	 */
	private $_currency = self::CURRENCY_EURO;
	
	/**
	 * Autoformat the value of the field
	 *
	 * @var bool
	 */
	private $_autoFormat = true;
	
	/**
	 * Number precision
	 */
	private $_precision;
	
	/**
	 * Will the field be formatted automatically?
	 * 
	 * @return bool
	 */
	public function isAutoFormat() {
		return $this->_autoFormat;
	}
	
	/**
	 * Set the default value
	 *
	 * @param string $value
	 */
	public function setDefaultValue($value) {
		if(! is_null($value)) {
			//format the number
			$number = new M_Number($value);
			
			//set the value
			$this->defaultValue = $number->toCurrencyString(
				$this->getCurrency(),
				null,
				$this->getPrecision()
			);
		}
		else {
			$this->defaultValue = NULL;
		}
	}
	
	/**
	 * Should the field be formatted automatically?
	 * 
	 * @param bool $_autoFormat
	 */
	public function setAutoFormat($_autoFormat) {
		$this->_autoFormat = (bool)$_autoFormat;
	}
	
	/**
	 * Get active currency
	 *
	 * @return string
	 */
	public function getCurrency() {
		return $this->_currency;
	}
	
	/**
	 * Set the active currency
	 *
	 * Example 1
	 * <code>
	 *    $field = new M_FieldCurrency('MyField');
	 *    $field->setCurrency('dollar');
	 * </code>
	 * 
	 * @param string $arg
	 */
	public function setCurrency($arg) {
		$this->_currency = $arg;
	}
		
	/**
	 * Set the precision
	 * 
	 * @param int $arg
	 * @return M_FieldCurrency 
	 */
	public function setPrecision($arg) {
		$this->_precision = (int)$arg;
		return $this;
	}
	
	/**
	 * Get the precision
	 * 
	 * @return int 
	 */
	public function getPrecision() {
		return $this->_precision;
	}
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()} and {@link M_FieldText::set()}, to make 
	 * the following (additional) definition keys available:
	 * 
	 * <code>currency</code>
	 * 
	 * Set currency. For more information, read the docs on 
	 * {@link M_FieldCurrency::setCurrency()}.
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// The currency in which the field's value is to be interpreted
			case 'currency':
				$this->setCurrency($definition);
				break;

			// Whether or not the introduced value should be autoformatted
			case 'autoformat':
				$this->setAutoFormat($definition);
				break;
			case 'precision':
				$this->setPrecision($definition);
				break;
			// Other properties:
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}

	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldCurrency} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldText
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldCurrency($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}