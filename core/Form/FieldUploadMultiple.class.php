<?php
/**
 * M_FieldUploadMultiple class
 * 
 * M_FieldUploadMultiple, a subclass of {@link M_FieldUpload}, handles the input 
 * control that allows users to choose multiple files from their computer
 * and upload them to the server.
 * 
 * @package Core
 */
class M_FieldUploadMultiple extends M_Field {
	/**
	 * Allowed extensions
	 * 
	 * This property holds the collection of file extensions that
	 * are allowed by M_FieldUpload. For more info, read the docs
	 * on {@link M_FieldUpload::setAllowedExtensions()}.
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_allowedExtensions = array();
	
	/**
	 * Maximum filesize
	 * 
	 * This property holds the maximum filesize that is allowed
	 * by M_FieldUpload. Read {@link M_FieldUpload::setMaxFileSize()}
	 * for more info.
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_maxSize = 0;
	
	/**
	 * Upload directory
	 * 
	 * This property holds the directory where the uploaded file 
	 * should be saved. Note that, if not given, this directory 
	 * will be defaulted to the "/files" folder in the application's 
	 * root folder.
	 * 
	 * @access private
	 * @var string
	 */
	private $_directory;
	
	/**
	 * Upload filename
	 * 
	 * The filename of the uploaded file. Note that, if not provided
	 * to M_FieldUpload by using {@link M_FieldUpload::setFilename()},
	 * this property will be defaulted to the current timestamp.
	 * 
	 * @access private
	 * @var string
	 */
	private $_filename;
	
	/**
	 * Upload original filename
	 * 
	 * The original filename of the uploaded file.
	 * 
	 * @access private
	 * @var string
	 */
	private $_filenameOriginal;

	/**
	 * Set the URL which will process the uploaded file
	 * 
	 * @var string
	 */
	private $_processUrl;

	/**
	 * Set the data which will be posted to {@link M_FieldUploadMultiple::_processUrl}
	 *
	 * @var string
	 */
	private $_processData = array();
	
	/**
	 * Is uploaded?
	 * 
	 * This property is a boolean, that indicates whether or not a
	 * file has been uploaded. For internal use only.
	 * 
	 * @access protected
	 * @var boolean
	 */
	protected $_isUploaded = FALSE;
	
	/**
	 * Uploaded filename
	 * 
	 * This property holds the path to the uploaded file. For internal
	 * use only.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_uploaded = NULL;
	
	/**
	 * Default error message
	 * 
	 * @see M_Field::$_defaultErrorMessage
	 * @access protected
	 * @var string
	 */
	protected $_defaultErrorMessage = 'Please choose file(s) from your computer';
	
	/**
	 * Attach to {@link M_Form}
	 * 
	 * This method overrides {@link M_Field::__form()}.
	 * 
	 * When a M_FieldUpload object is attached to a form, the field
	 * will update the following {@link M_Form} properties:
	 * 
	 * <code>
	 *    Property           | Value
	 *    -------------------|------------------------
	 *    method             | POST
	 *    enctype            | multipart/form-data
	 * </code>
	 * 
	 * These properties are crucial for the upload to work properly.
	 * Read {@link M_Form::setProperty()} for more info about the
	 * form properties.
	 * 
	 * @access public
	 * @see M_Field::__form()
	 * @param M_Form $form
	 * 		The form to which the field is being added.
	 * @return void
	 */
	public function __form(M_Form $form) {
		$form->setProperty('method', 'POST');
		$form->setProperty('enctype', 'multipart/form-data');
	}
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>allowedExtensions</code>
	 * <code>extensions</code>
	 * 
	 * Set the allowed upload extensions. For more info, read 
	 * {@link M_FieldUpload::setAllowedExtensions()}.
	 * 
	 * <code>maxFilesize</code>
	 * <code>maxSize</code>
	 * 
	 * The maximum filesize of the uploaded file. To learn more,
	 * read {@link M_FieldUpload::setMaxFileSize()}.
	 * 
	 * <code>directory</code>
	 * 
	 * Sets the directory where the uploaded file should be saved.
	 * Read {@link M_FieldUpload::setDirectory()}.
	 * 
	 * <code>filename</code>
	 * 
	 * Sets the filename under which the uploaded file should be saved.
	 * Read {@link M_FieldUpload::setFilename()}.
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// Allowed extensions
			case 'extensions':
			case 'allowedExtensions':
				$this->setAllowedExtensions($definition);
				break;

			// Maximum size of uploaded files
			case 'maxFilesize':
			case 'maxSize':
				$this->setMaxFileSize($definition);
				break;

			// Directory where uploaded files are saved
			case 'directory':
				$this->setDirectory($definition);
				break;

			// Which url needs to process the uploads
			case 'processUrl':
				$this->setProcessUrl($definition);
				break;

			// Post data to the url which processes the uploads
			case 'processData':
				$this->setProcessData($definition);
				break;

			// Other properties
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Set allowed extensions.
	 * 
	 * This method will set a collection of file extensions that
	 * are allowed by M_FieldUpload. If the user uploads a file
	 * of which the extension does not match one of the "allowed"
	 * extensions, a validation error message will be generated.
	 * 
	 * {@link M_FieldUpload::__validate()} will use this collection
	 * of extensions to evaluate the uploaded file.
	 * 
	 * NOTE:
	 * The collection of extensions can be provided as a string 
	 * (separating extensions by dot-comma), or as an array of extensions.
	 * 
	 * Example 1, pass in extensions with a string
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setAllowedExtensions('jpg;gif;png');
	 * </code>
	 * 
	 * Example 2, pass in extensions with an array
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setAllowedExtensions(array('jpg', 'gif', 'png));
	 * </code>
	 * 
	 * NOTE:
	 * The dot notation is completely ignored by this function,
	 * which makes Example 3 and 4 perfectly valid as well:
	 * 
	 * Example 3, pass in extensions with a string
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setAllowedExtensions('.jpg;.gif;.png');
	 * </code>
	 * 
	 * Example 4, pass in extensions with an array
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setAllowedExtensions(array('.jpg', '.gif', '.png));
	 * </code>
	 * 
	 * NOTE:
	 * The validation of the uploaded file's extension is not
	 * case sensitive!
	 * 
	 * @access public
	 * @param mixed $extensions
	 * 		The allowed extensions
	 * @return void
	 */
	public function setAllowedExtensions($extensions) {
		if(!is_array($extensions)) {
			$extensions = explode(';', $extensions);
		}
		for($i = 0, $n = count($extensions); $i < $n; $i ++) {
			array_push($this->_allowedExtensions, trim(strtolower($extensions[$i]), " \t\n\r\0\x0B."));
		}
	}
	
	/**
	 * Get the file-extensions that are allowed in the file-upload
	 *
	 * @see setAllowedExtensions
	 * @return array
	 */
	public function getAllowedExtensions() {
		return $this->_allowedExtensions;
	}
	
	/**
	 * Set maximum filesize.
	 * 
	 * This method will set the maximum file size that is allowed 
	 * by M_FieldUpload. If the user uploads a file that is bigger
	 * than the allowed size, a validation error message will be 
	 * generated.
	 * 
	 * {@link M_FieldUpload::__validate()} will use this maximum
	 * filesize to evaluate the uploaded file.
	 * 
	 * NOTE:
	 * The maximum filesize can be expressed in a number of bytes,
	 * or in a string.
	 * 
	 * Example 1, pass in maximum filesize with a string
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setMaxFileSize('1MB');
	 * </code>
	 * 
	 * Example 2, pass in maximum filesize with a number
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setMaxFileSize(1048576); // 1MB
	 * </code>
	 * 
	 * If the maximum filesize is passed in with a string, 
	 * {@link M_FsHelper::getNumberOfBytesInString()} will convert
	 * it to a number of bytes.
	 * 
	 * @access public
	 * @param mixed $size
	 * 		The maximum filesize
	 * @return void
	 */
	public function setMaxFileSize($size) {
		if(is_numeric($size)) {
			$this->_maxSize = $size;
		} else {
			$this->_maxSize = M_FsHelper::getNumberOfBytesInString($size);
		}
	}
	
	/**
	 * Get the maximum file-size of an upload
	 *
	 * @see setMaxFileSize
	 * @return int
	 */
	public function getMaxFileSize() {
		return $this->_maxSize;
	}

	/**
	 * Get the max-filesize as string
	 * 
	 * @return string
	 */
	public function getMaxFileSizeString() {
		return M_FsHelper::getFileSizeString($this->getMaxFileSize());
	}

	/**
	 * Get the relative url which will process the uploaded files
	 *
	 * @return string
	 */
	public function getProcessUrl() {
		return $this->_processUrl;
	}

	/**
	 * Set the relative url which will process the uploaded files
	 *
	 * @param string $url
	 */
	public function setProcessUrl($url) {
		$this->_processUrl = $url;
	}

	/**
	 * Get the data which will be posted to the url which processes the uploads
	 * in JSON-format
	 *
	 * @return string
	 */
	public function getProcessData() {
		return M_Helper::jsonEncode($this->_processData);
	}

	/**
	 * Set the data which will be posted to {@link M_FieldUploadMultiple::getProcessUrl()}
	 *
	 * @param array $url
	 */
	public function setProcessData(array $data) {
		$this->_processData = $data;
	}
	
	/**
	 * Set upload directory
	 * 
	 * This method will set the directory where the uploaded file
	 * is saved. Note that you should provide the absolute path
	 * to the upload directory: {@link M_Loader::getAbsolute()}.
	 * 
	 * @access public
	 * @param string $dir
	 * 		The upload directory (full; absolute path)
	 * @return void
	 */
	public function setDirectory($dir) {
		$this->_directory = rtrim($dir, " \t\n\r\0\x0B\\/" . DIRECTORY_SEPARATOR);
	}
	
	/**
	 * Field-type-specific validation
	 * 
	 * This method overrides {@link M_FieldUpload::__validate()}.
	 * 
	 * The specific implementation of this method in M_FieldUploadMultiple
	 * makes sure that there are any files uploaded.
	 * 
	 * The maximum filesize and the allowed extensions are validated within
	 * the Flash-component of this field.
	 * 
	 * @access public
	 * @see M_FieldUpload::__validate()
	 * @param string $value
	 * 		The temporary value of the field. This value is 
	 * 		being provided by {@link M_FieldUploadMultiple::getValue()}
	 * @return boolean
	 */
	public function __validate($value) {
		// If a file has been uploaded:
		if(!$this->__empty($value)) {
			// Return TRUE, if there are any files uploaded
			return TRUE;
		}
		// If no file has been uploaded:
		else {
			return !$this->isMandatory();
		}
	}
	
	/**
	 * Get Value
	 * 
	 * Get the default value (array)
	 *
	 */
	public function getValue() {
		return M_Request::getVariable($this->getId(), $this->getDefaultValue(), M_Request::TYPE_ARRAY);
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_FieldUpload::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldUploadMultiple} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldUpload
	 * @return M_View
	 */
	public function getInputView() {
		// Construct the File Upload view:
		$view = new M_ViewFieldUploadMultiple($this);
		
		$sessionid = M_Auth::getInstance()->getStorage()->getStorageId();
		$view->assign('sessionid', $sessionid);
		
		// Return the view:
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}

    /**
     * Get Form Elements
     *
     * get the form input-elements of the current uploaded items
     */
    public function getFileQueueElements() {
        return M_Request::getVariable('fileQueueElements');
    }

    /**
     *Get Form Html
     *
     * get the html of the current uploaded items
     */
    public function getFileQueueHtml() {
        return M_Request::getVariable('fileQueueHtml');
    }
}