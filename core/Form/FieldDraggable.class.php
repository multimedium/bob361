<?php
/**
 * M_FieldDraggable class
 * 
 * M_FieldDraggable, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldOptions}
 * 
 * handles the input control that allow users to manually drag item(s) to another
 * field or element, with a drag-and-drop interface.
 * 
 * @package Core
 */
class M_FieldDraggable extends M_FieldOptions {
	
	/**
	 * Constructor
	 * 
	 * M_FieldDraggable overrides the {@link M_Field} constructor, in
	 * order to override the default field decorator view. It will use
	 * {@link M_Field::setView()} to change the default field decorator.
	 * 
	 * @access public
	 * @param string $id
	 * 		The field id. This value will be used to set {@link M_Field::$id}
	 * @return M_Field
	 */
	public final function __construct($id) {
		// Construct the field:
		parent::__construct($id);
		
		// Set the default decoration view:
		$this->setView('M_ViewFieldDecoratorDraggable');
	}
	
	/**
	 * @var M_FieldSortable
	 */
	protected $_connectToSortable;
	
	/**
	 * @var bool Disables the ability to select text in sortable-items
	 */
	protected $_disableSelection;
	
	/**
	 * Connect this sortable field to a {@link M_FieldSortable}
	 * 
	 * You can connect this draggable field with another sortable field. This way
	 * you can move multiple items from the draggable field to the connected sortable
	 * field
	 * 
	 * @see M_FieldSortable::connectWithSortable()
	 * @param M_FieldSortable
	 * @return void
	 */
	public function connectToSortable(M_FieldSortable $field) {
		$this->_connectToSortable = $field;		
	}
	
	/**
	 * Get the field to which this sortable-field is connected
	 * 
	 * @return M_FieldSortable
	 */
	public function getConnectToSortable() {
		return $this->_connectToSortable;
	}
	
	/**
	 * Disable text-selection of sortable items
	 * 
	 * @param $arg bool
	 * @return void
	 */
	public function setDisableSelection($arg) {
		$this->_disableSelection = (bool)$arg;
	}
	
	/**
	 * Is the ability to select text in sortable items disabled?
	 * 
	 * @return bool
	 */
	public function isDisableSelection() {
		return $this->_disableSelection;
	}
	
	/**
	 * Get the temporary value
	 * 
	 * This method overrides {@link M_Field::getValue()}.
	 * 
	 * M_FieldSortable will provide with an array that holds the 
	 * items' values, in the same order they have been provided by
	 * the user.
	 * <code>
	 *    Array (
	 *       [item-value] => [item-label],
	 *       [item-value] => [item-label],
	 *       [item-value] => [item-label],
	 *       [item-value] => [item-label]
	 *       ...
	 *    )
	 * </code>
	 * 
	 * Note that this is also the delivered value by this field (see
	 * {@link M_Field::deliver()}), because we do not override that
	 * method.
	 * 
	 * @access public
	 * @see M_Field::getValue()
	 * @return array
	 */
	public function getValue() {
		$temp = M_Request::getVariable($this->getId(), FALSE);
		$c = ($temp !== FALSE);
		if($c) {
			$values = explode(';', $temp);
			$items  = $this->getItems();
			$value  = array();
			
			$n = count($values);
			if($n == count($items)) {
				for($i = 0; $i < $n && $c; $i ++) {
					$c = (array_key_exists($values[$i], $items));
					if($c) {
						if(!in_array($values[$i], $value)) {
							$value[] = $values[$i];
						}
					}
				}
			}
			
			$c = count($value) == count($items);
		}
		
		if($c) {
			return $value;
		} else {
			return array_keys($this->getItems());
		}
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldDraggable} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldDraggable
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldDraggable($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}