<?php
/**
 * Create a mathematical CAPTCHA-field which challenges the user to execute
 * a operation. Only if the operation is correct the CAPTCHA-field will allow
 * the user to continue
 *
 * @author Ben Brughmans
 */
class M_FieldCaptchaMath extends M_FieldCaptcha {

	 /**
     * Minimum range value
     *
     * This variable holds the minimum range value
     * default set to "1"
     *
     * @access private
     * @var    integer $_minValue The minimum range value
     */
    private $_minValue = 1;

    /**
     * Maximum range value
     *
     * This variable holds the maximum range value
     * default set to "50"
     *
     * @access private
     * @var    integer $_maxValue The maximum value of the number range
     */
    private $_maxValue = 10;

    /**
     * Operators
     *
     * The valid operators to use
     * in the numeral captcha. We could
     * use / and * but not yet.
     *
     * @access private
     * @var    array $_operators The operations for the captcha
     */
    private $_operators = array();

    /**
     * Operator to use
     *
     * This variable is basically the operation
     * that we're going to be using in the
     * numeral captcha we are about to generate.
     *
     * @access private
     * @var    string $_operator The operation's operator
     */
    private $_operator;

    /**
     * First number of the operation
     *
     * This variable holds the first number
     * of the numeral operation we are about
     * to generate.
     *
     * @access private
     * @var    integer $_firstNumber The first number of the operation
     */
    private $_firstNumber;

    /**
     * Second Number of the operation
     *
     * This variable holds the value of the
     * second variable of the operation we are
     * about to generate for the captcha.
     *
     * @access private
     * @var    integer $_secondNumber The second number of the operation
     */
    private $_secondNumber;

	/**
	 * The complexity of the captcha
	 * 
	 * @var int
	 */
	private $_complexity;
	
	/**
	 * Default error message for wrong answer
	 *
	 * @access private
	 * @var string
	 */
	private $_defaultErrorMsgWrongAnswer;

    /**
     * A constant that indicates the complexity of mathematical operations
     *
     * @access public
     *
     */
    const COMPLEXITY_ELEMENTARY = 1;


    /**
     * A constant that indicates the complexity of mathematical operations
     *
     * @access public
     *
     */
    const COMPLEXITY_HIGH_SCHOOL = 2;


    /**
     * A constant that indicates the complexity of mathematical operations
     *
     * @access public
     *
     */
    const COMPLEXITY_UNIVERSITY = 3;


	public function  __construct($id) {
		parent::__construct($id);

		// Set the default decoration view:
		$this->setView('M_ViewFieldDecoratorCaptchaMath');
	}
	
	/**
	 *
	 * @param <type> $key
	 * @param <type> $definition
	 * @param <type> $translateStrings 
	 */
	public function set($key, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($key) {
			// Set the FIELD COMPLEXITY
			case 'complexity':
				// Note that we translate the string, if requested
				$this->setComplexity($definition);
				break;
			default:
				parent::set($key, $definition, $translateStrings);
		}
	}

	/**
	 * Generate the captcha
	 *
	 * @param int
	 * @return string
	 *		The operation which has been executed a.k.a "Question"
	 */
	protected  function _generateCaptcha() {
		$this->_generateFirstNumber();
        $this->_generateSecondNumber();
        $this->_generateOperator();
        $this->_generate();

		return $this->getOperation();
	}

	/**
	 * Set the complexity (difficulty) of the operation
	 * 
	 * @see self::COMPLEXITY_HIGH_SCHOOL
	 * @see self::COMPLEXITY_ELEMENTARY
	 * @see self::COMPLEXITY_UNIVERSITY
	 *
	 * @param int $arg
	 * @return void
	 */
	public function setComplexity($arg) {
		if(!in_array($arg, array(self::COMPLEXITY_ELEMENTARY, self::COMPLEXITY_HIGH_SCHOOL, self::COMPLEXITY_UNIVERSITY))) {
			throw new M_FieldException(sprintf('Unknown complexity %s', $arg));
		}
		$this->_complexity = (int)$arg;
	}

	/**
     * Get operation
     *
     * This function will get the operation
     * string from $this->_operation
     *
     * @access public
     * @return string The operation String
     */
    public function getOperation()
    {
        return $this->getFirstNumber() . ' ' .
			$this->_operators . ' ' .
            $this->getSecondNumber();
    }

	/**
     * Get the answer value
     *
     * This function will retrieve the answer
     * value from this->_answer and return it so
     * we can then display it to the user.
     *
     * @access public
     * @return string The operation answer value.
     */
    public function getAnswer()
    {
       return $this->_getSessionNamespace()->__get('captcha-'.$this->getId());
    }

	/**
     * Get the first number
     *
     * This function will get the first number
     * value from $this->_firstNumber
     *
     * @access public
     * @return integer $this->_firstNumber The firstNumber
     */
    public function getFirstNumber()
    {
        return $this->_firstNumber;
    }

	/**
     * Get the second number value
     *
     * This function will return the second number value
     *
     * @access public
     * @return integer $this->_secondNumber The second number
     */
    public function getSecondNumber()
    {
        return $this->_secondNumber;
    }
	
	/**
	 * Get default error message, for wrong answer
	 *
	 * @access public
	 * @return string
	 */
	public function getDefaultErrorMessageWrongAnswer() {
		if($this->_defaultErrorMsgWrongAnswer) {
			return $this->_defaultErrorMsgWrongAnswer;
		} else {
			return t('Please give the correct answer to the question');
		}
	}

	/**
	 * Set default error message, for wrong answer
	 *
	 * @access public
	 * @param string $errorMessage
	 * @return M_FieldCaptchaMath $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDefaultErrorMessageWrongAnswer($errorMessage) {
		// Set the default error message
		$this->_defaultErrorMsgWrongAnswer = $errorMessage;

		// return myself
		return $this;
	}

	/**
     * Set Range Minimum value
     *
     * This function give the developer the ability
     * to set the range minimum value so the operations
     * can be bigger, smaller, etc.
     *
     * @access private
     * @param  integer $_minValue The minimum value
     */
    private function _setRangeMinimum($_minValue = '1')
    {
        $this->_minValue = (int)$_minValue;
    }

	/**
     * Set Range Maximum value
     *
     * This function gives the developer the ability
     * to set the range maximum value so the operations
     * can be bigger, smaller, etc.
     *
     * @access private
     * @param  integer $_maxValue The maximum value
     */
    private function _setRangeMaximum($_maxValue = '1')
    {
        $this->_maxValue = (int)$_maxValue;
    }

	/**
     * Sets the first number
     *
     * This function sets the first number
     * of the operation by calling the _generateNumber
     * function that generates a random number.
     *
     * @access private
     * @see    $this->_firstNumber, $this->_generateNumber
     */
    private function _generateFirstNumber()
    {
        $this->_setFirstNumber($this->_generateNumber());
    }

	/**
     * Sets second number
     *
     * This function sets the second number of the
     * operation by calling _generateNumber()
     *
     * @access private
     * @see    $this->_secondNumber, $this->_generateNumber()
     */
    private function _generateSecondNumber()
    {
        $this->_setSecondNumber($this->_generateNumber());
    }

	/**
     * Sets the operation operator
     *
     * This function sets the operation operator by
     * getting the array value of an array_rand() of
     * the $this->_operators() array.
     *
     * @access private
     * @see    $this->_operators, $this->_operators
     */
    private function _generateOperator()
    {
		switch ($this->_complexity) {
			case self::COMPLEXITY_HIGH_SCHOOL:
				 $this->_operators = array('+', '-', '*');
				 break;
			case self::COMPLEXITY_UNIVERSITY:
				 $this->_operators = array('+', '-', '*', '%', '/');
				 break;
			case self::COMPLEXITY_ELEMENTARY:
			default:
				 $this->_operators = array('-', '+');
				 break;
		}
		
        $this->_operators = $this->_operators[array_rand($this->_operators)];
    }

	/**
     * Sets the answer value
     *
     * This function will accept the parameters which is
     * basically the result of the function we have done
     * and it will set $this->_answer with it.
     *
     * @access private
     * @param  integer $_answerValue The answer value
     * @see    $this->_answer
     */
    private function _setAnswer($_answerValue)
    {
		//store in
		$this->_getSessionNamespace()->__set(
			'captcha-'.$this->getId(),
			$_answerValue
		);
        return $this;
    }

	/**
	 * Get the namespace in which captcha data is stored
	 * 
	 * @return M_SessionNamespace
	 */
	private function _getSessionNamespace() {
		if (!M_Session::getInstance()->isStarted()) {
			throw new M_FieldException("Cannot use captcha when session isn't started");
		}
		return new M_SessionNamespace('captcha');
	}

	/**
     * Set First number
     *
     * This function sets the first number
     * to the value passed to the function
     *
     * @access private
     * @param  integer $value The first number value.
     * @return object $this  The self object
     */
    private function _setFirstNumber($value)
    {
        $this->_firstNumber = (int)$value;
        return $this;
    }

	/**
     * Sets the second number
     *
     * This function sets the second number
     * with the value passed to it.
     *
     * @access private
     * @param  integer $value The second number new value.
     * @return object  $this  The self object
     */
    private function _setSecondNumber($value)
    {
        $this->_secondNumber = (int)$value;
        return $this;
    }

	/**
     * Generate a number
     *
     * This function takes the parameters that are in
     * the $this->_maxValue and $this->_minValue and get
     * the random number from them using mt_rand()
     *
     * @access private
     * @return integer Random value between _minValue and _maxValue
     */
    private function _generateNumber()
    {
        return mt_rand($this->_minValue, $this->_maxValue);
    }

	/**
     * Adds values
     *
     * This function will add the firstNumber and the
     * secondNumber value and then call _setAnswer to
     * set the answer value.
     *
     * @access private
     * @see    $this->_firstNumber, $this->_secondNumber, $this->_setAnswer()
     */
    private function _doAdd()
    {
        $_answer = $this->getFirstNumber() + $this->getSecondNumber();
        $this->_setAnswer($_answer);
    }

	/**
     * Do Multiplication
     *
     * This method will multiply two numbers
     *
     * @access private
     * @see $this->_firstNumber, $this->_secondNumber, $this->_setAnswer
     *
     */
    private function _doMultiplication()
    {
        $this->_setAnswer($this->getFirstNumber() * $this->getSecondNumber());
    }

	/**
     * Do Division
     *
     * This function executes a division based on the two
     * numbers.
     *
     * @param integer $_firstNumber The first number of the operation.
     *                             This is by default set to null.
     *
     * @param integer $_secondNumber The second number of the operation
     *                              This is by default set to null.
     *
     */
    private function _doDivision($_firstNumber = null, $_secondNumber = null)
    {
        if (is_null($_firstNumber)) {
            $_firstNumber = $this->getFirstNumber();
        }

        if (is_null($_secondNumber)) {
            $_secondNumber = $this->getSecondNumber();
        }

        if ($_secondNumber == 0) {
            ++$_secondNumber;
            $this->_doDivision($_firstNumber, $_secondNumber);
            return;
        }

        if ($_firstNumber % $_secondNumber != 0) {
            --$_firstNumber;
            --$_secondNumber;

            $this->_doDivision($_firstNumber, $_secondNumber);
            return;
        }

        $this->_setFirstNumber($_firstNumber)
             ->_setSecondNumber($_secondNumber)
             ->_setAnswer($this->getFirstNumber() / $this->getSecondNumber());
    }

	/**
     * Do modulus
     *
     * This method will do a modulus operation between two numbers
     *
     *
     * @access private
     * @see $this->_firstNumber, $this->_secondNumber, $this->_setAnswer()
     *
     */
    private function _doModulus()
    {
       $this->_setAnswer($this->getFirstNumber() % $this->getSecondNumber());
    }

	/**
     * Does a subtract on the values
     *
     * This function executes a subtraction on the firstNumber
     * and the secondNumber to then call $this->_setAnswer to set
     * the answer value.
     *
     * If the firstnumber value is smaller than the secondnumber value
     * then we regenerate the first number and regenerate the operation.
     *
     * @access private
     * @see    $this->_firstNumber, $this->_secondNumber, $this->_setAnswer()
     */
    private function _doSubtract()
    {
         $first  = $this->getFirstNumber();
         $second = $this->getSecondNumber();

        /**
         * Check if firstNumber is smaller than secondNumber
         */
        if ($first < $second) {
            $this->_setFirstNumber($second)
                 ->_setSecondNumber($first);
        }

        $_answer = $this->getFirstNumber() - $this->getSecondNumber();
        $this->_setAnswer($_answer);
    }

	/**
     * Generate the operation
     *
     * This function will call the function necessary
     * depending on which operation is set by this->_operators.
     *
     * @access private
     * @see    $this->_operators
     */
    private function _generate()
    {
        switch ($this->_operators) {
			case '+':
				$this->_doAdd();
				break;
			case '-':
				$this->_doSubtract();
				break;
			case '*':
				$this->_doMultiplication();
				break;
			case '%':
				$this->_doModulus();
				break;
			case '/':
				$this->_doDivision();
				break;
			default:
				$this->_doAdd();
				break;
        }
    }
	
	/**
	 * Validate the users's answer
	 *
	 * @return bool
	 */
	public function __validate() {
		//validate answer against the value the user gave
		$res = $this->getAnswer() == $this->deliver();

		if (!$res) {
			$this->setErrorMessage(
				$this->getDefaultErrorMessageWrongAnswer()
			);
		}

		return $res;
	}

	/**
	 * Get input control view
	 *
	 * This method overrides {@link M_Field::getInputView()}.
	 *
	 * This method provides with an {@link M_ViewFieldCaptchaMath} object,
	 * which allows {@link M_Field} to include the input control
	 * in the view that is returned by {@link M_Field::getView()}.
	 *
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldCaptchaMath
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldCaptchaMath($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}

	/**
	 * Get view
	 *
	 * When the form builds the interface, it will ask each of the
	 * contained fields for a view, to include the field interface
	 * (including input control) in the global form interface.
	 *
	 * This method will construct the {@link M_ViewFieldCaptchaMath} object, of
	 * which the class name has been set previously with the method
	 * {@link M_Field::setView()}. To render the input
	 * control, M_Field relies on {@link M_Field::getInputView()}.
	 *
	 * @access public
	 * @return M_ViewHtml
	 */
	public function getView() {
		//only generate when the captcha is displayed, this to prevent a new
		//captcha is generated upon construct. E.g. when this field would be
		//validated a new captcha will be genrated, resulting in a failed
		//validation (the user has responded to the old question).
		//the only way to prevent this is generating the captcha when a view
		//is displayed
		$this->_generateCaptcha();

		return parent::getView();
	}
}