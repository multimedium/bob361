<?php
class M_ViewForm extends M_ViewCoreHtml {
	/**
	 * All additional buttons
	 * 
	 * @var array
	 */
	private $_buttons = array();

	/**
	 * All additional submitbuttons
	 *
	 * @var array
	 */
	private $_submitButtons = array();

	/**
	 * The label of the submit button
	 * 
	 * @var string
	 */
	private $_submitButtonLabel;

	/**
	 * Construct the form
	 * 
	 * @param M_Form $form
	 */
	public function __construct(M_Form $form) {
		$this->setForm($form);
		$this->setSubmitButtonLabel(t('Send'));
	}

	/**
	 * Set the form so we can use it in this view
	 * 
	 * @param M_Form $form
	 */
	public function setForm(M_Form $form) {
		$this->assign('form', $form);
	}

	/**
	 * Set the label of the submitbutton
	 * @param string $title
	 */
	public function setSubmitButtonLabel($title) {
		$this->_submitButtonLabel = $title;
	}

	/**
	 * Add a button (<a> tag)
	 * @param string $title
	 * @param string $href
	 */
	public function addButton($title, $href) {
		$this->_buttons[] = array(
			'title' => $title,
			'href'  => $href
		);
	}

	/**
	 * Add an additional submitbutton
	 * @param string $title
	 * @param string $value
	 */
	public function addSubmitButton($title, $value) {
		$this->_submitButtons[] = array(
			'title' => $title,
			'value' => $value
		);
	}
	
	/**
	 * Get form
	 *
	 * @return M_Form
	 */
	public function getForm() {
		return $this->getVariable('form', NULL);
	}

	/**
	 * Do some preprocessing
	 *
	 * @return void
	 */
	protected function preProcessing() {
		// Check if a form has been provided to the view:
		if(!$this->getVariable('form', FALSE)) {
			// If not, throw exception
			throw new M_ViewException(sprintf(
				'%s: Missing form object',
				__CLASS__
			));
		}
		
		// Assign buttons:
		$this->assign('submitButtonLabel', $this->_submitButtonLabel);
		$this->assign('buttons', $this->_buttons);
		$this->assign('submitButtons', $this->_submitButtons);
	}

	/**
	 * Get the html for this view
	 * 
	 * @return string
	 */
	protected function getHtml() {
		// get form + info
		$form = $this->getForm();
		$errorMessage = $form->getErrorMessage();
		// Render HTML
		$html  = '';
		if($errorMessage) {
			$html .= '<div class="form-error-message">';
			$html .=    $errorMessage;
			$html .= '</div>';
		}
		$html .= '<form id="' . $this->getId() . '"';
		foreach($form->getProperties() as $name => $value) {
			$html .= ' ' . $name . '="' . $value . '"';
		}
		$html .= '>';
		foreach($form->getVariables() as $name => $value) {
			if (is_array($value)) {
				foreach($value AS $key => $arrayValue) {
					$html .='<input type="hidden" name="' . $name . '['.$key.']" value="' . $arrayValue . '" />';
				}
			}else {
				$html .='<input type="hidden" name="' . $name . '" value="' . $value . '" />';
			}
		}
		
		/* @var $group M_FieldGroup */
		$groups = $form->getFieldGroups();
		$groupFields = array();
		if ($groups->count() >0 ) {
			$groupFields = $form->getGroupFields()->getArrayCopy();
		}
		
		//first print all fields which are not part of a group
		foreach($form->getFields(false) as $name => $field) {
			//check if this field wasn't displayed yet in any group
			if (!array_key_exists($name, $groupFields)) {
				$html .= $field->getView()->fetch();
			}
		}
		
		//now we can print all groups
		foreach($groups AS $group) {
			$html .= $group->getView()->fetch();
		}

		$html .= '<div class="field-row-buttons">';
		$html .= '<input type="submit" name="submit-'.$this->getId().'" value="'. $this->_submitButtonLabel .'" class="field-submit"/>';

		$i = 1;
		foreach($this->_submitButtons as $button) {
			$html .= '<label class="left button-submit">';
			$html .=    '<input name="submit-'.$this->getId().'-'.$i.'" type="submit" value="'.$button['value'].'" />';
			$html .=	'<span>'.$button['title'].'</span>';
			$html .= '</label>';
			$i++;
		}
		foreach($this->_buttons as $button) {
			$html .= '<a href="'. $button['href'] .'" class="button left">';
			$html .=    '<span>';
			$html .=       $button['title'];
			$html .=    '</span>';
			$html .= '</a>';
		}
		$html .= '</div>';
		$html .= '</form>';
		
		// return the final render
		return $html;
	}

	/**
	 * Get a template for this view
	 * 
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/Form.tpl');
	}
}