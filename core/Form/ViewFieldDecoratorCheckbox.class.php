<?php
class M_ViewFieldDecoratorCheckbox extends M_ViewField {
	protected function getHtml() {
		// Get field + info
		$field = $this->getField ();
		$description = $field->getDescription ();
		$errorMessage = $field->getErrorMessage ();
		
		// Render HTML:
		$html = '<div class="field-row" id="container-' . $field->getId () . '">';
		$html .= '<div class="field-container">';
		if ($errorMessage) {
			$html .= '<div class="field-error-message">';
			$html .= $errorMessage;
			$html .= '</div>';
		}
		$html .= $field->getInputView ()->fetch ();
		$html .= '<label for="' . $field->getId() . '" class="checkbox">';
		$html .= $field->getTitle ();
		$html .= '</label>';
		if (! empty ( $description )) {
			$html .= '<div class="field-description">';
			$html .= 	'<div class="small note">';
			$html .=    	  $description;
			$html .=    '</div>';
			$html .= '</div>';
		}
		$html .= '</div>';
		$html .= '<div class="clear"></div>';
		$html .= '</div>';
		
		// return final render:
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner ( 'core-form/FieldDecoratorCheckbox.tpl' );
	}
}