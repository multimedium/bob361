<?php
/**
 * M_FieldUploadImage class
 * 
 * M_FieldUploadImage, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldUpload}
 * 
 * handles the input control that allows users to choose a file 
 * from their computer and upload it to the server. More specifically,
 * this class handles the upload of image files.
 * 
 * @package Core
 */
class M_FieldUploadImage extends M_FieldUpload {
	/**
	 * Copies
	 * 
	 * This property stores the copies that need to be made of the
	 * originally uploaded image. Check the docs on:
	 * 
	 * - {@link M_FieldUploadImage::set()}, and
	 * - {@link M_FieldUploadImage::addCopy()}
	 * 
	 * for more info.
	 * 
	 * @access private
	 * @var array
	 */
	private $_copy = array();
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>copies</code>
	 * 
	 * The copies that need to be made of the uploaded image. The
	 * field may generate various copies, for example: a thumbnail
	 * version, a preview version and the full version of the image.
	 * Each copy definition accepts the following keys:
	 * 
	 * <code>
	 *    directory
	 *    filename
	 *    filters
	 * </code>
	 * 
	 * The filters key is used exactly in the same manner as in the
	 * field; see {@link M_Field::factory()} for more info. A specific 
	 * usage example of the copies key would be as following:
	 * 
	 * <code>
	 *    $upload = M_Field::factory('myField', new M_Config(array(
	 *       'type' => 'UploadImage',
	 *       'extensions' => 'jpg, gif, png',
	 *       'maxSize' => 1048576,
	 *       'directory' => 'uploads/images/originals',
	 *       'filename' => time(),
	 *       // Declare the copies of the upload image:
	 *       'copies' => array(
	 *          // Here we use "thumb" as the key, but the key can be 
	 *          // anything (the key is completely ignored)
	 *          'thumb' => array(
	 *             'directory' => 'uploads/images/thumbnails',
	 *             'filename' => time(),
	 *             'filters' => array(
	 *                // Here we use numeric keys, but the keys can be 
	 *                // anything (the keys are completely ignored)
	 *                0 => array(
	 *                   0 => 'M_FilterImageResize',
	 *                   1 => 200, // width
	 *                   2 => 300  // height
	 *                )
	 *             )
	 *          )
	 *       )
	 *    )));
	 * </code>
	 * 
	 * NOTE:
	 * If you prefer not to mount your field via a definition array,
	 * you can use {@link FieldUploadImage::addCopy()} to define copies
	 * of the uploaded image.
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @see M_FieldUpload::set()
	 * @see FieldUploadImage::addCopy()
	 * @throws M_FieldException
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		switch($spec) {
			case 'copies':
				$c = (is_object($definition) && in_array('MI_Config', class_implements($definition)));
				if(!$c) {
					throw new M_FieldException('MI_Config expected for "copies"');
				}
				
				foreach($definition as $name => $copy) {
					if(!isset($copy->directory)) {
						throw new M_FieldException(sprintf('Missing "directory" in "copies / %s"', $name));
					}
					
					if(!isset($copy->filename)) {
						throw new M_FieldException(sprintf('Missing "filename" in "copies / %s"', $name));
					}
					
					if(!isset($copy->filters)) {
						$this->addCopy($copy->directory, $copy->filename);
					} else {
						$decorator = new M_Decorator;
						foreach($copy->filters as $value) {
							if(is_object($value) && in_array('MI_Config', class_implements($value))) {
								$temp = $value->toNumericKeyArray();
								$decorator->addDecoratorArgs(array_shift($temp), $temp);
							} else {
								$decorator->addDecoratorArgs($value);
							}
						}
						$this->addCopy($copy->directory, $copy->filename, $decorator);
					}
				}
				break;
			
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Add copy
	 * 
	 * Add a copies that needs to be made of the uploaded image. The
	 * field may generate various copies, for example: a thumbnail
	 * version, a preview version, etc.
	 * 
	 * To apply filters on the copy, you can provide with a chain of
	 * filters, contained in an {@link M_Decorator} object.
	 * 
	 * Example 1, add a copy for a grayscale thumbnail version of the 
	 * uploaded image.
	 * 
	 * <code>
	 *    // Create the decorator (chain of filters)
	 *    $decorator = new M_Decorator;
	 *    $decorator->addDecorator('M_FilterImageResize', 200, 300);
	 *    $decorator->addDecorator('M_FilterImageGrayscale');
	 *    
	 *    // Create the upload field
	 *    $field = new M_FieldUploadImage('myField');
	 *    $field->addCopy('uploads/images/thumbs', time(), $decorator);
	 *    // ...
	 * </code>
	 * 
	 * Check the docs on {@link M_FieldUploadImage::set()} to learn 
	 * how to add copies via the form definition array.
	 * 
	 * @access public
	 * @param string $directory
	 * 		The directory in which to save the copy of the uploaded
	 * 		image. Similar to {@link FieldUpload::setDirectory()}.
	 * @param string $filename
	 * 		The (file)name under which to save the copy of the uploaded
	 * 		image. Similar to {@link FieldUpload::setFilename()}.
	 * @param M_Decorator $decorator
	 * 		The filters to be applied. This function expects an 
	 * 		{@link M_Decorator} object that contains the chain of 
	 * 		filters ({@link MI_Filter} objects).
	 * @return void
	 */
	public function addCopy($directory, $filename, M_Decorator $decorator = NULL) {
		// We set the shared interface that should be implemented 
		// by the filters in the decorator's contained chain:
		// (an exception will be thrown if a filter does not implement
		// this interface)
		if($decorator != NULL) {
			$decorator->setInterface('MI_Filter');
		}
		
		// Prepare the directory:
		$directory = rtrim($directory, " \t\n\r\0\x0B\\/" . DIRECTORY_SEPARATOR);
		
		// Now, we add the copy to the field:
		$this->_copy[] = array($directory, $filename, $decorator);
	}
	
	/**
	 * Field-type-specific validation
	 * 
	 * This method overrides {@link M_Field::__validate()}.
	 * 
	 * This specific implementation will create the copies of the
	 * uploaded image, if the uploaded file has been validated correctly
	 * (see {@link FieldUpload::__validate()}). To add definitions of
	 * copies, use {@link M_FieldUploadImage::addCopy()}.
	 * 
	 * @access public
	 * @see M_Field::__validate()
	 * @see M_FieldUpload::__validate()
	 * @see M_FieldUploadImage::addCopy()
	 * @param string $value
	 * 		The temporary value of the field. This value is 
	 * 		being provided by {@link M_FieldUpload::getValue()}
	 * @return boolean
	 */
	public function __validate($value) {
		// If
		// - a file has been uploaded
		// - the upload has validated correctly, and 
		// - the file has been saved successfully:
		if(parent::__validate($value)) {
			if($this->_isUploaded) {
				// Check if copies should be made:
				$n = count($this->_copy);
				if($n > 0) {
					// We construct an M_File object out of the uploaded file:
					$file = new M_File($this->_uploaded);
					
					// For each of the copies:
					for($i = 0; $i < $n; $i ++) {
						// - Compose the full path to the copy
						//   (without extension)
						$target = $this->_copy[$i][0] . DIRECTORY_SEPARATOR . $this->_copy[$i][1];
						
						// - Apply the decorator (chain of filter objects) 
						//   to the image
						if($this->_copy[$i][2]) {
							// We apply the filters on the image:
							$this->_copy[$i][2]->setDecorated(new M_FilterImage($file->getPath()));
							$rs = $this->_copy[$i][2]->getChain()->apply();
							
							// And save the image:
							$rs->saveToFile($target, $file->getFileExtension());
						}
						// If no decorator has been provided for the 
						// copy, we simply copy the original
						elseif(!@copy($file->getPath(), $target . '.' . $file->getFileExtension())) {
							// If the file could not have been copied to its
							// target location, throw exception:
							throw new M_FieldException(sprintf('Could not create copy in %s', $this->_copy[$i][0]));
						}
					}
				}
			}
			
			// return success
			return TRUE;
		}
		// If not validated by superclass, return failure:
		else {
			return FALSE;
		}
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}. 
	 * 
	 * For now, this method simply returns the result of the parent
	 * {@link FieldUpload::getInputView()} method.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see FieldUpload::getInputView()
	 * @see M_ViewFieldUpload
	 * @return M_View
	 */
	public function getInputView() {
		return parent::getInputView();
	}
}
?>