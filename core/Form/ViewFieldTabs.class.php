<?php
/**
 * M_ViewFieldTabs
 *
 * ... Renders the default view of {@link M_FieldTabs}
 *
 * @package Core
 */
class M_ViewFieldTabs extends M_ViewField {
	/**
	 * Get HTML
	 *
	 * @access protected
	 * @return string $html
	 *		The HTML Source Code that can be used to display the field
	 */
	protected function getHtml() {
		return 'M_FieldTabs';
	}

	/**
	 * Get resource/template
	 *
	 * @access protected
	 * @return M_ViewHtmlResource $resource
	 *		The template resource that is used to render the HTML Source code
	 *		of this view. May be non-existing, in which case the HTML is rendered
	 *		by {@link M_ViewFieldTabs::getHtml()}. If existing though, the template
	 *		that is returned by this method will eventually overwrite the results
	 *		of {@link M_ViewFieldTabs::getHtml()}
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldTabs.tpl');
	}
}