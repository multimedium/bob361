<?php
class M_ViewFieldSelect extends M_ViewField {
	protected function getHtml() {
		// Get field + info
		/* @var $field M_FieldSelect */
		$field = $this->getField();
		$fieldValue = $field->getValue();
		// Render the HTML
		$html = '<select class="field-select" size="1" name="'. ($field->getName() ?: $field->getId()) .'" id="'. $this->getId() . '"';
		if($field->getDisabled()) {
			$html .= ' disabled="disabled"';
		}
		$html.='>';
		
		//render optgroups
		foreach($field->getGroups() AS $group) {
			$html .= sprintf('<optgroup label="%s">', $group[0]);
			foreach( $group[1] AS $value => $label) {
				$html .= $this->_getOption($value, $label, $fieldValue);
			}
			$html .= '</optgroup>';
		}
		
		//render options
		foreach($field->getItems() as $value => $label) {
			$html .= $this->_getOption($value, $label, $fieldValue);
		}
		$html .= '</select>';
		
		// Return final render
		return $html;
	}
	
	/**
	 * Get html for an option
	 *
	 * @param mixed $value
	 * @param string $label
	 * @param string $fieldValue
	 * @return string
	 */
	private function _getOption($value, $label, $fieldValue) {
        $filter = new M_FilterTextHtmlEntities(new M_FilterTextValue((string)$label));
		$label = $filter->apply();
		
		return sprintf(
			'<option value="%s"%s>%s</option>', 
			$value, 
			((! is_null($fieldValue) && $value == $fieldValue) ? ' selected="selected"' : ''), 
			$label
		);
	}
	
	/**
	 * Get resource
	 *
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldSelect.tpl');
	}
}