<?php
/**
 * M_FieldNumeric class
 * 
 * M_FieldNumeric, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldText}
 * 
 * handles the input control that allows users to introduce a 
 * number.
 * 
 * @package Core
 */
class M_FieldNumeric extends M_FieldText {
	
	/**
	 * HTML5 types
	 */
	const HTML5_TYPE_NUMBER = 'number';
	const HTML5_TYPE_SLIDER = 'slider';

	/**
	 * The numeric validator
	 * 
	 * @var M_ValidatorIsNumeric
	 */
	private $_validator;
	
	/**
	 * Format the number
	 * 
	 * @var bool
	 */
	private $_format = true;
	
	/**
	 * Format Label
	 * 
	 * @see M_FieldNumeric::setFormatLabel()
	 * @access private
	 * @var string
	 */
	private $_formatLabel;
	
	/**
	 * Max value
	 * 
	 * @access private
	 * @var float|int
	 */
	private $_max;
	
	/**
	 * Min value
	 * 
	 * @access private
	 * @var float|int
	 */
	private $_min;
	
	/**
	 * Step
	 * 
	 * @access private
	 * @var float|int
	 */
	private $_step;
	
	/**
	 * HTML5 type
	 * 
	 * @access private
	 * @var string
	 */
	private $_html5type = self::HTML5_TYPE_NUMBER;

    /**
     * Has deliver() been ran?
     *
     * @see M_FieldNumeric::deliver()
     * @access private
     * @var bool
     */
    private $_deliverDone = FALSE;

    /**
     * Stores the deliver() value locally
     *
     * @see M_FieldNumeric::deliver()
     * @access private
     * @var float
     */
    private $_deliver;

	/**
	 * Construct
	 *
	 * @param string $id
	 */
	public function __construct($id) {
		parent::__construct($id);

		$this->_validator = new M_ValidatorIsNumeric();
		
		//add the validator
		$this->addValidatorObject(
			$this->_validator,
			t('Please introduce a valid number')
		);
	}

	/**
	 * Set the minimum value
	 * 
	 * @param float|int $arg
	 * @return M_FieldNumeric
	 */
	public function setMin($arg, $errorMessage = null) {
		$this->_validator->setCriteria(M_ValidatorIsNumeric::MIN, $arg, $errorMessage);
		$this->_min = $arg;
		return $this;
	}

	/**
	 * Set the maximum value
	 * 
	 * @param float|int $arg
	 * @return M_FieldNumeric
	 */
	public function setMax($arg, $errorMessage = null) {
		$this->_validator->setCriteria(M_ValidatorIsNumeric::MAX, $arg, $errorMessage);
		$this->_max = $arg;
		return $this;
	}
	
	/**
	 * Set the steps value
	 * 
	 * this can be used for the creation of an HTML5 field with spinner
	 * 
	 * @param float|int $arg
	 * @return M_FieldNumeric
	 */
	public function setStep($arg) {
		$this->_step = $arg;
		return $this;
	}
	
	/**
	 * Set the field type
	 * 
	 * @param string $arg  use one of the provided constants (HTML5_TYPE_...)
	 * @return M_FieldNumeric
	 */
	public function setHtml5Type($arg) {
		$this->_html5type = $arg;
		return $this;
	}

	/**
	 * Set the default value
	 *
	 * @param string $value
	 */
	public function setDefaultValue($value) {
		//format the number
		$number = new M_Number($value);
		
		//set the value
		parent::setDefaultValue(
			$this->_format ? $number->toString() : $number->getNumber()
		);
	}
	
	/**
	 * Enable/disable formatting
	 * 
	 * @param bool $arg
	 * @return M_FieldNumeric
	 */
	public function setFormat($arg) {
		$this->_format = (bool)$arg;
		return $this;
	}
	
	/**
	 * Set Format Label
	 * 
	 * Allows for adding a display label for the format the number is supposed
	 * to have, like "kg" or "mm".
	 * 
	 * NOTE: as of currently, this label is not featured in the view of
	 * this field. It is purely meant as an extra option in case you are
	 * using a custom template for this field.
	 * 
	 * @author Tim Segers
	 * @access public
	 * @param string $label
	 * @return M_FieldNumeric
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFormatLabel($label) {
		$this->_formatLabel = (string) $label;
		return $this;
	}
	
	/**
	 * Get Format Label
	 * 
	 * @see M_FieldNumeric::setFormatLabel()
	 * @access public
	 * @return string
	 */
	public function getFormatLabel() {
		return $this->_formatLabel;
	}
	
	/**
	 * Get Max
	 * 
	 * @access public
	 * @return int|float|null
	 */
	public function getMax() {
		return $this->_max;
	}
	
	/**
	 * Get Min
	 * 
	 * @access public
	 * @return int|float|null
	 */
	public function getMin() {
		return $this->_min;
	}
	
	/**
	 * Get Steps
	 * 
	 * @access public
	 * @return int|float|null
	 */
	public function getStep() {
		return $this->_step;
	}
	
	/**
	 * Get HTML5 Type
	 * 
	 * @access public
	 * @return string
	 */
	public function getHtml5Type() {
		return $this->_html5type;
	}

	/**
	 * @access public
	 * @throws M_FieldException
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($key, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($key) {
			// Set the minimum value
			case 'min':
				$this->setMin($definition);
				break;
			// Set the maximum value
			case 'max':
				$this->setMax($definition);
				break;
			// Set the steps value
			case 'step':
				$this->setStep($definition);
				break;
			// Set the HTML5 type
			case 'html5type':
				$this->setHtml5Type($definition);
				break;
			case 'format':
				$this->setFormat($definition);
				break;
			case 'formatLabel':
				$this->setFormatLabel($definition);
				break;
			default:
				 parent::set($key, $definition);
		}
	}

	/**
	 * Deliver final value
	 *
     * NOTE: we implement local caching, as {@link M_Number::constructWithString}
     * is very slow
     *
     * @access public
	 * @return float
	 */
	public function deliver() {
        // If deliver was not done before (note that we store this separately,
        // to still allow NULL values as deliver)
        if($this->_deliverDone === FALSE) {
            try {
                // Convert to valid float based on string and locale
                $this->_deliver = M_Number::constructWithString(parent::deliver())->getNumber();
            }
            // Unable to create a number
            catch(M_Exception $e) {
                $this->_deliver = NULL;
            }

            // Store that deliver has been done
            $this->_deliverDone = TRUE;
        }

        // Return the deliver value
        return $this->_deliver;
	}

	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldNumeric} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldNumeric
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldNumeric($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}