<?php
class M_ViewFieldUpload extends M_ViewField {
	public function setUploadedFilePreview(M_ViewHtml $view) {
		$this->assign('preview', $view->fetch());
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	protected function getHtml() {
		$preview = $this->getVariable('preview');
		$field = $this->getField();
		
		// Start render
		$html  = '';
		
		// If file preview is available
		if($preview && ! $field->getErrorMessage()) {
			$html .= $preview;
		}
		
		// Add upload field:
		$html .= '<input type="file" name="'. $field->getId() .'" id="'. $this->getId() .'" />';
		
		// If a file is already available:
		$value = $field->getValue();
		if($value) {
			$html .= '<input type="hidden" name="'. $field->getId() .'_hidden" value="'. $value .'" />';
		}

		// If a original filename is already available:
		$filenameOriginal = $field->getFilenameOriginal();
		if($filenameOriginal) {
			$html .= '<input type="hidden" name="'. $field->getId() .'_filenameOriginal_hidden" value="'. $filenameOriginal .'" />';
		}

		// return final render:
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldUpload.tpl');
	}
}
?>