<?php
/**
 * M_ViewFieldDateTime class
 * 
 * Used by the class {@link M_FieldDateTime}, in order to render the view of the
 * input control.
 * 
 * @package Core
 * @author Ben Brughmans
 */
class M_ViewFieldDateTime extends M_ViewField {
	
	/**
	 * Default HTML source code rendering
	 * 
	 * This method overrides {@link M_ViewCoreHtml::getHtml()}. All 
	 * subclasses of {@link M_ViewCoreHtml} must implement this method, 
	 * to provide with default HTML rendering.
	 * 
	 * @access protected
	 * @return string
	 */
	protected function getHtml() {
		// Get the field that is being rendered for display:
		$field = $this->getField();
		/* @var $field M_FieldDateTime */
		$out = '';

		// Error message
		// (if the field does not have a general error message)
		if(! $field->getErrorMessage()) {
			// Then, we show the first error message we can find in one of the
			// contained fields:
			$out .= $this->_getErrorMessage();
		}

		// Date field
		$out .=	$this->_getFieldView($field->getFieldDate());
		$out .= '<div class="field-datetime-container-time">';
		$out .=		$this->_getFieldView($field->getFieldHours());
		$out .=		'<div class="field-datetime-container">:</div>';
		$out .=		$this->_getFieldView($field->getFieldMinutes());
		$out .=	'</div>';
		
		// Return the HTML Source code:
		return $out;
	}

	/**
	 * Get the view of a field
	 * 
	 * @param M_Field $field
	 * @return string
	 */
	private function _getFieldView(M_Field $field) {
		$out = '<div class="field-datetime-container">';
		
		//add the title
		if ($field->getTitle()) {
			$out .= '<label for="'.$field->getId().'">'.$field->getTitle().'</label>';
		}

		//add the field itself
		$out .= $field->getInputView()->fetch();

		//add the description
		if($field->getDescription()) {
			$out .=    '<div class="field-description">';
			$out .= 		'<div class="small note">';
			$out .=				$field->getDescription();
			$out .=			'</div>';
			$out .=    '</div>';
		}

		$out .= '</div>';

		return $out;
	}
	
	/**
	 * Get (general) error message
	 *
	 * @access private
	 * @return string
	 */
	private function _getErrorMessage() {
		// Create a collection of getters. Each of the getters will be used to
		// retrieve a field, and check if an error message exists for that field.
		$fields = array(
			'getFieldDate',
			'getFieldHours',
			'getFieldMinutes'
		);

		// For each of the field getters:
		foreach($fields as $getter) {
			// Get the field:
			$field = $this->getField()->$getter();

			// Check if an error message exists for the field
			/* @var $field M_Field */
			$error = $field->getErrorMessage();
			if($error) {
				// If so, then we render the error message for display
				$out  = '<div class="error-message">';
				$out .=    $error;
				$out .= '</div>';
				return $out;
			}
		}

		// If still here, there is no error
		return '';
	}

	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldDateTime.tpl');
	}
}