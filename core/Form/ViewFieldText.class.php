<?php
class M_ViewFieldText extends M_ViewField {
	protected function getHtml() {
		$field = $this->getField();
		$html = '<input type="text" name="'. ($field->getName() ?: $field->getId()) .'"' 
				. ' id="'. $this->getId() .'"' 
				. ' value="'. $field->getValue() .'"' 
				. ' class="';
		$html .=		'field-text';
		if($field->getReadonly()) {
			$html .=	' readonly';
		}
		$html .=		'"';
		
		if($field->getWidth()) {
			$html .= ' style="width: '. $field->getWidth() .'px;"';
		}
		if($field->getMaxLength()) {
			$html .= ' maxlength="'. $field->getMaxLength() .'"';
		}
		if(!is_null($field->getHint())) {
			$text = new M_Text($field->getHint());
			$html .= ' title="'. $text->toHtmlAttribute() .'"';
		}
		if($field->getReadonly()) {
			$html .= ' readonly="readonly"';
		}
		if($field->getDisabled()) {
			$html .= ' disabled="disabled"';
		}
		$html .= ' />';
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldText.tpl');
	}
}