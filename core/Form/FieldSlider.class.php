<?php
/**
 * M_FieldSlider class
 * 
 * M_FieldSlider, a subclass of {@link M_Field}, handles the input 
 * control that allow users to introduce a numerical value through
 * a slider interface
 * 
 * @package Core
 */
class M_FieldSlider extends M_Field {
	/**
	 * The lowest value
	 * 
	 * @access private
	 * @var integer
	 */
	private $_minValue  = 0;
	
	/**
	 * The highest value
	 * 
	 * The highest value of the slider is defaulted to 100.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_maxValue  = 100;
	
	/**
	 * The number of steps between lowest and highest value
	 * 
	 * @access private
	 * @see M_FieldSlider::$_minValue
	 * @see M_FieldSlider::$_maxValue
	 * @var integer
	 */
	private $_numberOfSteps = 101;
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>min</code>
	 * <code>lowest</code>
	 * 
	 * Sets the lowest value in the slider. Read more info at
	 * {@link M_FieldSlider::setMinValue()}
	 * 
	 * <code>max</code>
	 * <code>highest</code>
	 * 
	 * Sets the highest value in the slider. Read more info at
	 * {@link M_FieldSlider::setMaxValue()}
	 * 
	 * <code>steps</code>
	 * 
	 * Sets the number of steps between lowest and highest value
	 * in the slider. Read {@link M_FieldSlider::setNumberOfSteps()}
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		switch($spec) {
			case 'min':
			case 'lowest':
				$this->setMinValue($definition);
				break;
			
			case 'max':
			case 'highest':
				$this->setMaxValue($definition);
				break;
			
			case 'steps':
				$this->setNumberOfSteps((int) $definition);
				break;
			
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Sets the lowest value
	 * 
	 * Note that you cannot set the lowest value to a number that
	 * is greater than (or equal to) the highest value. In that case,
	 * this method will throw a {@link M_FieldException}.
	 * 
	 * @access public
	 * @param integer $number
	 * 		The lowest value
	 * @return void
	 */
	public function setMinValue($number) {
		if($number < $this->_maxValue) {
			$this->_minValue = $number;
		} else {
			throw new M_FieldException(
				'The lowest value of the slider cannot be greater than (or equal to) the highest.'
			);
		}
	}
	
	/**
	 * Sets the highest value
	 * 
	 * Note that you cannot set the highest value to a number that
	 * is less than (or equal to) the lowest value. In that case,
	 * this method will throw a {@link M_FieldException}.
	 * 
	 * @access public
	 * @param integer $number
	 * 		The lowest value
	 * @return void
	 */
	public function setMaxValue($number) {
		if($number > $this->_minValue) {
			$this->_maxValue = $number;
		} else {
			throw new M_FieldException(
				'The highest value of the slider cannot be less than (or equal to) the lowest.'
			);
		}
	}
	
	/**
	 * Sets the number of steps between lowest and highest value
	 * 
	 * @access public
	 * @param integer $number
	 * 		The lowest value
	 * @return void
	 */
	public function setNumberOfSteps($number) {
		$this->_numberOfSteps = $number;
	}
	
	/**
	 * Get steps
	 * 
	 * This method will calculate the numerical value of each step
	 * that has been defined with the following values:
	 * 
	 * - {@link M_FieldSlider::setMinValue()}
	 * - {@link M_FieldSlider::setMaxValue()}
	 * - {@link M_FieldSlider::setNumberOfSteps()}
	 * 
	 * Assuming we have set the lowest value to 0, the highest
	 * value to 10, and we have set 5 steps in the slider, this
	 * method would return the following array:
	 * 
	 * <code>
	 *    Array(
	 *       0 => 0,
	 *       1 => 2.5,
	 *       2 => 5,
	 *       3 => 7.5,
	 *       4 => 10
	 *    )
	 * </code>
	 * 
	 * Assuming we have set the lowest value to 10, the highest
	 * value to 20, and we have set 11 steps in the slider, this
	 * method would return the following array:
	 * 
	 * <code>
	 *    Array(
	 *       0  => 10,
	 *       1  => 11,
	 *       2  => 12,
	 *       3  => 13,
	 *       4  => 14,
	 *       5  => 15,
	 *       6  => 16,
	 *       7  => 17,
	 *       8  => 18,
	 *       9  => 19,
	 *       10 => 20
	 *    )
	 * </code>
	 * 
	 * Typically, this method is used internally by M_FieldSlider to
	 * validate the user-supplied value.
	 * 
	 * @access public
	 * @return array
	 */
	public function getSteps() {
		$output = array();
		$interval = ($this->_maxValue - $this->_minValue) / ($this->_numberOfSteps - 1);
		for($i = $this->_minValue; $i <= $this->_maxValue; $i += $interval) {
			array_push($output, $i);
		}
		return $output;
	}
	
	/**
	 * Get the temporary value
	 * 
	 * This method overrides {@link M_Field::getValue()}.
	 * 
	 * M_FieldSlider makes sure that the temporary value is one of 
	 * the values that is returned by {@link M_FieldSlider::getSteps()}.
	 * If the temporary value is not one of those values, the
	 * temporary value will be set to the lowest value that has been
	 * provided with {@link M_FieldSlider::setMinValue()}.
	 * 
	 * @access public
	 * @see M_Field::getValue()
	 * @return array|boolean
	 */
	public function getValue() {
		$value = parent::getValue();
		if(in_array($value, $this->getSteps())) {
			return $value;
		} else {
			return $this->_minValue;
		}
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldSlider} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldSlider
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldSlider;
		$view->assign('name', $this->getId());
		$view->assign('value', $this->getValue());
		$view->assign('min', $this->_minValue);
		$view->assign('max', $this->_maxValue);
		$view->assign('numberOfSteps', $this->_numberOfSteps);
		$view->assign('steps', $this->getSteps());
		return $view;
	}
}
?>