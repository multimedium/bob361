<?php
class M_ViewFieldCheckbox extends M_ViewField {
	protected function getHtml() {
		$field = $this->getField();
		return '<input type="checkbox"'
			. ' name="'. $field->getId() .'"'
			. ' id="'. $field->getId() .'"'
			. ' class="field-checkbox"'
			. ' value="1"'
			. ($field->getValue() == 1 ? ' checked="checked"' : '') 
			. ($field->getDisabled() ? ' disabled="disabled"' : '')
			. ' />';
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldCheckbox.tpl');
	}
}