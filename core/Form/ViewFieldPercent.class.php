<?php
class M_ViewFieldPercent extends M_ViewField {
	protected function getHtml() {
		$field = $this->getField();
		$class = array(
			'field-text',
			'field-percent',
		);
		//add extra class when autoformat is enabled
		if ($field->isAutoFormat()) {
			$class[] ='autoformat';
		}
		
		/* @var $field M_FieldPercent */
		$field = $this->getField();
		$html = '<input type="text"'
			. ' name="'. $field->getId() .'"'
			. ' id="'. $this->getId() .'"'
			. ' class="'.implode(' ', $class).'"';
		if ($field->getReadonly()) {
			$html .= ' readonly="readonly"';
		}
		if($field->getWidth()) {
			$html .= ' style="width: '. $field->getWidth() .'px;"';
		}

		$html .= ' value="'.$field->getValue().'" />';
		
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldPercent.tpl');
	}
}