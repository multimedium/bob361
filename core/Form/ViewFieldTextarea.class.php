<?php
class M_ViewFieldTextarea extends M_ViewField {
    /**
     * Get html to display
     * 
     * @return string
     */
    protected function getHtml() {
		$field = $this->getField();
		$input = '<textarea name="'. $field->getId() .'"' 
			. ' class="field-text-multiline"' 
			. ' id="'. $this->getId() .'"' 
			. ' rows="'. $field->getRows() .'"' 
			. ' cols="'. $field->getCols() .'"' 
			. ' style="width: '. $field->getWidth() .'px; height: '. $field->getHeight() .'px"';
		//add a hint?
		if ($field->getHint()):
				$text = new M_Text($field->getHint());
			$input .= ' title="'.$text->toHtmlAttribute().'"';
		endif;
		if($field->getDisabled()) {
			$input .= ' disabled="disabled"';
		}
		$input .= '>' 
			. $field->getValue();
		
		
		$input .= '</textarea>';
		
		return $input;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldTextarea.tpl');
	}
}
?>