<?php
/**
 * M_ViewFieldTime class
 *
 * Used by the class {@link M_FieldTime}, in order to render the view of the
 * input control.
 *
 * @package Core
 * @author Ben Brughmans
 */
class M_ViewFieldTime extends M_ViewField {

	/**
	 * Default HTML source code rendering
	 *
	 * This method overrides {@link M_ViewCoreHtml::getHtml()}. All
	 * subclasses of {@link M_ViewCoreHtml} must implement this method,
	 * to provide with default HTML rendering.
	 *
	 * @access protected
	 * @return string
	 */
	protected function getHtml() {
		// Get the field that is being rendered for display:
		/* @var $field M_FieldTime */
		$field = $this->getField();
		$out = '';

		// Error message
		// (if the field does not have a general error message)
		if(! $field->getErrorMessage()) {
			// Then, we show the first error message we can find in one of the
			// contained fields:
			$out .= $this->_getErrorMessage();
		}

		// Container
		$out .= '<div class="field-time-container">';
		$out .=		$field->getFieldHours()->getInputView();
		$out .=	'</div>';
		$out .=	'<div class="field-time-container field-time-container-split">:</div>';
		$out .= '<div class="field-time-container">';
		$out .=		$field->getFieldMinutes()->getInputView();
		$out .=	'</div>';

		if($field->getShowSeconds()) :
		$out .=	'<div class="field-time-container field-time-container-split">:</div>';
		$out .= '<div class="field-time-container">';
		$out .=		$field->getFieldSeconds()->getInputView();
		$out .=	'</div>';
		endif;
		
		// Return the HTML Source code:
		return $out;
	}

	/**
	 * Get (general) error message
	 *
	 * @access private
	 * @return string
	 */
	private function _getErrorMessage() {
		// Create a collection of getters. Each of the getters will be used to
		// retrieve a field, and check if an error message exists for that field.
		$fields = array(
			'getFieldHours',
			'getFieldMinutes',
			'getFieldSeconds',
		);

		// For each of the field getters:
		foreach($fields as $getter) {
			// Get the field:
			$field = $this->getField()->$getter();

			// Check if an error message exists for the field
			/* @var $field M_Field */
			$error = $field->getErrorMessage();
			if($error) {
				// If so, then we render the error message for display
				$out  = '<div class="error-message">';
				$out .=    $error;
				$out .= '</div>';
				return $out;
			}
		}

		// If still here, there is no error
		return '';
	}

	/**
	 * Get template
	 *
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldTime.tpl');
	}
}