<?php
/**
 * M_FieldTime class
 * 
 * M_FieldTime, a subclass of {@link M_Field}, handles the input 
 * control that allows users to introduce a time. A time consists
 * of:
 * 
 * - hour
 * - minutes
 * - seconds
 * 
 * Typically, this specific field will generate an input control
 * that enables users to introduce the time through a visual
 * clock interface.
 * 
 * NOTE:
 * M_FieldTime works tightly with the following classes, in order
 * to present the time interface according to the user's cultural
 * conventions:
 * 
 * - {@link M_Locale}
 * - {@link M_LocaleData}
 * 
 * @package Core
 */
class M_FieldTime extends M_Field {
	
	/**
	 * @var M_FieldNumeric
	 */
	private $_fieldHour;

	/**
	 * @var M_FieldNumeric
	 */
	private $_fieldMinutes;

	/**
	 * @var M_FieldNumeric
	 */
	private $_fieldSeconds;

	/**
	 * @var bool
	 */
	private $_showSeconds = true;

	/**
	 * Get the hour field
	 * 
	 * @return M_FieldNumeric
	 */
	public function getFieldHours() {
		if(!$this->_fieldHour) {
			$fieldHour = new M_FieldNumeric($this->getId().'-hours');
			$validator = new M_ValidatorIsNumeric();
			$validator->setCriteria(M_ValidatorIsNumeric::MIN, 0);
			$validator->setCriteria(M_ValidatorIsNumeric::MAX, 24);
			$fieldHour->addValidatorObject($validator, t('Please enter a valid hour'));
			$fieldHour->setMaxlength(2);
			$fieldHour->setWidth(30);
			$fieldHour->setHint('hh');
			$this->_fieldHour = $fieldHour;
		}
		return $this->_fieldHour;
	}

	/**
	 * Get the minutes field
	 *
	 * @return M_FieldNumeric
	 */
	public function getFieldMinutes() {
		if(!$this->_fieldMinutes) {
			$fieldMinutes = new M_FieldNumeric($this->getId().'-minutes');
			$validator = new M_ValidatorIsNumeric();
			$validator->setCriteria(M_ValidatorIsNumeric::MIN, 0);
			$validator->setCriteria(M_ValidatorIsNumeric::MAX, 60);
			$fieldMinutes->addValidatorObject($validator, t('Please enter a valid minute'));
			$fieldMinutes->setMaxlength(2);
			$fieldMinutes->setWidth(30);
			$fieldMinutes->setHint('mm');
			$this->_fieldMinutes = $fieldMinutes;
		}
		return $this->_fieldMinutes;
	}

	/**
	 * Get the seconds field
	 *
	 * @return M_FieldNumeric
	 */
	public function getFieldSeconds() {
		if(!$this->_fieldSeconds) {
			$fieldSeconds = new M_FieldNumeric($this->getId().'-seconds');
			$validator = new M_ValidatorIsNumeric();
			$validator->setCriteria(M_ValidatorIsNumeric::MIN, 0);
			$validator->setCriteria(M_ValidatorIsNumeric::MAX, 60);
			$fieldSeconds->addValidatorObject($validator, t('Please enter a valid second'));
			$fieldSeconds->setMaxlength(2);
			$fieldSeconds->setWidth(30);
			$fieldSeconds->setHint('ss');
			$this->_fieldSeconds = $fieldSeconds;
		}
		return $this->_fieldSeconds;
	}

	/**
	 * Deliver the time in hh:mm:ss format
	 *
	 * @return string
	 */
	public function deliver() {
		$hour = $this->_fieldHour->deliver();
		$minutes = $this->_fieldMinutes->deliver();
		$seconds = '';
		if($this->getShowSeconds()) $seconds = $this->_fieldSeconds->deliver();

		//all empty? null value
		if(empty($hour) && empty($minutes) && empty($seconds)) return null;

		$date = new M_Date();
		$date->setHour($hour);
		$date->setMinutes($minutes);
		$date->setSeconds($seconds);

		return $date->toString(M_Date::FULL_TIME);
	}

	/**
	 * Validate the fields
	 * 
	 * @return bool
	 */
	public function __validate() {
		$result = true;
		if(!$this->getFieldHours()->validate()) {
			$this->setErrorMessage($this->getFieldHours()->getErrorMessage());
			return false;
		}
		if(!$this->getFieldMinutes()->validate()) {
			$this->setErrorMessage($this->getFieldMinutes()->getErrorMessage());
			return false;
		}
		if($this->getShowSeconds() && !$this->getFieldSeconds()->validate()) {
			$this->setErrorMessage($this->getFieldSeconds()->getErrorMessage());
			return false;
		}

		return true;
	}

	/**
	 * Get value
	 *
	 * @return $value
	 */
	public function getValue() {
		$out = $this->getFieldHours()->getValue();
		$out .= ':' . $this->getFieldMinutes()->getValue();
		
		if($this->getShowSeconds()) {
			$out .= ':' . $this->getFieldSeconds()->getValue();
		}

		return $out;
	}

	/**
	 * Show the seconds?
	 * 
	 * @return bool
	 */
	public function getShowSeconds() {
		return (bool)$this->_showSeconds;
	}

	/* -- SETTERS -- */

	/**
	 * Set the hour field
	 *
	 * @param M_Field $field
	 * @return M_FieldTime
	 */
	public function setFieldHour(M_Field $field) {
		$this->_fieldHour = $field;
		return $this;
	}

	/**
	 * Set the minutes field
	 *
	 * @param M_Field $field
	 * @return M_FieldTime
	 */
	public function setFieldMinutes(M_Field $field) {
		$this->_fieldMinutes = $field;
		return $this;
	}

	/**
	 * Set the seconds field
	 *
	 * @param M_Field $field
	 * @return M_FieldTime
	 */
	public function setFieldSeconds(M_Field $field) {
		$this->_fieldSeconds = $field;
		return $this;
	}

	/**
	 * Set the module owner for this field and all nested fields too
	 *
	 * M_FieldTime is a field which holds other fields: hour, minutes, seconds.
	 * We need to set the module-owner for all of these fields
	 *
	 * @param string $moduleOwner
	 */
	public function setModuleOwner($moduleOwner) {
		parent::setModuleOwner($moduleOwner);
		$this->getFieldHours()->setModuleOwner($moduleOwner);
		$this->getFieldMinutes()->setModuleOwner($moduleOwner);
		$this->getFieldSeconds()->setModuleOwner($moduleOwner);
	}

	/**
	 * Show the seconds?
	 *
	 * @param bool
	 * @return M_FieldTime
	 */
	public function setShowSeconds($arg) {
		$this->_showSeconds = (bool)$arg;
		return $this;
	}

	/**
	 * Set default value
	 *
	 * @param string $time String in format hh:mm:ss
	 * @return M_FieldTime
	 */
	public function setDefaultValue($time) {
		
		//leave empty if null
		if(is_null($time)) return $this;

		$timeParts = explode(':', $time);
		if(count($timeParts) != 3) {
			throw new M_Exception(sprintf(
				'%s::%s expects a value in format hh:mm:ss. Given value was %s',
				__CLASS__,
				__FUNCTION__,
				$time)
			);
		}

		$this->getFieldHours()->setDefaultValue((int)$timeParts[0]);
		$this->getFieldMinutes()->setDefaultValue((int)$timeParts[1]);
		$this->getFieldSeconds()->setDefaultValue((int)$timeParts[2]);

		return $this;
	}


	/**
	 * Add field definition
	 *
	 * This method is used by {@link M_Field::factory()}, to mount
	 * the field with the given form definition. Check out Example 1
	 * to see how exactly this method is being used by
	 * factory():
	 *
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			case 'showSeconds':
				$this->setShowSeconds(M_Helper::isBooleanTrue($definition));
				break;
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}

	/**
	 * Get input control view
	 *
	 * This method overrides {@link M_Field::getInputView()}.
	 *
	 * This method provides with an {@link M_ViewFieldTime} object,
	 * which allows {@link M_Field} to include the input control
	 * in the view that is returned by {@link M_Field::getView()}.
	 *
	 * @access public
	 * @see M_Field::getView()
	 * @return M_View
	 */
	public function getInputView() {
	    $view = new M_ViewFieldTime($this);
		$view->setId($this->getId());
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}