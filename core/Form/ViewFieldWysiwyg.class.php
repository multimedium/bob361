<?php
class M_ViewFieldWysiwyg extends M_ViewFieldTextarea {
	public function getHtml() {
		$html = '<textarea id="'.$this->getId().'" name="'.$this->getId().'" rows="'.$this->getField()->getRows().'" cols="'.$this->getField()->getCols().'" class="field-text field-multiline wysiwyg" style="width: '.$this->getField()->getWidth().'px; height: '.$this->getField()->getHeight().'px">';
		$html .= $this->getField()->getValue();
		$html .= '</textarea>';
		
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldWysiwyg.tpl');
	}
}