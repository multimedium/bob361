<?php
/**
 * M_FormException class
 * 
 * Exceptions thrown by {@link M_Form} and subclasses.
 *
 * @package Core
 */
class M_FormException extends M_Exception {
}
?>