<?php
class M_ViewFieldDecoratorCaptchaQuestion extends M_ViewField {
	protected function getHtml() {
		// Get field + info
		$field = $this->getField ();
		$description = $field->getDescription ();
		$errorMessage = $field->getErrorMessage ();

			// Render HTML:
		$html  = '<div class="field-row" id="container-' . $field->getId() . '">';
		$html .=    '<label for="'. $this->getId() .'">';
		$html .=       $field->getTitle() ? $field->getTitle() : $field->getActiveQuestion();
		$html .=    '</label>';

		$html .=    '<div class="field-container">';
		if($errorMessage) {
			$html .= '<div class="error-message">';
			$html .=    $errorMessage;
			$html .= '</div>';
			$html .= '<div class="clear"></div>';
		}

		if($field->getTitle()) {
			$html .=		'<span>';
			$html .=			$field->getActiveQuestion();
			$html .=		   '&nbsp;';
			$html .=		'</span>';
		}

		$html .=    $field->getInputView()->fetch();

		if(!empty($description)) {
			$html .=    '<div class="field-description">';
			$html .= 		'<div class="small note">';
			$html .=    	   $description;
			$html .=    	'</div>';
			$html .=    '</div>';
		}
		$html .=    '</div>';

		$html .= '<div class="clear"></div>';

		$html .= '</div>';

		return $html;
	}

	protected function getResource() {
		return $this->_getResourceFromModuleOwner ( 'core-form/FieldDecoratorCaptchaQuestion.tpl' );
	}
}