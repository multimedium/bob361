<?php
/**
 * Captcha field
 *
 * Completely Automated Public Turing-test to tell Computers and Humans Apart
 *
 * A captcha field will test the user to see if it's human. This can be done
 * by performing a mathematical operation, asking a simple textual question or
 * even by presenting a distorted image. By doing this spambots can be stopped
 * from abusing contact (or other) forms.
 *
 * @see http://nl.wikipedia.org/wiki/Captcha
 * @author Ben Brughmans
 */
abstract class M_FieldCaptcha extends M_FieldText {

	/**
	 * Create a new captcha field
	 * 
	 * @param string $id
	 */
	public function  __construct($id) {
		parent::__construct($id);

		//a captcha field is always mandatory
		$this->setMandatory(true);
	}
	
	/**
	 * Get the answer to the captcha
	 *
	 * @return string|int
	 */
	abstract public function getAnswer();

	/**
	 * Validate the users's answer
	 * 
	 * @return bool
	 */
	public function __validate() {

		//validate answer against the value the user gave
		$res = $this->getAnswer() == $this->deliver();

		if (!$res) {
			$this->setErrorMessage(
				t('Please give the correct answer to the question')
			);
		}

		return $res;
	}
}