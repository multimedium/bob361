<?php
class M_ViewFieldSelectMultiple extends M_ViewField {
	protected function getHtml() {
		// Get field + info
		$field = $this->getField();
		$fieldValue = $field->getValue();
		
		/* @var $field M_FieldSelectMultiple */
		// Render HTML
		$html  = '';
		$html .= '<select class="select-multiple" size="5" name="'. $field->getId() .'[]" multiple="multiple" id="'. $this->getId() .'"';
		
		// Has a width been provided?
		$w = $field->getWidth();
		$h = $field->getHeight();
		if($w || $h) {
			$html .= ' style="';
			if($w) {
				$html .= 'width: ' . $w . 'px; ';
			}
			if($h) {
				$html .= 'height: ' . $h . 'px;';
			}
			$html .= '"';
		}
		
		$html .= '>';
		
		//render optgroups
		foreach($field->getGroups() AS $group) {
			$html .= sprintf('<optgroup label="%s">', $group[0]);
			foreach( $group[1] AS $value => $label) {
				$html .= $this->_getOption($value, $label, $fieldValue);
			}
			$html .= '</optgroup>';
		}
		
		//render options
		foreach($field->getItems() as $value => $label) {
			$html .= $this->_getOption($value, $label, $fieldValue);
		}

		$html .= '</select>';
		
		// return final render:
		return $html;
	}
	
	/**
	 * Get html for an option
	 *
	 * @param mixed $value
	 * @param string $label
	 * @param string $fieldValue
	 * @return string
	 */
	private function _getOption($value, $label, $fieldValue) {
		return sprintf(
			'<option value="%s"%s>%s</option>', 
			$value, 
			(is_array($fieldValue) && in_array($value, $fieldValue) ? ' selected="selected"' : ''), 
			$label
		);
	}
	
	/**
	 * Get the resource
	 *
	 * @return M_ViewHtmlResource
	 */	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldSelectMultiple.tpl');
	}
}