<?php
class M_ViewFieldDecoratorPercent extends M_ViewField {
	protected function getHtml() {
		// Get field + info
		/* @var $field M_FieldPercent */
		$field = $this->getField();
		$description = $field->getDescription();
		$errorMessage = $field->getErrorMessage();
		
		// Render HTML:
		$html  = '<div class="field-row" id="container-' . $field->getId() . '">';
		if($errorMessage) {
			$html .= '<div class="error-message">';
			$html .=    $errorMessage;
			$html .= '</div>';
		}
		if ($field->getTitle()) {
			$html .=    '<label for="'. $this->getId() .'">';
			$html .=       $field->getTitle();
			$html .=    '</label>';
		}
		$html .=    '<div class="field-container">';
		$html .=    $field->getInputView()->fetch();
		$numericField = $field->getNumericField();
		if ($numericField) {
			$html .= ' ' . $numericField->getInputView()->fetch();
		}
		if(!empty($description)) {
			$html .=    '<div class="field-description">';
			$html .= 		'<div class="small note">';
			$html .=    	   $description;
			$html .=    	'</div>';
			$html .=    '</div>';
		}
		$html .=    '</div>';
		$html .= '<div class="clear"></div>';
		$html .= '</div>';
		
		// return final render:
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldDecoratorPercent.tpl');
	}
}