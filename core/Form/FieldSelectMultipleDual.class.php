<?php
/**
 * M_FieldSelectMultipleDual class
 *
 * M_FieldSelectMultipleDual, a subclass of
 *
 * - {@link M_Field}
 * - {@link M_FieldOptions}
 * - {@link M_FieldOptionsMultiple}
 * - {@link M_FieldSelectMultiple}
 *
 * handles the input control that allow users to choose from a
 * list of options. This specific field allows multiple options
 * to be selected from the menu.
 *
 * @package Core
 */
class M_FieldSelectMultipleDual extends M_FieldSelectMultiple {
	/**
	 * Get input control view
	 *
	 * This method overrides {@link M_Field::getInputView()}.
	 *
	 * This method provides with an {@link M_ViewFieldSelectMultiple}
	 * object, which allows {@link M_Field} to include the input
	 * control in the view that is returned by
	 * {@link M_Field::getView()}.
	 *
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldSelectMultiple
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldSelectMultipleDual($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
	
	/**
	 * Get items
	 * 
	 * This method will return the list of items that is contained 
	 * in the field. Note that this method will return an associative
	 * array:
	 * 
	 * <code>
	 *    Array (
	 *       [value] => [label],
	 *       [value] => [label],
	 *       [value] => [label],
	 *       ...
	 *    )
	 * </code>
	 * 
	 * Typically, this method is used to assign the list of items
	 * to the {@link M_View} class that renders the field's input 
	 * control view ({@link M_Field::getInputView()})
	 * 
	 * @return array
	 */
	public function getItems() {
		// Working array
		$out = array();
		
		// Fetch the initial list of items
		$items = parent::getItems();
		
		// Now, for every default value set
		foreach($this->getValue() as $selected) {
			// Add it to the working array and delete it from the values
			// array. This is required to preserve the order the user chose
			// for the items
			$out[$selected] = M_Helper::getArrayElement($selected, $items);
			unset($items[$selected]);
		}
		
		// Return the output
		return $out + $items;
	}
}