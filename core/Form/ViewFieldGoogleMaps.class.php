<?php
class M_ViewFieldGoogleMaps extends M_ViewField {

	/**
     * Get html to display
     * 
     * @return string
     */
    protected function getHtml() {
		$field = $this->getField();
		/* @var $field M_FieldGoogleMaps */

		//add some javascript
		$js = M_ViewJavascript::getInstance();
		$js->addExternal(
			new M_Uri('http://maps.google.com/maps/api/js?sensor=false'),
			'google-maps-api'
		);

		$input = '<div ';
		$input .=	'style="width: '. $field->getWidth() .'px; height: '. $field->getHeight() .'px"';
		$input .=	'id="'.$field->getId().'">';
		$input .= '</div>';

		return $input;
	}

	/**
	 * Get resource from module
	 * 
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldGoogleMaps.tpl');
	}
}