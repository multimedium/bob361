<?php
class M_ViewFieldUriPathElement extends M_ViewField {
	protected function getHtml() {
		$field = $this->getField();
		/* @var $field M_FieldUriPathElement */
		
		$html = '<input type="text" name="'. $field->getId() .'"' 
				. ' id="'. $this->getId() .'"' 
				. ' value="'. $field->getValue() .'"' 
				. ' class="field-text field-uri-path-element"';
		
		if($field->getWidth()) {
			$html .= ' style="width: '. $field->getWidth() .'px;"';
		}
		if($field->getMaxLength()) {
			$html .= ' maxlength="'. $field->getMaxLength() .'"';
		}
		if($field->getHint()) {
			$html .= ' title="'. $field->getHint() .'"';
		}
		if($field->getReadonly()) {
			$html .= ' readonly="readonly"';
		}
		$html .= ' />';

		//add a hidden variable which is used by jQuery to get the value
		//of the connected field
		$sourceFieldId = $field->getSourceFieldId();
		if ($sourceFieldId) {
			$html .= '<input type="hidden"';
			$html .= ' name="'.$field->getId().'-source-field-id"';
			$html .= ' value="'.$sourceFieldId.'">';
		}

		//add a hidden variable which is used by jQuery to get the value
		//of the field which holds the maximum amount of words this field
		//will use
		$html .= '<input type="hidden"';
		$html .= ' name="'.$field->getId().'-maximum-amount-words"';
		$html .= ' value="'.$field->getMaximumAmountOfWords().'">';
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldUriPathElement.tpl');
	}
}