<?php
/**
 * M_FieldPassword class
 * 
 * M_FieldPassword, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldText}
 * 
 * handles the input control that allows users to introduce a 
 * password.
 * 
 * @package Core
 */
class M_FieldPassword extends M_FieldText {
	/**
	 * Encryption
	 * 
	 * This property stores the encryption that should be applied on
	 * the delivered password value. For more information, read docs
	 * on {@link M_FieldPassword::setEncryption()}.
	 * 
	 * @access private
	 * @var string
	 */
	private $_encryption = NULL;
	
	/**
	 * Show strength?
	 * 
	 * This property stores the flag that tells us whether or not the
	 * password's strength should be displayed in the interface.
	 * 
	 * @see M_FieldPassword::setShowingStrength()
	 * @access private
	 * @var bool
	 */
	private $_showStrength = false;
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()} and {@link M_FieldText::set()}, to make 
	 * the following (additional) definition keys available:
	 * 
	 * <code>encryption</code>
	 * 
	 * Set encryption. For more information, read the docs on 
	 * {@link M_FieldPassword::setEncryption()}.
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @see M_FieldText::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// The encryption to be used in order to deliver the password
			case 'encryption':
				$this->setEncryption($definition);
				break;

			// Whether or not the password strength should be shown in the
			// application interface
			case 'strength':
			case 'showStrength':
			case 'showingStrength':
				$this->setShowingStrength($definition ? TRUE : FALSE);
				break;

			// Other properties
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Set Encryption
	 * 
	 * This method sets the encryption that should be applied on the
	 * introduced password, when delivering the final value to the
	 * form. This will affect {@link M_FieldPassword::deliver()}.
	 * 
	 * Example 1
	 * <code>
	 *    $field = new M_FieldPassword('MyField');
	 *    $field->setEncryption('md5');
	 * </code>
	 * 
	 * If no encryption is set by this method, no encryption will be
	 * applied at all to the delivered value.
	 * 
	 * @access public
	 * @param string $algorithm
	 * 		The encryption algorithm to be applied on the password
	 * @return void
	 */
	public function setEncryption($algorithm) {
		// 2 common encryption functions are checked here. More
		// extensive list is of course welcome.
		if(in_array($algorithm, array('md5', 'sha1'))) {
			$this->_encryption = $algorithm;
		} else {
			throw new M_FieldException('Unrecognized encryption: "'. $algorithm .'"');
		}
	}
	
	/**
	 * Show strength?
	 * 
	 * This method allows you to specify whether or not a password
	 * strength should be shown, along with the introduced password
	 * 
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE if a password strength should be shown, FALSE
	 * 		if not
	 * @return void
	 */
	public function setShowingStrength($flag) {
		$this->_showStrength = (bool) $flag;
	}
	
	/**
	 * Show strength?
	 * 
	 * This method allows you to check whether or not a password strength 
	 * is shown with the field.
	 * 
	 * @see M_FieldPassword::setShowingStrength()
	 * @access public
	 * @return bool
	 */
	public function isShowingStrength() {
		return $this->_showStrength;
	}
	
	/**
	 * Deliver the final value
	 * 
	 * This method overrides {@link M_Field::deliver()}.
	 * 
	 * This method will apply encryption to the password delivered
	 * by {@link M_FieldText::deliver()}, if the field has been told
	 * do so (with {@link M_FieldPassword::setEncryption()}.
	 * 
	 * @access public
	 * @see M_FieldText::deliver()
	 * @return mixed
	 */
	public function deliver() {
		if($this->_encryption) {
			return call_user_func($this->_encryption, parent::deliver());
		} else {
			return parent::deliver();
		}
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldPassword} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldText
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldPassword($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}
?>