<?php
/**
 * M_FieldRadio class
 * 
 * M_FieldRadio, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldOptions}
 * 
 * handles the input control that allow users to choose from a
 * list of options. Note that, M_FieldRadio will allow the user
 * to select only one of the provided options.
 * 
 * @package Core
 */
class M_FieldRadio extends M_FieldOptions {
	public function getInputView() {
		$view = new M_ViewFieldRadio($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}
?>