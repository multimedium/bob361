<?php
/**
 * M_FieldException class
 * 
 * Exceptions thrown by {@link M_Field} and subclasses.
 *
 * @package Core
 */
class M_FieldException extends M_Exception {
}
?>