<?php
/**
 * M_ViewField class
 * 
 * M_ViewField is an abstract class, and is used as the superclass to
 * all views that render a given field's input control or decoration.
 * 
 * @package Core
 */
abstract class M_ViewField extends M_ViewCoreHtml {
	
	/**
	 * Construct
	 *
	 * @param M_Field $field
	 * @return void
	 */
	public function __construct(M_Field $field) {
		$this->setField($field);
	}
	
	/**
	 * Set field
	 *
	 * @param M_Field $field
	 * @return void
	 */
	public function setField(M_Field $field) {
		$this->assign('field', $field);
		$this->setId($field->getId());
	}
	
	/**
	 * Get field
	 *
	 * @return M_Field
	 */
	public function getField() {
		return $this->getVariable('field', NULL);
	}
	
	/**
	 * Rend field
	 *
	 * @return string
	 */
	protected function _render() {
		// Check if a field has been provided to the view:
		if($this->getVariable('field', FALSE)) {
			// If so, return final render:
			return parent::_render();
		} else {
			// If not, throw exception
			throw new M_ViewException(sprintf(
				'%s: Missing field object',
				__CLASS__
			));
		}
	}
}