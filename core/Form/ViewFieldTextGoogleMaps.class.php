<?php

/**
 * ViewFieldTextGoogleMaps
 * 
 * @package App
 * @subpackage module
 * @author Willem Cornelissen
 */

class M_ViewFieldTextGoogleMaps extends M_ViewFieldText {
	
	 protected function getHtml() {
		// Get the field that is being rendered for display:
		/* @var $field M_FieldTextGoogleMaps */
		$field = $this->getField();
		
		$out = parent::getHtml();
		
		if($field->getShowGoogleMaps()) {
			$out .= '<div class="address-container">';
			$out .=		$this->_getFieldContainerView($field->getFieldGoogleMaps());
			$out .= '</div>';
		}
		
		$out .= '<div class="m-clear"></div>';
		return $out;
	}

	/**
	 * Create a container including a {@link M_Field}
	 *
	 * Each field lives in its own container: this way we can style a field,
	 * or communicate to it more easily with jQuery, ...
	 *
	 * @param M_Field $field
	 * @param string $class
	 * @return string
	 */
	private function _getFieldContainerView(M_Field $field, $class = '') {
		$out =	'<div class="field-address-container '.$class.'" id="container-'.$field->getId().'">';

		//add the title
		if ($field->getTitle()) {
			$out .= '<label for="'.$field->getId().'">'.$field->getTitle().'</label>';
		}

		/* if($field->getErrorMessage()) {
			$out .= '<div class="error-message">';
			$out .=    $field->getErrorMessage();
			$out .= '</div>';
		} */

		//add the field itself
		$out .= $field->getInputView()->fetch();

		//add the description
		if($field->getDescription()) {
			$out .=    '<div class="field-description">';
			$out .= 		'<div class="small note">';
			$out .=				$field->getDescription();
			$out .=			'</div>';
			$out .=    '</div>';
		}

		//add buttons when this is a Google Maps Field
		if (M_Helper::isInstanceOf($field, 'M_FieldGoogleMaps')) {
			$out .=		'<div class="field-buttons">';
			$out .=			'<a href="#" class="left button-submit" id="'.$field->getId().'-button-locate"><span>'.t('locate').'</span></a>';
			$out .=			'<a href="#" class="left button" id="'.$field->getId().'-button-clear"><span>'.t('clear').'</span></a>';
			$out .=		'</div>';
		}

		$out .= '</div>';

		return $out;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldTextGoogleMaps.tpl');
	}
}
