<?php
class M_ViewFieldCron extends M_ViewField {
	protected function getHtml() {
		static $counter = 0;
		$html  = '';
		
		// The javascript singleton
		$jsO = M_ViewJavascript::getInstance();
		
		// Add javascript function, to be executed when the field 
		// changes of value:
		if($counter ++ == 0) {
			$js  =    'function updateCronField(f, s) { ';
			$js .=       'var e = document.getElementById(f); ';
			$js .=       'var v = e.options[e.options.selectedIndex].value; ';
			$js .=       'switch(v) { ';
			$js .=          'case "never": ';
			$js .=             '$("#" + f + "-weekly").hide(s); ';
			$js .=             '$("#" + f + "-monthly").hide(s); ';
			$js .=             '$("#" + f + "-time").hide(s); ';
			$js .=             'break; ';
			$js .=          'case "daily": ';
			$js .=             '$("#" + f + "-weekly").hide(s); ';
			$js .=             '$("#" + f + "-monthly").hide(s); ';
			$js .=             '$("#" + f + "-time").show(s); ';
			$js .=             'break; ';
			$js .=          'case "weekly": ';
			$js .=             '$("#" + f + "-weekly").show(s); ';
			$js .=             '$("#" + f + "-monthly").hide(s); ';
			$js .=             '$("#" + f + "-time").show(s); ';
			$js .=             'break; ';
			$js .=          'case "monthly": ';
			$js .=             '$("#" + f + "-weekly").hide(s); ';
			$js .=             '$("#" + f + "-monthly").show(s); ';
			$js .=             '$("#" + f + "-time").show(s); ';
			$js .=             'break; ';
			$js .=       '}';
			$js .=    '}';
			$jsO->addInline($js);
		}
		
		// Get field and current value
		$field = $this->getField();
		$fieldValue = $field->getValue();
		
		// Start input control
		$html .= '<select size="1" name="'. $field->getId() .'" id="'. $this->getId() .'">';
		$html .=    '<option value="never"'. ($fieldValue['value'] == 'never' ? ' selected="selected"' : '') .'>'. t('Never') .'</option>';
		$html .=    '<option value="daily"'. ($fieldValue['value'] == 'daily' ? ' selected="selected"' : '') .'>' . t('Daily') . '</option>';
		$html .=    '<option value="weekly"'. ($fieldValue['value'] == 'weekly' ? ' selected="selected"' : '') .'>' . t('Weekly') . '</option>';
		$html .=    '<option value="monthly"'. ($fieldValue['value'] == 'monthly' ? ' selected="selected"' : '') .'>'. t('Monthly') .'</option>';
		$html .= '</select>';
		
		// Weekly:
		$html .= '<div class="field-cron-weekly" id="'. $field->getId() .'-weekly">';
		$html .=    t('Every');
		$html .=    '<select size="1" name="'. $field->getId() .'-wday">';
		$html .=       '<option value="1"'. ((isset($fieldValue['wday']) && $fieldValue['wday'] == 1) ? ' selected="selected"' : '') .'>'. M_Locale::getData()->getDay(1, 'wide') .'</option>';
		$html .=       '<option value="2"'. ((isset($fieldValue['wday']) && $fieldValue['wday'] == 2) ? ' selected="selected"' : '') .'>'. M_Locale::getData()->getDay(2, 'wide') .'</option>';
		$html .=       '<option value="3"'. ((isset($fieldValue['wday']) && $fieldValue['wday'] == 3) ? ' selected="selected"' : '') .'>'. M_Locale::getData()->getDay(3, 'wide') .'</option>';
		$html .=       '<option value="4"'. ((isset($fieldValue['wday']) && $fieldValue['wday'] == 4) ? ' selected="selected"' : '') .'>'. M_Locale::getData()->getDay(4, 'wide') .'</option>';
		$html .=       '<option value="5"'. ((isset($fieldValue['wday']) && $fieldValue['wday'] == 5) ? ' selected="selected"' : '') .'>'. M_Locale::getData()->getDay(5, 'wide') .'</option>';
		$html .=       '<option value="6"'. ((isset($fieldValue['wday']) && $fieldValue['wday'] == 6) ? ' selected="selected"' : '') .'>'. M_Locale::getData()->getDay(6, 'wide') .'</option>';
		$html .=       '<option value="0"'. ((isset($fieldValue['wday']) && $fieldValue['wday'] == 0) ? ' selected="selected"' : '') .'>'. M_Locale::getData()->getDay(0, 'wide') .'</option>';
		$html .=    '</select>';
		$html .= '</div>';
		
		// Monthly:
		$html .= '<div class="field-cron-monthly" id="'. $field->getId() .'-monthly">';
		$html .=    t('Every day @number', array('@number' => ''));
		$day = isset($fieldValue['day']) ? $fieldValue['day'] : '';
		$html .=    '<input type="text" name="'. $field->getId() .'-day" value="'. $day .'" class="field" size="2" maxlength="2" /> ';
		$html .=    t('of @month', array('@month' => ''));
		$html .=    '<select size="1" name="'. $field->getId() .'-month">';
		$html .=       '<option value="*"' . ((isset($fieldValue['month']) && $fieldValue['month'] == 0)  ? ' selected="selected"' : '') .'>' . t('every month') . '</option>';
		$html .=       '<option value="1"' . ((isset($fieldValue['month']) && $fieldValue['month'] == 1)  ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(1) . '</option>';
		$html .=       '<option value="2"' . ((isset($fieldValue['month']) && $fieldValue['month'] == 2)  ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(2) . '</option>';
		$html .=       '<option value="3"' . ((isset($fieldValue['month']) && $fieldValue['month'] == 3)  ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(3) . '</option>';
		$html .=       '<option value="4"' . ((isset($fieldValue['month']) && $fieldValue['month'] == 4)  ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(4) . '</option>';
		$html .=       '<option value="5"' . ((isset($fieldValue['month']) && $fieldValue['month'] == 5)  ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(5) . '</option>';
		$html .=       '<option value="6"' . ((isset($fieldValue['month']) && $fieldValue['month'] == 6)  ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(6) . '</option>';
		$html .=       '<option value="7"' . ((isset($fieldValue['month']) && $fieldValue['month'] == 7)  ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(7) . '</option>';
		$html .=       '<option value="8"' . ((isset($fieldValue['month']) && $fieldValue['month'] == 8)  ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(8) . '</option>';
		$html .=       '<option value="9"' . ((isset($fieldValue['month']) && $fieldValue['month'] == 9)  ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(9) . '</option>';
		$html .=       '<option value="10"'. ((isset($fieldValue['month']) && $fieldValue['month'] == 10) ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(10) . '</option>';
		$html .=       '<option value="11"'. ((isset($fieldValue['month']) && $fieldValue['month'] == 11) ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(11) . '</option>';
		$html .=       '<option value="12"'. ((isset($fieldValue['month']) && $fieldValue['month'] == 12) ? ' selected="selected"' : '') .'>' . M_LocaleData::getInstance()->getMonth(12) . '</option>';
		$html .=    '</select>';
		$html .= '</div>';
		
		// Hours + minutes
		$html .= '<div class="field-cron-time" id="'. $field->getId() .'-time">';
		$html .=    t('at @time', array('@time' => ''));
		$hours = isset($fieldValue['hours']) ? $fieldValue['hours'] : '';
		$html .=    '<input type="text" name="'. $field->getId() .'-hours" value="'. $hours .'" class="field" size="2" maxlength="2" />';
		$html .=    ' : ';
		$minutes = isset($fieldValue['minutes']) ? $fieldValue['minutes'] : '';
		$html .=    '<input type="text" name="'. $field->getId() .'-minutes" value="'. $minutes .'" class="field" size="2" maxlength="2" />';
		$html .= '</div>';
		
		// Update to the current state:
		$js  =    'updateCronField("'. $this->getId() .'", 0); ';
		$js .=    '$("#'. $this->getId() .'").change(function() { ';
		$js .=       'updateCronField($(this).attr("id"), 350); ';
		$js .=    '})';
		$jsO->addInline($js);
		
		// return final render
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldCron.tpl');
	}
}
?>