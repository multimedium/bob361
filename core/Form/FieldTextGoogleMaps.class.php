<?php
/**
 * FieldTextGoogleMaps class
 * 
 * FieldTextGoogleMaps, a subclass of {@link M_FieldText}, handles the input 
 * control that allows users to introduce an address as a string and locate it
 * on a map
 * 
 * @package Core
 * @author Willem Cornelissen
 */
class M_FieldTextGoogleMaps extends M_FieldText {
	/**
	 * The field which holds the coördinates
	 *
	 * @var M_FieldGoogleMaps
	 */
	protected $_fieldGoogleMaps;

	/**
	 * Do we want to show the Google-Maps--field?
	 *
	 * In most cases we don't want to show the Google-Maps-field: a simple address
	 * field mostly does not contain coördinates-data. That's why this field defaults
	 * to false
	 *
	 * @see _fieldGoogleMaps
	 * @var bool
	 */
	protected $_showGoogleMaps = false;

	/**
	 * Get the Google Maps field which holds the location (longitude and
	 * latitude)
	 *
	 * @return M_FieldGoogleMaps
	 */
	public function getFieldGoogleMaps() {
		if (!$this->hasFieldGoogleMaps()) {
			$this->_fieldGoogleMaps = new M_FieldGoogleMaps(
				$this->getId().'_google-maps'
			);
			//Set the id of this address field so the Google Maps has a link
			//to this field (and can use the data to update location e.g.)
			$this->_fieldGoogleMaps->setAddressFieldId($this->getId());
			
			//Set a default description if no description has been set
			$this->_fieldGoogleMaps->setDescription(
				t('Use the <strong>locate-button</strong> to update the' .
				' location, or drag the marker to the correct position' .
				' if the position is incorrect.')
			);
		}
		return $this->_fieldGoogleMaps;
	}

	/**
	 * Set the module owner for this field and all nested fields too
	 *
	 * M_FieldAddress is a field which holds other fields: streetname, zip,
	 * country, ... We need to set the module-owner for all of these fields
	 * 
	 * @param string $moduleOwner
	 */
	public function setModuleOwner($moduleOwner) {
		parent::setModuleOwner($moduleOwner);
		$this->getFieldGoogleMaps()->setModuleOwner($moduleOwner);
	}

	/**
	 * Set the Google Maps field
	 *
	 * @param M_FieldGoogleMaps $_fieldGoogleMaps
	 */
	public function setFieldGoogleMaps(M_FieldGoogleMaps $_fieldGoogleMaps = null) {
		$this->_fieldGoogleMaps = $_fieldGoogleMaps;
	}

	/**
	 * Get ShowGoogleMaps
	 *
	 * @return bool
	 */
	public function getShowGoogleMaps() {
		return $this->_showGoogleMaps;
	}

	/**
	 * Check if the field address has a Google Maps-field
	 *
	 * @return boolean
	 */
	public function hasFieldGoogleMaps() {
		return !is_null($this->_fieldGoogleMaps);
	}

	/**
	 * Set ShowGoogleMaps
	 *
	 * @param bool $arg
	 * @return M_FieldAddress
	 */
	public function setShowGoogleMaps($arg) {
		$this->_showGoogleMaps = $arg;
		//if the field needs to be shown: create the field by getting it.
		//this way {@link hasFieldGoogleMaps()} knows a field exists
		if($arg) {
			$this->getFieldGoogleMaps();
		}else {
			$this->setFieldGoogleMaps(null);
		}
		return $this;
	}

	/**
	 * Deliver
	 * 
	 * @access public
	 * @return M_ContactAddress
	 */
	public function deliver() {
		parent::deliver();
		
		// Create a new ContactAdress and save the address string as 
		// streetaddress
		$contactAddress = new M_ContactAddress();
		$contactAddress->setStreetAddress($this->getValue());
		
		// if a Google Maps field isset: we check if coördinates are set
		if ($this->hasFieldGoogleMaps()) {
			$coördinates = $this->getFieldGoogleMaps()->deliver();
			
			//coördinates posted
			if ($coördinates != '') {
				$latitude = M_FieldGoogleMaps::getLatitudeFromDeliveredValue($coördinates);
				if ($latitude) $contactAddress->setLatitude($latitude);

				$longitude = M_FieldGoogleMaps::getLongitudeFromDeliveredValue($coördinates);
				if ($longitude) $contactAddress->setLongitude($longitude);
				
				// Using the coordinates, rebuild the address, so we can
				// fetch the country
				$googleMapsService = new M_ServiceGoogleMaps();
				$address = $googleMapsService->getAddressByCoordinates($latitude, $longitude);
				if($address) {
					$contactAddress->setCountry($address->getCountry());
					$contactAddress->setCountryISO($address->getCountryISO());
				}
			}

			//no coördinates given: try to retrieve anyway
			else {
				$googleMapsService = new M_ServiceGoogleMaps();
				try {
					//update address with coödinates using the service
					$coords = $googleMapsService->getCoordinatesByString(
						$this->getValue()
					);
					
					if($coords) {
						$contactAddress->setLatitude($coords->getLatitude());
						$contactAddress->setLongitude($coords->getLongitude());
						
						//update country
						$address = $googleMapsService->getAddressByCoordinates(
							$contactAddress->getLatitude(), 
							$contactAddress->getLongitude()
						);
						if($address) {
							$contactAddress->setCountry($address->getCountry());
							$contactAddress->setCountryISO($address->getCountryISO());
						}
					}
					
				}catch (M_Exception $e) {
					// not able to get location data
				}
			}
		}	
		
		return $contactAddress;
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldTextGoogleMaps} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @return M_View
	 */
	public function getInputView() {
	    $view = new M_ViewFieldTextGoogleMaps($this);
		$view->setId($this->getId());
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
	
	/**
	 * Set the default value
	 * 
	 * @access public
	 * @param M_ContactAddress
	 * @return void
	 */
	public function setDefaultValue(M_ContactAddress $contactAddress = null) {
		if (!is_null($contactAddress)) $this->setContactAddress($contactAddress);
	}
	
	/**
	 * Set the contact address
	 * 
	 * @access public
	 * @param M_ContactAddress
	 * @return void
	 */
	public function setContactAddress(M_ContactAddress $contactAddress) {
		parent::setDefaultValue($contactAddress);

		//set coördinates, if a Google Maps field is found
		if ($this->hasFieldGoogleMaps()) {
			$fldGoogleMaps = $this->getFieldGoogleMaps();
			
			//if both latitude and longitude are set: update field
			if ($contactAddress->getLatitude() && $contactAddress->getLongitude()) {
				$fldGoogleMaps->setDefaultValue(
					$contactAddress->getLatitude().','.$contactAddress->getLongitude()
				);
			}
		}
	}

	/**
	 * Add field definition
	 *
	 * This method is used by {@link M_Field::factory()}, to mount
	 * the field with the given form definition. Check out Example 1
	 * to see how exactly this method is being used by
	 * factory():
	 *
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			case 'showGoogleMaps':
				$this->setShowGoogleMaps(M_Helper::isBooleanTrue($definition));
				break;
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
}
