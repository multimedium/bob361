<?php
/**
 * M_Helper class
 * 
 * This class contains different helper (or utitlity) functions which can 
 * be used throughout the application.
 * 
 * @package Core
 */
class M_Helper extends M_Object {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Constants for the alphaId settings
	 */
	const ALPHA_ID_SETTING_LOWERCASE = 'lower';
	const ALPHA_ID_SETTING_UPPERCASE = 'upper';
	const ALPHA_ID_SETTING_NUMERIC = 'numeric';
	
	/* -- PROPERTIES -- */
	
	/**
	 * Default alphaId settings
	 * 
	 *		We can set to use:
	 *			- lowercase characters
	 *			- uppercase characters
	 *			- numeric characters
	 * 
	 * @var array 
	 */
 	private static $_alphaIdSettings = array(
		self::ALPHA_ID_SETTING_LOWERCASE => true,
		self::ALPHA_ID_SETTING_UPPERCASE => true,
		self::ALPHA_ID_SETTING_NUMERIC => true
	);

	/* -- PUBLIC -- */
	
	/**
	 * Make a string's first character lowercase
	 * 
	 * Returns a string with the first character lowercased, if that character 
	 * is alphabetic.
	 *
	 * @access public
	 * @param string $str
	 * @return string
	 */
	public static function lcfirst($str) {
		if(! is_string($str)) {
			throw new M_Exception(sprintf(
				'Expected string value in %s::%s()',
				__CLASS__,
				__FUNCTION__
			));
		}
		return strtolower( $str{0} ) . substr( $str, 1);
	}
	
	/**
	 * Strip whitespace (or other characters) from a string
	 * 
	 * Will do exactly the same as PHP's built-in trim() function. Note however
	 * that white-space characters are ALWAYS removed from the original string,
	 * but you can choose to strip additional characters, by providing the second
	 * argument.
	 * 
	 * Example 1, strip /
	 * <code>
	 *    $original = 'Hello    /';
	 *    echo '"' . M_Helper::trimCharlist($original, '/') . '"';
	 * </code>
	 * 
	 * Example 1 will produce the following output:
	 * <code>
	 *    "Hello"
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string that will be trimmed
	 * @param string $characterList
	 * 		The list of additional characters to be trimmed from the string
	 * @return string
	 */
	public static function trimCharlist($string, $characterList = '') {
		if(! is_string($string) && ! is_null($string)) {
			throw new M_Exception(sprintf(
				'Expected string value in %s::%s()',
				__CLASS__,
				__FUNCTION__
			));
		}
		return trim((string) $string, " \t\n\r\0\x0B" . (is_array($characterList) ? implode('', $characterList) : (string) $characterList));
	}
	
	/**
	 * Strip whitespace (or other characters) from a string
	 * 
	 * Will do exactly the same as {@link M_Helper::trimCharlist()}, but will
	 * strip the characters only from the BEGINNING of the string!
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string that will be trimmed
	 * @param string $characterList
	 * 		The list of additional characters to be trimmed from the string
	 * @return string
	 */
	public static function ltrimCharlist($string, $characterList = '') {
		if(! is_string($string) && ! is_null($string)) {
			throw new M_Exception(sprintf(
				'Expected string value in %s::%s()',
				__CLASS__,
				__FUNCTION__
			));
		}
		return ltrim((string) $string, " \t\n\r\0\x0B" . (is_array($characterList) ? implode('', $characterList) : (string) $characterList));
	}
	
	/**
	 * Strip whitespace (or other characters) from a string
	 * 
	 * Will do exactly the same as {@link M_Helper::trimCharlist()}, but will
	 * strip the characters only from the END of the string!
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string that will be trimmed
	 * @param string $characterList
	 * 		The list of additional characters to be trimmed from the string
	 * @return string
	 */
	public static function rtrimCharlist($string, $characterList = '') {
		if(! is_string($string) && ! is_null($string)) {
			throw new M_Exception(sprintf(
				'Expected string value in %s::%s()',
				__CLASS__,
				__FUNCTION__
			));
		}
		return rtrim((string) $string, " \t\n\r\0\x0B" . (is_array($characterList) ? implode('', $characterList) : (string) $characterList));
	}
	
	/**
	 * Remove newline-characters from a string
	 *
	 * @param string $string
	 * @return string
	 */
	public static function filterNewlines($string) {
		return str_replace(array("\n", "\r"), '', $string);
	}
	
	/**
	 * Get unique token
	 * 
	 * Will generate a unique token based on time in microseconds. The returned
	 * value is a 32-character long hash (MD5).
	 * 
	 * @access public
	 * @return str
	 */
	public static function getUniqueToken() {
		return md5(uniqid(rand(), true));
	}

	/**
	 * Create a random string
	 * 
	 * @param int $length
	 * @param bool $lower
	 * @param bool $upper
	 * @param bool $numeric
	 * @return bool
	 */
	public static function getRandomString($length = 8, $lower = true, $upper = true, $numeric = true) {
		$string = "";
		$characters = "";

		//the characters to use
		if($upper) {
			$characters .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		}if($lower) {
			$characters .= "abcdefghijklmnopqrstuvwxwz";
		}if($numeric) {
			$characters .= "0123456789";
		}
		
		if($characters == "") {
			throw new M_Exception('Cannot create random string without characters, please choose at least one of lower, upper or numeric');
		}

		//compose string
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters)-1)];
		}

		//randomize :)
		return str_shuffle($string);
	}
	
	/**
	 * Generate a list of unique random strings
	 * 
	 * Note: beware of infinite loops when specifying an amount too high for the
	 * 
	 * 
	 * @param int $amount
	 * @param int $length
	 * @param bool $lower
	 * @param bool $upper
	 * @param bool $numeric
	 * @return array 
	 */
	public static function getRandomStrings($amount, $length = 8, $lower = true, $upper = true, $numeric = true) {
		//check if amount is possible
		$characterAmount = 0;
		if($lower) $characterAmount = 26;
		if($upper) $characterAmount += 26;
		if($numeric) $characterAmount += 10;
		
		$maxCombinations = pow($characterAmount, $length);
		
		//check if amount is 80% of maximum available combinations
		$ratio = ($amount / $maxCombinations);
		if($ratio > 0.8) {
			throw new M_Exception(sprintf(
				"You requested %s unique strings where as we can produce ".
				"%s total unique combinations (ratio %s). Please reduce the ".
				"requested amount so the ratio is below 0.8",
				$amount, 
				$maxCombinations,
				round($ratio,2)
			));
		}
		
		$strings = array();
		for($i = 0 ; $i < $amount ; $i++) {
			//get a unique string
			$string = M_Helper::getRandomString($length, $lower, $upper, $numeric);
			
			//check if this string was not been used, if so: create a new one (recursive)
			$strings[] = self::_getRandomStringsUnique($string, $strings, $length, $lower, $upper, $numeric);
		}
		
		return $strings;
	}
	
	/**
	 * Generate a unique string
	 * 
	 * Note: this is a recursive function!
	 * 
	 * @param string $string
	 * @param array $strings Array containing strings
	 * @param int $length
	 * @param bool $lower
	 * @param bool $upper
	 * @param bool $numeric
	 * @return string
	 */
	private static function _getRandomStringsUnique($string, $strings, $length, $lower, $upper, $numeric) {
		if(in_array($string, $strings)) {
			return self::_getRandomStringsUnique(
				M_Helper::getRandomString($length, $lower, $upper, $numeric), 
				$strings, 
				$length, 
				$lower, 
				$upper, 
				$numeric
			);
		}
		
		return $string;
	}
	
	/**
	 * Explode a string into array, by camel casing
	 *
	 * Will explode a string into an array. An array element is created out of
	 * each word in the camel-cased string.
	 * 
	 * Example 1
	 * <code>
	 *    print_r(M_Helper::explodeByCamelCasing('ThisIsMyExample'));
	 * </code>
	 * 
	 * Example 1 would output the following array:
	 * <code>
	 *    Array (
	 *       [0] => 'This',
	 *       [1] => 'Is',
	 *       [2] => 'My',
	 *       [3] => 'Example'
	 *    )
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The camel-cased string, to be exploded into an array
	 * @param bool $explodeByNumbers
	 *		Set to TRUE if numbers should be considered words too, FALSE if not
	 * @return array
	 */
	public static function explodeByCamelCasing($string, $explodeByNumbers = TRUE) {
		$out = array('');
		$pos = 0;
		$num = true;
		for($i = 0, $n = strlen($string); $i < $n; $i ++) {
			$o = ord($string{$i});
			if($o > 64 && $o < 91) {
				$cur = ($pos == 0 && empty($out[0])) ? 0 : ++ $pos;
				$out[$cur] = $string{$i};
				$num = false;
			} elseif($explodeByNumbers && $o > 47 && $o < 58) {
				if($num) {
					$out[$pos] .= $string{$i};
				} else {
					$cur = ($pos == 0 && empty($out[0])) ? 0 : ++ $pos;
					$out[$cur] = $string{$i};
				}
				$num = true;
			} else {
				$out[$pos] .= $string{$i};
				$num = false;
			}
		}
		return $out;
	}

	/**
	 * Explode a string into array, by newlines
	 *
	 * Will explode a string into an array. An array element is created out of
	 * each newline.
	 *
	  * Example 1
	 * <code>
	 *    print_r(M_Helper::explodeByNewLines('Here goes a
	 * newline'));
	 * </code>
	 *
	 * Example 1 would output the following array:
	 * <code>
	 *    Array (
	 *       [0] => 'Here goes a',
	 *       [1] => 'newline'
	 *    )
	 * </code>
	 *
	 * @param string $string
	 * @return array
	 * @author Ben Brughmans
	 * @static
	 */
	public static function explodeByNewLines($string) {
		$string = str_replace("\r", '', $string);
		return explode("\n", trim($string));
	}

	/**
	 * Convert a string to a camel-cased string
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string to be converted into camel-cased string
	 * @param bool $lcFirst
	 * 		Set to TRUE if you want the first word to start with an uppercase
	 * 		character, FALSE if you prefer lowercase
	 * @return string
	 */
	public static function getCamelCasedString($string, $lcfirst = FALSE) {
		// First of all, we create a chain of filters, to
    	// - remove all accents (converted to corresponding character)
    	// - remove html markup
    	$filter = new M_FilterTextAccents(new M_FilterTextHtml(new M_FilterTextValue($string)));
    	
    	// Remove punctuation at the beginning/end of the string
    	$string = M_Helper::trimCharlist($filter->apply(), ',.?!)("\'/\\_+;:*');
    	
		// Remove redundant white-spaces:
		$string = preg_replace('/[\s\-]+/', ' ', $string);
		
		// Remove punctuation (replaced by dashes):
		$string = str_replace(',', '-', $string);
		$string = str_replace('.', '-', $string);
		$string = str_replace('?', '-', $string);
		$string = str_replace('!', '-', $string);
		$string = str_replace(')', '-', $string);
		$string = str_replace('(', '-', $string);
		$string = str_replace('"',  '', $string);
		$string = str_replace('\'', '', $string);
		$string = str_replace('/', '-', $string);
		$string = str_replace('_', '-', $string);
		$string = str_replace('+', '-', $string);
		$string = str_replace(',', '-', $string);
		$string = str_replace(';', '-', $string);
		$string = str_replace(':', '-', $string);
		$string = str_replace('*',  '', $string);
		$string = str_replace(' ', '-', $string);
		
		// Some characters are replaced by full words:
		$string = str_replace('%', 'percent-', $string);
		$string = str_replace('@', 'at-',      $string);
		$string = str_replace('&', 'and-',     $string);
		
		// Remove redundant dashes, and make lowercase:
		$string = strtolower(preg_replace('/[-]+/', '-', $string));
		
		// Now, we remove anything that is not in the alphanumerical range. For
		// each of the characters in the string:
		$out = '';
		for($i = 0; $i < strlen($string); $i ++) {
			// If the current character is in the range of allowed characters:
			if(strpos(' abcdefghijklmnopqrstuvwxyz0123456789-', $string{$i})) {
				// We add the character to the output string:
				$out .= $string{$i};
			}
		}
		
		// return final camel-cased string:
		$out = implode(array_map('ucfirst', explode('-', $out)));
		
		return (
			$lcfirst
				? self::lcfirst($out)
				: $out
		);
	}
	
	/**
	 * Remove repeated characters from a string
	 * 
	 * @static
	 * @access public
	 * @example M_Helper::getUniqueCharsInString(1,'aaaaaaabcbc')   = 'abcbc'
	 * @example M_Helper::getUniqueCharsInString(2,'aaaaaaabcbc')   = 'aabc'
	 * @example M_Helper::getUniqueCharsInString(2,'aaaaaaabcdbcd') = 'aabcd'
	 * @param integer $charLength
	 * 		How big is the repeated string?
	 * @param string $string
	 * 		The string to be cleaned
	 * @return string
	 */
	public static function getUniqueCharsInString($charLength, $string) {
		$cleaned = '';
		for ($i = 0; $i < strlen($string) ; $i++) {
			$repeated = true;
			for ($j = 0; $j < $charLength && ($j + $i + $charLength) < strlen($string) ; $j++) {
				$repeated = $repeated && ($string{($j + $i)} == $string{($j + $i + $charLength)});
			}
			if ($j < $charLength) {
				$repeated = false;
			}
			if ($repeated) {
				$i += $charLength - 1;
				$repeated = false;
			}
			else {
				$cleaned .= $string{$i};
			}
		}
		return $cleaned;
	}
	
	/**
	 * Has character(s)?
	 * 
	 * Will check if the provided string contains one of the characters.
	 * 
	 * @static
	 * @access public
	 * @example M_Helper::containsCharacters('tom', 't') = TRUE
	 * @example M_Helper::containsCharacters('tom', 'k') = FALSE
	 * @example M_Helper::containsCharacters('tom', 'om') = TRUE
	 * @example M_Helper::containsCharacters('tom', 'mj') = TRUE
	 * @param string $string
	 * 		The string value to be evaluated
	 * @param string $characterList
	 * 		The characters to look for
	 * @return bool $flag
	 * 		Returns TRUE if the string contains any of the character(s),
	 * 		FALSE if not.
	 */
	public static function containsCharacters($string, $characterList) {
		// For each of the characters in the string:
		for($i = 0, $n = strlen($string); $i < $n; $i ++) {
			// Check if the current character is one of the characters we are
			// looking for:
			if(strpos($characterList, $string{$i}) !== FALSE) {
				// If so, we return TRUE:
				return TRUE;
			}
		}
		
		// If we're still here, it means that we did not find any of the characters.
		// In that case, we return FALSE
		return FALSE;
	}
	
	/**
	 * Has CDATA characters?
	 * 
	 * Will check if the provided string contains special characters, which would
	 * require the string to be wrapped by 
	 * 
	 * <code><![CDATA[ ]]></code>
	 * 
	 * in an XML file's source code. 
	 * 
	 * FYI, W3C Says:
	 * The term CDATA is used about text data that should not be parsed by the 
	 * XML parser. Characters like "<" and "&" are illegal in XML elements. "<" 
	 * will generate an error because the parser interprets it as the start of 
	 * a new element. "&" will generate an error because the parser interprets 
	 * it as the start of an character entity.
	 * 
	 * @static
	 * @access public
	 * @example M_Helper::containsCharactersForXmlCdata('tom') = FALSE
	 * @example M_Helper::containsCharactersForXmlCdata('Tom Bauwens <tom@multimedium.be>') = TRUE
	 * @uses M_Helper::containsCharacters()
	 * @param string $string
	 * 		The string value to be evaluated
	 * @return bool $flag
	 * 		Returns TRUE if the string contains any of the special character(s),
	 * 		FALSE if not.
	 */
	public static function containsCharactersForXmlCdata($string) {
		return self::containsCharacters($string, '&<');
	}
	
	/**
	 * Get closest interval number
	 * 
	 * This method will round the provided number to the "closest
	 * neighbour". This closest neighbour is defined by another number,
	 * of which a multiplying table is used to create neighbours.
	 * 
	 * @static
	 * @access public
	 * @example M_Helper::getIntegerClosestNeighbour(5, 88) = 90
	 * @example M_Helper::getIntegerClosestNeighbour(5, 51) = 50
	 * @example M_Helper::getIntegerClosestNeighbour(5, 53) = 55
	 * @example M_Helper::getIntegerClosestNeighbour(5, 2)  = 0
	 * @param integer $neighbours
	 * 		The number with which to create neighbours
	 * @param integer $number
	 * 		The number to be rounded to the closest neighbour
	 * @return integer
	 */
	public static function getIntegerClosestNeighbour($neighbours, $number) {
		return ((int) $neighbours * round((int) $number / (int) $neighbours));
	}
	
	/**
	 * Is boolean true?
	 * 
	 * This method will check if a given variable is a boolean, in the widest 
	 * meaning of the word. The following values will be considered a boolean
	 * TRUE:
	 * 
	 * - (bool) TRUE
	 * - (string) yes|YES
	 * - (int) 1
	 * 
	 * The following values will be considered a boolean FALSE:
	 * 
	 * - (bool) FALSE
	 * - (string) no|NO
	 * - (int) 0
	 * 
	 * This method is typically used to recognize boolean values in XML files,
	 * or other formats in which configuration variables are defined.
	 * 
	 * This method will return TRUE if the variable is considered a boolean TRUE,
	 * FALSE if not
	 * 
	 * @access public
	 * @param mixed $value
	 * @return bool
	 */
	public static function isBooleanTrue($value) {
		if(is_bool($value)) {
			return $value;
		} else {
			if(strtolower((string) $value) == 'true' || strtolower((string) $value) == 'yes' || $value == 1) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	/**
	 * Check if a class/object implements an interface
	 * 
	 * Will check if a given class/object implements a given interface name.
	 * Returns TRUE If the class/object implements the interface, FALSE if not.
	 * 
	 * Example 1:
	 * <code>
	 *    $rs = M_Helper::isInterface(new M_Config, 'MI_Config');
	 *    var_dump($rs);
	 * </code>
	 * 
	 * Example 1 would produce the following output:
	 * <code>
	 *    bool(true)
	 * </code>
	 * 
	 * NOTE:
	 * If the provided class/object cannot be found, this method will return FALSE.
	 * No exception is thrown. The class/object and the interface will be auto-loaded. 
	 * For more info on auto-loading, read {@link M_Loader::setAsAutoloader()}.
	 * 
	 * @access public
	 * @return bool
	 */
	public static function isInterface($object, $interface) {
		$className = is_object($object) ? get_class($object) : (string) $object;
		return (
			class_exists($className, TRUE) &&
			in_array((string) $interface, class_implements($className, TRUE))
		);
	}
	
	/**
	 * Check if an object is an instance of a given class
	 * 
	 * Will check if the given object is an instance of the given class. For
	 * example:
	 *
	 * <code>
	 *   if(! M_Helper::isInstanceOf($config, 'MI_Config')) {
	 *      echo 'not a MI_Config instance!';
	 *   }
	 * </code>
	 *
	 * Note that you can also provide with various class names. If you do, this
	 * method will check if the object is an object of either one of the provided
	 * class names. For example:
	 *
	 * <code>
	 *   if(! M_Helper::isInstanceOf($field, array('M_FieldText', 'M_FieldSelect'))) {
	 *      echo 'not an instance of M_FieldText or M_FieldSelect!';
	 *   }
	 * </code>
	 * 
	 * @access public
	 * @param mixed $object
	 * 		The object to be checked
	 * @param string $class
	 * 		The name of the class
	 * @return bool $flag
	 * 		Returns TRUE if the object is an instance of the class, FALSE if not
	 */
	public static function isInstanceOf($object, $class) {
		// If the provided class name is an array/iterator:
		if(self::isIterator($class)) {
			// For each of the class names:
			foreach($class as $currentClass) {
				// Do the check for the current class name:
				if(is_object($object) && $object instanceof $currentClass) {
					// Return TRUE if the object is an instance of the current
					// class name:
					return TRUE;
				}
			}

			// Return FALSE, if we could not find a match
			return FALSE;
		}

		// If the provided class name is not an array/iterator:
		return (is_object($object) && $object instanceof $class);
	}
	
	/**
	 * Is Iterator?
	 * 
	 * Will check if the provided argument is an iterator. Note that the following
	 * are considered iterators:
	 * 
	 * - Array
	 * - Object that implements the Iterator interface
	 * 
	 * @see M_Helper::isInterface()
	 * @access public
	 * @param mixed $var
	 * 		The variable that is to be evaluated as iterator
	 * @return bool $flag
	 * 		Returns TRUE if the argument is an iterator, FALSE if not
	 */
	public static function isIterator($var) {
		if(is_array($var)) {
			return TRUE;
		} elseif(is_object($var)) {
			return M_Helper::isInterface($var, 'Iterator');
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Is Iterator with elements of given interface?
	 * 
	 * Will check if the provided argument is an iterator. To do so, it uses
	 * {@link M_Helper::isIterator()}. Additionally, this method will make sure
	 * that all of the contained elements implement the given interface.
	 * 
	 * @see M_Helper::isIterator()
	 * @access public
	 * @param mixed $var
	 * 		The variable that is to be evaluated as iterator
	 * @param string $interface
	 * 		The name of the interface
	 * @return bool $flag
	 * 		Returns TRUE if the argument is an iterator with elements of the given
	 * 		interface, FALSE if not
	 */
	public static function isIteratorOfInterface($var, $interface) {
		if(! M_Helper::isIterator($var)) {
			return FALSE;
		}
		foreach($var as $element) {
			if(! M_Helper::isInterface($element, $interface)) {
				return FALSE;
			}
		}
		return TRUE;
	}
	
	/**
	 * Is Iterator with elements of given class?
	 * 
	 * Will check if the provided argument is an iterator. To do so, it uses
	 * {@link M_Helper::isIterator()}. Additionally, this method will make sure
	 * that all of the contained elements are instances of a given class name.
	 *
	 * <code>
	 *   if(! M_Helper::isIteratorOfClass($configs, 'MI_Config')) {
	 *      echo 'Oops; expecting MI_Config instances';
	 *   }
	 * </code>
	 *
	 * Note that you can also provide with various class names. If you do, this
	 * method will check if the objects are of either one of the provided class
	 * names. For example:
	 *
	 * <code>
	 *   if(! M_Helper::isIteratorOfClass($field, array('M_FieldText', 'M_FieldSelect'))) {
	 *      echo 'Oops; expecting instances of M_FieldText or M_FieldSelect!';
	 *   }
	 * </code>
	 * 
	 * @see M_Helper::isIterator()
	 * @see M_Helper::isIteratorOfInterface()
	 * @access public
	 * @param mixed $var
	 * 		The variable that is to be evaluated as iterator
	 * @param string $class
	 * 		The name of the class
	 * @return bool $flag
	 * 		Returns TRUE if the argument is an iterator with elements of the given
	 * 		class, FALSE if not
	 */
	public static function isIteratorOfClass($var, $class) {
		if(! M_Helper::isIterator($var)) {
			return FALSE;
		}
		foreach($var as $element) {
			if(! M_Helper::isInstanceOf($element, $class)) {
				return FALSE;
			}
		}
		return TRUE;
	}
	
	/**
	 * Get a value from an array or return a default value when it doesn't exist
	 * 
	 * Will safely check if $key exists in $array
	 * 
	 * @param str|int $key
	 * @param array $array
	 * @param mixed $default
	 * 			The value which will be returned when element doesn't exist
	 */
	public static function getArrayElement($key, array $array,$default = null) {
		if (array_key_exists($key,$array)) return $array[$key];
		else return $default;
	}
	
	/**
	 * Sort a 2-dimensional array
	 *
	 * @param array $array
	 * @param string $key
	 * @param string $order ASC|DESC
	 * @return unknown
	 */
	public static function arraySortMulti($array, $key, $order = "ASC")
    {
        $tmp = array();
        foreach($array as $akey => $array2) {
            $tmp[$akey] = $array2[$key];
        }
       
        if($order == "DESC") {
        	arsort($tmp , SORT_NUMERIC );
        }
        else {
        	asort($tmp , SORT_NUMERIC );
       	}

        $tmp2 = array();       
        foreach($tmp as $key => $value) {
            $tmp2[$key] = $array[$key];
        }       
       
        return $tmp2;
    } 
	
	/**
	 * Decode a json-string
	 *
	 * Note: this will check if the php function json_decode exists, if not
	 * an internal service will be used
	 * 
	 * @param string $json
	 * @return array
	 */
	public static function jsonDecode($json) {
		
		if (function_exists('json_decode')) {
			$result = json_decode($json);
		}
		else
		{
			M_Loader::loadRelative('core/_thirdparty/json/json.class.php', true);
			$jsonService = new Services_JSON;
			$result = $jsonService->decode($json);
		}
		
		return $result;
   	}  
   	
   	/**
   	 * Encode to a json string
   	 *
	 * Note: this will check if the php function json_encode exists, if not
	 * an internal service will be used
	 * 
   	 * @param mixed $arg
   	 * @return string
   	 */
   	public static function jsonEncode($arg) {
		if (function_exists('json_encode')) {
			$result = json_encode($arg);
		}
		else
		{
			M_Loader::loadRelative('core/_thirdparty/json.class.php', true);
			$jsonService = new Services_JSON;
			$result = $jsonService->encode($arg);
		}
		
		return $result;
   	}

	/**
	 * Check if a string is UTF-8 or not
	 * 
	 * @param string $string
	 * @return bool
	 */
	public static function isUtf8($string) {
		$_is_utf8_split = 5000;
		if (strlen($string) > $_is_utf8_split) {
			// Based on: http://mobile-website.mobi/php-utf8-vs-iso-8859-1-59
			for ($i=0,$s=$_is_utf8_split,$j=ceil(strlen($string)/$_is_utf8_split);$i < $j;$i++,$s+=$_is_utf8_split) {
				if (is_utf8(substr($string,$s,$_is_utf8_split))) return true;
			}
			return false;
		} else {
			// From http://w3.org/International/questions/qa-forms-utf-8.html
			return preg_match('%^(?:
					[\x09\x0A\x0D\x20-\x7E]            # ASCII
				| [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
				|  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
				| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
				|  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
				|  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
				| [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
				|  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
			)*$%xs', $string);
		}
	}

	/**
	 * Encode Alpha ID
	 *
	 * @param int $numericId
	 * @param string $passKey
	 * @return string
	 */
	public static function encodeAlphaId($numericId, $passKey = null) {
		return self::_alphaID($numericId, false, $passKey);
	}

	/**
	 * Decode Alpha ID
	 *
	 * @param string $alphaId
	 * @param string $passKey
	 * @return int
	 */
	public static function decodeAlphaId($alphaId, $passKey = null) {
		return self::_alphaID($alphaId, true, $passKey);
	}

	/**
	 * Set an alpha id setting
	 * 
	 * Please use this in the index.php-file to avoid uppercase, lowercase or
	 * numeric characters
	 * 
	 * NOTE: PLEASE CONSIDER BEFORE CHANGING THE SETTINGS FOR AN EXISTING APPLICATION!
	 * 
	 * Example:
	 * M_Helper::setAlphaIdSetting(M_Helper::ALPHA_ID_SETTING_UPPERCASE, false);
	 * 
	 * @param type $setting
	 * @param type $flag
	 * @throws M_Exception
	 */
	public static function setAlphaIdSetting($setting, $flag) {
		// If the setting was not found: throw an exception
		if(!in_array($setting, array_keys(self::$_alphaIdSettings))) {
			throw new M_Exception(sprintf(
				'Could not find alpha ID setting "%s"',
				$setting
			));
		}
		
		// Set the setting
		self::$_alphaIdSettings[$setting] = $flag;
	}

	/**
	 * Translates a number to a short alhanumeric version
	 *
	 * Translated any number up to 9007199254740992
	 * to a shorter version in letters e.g.:
	 * 9007199254740989 --> PpQXn7COf
	 *
	 * specifiying the second argument true, it will
	 * translate back e.g.:
	 * PpQXn7COf --> 9007199254740989
	 *
	 * this function is based on any2dec && dec2any by
	 * fragmer[at]mail[dot]ru
	 * see: http://nl3.php.net/manual/en/function.base-convert.php#52450
	 *
	 * If you want the alphaID to be at least 3 letter long, use the
	 * $pad_up = 3 argument
	 *
	 * In most cases this is better than totally random ID generators
	 * because this can easily avoid duplicate ID's.
	 * For example if you correlate the alpha ID to an auto incrementing ID
	 * in your database, you're done.
	 *
	 * The reverse is done because it makes it slightly more cryptic,
	 * but it also makes it easier to spread lots of IDs in different
	 * directories on your filesystem. Example:
	 * $part1 = substr($alpha_id,0,1);
	 * $part2 = substr($alpha_id,1,1);
	 * $part3 = substr($alpha_id,2,strlen($alpha_id));
	 * $destindir = "/".$part1."/".$part2."/".$part3;
	 * // by reversing, directories are more evenly spread out. The
	 * // first 26 directories already occupy 26 main levels
	 *
	 * more info on limitation:
	 * - http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-talk/165372
	 *
	 * if you really need this for bigger numbers you probably have to look
	 * at things like: http://theserverpages.com/php/manual/en/ref.bc.php
	 * or: http://theserverpages.com/php/manual/en/ref.gmp.php
	 * but I haven't really dugg into this. If you have more info on those
	 * matters feel free to leave a comment.
	 *
	 * The following code block can be utilized by PEAR's Testing_DocTest
	 * <code>
	 * // Input //
	 * $number_in = 2188847690240;
	 * $alpha_in  = "SpQXn7Cb";
	 *
	 * // Execute //
	 * $alpha_out  = alphaID($number_in, false, 8);
	 * $number_out = alphaID($alpha_in, true, 8);
	 *
	 * if ($number_in != $number_out) {
	 *     echo "Conversion failure, ".$alpha_in." returns ".$number_out." instead of the ";
	 *     echo "desired: ".$number_in."\n";
	 * }
	 * if ($alpha_in != $alpha_out) {
	 *     echo "Conversion failure, ".$number_in." returns ".$alpha_out." instead of the ";
	 *     echo "desired: ".$alpha_in."\n";
	 * }
	 *
	 * // Show //
	 * echo $number_in." => ".$alpha_out."\n";
	 * echo $alpha_in." => ".$number_out."\n";
	 *
	 * // expects:
	 * // 2188847690240 => SpQXn7Cb
	 * // SpQXn7Cb => 2188847690240
	 *
	 * </code>
	 *
	 * @author    Kevin van Zonneveld <kevin@vanzonneveld.net>
	 * @author    Simon Franz
	 * @copyright 2008 Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
	 * @version   SVN: Release: $Id: alphaID.inc.php 344 2009-06-10 17:43:59Z kevin $
	 * @link      http://kevin.vanzonneveld.net/
	 *
	 * @param mixed   $in      String or long input to translate
	 * @param boolean $to_num  Reverses translation when true
	 * @param string  $passKey Supplying a password makes it harder to calculate the original ID
	 *
	 * @return mixed string or long
	 */
	protected static function _alphaID($in, $to_num = false, $passKey = null) {

		// Number or boolean padds the result up to a specified length*
		$pad_up = 5;

		if(!$passKey) {
			$passKey = 'multipasskey';
		}

		$index = "";

		//the characters to use
		if(self::$_alphaIdSettings[self::ALPHA_ID_SETTING_LOWERCASE]) {
			$index .= "bcdfghjklmnpqrstvwxyz";
		}if(self::$_alphaIdSettings[self::ALPHA_ID_SETTING_NUMERIC]) {
			$index .= "0123456789";
		}if(self::$_alphaIdSettings[self::ALPHA_ID_SETTING_UPPERCASE]) {
			$index .= "BCDFGHJKLMNPQRSTVWXYZ";
		}
		
		if($index == "") {
			throw new M_Exception('Cannot create an alpha ID without characters, please choose at least one of lower, upper or numeric');
		}
		
		if ($passKey !== null) {
			// Although this function's purpose is to just make the
			// ID short - and not so much secure,
			// with this patch by Simon Franz (http://blog.snaky.org/)
			// you can optionally supply a password to make it harder
			// to calculate the corresponding numeric ID

			for ($n = 0; $n<strlen($index); $n++) {
				$i[] = substr( $index,$n ,1);
			}

			$passhash = hash('sha256',$passKey);
			$passhash = (strlen($passhash) < strlen($index))
				? hash('sha512',$passKey)
				: $passhash;

			for ($n=0; $n < strlen($index); $n++) {
				$p[] =  substr($passhash, $n ,1);
			}

			array_multisort($p,  SORT_DESC, $i);
			$index = implode($i);
		}

		$base  = strlen($index);

		if ($to_num) {
			// Digital number  <<--  alphabet letter code
			$in  = strrev($in);
			$out = 0;
			$len = strlen($in) - 1;
			for ($t = 0; $t <= $len; $t++) {
				$bcpow = bcpow($base, $len - $t);
				$out   = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
			}

			if (is_numeric($pad_up)) {
				$pad_up--;
				if ($pad_up > 0) {
					$out -= pow($base, $pad_up);
				}
			}
		} else {
			// Digital number  -->>  alphabet letter code
			if (is_numeric($pad_up)) {
				$pad_up--;
				if ($pad_up > 0) {
					$in += pow($base, $pad_up);
				}
			}

			$out = "";
			for ($t = floor(log10($in) / log10($base)); $t >= 0; $t--) {
				$a   = floor($in / bcpow($base, $t));
				$out = $out . substr($index, $a, 1);
				$in  = $in - ($a * bcpow($base, $t));
			}
			$out = strrev($out); // reverse
		}

		return $out;
	}
}