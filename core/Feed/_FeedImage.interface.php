<?php
interface MI_FeedImage {
	public function setTitle($title);
	public function setDescription($description);
	public function setUrl(M_Uri $url);
	public function setLink(M_Uri $link);
	public function setHeight($height);
	public function setWidth($width);
	public function toString();
}