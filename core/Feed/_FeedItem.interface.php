<?php
interface MI_FeedItem {
	public function setTitle($title);
	public function setDescription($description);
	public function setLink(M_Uri $link);
	public function setPublicationDate(M_Date $date);
	public function getTitle();
	public function getDescription();
	public function getLink();
	public function getPublicationDate();
	public function toString();
	public function __toString();
}