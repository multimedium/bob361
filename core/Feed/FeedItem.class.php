<?php
abstract class M_FeedItem implements MI_FeedItem {
	protected $_title;
	protected $_description;
	protected $_link;
	protected $_publicationDate;
	
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	public function setDescription($description) {
		$this->_description = (string) $description;
	}
	
	public function setLink(M_Uri $link) {
		$this->_link = $link;
	}
	
	public function setPublicationDate(M_Date $date) {
		$this->_publicationDate = $date;
	}
	
	public function getTitle() {
		return $this->_title;
	}
	
	public function getDescription() {
		return $this->_description;
	}
	
	public function getLink() {
		return $this->_link;
	}
	
	public function getPublicationDate() {
		return $this->_publicationDate;
	}
}