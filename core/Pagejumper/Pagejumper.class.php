<?php
/**
 * M_Pagejumper class
 * 
 * M_Pagejumper is used to build the page navigation in a list of
 * items.
 * 
 * @package Core
 */
class M_Pagejumper extends M_Object {
	/**
	 * The active page
	 * 
	 * @access private
	 * @see M_Pagejumper::setPage()
	 * @var integer
	 */
	private $_page;

	/**
	 * The name of the query-variable which is used in the request
	 *
	 * @var string
	 */
	private $_queryVariableName = 'page';
	
	/**
	 * Items per page
	 * 
	 * This property stores the number of items that are listed per
	 * page. Read more at {@link M_List::setNumberOfItemsPerPage()}.
	 * 
	 * This value of this property is defaulted to 10.
	 * 
	 * @access private
	 * @see M_Pagejumper::setNumberOfItemsPerPage()
	 * @var integer
	 */
	private $_itemsPerPage = 10;
	
	/**
	 * Total number of items
	 * 
	 * This property stores the total number of items that are being 
	 * listed, through all pages. For more information, read docs on
	 * {@link M_List::setTotalCount()}.
	 * 
	 * @access private
	 * @see M_Pagejumper::setTotalCount()
	 * @var integer
	 */
	private $_total;
	
	/**
	 * The (maximum) number of pages
	 * 
	 * The maximum number of pages to be shown in the page jumper
	 * interface. To set the value of this property, use the method
	 * {@link M_Pagejumper::setNumberOfPages()}.
	 * 
	 * This value of this property is defaulted to 11.
	 * 
	 * @access private
	 * @see M_Pagejumper::setNumberOfPages()
	 * @var integer
	 */
	private $_numberOfPages = 11;
	
	/**
	 * Align active page
	 * 
	 * The value of this property defines how the active page number
	 * is aligned in the page jumper interface. Possible values:
	 * 
	 * <code>
	 *    left
	 *    middle (default)
	 * </code>
	 * 
	 * @access private
	 * @see M_Pagejumper::alignActivePage()
	 * @var string
	 */
	private $_alignActivePage = 'middle';
	
	/**
	 * Base URI
	 * 
	 * @see M_Pagejumper::setBaseUri()
	 * @access private
	 * @var M_Uri
	 */
	private $_uri;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @see M_Pagejumper::setPage()
	 * @see M_Pagejumper::setItemsPerPage()
	 * @see M_Pagejumper::setTotalCount()
	 * @param integer $itemsPerPage
	 * 		The (maximum) number of items per page. For more info, 
	 * 		see {@link M_Pagejumper::setNumberOfItemsPerPage()}
	 * @param integer $total
	 * 		The total number of listed items. For more information,
	 * 		see {@link M_Pagejumper::setTotalCount()}
	 * @param integer $page
	 * 		The active page number; see {@link M_Pagejumper::setPage()}
	 * @return M_Pagejumper
	 */
	public function __construct($itemsPerPage = NULL, $total = NULL, $page = NULL) {
		if(! is_null($page)) {
			$this->_page = (int) $page;
		}
		if(! is_null($itemsPerPage)) {
			$this->_itemsPerPage = (int) $itemsPerPage;
		}
		if(! is_null($total)) {
			$this->_total = (int) $total;
		}
	}
	
	/**
	 * Sets the active page
	 * 
	 * This method sets the active page in the page jumper navigation.
	 * The active page is the page number that has been selected.
	 * 
	 * @access public
	 * @param integer $number
	 * 		The active page number
	 * @return void
	 */
	public function setPage($number) {
		$this->_page = (int) $number;
	}

	/**
	 * Set the name of the query variable which is used in the request
	 *
	 * Default set to "page", you can choose the name of the query variable.
	 * E.g. if you set the query variable-name to "project", the href will be
	 * www.yourdomain.com/a-page?project=x
	 *
	 * @return void
	 * @author Ben Brughmans
	 */
	public function setQueryVariableName($arg) {
		$this->_queryVariableName = (string)$arg;
	}
	
	/**
	 * Set Base URI
	 * 
	 * This method allows to set the URI that will serve as a base to compose the
	 * link for each page in the page jumper. Page numbers will be added to this 
	 * URI, with the query variable as specified in {@link M_Pagejumper::setQueryVariableName()}
	 * 
	 * @access public
	 * @param M_Uri $uri
	 * @return M_Pagejumper
	 */
	public function setBaseUri(M_Uri $uri) {
		$this->_uri = $uri;
		return $this;
	}
	
	/**
	 * Set the number of items per page
	 * 
	 * This method sets the (maximum) number of items that is being
	 * displayed per page. M_Pagejumper needs this information to
	 * correctly calculate the number of pages.
	 * 
	 * @access public
	 * @param integer $number
	 * 		The (maximum) number of items per page
	 * @return void
	 */
	public function setNumberOfItemsPerPage($number) {
		$this->_itemsPerPage = (int) $number;
	}
	
	/**
	 * Set total count
	 * 
	 * This method will set the total number of items that are being
	 * listed through all pages. M_Pagejumper needs this information 
	 * to correctly calculate the number of pages.
	 * 
	 * @access public
	 * @param integer $number
	 * 		The total number of listed items
	 * @return void
	 */
	public function setTotalCount($number) {
		$this->_total = (int) $number;
	}
	
	/**
	 * Set number of pages
	 * 
	 * This method will set the (maximum) number of pages numbers that are to be 
	 * included in the page jumper interface. This method is a setter of 
	 * {@link M_Pagejumper::$_numberOfPages}.
	 * 
	 * @access public
	 * @param integer $number
	 * 		the (maximum) number of pages
	 * @return void
	 */
	public function setNumberOfPages($number) {
		$this->_numberOfPages = (int) $number;
	}
	
	/**
	 * Get the number of pages
	 *
	 * The number of pages is set through {@link M_Pagejumper::setNumberOfPages()},
	 * but depending on the total amount of items, this can exceed the total amount
	 * of available pages.
	 *
	 * Example 1
	 * <code>
	 *    $pagejumper = new M_Pagejumper(1, 10);
	 *    $pagejumper->setNumberOfPages(3);
	 *    $pagejumper->getNumberOfPagesVisible();
	 * </code>
	 * ... will return 3
	 *
	 * Example 2
	 * <code>
	 *    $pagejumper = new M_Pagejumper(1, 10);
	 *    $pagejumper->setNumberOfPages(100);
	 *    $pagejumper->getNumberOfPagesVisible();
	 * </code>
	 * ... will return 10, despite we have set the number of pages to 100 (there
	 * are only 10 pages to be shown)
	 *
	 * In this case we will return the total amount of available pages.
	 *
	 * @see M_Pagejumper::setNumberOfPages()
	 * @see M_Pagejumper::getTotalCountOfPages()
	 * @return int
	 * @author Ben Brughmans
	 */
	public function getNumberOfPagesVisible() {
		// Total number of pages
		$totalCountOfPages = $this->getTotalCountOfPages();

		// If the total number of pages in the list is lower than the allowed 
		// maximum of displayed pages:
		if ($totalCountOfPages < $this->_numberOfPages) {
			// Then, we return the total number of pages:
			return $totalCountOfPages;
		}

		// If not, we return the maximum number:
		return $this->_numberOfPages;
	}
	
	/**
	 * Set alignment of active page
	 * 
	 * By using this method, you can define how the active page number
	 * should be aligned in the page jumper interface. This method
	 * is a setter for {@link M_Pagejumper::$_alignActivePage}.
	 * 
	 * @access public
	 * @param string $align
	 * 		The alignment string. Possible values are: left, middle
	 * @return void
	 */
	public function alignActivePage($align) {
		$align = strtolower($align);
		if(in_array($align, array('left', 'middle'))) {
			$this->_alignActivePage = $align;
		}
	}
	
	/**
	 * Get the number of items per page
	 * 
	 * This method returns the (maximum) number of items that is being
	 * displayed per page. This number has been set previously with
	 * {@link M_Pagejumper::setNumberOfItemsPerPage()}.
	 * 
	 * Check the docs on {@link M_Pagejumper::$_itemsPerPage} for
	 * the default value of the returned number, if not set before
	 * with {@link M_Pagejumper::setNumberOfItemsPerPage()}.
	 * 
	 * @access public
	 * @return integer
	 * 		The (maximum) number of items per page
	 */
	public function getNumberOfItemsPerPage() {
		return $this->_itemsPerPage;
	}
	
	/**
	 * Get total count
	 * 
	 * @see M_PageJumper::setTotalCount()
	 * @access public
	 * @return integer
	 */
	public function getTotalCount() {
		return $this->_total;
	}
	
	/**
	 * Get number of pages
	 * 
	 * @see M_PageJumper::setTotalCount
	 * @access public
	 * @return integer
	 */
	public function getTotalCountOfPages() {
		return ceil($this->_total / $this->_itemsPerPage);
	}
	
	/**
	 * Get pages
	 * 
	 * This method will return the pages of the page jumper interface.
	 * The number of pages that is outputted by this method will 
	 * depend on:
	 * 
	 * - {@link M_Pagejumper::setNumberOfItemsPerPage()}
	 * - {@link M_Pagejumper::setTotalCount()}
	 * - {@link M_Pagejumper::setNumberOfPages()}
	 * 
	 * The result of this method is an array. Each of the elements in
	 * that array is in turn another (associative) array with values
	 * for the following keys:
	 * 
	 * <code>
	 *    Array (
	 *       Array (
	 *          [number]   => (integer),
	 *          [href]     => (string),
	 *          [range]    => Array (
	 *             [0] => (integer),
	 *             [1] => (integer)
	 *          ),
	 *          [selected] => (integer)
	 *       ),
	 *       ...
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @return array
	 */
	public function getPages() {
		$out = array();
		
		// $n is the total number of pages in the page jumper interface
		$n = $this->getTotalCountOfPages();
		
		// $a is the active page number
		$a = $this->getPage();

		// $p is the total number of pages which are shown in the page jumper interface
		$p = $this->getNumberOfPagesVisible();

		// $start is the first page number in the interface.
		switch($this->_alignActivePage) {
			case 'left':
				$start = $a - 1;
				break;
			
			case 'middle':
				$start = $a - ceil($p / 2);
				if($start < 0) {
					$start = 0;
				}
				//check if we are still showing defined number of pages
				if( ($this->_total - $start) < $p) {
					//if not: adjust start position
					$start = $this->_total - $p;
				}
				break;
		}
		
		// $count is the number of page numbers in the interface.
		$count = $start + $this->_numberOfPages;
		if($count > $n) {
			$count = $n;
		}

		// If we are about to render an insufficient number of pages, we rewind
		// to a lower starting page number. Of course, we only do this if the
		// start page is actually higher than 0 (the first page).
		if(($count - $start) < $this->_numberOfPages && $start > 0) {
			// Rewind, in order to show the requested number of pages
			$start = $count - $this->_numberOfPages;

			// Make sure we don't go negative :)
			if($start < 0) {
				$start = 0;
			}
		}
		
		// Mount an M_Uri object out of the page request. We'll be
		// using this object to compose each page number's URL
		$uri = M_Request::getUri();
		
		// We add the page variables that may have been added through
		// POST method (via form). This is done to maintain parameters
		// in eg. search results page.
		if(M_Request::getMethod() == M_Request::POST) {
			foreach(M_Request::getVariables() as $name => $value) {
				$uri->setQueryVariable($name, $value);
			}
		}
		
		// for each of the pages:
		for($i = $start; $i < $count; $i ++) {
			$j = $i + 1;
			
			// Compose URL:
			$uri->setQueryVariable($this->_queryVariableName, $j);
			
			// add page to output:
			$out[] = array(
				'number'   => $j,
				'uri'      => $uri,
				'href'     => $uri->getUri(),
				'range'    => $this->getRange($j),
				'selected' => ($j == $a)
			);
		}
		
		// return the result:
		return $out;
	}
	
	/**
	 * Get Page Numbers
	 * 
	 * Will provide an array containing all curretly shown page numbers. For
	 * example:
	 * 
	 * <code>
	 *	array(2, 3, 4, 5, 6);
	 * </code>
	 * 
	 * @access public
	 * @return array
	 */
	public function getPageNumbers() {
		// Output array
		$out = array();
		
		// For each set page
		foreach($this->getPages() as $page) {
			// Add its page number to the output array
			$out[] = $page['number'];
		}
		
		// Return the array
		return $out;
	}
	
	/**
	 * Get page number
	 * 
	 * This method will return the active page number. If no page has been set 
	 * previously with {@link M_Pagejumper::setPage()}, this method will check 
	 * for the value of the request variable which is set in 
	 * {@link M_Pagejumper::_queryVariableName}. If no value is there either, 
	 * the page number is defaulted to 1.
	 * 
	 * NOTE:
	 * To fetch the page from the request variables, this method only checks GET 
	 * variables, even if the page has been requested via the POST method.
	 * 
	 * @access public
	 * @uses M_Request::getVariable()
	 * @return integer
	 */
	public function getPage() {
		if($this->_page) {
			return $this->_page;
		} else {
			return M_Request::getVariable($this->_queryVariableName, 1, M_Request::TYPE_INT, M_Request::GET);
		}
	}
	
	/**
	 * Get next page number
	 * 
	 * This method will return the page number of the next page in the navigation.
	 * If no next page number is available, this method will return NULL instead.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getPageNext() {
		return ($this->isLastPage() ? NULL : $this->getPage() + 1);
	}
	
	/**
	 * Get previous page number
	 * 
	 * This method will return the page number of the previous page in the 
	 * navigation. If no next page number is available, this method will return 
	 * NULL instead.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getPagePrevious() {
		return ($this->isFirstPage() ? NULL : $this->getPage() - 1);
	}
	
	/**
	 * Get range
	 * 
	 * This method will return the range of instances that is 
	 * displayed on a given page number. Typically, the result of this
	 * method is used to form the "Showing x to x from x" phrase.
	 * 
	 * @access public
	 * @param integer $page
	 * 		The page number of which to return the range
	 * @return array
	 */
	public function getRange($page = NULL) {
		if(is_null($page)) {
			$page = $this->getPage();
		}
		$e = $page * $this->_itemsPerPage;
		if($e > $this->_total) {
			$e = $this->_total;
		}
		return array((($page - 1) * $this->_itemsPerPage) + 1, $e);
	}
	
	/**
	 * Get offset of item in page
	 * 
	 * This method will calculate the offset, relative to the beginning of the
	 * list, for a given item. The item is identified by its index number (starting
	 * at ZERO) in the current page.
	 * 
	 * @access public
	 * @param integer $index
	 * @return integer
	 */
	public function getOffsetOfItemAt($index) {
		return ((($this->getPage() - 1) * $this->_itemsPerPage) + $index);
	}
	
	/**
	 * Get number of remaining items
	 * 
	 * Will take into account the page that is showing currently, and the number
	 * of items that have been shown in the list. Accordingly, this method will
	 * return the total number of items that is shown in higher page numbers.
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @return integer
	 */
	public function getTotalCountRemaining() {
		// Get the range of the active page:
		$range = $this->getRange();
		
		// Calculate the number of remaining items:
		$count = $this->getTotalCount() - $range[1];
		
		// Return the number:
		return ($count > 0 ? $count : 0);
	}
	
	/**
	 * Get number of items in previous page
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @return integer
	 */
	public function getNumberOfItemsInNextPage() {
		$remaining = $this->getTotalCountRemaining();
		$itemsPerPage = $this->getNumberOfItemsPerPage();
		return ($remaining < $itemsPerPage ? $remaining : $itemsPerPage);
	}
	
	/**
	 * Get number of items in next page
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @return integer
	 */
	public function getNumberOfItemsInPreviousPage() {
		$previousCount = $this->getTotalCountInPreviousPages();
		$itemsPerPage = $this->getNumberOfItemsPerPage();
		return ($previousCount < $itemsPerPage ? $previousCount : $itemsPerPage);
	}
	
	/**
	 * Get number of previous items in the list
	 * 
	 * Will take into account the page that is showing currently, and the number
	 * of items that have been shown so far. Accordingly, this method will return
	 * the total number of items that are shown BEFORE the current page.
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @return integer
	 */
	public function getTotalCountInPreviousPages() {
		// Get the range of the active page:
		$range = $this->getRange($this->getPagePrevious());
		
		// Calculate the number of previous items:
		return $range[1];
	}
	
	/**
	 * Check if this is the first page
	 * 
	 * Will return TRUE if this page is the first page available
	 *
	 * @return bool
	 */
	public function isFirstPage() {
		return $this->getPage() == 1; 
	}
	
	/**
	 * Check if this is the last page
	 * 
	 * Will return TRUE if this page is the last page available
	 *
	 * @return bool
	 */
	public function isLastPage() {
		return $this->getPage() == $this->getTotalCountOfPages();
	}
	
	/**
	 * Get Base URI
	 * 
	 * Will return the Base URI; see {@link M_Pagejumper::setBaseUri()}. Note that,
	 * if no URI has been defined, this method will return a copy of the current
	 * Request URI.
	 * 
	 * @access public
	 * @return M_Uri
	 */
	public function getBaseUri() {
		if(! $this->_uri) {
			return M_Request::getUri();
		} else {
			return $this->_uri;
		}
	}
	
	/**
	 * Get URI for specific page number
	 * 
	 * @access public
	 * @return M_Uri
	 */
	public function getUriForPageNumber($number) {
		$uri = $this->getBaseUri();
		$uri->setQueryVariable($this->_queryVariableName, (int) $number);
		return $uri;
	}
	
	/**
	 * Get view
	 * 
	 * This method will return the {@link M_View} object that's 
	 * responsible for rendering the page jumper interface.
	 * 
	 * Example 1, render and show page jumper interface:
	 * <code>
	 *    $pages = new M_Pagejumper(1, 10, 100);
	 *    $pages->getView()->display();
	 * </code>
	 * 
	 * @access public
	 * @return M_PagejumperView
	 */
	public function getView() {
		$view = new M_PagejumperView;
		$view->setPagejumper($this);
		return $view;
	}
}