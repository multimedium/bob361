<?php
/**
 * M_Messenger class
 * 
 * Think of M_Messenger as a person who carries a message; it is
 * employed to deliver messages. Typically, these messages describe 
 * the result (success or failure) of the user's performed actions. 
 * For example, an {@link M_Form} may want set a "thank you" message 
 * if the form has been completed successfully.
 * 
 * @package Core
 */
class M_Messenger extends M_ObjectSingleton {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Message type
	 * 
	 * This constant is used to address a message type. This message
	 * type is typically used to describe a successful outcome of the
	 * user's actions.
	 */
	const SUCCESS = 'success';
	
	/**
	 * Message type
	 * 
	 * This constant is used to address a message type. This message
	 * type is typically used to describe the failure to complete the
	 * user's request.
	 */
	const FAILURE = 'failure';
	
	/**
	 * The singleton object
	 * 
	 * @static
	 * @access private
	 * @var M_Messenger
	 */
	private static $_instance;
	
	/**
	 * The messages
	 * 
	 * The messages are stored in the current client's session, so we 
	 * use {@link M_SessionNamespace} to store the messages.
	 * 
	 * @access private
	 * @var M_SessionNamespace
	 */
	private $_db;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * (Protected) Constructor
	 * 
	 * The signature on the M_Messenger class constructor is PROTECTED.
	 * This way, we force the M_Messenger object to be a singleton, as
	 * objects can only be instantiated by the class itself.
	 * 
	 * To get the singleton object, use {@link M_Messenger::getInstance()}.
	 * 
	 * @see M_Messenger::getInstance()
	 * @access protected
	 * @return M_Messenger
	 */
	protected function __construct() {
		$this->_db = new M_SessionNamespace('m_messenger');
		if(!isset($this->_db->messages)) {
			$this->_db->messages = array();
		}
	}
	
	/**
	 * Singleton constructor
	 * 
	 * @static
	 * @access public
	 * @return M_Messenger $messenger
	 * 		The singleton object
	 */
	public static function getInstance() {
		// If not requested before
		if(! self::$_instance) {
			// Then, construct now:
			self::$_instance = new self;
		}
		
		// Return the singleton object
		return self::$_instance;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get messages
	 * 
	 * This method will return the collection of message(s) that have
	 * been added to {@link M_Messenger}. The return value of this
	 * method is an array, with the following format:
	 * 
	 * <code>
	 *    Array (
	 *       0 => #message,
	 *       1 => #message,
	 *       ...
	 *    )
	 * </code>
	 * 
	 * Each of the messages are objects that contain the following
	 * public properties:
	 * 
	 * <code>
	 *    flag
	 *    title
	 *    description
	 * </code>
	 * 
	 * So, you could display the messages with the following example
	 * code:
	 * 
	 * Example 1
	 * <code>
	 *    $messenger = M_Messenger::getInstance();
	 *    $messenger->addMessage('Thank You!', M_Messenger::SUCCESS);
	 *    $messenger->addMessage('Thank You Again!', M_Messenger::SUCCESS);
	 *    
	 *    foreach($messenger->getMessages() as $message) {
	 *       echo '<h1>' . $message->title . '</h1>';
	 *       echo '<p>'  . $message->description . '</p>';
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @return array
	 */
	public function getMessages() {
		$out = array();
		$tmp = $this->_db->messages;
		foreach($tmp as $message) {
			if(is_array($message[1])) {
				$out[] = (object) array(
					'title' => $message[1][0],
					'description' => $message[1][1],
					'flag' => $message[0]
				);
			} else {
				$out[] = (object) array(
					'title' => '',
					'description' => (string) $message[1],
					'flag' => $message[0]
				);
			}
		}
		return $out;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Add message
	 * 
	 * This method will add a new message, to be delivered by the
	 * {@link M_Messenger}. To obtain the messages that have been
	 * passed to M_Messenger, use {@link M_Messenger::getMessages()}.
	 * 
	 * You can also describe the type of the message, by adding a
	 * second argument to this method. These message descriptions are
	 * custom (no extra logic is added to process message types). 
	 * However, {@link M_Messenger} does provide with some commonly
	 * used descriptions, such as:
	 * 
	 * - {@link M_Messenger::SUCCESS}
	 * - {@link M_Messenger::FAILURE}
	 * 
	 * Example 1, set satisfactory result message
	 * <code>
	 *    $messenger = M_Messenger::getInstance();
	 *    $messenger->addMessage(t('Thank you'), M_Messenger::SUCCESS);
	 * </code>
	 * 
	 * You have the option to distinguish between the title and
	 * description of the message, by providing an array instead of
	 * a string.
	 * 
	 * Example 2
	 * <code>
	 *    $messenger = M_Messenger::getInstance();
	 *    $messenger->addMessage(array(
	 *       // Message title
	 *       t('Thank you'),
	 *       // Message description
	 *       t('Your request has been completed successfully')
	 *    ), M_Messenger::SUCCESS);
	 * </code>
	 * 
	 * NOTE:
	 * Since the messenger is typically used to deliver messages to 
	 * the user's interface, you should probably pass in translated 
	 * strings. For more info on translated strings, read the docs on 
	 * {@link M_LocaleMessageCatalog}.
	 * 
	 * @access public
	 * @param string $message
	 * 		The message to be delivered to the interface
	 * @param string $flag
	 * 		The message type
	 * @return void
	 */
	public function addMessage($msg, $flag = NULL) {
		$tmp = $this->_db->messages;
		array_push($tmp, array((string) $flag, $msg));
		$this->_db->messages = $tmp;
	}
	
	/**
	 * Clear messages
	 * 
	 * This method will clear all messages that may have been passed
	 * to {@link M_Messenger}.
	 * 
	 * @access public
	 * @return void
	 */
	public function clearMessages() {
		$this->_db->messages = array();
	}
}