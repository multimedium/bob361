<?php
/**
 * MI_AuthResult interface
 * 
 * MI_AuthResult dictates the public API that must be implemented by an 
 * authentication result. Such an authentication result is returned by
 * {@link MI_AuthAttempt::authenticate()}.
 * 
 * @package Core
 */
interface MI_AuthResult {
	/**
	 * Result Code
	 * 
	 * This constant is used to describe the result of the authentication process.
	 * See {@link MI_AuthResult::getCode()} for more information.
	 */
	const SUCCESS = 1;
	
	/**
	 * Result Code
	 * 
	 * This constant is used to describe the result of the authentication process.
	 * See {@link MI_AuthResult::getCode()} for more information.
	 */
	const FAILURE = 2;
	
	/**
	 * Result Code
	 * 
	 * This constant is used to describe the result of the authentication process.
	 * See {@link MI_AuthResult::getCode()} for more information.
	 */
	const FAILURE_IDENTITY_NOT_FOUND = 3;
	
	/**
	 * Result Code
	 * 
	 * This constant is used to describe the result of the authentication process.
	 * See {@link MI_AuthResult::getCode()} for more information.
	 */
	const FAILURE_IDENTITY_AMBIGUOUS = 4;
	
	/**
	 * Result Code
	 * 
	 * This constant is used to describe the result of the authentication process.
	 * See {@link MI_AuthResult::getCode()} for more information.
	 */
	const FAILURE_PASSWORD_INVALID = 5;
	
	/**
	 * Check if valid
	 * 
	 * Will tell if the authentication has been completed successfully. Used to
	 * check if the person/entity has been identified.
	 * 
	 * @access public
	 * @return boolean $flag
	 * 		Returns TRUE if identified correctly, FALSE if not
	 */
	public function isValid();
	
	/**
	 * Get identity
	 * 
	 * Will provide with the identity of the person/entity that has been authenticated.
	 * Note that this will only provide with an identity, if the result of the
	 * authentication is valid; see {@link MI_AuthResult::isValid()}.
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getIdentity();
	
	/**
	 * Get code
	 * 
	 * Will return a code, which describes the result of the authentication
	 * process. A range of constants is available, which enable you to analyze
	 * the result of the authentication:
	 * 
	 * - {@link MI_AuthResult::SUCCESS}
	 * - {@link MI_AuthResult::FAILURE}
	 * - {@link MI_AuthResult::FAILURE_IDENTITY_NOT_FOUND}
	 * - {@link MI_AuthResult::FAILURE_PASSWORD_INVALID}
	 * 
	 * @access public
	 * @return integer
	 */
	public function getCode();
	
	/**
	 * Get Message
	 * 
	 * Similar to {@link MI_AuthResult::getCode()}, but will return a message
	 * instead of a code.
	 * 
	 * @access public
	 * @return string
	 */
	public function getMessage();
}