<?php
/**
 * AuthAttemptDataObjectHelper
 * 
 * @author Tim
 * @package Core
 */
class M_AuthAttemptDataObjectHelper extends M_Object {
	
	/* -- GETTERS -- */
	
	/**
	 * Get Salt For Data Object
	 * 
	 * Can be used to generate the salt for a given {@link M_DataObject}, based
	 * on the data object's alpha id. This salt may for example be used to
	 * encrypt a password using Bcrypt.
	 * 
	 * The generated salt will be 32 characters long, and contain only
	 * characters from the following range: ./0-9A-Za-z
	 * 
	 * @static
	 * @access public
	 * @param M_DataObject $dataObject
	 * @return string
	 */
	public static function getSaltForDataObject(M_DataObject $dataObject) {
		// First of all, make sure that the data object is not new, because
		// then we can't create an alpha id
		if($dataObject->isNew()) {
			throw new M_Exception(sprintf(
				'Cannot create salt for the provided M_DataObject in ' .
				'%s::%s(); the M_DataObject has no database id yet!',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Now check if the second application hash value is defined
		if(! (defined('APPLICATION_HASH2') && APPLICATION_HASH2)) {
			throw new M_Exception(sprintf(
				'Cannot create salt for the provided M_DataObject in ' .
				'%s::%s(); the required constant APPLICATION_HASH2 is not ' .
				'defined yet!',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Finally, also check if the default work factor for Bcrypt is set.
		// If it is not:
		if(! (defined('APPLICATION_WORK_FACTOR') && APPLICATION_WORK_FACTOR)) {
			throw new M_Exception(sprintf(
				'Cannot create salt for the provided M_DataObject in ' .
				'%s::%s(); the required constant APPLICATION_WORK_FACTOR ' .
				'is not defined yet!',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Fetch the alpha id of the data object and create a SHA value for it,
		// for additional security and a longer alpha id value
		$alphaId = sha1($dataObject->getAlphaId());
		
		// Create a new Bcrypt cryptographer, which we will use to create
		// the salt
		$hash = new M_HashBcrypt();
		$hash->setInput($alphaId);
		$hash->setSalt(APPLICATION_HASH2);
		$hash->setWorkFactor(APPLICATION_WORK_FACTOR);
		
		// Return the Bcrypt hash as salt
		return $hash->getHash();
	}
}