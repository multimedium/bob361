<?php
/**
 * M_AuthAttemptDataObject
 * 
 * @package Core
 */
class M_AuthAttemptDataObject implements MI_AuthAttempt {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Password
	 * 
	 * @see M_AuthAttemptDataObject
	 * @access private
	 * @var string
	 */
	private $_password;
	
	/**
	 * Data Object Profile
	 * 
	 * @see M_AuthAttemptDataObjectHash::setProfiler()
	 * @see M_AuthAttemptDataObjectHash::getProfiler()
	 * @access private
	 * @var M_AuthAttemptDataObjectProfile
	 */
	private $_profile;
	
	/* -- SETTERS -- */
	
	/**
	 * Set password
	 * 
	 * The password to be compared with the password stored in the DB.
	 * 
	 * @access public
	 * @param string $password
	 * @return M_AuthAttemptDataObject
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPassword($password) {
		// Set the property
		$this->_password = (string) $password;
		
		// Return self
		return $this;
	}
	
	/**
	 * Set Data Object Profile
	 * 
	 * @access public
	 * @param M_AuthAttemptDataObjectProfile $profile
	 * @return M_AuthAttemptDataObject
	 *		Returns itself, for a fluent programming interface
	 */
	public function setProfile(M_AuthAttemptDataObjectProfile $profile) {
		// Set the property
		$this->_profile = $profile;
		
		// Return self
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get profile
	 * 
	 * @return M_AuthAttemptDataObjectProfile
	 */
	public function getProfile() {
		return $this->_profile;
	}
	
	/**
	 * Authenticate
	 * 
	 * @see MI_AuthAttempt::authenticate()
	 * @access public
	 * @return MI_AuthResult
	 */
	public function authenticate() {
		// An instance of M_AuthAttemptDataObjectProfile should be defined in 
		// the attempt. If not so:
		if(! $this->_profile) {
			// Throw an exception to inform about the error:
			throw new M_AuthException(
				'Cannot perform attempt to authentication! Missing ' . 
				'M_AuthAttemptDataObjectProfile instance!'
			);
		}
		
		// Ask the profile for a possible candidate:
		$candidate = $this->_profile->getCandidate();
		
		// If no such candidate can be found:
		if(! $candidate) {
			// Then, authentication has failed! The reason why it has failed,
			// depends on the number of possible candidates though. If we have
			// found more than one candidate:
			if($this->_profile->getNumberOfCandidates() > 1) {
				// Then we have failed: ambiguous result!
				return new M_AuthResult(FALSE, MI_AuthResult::FAILURE_IDENTITY_AMBIGUOUS);
			}
			// If we did not find any candidate:
			else {
				// Then we have failed: no data object found!
				return new M_AuthResult(FALSE, MI_AuthResult::FAILURE_IDENTITY_NOT_FOUND);
			}
		}
		
		// If a candidate has been found, then we will need to compare the 
		// passwords. So, first of all, we get the password that is stored for 
		// the data object:
		$password = $this->_profile->getCandidatePasswordStored();
		
		// Now, compare passwords:
		if($password != $this->_password) {
			// If they do not match, we return FAILURE!
			return new M_AuthResult(FALSE, MI_AuthResult::FAILURE_PASSWORD_INVALID);
		}
		
		// If we are still here, we'll return SUCCESS! This means that the hashes
		// match and that authentication has succeeded:
		$rs = new M_AuthResult(TRUE, MI_AuthResult::SUCCESS);
		$rs->setIdentity($candidate->getId());
		return $rs;
	}
}