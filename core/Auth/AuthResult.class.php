<?php
/**
 * M_AuthResult interface
 * 
 * M_AuthResult is the default implementation of {@link MI_AuthResult}.
 * 
 * @package Core
 */
class M_AuthResult implements MI_AuthResult {
	protected $_valid = FALSE;
	protected $_identity;
	protected $_code;
	protected $_message;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param bool $isValid
	 * 		Boolean that sets result of authentication; see {@link M_AuthResult::isValid()}
	 * @param integer $code
	 * 		The code that describes the result of authentication
	 * @return M_AuthResult
	 */
	public function __construct($isValid = FALSE, $code = NULL) {
		$this->setIsValid($isValid);
		$this->setCode($code);
	}
	
	/* -- GETTERS (support for MI_AuthResult interface) -- */
	
	/**
	 * Check if valid
	 * 
	 * @see MI_AuthResult::isValid()
	 * @access public
	 * @return boolean $flag
	 * 		Returns TRUE if identified correctly, FALSE if not
	 */
	public function isValid() {
		return $this->_valid;
	}
	
	/**
	 * Get identity
	 * 
	 * @see MI_AuthResult::getIdentity()
	 * @access public
	 * @return mixed
	 */
	public function getIdentity() {
		return $this->_identity;
	}
	
	/**
	 * Get code
	 * 
	 * @see MI_AuthResult::getCode()
	 * @access public
	 * @return integer
	 */
	public function getCode() {
		return $this->_code;
	}
	
	/**
	 * Get Message
	 * 
	 * @see MI_AuthResult::getMessage()
	 * @access public
	 * @return string
	 */
	public function getMessage() {
		return $this->_message;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set flag: is valid?
	 * 
	 * @see M_AuthResult::isValid()
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE if authentication was success, FALSE if not
	 */
	public function setIsValid($flag) {
		$this->_valid = (bool) $flag;
	}
	
	/**
	 * Set identity
	 * 
	 * @see M_AuthResult::getIdentity()
	 * @access public
	 * @param mixed $identity
	 * 		The identity of the authenticated person/entity
	 * @return void
	 */
	public function setIdentity($identity) {
		$this->_identity = $identity;
	}
	
	/**
	 * Set code
	 * 
	 * @see M_AuthResult::getCode()
	 * @access public
	 * @param integer $code
	 * 		The code that describes the result of authentication
	 * @return void
	 */
	public function setCode($code) {
		$this->_code = $code;
	}
	
	/**
	 * Set message
	 * 
	 * @see M_AuthResult::getMessage()
	 * @access public
	 * @param integer $identity
	 * 		The message that describes the result of authentication
	 * @return void
	 */
	public function setMessage($message) {
		$this->_message = (string) $message;
	}
}