<?php
/**
 * M_DataObject class
 *
 * Provides an abstract implementation for data objects in the application. All
 * data objects in the application typically inherit from this abstract class and,
 * in doing so, provide with some basic functionality out of the box.
 *
 * @package Core
 */
abstract class M_DataObject extends M_Object {

	/* -- PROPERTIES -- */

	/**
	 * ID
	 *
	 * Stores the (numerical) ID of the data object.
	 *
	 * @access protected
	 * @var int
	 */
	protected $_id;
	
	/**
	 * Localized texts
	 *
	 * Stores an array with the localized texts of the data object. Is populated
	 * when a localized texts is requested from the data object
	 *
	 * @see M_DataObject::_getLocalizedText()
	 * @see M_DataObject::_setLocalizedText()
	 * @access protected
	 * @var array
	 */
	protected $_localizedTexts;

	/**
	 * The locales for which the mapper has set the localized texts
	 *
	 * For performance issues: we store the locales for which the mapper has
	 * set all localized texts.
	 *
	 * @interal: this fixed a bug which was caused whenever a non-localized
	 * called the setter of a localized field before any localized-field-setter
	 * has been called.
	 *
	 * E.g. we have 3 fields:
	 *
	 * - url is non-localized
	 * - name is localized
	 * - lastname is localized
	 *
	 * Imagine we modify the setUrl(), and add a call to the setName() in it.
	 * Now, when the object is created (using {@link M_DataObjectMapper::_updateObjectFromRecord()}
	 * setUrl will be called and thus setName() too.
	 *
	 * When, later on, we would call getName() this would always return null,
	 * since {@link M_DataObject::_getLocalizedText()} would check if
	 * the {@link M_DataObject::_localizedTexts} wasn't null anymore
	 * (because all localized texts would have been set).
	 *
	 * In fact: the only value in the localizedTexts array would be "name",
	 * because it was called in the setUrl() function.
	 *
	 * To prevent this behaviour we store the locales which have been fetched
	 * in this array.
	 *
	 * @author Ben Brughmans
	 * @var array
	 */
	protected $_localizedTextsFromMapper = array();
	
	/**
	 * Mapper
	 * 
	 * @access protected
	 * @var type 
	 */
	protected $_mapper;
	
	/* -- GETTERS -- */
	
	/**
	 * Get ID
	 *
	 * @access public
	 * @return int
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Get mapper
	 *
	 * Provides with the mapper that is typically used by the data object, in
	 * order to perform operations on the database.
	 * 
	 * IMPORTANT NOTE:
	 * If the mapper's class name differs from the class name, you need to
	 * implement your own getMapper method in the data-object
	 *
	 * @access public
	 * @return M_DataObjectMapper $mapper
	 *		The requested data object mapper
	 */
	public function getMapper() {
		$class = $this->getClassName();
		M_Loader::loadDataObjectMapper($class, $class);
		$mapperClass = $class . M_DataObjectMapper::MAPPER_SUFFIX;
		return new $mapperClass;
	}
	
	/**
	 * Get Alpha ID
	 * 
	 * Will provide the alpha id of this {@link M_DataObject}.
	 * 
	 * @see M_Helper::encodeAlphaId()
	 * @access public
	 * @return string
	 */
	public function getAlphaId() {
		// If the object does not yet have an ID:
		if($this->isNew()) {
			// Then we return NULL instead:
			return NULL;
		}
		
		// Return the encoded alpha id
		return M_Helper::encodeAlphaId(
			// Based on the data object id, as integer
			(int) $this->getId(),
			// And using the private key from the mapper
			$this->getMapper()->getPrivateKeyForAlphaIds()
		);
	}
	
	/**
	 * Get available locales
	 * 
	 * Will provide with the collection of locales for which the data object has
	 * stored localized texts.
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @return M_ArrayIterator $iterator
	 * 		A collection of locales (strings, as provided by the method
	 * 		{@link M_LocaleMessageCatalog::getInstalled()}
	 */
	public function getLocalesOfLocalizedTexts() {
		// If the object is not localized:
		if(! $this->isLocalized()) {
			// Then, we return an empty collection:
			return new M_ArrayIterator(array());
		}
		
		// No localized values have been fetched yet. Note however that we
		// do not proceed to fetching these values, if the data object is
		// new (not stored in db):
		if($this->isNew()) {
			// Return empty collection, if this is a new object:
			return new M_ArrayIterator(array());
		}

		// Populate the data object with all localized texts now:
		foreach(M_LocaleMessageCatalog::getInstalled() AS $locale) {
			if (!isset($this->_localizedTextsFromMapper[$locale])) {
				$this->_localizedTextsFromMapper[$locale] = true;
				$this->getMapper()->setLocalizedTextsIn($this, $locale);
			}
		}
		
		// We prepare the return value:
		$out = array();

		// For each of the localized fields:
		foreach((array)$this->_localizedTexts as $field) {
			// We collect the locales in which the current field is available:
			foreach($field as $locale => $value) {
				// If the locale is being supported currently, by the system,
				// and if a value is present:
				if(M_LocaleMessageCatalog::isInstalled($locale) && ! is_null($value)) {
					// If the locale has not already been added to the return
					// value:
					if(! in_array($locale, $out)) {
						// We add the locale to the output:
						$out[] = $locale;
					}
				}
			}
		}
		
		// Return the collection of locales:
		return new M_ArrayIterator($out);
	}

	/**
	 * Get localized value of property
	 *
	 * Will provide with the value of a localized property in the data object.
	 * Note that this method will look up the localized value, but will NOT take
	 * into account the priority list of locales.
	 *
	 * This method is used by M_DataObject::_getLocalizedText() for the currently
	 * active locale, and then for other locales in the priority list (if no value
	 * could have been found for the active locale)
	 *
	 * @access public
	 * @param string $property
	 * 		The name of the property
	 * @param string $locale
	 * 		The locale in which to get the property's value
	 * @return string
	 */
	public function getLocalizedProperty($property, $locale) {
		// If the localized value has been set before (is possible for objects
		// that have been provided with a localized text, before any localized
		// text is being retrieved from the object). This way, it can be possible
		// to reduce the number of SQL queries as well
		if(isset($this->_localizedTexts[$property]) && array_key_exists($locale, $this->_localizedTexts[$property])) {
			// If a value already exists, we return that value:
			return $this->_localizedTexts[$property][$locale];
		}

		// If the internal variable with localized values has not yet been
		// populated by the MAPPER:
		if(! isset($this->_localizedTextsFromMapper[$locale])) {
			// Tell the object we have fetched all locale-data with the mapper!
			// We need to do this before we call M_DataObjectMapper::setLocalizedTextIn()
			// because this method will call every setter in the object (when
			// creating the object). If a getter-method is called in one of this
			// setters, this will cause an infinite loop since a localized-getter
			// method will call _getLocalizedText again. If we set this flag, the
			// _getLocalizedText method won't call the setLocalizedTextsIn-method
			// again
			$this->_localizedTextsFromMapper[$locale] = true;

			// Then, we ask the mapper to fetch all localized values and to
			// provide this object with the values it finds...
			$this
				->getMapper()
				->setLocalizedTextsIn($this, $locale);
		}

		// If the internal variable with localized values has still not been
		// populated, it means that the mapper could not find localized values
		// for this object. In that case, we make sure that the internal variable
		// is set to an empty array, so we don't have to go through all of this
		// the next time that the property is requested from the data object.
		if($this->_localizedTexts == NULL) {
			// Then, we do so now:
			$this->_localizedTexts = array();
		}

		// Now, we check if a localized value can be found for the requested
		// property:
		if(isset($this->_localizedTexts[$property]) && isset($this->_localizedTexts[$property][$locale])) {
			// If so, we return that value:
			return $this->_localizedTexts[$property][$locale];
		}
		
		// If we are still here, it means that we did not find a localized value.
		// In that case, we return NULL as the final result:
		return NULL;
	}

	/**
	 * Set localized value of property
	 *
	 * @access public
	 * @param string $property
	 *		The name of the property
	 * @param string $value
	 *		The localized value of the property
	 * @param string $locale
	 *		The locale in which to set the localized value
	 * @return M_DataObject $object
	 *		Returns itself, for a fluent programming interface
	 */
	public function setLocalizedProperty($property, $value, $locale) {
		// If the internal variable with localized values has not been
		// populated yet
		if($this->_localizedTexts == NULL) {
			// Then, we initiate the internal variable now:
			$this->_localizedTexts = array();
		}

		// If an entry has not yet been created for this property
		if(!isset($this->_localizedTexts[$property])) {
			// Then, we initiate the entry now:
			$this->_localizedTexts[$property] = array();
		}

		// Set the localized value
		$this->_localizedTexts[$property][$locale] = $value;

		// Return myself
		return $this;
	}
	
	/**
	 * Save this object into the database
	 *
	 * @see M_DataObject::getMapper()
	 * @see M_DataObjectMapper::save()
	 * @access public
	 * @return bool
	 */
	public function save()
	{
		return $this->getMapper()->save($this);
	}
	
	/**
	 * Is new?
	 * 
	 * Returns TRUE if the object is not stored in database, FALSE if
	 * already stored in database
	 *
	 * @see M_DataObject::getMapper()
	 * @see M_DataObjectMapper::isNew()
	 * @access public
	 * @param M_DataObject $object
	 * @return bool
	 */
	public final function isNew() {
		return $this->getMapper()->isNew($this);
	}
	
	
	/**
	 * Is updated?
	 *
	 * Will check whether or not this object's data has differences, when comparing
	 * against the data that is stored in the database...
	 *
	 * @see M_DataObject::getMapper()
	 * @see M_DataObjectMapper::isUpdated()
	 * @access public
	 * @return bool $flag
	 *		Returns TRUE if the data object has been updated, FALSE if not
	 */
	public function isUpdated() {
		// Note that we can only call this method on data objects that have already
		// been stored in the database:
		if($this->getId()) {
			// If that's the case, we route the question to the mapper:
			return $this->getMapper()->isUpdated($this);
		}
		// If the data object has not yet been stored:
		else {
			// Then, we throw an exception to inform about the error:
			throw new Exception('isUpdate() called on object with no id');
		}
	}
	
	/**
	 * Is localized?
	 * 
	 * Will tell whether or not the data object contains localized fields.
	 *
	 * @see M_DataObject::getMapper()
	 * @see M_DataObjectMapper::isLocalized()
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if localized, FALSE if not
	 */
	public function isLocalized() {
		return $this->getMapper()->isLocalized();
	}
	
	/**
	 * Is localized field?
	 * 
	 * Will tell whether or not the given field of the data object is a 
	 * localized field.
	 *
	 * @see M_DataObject::getMapper()
	 * @see M_DataObjectMapper::isLocalizedField()
	 * @access public
	 * @param string $fieldName
	 * 		The name of the field
	 * @return bool $flag
	 * 		Returns TRUE if localized, FALSE if not
	 */
	public function isLocalizedField($fieldName) {
		// Get the mapper. The data object does not know about localized fields,
		// so it needs to ask the mapper for this information.
		$mapper = $this->getMapper();

		// Get information about the requested field, via the mapper:
		$field = $mapper->getField($fieldName);

		// Check if this field exists, in the data definition:
		if($field) {
			// If the field exists, then we ask the mapper to tell us whether
			// or not it is a localized field:
			return $mapper->isLocalizedField($field);
		}

		// Return the default outcome, if still here
		return false;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set ID
	 *
	 * Will set the ID for this object, this can only be done when the id hasn't
	 * been set yet
	 *
	 * @access public
	 * @param int $arg
	 * @return void
	 */
	public function setId($arg) {
		$this->_id = (int) $arg;
	}
	
	/**
	 * Insert
	 *
	 * Will insert this object into the database. In order to do so, it will
	 * ask the mapper to insert this object: {@link M_DataObject::getMapper()}
	 *
	 * @see M_DataObject::getMapper()
	 * @see M_DataObjectMapper::insert()
	 * @access public
	 * @return bool $flag
	 *		Returns TRUE on success, FALSE on failure
	 */
	public function insert() {
		return $this->getMapper()->insert($this);
	}
	
	/**
	 * Update
	 *
	 * Will update this object in the database. In order to do so, it will
	 * ask the mapper to update this object: {@link M_DataObject::getMapper()}
	 *
	 * @see M_DataObject::getMapper()
	 * @see M_DataObjectMapper::update()
	 * @access public
	 * @return bool $flag
	 *		Returns TRUE on success, FALSE on failure
	 */
	public function update() {
		return $this->getMapper()->update($this);
	}
	
	/**
	 * Delete
	 *
	 * Will delete this object from the database. In order to do so, it will
	 * ask the mapper to delete this object: {@link M_DataObject::getMapper()}
	 *
	 * @see M_DataObject::getMapper()
	 * @see M_DataObjectMapper::delete()
	 * @access public
	 * @return bool $flag
	 *		Returns TRUE on success, FALSE on failure
	 */
	public function delete() {
		return $this->getMapper()->delete($this);
	}
	
	/**
	 * Get (associative) array
	 * 
	 * Will convert the data object to an associative array. The keys
	 * of the array are the data object's field names, while the values of the
	 * array are the corresponing values of the data object fields.
	 * 
	 * @access public
	 * @return array
	 */
	public function toArray() {
		// Output array:
		$out = array();
		
		// Get the object's mapper
		$mapper = $this->getMapper();
		
		// Ask the mapper for the fields in the data object definition.
		// => For each of the fields:
		foreach($mapper->getFields() as $field) {
			// The getter for the current field is:
			$getter = (string) $field->getter;
			
			// Add the field to the output array:
			$out[(string) $field->name] = $this->$getter();
		}
		
		// Return the array:
		return $out;
	}

	/* -- MAGIC METHODS -- */

	/**
	 * Magic getter
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the field
	 * @return void
	 */
	public function __get($name) {
		$mapper = $this->getMapper();
		$f  = $mapper->getField($name);
		if($f) {
			$f = (string) $f->getter;
			return $this->$f();
		} else {
			return NULL;
		}
	}

	/**
	 * Magic setter
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the field
	 * @param string $name
	 * 		The new value of the field
	 * @return void
	 */
	public function __set($name, $value) {
		$mapper = $this->getMapper();
		$f  = $mapper->getField($name);
		if($f) {
			$f = (string) $f->setter;
			$this->$f($value);
		}
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get localized text
	 *
	 * Will provide with the value of a localized field in the data object. Typically,
	 * this method being protected, it is used by subclasses in order to retrieve
	 * a localized value in a getter method.
	 *
	 * @uses M_DataObject::getLocalizedProperty()
	 * @access protected
	 * @param string $property
	 * 		The name of the property
	 * @param string $locale
	 * 		The locale in which to get the property's value. Will default to the
	 *		locale that has been set for the category LC_MESSAGES
	 * @return string
	 */
	protected function _getLocalizedText($property, $locale = NULL) {
		// If no locale has been specified
		if(! $locale) {
			// Then, we get the currently selected one
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);

			// If still no locale has been found
			if(! $locale) {
				// Then, we cannot provide with a localized value. We return
				// NULL as the result of this method
				return NULL;
			}
		}
		
		// Try to get the property in the currently selected locale:
		$value = $this->getLocalizedProperty($property, $locale);

		// If a value has been found for the property, in this locale:
		if($value) {
			// Then, return that value:
			return $value;
		}
		
		// If we are still here, then the requested text could not have been
		// found in the requested locale. So, while a lower priority locale is
		// available:
		$i = 
			$locale == M_Locale::getLanguagePriority(0) 
				? 1 
				: 0;
		
		$locale = M_Locale::getLanguagePriority($i);
		while($locale) {
			// Try to get the property in that locale:
			$value = $this->getLocalizedProperty($property, $locale);

			// If a value has been found for the property, in this locale:
			if($value) {
				// Then, return that value:
				return $value;
			}
			
			// If not, we move to the next locale in the priority list:
			$locale = M_Locale::getLanguagePriority(++ $i);
		}
		
		// If we are still here, it means that we did not find a localized value.
		// In that case, we return NULL as the final result:
		return NULL;
	}

	/**
	 * Set localized text
	 *
	 * Will set the value of a localized field in the data object. Typically,
	 * this method being protected, it is used by subclasses in order to set
	 * the value of a localized value in a setter method.
	 *
	 * @uses M_DataObject::setLocalizedProperty()
	 * @access protected
	 * @param string $property
	 * 		The name of the property
	 * @param string $text
	 * 		The new value of the property
	 * @param string $locale
	 * 		The locale in which to get the property's value
	 * @return M_DataObject $object
	 *		Returns itself, for a fluent programming interface
	 */
	protected function _setLocalizedText($property, $text, $locale = NULL) {
		// If no locale has been specified
		if(! $locale) {
			// Then, we get the currently selected one
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);

			// If still no locale has been found
			if(! $locale) {
				// Then, we cannot set a localized value in the object! We throw
				// an exception, to inform about this error:
				throw new M_Exception(sprintf(
					'Cannot set %s in %s: no locale given and no default locale can be identified',
					$property,
					$this->getClassName()
				));
			}
		}
		
		// Set the localized property
		return $this->setLocalizedProperty($property, $text, $locale);
	}
	
	/**
	 * Set state when waking from var_export
	 * 
	 * Whenever a dataobject is constructed out of the result of var_export, 
	 * __set_state  will be called.
	 * 
	 * An array containing the variables of the object is passed. We can use
	 * the magic setter to set these variables again
	 * 
	 * @param array $array
	 */
	public function __set_state($array) {
		foreach($array AS $key => $value) {
			$this->__set($key, $value);
		}
	}
}