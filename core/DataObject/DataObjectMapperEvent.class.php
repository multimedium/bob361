<?php
/**
 * M_DataObjectMapperEvent
 * 
 * M_DataObjectMapperEvent is used to describe events that are triggered by the 
 * {@link M_DataObjectMapperEventDispatcher} class.
 * 
 * @package Core
 */
class M_DataObjectMapperEvent extends M_Event {
	/**
	 * Event Type
	 * 
	 * Triggered in {@link M_DataObjectMapper}, to notify about the fact that an 
	 * instance of {@link M_DataObject} is about to be edited.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_BEFORE_UPDATE = 'data-object-before-update';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link M_DataObjectMapper}, to notify about the fact that an 
	 * instance of {@link M_DataObject} has been edited.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_UPDATE = 'data-object-update';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link M_DataObjectMapper}, to notify about the fact that an
	 * instance of {@link M_DataObject} is about to be created.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_BEFORE_INSERT = 'data-object-before-insert';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link M_DataObjectMapper}, to notify about a {@link M_DataObject}
	 * having been created.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_INSERT = 'data-object-insert';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link M_DataObjectMapper}, to notify about the fact that an
	 * instance of {@link M_DataObject} is about to be deleted.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_BEFORE_DELETE = 'data-object-before-delete';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link M_DataObjectMapper}, to notify about a {@link M_DataObject}
	 * having been deleted.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_DELETE = 'data-object-delete';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link M_DataObjectMapper}, to notify about the fact that the
	 * mapper is about to be truncated.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_MAPPER_BEFORE_TRUNCATE = 'data-object-mapper-before-truncate';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link M_DataObjectMapper}, to notify about the mapper having
	 * been truncated
	 * 
	 * @var string
	 */
	const DATA_OBJECT_MAPPER_TRUNCATE = 'data-object-mapper-truncate';
	
	/* -- PROPERTIES -- */
	
	/**
	 * DataObject
	 * 
	 * This property stores the data-object for which the event is being triggered
	 * 
	 * @see M_DataObjectMapperEvent::getDataObject()
	 * @see M_DataObjectMapperEvent::setDataObject()
	 * @access private
	 * @var M_DataObject
	 */
	private $_dataObject;
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $type
	 * 		The event type, e.g. {@link M_DataObjectMapperEvent::DATA_OBJECT_DELETE}
	 * @param object $target
	 * 		The event target; the object that triggered the event (an instance
	 * 		of {@link M_DataObjectMapper}).
	 * @return M_DataObjectMapperEvent
	 */
	public function __construct($type, M_DataObjectMapper $target) {
		parent::__construct($type, $target);
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get {@link M_DataObject} instance
	 * 
	 * This method will provide with the instance of {@link M_DataObject} for
	 * which the event has been triggered. By releasing the {@link M_DataObject},
	 * other objects can react to significant events related to that object.
	 * 
	 * @see M_DataObjectMapperEvent::getDataObjectMapper()
	 * @access public
	 * @return M_DataObject
	 */
	public function getDataObject() {
		return $this->_dataObject;
	}
	
	/**
	 * Get {@link M_DataObjectMapper} instance
	 * 
	 * This method will provide with the instance of {@link M_DataObjectMapper}
	 * in which the event took place.
	 * 
	 * @see M_DataObjectMapperEvent::getDataObject()
	 * @uses M_Event::getTarget()
	 * @access public
	 * @return M_DataObjectMapper
	 */
	public function getDataObjectMapper() {
		return $this->getTarget();
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set {@link M_DataObject} instance
	 * 
	 * This method allows you to set the instance of {@link M_DataObject} for
	 * which the event is being triggered.
	 * 
	 * @see M_DataObjectMapperEvent::getDataObject()
	 * @access public
	 * @param M_DataObject $object
	 * @return void
	 */
	public function setDataObject(M_DataObject $object) {
		$this->_dataObject = $object;
	}
}