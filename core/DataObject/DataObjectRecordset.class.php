<?php
class M_DataObjectRecordset extends M_ArrayIterator {
	/**
	 * @var M_DbResultAdo
	 */
	private $_resultset;

	/**
	 * Store DataObjectMapper
	 * 
	 * We need to store the DataObjectMapper so we can create an instance of
	 * {@link M_DataObject} from a record
	 *
	 * @var M_DataObjectMapper
	 */
	private $_mapper;

	/**
	 * Create a
	 * @param MI_DbResult $resultset
	 */
	public function __construct(MI_DbResult $resultset, M_DataObjectMapper $mapper) {
		$this->_resultset = $resultset;
		$this->_mapper = $mapper;
	}

	/**
	 * Get the first record
	 *
	 * @return M_DataObject
	 */
	public function getOne() {
		return $this->current();
	}

	/**
	 * Download all records
	 *
	 * @return array
	 */
	public function getArrayCopy() {
		$all = array();

		$this->rewind();
		while($this->valid())
		{
			$all[] = $this->current();
			$this->next();
		}

		return $all;
	}

	/**
	 * Get first element of recordset
	 * 
	 * @return M_DataObject
	 */
	public final function first() {
		$this->rewind();
		return $this->getOne();
	}

	/**
	 * Get last element of recordset
	 * 
	 * @return M_DataObject
	 */
	public final function last() {
		$this->_resultset->end();
		return $this->current();
	}

	/**
	 * implementation of Countable
	 *
	 * @return int
	 */
	public final function count() {
		return $this->_resultset->count();
	}

	/**
	 * Get current key
	 *
	 * @return int
	 */
	public final function key() {
		return $this->_resultset->key();
	}

	/**
	 * Move to next record
	 *
	 * @return void
	 */
	public final function next() {
		$this->_resultset->next();
	}

	/**
	 * Rewind recordset
	 *
	 * @return void
	 */
	public final function rewind() {
		$this->_resultset->rewind();
	}

	/**
	 * Check if recordset is still valid
	 * 
	 * @return bool implementation of Iterator
	 */
	public final function valid() {
		return $this->_resultset->valid();
	}

	/**
	 * Get current object
	 *
	 * @return M_DataObject
	 */
	public final function current() {
		//result found?
		$current = $this->_resultset->current();
		
		//if yes: create object
		if ($current) {
			$current = $this->_mapper->createObjectFromRecord($current);
		}

		return $current;
	}

	/**
	 * Append
	 *
	 * NOTE: you cannot append data to an existing recordset
	 * 
	 * @param M_DataObject $object
	 */
	public final function append(M_DataObject $object) {
		throw new M_Exception('Cannot append object to DataObjectRecordSet');
	}
}