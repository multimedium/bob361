<?php
/**
 * M_MenuItem
 * 
 * M_MenuItem is used to describe menu items. It provides with tools to create
 * a hierarchy of menu items, fetch a breadcrumb, get a sitemap, etc.
 * 
 * @package Core
 */
class M_MenuItem extends M_Object {
	/**
	 * Parent
	 * 
	 * This property stores the menu item's parent. The parent is represented
	 * by an instance of {@link M_MenuItem}.
	 * 
	 * @access private
	 * @var M_MenuItem
	 */
	private $_parent;
	
	/**
	 * Title
	 * 
	 * This property stores the menu item's title.
	 * 
	 * @access private
	 * @var string
	 */
	private $_title;
	
	/**
	 * Description
	 * 
	 * This property stores the menu item's description.
	 * 
	 * @access private
	 * @var string
	 */
	private $_description;
	
	/**
	 * Batch Number
	 * 
	 * This property stores the menu item's batch number.
	 * 
	 * @access private
	 * @var string
	 */
	private $_batchNumber;
	
	/**
	 * CSS
	 * 
	 * @see M_MenuItem::setCss()
	 * @access private
	 * @var M_ElementCss
	 */
	private $_css;
	
	/**
	 * Path
	 * 
	 * This property stores the menu item's (relative) path.
	 * 
	 * @access private
	 * @var string
	 */
	private $_path;
	
	/**
	 * URI
	 * 
	 * @see M_MenuItem::setUri()
	 * @access private
	 * @var M_Uri
	 */
	private $_uri;
	
	/**
	 * Target
	 * 
	 * @see M_MenuItem::setTarget()
	 * @access private
	 * @var string
	 */
	private $_target;
	
	/**
	 * Path, populated by Data Object
	 * 
	 * This property stores the menu item's (relative) path, which has been 
	 * populated by a data object. For more info, read:
	 * 
	 * - {@link M_MenuItem::setDataObjectsWithPath()}
	 * - {@link M_MenuItem::setDataObjects()}
	 * 
	 * @access private
	 * @var string
	 */
	private $_pathPopulated;
	
	/**
	 * Data Object
	 * 
	 * This property stores information about the data objects that are being
	 * used to interpret the menu item.
	 * 
	 * @access private
	 * @var array
	 */
	private $_dataObject = array();
	
	/**
	 * Data Object
	 * 
	 * This property stores information about the data object mappers that are 
	 * being used to interpret the menu item.
	 * 
	 * @access private
	 * @var array
	 */
	private $_dataObjectMapper = array();
	
	/**
	 * Menu items
	 * 
	 * Each instance of {@link M_MenuItem} may contain a submenu. Such a submenu
	 * is represented by a collection of {@link M_MenuItem} instances.
	 * 
	 * @access private
	 * @var array
	 */
	private $_items = array();
	
	/**
	 * Flag: is this MenuItem active or not
	 *
	 * @var bool
	 */
	private $_active;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $path
	 * @param string $title
	 * @param string $description
	 * @param string $batchNumber
	 * @return M_MenuItem
	 */
	public function __construct($path = NULL, $title = NULL, $description = NULL, $batchNumber = NULL) {
		if($path) {
			$this->setPath($path);
		}
		if($title) {
			$this->setTitle($title);
		}
		if($description) {
			$this->setDescription($description);
		}
		if($batchNumber) {
			$this->setBatchNumber($batchNumber);
		}
	}
	
	/**
	 * Construct with {@link M_Uri}
	 * 
	 * @static
	 * @access public
	 * @param M_Uri $uri
	 * @param string $title
	 * @param string $description
	 * @param string $batchNumber
	 * @return M_MenuItem
	 */
	public static function constructWithUri(M_Uri $uri, $title = NULL, $description = NULL, $batchNumber = NULL) {
		$item = new self;
		$item->setUri($uri);
		$item->setTitle($title);
		$item->setDescription($description);
		$item->setBatchNumber($batchNumber);
		return $item;
	}
	
	/**
	 * Construct with SimpleXMLElement
	 * 
	 * Will construct an instance of {@link M_MenuItem} out of a given instance
	 * of {@link SimpleXMLElement}.
	 * 
	 * The following XML structure may be used, in order to construct an instance
	 * of {@link M_MenuItem}:
	 * 
	 * Example 1
	 * <code>
	 *    <item>
	 *       <title>News</title>
	 *       <description>The latest headlines in my website</description>
	 *       <path>news</path>
	 *       <item>
	 *          <title>@title</title>
	 *          <description></description>
	 *          <path>news/@url</path>
	 *          <dataObject>
	 *             <module>news</module>
	 *             <dataObjectId>category</dataObjectId>
	 *          </dataObject>
	 *          <item>
	 *             <title>@title</title>
	 *             <description></description>
	 *             <path>news/@url/@url</path>
	 *             <dataObject>
	 *                <module>news</module>
	 *                <dataObjectId>category</dataObjectId>
	 *             </dataObject>
	 *             <dataObject>
	 *                <module>news</module>
	 *                <dataObjectId>news</dataObjectId>
	 *             </dataObject>
	 *          </item>
	 *       </item>
	 *    </item>
	 * </code>
	 * 
	 * @access public
	 * @param SimpleXMLElement $element
	 * @return M_MenuItem
	 */
	public static function constructWithSimpleXMLElement(SimpleXMLElement $element) {
		// Try block:
		try {
			// Make sure the element specifies the path:
			if(! isset($element->path)) {
				// If it doesn't, we throw an exception:
				throw new M_MenuException(sprintf(
					'Missing path!'
				));
			}
			
			// Construct a new M_MenuItem instance:
			$item = new self((string) $element->path);
			
			// Has a title been given?
			if(isset($element->title)) {
				$item->setTitle((string) $element->title);
			}
			
			// Has a description been given?
			if(isset($element->description)) {
				$item->setDescription((string) $element->description);
			}
			
			// Prepare an array of Data Object Mappers:
			$mappers = array();
			
			// For each of the Data Object definitions
			foreach($element->xpath('dataObject') as $do) {
				// If so, it must specify the Data Object ID and, optionally, the
				// Application Module's ID
				$isDoId = (M_Helper::isInstanceOf($do, 'SimpleXMLElement') && isset($do->dataObjectId) && ! empty($do->dataObjectId));
				$isModuleId = ($isDoId && isset($do->module) && ! empty($do->module));
				
				// If the Data Object ID has not been provided:
				if(! $isDoId) {
					// Throw an exception:
					throw new M_MenuException(sprintf(
						'Missing Data Object ID!'
					));
				}
				
				// If the Data Object is to be loaded from a module:
				if($isModuleId) {
					// Construct the module:
					$module = new M_ApplicationModule((string) $do->module);
					
					// Ask the module for the data object's mapper classname:
					$className = $module->getDataObjectClassName((string) $do->dataObjectId);
					
					// Load the mapper:
					M_Loader::loadDataObject($className, $module->getId());
					M_Loader::loadDataObjectMapper($className, $module->getId());
					
					// Construct the mapper, and add it to the list of mappers:
					$className .= M_DataObjectMapper::MAPPER_SUFFIX;
					$mappers[]  = new $className;
				}
			}
			
			// If mappers have been constructed, attach it to the menu item:
			$item->setDataObjectMappers($mappers);
		}
		// If something failed:
		catch(Exception $e) {
			// Throw an exception:
			throw new M_MenuException(sprintf(
				'Cannot construct an instance of M_MenuItem with SimpleXMLElement (%s: %s)',
				$e->getCode(),
				$e->getMessage()
			));
		}
		
		// Now, we have constructed the menu item from the SimpleXMLElement instance.
		// For each of the element's sub-items:
		foreach($element->xpath('item') as $element) {
			// Construct the item, and add it to our item:
			$item->addMenuItem(self::constructWithSimpleXMLElement($element));
		}
		
		// Return the menu item:
		return $item;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get parent
	 * 
	 * Will provide with the menu item's parent
	 * 
	 * NOTE:
	 * If the menu item does not have a parent, this method will return NULL
	 * instead.
	 * 
	 * @access public
	 * @return M_MenuItem
	 */
	public function getParent() {
		return $this->_parent;
	}
	
	/**
	 * Get title
	 * 
	 * Will provide with the title of the menu item.
	 * 
	 * @access public
	 * @return string
	 */
	public function getTitle() {
		// If the menu item has been populated by a data object:
		if($this->hasDataObjects()) {
			// Populate the title, before returning:
			return $this->_getPopulatedString($this->_title);
		}
		
		// Return the title, if still here (not populated)
		return $this->_title;
	}
	
	/**
	 * Get description
	 * 
	 * Will provide with the description of the menu item.
	 * 
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		// If the menu item has been populated by a data object:
		if($this->hasDataObjects()) {
			// Populate the description, before returning:
			return $this->_getPopulatedString($this->_description);
		}
		
		// Return the description, if still here (not populated)
		return $this->_description;
	}
	
	/**
	 * Get Batch Number
	 * 
	 * Will provide the batch number, if it was set
	 * 
	 * @see M_MenuItem::setBatchNumber()
	 * @access public
	 * @return string
	 */
	public function getBatchNumber() {
		return $this->_batchNumber;
	}
	
	/**
	 * Get CSS
	 * 
	 * This method will return the CSS that has been added to the menu item 
	 * previously, with {@link M_MenuItem::setCss()}. If it has not been set
	 * yet, an empty {@link M_ElementCss} will be constructed and returned
	 * 
	 * @see M_MenuItem::setCss()
	 * @access public
	 * @return M_ElementCss
	 */
	public function getCss() {
		// If not yet set, we will create a new, empty instance
		if(is_null($this->_css)) {
			$this->_css = new M_ElementCss();
		}
		
		// Return the CSS instance
		return $this->_css;
	}
	
	/**
	 * Get path
	 * 
	 * Will provide with the (relative) path of the menu item.
	 * 
	 * @access public
	 * @return string
	 */
	public function getPath() {
		if($this->_pathPopulated) {
			return $this->_pathPopulated;
		}
		
		return $this->_path;
	}
	
	/**
	 * Get URI
	 * 
	 * Will return the instance of {@link M_Uri} that has been set for the menu
	 * item.
	 * 
	 * @access public
	 * @return M_Uri
	 */
	public function getUri() {
		return $this->_uri;
	}
	
	/**
	 * Get link
	 * 
	 * This method will render the link that is to be used for this menu item.
	 * If a relative path has been set for the menu item, then the link will be
	 * rendered with {@link M_Request::getLink()}. However, if an URI has been
	 * set for this menu item, it will render that URI with {@link M_Uri::getUri()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getLink() {
		// If an URL has been defined:
		if($this->_uri) {
			// Then, we return the render of that URL:
			return $this->_uri->getUri();
		}
		
		// If not, we render a link to a relative path:
		return M_Request::getLink($this->_path);
	}
	
	/**
	 * Get Target
	 * 
	 * This method will return a string representing the target value of the
	 * menu item if it has been specified.
	 * 
	 * @access public
	 * @return string
	 */
	public function getTarget() {
		return $this->_target;
	}
	
	/**
	 * Is path?
	 * 
	 * Will check whether or not the provided path matches the menu item's path
	 * 
	 * @access public
	 * @param string $path
	 * 		The path to compare with
	 * @return bool $flag
	 * 		Returns TRUE if the path matches the menu item's path, FALSE if not
	 */
	public function isPath($path) {
		// Clean the provided path
		$path = M_Helper::trimCharlist($path, '/');
		
		// A simple comparison is enough, if the menu item's path does not contain
		// dynamic elements:
		$doSimple = ! $this->hasDynamicPath();
		
		// However, if the menu item's path contains dynamic elements, but has
		// already been populated with a data object, a simple comparison will
		// also do.
		if(! $doSimple) {
			$doSimple = $this->hasDataObjects();
		}
		
		// If a simple comparison will do:
		if($doSimple) {
			// Do the simple comparison:
			return (M_Helper::trimCharlist($this->getPath(), '/') == $path);
		}
		// If the path contains dynamic path elements (and has not been populated
		// with data objects):
		else {
			// Do the comparison with regular expression:
			return (
				preg_match(
					'/^' . preg_replace('/(@[^\\\\\/@]+)/', '[^\/]+', preg_quote($this->_path, '/')) . '$/',
					$path
				)
				== 1
			);
		}
	}
	
	/**
	 * Has dynamic path elements?
	 * 
	 * Will tell whether or not the menu item's path contains dynamic path elements
	 * (which are to be populated by data objects)
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if containing dynamic path elements, FALSE if not
	 */
	public function hasDynamicPath() {
		return (strpos($this->_path, '@') !== FALSE);
	}
	
	/**
	 * Has Data Objects?
	 * 
	 * Will tell whether or not the menu item has been populated with data objects,
	 * by
	 * 
	 * - {@link M_MenuItem::setDataObjectsWithPath()}, or
	 * - {@link M_MenuItem::setDataObjects()}
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if populated with Data Objects, FALSE if not
	 */
	public function hasDataObjects() {
		return (count($this->_dataObject) > 0);
	}
	
	/**
	 * Has Data Object Mappers?
	 * 
	 * Will tell whether or not the menu item has been provided with Data Object
	 * Mappers, by {@link M_MenuItem::setDataObjectMappers()}.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if populated with Data Objects, FALSE if not
	 */
	public function hasDataObjectMappers() {
		return (count($this->_dataObjectMapper) > 0);
	}
	
	/**
	 * Get Data Objects
	 * 
	 * Will provide with the collection of Data Objects with which the menu item
	 * is being populated.
	 * 
	 * @access public
	 * @return ArrayIterator $iterator
	 * 		The collection of {@link M_DataObject} instances
	 */
	public function getDataObjects() {
		return new ArrayIterator($this->_dataObject);
	}
	
	/**
	 * Get Data Object, at given index
	 * 
	 * NOTE:
	 * Will return NULL, if the requested data object could not have been found
	 * 
	 * @access public
	 * @param integer $index
	 * 		The index of the requested data object
	 * @return M_DataObject $dataObject
	 * 		The requested data object
	 */
	public function getDataObject($index) {
		if(isset($this->_dataObject[(int) $index])) {
			return $this->_dataObject[(int) $index];
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get Data Object Mappers
	 * 
	 * Will provide with the collection of Data Object Mappers with which the 
	 * menu item is being populated.
	 * 
	 * @access public
	 * @return ArrayIterator $iterator
	 * 		The collection of {@link M_DataObjectMapper} instances
	 */
	public function getDataObjectMappers() {
		return new ArrayIterator($this->_dataObjectMapper);
	}
	
	/**
	 * Get Data Object Mapper, at given index
	 * 
	 * NOTE:
	 * Will return NULL, if the requested data object mapper could not have 
	 * been found
	 * 
	 * @access public
	 * @param integer $index
	 * 		The index of the requested data object
	 * @return M_DataObject $dataObject
	 * 		The requested data object
	 */
	public function getDataObjectMapper($index) {
		if(isset($this->_dataObjectMapper[(int) $index])) {
			return $this->_dataObjectMapper[(int) $index];
		} else {
			return NULL;
		}
	}
	
	/**
	 * Force this MenuItem to be active
	 * 
	 * We can automatically check if this MenuItem is active or not with 
	 * {@link M_MenuItem::isActive()}. To force this MenuItem to be active,
	 * you can use this method. After this is set, {@link M_MenuItem::isActive()}
	 * will no longer try to calculate the active-status, but will return the
	 * value which has been set with setActive
	 * 
	 * @param bool $_active
	 * @see isActive()
	 */
	public function setActive($_active) {
		$this->_active = $_active;
	}

	/**
	 * Is active?
	 * 
	 * Will tell whether or not the menu item has been activated/selected. Returns
	 * TRUE if the menu item is active/selected.
	 * 
	 * @access public
	 * @param bool $checkChildren
	 * 		Check if one the children is active, if so this will return true
	 * @return bool $flag
	 * 		Returns TRUE if the menu item is active/selected, FALSE if not
	 */
	public function isActive($checkChildren = true) {
		// check if the status has been set
		if(! is_null($this->_active)) {
			// If so, the status overrides the comparison with active path
			return $this->_active;
		}
		
		// If an URI has been set for this menu item:
		if($this->_uri) {
			// Then, we make sure the URI matches the Request URI and the paths. 
			// If that is not the case:
			$active = ($this->_uri->getHost() == M_Request::getHost() && M_MenuHelper::getActivePath() == $this->_uri->getRelativePath());
		}
		// If no URI has been set:
		else {
			// Compare paths:
			$active = (M_MenuHelper::getActivePath() == $this->getPath());
		}

		// Maybe one the children is active?
		if($active === false && $checkChildren === true) {
			// Get the boolean from the child menu items:
			$active = $this->isChildActive(true);
		}
		
		// Return the boolean:
		return $active;
	}
	
	/**
	 * Is one of the menu item's children active?
	 * 
	 * Will tell whether or not one the children is active. Returns TRUE if this
	 * is the case.
	 *
	 * @param bool $recursive
	 * @access public
	 * @return bool
	 * @author Ben Brughmans
	 */
	public function isChildActive($recursive = true) {
		/* @var $menuItem M_MenuItem */
		foreach($this->getMenuItems() AS $menuItem) {
			if ($menuItem->isActive($recursive)) return true;
		}
		
		return false;
	}
	
	/**
	 * Get number of contained menu items
	 * 
	 * Will provide with the total number of menu items that are contained by 
	 * the menu item.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNumberOfMenuItems() {
		return count($this->_items);
	}
	
	/**
	 * Get contained menu items
	 * 
	 * Will provide with the collection of menu items that are contained by the
	 * menu item. Typically, such a nested structure would be used to create
	 * submenus.
	 * 
	 * @access public
	 * @return M_ArrayIterator $iterator
	 * 		A collection of {@link M_MenuItem} instances
	 */
	public function getMenuItems() {
		return new M_ArrayIterator($this->_items);
	}
	
	/**
	 * Get contain menu item, by path
	 * 
	 * NOTE:
	 * Will return NULL, if no menu item could have been found!
	 * 
	 * @access public
	 * @uses M_MenuItem::isPath()
	 * @param string $path
	 * 		The path to look for
	 * @param bool $recursive
	 * 		Set to TRUE if you want to search recursively, FALSE if not
	 * @return M_MenuItem
	 */
	public function getMenuItemByPath($path, $recursive = TRUE) {
		// For each of the contained menu items:
		foreach($this->_items as $currentItem) {
			// Check recursively?
			if($recursive) {
				// If so, check if the current item contains an item that
				// matches the provided path:
				$rs = $currentItem->getMenuItemByPath($path, TRUE);
				
				// If so, we return it as the result of the search:
				if($rs) {
					return $rs;
				}
			}
			
			// If the current item matches the requested path
			if($currentItem->isPath($path)) {
				// we return the item as the result of the search
				return $currentItem;
			}
		}
		
		// If we're still here, it means that we have not found the item:
		return NULL;
	}
	
	/**
	 * Get sitemap
	 */
	public function getSitemap() {
	}
	
	/**
	 * Get breadcrumb
	 * 
	 * Will provide with the complete chain of {@link M_MenuItem} instances that
	 * leads to the current menu item (by looking up parents; 
	 * 
	 * @see @link M_MenuItem::getParent()
	 * @access public
	 * @return ArrayIterator $iterator
	 * 		The complete chain of {@link M_MenuItem} instances
	 */
	public function getBreadcrumb() {
		// Get the path of the menu item:
		$path = $this->getPath();
		
		// We prepare the output array, in which we'll store the complete 
		// collection of menu items in the breadcrumb:
		$out = array(
			// The last element in the breadcrumb is this menu item. So, we
			// pre-populate the array with this menu item:
			$this
		);
		
		// Get the parent of this menu item:
		$p = $this->getParent();
		
		// While a parent can be found:
		while($p) {
			// Is the parent being populated by data-objects?
			if($p->hasDynamicPath()) {
				// Then we can provide the parent with the path, in order to
				// identify the data object(s). That is, of course, if this
				// menu item is being populated successfully:
				if($this->hasDynamicPath() && $this->hasDataObjects()) {
					// We pop the last element from the path, shortening the 
					// path by one element:
					list($last, $path) = M_Uri::popElementFromPath($path);
					
					// Set the data object(s)
					$p->setDataObjectsWithPath($path);
				}
			}
			
			// We add the parent to the output array:
			array_unshift($out, $p);
			
			// We get the parent of the current item's parent. This way, we 
			// prepare the next iteration of the loop, which will stop when no
			// parents can be found anymore:
			$p = $p->getParent();
		}
		
		// Return the output array:
		return $out;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set parent
	 * 
	 * Will set the parent of the menu item
	 * 
	 * @access public
	 * @param M_MenuItem $item
	 * 		The menu item's parent
	 * @return void
	 */
	public function setParent(M_MenuItem $item) {
		$this->_parent = $item;
	}
	
	/**
	 * Set title
	 * 
	 * Will set the title of the menu item
	 * 
	 * @access public
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	/**
	 * Set description
	 * 
	 * Will set the description of the menu item
	 * 
	 * @access public
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
	}
	
	/**
	 * Set Batch Number
	 * 
	 * Allows for setting a batch number in the menu item. Typically, this
	 * property can be used to set the number of items that belong in a
	 * category for example, so it can be displayed somewhere along the
	 * menu item.
	 * 
	 * Note that while we speak of a number, you may provide any string,
	 * allowing you to for example display "5 items".
	 * 
	 * @access public
	 * @param string $batchNumber
	 * @return void
	 */
	public function setBatchNumber($batchNumber) {
		$this->_batchNumber = (string) $batchNumber;
	}
	
	/**
	 * Set CSS
	 * 
	 * This method allows you to add CSS properties to the menu item, by providing
	 * an instance of {@link M_ElementCss}.
	 * 
	 * @access public
	 * @param M_ElementCss $css
	 * @return void
	 */
	public function setCss(M_ElementCss $css) {
		$this->_css = $css;
	}
	
	/**
	 * Set path
	 * 
	 * Will set the (relative) path of the menu item
	 * 
	 * @access public
	 * @param string $path
	 * @return void
	 */
	public function setPath($path) {
		$this->_path = M_Helper::trimCharlist((string) $path, '/');
	}
	
	/**
	 * Set URI
	 * 
	 * You have the option to set a relative path for the menu item, with the
	 * setter {@link M_MenuItem::setPath()}. However, if you want to create a 
	 * menu item that links to an external page, you'll want to set an instance
	 * of {@link M_Uri} instead of a relative path.
	 * 
	 * @access public
	 * @param M_Uri $uri
	 * @return void
	 */
	public function setUri(M_Uri $uri) {
		$this->_uri = $uri;
	}
	
	/**
	 * Set Target
	 * 
	 * Will set the target of the menu item. Any string is valid since a target
	 * can also be any iFrame specified on the page.
	 * 
	 * @access public
	 * @param string $target
	 * @return void
	 */
	public function setTarget($target){
		$this->_target = (string) $target;
	}
	
	/**
	 * Set Data Objects
	 * 
	 * Will set the {@link M_DataObjectMapper} instances that are to be used to 
	 * populate the menu item. Consider the following example:
	 * 
	 * Example 1
	 * <code>
	 *    // Construct a menu item
	 *    $item = new M_MenuItem();
	 *    $item->setTitle('@title');
	 *    $item->setDescription('@text');
	 *    $item->setPath('/news/@urlSuffix');
	 *    
	 *    // Construct a mapper, in order to fetch the selected data object:
	 *    $mapper = new NewsMapper();
	 *    
	 *    // Set the path to the selected data object:
	 *    $item->setDataObjects(array(
	 *       0 => $mapper->getById(1)
	 *    ));
	 * </code>
	 * 
	 * @see M_MenuItem::setDataObjectsWithPath()
	 * @access public
	 * @param array $dataObjects
	 * 		The collection of {@link M_DataObject} instances
	 * @return void
	 */
	public function setDataObjects(array $dataObjects) {
		// TODO: M_MenuItem::setDataObjects()
	}
	
	/**
	 * Set Data Object Mappers
	 * 
	 * Will set the {@link M_DataObjectMapper} instances that are to be used to 
	 * populate the menu item. Consider the following example:
	 * 
	 * Example 1
	 * <code>
	 *    // Construct a menu item
	 *    $item = new M_MenuItem();
	 *    $item->setTitle('@title');
	 *    $item->setDescription('@text');
	 *    $item->setPath('/news/@urlSuffix');
	 *    
	 *    // Set the data object mappers, to interpret the menu item:
	 *    $item->setDataObjectMappers(array(
	 *       new NewsMapper()
	 *    ));
	 * </code>
	 * 
	 * To learn more about the use of this method, read the documentation on
	 * {@link M_MenuItem::setDataObjectsWithPath()}
	 * 
	 * @see M_MenuItem::setDataObjectsWithPath()
	 * @access public
	 * @param array $dataObjectMappers
	 * @return void
	 */
	public function setDataObjectMappers(array $dataObjectMappers) {
		// Make sure the array is a collection of Data Object Mappers:
		if(! M_Helper::isIteratorOfClass($dataObjectMappers, 'M_DataObjectMapper')) {
			// If the're not, we throw an exception
			throw new M_MenuException(sprintf(
				'%s:%s() expects an array of %s objects',
				__CLASS__,
				__FUNCTION__,
				'M_DataObjectMapper'
			));
		}
		
		// Set the mappers:
		$this->_dataObjectMapper = $dataObjectMappers;
	}
	
	/**
	 * Set Data Objects, with path
	 * 
	 * Will set the path in order to identify the data objects that are used in
	 * the menu item. Consider the following example:
	 * 
	 * Example 1
	 * <code>
	 *    // Construct a menu item
	 *    $item = new M_MenuItem();
	 *    $item->setTitle('@title');
	 *    $item->setDescription('@text');
	 *    $item->setPath('/news/@urlSuffix');
	 *    
	 *    // Set the mappers that will be used to identify the data object:
	 *    $item->setDataObjectMappers(array(
	 *       new NewsMapper()
	 *    ));
	 *    
	 *    // Set the data object, by providing a path:
	 *    $item->setDataObjectsWithPath('news/my-headline');
	 * </code>
	 * 
	 * The above code will cause the menu item to look up the requested news item.
	 * To do so, it will use the provided path. This path should provide with 
	 * the required variables to identify a unique data object.
	 * 
	 * In our example, the menu item's path has been defined as /news/@urlSuffix.
	 * We are setting the data object with the path /news/my-headline. This means
	 * that we are providing "my-headline" as the value for the variable @urlSuffix.
	 * Thus, the data object will be looked up as following:
	 * 
	 * <code>
	 *    // In our example, $mapper is the provided NewsMapper instance
	 *    $dataObject = $mapper
	 *        ->getByField('urlSuffix', 'my-headline')
	 *        ->current();
	 * </code>
	 * 
	 * This should provide the menu item with a unique data object (if not, an 
	 * exception is thrown). Then, this data object is used to further populate 
	 * the menu item.
	 * 
	 * @see M_MenuItem::setDataObjects()
	 * @access public
	 * @param string $path
	 * @return void
	 */
	public function setDataObjectsWithPath($path) {
		// We make sure that Data Object Mappers have been provided
		if(! $this->hasDataObjectMappers()) {
			// If not, we throw an exception
			throw new M_MenuException(sprintf(
				'Cannot set data objects with path "%s"; Menu item does not have instances of %s',
				$path,
				'M_DataObjectMapper'
			));
		}
		
		// Remove data objects that may have been set previously:
		$this->removeDataObjects();
		
		// Remove populated path (from previous data objects)
		$this->_removePathPopulated();
		
		// Get dynamic path elements:
		// (and make sure the number of elements matches the number of mappers)
		$dynamicPathElement = $this->_getDynamicPathElements(count($this->_dataObjectMapper));
		
		// Get the values in the provided path:
		$vars = $this->_getValuesForDynamicPathElements($path);
		
		// For each of the mappers:
		foreach($this->_dataObjectMapper as $i => $mapper) {
			// Make sure the current object is a mapper:
			if(! M_Helper::isInstanceOf($mapper, 'M_DataObjectMapper')) {
				// If not, throw an exception:
				throw new M_MenuException(sprintf(
					'Instances of %s are expected, in order to set data objects with the path "%s"',
					'M_DataObjectMapper',
					$path
				));
			}
			
			// Is matching element found for this mapper?
			if(! isset($dynamicPathElement[$i])) {
				// If not, throw an exception:
				throw new M_MenuException(sprintf(
					'Cannot find matching path element for mapper %d',
					$i
				));
			}
		
			// Is a value available for the current variable?
			if(! isset($vars[$i])) {
				// If not, throw an exception:
				throw new M_MenuException(sprintf(
					'Cannot find value for variable %d; Please make sure that values are provided for all dynamic path elements',
					$i
				));
			}
			
			// Ask the mapper for the matching data-object
			$dataObject = $mapper->getByField(
				$this->_getFieldFromDynamicPathElement($dynamicPathElement[$i]),
				$vars[$i]
			)->current();
			
			// If the data object has not been found:
			if(! $dataObject) {
				// throw an exception:
				throw new M_MenuException(sprintf(
					'Cannot find M_DataObject in M_MenuItem, with value "%s" for field "%s"',
					$vars[$i],
					$dynamicPathElement[$i]
				));
			}
			
			// Remember about the data-object
			$this->_dataObject[$i] = $dataObject;
		}
		
		// Set the populated path:
		$this->_setPathPopulated((string) $path);
	}
	
	/**
	 * Remove data objects
	 * 
	 * Will remove the Data Objects from menu item, which may have been added
	 * previously with:
	 * 
	 * - {@link M_MenuItem::setDataObjectsWithPath()}, or
	 * - {@link M_MenuItem::setDataObjects()}
	 * 
	 * @access public
	 * @return void
	 */
	public function removeDataObjects() {
		$this->_dataObject = array();
	}
	
	/**
	 * Add a menu item
	 * 
	 * Will add a menu item, that is to be contained by the menu item object.
	 * 
	 * @uses M_MenuItem::setParent()
	 * @access public
	 * @param M_MenuItem $item
	 * @return void
	 */
	public function addMenuItem(M_MenuItem $item) {
		// First, set me as parent
		$item->setParent($this);
		
		// Add the menu item:
		$this->_items[] = $item;
	}
	
	/**
	 * Remove contained menu items, by path
	 * 
	 * Will remove all contained menu items that match the provided path
	 * 
	 * @access public
	 * @param string $path
	 * 		The path to look for
	 * @param bool $recursive
	 * 		Set to TRUE if you want to search recursively, FALSE if not
	 * @return void
	 */
	public function removeMenuItemByPath($path, $recursive = FALSE) {
		// for each of the contained menu items:
		foreach($this->_items as $i => $currentItem) {
			// If the current item's path matches the provided one:
			if($currentItem->isPath($path)) {
				// Remove the menu item
				unset($this->_items[$i]);
			}
			
			// If searching recursively:
			if($recursive) {
				// Remove contained matching menu items
				$currentItem->removeMenuItemByPath($path, TRUE);
			}
		}
	}
	
	/**
	 * Remove contained menu item
	 * 
	 * Will remove the specified menu item from the contained menu items
	 * 
	 * @access public
	 * @param M_MenuItem $item
	 * 		The item to be removed
	 * @return void
	 */
	public function removeMenuItem(M_MenuItem $item) {
		// for each of the contained menu items:
		foreach($this->_items as $i => $currentItem) {
			// If the current item is the one we are looking for:
			if($currentItem == $item) {
				// Remove the menu item
				unset($this->_items[$i]);
			}
		}
	}
	
	/**
	 * Remove all contained menu items
	 * 
	 * @access public
	 * @return void
	 */
	public function removeMenuItems() {
		$this->_items = array();
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Clear populated path
	 * 
	 * @access private
	 * @return void
	 */
	private function _removePathPopulated() {
		$this->_pathPopulated = NULL;
	}
	
	/**
	 * Clear populated path
	 * 
	 * @access private
	 * @return void
	 */
	private function _setPathPopulated($path) {
		$this->_pathPopulated = M_Helper::trimCharlist((string) $path, '/');
	}
	
	/**
	 * Get dynamic path elements
	 * 
	 * @throws M_MenuException
	 * @access private
	 * @return array
	 */
	private function _getDynamicPathElements($expectedNumberOfElements) {
		// Get dynamic path elements:
		$m = array();
		
		// If the number of path elements does not match the expected number:
		if(preg_match_all('/@[^\/@]+/', (string) $this->_path, $m) != (int) $expectedNumberOfElements) {
			// We throw an exception!
			throw new M_MenuException(sprintf(
				'Expecting exactly %d dynamic path elements in %s (%s)',
				$expectedNumberOfElements,
				$this->getTitle(),
				$this->getPath()
			));
		}
		
		// Return the path elements:
		return $m[0];
	}
	
	/**
	 * Get field in dynamic path element
	 * 
	 * @access private
	 * @param string $pathElement
	 * @return string
	 */
	private function _getFieldFromDynamicPathElement($pathElement) {
		// Remove reference to the data object:
		$info = $this->_getDynamicStringWithoutDataObjectReference((string) $pathElement);
		
		// Return the field name, without @ prefix:
		return substr((string) $info[1], 1);
	}
	
	/**
	 * Get variables in given path
	 * 
	 * Will provide with the values of the dynamic path elements, based on a 
	 * given path.
	 * 
	 * @access public
	 * @return array
	 */
	private function _getValuesForDynamicPathElements($path) {
		return M_Request::getPathVariables(
			$path, 
			preg_replace(
				'/@[^\/@]+/', 
				'%', 
				(string) $this->_path
			)
		);
	}
	
	/**
	 * Get populated string
	 * 
	 * Will provide with a populated string. This method is used internally, to
	 * populate the title and description of the menu item
	 * 
	 * @see M_MenuItem::getTitle()
	 * @see M_MenuItem::getDescription()
	 * @access private
	 * @return string
	 */
	private function _getPopulatedString($string) {
		// Find the dynamic attributes, and replace them:
		return preg_replace(
			'/(@[^\/@\s]+)/e', 
			'$this->_getPopulatedStringValue("$1")', 
			(string) $string
		);
	}
	
	/**
	 * Get populated string value
	 * 
	 * Used by {@link M_MenuItem::_getPopulatedString()} to get the value of a
	 * detected data object attribute in the string.
	 * 
	 * @access private
	 * @return string
	 */
	private function _getPopulatedStringValue($attribute) {
		// Remove the reference to the data object:
		list($dataObjectIndex, $attribute) = $this->_getDynamicStringWithoutDataObjectReference($attribute);
		
		// Get the data object:
		$dataObject = $this->getDataObject($dataObjectIndex);
		if(! $dataObject) {
			// If the data object has not been found, throw an exception:
			throw new M_MenuException(sprintf(
				'Cannot populate the menu item\'s string "%s"; cannot find data object %d',
				$attribute,
				$dataObjectIndex
			));
		}
		
		// Remove the @ prefix:
		$attribute = substr($attribute, 1);
		
		// Replace the string value by the data object's attribute value:
		return $dataObject->$attribute;
	}
	
	/**
	 * Strip reference to dataObject from dynamic string
	 * 
	 * Will remove the reference to the data object from the provided string. The
	 * result of this method is an array with the following elements:
	 * 
	 * - The data object index (number)
	 * - The dynamic string, without reference to the data object
	 * 
	 * @example M_MenuItem::_getDynamicStringWithoutDataObjectReference('@dataObject0[text]') = array(0, '@text')
	 * @example M_MenuItem::_getDynamicStringWithoutDataObjectReference('@dataObject1[title]') = array(1, '@title')
	 * @example M_MenuItem::_getDynamicStringWithoutDataObjectReference('@title') = array(0, '@title')
	 * @access private
	 * @param string $string
	 * @return string
	 */
	private function _getDynamicStringWithoutDataObjectReference($string) {
		// If the string does not start with the prefix of dynamic strings
		if($string{0} != '@') {
			// Throw exception!
			throw new M_MenuException(sprintf(
				'Invalid dynamic string "%s"; must start with @ sign',
				$string
			));
		}
		
		// If a reference is present:
		$m = array();
		if(preg_match('/^@dataObject([\d]+)(.*)$/', $string, $m)) {
			// Check if a field has been provided:
			if(substr($m[2], 1) != '[' && substr($m[2], -1) != ']') {
				// If not, throw exception!
				throw new M_MenuException(sprintf(
					'Invalid dynamic string; Cannot find enclosing [ ] brackets in "%s"',
					$string
				));
			}
			
			// Get the field:
			$fieldName = M_Helper::trimCharlist($m[2], '[]@');
			
			// If the field name is empty:
			if(empty($fieldName)) {
				// If not, throw exception!
				throw new M_MenuException(sprintf(
					'Invalid dynamic string; Missing field name in "%s"',
					$string
				));
			}
			
			// Return the cleaned string, and the data object's index number
			return array(
				(int) $m[1],
				'@' . $fieldName
			);
		}
		// If no reference is present:
		else {
			return array(
				0, // Data Object 0 by default
				$string
			);
		}
	}
}