<?php
/**
 * M_MultiSafepayStatus
 * 
 * Represents a status within the MultiSafepay environment.
 * made.
 * 
 * @package Core
 */
class M_MultiSafepayStatus extends M_Object {

	/* -- CONSTANTS -- */
	
	const STATUS_COMPLETED = 'completed';
	const STATUS_INITIALIZED = 'initialized';
	const STATUS_UNCLEARED = 'uncleared';
	const STATUS_VOID = 'void';
	const STATUS_DECLINED = 'declined';
	const STATUS_REFUNDED = 'refunded';
	const STATUS_EXPIRED = 'expired';
	
	/* -- PROPERTIES -- */
	
	/**
	 * Status
	 * 
	 * @var string
	 */
	private $_status;
	
	/**
	 * Payment type
	 * 
	 * @var string 
	 */
	private $_paymentType;
	
	/* -- PUBLIC -- */
	
	/**
	 * Construct with XML
	 * 
	 * @param M_XmlElement $xml
	 * @return M_MultiSafepayStatus
	 * @throws M_Exception
	 */
	public static function constructWithXml(M_XmlElement $xml) {
		$status = (string) $xml->ewallet->status;
		
		if(! $status) {
			throw new M_Exception(sprintf('No status found in XML'));
		}
		
		$multiSafepayStatus = new self();
		$multiSafepayStatus->setStatus($status);
		$multiSafepayStatus->setPaymentType((string) $xml->paymentdetails->type);
		return $multiSafepayStatus;
	}
	
	/**
	 * Get the status
	 * 
	 * @return string
	 */
	public function getStatus() {
		return $this->_status;
	}

	/**
	 * Set status
	 * 
	 * @param string $status
	 * @return M_MultiSafepayStatus
	 */
	public function setStatus($status) {
		$this->_status = $status;
		return $this;
	}

	/**
	 * Get payment type
	 * 
	 * @return string
	 */
	public function getPaymentType() {
		return $this->_paymentType;
	}

	/**
	 * Set payment type
	 * 
	 * @param string $paymentType
	 * @return M_MultiSafepayStatus
	 */
	public function setPaymentType($paymentType) {
		$this->_paymentType = $paymentType;
		return $this;
	}

		
	/**
	 * Is completed?
	 * 
	 * @return string
	 */
	public function isCompleted() {
		return ($this->getStatus() == self::STATUS_COMPLETED);
	}

}