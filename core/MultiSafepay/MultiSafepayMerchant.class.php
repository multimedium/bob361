<?php
/**
 * M_MultiSafepayMerchant
 * 
 * Represents a merchant within the MultiSafepay environment. Typically, this
 * {@link M_MultiSafepayMerchant} represents the webshop's account at
 * MultiSafepay, and is required for every {@link M_MultiSafepay}
 * made.
 * 
 * @package Core
 */
class M_MultiSafepayMerchant extends M_Object {

	/* -- PROPERTIES -- */
	
	/**
	 * Account Id
	 * 
	 * Contains the account id of the merchant at MultiSafepay
	 * 
	 * NOTE: this property is mandatory for correct merchant functionality
	 * 
	 * @see M_MultiSafepayMerchant::setAccountId()
	 * @see M_MultiSafepayMerchant::getAccountId()
	 * @access private
	 * @var int
	 */
	private $_accountId;
	
	/**
	 * Site Id
	 * 
	 * Contains the id of the currently active site, specified in the
	 * merchant's account control panel at MultiSafepay
	 * 
	 * NOTE: this property is mandatory for correct merchant functionality
	 * 
	 * @see M_MultiSafepayMerchant::setSiteId()
	 * @see M_MultiSafepayMerchant::getSiteId()
	 * @access private
	 * @var int
	 */
	private $_siteId;
	
	/**
	 * Site Secure Code
	 * 
	 * Contains the secure code of the currently active site, specified in the
	 * merchant's account control panel at MultiSafepay
	 * 
	 * NOTE: this property is mandatory for correct merchant functionality
	 * 
	 * @see M_MultiSafepayMerchant::setSiteSecureCode()
	 * @see M_MultiSafepayMerchant::getSiteSecureCode()
	 * @access private
	 * @var int
	 */
	private $_siteSecureCode;
	
	/**
	 * Redirect Uri
	 * 
	 * Contains the redirect uri, where the user will be redirected
	 * after their payment has been completed with MultiSafepay
	 * 
	 * NOTE: this property is mandatory for correct merchant functionality
	 * 
	 * @see M_MultiSafepayMerchant::setRedirectUri()
	 * @see M_MultiSafepayMerchant::getRedirectUri()
	 * @access private
	 * @var M_Uri
	 */
	private $_redirectUri;
	
	/**
	 * Cancel Uri
	 * 
	 * Contains the cancel uri, where the user will be redirected if they
	 * choose to cancel the transaction
	 * 
	 * @see M_MultiSafepayMerchant::setCancelUri()
	 * @see M_MultiSafepayMerchant::getCancelUri()
	 * @access private
	 * @var M_Uri
	 */
	private $_cancelUri;
	
	/**
	 * Close Window
	 * 
	 * Holds whether or not we have to close the payment window when the user
	 * cancels the payment, and no cancel uri has been specified. For example:
	 * 
	 * Case 1: the user cancels the payment, and a cancel uri has been
	 * specified. In this case, the user will be redirected to the cancel uri,
	 * and this property will be ignored.
	 * 
	 * Case 2: the user cancels the payment, and no cancel uri has been
	 * specified. The close window property is set to FALSE: in this case,
	 * the window with the payment will remain open after cancelling the
	 * payment, without any further possible interaction.
	 * 
	 * Case 3: the user cancels the payment, and no cancel uri has been
	 * specified. The close window property is set to TRUE: in this case,
	 * the window with the payment will automatically be closed, as it does
	 * not have any use any longer.
	 * 
	 * Note that for now we don't provide a setter, defaulting the
	 * functionality to closing the window upon cancelling the payment.
	 * 
	 * @see M_MultiSafepayMerchant::getCloseWindow()
	 * @access private
	 * @var bool
	 */
	private $_closeWindow = true;
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param int $accountId
	 *		The id of the merchant's MultiSafepay account
	 * @param int $siteId
	 *		The currently active site's id, specified in the merchant's
	 *		MultiSafepay control panel
	 * @param int $siteSecureCode
	 *		The currently active site's secure code, specified in the
	 *		merchant's MultiSafepay control panel
	 * @return M_MultiSafepayMerchant
	 */
	public function __construct($accountId, $siteId, $siteSecureCode) {
		// Set the properties
		$this->setAccountId($accountId);
		$this->setSiteId($siteId);
		$this->setSiteSecureCode($siteSecureCode);
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set Account Id
	 * 
	 * Allows for setting the account id of the merchant at MultiSafepay
	 * 
	 * NOTE: setting this property is mandatory for correct merchant
	 * functionality
	 * 
	 * @access public
	 * @var int $accountId
	 * @return M_MultiSafepayMerchant
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAccountId($accountId) {
		$this->_accountId = (int) $accountId;
		return $this;
	}
	
	/**
	 * Set Site Id
	 * 
	 * Allows for setting the id of the currently active website, specified
	 * in the MultiSafepay control panel of the merchant
	 * 
	 * NOTE: setting this property is mandatory for correct merchant
	 * functionality
	 * 
	 * @access public
	 * @var int $siteId
	 * @return M_MultiSafepayMerchant
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSiteId($siteId) {
		$this->_siteId = (int) $siteId;
		return $this;
	}
	
	/**
	 * Set Site Secure Code
	 * 
	 * Allows for setting the secure code of the currently active website,
	 * specified in the MultiSafepay control panel of the merchant
	 * 
	 * NOTE: setting this property is mandatory for correct merchant
	 * functionality
	 * 
	 * @access public
	 * @var int $siteSecureCode
	 * @return M_MultiSafepayMerchant
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSiteSecureCode($siteSecureCode) {
		$this->_siteSecureCode = (int) $siteSecureCode;
		return $this;
	}

	/**
	 * Set Redirect Uri
	 * 
	 * Allows for setting the redirect uri. This is the uri the user will be
	 * redirected to after they successfully completed their payment through
	 * MultiSafepay. Typically, this is the webshop's thank you page.
	 * 
	 * NOTE: when redirected, the uri will contain a GET variable containing
	 * the transaction ID, which may then be used to request additional
	 * information about the transaction from MultiSafepay. Do note that this
	 * functionality has not yet been implemented in our core MultiSafepay
	 * classes.
	 * 
	 * NOTE: MultiSafepay allows for setting a notification uri in the
	 * merchant object as well, but that is only used if no redirect uri
	 * has been set. In that case, the user will remain in the MultiSafepay
	 * interface (viewing presumably a payment completed page), and the
	 * notification uri will be called in the background to provide the
	 * transaction ID to the webshop. As we always want to work with a
	 * redirect uri, we do not provide the option of setting the notification
	 * uri.
	 * 
	 * NOTE: next to the redirect uri and the notification uri we don't use,
	 * it is also required to set a notification uri in the MultiSafepay
	 * account settings. That notification uri will be called by MultiSafepay
	 * every time the status of the transaction changes (e.g. paid,
	 * declined etc). Typically the uri we set there will link to the
	 * front end {@link MultiSafepayController}.
	 * 
	 * @access public
	 * @var M_Uri $redirectUri
	 * @return M_MultiSafepayMerchant
	 *		Returns itself, for a fluent programming interface
	 */
	public function setRedirectUri(M_Uri $redirectUri) {
		$this->_redirectUri = $redirectUri;
		return $this;
	}
	
	/**
	 * Set Cancel Uri
	 * 
	 * Allows for setting the cancel uri. When the user cancels their payment
	 * in the MultiSafepay interface, they will be redirected to this uri.
	 * 
	 * @access public
	 * @var M_Uri $cancelUri
	 * @return M_MultiSafepayMerchant
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCancelUri(M_Uri $cancelUri) {
		$this->_cancelUri = $cancelUri;
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Account Id
	 * 
	 * @see M_MultiSafepayMerchant::setAccountId()
	 * @access public
	 * @return int
	 */
	public function getAccountId() {
		return $this->_accountId;
	}
	
	/**
	 * Get Site Id
	 * 
	 * @see M_MultiSafepayMerchant::setSiteId()
	 * @access public
	 * @return int
	 */
	public function getSiteId() {
		return $this->_siteId;
	}
	
	/**
	 * Get Site Secure Code
	 * 
	 * @see M_MultiSafepayMerchant::setSiteSecureCode()
	 * @access public
	 * @return int
	 */
	public function getSiteSecureCode() {
		return $this->_siteSecureCode;
	}
	
	/**
	 * Get Redirect Uri
	 * 
	 * @see M_MultiSafepayMerchant::setRedirectUri()
	 * @access public
	 * @return M_Uri
	 */
	public function getRedirectUri() {
		return $this->_redirectUri;
	}
	
	/**
	 * Get Cancel Uri
	 * 
	 * @see M_MultiSafepayMerchant::setCancelUri()
	 * @access public
	 * @return M_Uri
	 */
	public function getCancelUri() {
		return $this->_cancelUri;
	}
	
	/**
	 * Get Close Window
	 * 
	 * @see M_MultiSafepayMerchant::$_closeWindow
	 * @access public
	 * @return bool
	 */
	public function getCloseWindow() {
		return $this->_closeWindow;
	}
	
	/**
	 * Get XML
	 * 
	 * Will return this {@link M_MultiSafepayMerchant} in XML format, which
	 * is typically used to make the XML request in {@link M_MultiSafepay}
	 * 
	 * @access public
	 * @return string
	 */
	public function getXml() {
		// Before starting the output, we have to make sure that all
		// necessary properties have been set:
		if(is_null($this->getAccountId())) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): the account id must be specified',
				__CLASS__,
				__FUNCTION__
			));
		}
		if(is_null($this->getSiteId())) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): the site id must be specified',
				__CLASS__,
				__FUNCTION__
			));
		}
		if(is_null($this->getSiteSecureCode())) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): the site secure code must be specified',
				__CLASS__,
				__FUNCTION__
			));
		}
		if(is_null($this->getRedirectUri())) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): the redirect uri must be specified',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Start the output
		$out = '<merchant>';
		
		// Add the mandatory properties
		$out .=		'<account>'.			$this->getAccountId()							.'</account>';
		$out .=		'<site_id>'.			$this->getSiteId()								.'</site_id>';
		$out .=		'<site_secure_code>'.	$this->getSiteSecureCode()						.'</site_secure_code>';
		$out .=		'<notification_url>'	/* not in use currently */						.'</notification_url>';
		$out .=		'<redirect_url>'.		$this->getRedirectUri()->toString()				.'</redirect_url>';
		
		// Add the optional properties
		if($this->getCancelUri()) {
			$out .=	'<cancel_url>'.			$this->getCancelUri()->toString()				.'</cancel_url>';
		}
		
		// Finally, add the close window boolean
		$out .=		'<close_window>'.		($this->getCloseWindow() ? 'true' : 'false')	.'</close_window>';
		
		// Close the output
		$out .= '</merchant>';
		
		// Return return the output
		return $out;
	}
}