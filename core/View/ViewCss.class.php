<?php
/**
 * Css holder
 * 
 * This class holds all css which can be added to a View. By gathering
 * all css instead of outputting it directly we have more control about which
 * css has been added and we can prevent double css-links
 */
class M_ViewCss extends M_ViewPageElement {
	
	/**
	 * Singleton object
	 * 
	 * M_ViewCss is a singleton object. This property holds the 
	 * singleton. Note that it is defaulted to NULL, and no M_ViewCss
	 * object will be constructed until specifically requested by
	 * other classes.
	 *
	 * @access private
	 * @var M_ViewCss
	 */
	private static $_instance;
	
	/**
	 * Private Constructor
	 * 
	 * The signature on the M_ViewCss class constructor is PRIVATE.
	 * This way, we force the M_ViewCss object to be a singleton, as
	 * objects need to be constructed with the {@link M_ViewCss::getInstance()}
	 * method.
	 *
	 * @access private
	 * @return M_ViewCss
	 */
	private function __construct() {
	}
	
	/**
     * Get a instance M_ViewCss instance
     * 
     * @return M_ViewCss
     */
    public static function getInstance() {
    	if(self::$_instance == NULL) {
			self::$_instance = new self;
		}
		return self::$_instance;
    }
	
	/**
	 * Add inline data
	 * 
	 * @param string
	 * @param string $context
	 * @param string $condition
	 * @param string $media
	 * @return M_ViewPageElement
	 */
    public function addInline($str, $context = null, $condition = null, $media = null) {
    	$this->_addData((string)$str,$context,$condition,false, $media);
    	return $this;
    }
    
	/**
	 * Add external URL
	 * 
	 * @param M_Uri
	 * @param string $context
	 * @param string $condition
	 * @param string $media
	 * @return M_ViewPageElement
	 */
    public function addExternal(M_Uri $uri, $context = null, $condition = null, $media = null) {
    	$this->_addData($uri,$context,$condition,false, $media);
    	return $this;
    }
    
    /**
	 * Add inline data
	 * 
	 * @param M_File
	 * @param string $context
	 * @param string $condition
	 * @param string $media
	 * @return M_ViewPageElement
	 */
    public function addFile(M_File $file, $context = null, $condition = null, $media = null) {
		//check if the path is a file
    	if (is_file($this->_getPathWithoutQueryVariables($file->getPath())) == false) {
    		throw new M_Exception(
    			sprintf('Failed to add file %s', $file->getPath())
    		);
    	}
    	
    	$this->_addData($file,$context,$condition,false, $media);
    	return $this;
    }

    /**
     * Add final inline data
     * 
     * @param string
     * @param string $context
     * @param string $condition
	 * @param string $media
     * @return M_ViewPageElement
     */
    public function addInlineFinal($str, $context, $condition = null, $media = null) {
    	$this->_addData((string)$str,$context,$condition, true, $media);
    	return $this;
    }
    
    /**
     * Add final file data
     * 
     * @param M_File
     * @param string $context
     * @param string $condition
	 * @param string $media
     * @return M_ViewPageElement
     */
    public function addFileFinal(M_File $file, $context,$condition = null, $media = null) {
    	$this->_addData($file,$context,$condition, true, $media);
    	return $this;
    }
   
    /**
	 * Defines the starting tag for inline content
	 * 
	 * @return string
	 */
    protected function _getInlineStartingTag() {
    	return '<style>';
    }
    
    /**
	 * Defines the closing tag for inline content
	 * 
	 * @return string
	 */
    protected function _getInlineClosingTag() {
    	return '</style>';
    }
    
    /**
     * Use a file to get valid (x)html sourcecode to link with a css
     * 
     * @param M_File $file
	 * @param string $media
     * @return string
     */
    protected function _getFileCode(M_File $file, $media = 'screen') {
		$output = M_CodeHelper::getNewLineWithTab(1,1);
		$output .= '<link rel="stylesheet" href="';
		$output .= M_Request::getLinkWithoutPrefix(M_Loader::getRelative($file->getPath()));
		$output .= '" media="'.$media.'" />';
		
		return $output;
    }
    
    /**
     * Use a file to get valid (x)html sourcecode to link with a css
     * 
     * @param M_Uri $uri
	 * @param string $media
     * @return string
     */
    protected function _getExternalCode(M_Uri $uri, $media = 'screen') {
		$output = M_CodeHelper::getNewLineWithTab(1,1);
		$output .= '<link rel="stylesheet" href="';
		$output .= htmlentities($uri->toString());
		$output .= '" media="'.$media.'" />';
		
		return $output;
    }
    
    /**
     * Use inline css-data to use in a html file
     * 
     * @param M_File $file
	 * @param string $media
     * @return string
     */
    protected function _getInlineCode($data, $media = 'screen') {
    	$output = M_CodeHelper::getNewLine(1) . '<style media="'.$media.'">';
    	$output .= M_CodeHelper::getNewLine(1) . $data;
    	$output .= M_CodeHelper::getNewLine(1) . '</style>';
    	
    	return $output;
    }
}