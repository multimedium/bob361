<?php
/**
 * M_ViewCoreHtml class
 * 
 * @package Core
 */
abstract class M_ViewCoreHtml extends M_ViewHtml {
	protected $_moduleOwnerId;
	
	/**
	 * Set module owner
	 * 
	 * 
	 */
	public function setModuleOwner($moduleId) {
		$this->_moduleOwnerId = (string) $moduleId;
	}
	
	public function getModuleOwner() {
		return $this->_moduleOwnerId;
	}
	
	protected function _getResourceFromModuleOwner($filename) {
		if($this->_moduleOwnerId) {
			return M_ViewHtmlResource::constructWithModuleId($this->_moduleOwnerId, $filename);
		} else {
			return M_ViewHtmlResource::constructWithTemplateBasePath($filename);
		}
	}
	
	/**
	 * Render HTML
	 * 
	 * This (protected) method renders the HTML source code of the view.
	 * The result of this method is used by:
	 * 
	 * - {@link M_View::fetch()}
	 * - {@link M_View::display()}
	 * 
	 * This method will do the following:
	 * 
	 * - pre-processing
	 * - HTML source code rendering (template or {@link M_ViewCoreHtml::getHtml()}
	 * - post-processing
	 * 
	 * Read the intro of {@link M_ViewCoreHtml} to learn more.
	 * 
	 * @access protected
	 * @return string
	 */
	protected function _render() {
		// Pre-processing
		if(method_exists($this, 'preProcessing')) {
			$this->preProcessing();
		}
		
		// Get HTML resource file (template):
		$r = $this->getResource();
		
		// Check if the template exists:
		if($r->getFile()->exists()) {
			// Now, we assign all presentation variables to the template:
			foreach($this->_variables as $name => $variable) {
				$r->assign($name, $variable);
			}
			
			// Now, we assign all persistent presentation variables to the 
			// template:
			foreach(self::getPersistentVariables() as $name => $variable) {
				$r->assign($name, $variable);
			}
		
			// Assign the view's unique ID to the template
			$r->assign('viewId', $this->getId());
		
			// Get the final output of the template:
			$html = $r->fetch();
		}
		// If the HTML resource file (template) does not exist for this
		// specific view, we simply do the default render. To do so, we
		// call the getHtml() method, which is a required implementation
		// in subclasses of M_ViewCoreHtml:
		else {
			$html = $this->getHtml();
		}
		
		// Post-processing
		if(method_exists($this, 'postProcessing')) {
			$html = $this->postProcessing($html);
		}
		
		// return final render
		return $html;
	}
	
	/**
	 * Default HTML source code rendering
	 * 
	 * This method overrides {@link M_ViewCoreHtml::getHtml()}. All 
	 * subclasses of {@link M_ViewCoreHtml} must implement this method, 
	 * to provide with default HTML rendering.
	 * 
	 * @access protected
	 * @return string
	 */
	abstract protected function getHtml();
}