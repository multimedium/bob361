<?php
/**
 * M_ServiceWithApiKey
 * 
 * This class is an abstract motherclass for all services that require an API
 * key, such as Flickr, Google Maps, etc...
 * 
 * @package Core
 */
abstract class M_ServiceWithApiKey extends M_Service {
	
	/* -- PROPERTIES -- */
	
	/**
	 * API Key
	 * 
	 * This property stores the API Key that is used by the class, when 
	 * communicating with the service.
	 * 
	 * @access private
	 * @var string
	 */
	private $_apiKey;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Construct the Service with API Key
	 * 
	 * @access public
	 * @param string $apiKey
	 * 		The API Key that is used when communicating with the service
	 * @return M_ServiceWithApiKey
	 */
	public function __construct($apiKey) {
		$this->setApiKey($apiKey);
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set the API Key
	 * 
	 * @access public
	 * @param string $apiKey
	 * 		The API Key that is used when communicating with the service
	 * @return void
	 */
	public function setApiKey($apiKey) {
		$this->_apiKey = (string) $apiKey;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Provides API Key
	 * 
	 * @access public
	 * @return string $apiKey
	 * 		The API Key that is used when communicating with the service
	 */
	public function getApiKey() {
		return $this->_apiKey;
	}
}