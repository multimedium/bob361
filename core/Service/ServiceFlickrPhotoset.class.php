<?php
/**
 * M_ServiceFlickrPhotoset
 * 
 * ... contains functions to handle Flickr Photosets
 * 
 * @package Core
 */
class M_ServiceFlickrPhotoset extends M_Object {
	
	/* -- PROPERTIES -- */
	
	/**
	 * The Flickr API
	 * 
	 * This property stores the Flickr API Instance that has been used to retrieve
	 * this photoset. For more info, read {@link M_ServiceFlickr}
	 * 
	 * @access private
	 * @var M_ServiceFlickr
	 */
	private $_flickrService;
	private $_photosetId;
	private $_userId;
	private $_numberOfPhotos;
	private $_title;
	private $_description;
	
	/* -- CONSTRUCTORS -- */
	
	public function __construct(M_ServiceFlickr $flickr, $photosetId) {
		$this->_flickrService = $flickr;
		$this->setPhotosetId($photosetId);
	}
	
	/* -- GETTERS -- */
	
	public function getPhotosetId() {
		return $this->_photosetId;
	}
	
	public function getUserId() {
		return $this->_userId;
	}
	
	public function getNumberOfPhotos() {
		return $this->_numberOfPhotos;
	}
	
	public function getTitle() {
		return $this->_title;
	}
	
	public function getDescription() {
		return $this->_description;
	}
	
	public function getPhotos($numberOfItemsPerPage = 100, $pageNumber = 1) {
		return $this->_flickrService->getPhotosOfPhotosetId(
			$this->getPhotosetId(),
			$numberOfItemsPerPage,
			$pageNumber
		);
	}
	
	/* -- SETTERS -- */
	
	public function setPhotosetId($photosetId) {
		$this->_photosetId = (string) $photosetId;
	}
	
	public function setUserId($userId) {
		$this->_userId = (string) $userId;
	}
	
	public function setNumberOfPhotos($numberOfPhotos) {
		$this->_numberOfPhotos = (int) $numberOfPhotos;
	}
	
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	public function setDescription($description) {
		$this->_description = (string) $description;
	}
}