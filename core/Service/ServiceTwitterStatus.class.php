<?php
/**
 * M_ServiceTwitterStatus
 *
 * ... contains the data of a twitter user-status
 */
class M_ServiceTwitterStatus {

	/* -- PROPERTIES -- */

	/**
	 * @var string
	 */
	private $_createdAt;

	/**
	 *
	 * @var float
	 */
	private $_id;

	/**
	 * @var string
	 */
	private $_text;

	/**
	 * @var string
	 */
	private $_source;

	/**
	 * @var M_ServiceTwitterUser
	 */
	private $_user;

	/**
	 * @var string
	 */
	private $_usernameFrom;

	/**
	 * @var string
	 */
	private $_usernameTo;
	
	/* -- GETTERS -- */
	
	/**
	 * @return M_Date
	 */
	public function getCreatedAt() {
		return new M_Date($this->_createdAt);
	}

	/**
	 * @return float
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @return string
	 */
	public function getText() {
		return $this->_text;
	}

	/**
	 * @return the string
	 */
	public function getSource() {
		return $this->_source;
	}

	/**
	 * get User
	 *
	 * @return M_ServiceTwitterUser
	 */
	public function getUser() {
		return $this->_user;
	}

	/**
	 * get Username (from)
	 *
	 * @return string
	 */
	public function getUsernameFrom() {
		return $this->_usernameFrom;
	}

	/**
	 * get Username (to)
	 * 
	 * @return string
	 */
	public function getUsernameTo() {
		return $this->_usernameTo;
	}

	/* -- SETTERS -- */

	/**
	 * @param string
	 */
	public function setCreatedAt($_createdAt) {
		$this->_createdAt = $_createdAt;
	}

	/**
	 * @param string
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}

	/**
	 * @param string
	 */
	public function setText($_text) {
		$this->_text = $_text;
	}

	/**
	 * @param string
	 */
	public function setSource($_source) {
		$this->_source = $_source;
	}

	/**
	 * Set the user of this status
	 *
	 * @param M_ServiceTwitterUser $user
	 */
	public function setUser(M_ServiceTwitterUser $user) {
		$this->_user = $user;
	}

	/**
	 * Set the username to which this message has been sent
	 * 
	 * @param string $username
	 */
	public function setUsernameTo($username) {
		$this->_usernameTo = $username;
	}

	/**
	 * Set the username from which this message has been sent
	 *
	 * @param string $username
	 */
	public function setUsernameFrom($username) {
		$this->_usernameFrom = $username;
	}
}