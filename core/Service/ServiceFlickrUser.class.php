<?php
/**
 * M_ServiceFlickrUser
 * 
 * ... contains functions to handle Flickr Users
 * 
 * @package Core
 */
class M_ServiceFlickrUser extends M_Object {
	
	/* -- PROPERTIES -- */
	
	private $_flickrService;
	private $_userId;
	private $_username;
	private $_realname;
	private $_pathalias;
	private $_location;
	private $_photosUri;
	private $_profileUri;
	private $_mobileUri;
	private $_numberOfPhotos;
	private $_firstDateOfPhoto;
	private $_isAdmin;
	private $_isPro;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param M_ServiceFlickr $flickr
	 * 		The constructed Flickr Service API
	 * @return M_ServiceFlickrUser
	 */
	public function __construct(M_ServiceFlickr $flickr, $userId, $username) {
		$this->_flickrService = $flickr;
		$this->setUserId($userId);
		$this->setUsername($username);
	}
	
	/* -- GETTERS -- */
	
	public function getUserId() {
		return $this->_userId;
	}
	
	public function getUsername() {
		return $this->_username;
	}
	
	public function getRealName() {
		return $this->_realname;
	}
	
	public function getPathAlias() {
		return $this->_pathalias;
	}
	
	public function getLocation() {
		return $this->_location;
	}
	
	public function getPhotosUri() {
		return $this->_photosUri;
	}
	
	public function getProfileUri() {
		return $this->_profileUri;
	}
	
	public function getMobileUri() {
		return $this->_mobileUri;
	}
	
	public function getNumberOfPhotos() {
		return $this->_numberOfPhotos;
	}
	
	public function getFirstDateOfPhoto() {
		return $this->_firstDateOfPhoto;
	}
	
	public function isAdmin() {
		return $this->_isAdmin;
	}
	
	public function isPro() {
		return $this->_isPro;
	}
	
	/* -- SETTERS -- */
	
	public function setUserId($userId) {
		$this->_userId = (string) $userId;
	}
	
	public function setUsername($username) {
		$this->_username = $username;
	}
	
	public function setRealName($realname) {
		$this->_realname = (string) $realname;
	}
	
	public function setPathAlias($alias) {
		$this->_pathalias = (string) $alias;
	}
	
	public function setLocation($location) {
		$this->_location = (string) $location;
	}
	
	public function setPhotosUri(M_Uri $uri) {
		$this->_photosUri = $uri;
	}
	
	public function setProfileUri(M_Uri $uri) {
		$this->_profileUri = $uri;
	}
	
	public function setMobileUri(M_Uri $uri) {
		$this->_mobileUri = $uri;
	}
	
	public function setNumberOfPhotos($count) {
		$this->_numberOfPhotos = (int) $count;
	}
	
	public function setFirstDateOfPhoto(M_Date $date) {
		$this->_firstDateOfPhoto = $date;
	}
	
	public function setIsAdmin($flag) {
		$this->_isAdmin = (bool) $flag;
	}
	
	public function setIsPro($flag) {
		$this->_isPro = (bool) $flag;
	}
}