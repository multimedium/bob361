<?php
/**
 * M_ServiceGoogleMaps
 * 
 * ... contains functions to communicate with the Google Maps API
 *
 * @link http://code.google.com/intl/nl/apis/maps/documentation/v3/basics.html
 * @package Core
 * @author Ben Brughmans
 */
class M_ServiceGoogleMaps extends M_Service {

	/**
	 * Status G_GEO_SUCCESS
	 */
	const STATUS_OK = 'OK';
	const SERVICE_NAME = 'google-maps';
	
	/**
	 * Holds the url which points to the geocoding service
	 * 
	 * @var string
	 */
	private $_geoServiceUrl = 'http://maps.googleapis.com/maps/api/geocode/json';

	/**
	 * Get the geo-service URI
	 * 
	 * @return M_Uri
	 */
	private function _getGeoServiceUri() {
		return new M_Uri($this->_geoServiceUrl);
	}

	/**
	 * Get the coördinates of an address
	 *
	 * Will return M_ServiceGoogleMapsLocation holding the latitude and longitude.
	 *
	 * Also the given {@link M_ContactAddress} object will be updated with
	 * the correct latitude and longitude
	 *
	 * @param M_ContactAddress $address
	 * @link http://code.google.com/apis/maps/documentation/reference.html#GGeoStatusCode
	 * @return M_ServiceGoogleMapsLocation
	 * @author Ben Brughmans
	 */
	public function getCoordinatesByAddress( M_ContactAddress $address) {
		$location = $this->getCoordinatesByString($address->toString());

		// update address
		 $address
			->setLatitude($location->getLatitude())
			->setLongitude($location->getLongitude());

		// return location
		 return $location;
	}

	/**
	 * Get the coördinates of a string
	 *
	 * Will return array holding latitude and longitude values:
	 * array('lat' => 51.24658296002271 , 'lng' => 5.201823749999992);
	 *
	 * @param string $address
	 * @link http://code.google.com/apis/maps/documentation/reference.html#GGeoStatusCode
	 * @return M_ServiceGoogleMapsLocation
	 * @author Ben Brughmans
	 */
	public function getCoordinatesByString($string) {
		
		sleep(1);
		
		// Set all properties
		$uri = $this->_getGeoServiceUri();
		$uri->setQueryVariable('address', $string);
		$uri->setQueryVariable('sensor', 'false');
		
		// Get Google Maps Coordinates
		$jsonData = $this->_callUri($uri, self::SERVICE_NAME);
		$jsonData = utf8_encode($jsonData);
		$arrayData = M_Helper::jsonDecode($jsonData);

		// Get and check status code
		$statusCode = $arrayData->status;
		
		if($statusCode != self::STATUS_OK) {
			throw new M_Exception(sprintf(
				'Cannot get coördinates for address "%s", received error with'.
				'status code %s',
				$string,
				$statusCode)
			);
		}

		// Return location object
		$location = new M_ServiceGoogleMapsLocation();
		$location->setLatitude($arrayData->results[0]->geometry->location->lat);
		$location->setLongitude($arrayData->results[0]->geometry->location->lng);

		return $location;
	}

	/**
	 * Lookup an address using a location (coordinates)
	 * 
	 * @param float $latitude
	 * @param float $longitude
	 * @return M_ContactAddress
	 */
	public function getAddressByCoordinates($latitude, $longitude) {
		
		$uri = $this->_getGeoServiceUri();
		$uri->setQueryVariable('latlng', $latitude.','.$longitude);
		$uri->setQueryVariable('sensor', 'false');
		
		// Get Google Maps Coordinates
		$jsonData = $this->_callUri($uri, self::SERVICE_NAME);
		$jsonData = utf8_encode($jsonData);
		$arrayData = M_Helper::jsonDecode($jsonData);
		
		// Get and check status code
		$statusCode = $arrayData->status;
		if($statusCode != self::STATUS_OK) {
			throw new M_Exception(sprintf(
				'Cannot get addres for coordinates "%s:%s", received error with'.
				'status code %s',
				$latitude,
				$longitude,
				$statusCode)
			);
		}		

		// Create address from response
		$address = new M_ContactAddress();
		foreach($arrayData->results[0]->address_components as $data) {
			$value = $data->long_name;
			switch ($data->types) {
				case array('street_number'):
					$address->setStreetNumber($value);
					break;
				case array('route'):
					$address->setStreetName($value);
					break;
				case array('postal_code'):
					$address->setPostalCode($value);
					break;
				case array("locality", "political"):
					$address->setCity($value);
					break;
				case array("country", "political");
					$address->setCountryISO($data->short_name);
					$address->setCountry($value);
					break;
			}
		}
		
		// Set coordinates
		$address
			->setStreetAddress($arrayData->results[0]->formatted_address)
			->setLatitude($latitude)
			->setLongitude($longitude);
		
		return $address;
	}
}