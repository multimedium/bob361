<?php

// Twitter class
require_once(M_Loader::getAbsolute('core/_thirdparty/twitter/Twitter.class.php'));

/**
 * M_ServiceTwitter
 *
 * ... contains functions to communicate with a Twitter stream
 *
 * @package Core
 * @author Ben Brughmans
 */

class M_ServiceTwitter extends M_Service {

	/**
	 * @var Twitter
	 */
	private $_twitter;

	/**
	 * @var string
	 */
	private $_key;

	/**
	 * @var string
	 */
	private $_secret;

	/* -- PUBLIC -- */

	/**
	 * Construct
	 *
	 * Create a twitter Client
	 * 
	 * @param string $username
	 * @param string $password
	 */
	public function __construct($key = null, $secret = null) {
		// Username & Password
		if (!is_null($key)) $this->_key = $key;
		if (!is_null($secret)) $this->_secret = $secret;
	}

	/**
	 * Set the key which will be used to connect to the API
	 * 
	 * @param string $key
	 * @return M_ServiceTwitter
	 */
	public function setKey($key) {
		$this->_key = $key;
		return $this;
	}

	/**
	 * Set the secret which will be used to connect to the API
	 *
	 * @param string $secret
	 * @return M_ServiceTwitter
	 */
	public function setSecret($secret) {
		$this->_secret = $secret;
		return $this;
	}
	
	/**
	 * Get the UserTimeline
	 *
	 * @access public
	 * @param string $username
	 * @param int $limit
	 * @param bool $includeRetweets
	 * @return M_ArrayIterator
	 */
	public function getUserTimeLine($username, $limit = 20, $includeRetweets = true) {
		// Ask the timeline from the Twitter-object
		$timeline = $this->_getTwitter()->statusesUserTimeline(
			(string) $username,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			(bool)$includeRetweets
		);
		
		return $this->_getStatusObjectsFromTimelineArray($timeline, $limit);
	}

	/**
	 * Search twitter
	 * 
	 * @param string $keyword
	 * @param int $maxResultsPerPage
	 * @param M_Date $until
	 * @param string|array the username(s) from which the tweet has been sent
	 * @return M_ArrayIterator
	 *
	 * @see	http://dev.twitter.com/doc/get/search
	 * @author Ben Brughmans
	 */
	public function search($keyword, $maxResultsPerPage = null, M_Date $until = null, $from = null) {

		//create the query-string
		$queryString = 'q='.urlencode($keyword);

		//max-results?
		if (!is_null($maxResultsPerPage)) $queryString .= '&rpp=' . (int)$maxResultsPerPage;
		
		//until a certain date in time?
		if(!is_null($until)) $queryString .= '&until=' . $until->toString ('yyyy-mm-dd');

		//from one (or more users)
		if(!is_null($from)) {

			if(!is_array($from)) $form = array($from);

			//if an array is provided: check if at least one username has been given
			elseif(count($from) == 0) {
				throw new M_ServiceException('Please provide at least one username in the from-array');
			}

			$queryStringFrom ='&from=' . urlencode(array_shift($from));

			//compose querystring
			foreach($from AS $username) {
				$queryStringFrom .= ' OR ' . urlencode($username);
			}

			$queryString .= $queryStringFrom;
		}

		//compose url and perform call
		$uri = new M_Uri("http://search.twitter.com/search.json?" . $queryString);
		$results = $this->_callUri($uri, 'twitter');

		//store all output data in an array
		$output = array();

		//if we call was succesfull
		if($results) {
			$results = M_Helper::jsonDecode($results);
			$resultData = M_Helper::getArrayElement('results', (array)$results, array());
			foreach( $resultData AS $result ) {
				$output[] = $this->_arrayToServiceTwitterStatus((array)$result);
			}
		}

		return new M_ArrayIterator($output);
	}
	
	/* -- PRIVATE / PROTECTED -- */

	/**
	 * Get Twitter
	 * 
	 * @return Twitter
	 */
	protected function _getTwitter() {
		if(is_null($this->_twitter)) {
			try {
				$twitter = new Twitter($this->_key, $this->_secret);

				// get a request token
				$twitter->oAuthRequestToken();

				$this->_twitter = $twitter;
			
			} catch(Exception $e) {}

		}

		return $this->_twitter;
	}

	/**
	 * Get status objects from timeline array
	 * 
	 * @param array $timeline
	 * @return M_ArrayIterator
	 */
	protected function _getStatusObjectsFromTimelineArray($timeline, $limit = 20) {
		$result = array();

		// Create a arrayiterator of the statuses
		foreach($timeline as $status) {
			$result[] = $this->_arrayToServiceTwitterStatus($status);
		}

		// Slice the limit
		$result = array_slice($result, 0, $limit);

		// Return the iterator
		return new M_ArrayIterator($result);
	}

	/**
	 * Array To Object M_ServiceTwitterStatus
	 * 
	 * @param array $status
	 * @return M_ServiceTwitterStatus
	 */
	protected function _arrayToServiceTwitterStatus(array $status) {
		// Create status object
		$serviceTwitterStatus = new M_ServiceTwitterStatus();

		// Set the variables
		$serviceTwitterStatus->setId($status['id']);
		$serviceTwitterStatus->setText($status['text']);
		$serviceTwitterStatus->setCreatedAt($status['created_at']);
		$serviceTwitterStatus->setSource($status['source']);
		if (isset($status['from_user'])) {
			$serviceTwitterStatus->setUsernameFrom($status['from_user']);
		}
		if (isset($status['to_user'])) {
			$serviceTwitterStatus->setUsernameTo($status['to_user']);
		}
		if (isset($status['user'])) {
			$serviceTwitterStatus->setUser($this->_arrayToServiceTwitterUser($status['user']));
		}

		// Return the object
		return $serviceTwitterStatus;
	}

	/**
	 * Array To Object M_ServiceTwitterUser
	 * 
	 * @param array $user
	 * @param bool $extended
	 * @return M_ServiceTwitterUser
	 */
	protected function _arrayToServiceTwitterUser(array $user, $extended = false) {

		// Create object
		$serviceTwitterUser = new M_ServiceTwitterUser();

		// Set the basic properties
		$serviceTwitterUser->setName($user['name']);
		$serviceTwitterUser->setScreenName($user['screen_name']);
		$serviceTwitterUser->setDescription($user['description']);
		$serviceTwitterUser->setLocation($user['location']);
		$serviceTwitterUser->setUrl($user['url']);
		$serviceTwitterUser->setProtected($user['protected']);
		$serviceTwitterUser->setFollowersCount($user['followers_count']);
		$serviceTwitterUser->setProfileImageUrl($user['profile_image_url']);

		// If extended
		if($extended)
		{
			// Set extra properties
			if(isset($user['profile_background_color'])) $serviceTwitterUser->setBgColor(utf8_decode((string) $user['profile_background_color']));
			if(isset($user['profile_text_color'])) $serviceTwitterUser->setTextColor(utf8_decode((string) $user['profile_text_color']));
			if(isset($user['profile_link_color'])) $serviceTwitterUser->setLinkColor(utf8_decode((string) $user['profile_link_color']));
			if(isset($user['profile_sidebar_fill_color'])) $serviceTwitterUser->setSidebarBgColor(utf8_decode((string) $user['profile_sidebar_fill_color']));
			if(isset($user['profile_sidebar_border_color'])) $serviceTwitterUser->setSidebarBorderColor(utf8_decode((string) $user['profile_sidebar_border_color']));
			if(isset($user['statuses_count'])) $serviceTwitterUser->setStatusesCount((int) $user['statuses_count']);
			if(isset($user['favourites_count'])) $serviceTwitterUser->setFavouritesCount((int) $user['favourites_count']);
			if(isset($user['utc_offset'])) $serviceTwitterUser->setUtcOffset((int) $user['utc_offset']);
		}

		// Return the object
		return $serviceTwitterUser;
	}
}