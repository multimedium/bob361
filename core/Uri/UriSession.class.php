<?php
/**
 * M_UriSession class
 * 
 * M_UriSession is used to work with URL sessions. Typically, M_UriSession is used
 * to get the contents of a page at a given URL, to check if a given URL exists,
 * to upload data to a URL, etcetera...
 * 
 * @package Core
 */
class M_UriSession extends M_Object {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Error Code
	 * 
	 * This is one of the possible error codes that may be returned by the
	 * method {@link M_UriSession::getErrorCode()}
	 * 
	 * This particular error code is not actually an error. This error code would
	 * be returned by {@link M_UriSession::getErrorCode()} if everything went well.
	 * 
	 * @see M_UriSession::getErrorCode()
	 * @var integer
	 */
	const E_OK = 0;
	
	/**
	 * Error Code
	 * 
	 * This is one of the possible error codes that may be returned by the
	 * method {@link M_UriSession::getErrorCode()}
	 * 
	 * @see M_UriSession::getErrorCode()
	 * @var integer
	 */
	const E_UNSUPPORTED_PROTOCOL = 1;
	
	/**
	 * Error Code
	 * 
	 * This is one of the possible error codes that may be returned by the
	 * method {@link M_UriSession::getErrorCode()}
	 * 
	 * @see M_UriSession::getErrorCode()
	 * @var integer
	 */
	const E_FAILED_INIT = 2;
	
	/**
	 * Error Code
	 * 
	 * This is one of the possible error codes that may be returned by the
	 * method {@link M_UriSession::getErrorCode()}
	 * 
	 * @see M_UriSession::getErrorCode()
	 * @var integer
	 */
	const E_URL_MALFORMAT = 3;
	
	/**
	 * Error Code
	 * 
	 * This is one of the possible error codes that may be returned by the
	 * method {@link M_UriSession::getErrorCode()}
	 * 
	 * @see M_UriSession::getErrorCode()
	 * @var integer
	 */
	const E_URL_MALFORMAT_USER = 4; // Not used!
	
	/**
	 * Error Code
	 * 
	 * This is one of the possible error codes that may be returned by the
	 * method {@link M_UriSession::getErrorCode()}
	 * 
	 * @see M_UriSession::getErrorCode()
	 * @var integer
	 */
	const E_COULDNT_RESOLVE_PROXY = 5;
	
	/**
	 * Error Code
	 * 
	 * This is one of the possible error codes that may be returned by the
	 * method {@link M_UriSession::getErrorCode()}
	 * 
	 * @see M_UriSession::getErrorCode()
	 * @var integer
	 */
	const E_COULDNT_RESOLVE_HOST = 6;
	
	/**
	 * Error Code
	 * 
	 * This is one of the possible error codes that may be returned by the
	 * method {@link M_UriSession::getErrorCode()}
	 * 
	 * @see M_UriSession::getErrorCode()
	 * @var integer
	 */
	const E_COULDNT_CONNECT = 7;
	
	/**
	 * Error Code
	 * 
	 * This is one of the possible error codes that may be returned by the
	 * method {@link M_UriSession::getErrorCode()}
	 * 
	 * @see M_UriSession::getErrorCode()
	 * @var integer
	 */
	const E_FTP_WEIRD_SERVER_REPLY = 8;
	
	/* -- PROPERTIES -- */
	
	/**
	 * URI
	 * 
	 * This property stores the URI.
	 * 
	 * @access private
	 * @var M_Uri
	 */
	private $_uri;

	/**
	 * URI
	 *
	 * This property stores the CA Certificate.
	 *
	 * @see M_Session::setCACertificate()
	 * @access private
	 * @var M_File
	 */
	private $_certificate;

	/**
	 * Contents
	 *
	 * This property stores the (raw) contents that have been returned by the
	 * URI. In other words: it is the response string
	 *
	 * @access private
	 * @var string
	 */
	private $_response = NULL;

	/**
	 * cURL Handle
	 * 
	 * This property stores the cURL handle that is used to manage the session.
	 *
	 * @access private
	 * @var resource
	 */
	private $_curlHandle;

	/**
	 * POST Data
	 *
	 * Stores the (POST) variables that are to be sent in the request
	 *
	 * @access private
	 * @var array
	 */
	private $_postData = array();
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param M_Uri $uri
	 * 		The URI in which to start a session
	 * @return M_UriSession
	 */
	public function __construct(M_Uri $uri) {
		// Check if the cURL functions are available:
		if(! function_exists('curl_init')) {
			// Throw an exception if not
			// (thus preventing the object to be constructed)
			throw new M_UriException(sprintf(
				'Cannot instantiate %s; Missing cURL Extension for PHP',
				__CLASS__
			));
		}
		
		// Set the URI:
		$this->_uri = $uri;

		// By default, we enable redirects
		$this->setFollowRedirects(TRUE);
	}
	
	/* -- DESTRUCTOR -- */
	
	/**
	 * Destructor
	 * 
	 * When all references to the {@link M_UriSession} instances are removed,
	 * the object will be destructed (by PHP) by a call to this method. This
	 * method will destroy the internal cURL handle, in order to free memory.
	 * 
	 * @access public
	 * @return void
	 */
	public function __destruct() {
		// Close the cURL session
		curl_close($this->_curlHandle);
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Exists?
	 * 
	 * Will check whether or not the URL exists. In other words, this method will
	 * make sure that the URL does not return a "HTTP/404 File not found" error
	 * page.
	 * 
	 * NOTE!
	 * In order to do so, this method will send a request to the URL.
	 * 
	 * @access public
	 * @param bool $refresh
	 *		Set to TRUE if you want to refresh the response, FALSE if you want to
	 *		allow a cache'd response
	 * @return boolean $flag
	 * 		Returns TRUE if the URL exists, FALSE if not
	 */
	public function uriExists($refresh = FALSE) {
		// Get the response:
		$this->_getCurlExecResponse($refresh);
		
		// Get the error code from the session:
		$e = $this->getErrorCode();
		
		// Return result (boolean flag)
		switch($e) {
			case self::E_COULDNT_CONNECT:
			case self::E_COULDNT_RESOLVE_HOST:
			case self::E_COULDNT_RESOLVE_PROXY:
			case self::E_UNSUPPORTED_PROTOCOL:
			case self::E_URL_MALFORMAT:
			case self::E_URL_MALFORMAT_USER:
				return FALSE;
			
			default:
				return TRUE;
		}
	}
	
	/**
	 * Get contents
	 * 
	 * Will execute the session, and fetch the contents (response) of the URI
	 * 
	 * @access public
	 * @param bool $refresh
	 *		Set to TRUE if you want to refresh the response, FALSE if you want to
	 *		allow a cache'd response
	 * @return string
	 */
	public function getContents($refresh = FALSE) {
		return $this->_getCurlExecResponse($refresh);
	}
	
	/**
	 * Get Error Code
	 * 
	 * Will provide with the error code that has been generated by the session.
	 * For more information about error codes, read the docs on
	 * 
	 * - {@link M_UriSession::E_COULDNT_CONNECT}
	 * - {@link M_UriSession::E_COULDNT_RESOLVE_HOST}
	 * - {@link M_UriSession::E_COULDNT_RESOLVE_PROXY}
	 * - {@link M_UriSession::E_FAILED_INIT}
	 * - {@link M_UriSession::E_FTP_WEIRD_SERVER_REPLY}
	 * - {@link M_UriSession::E_OK}
	 * - {@link M_UriSession::E_UNSUPPORTED_PROTOCOL}
	 * - {@link M_UriSession::E_URL_MALFORMAT}
	 * - {@link M_UriSession::E_URL_MALFORMAT_USER}
	 *
	 * @see M_UriSession::getErrorString()
	 * @access public
	 * @return int $errorCode
	 */
	public function getErrorCode() {
		return curl_errno($this->_getCurl());
	}

	/**
	 * Get Error string
	 *
	 * Returns a clear text error message for the last request
	 *
	 * @see M_UriSession::getErrorCode()
	 * @access public
	 * @return string $errorString
	 */
	public function getErrorString() {
		return curl_error($this->_getCurl());
	}

	/**
	 * Get total transaction time
	 *
	 * This method will provide with the total transaction time, in seconds,
	 * of the last request
	 *
	 * @access public
	 * @return integer
	 */
	public function getTotalTransactionTime() {
		return curl_getinfo($this->_getCurl(), CURLINFO_TOTAL_TIME);
	}

	/**
	 * Get name lookup time
	 *
	 * This method will provide with the time, in seconds, until name resolving
	 * was complete
	 *
	 * @access public
	 * @return integer
	 */
	public function getNameLookupTime() {
		return curl_getinfo($this->_getCurl(), CURLINFO_NAMELOOKUP_TIME);
	}

	/**
	 * Get connection time
	 *
	 * This method will provide with the time, in seconds, it took to establish
	 * the connection
	 *
	 * @access public
	 * @return integer
	 */
	public function getConnectionTime() {
		return curl_getinfo($this->_getCurl(), CURLINFO_CONNECT_TIME);
	}

	/**
	 * Get redirection time
	 *
	 * This method will provide with the time, in seconds, of all redirection
	 * steps before final transaction was started
	 *
	 * @access public
	 * @return integer
	 */
	public function getRedirectTime() {
		return curl_getinfo($this->_getCurl(), CURLINFO_REDIRECT_TIME);
	}

	/**
	 * Get bytes uploaded
	 *
	 * Will return the total number of bytes uploaded
	 *
	 * @access public
	 * @return integer
	 */
	public function getBytesUploaded() {
		return curl_getinfo($this->_getCurl(), CURLINFO_SIZE_UPLOAD);
	}

	/**
	 * Get bytes downloaded
	 *
	 * Will return the total number of bytes downloaded
	 *
	 * @access public
	 * @return integer
	 */
	public function getBytesDownloaded() {
		return curl_getinfo($this->_getCurl(), CURLINFO_SIZE_DOWNLOAD);
	}

	/**
	 * Get average upload speed
	 *
	 * @access public
	 * @return integer
	 */
	public function getAverageUploadSpeed() {
		return curl_getinfo($this->_getCurl(), CURLINFO_SPEED_UPLOAD);
	}

	/**
	 * Get average download speed
	 *
	 * @access public
	 * @return integer
	 */
	public function getAverageDownloadSpeed() {
		return curl_getinfo($this->_getCurl(), CURLINFO_SPEED_DOWNLOAD);
	}

	/**
	 * Get header size
	 *
	 * This method will return the total size of all headers received
	 *
	 * @access public
	 * @return integer
	 */
	public function getHeadersReceivedSize() {
		return curl_getinfo($this->_getCurl(), CURLINFO_HEADER_SIZE);
	}

	/**
	 * Get request string sent
	 *
	 * @access public
	 * @return string
	 */
	public function getRequestStringSent() {
		return curl_getinfo($this->_getCurl(), CURLINFO_HEADER_OUT);
	}

	/**
	 * Get Content type
	 *
	 * Will return the content type of the downloaded 'object'. NULL indicates
	 * that the server did not send a valid 'Content-Type' header
	 *
	 * @access public
	 * @return string
	 */
	public function getContentType() {
		return curl_getinfo($this->_getCurl(), CURLINFO_CONTENT_TYPE);
	}

	/**
	 * Get Last received HTTP code
	 *
	 * @access public
	 * @return string
	 */
	public function getHttpCode() {
		return curl_getinfo($this->_getCurl(), CURLINFO_HTTP_CODE);
	}

	/* -- SETTERS -- */

	/**
	 * Set connection timeout
	 *
	 * This method can be used to set the number of seconds to wait while trying
	 * to connect. Use 0 to wait indefinitely.
	 *
	 * @access public
	 * @param integer $seconds
	 *		The number of seconds
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setConnectionTimeout($seconds) {
		// Set the timeout
		$this->_setCurlOptions(array(CURLOPT_CONNECTTIMEOUT => (int) $seconds));

		// Finally, return myself
		return $this;
	}

	/**
	 * The maximum number of seconds to allow cURL functions to execute.
	 *
	 * @access public
	 * @param integer $seconds
	 *		The number of seconds
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTimeout($seconds) {
		// Set the timeout
		$this->_setCurlOptions(array(CURLOPT_TIMEOUT => (int) $seconds));

		// Finally, return myself
		return $this;
	}

	/**
	 * Set DNS Cache Lifetime
	 *
	 * Tis method can be used to set the number of seconds to keep DNS entries in
	 * memory. This option is set to 120 (2 minutes) by default.
	 *
	 * @access public
	 * @param integer $seconds
	 *		The number of seconds
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDnsCacheLifetime($seconds) {
		// Set the cache lifetime
		$this->_setCurlOptions(array(CURLOPT_DNS_CACHE_TIMEOUT => (int) $seconds));

		// Finally, return myself
		return $this;
	}

	/**
	 * Set PORT for connection
	 *
	 * This method can be used to set an alternative port number to connect to.
	 *
	 * @access public
	 * @param integer $port
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPort($port) {
		// Set the cache lifetime
		$this->_setCurlOptions(array(CURLOPT_PORT => (int) $port));

		// Finally, return myself
		return $this;
	}

	/**
	 * Set headers
	 *
	 * This method will set an array with header information for the request. An
	 * example:
	 *
	 * <code>
	 *    $session = new M_UriSession(new M_Uri('http://www.google.com'));
	 *    $session->setHeaders(array(
	 *       'Content-type: text/plain',
	 *       'Content-length: 100'
	 *    ));
	 * </code>
	 *
	 * @access public
	 * @param array $headers
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setHeaders(array $headers) {
		// Set the headers for the request
		$this->_setCurlOptions(array(CURLOPT_HTTPHEADER => $headers));

		// Finally, return myself
		return $this;
	}

	/**
	 * Write headers to...
	 *
	 * Sets the file that the header part of the transfer is written to
	 *
	 * @access public
	 * @param M_File $headersFile
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setWriteHeader(M_File $headersFile) {
		$this->_setCurlOptions(array(CURLOPT_WRITEHEADER => $headersFile->getPath()));
		return $this;
	}

	/**
	 * Include header information in output?
	 *
	 * This method can be used to specify whether or not the header information
	 * should be provided in the output of the request. Set to TRUE to include
	 * the header in the output, FALSE if not.
	 *
	 * @access public
	 * @param bool $flag
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setIncludeHeaderInOutput($flag) {
		$this->_setCurlOptions(array(CURLOPT_HEADER => (bool) $flag));
		return $this;
	}

	/**
	 * Set Flag: "Follow redirects?"
	 *
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE to follow any "Location: " header that the server sends
	 * 		as part of the HTTP header (note this is recursive, PHP will follow
	 * 		as many "Location: " headers that it is sent, unless  $maxRedirects
	 * 		is set).
	 * @param integer $maxRedirects
	 * 		The maximum number of "Location: " headers to follow
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFollowRedirects($followRedirects, $maxRedirects = 0) {
		// Set up referrer:
		$ops = array();
		$ops[CURLOPT_AUTOREFERER] = TRUE;

		// Follow redirects?
		// (CURLOPT_FOLLOWLOCATION cannot be activated when an
		// open_basedir is set. So, before setting the cURL option, we check for 
		// for the open_basedir configuration:
		if($followRedirects && ini_get('open_basedir') == '') {
			// Set the cURL option
			$ops[CURLOPT_FOLLOWLOCATION] = TRUE;
			
			// Maximum # of redirects?
			if($maxRedirects) {
				$ops[CURLOPT_MAXREDIRS] = $maxRedirects;
			}
		}
		// If not
		else {
			// Then, we do not allow redirects:
			$ops[CURLOPT_FOLLOWLOCATION] = FALSE;
		}

		// Set the options:
		$this->_setCurlOptions($ops);

		// Return myself
		return $this;
	}

	/**
	 * Set Authentication Method(s)
	 *
	 * This method sets the HTTP authentication method(s) to use. The options
	 * are: 
	 * 
	 * - CURLAUTH_BASIC, 
	 * - CURLAUTH_DIGEST, 
	 * - CURLAUTH_GSSNEGOTIATE, 
	 * - CURLAUTH_NTLM, 
	 * - CURLAUTH_ANY, and 
	 * - CURLAUTH_ANYSAFE
	 * 
	 * The bitwise | (or) operator can be used to combine more than one method.
	 * If this is done, cURL will poll the server to see what methods it supports 
	 * and pick the best one.
	 * 
	 * NOTE:
	 * CURLAUTH_ANY is an alias for
	 *
	 * <code>
	 * CURLAUTH_BASIC | CURLAUTH_DIGEST | CURLAUTH_GSSNEGOTIATE | CURLAUTH_NTLM
	 * </code>
	 *
	 * NOTE:
	 * CURLAUTH_ANYSAFE is an alias for
	 *
	 * <code>
	 * CURLAUTH_DIGEST | CURLAUTH_GSSNEGOTIATE | CURLAUTH_NTLM
	 * </code>
	 *
	 * This method is typically used to send a request to a http(s) page that
	 * needs basic authentication before submitting the information. Consider the
	 * following example:
	 *
	 * <code>
	 *    $uri = new M_Uri('http://www.mydomain.com');
	 *    $uri->setUsername('username');
	 *    $uri->setPassword('password');
	 *
	 *    $session = $uri->getSession();
	 *    $session->setAuthenticationMethod(CURLAUTH_BASIC);
	 * </code>
	 *
	 * @access public
	 * @param int $autenticationMethod
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAuthenticationMethod($autenticationMethod) {
		$this->_setCurlOptions(array(CURLOPT_HTTPAUTH => $autenticationMethod));
		return $this;
	}

	/**
	 * Set SSL Version
	 *
	 * The SSL version (2 or 3) to use. By default PHP will try to determine this
	 * itself, although in some cases this must be set manually.
	 *
	 * @access public
	 * @param integer $version
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSslVersion($version) {
		// The provided version must be 2 or 3!
		if($version != 2 && $version != 3) {
			// If that's not the case, we inform about an error:
			throw new M_Exception(sprintf(
				'Cannot set SSL Version in %s. The version must be either 2 or 3. ' .
				'The requested version is %s',
				$this->getClassName(),
				$version
			));
		}

		// Set the version
		$this->_setCurlOptions(array(CURLOPT_SSLVERSION => (int) $version));

		// Return myself
		return $this;
	}

	/**
	 * Set CA Certificate
	 *
	 * Is you are trying to access an HTTPS (SSL or TLS-protected resource), you
	 * are likely to run into some difficulty. Such a connection requires extra
	 * verifications, in order to ensure a secure communication with the server.
	 *
	 * In order to enable such a secure conversation with a server, you need to
	 * provide the URI Session with a CA Certificate that should be trusted. Thus,
	 * any server/peer certificates issued by this CA will also be trusted by the
	 * session.
	 *
	 * NOTE:
	 * {@link http://unitstep.net/blog/2009/05/05/using-curl-in-php-to-access-https-ssltls-protected-sites/}
	 * shows how you can create such a certificate for your connection.
	 *
	 * This method allows you to specify the certificate that is to be used in
	 * the connection with the URI.
	 *
	 * NOTE:
	 * Another option, while NOT recommendable, is to connect to the server without
	 * any certificate at all. You can do so by accepting ANY server (peer)
	 * certificate in the session. Of course, this is not optimal from a security
	 * point of view, but if you are not passing sensitive information forth and
	 * back, this is probably alright. For more information, read the docs on
	 * {@link M_UriSession::setAcceptanceOfAnyPeerCertificate()}
	 *
	 * @author Tom Bauwens
	 * @access public
	 * @param M_File $file
	 *		A file containing a PEM formatted certificate is expected
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCACertificate(M_File $file) {
		// Make sure the file exists:
		if(! $file->exists()) {
			// Throw new exception, if not the case
			throw new M_UriException(sprintf(
				'Cannot set CA Certificate; Cannot find file %s',
				$file->getPath()
			));
		}

		// Set the certificate
		$this->_certificate = $file;

		// Prepare some additional cURL options, so verifications are performed:
		$ops = array();
		
		// - Perform verification of the secure connection (certificate)
		$ops[CURLOPT_SSL_VERIFYPEER] = TRUE;

		// - Make sure that the common name exists and that it matches the host
		//   name of the server
		$ops[CURLOPT_SSL_VERIFYHOST] = 2;

		// - Set the certificate
		$ops[CURLOPT_CAINFO] = $this->_certificate->getPath();

		// Set the options
		$this->_setCurlOptions($ops);
		
		// Return myself
		return $this;
	}
	
	/**
	 * set SSL certificate
	 * 
	 * This method offers an alternative to the setCACertyficate method.
	 * It allows you to set an ssl certificate for the connection with
	 * a secure server.
	 * 
	 * 
	 * @param M_File $file
	 * @return \M_UriSession
	 * @throws M_UriException
	 */
	public function setSslCertificate(M_File $file) {
		// Make sure the file exists:
		if(! $file->exists()) {
			// Throw new exception, if not the case
			throw new M_UriException(sprintf(
				'Cannot set SSL certificate; Cannot find file %s',
				$file->getPath()
			));
		}
		
		// Set the certificate
		$this->_certificate = $file;

		// Prepare some additional cURL options, so verifications are performed:
		$ops = array();
		
		// set the certificate
		$ops[CURLOPT_SSLCERT] = $this->_certificate->getPath();
		
		// Set the options
		$this->_setCurlOptions($ops);
		
		// Return myself
		return $this;
	}

	/**
	 * Accept ANY Peer Certificate
	 *
	 * Is you are trying to access an HTTPS (SSL or TLS-protected resource), you
	 * are likely to run into some difficulty. Such a connection requires extra
	 * verifications, in order to ensure a secure communication with the server.
	 *
	 * The easy way out of this problem, is to accept ANY certificate that is
	 * issued by the server (peer), thus NOT verifying the certificates and the
	 * host to which you are connecting. You can do so by calling this method.
	 *
	 * For a more secure solution to this problem, you should read the documentation
	 * on {@link M_UriSession::setCACertificate()}
	 *
	 * @access public
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAcceptanceOfAnyPeerCertificate() {
		// Prepare options, so verification is not performed:
		$ops = array();
		$ops[CURLOPT_SSL_VERIFYPEER] = FALSE;

		// Set the options
		$this->_setCurlOptions($ops);
		
		// Return myself
		return $this;
	}

	/**
	 * Set Request method
	 *
	 * This method can be used to set a custom request method to use, when doing
	 * a HTTP request. Valid values are things like "GET", "POST", "CONNECT" and
	 * so on; i.e. Do not enter a whole HTTP request line here. For instance,
	 * entering "GET /index.html HTTP/1.0\r\n\r\n" would be incorrect.
	 *
	 * This method will use the cURL options:
	 *
	 * <code>CURLOPT_CUSTOMREQUEST</code>
	 *
	 * @access public
	 * @param string $requestMethod
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setRequestMethod($requestMethod) {
		// Set options that define the request method:
		$ops = array();
		$ops[CURLOPT_CUSTOMREQUEST] = $requestMethod;

		// Setting to POST?
		$ops[CURLOPT_POST] = ($requestMethod == M_Request::POST) ? TRUE : FALSE;

		// Setting to GET?
		$ops[CURLOPT_HTTPGET] = ($requestMethod == M_Request::GET) ? TRUE : FALSE;

		// Set the options
		$this->_setCurlOptions($ops);

		// Return myself
		return $this;
	}

	/**
	 * Set Request Method to POST
	 *
	 * @see M_Uri::setRequestMethod()
	 * @access public
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setRequestMethodPost() {
		return $this->setRequestMethod(M_Request::POST);
	}

	/**
	 * Set HTTP POST Variables
	 *
	 * This method can be used to set the data that is to be sent via HTTP POST.
	 * An associative array is expected, defining the name of each variable and
	 * its value.
	 *
	 * This method will use the cURL options:
	 *
	 * <code>CURLOPT_POSTFIELDS</code>
	 *
	 * IMPORTANT NOTE:
	 * Note that this method will also change the request method of the request
	 * to POST, by using the {@link M_UriSession::setRequestMethodPost()} method
	 *
	 * @uses M_UriSession::setRequestMethodPost()
	 * @access public
	 * @param array $data
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPostData(array $data) {
		// Set new request method
		$this->setRequestMethodPost();

		// Set internals
		// (we maintain a local copy of POST variables, so we can add variables
		// to this array later on; eg file uploads)
		$this->_postData = $data;

		// Also, set the post data in the cURL resource:
		$this->_setCurlOptions(array(CURLOPT_POSTFIELDS => http_build_query($data)));

		// Finally, return myself
		return $this;
	}

	/**
	 * Set HTTP POST Data
	 *
	 * This method can be used to set the data that is to be sent via HTTP POST.
	 * The data is sent as-is (not converted to a query with variables).
	 *
	 * This method will use the cURL options:
	 *
	 * <code>CURLOPT_POSTFIELDS</code>
	 *
	 * IMPORTANT NOTE:
	 * Note that this method will also change the request method of the request
	 * to POST, by using the {@link M_UriSession::setRequestMethodPost()} method
	 *
	 * @uses M_UriSession::setRequestMethodPost()
	 * @access public
	 * @param mixed $data
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPostDataRaw($data) {
		// Set new request method
		$this->setRequestMethodPost();

		// Set internals
		// (we maintain a local copy of POST variables, so we can add variables
		// to this array later on; eg file uploads)
		$this->_postData = $data;

		// Also, set the post data in the cURL resource:
		$this->_setCurlOptions(array(CURLOPT_POSTFIELDS => $data));

		// Finally, return myself
		return $this;
	}

	/**
	 * Set Header: User Agent
	 *
	 * This method can be used to set the contents of the "User-Agent: " header
	 * to be used in a HTTP request. Typically, this is done to simulate a given
	 * request with a given internet browser; eg. "Mozilla/4.0 (compatible;)"
	 *
	 * @access public
	 * @param string $userAgent
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setUserAgent($userAgent) {
		$this->_setCurlOptions(array(CURLOPT_USERAGENT => (string) $userAgent));
		return $this;
	}

	/**
	 * Set Cookie
	 *
	 * This method can be applied to set the contents of the "Cookie: " header
	 * that is to be used in the HTTP request. Note that multiple cookies are
	 * separated with a semicolon followed by a space
	 * (e.g., "fruit=apple; colour=red")
	 *
	 * @access public
	 * @param string $cookie
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCookie($cookie) {
		$this->_setCurlOptions(array(CURLOPT_COOKIE => (string) $cookie));
		return $this;
	}

	/**
	 * Set Cookie File
	 *
	 * Does the same thing as {@link M_UriSession::setCookie()}, except that this
	 * method will allow you to set a file that contains the cookie data. The
	 * cookie file can be in Netscape format, or just plain HTTP-style headers
	 * dumped into a file.
	 *
	 * @access public
	 * @param MI_FsItemFile $file
	 *		The cookie file
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCookieFile(MI_FsItemFile $file) {
		$this->_setCurlOptions(array(CURLOPT_COOKIEFILE => $file->getPath()));
		return $this;
	}

	/**
	 * Set Cookie Jar
	 *
	 * This method allows you to set the name of the file to save all internal
	 * cookies to when the connection closes.
	 *
	 * @access public
	 * @param MI_FsItemFile $jarFile
	 *		The cookie jar file
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCookieJar(MI_FsItemFile $file) {
		$this->_setCurlOptions(array(CURLOPT_COOKIEJAR => $file->getPath()));
		return $this;
	}

	/**
	 * Add file (for upload)
	 *
	 * This method can be used to add a file to the request, for upload. This
	 * will simulate a user-triggered upload, via a web form.
	 *
	 * IMPORTANT NOTE:
	 * Note that this method will also change the request method of the request
	 * to POST, because the method {@link M_UriSession::setPostData()} is used.
	 *
	 * @uses M_UriSession::setPostData()
	 * @access public
	 * @param string $variableName
	 *		The variable name, with which the file should be sent in the request
	 * @param MI_FsItemFile $file
	 *		The file to be included for upload
	 * @return M_UriSession $session
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFile($variableName, MI_FsItemFile $file) {
		// We make sure that the file exists:
		if(! $file->exists()) {
			// If the file does not exist, then we throw an exception in order
			// to inform about the error:
			throw new M_Exception(sprintf(
				'Cannot add file %s to %s; Cannot locate file',
				$file->getPath(),
				$this->getClassName()
			));
		}

		// Get POST Data:
		$data = $this->_postData;

		// Add the file path to the POST data:
		$data[(string) $variableName] = '@' . $file->getPath();

		// Set new POST Data:
		return $this->setPostData($data);
	}

	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get cURL Session
	 * 
	 * Will initiate a cURL session, and return the cURL handle if successful.
	 * Note that the cURL extension has to be installed. If not so, this method
	 * will return FALSE instead!
	 * 
	 * @access public
	 * @return mixed
	 */
	protected function _getCurl() {
		// If not yet initiated:
		if(! $this->_curlHandle) {
			// Create a new cURL resource
			$this->_curlHandle = @curl_init();

			// If we fail to do so:
			if(! $this->_curlHandle) {
				// Throw an exception if the resource cannot be created
				throw new M_UriException(
					'Cannot create cURL handle; curl_init() has failed'
				);
			}
			
			// Set URL and other appropriate options
			curl_setopt($this->_curlHandle, CURLOPT_URL, $this->_uri->toString());
			
			// Set the option that makes the session provide us with the raw 
			// response of the URL
			curl_setopt($this->_curlHandle, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($this->_curlHandle, CURLOPT_BINARYTRANSFER, TRUE);

			// If a username and password has been defined in the URL:
			if($this->_uri->getUsername() && $this->_uri->getPassword()) {
				curl_setopt(
					$this->_curlHandle,
					CURLOPT_USERPWD,
					$this->_uri->getUsername() . ':' . $this->_uri->getPassword()
				);
			}
		}
		
		// Return the cURL Handle
		return $this->_curlHandle;
	}
	
	/**
	 * Set cURL option(s)
	 * 
	 * @see http://be2.php.net/manual/en/function.curl-setopt-array.php
	 * @access public
	 * @param array $options
	 * 		The options to be set in the cURL session
	 * @return void
	 */
	protected function _setCurlOptions(array $options) {
		// Initiate the cURL handle, if not already done
		$this->_getCurl();
		
		// Set the options (if any)
		if(count($options) > 0) {
			curl_setopt_array($this->_curlHandle, $options);
		}
	}
	
	/**
	 * Execute session (and fetch result)
	 * 
	 * @access public
	 * @return void
	 */
	protected function _getCurlExec() {
		// Initiate the cURL handle, if not already done
		$this->_getCurl();
		
		// Execute the session, and return the result
		return curl_exec($this->_curlHandle);
	}

	/**
	 * Get (cache'd) response
	 *
	 * Will return the (raw) response of the current cURL resource, if not fetched
	 * before in the object.
	 *
	 * @uses M_UriSession::_getCurlExec()
	 * @access protected
	 * @param bool $refresh
	 *		Set to TRUE if you want to refresh the response, FALSE if you want to
	 *		allow a cache'd response
	 * @return string
	 */
	protected function _getCurlExecResponse($refresh = FALSE) {
		// If not requested before:
		if($this->_response === NULL || $refresh === TRUE) {
			// Then, we get the response now:
			// echo 'FETCH RESPONSE!!';
			$this->_response = $this->_getCurlExec();
		}

		// Return the response:
		return $this->_response;
	}
}