<?php
/**
 * M_CalendarIcsSourceHelper
 * 
 * This class is used by the iCalendar, {@link M_CalendarIcs}, and the calendar
 * components, in order to render the source (code). The helper functions in this
 * class are used intensively in:
 * 
 * - {@link M_CalendarIcs::toString()}
 * - {@link M_CalendarIcsAlarm::toString()}
 * - {@link M_CalendarIcsEvent::toString()}
 * - {@link M_CalendarIcsTodo::toString()}
 * - {@link M_CalendarIcsJournal::toString()}
 * 
 * @package Core
 */
class M_CalendarIcsSourceHelper {
	
	/* -- GETTERS -- */
	
	/**
	 * Get new line
	 * 
	 * Provides with the new-line character(s) that should be used in the source 
	 * code, to separate lines from each other.
	 * 
	 * @static
	 * @access public
	 * @return string
	 */
	public static function getNewLine() {
		return "\r\n";
	}
	
	/**
	 * Get begin tag
	 * 
	 * Provides with a formatted string, in order to open a given tag. This is used
	 * to open a tag that in turn contains other tags with data.
	 * 
	 * @static
	 * @see M_CalendarIcsSourceHelper::getEndTag()
	 * @access public
	 * @param string $name
	 * 		The name of the tag to be opened
	 * @return string
	 */
	public static function getBeginTag($name) {
		return 'BEGIN:' . (string) $name;
	}
	
	/**
	 * Get end tag
	 * 
	 * Provides with a formatted string, in order to end a given tag. This is used
	 * to close a tag that has been opened previously with the other helper function
	 * {@link M_CalendarIcsSourceHelper::getBeginTag()}
	 * 
	 * @static
	 * @see M_CalendarIcsSourceHelper::getBeginTag()
	 * @access public
	 * @param string $name
	 * 		The name of the tag to be opened
	 * @return string
	 */
	public static function getEndTag($name) {
		return 'END:' . (string) $name;
	}
	
	/**
	 * Get self-closing tag
	 * 
	 * Provides with a formatted string, in order to render a self-closing tag 
	 * with data. Unlike tags that
	 * 
	 * - are opened with {@link M_CalendarIcsSourceHelper::getBeginTag()},
	 * - then are closed with {@link M_CalendarIcsSourceHelper::getEndTag()}
	 * 
	 * this tag does not require you to close/end it; it is self-closing. You
	 * can compare it to self-closing tags in HTML; eg: <img ... />
	 * 
	 * @static
	 * @access public
	 * @param string $name
	 * 		The name of the self-closing tag
	 * @param string $value
	 * 		The value (contained data) of the tag
	 * @param array $attributes
	 * 		An associative array, with attributes of the tag
	 * @return string
	 */
	public static function getSelfClosingTag($name, $value, array $attributes = array()) {
		// The tag name:
		$out = (string) $name;
		
		// For each of the attributes:
		foreach($attributes as $currentName => $currentValue) {
			// Add the attribute to the tag
			$out .= ';' . (string) $currentName . '=' . $currentValue;
		}
		
		// Add the value:
		$out .= ':' . (string) $value;
		
		// Return the source:
		return $out;
	}
	
	/**
	 * Get boolean value
	 * 
	 * @static
	 * @access public
	 * @param bool $flag
	 * @return string
	 */
	public static function getBooleanValue($flag) {
		return ($flag ? 'TRUE' : FALSE);
	}
	
	/**
	 * Get string value
	 * 
	 * Provides with a formatted string, in order to use it as data in a given
	 * tag. Typically, this helper function is used to include large strings in
	 * the ICS Source code.
	 * 
	 * This method will escape special characters in the string, such as
	 * new-lines, tabs, double quotes, etc...
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string to be formatted for use in ICS Source Code
	 * @return string
	 */
	public static function getStringValue($string) {
		$temp = str_replace('\\',  '\\\\',  (string) $string);
		$temp = str_replace("\r", '',       $temp);
		$temp = str_replace("\n", '\n',     $temp);
		$temp = str_replace("\t", '\t',     $temp);
		$temp = str_replace('"',  'DQUOTE', $temp);
		$temp = str_replace(',',  '\\,',    $temp);
		$temp = str_replace(':',  '":"',    $temp);
		$temp = str_replace(';',  '\\;',    $temp);
		return $temp;
	}
	
	/**
	 * Get date string
	 * 
	 * Provides with a formatted string representation of a given date, in order
	 * to use it as data in a given tag. Typically, this helper function is used
	 * to include dates in the ICS Source Code.
	 * 
	 * IMPORTANT NOTE: based on wheter a timezone definition is provided to
	 * the element we are using the date string for, we have to use absolute
	 * time or not. Absolute time means that the outputted string will
	 * contain the capital letter Z as suffix, ignoring any timezone id. For
	 * example, if this method is used to parse the start date of a
	 * {@link M_CalendarIcsEvent}, and no timezone is specified, we will
	 * output the following string:
	 * 
	 * <code>
	 *	20110812T000000Z
	 * </code>
	 * 
	 * Resulting in the following DTSTART:
	 * 
	 * <code>
	 *  DTSTART:20110812T000000Z
	 * </code>
	 * 
	 * If a timezone id is provided however, we cannot use the Z suffix, as
	 * time is relative to the specified timezone:
	 * 
	 * <code>
	 *	DTSTART;TZID=Europe/Brussels:20110812T000000
	 * </code>
	 * 
	 * For more info, read page 35 of the official RFC2445 standard, which
	 * you may find at {@link http://www.ietf.org/rfc/rfc2445.txt}
	 * 
	 * @static 
	 * @access public
	 * @param M_Date $date
	 * 		The date to be formatted for use in ICS Source Code
	 * @param bool $absoluteTime
	 * @return string
	 */
	public static function getDateString(M_Date $date, $absoluteTime = false, $includeTime = true) {
		return date(
			($includeTime ? 'Ymd\THis' : 'Ymd') . ($absoluteTime ? '\Z' : ''), 
			$date->getTimestamp()
		);
	}
	
	/**
	 * Get duration string
	 * 
	 * This method will accept a given number. It will interpret this number as
	 * a duration in time (more specifically: as the number of seconds). It will
	 * then format a string that expresses this duration in ICS Source Code.
	 * 
	 * The following is an example of a duration that specifies an interval of 
	 * time of 1 hour and zero minutes and zero seconds
	 * 
	 * <code>DURATION:PT1H0M0S</code>
	 * 
	 * The following is an example of this property that specifies an interval 
	 * of time of 15 minutes.
	 * 
	 * <code>DURATION:PT15M</code>
	 * 
	 * @static
	 * @uses M_Date::getTimeArray()
	 * @access public
	 * @param integer $numberOfSeconds
	 * 		The number of seconds to be formatted for use in ICS Source Code
	 * @return string
	 */
	public static function getDurationString($numberOfSeconds) {
		// We have been provided a number of seconds. To express the duration
		// correctly, we have to explode the duration of time into an array of 
		// time blocks (hours, minutes, and seconds). We ask M_Date to provide
		// us with a time array:
		$array = M_Date::getTimeArray($numberOfSeconds, M_Date::SECOND);
		
		// Now, we have blocks of time, and we simply output it into the right
		// format:
		$out = 'PT';

		// If hours are included:
		if($array['hours']) {
			$out .= $array['hours'] . 'H';
		}

		// If minutes are included:
		if($array['minutes']) {
			$out .= $array['minutes'] . 'M';
		}

		// If seconds are included:
		if($array['seconds']) {
			$out .= $array['seconds'] . 'S';
		}
		
		// Return the final duration syntax
		return $out;
	}

	/**
	 * Get PRODID string
	 *
	 * Provides with a formatted string representation of a given company name,
	 * product name and language, in order to use it as data for the PRODID tag.
	 * For more information, please read {@link M_CalendarIcs::setProductId()}
	 *
	 * @static
	 * @uses M_CalendarIcsSourceHelper::getStringValue()
	 * @access public
	 * @param string $companyName
	 * @param string $productName
	 * @param string $language
	 * @return string
	 */
	public static function getProductIdString($companyName, $productName, $language) {
		return self::getStringValue('-//' . $companyName . '//NONSGML ' . $productName . '//' . strtoupper($language));
	}

	/**
	 * Get tags in ICS Source Code
	 *
	 * This method is used to parse information from ICS Source Code. This
	 * particular function provides with the lines/tags in the source code. The
	 * result is an array, which has been populated with the lines...
	 *
	 * @access public
	 * @param string $string
	 * @return array
	 */
	public static function getLinesFromString($string) {
		// Output
		$out = array();
		$tmp = array();
		$n   = 0;
		
		// First, we replace the occurrences of \r
		$string = trim($string);
		$string = str_replace("\r", '', $string);

		// Then, we explode the string by the newline character. The result is
		// a collection of lines in the original source code:
		foreach(explode("\n", $string) as $line) {
			// If the line starts with a tag name:
			if(preg_match('/^[A-Z-]+[:;]{1}.*/', $line)) {
				// If the temporary buffer is not empty
				if($n > 0) {
					// Then, add a line:
					$out[] = implode(self::getNewLine(), $tmp);
					$tmp   = array();
					$n     = 0;
				}
			}

			// Add the current line to the temporary buffer
			$tmp[] = $line;
			$n ++;
		}

		// If the temporary buffer is not empty
		if($n > 0) {
			// Then, add another line with the buffer's contents:
			$out[] = implode(self::getNewLine(), $tmp);
		}

		// Return the lines:
		return $out;
	}

	/**
	 * Get tag elements
	 *
	 * This method is used to parse information from ICS Source Code. This
	 * particular function extracts the elements of a tag:
	 *
	 * - The name of the tag
	 * - The attributes of the tag
	 * - The tag's value
	 *
	 * In effect, this method does the reverse of
	 *
	 * - {@link M_CalendarIcsSourceHelper::getBeginTag()}
	 * - {@link M_CalendarIcsSourceHelper::getEndTag()}
	 * - {@link M_CalendarIcsSourceHelper::getSelfClosingTag()}
	 *
	 * The result of this method is an array with three elements:
	 *
	 * <code>
	 *    Array(
	 *       'name'  => [string],
	 *       'value' => [string],
	 *       '@'     => Array(
	 *       )
	 *    )
	 * </code>
	 *
	 * @throws M_CalendarException
	 * @access public
	 * @param string $string
	 * @return array
	 */
	public static function getTagElements($string) {
		// The output array:
		$out = array(
			// The tag's attribute values
			'@'     => array(),
			// The tag's name
			'name'  => '',
			// The tag's string value
			'value' => ''
		);

		// First, we separate the tag (with attributes) from the value. The
		// delimiter is the : symbol. So, we look for the last occurrence of that
		// symbol in the provided string. Note however that the value part in the
		// provided string may also contain the same symbol, but then in escaped
		// form. We replace the escaped form by another word for now:
		$temp = str_replace('":"',     'DCOLON',  (string) $string);
		$temp = str_replace('MAILTO:', 'MAILTO!', $string);

		// Now, we look for the last occurence of the : symbol
		$pos = strpos($temp, ':');

		// If we could not find the position of that symbol:
		if($pos === FALSE) {
			// Then we throw an exception to inform about the error. We cannot
			// separate the tag from its value:
			throw new M_CalendarException(sprintf(
				'Cannot extract ICS Tag Elements from the string "%s"',
				$string
			));
		}

		// We separate the tag from its value. While we're at it, we revert back 
		// to the former escaped form of the : symbol
		$tag   = str_replace('MAILTO!', 'MAILTO:', str_replace('DCOLON', '":"', substr($temp, 0, $pos)));
		$value = str_replace('MAILTO!', 'MAILTO:', str_replace('DCOLON', '":"', substr($temp, $pos + 1)));
		
		// If we are still here, we know that we have both the tag name (including
		// the attributes), and the tag's value. We now strip the attributes and
		// their values from the tag name. An attribute starts with a semicolon
		// (;) and its value is preceeded by the "equals" symbol (=). So, we
		// explode the tag name by the semicolon to start with:
		$attributes = explode(';', $tag);

		// If any attribute is present in the tag, then the new array contains
		// more than one element. Therefore, in order to loop through the attributes
		// of the tag, we start at index 1:
		for($i = 1, $n = count($attributes); $i < $n; $i ++) {
			// We separate the current attribute's name from its value:
			$attribute = explode('=', $attributes[$i]);

			// Now, we should have an array with a total of 2 elements. The element
			// at index 0 should contain the attribute name, while the element at
			// index 1 should contain the attribute's value. Note that we make
			// sure we are dealing with an array of 2 elements:
			if(count($attribute) != 2) {
				// If not, we throw an exception to inform about the error:
				throw new M_CalendarException(sprintf(
					'Failed to extract ICS Tag Attribute (index %d) from string %s',
					$i,
					$string
				));
			}

			// We add the current attribute's value to the output
			$out['@'][$attribute[0]] = $attribute[1];
		}

		// We add the tag name to the output array
		$out['name'] = strtoupper($attributes[0]);

		// And also the value of the tag:
		$out['value'] = self::getValueFromString($value);

		// Return the output:
		return $out;
	}

	/**
	 * Get value from string
	 *
	 * This method is used to parse information from ICS Source Code. This
	 * particular function unescapes the provided value, and in effect does the
	 * reverse of {@link M_CalendarIcsSourceHelper::getStringValue()}.
	 *
	 * @access public
	 * @param string $string
	 * @return string
	 */
	public static function getValueFromString($string) {
		$string = str_replace('\n',     "\r\n", (string) $string);
		$string = str_replace('\t',     "\t",   $string);
		$string = str_replace('DQUOTE', '"',    $string);
		$string = str_replace('":"',    ':',    $string);
		$string = str_replace('\\,',    ',',    $string);
		$string = str_replace('\\;',    ';',    $string);
		$string = str_replace('\\\\',    '\\',  $string);
		return $string;
	}

	/**
	 * Get {@link M_Contact} from value string
	 *
	 * This method is used to parse information from ICS Source Code. This
	 * particular function constructs an instance of {@link M_Contact} with the
	 * provided value string.
	 *
	 * @access public
	 * @param string $string
	 * @return M_Contact
	 */
	public static function getContactFromString($string) {
		// Construct an instance of M_Contact
		$contact = new M_Contact();
		
		// We remove the MAILTO: prefix from the provided string:
		if(! strncmp($string, 'MAILTO:', 7)) {
			// Strip the prefix
			$string = substr($string, 7);
		}

		// If the value contains an @ sign
		if(strpos($string, '@') !== FALSE) {
			// Then, we extract the email recipient:
			list($email, $name) = M_MailHelper::getRecipientFromString($string);
				
			// Set the name of the contact:
			$contact->setName($name);

			// And also, set the email of the contact person:
			$contact->addEmail(new M_ContactEmail($email));
		}
		// If the value does not contain an @ sign
		else {
			// Then, we use the value as the name of the contact
			$contact->setName($string);
		}

		// Return the contact
		return $contact;
	}

	/**
	 * Get date from string
	 *
	 * This method is used to parse information from ICS Source Code. This
	 * particular function extracts a date, an instance of {@link M_Date},
	 * from the provided string.
	 *
	 * For more information about the expected string formats, read the docs on
	 * {@link M_CalendarIcsSourceHelper::getDurationString()}.
	 *
	 * @throws M_CalendarException
	 * @access public
	 * @param string $string
	 * @return M_Date
	 */
	public static function getDateFromString($string) {
		// We extract the elements from the date string, and while doing so, we
		// validate the format of the provided string. We prepare an array in order
		// to store the matches we find in the date string:
		$matches = array();

		// We run a regular expression, to find matches:
		if(preg_match('/^([0-9]{4})([0-9]{2})([0-9]{2})T([0-9]{2})([0-9]{2})([0-9]{2})Z?$/', (string) $string, $matches)) {
			// If we have found all matches, then the provided string is correct.
			// We construct a new M_Date object, and return it as the result of
			// this method:
			return new M_Date(array(
				'year'   => $matches[1],
				'month'  => $matches[2],
				'day'    => $matches[3],
				'hour'   => $matches[4],
				'minute' => $matches[5],
				'second' => $matches[6]
			));
		}
		// If no matches have been found, then we try a simpler expression
		elseif(preg_match('/^([0-9]{4})([0-9]{2})([0-9]{2})$/', (string) $string, $matches)) {
			// If we have found all matches, then the provided string is correct.
			// We construct a new M_Date object, and return it as the result of
			// this method:
			return new M_Date(array(
				'year'   => $matches[1],
				'month'  => $matches[2],
				'day'    => $matches[3],
				'hour'   => 0,
				'minute' => 0,
				'second' => 0
			));
		}

		// If we are still here, it means that the provided string could not be
		// converted to an object of M_Date. In that case, we throw an exception
		// to inform about the error
		throw new M_CalendarException(sprintf(
			'Cannot construct M_Date object from the provided date string: "%s". ' .
			'Expecting string in format %s',
			$string,
			self::getDateString(new M_Date())
		));
	}

	/**
	 * Get duration from string
	 *
	 * This method is used to parse information from ICS Source Code. This
	 * particular function extracts a duration, expressed in a number of seconds,
	 * from a provided string.
	 *
	 * For more information about the expected string formats, read the docs on
	 * {@link M_CalendarIcsSourceHelper::getDurationString()}.
	 *
	 * @throws M_CalendarException
	 * @access public
	 * @param string $string
	 * @return integer
	 */
	public static function getDurationFromString($string) {
		// The expression of time duration should start with "PT"
		if(strncmp($string, 'PT', 2) != 0) {
			// If that's not the case, then we throw an exception to inform about
			// the error:
			throw new M_CalendarException(sprintf(
				'Cannot extract time duration from string "%s". Expecting the ' .
				'string to start with "PT".',
				$string
			));
		}

		// Trim the PT prefix from the string:
		$duration = substr($string, 2);

		// Output (number of seconds)
		$out .= 0;

		// Working variable to extract numbers from the duration string:
		$tmp  = '0';

		// For each of the characters in the string:
		for($i = 0, $n = strlen($duration); $i < $n; $i ++) {
			// If the current character is numeric:
			if(is_numeric($duration[$i])) {
				// then, we add it to the temporary string:
				$tmp .= $duration[$i];
			}
			// If the current character is not numeric:
			else {
				// Then, we interpret the number that we have been storing in the
				// temporary string. First of all, we cast the string to a number:
				$number = (int) $tmp;

				// And, we reset the temporary string again:
				$tmp = '0';

				// The current character will tell us how to interpret the number.
				switch(strtoupper($duration[$i])) {
					// If the character is H, then the number is a numnber of
					// hours
					case 'H':
						// In that case, we add the number of seconds in an hour
						// to the output variable
						$out += $number * M_Date::SECONDS_IN_HOUR;
						break;

					// If the character is M, then the number is a numnber of
					// minutes
					case 'M':
						// In that case, we add the number of seconds in a minute
						// to the output variable
						$out += $number * M_Date::SECONDS_IN_MINUTE;
						break;

					// If the character is S, then the number is a numnber of
					// seconds
					case 'S':
						// In that case, we add the number of seconds in a minute
						// to the output variable
						$out += $number;
						break;
				}
			}
		}

		// Return the final duration
		return $out;
	}

	/**
	 * Get PRODID from string
	 *
	 * This method is used to parse information from ICS Source Code. This
	 * particular function extracts a PRODID value from a provided string.
	 *
	 * For more information about the expected string formats, read the docs on
	 * {@link M_CalendarIcsSourceHelper::getProductIdString()}.
	 *
	 * The result of this method is an array with three elements:
	 *
	 * <code>
	 *    Array(
	 *       'company'  => [string],
	 *       'product'  => [string],
	 *       'language' => [string]
	 *    )
	 * </code>
	 *
	 * @throws M_CalendarException
	 * @access public
	 * @param string $string
	 * @return array
	 */
	public static function getProductIdFromString($string) {
		// The Product ID of the iCalendar Object is to be provided in a certain
		// format. We will run a regular expression on the provided string, in
		// order to validate the format, and to extract the parts from that format.
		// First, we prepare an array in which to store matches:
		$match = array();
		
		// We run the expression against the provided string:
		if(preg_match('`^-//(.*)//(.*)//([a-z]{2,})$`im', $string, $match)) {
			// We have got a match! Before we continue and return the Product ID
			// as a result, we clean up the Product name (may be preceeded by
			// NONSGML)
			if(! strncmp($match[2], 'NONSGML ', 8)) {
				// Strip the NONSGML prefix from the Product ID
				$match[2] = substr($match[2], 8);
			}

			// Now, we return the extracted information:
			$output = array(
				'company'  => $match[1],
				'product'  => $match[2],
				'language' => isset($match[3]) ? $match[3] : NULL
			);
		}
		// If the value did not match the regular expression
		else {
			return array(
				'company' => $string,
				'product' => null,
				'language' => null
			);
		}
	}
}