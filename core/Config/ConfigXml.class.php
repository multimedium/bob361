<?php
/**
 * M_ConfigXml class
 * 
 * This is a specific implementation of the {@link MI_Config} 
 * interface, that loads the data structure from an XML file.
 * 
 * IMPORTANT NOTE:
 * A M_Config object is a simple structure of keys and values. 
 * Note that, if the tags in the original XML file come with 
 * attributes, they will not be parsed into the M_Config object. 
 * Once parsed into a M_Config and saved by the save() method, 
 * you will have lost all attributes!!
 * 
 * @package Core
 */
class M_ConfigXml extends M_Config {
	/**
	 * The source location
	 * 
	 * This property holds the full path to the original XML
	 * file, that has been parsed into the M_Config object.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_source;
	
	/**
	 * Constructor
	 * 
	 * IMPORTANT NOTE:
	 * The XML source file should be provided as an ABSOLUTE path,
	 * since the {@link M_ConfigXml::save()} method may be called 
	 * from another location. For example, {@link M::__destruct()}
	 * will call {@link M_ConfigXml::save()} from the core/ folder,
	 * because it is destructing a singleton object.
	 * 
	 * @access public
	 * @param string $source
	 * 		The path to the XML file, that should be loaded and
	 * 		parsed into the M_Config object.
	 * @param boolean $lock
	 * 		Set to TRUE to lock the M_Config, FALSE to unlock. 
	 * 		Read the documentation on {@link M_Config::lock()} 
	 * 		for more info.
	 * @return M_ConfigXml
	 */
	public function __construct($source = NULL, $lock = FALSE) {
		// If a source has been provided:
		if($source) {
			// Check if the source file exists:
			if(!is_file($source)) {
				throw new M_ConfigException(sprintf(
					'Unable to locate XML config file "%s"', 
					$source
				));
			}
			
			// Try to mount the config object:
			try {
				// Add the root node to the Config object
				$this->_index = 0;
				$this->addSimpleXMLElement(simplexml_load_file($source));
			}
			catch(Exception $e) {
				throw new M_ConfigException(sprintf(
					'Cannot construct instance of %s from %s. Failed with error: %s',
					__CLASS__,
					$source,
					$e->getMessage()
				));
			}
			
			// Set the source
			$this->_source = $source;
		}
		
		// Locking the object is the last thing we do. We don't want
		// Exceptions to be thrown when writing to the config object
		// while constructing.
		$this->_locked = $lock;
	}
	
	/**
	 * Save the config
	 * 
	 * M_ConfigXml overrides the default implementation of the
	 * {@link M_Config} base class, for the save() method. In the
	 * case of a M_ConfigXml object, this method will save the config
	 * data structure back to the original XML file.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function save() {
		// If a reference to source file does not exist:
		if(! $this->_source) {
			throw new M_ConfigException('M_ConfigXml cannot save() changes; missing source file!');
		}
		
		// Try to open the file, and write to it:
		$fp = @fopen($this->_source, 'w');
		if($fp) {
			if(@fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>' . $this->_toXML($this))) {
				@fclose($fp);
				return TRUE;
			}
		}
		
		// If we could not write to the file, we throw an exception:
		throw new M_ConfigException(sprintf(
			'Cannot save M_ConfigXml to %s; Permissions denied!',
			$this->_source
		));
	}
	
	/**
	 * Get source
	 * 
	 * Will return the path to the XML source file.
	 * 
	 * @access public
	 * @return string
	 */
	public function getPath() {
		return $this->_source;
	}
	
	/**
	 * Add {@link SimpleXMLElement} to the Config
	 * 
	 * This method is also used internally by 
	 * 
	 * - {@link M_ConfigXml::__construct()},
	 * - {@link M_ConfigXmlString::__construct()}
	 * 
	 * to extract the data structure from the original XML file/string.
	 * 
	 * @access protected
	 * @param SimpleXMLElement $node
	 * 		The XML node
	 * @return array
	 */
	public function addSimpleXMLElement(SimpleXMLElement $elm) {
		// Get the name of the provided element:
		$name = (string) $elm->getName();
		
		// Get the provided element's children:
		$children = $elm->children();
		
		// If the element has at least 1 child:
		if(count($children) > 0) {
			// We create a new Config object, to add the children:
			$this->_data[$name] = new M_ConfigXml();
			
			// For each of the element's children:
			foreach($children as $child) {
				// We add the child to the new Config object:
				$this->_data[$name]->addSimpleXMLElement($child);
			}
		}
		// If the element does not have children:
		else {
			$this->_data[$name] = (string) $elm;
		}
		
		// We check if the element provides with a title:
		if(isset($elm['title'])) {
			$this->setTitleOf($name, (string) $elm['title']);
		}
		
		// We check if the element provides with a description:
		if(isset($elm['description'])) {
			$this->setDescriptionOf($name, (string) $elm['description']);
		}
		
		// We update the count of data:
		$this->_count = count($this->_data);
	}
	
	/* -- PRIVATE/PROTECTED - */
	
	/**
	 * Parse a config object into an XML Node
	 * 
	 * Parse a M_Config object into a string representation of an
	 * XML Node. This method is used internally by {@link M_Config::save()},
	 * to save the config back to the original XML file format.
	 * 
	 * @access private
	 * @param MI_Config $config
	 * 		The M_Config object to be parsed into an XML node
	 * @param integer $level
	 * 		This parameter indicates the level of the M_Config object.
	 * 		This is used to generate the tabs in the XML output.
	 * @return string
	 */
	private function _toXML(MI_Config $config, $level = 0) {
		// The return value (empty for now)
		$output  = '';
		
		// Prepare the new-line character(s)
		$newline = "\n" . str_repeat("\t", $level);
		
		// For each of the values in the config object:
		foreach($config as $name => $value) {
			// Add the opening node:
			$output .= $newline . '<'. $name;
			
			// We add the attributes of the current value
			// (title + description)
			$m = $config->getTitleOf($name);
			if($m) {
				$output .= ' title="'. $m .'"';
			}
			
			$m = $config->getDescriptionOf($name);
			if($m) {
				$output .= ' description="'. $m .'"';
			}
			
			// We finish the opening node:
			$output .= '>';
			
			// If the value is in turn another config object:
			if(M_Helper::isInstanceOf($value, 'MI_Config')) {
				// We do the same thing for all values in the config (recursively):
				$output .= $this->_toXML($value, $level + 1);
				
				// Then, we close the tag (with a new-line):
				$output .= $newline . '</'. $name .'>';
			}
			// If the value is not another config object:
			else {
				// Then, we get the string value:
				$temp = (string) $value;
				
				// We check if the string value has special characters:
				if(M_Helper::containsCharactersForXmlCdata($temp)) {
					// If so, we wrap the string by a CDATA block:
					$temp = '<![CDATA[' . $temp . ']]>';
				}
				
				// We add the string to the output:
				$output .= $temp;
				
				// Then, we close the tag (without a new-line)
				$output .= '</'. $name .'>';
			}
		}
		
		// Return the XML Source Code:
		return $output;
	}
}
?>