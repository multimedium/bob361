<?php
/**
 * M_Cookie class
 * 
 * M_Cookie is used to store data to and retrieve data from a client-
 * side cookie file.
 * 
 * @package Core
 */
class M_Cookie extends M_Object {
	/**
	 * The cookie name
	 * 
	 * @access private
	 * @var string
	 */
	private $_name;
	
	/**
	 * The stored data
	 * 
	 * This property holds the data that is being stored in the cookie.
	 * If the cookie is being encrypted with a crypto key, this 
	 * property will hold the decrypted data.
	 * 
	 * @access private
	 * @var NULL|mixed
	 */
	private $_data = NULL;
	
	/**
	 * The crypto key
	 * 
	 * This property holds the private key for encrypting and 
	 * decrypting the data that is being stored in the cookie.
	 * 
	 * @access private
	 * @var string
	 */
	private $_cryptoKey;
	
	/**
	 * The cookie path
	 * 
	 * This property holds the path on the server in which the cookie 
	 * will be available on. Note that this property, when M_Cookie
	 * is constructed, is defaulted to the base href of the app. For 
	 * more info, read {@link M_Server::getBaseHref()}.
	 * 
	 * @access private
	 * @var string
	 */
	private $_path = '/';
	
	
	/**
	 * The cookie domain
	 * 
	 * The domain that the cookie is available to. Setting the domain to 
	 * 'www.example.com' will make the cookie available in the www subdomain 
	 * and higher subdomains. Cookies available to a lower domain, such as 
	 * 'example.com' will be available to higher subdomains, such as 
	 * 'www.example.com'. 
	 * 
	 * @var type 
	 */
	private $_domain = null;
	
	/**
	 * Constructor
	 * 
	 * NOTE:
	 * The constructor will set the default value of the following
	 * properties:
	 * 
	 * - {@link M_Cookie::$_path}
	 * 
	 * @access public
	 * @uses M_Cookie::setPath()
	 * @param string $name
	 * 		The name of the cookie
	 * @return M_Cookie
	 */
	public function __construct($name) {
		$this->_name = $name;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set cookie path
	 * 
	 * This method will set the path on the server in which the cookie
	 * will be available. When M_Cookie is constructed, this method is
	 * called, passing in the base href of the application.
	 * 
	 * @access public
	 * @param string $path
	 * 		The path in which the cookie should be available
	 * @return void
	 */
	public function setPath($path) {
		$this->_path = rtrim($path, " \t\n\r\0\x0B/\\");
	}
	
	/**
	 * Set cookie domain
	 * 
	 * The domain that the cookie is available to. Setting the domain to 
	 * 'www.example.com' will make the cookie available in the www subdomain 
	 * and higher subdomains. Cookies available to a lower domain, such as 
	 * 'example.com' will be available to higher subdomains, such as 
	 * 'www.example.com'.
	 * 
	 * @access public
	 * @param string $domain
	 * 		The domain in which the cookie should be available
	 * @return void
	 */
	public function setDomain($domain) {
		$this->_path = $domain;
	}
	
	/**
	 * Set crypto key
	 * 
	 * This method will set the private key with which the data is
	 * encrypted and decrypted. To encrypt and decrypt data, M_Cookie
	 * uses {@link M_Crypto}. M_Cookie will do this encryption under
	 * the hood, when
	 * 
	 * - the cookie is read: {@link M_Cookie::read()}
	 * - the cookie is written: {@link M_Cookie::write()}
	 * 
	 * @access public
	 * @see M_Crypto
	 * @param string $key
	 * 		The crypto key
	 * @return void
	 */
	public function setCryptoKey($key) {
		$this->_cryptoKey = $key;
	}
	
	/**
	 * Save the cookie
	 * 
	 * This method will write the given data to the cookie. Note that
	 * you can also (optionally) provide the expiration date of the
	 * stored data. If this expiration date is not passed in, M_Cookie
	 * will default the expiration date to the duration of the current
	 * session.
	 * 
	 * NOTE:
	 * This method will return a boolean, to indicate the success or
	 * failure of the writing process. If this method returns FALSE,
	 * the data could not have been stored in the cookie.
	 * 
	 * @access public
	 * @see M_Cookie::setCryptoKey()
	 * @param string $data
	 * 		The data to be stored in the cookie
	 * @param M_Date $expire
	 * 		The expiration date (optional)
	 * @return boolean
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function write($data, M_Date $expire = NULL) {
		// default expiration date to duration of current session, 
		// if not provided
		$time = ($expire != NULL) ? $expire->timestamp : NULL;
		
		// Serialize the data:
		if($this->_cryptoKey) {
			$data = @serialize(M_Crypto::encrypt($data, $this->_cryptoKey));
		} else {
			$data = @serialize($data);
		}
		
		// write the cookie
		if(setcookie(
			// name
			$this->_name, 
			// data
			$data, 
			// expiration date (unix timestamp)	
			$time, 
			// path
			$this->_path,
			// domain
			$this->_domain
			)) {
			// if successfully written, also save data to internal
			// data property
			$this->_data = $data;
			
			// return success
			return TRUE;
		}
		
		// return failure
		return FALSE;
	}
	
	/**
	 * Delete the cookie
	 * 
	 * This method will delete the cookie. Note that, similar to 
	 * {@link M_Cookie::write()}, this method will return a boolean
	 * to indicate the success or failure of the writing process.
	 * If this method returns FALSE, the cookie could not have been
	 * deleted.
	 * 
	 * @access public
	 * @return boolean
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function delete() {
		if(setcookie($this->_name, '', time() - 31556926, $this->_path)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	/* -- GETTERS -- */

	/**
	 * Get cookie path
	 * 
	 * This method will return the path of the cookie, previously set
	 * with {@link M_Cookie::setPath()}. Check the documentation on 
	 * {@link M_Cookie::$_path} to learn about the default value.
	 * 
	 * @access public
	 * @return string
	 * 		The cookie path
	 */
	public function getPath() {
		return $this->_path;
	}
	
	/**
	 * Read cookie
	 * 
	 * This method will read the data that is being stored in the 
	 * cookie. Note that this method will return NULL, if the cookie
	 * could not have been found, and therefore no data is available.
	 * 
	 * @access public
	 * @see M_Cookie::setCryptoKey()
	 * @return mixed
	 * 		Returns NULL if no data could have been read, or the
	 * 		stored data.
	 */
	public function read() {
		if($this->_data == NULL) {
			if(isset($_COOKIE[$this->_name])) {
				if($this->_cryptoKey) {
					$this->_data = @unserialize(M_Crypto::decrypt($_COOKIE[$this->_name], $this->_cryptoKey));
				} else {
					$this->_data = @unserialize($_COOKIE[$this->_name]);
				}
			} else {
				$this->_data = FALSE;
			}
		}
		return ($this->_data === FALSE ? NULL : $this->_data);
	}
}
?>