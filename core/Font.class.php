<?php
/**
 * M_Font class
 * 
 * M_Font is used to describe a font. A font, being a file that needs 
 * to be installed on the server, is an object that inherits from the 
 * {@link M_File} class.
 * 
 * @package Core
 */
class M_Font extends M_File {
	/**
	 * Font size
	 * 
	 * This property stores the font size. Note that the font size is
	 * expressed in a number of pixels. For more info, read the docs on
	 * {@link M_Font::getSize()}.
	 * 
	 * @access private
	 * @var integer
	 */
	protected $_size;
	
	/**
	 * Font color
	 * 
	 * This property stores the font color. Note that the color is
	 * being represented by an object of {@link M_Color}.
	 *
	 * @access private
	 * @var M_Color
	 */
	protected $_color;
	
	/**
	 * Get font size
	 * 
	 * This method provides with the font size that is contained by
	 * the object. The font size may have been set previously with
	 * {@link M_Font::setSize()}.
	 * 
	 * NOTE:
	 * If no font size has been set previously, this method will return 
	 * 12 by default.
	 *
	 * @access public
	 * @return integer
	 */
	public function getSize() {
		if($this->_size) {
			return $this->_size;
		} else {
			return 12;
		}
	}
	
	/**
	 * Get font color
	 * 
	 * This method provides with the font color that is contained by
	 * the object. The font color may have been set previously with
	 * {@link M_Font::setColor()}.
	 * 
	 * NOTE:
	 * If no font color has been set previously, this method will return 
	 * the black color by default.
	 *
	 * @access public
	 * @return M_Color
	 */
	public function getColor() {
		if(is_null($this->_color)) {
			$this->_color = new M_Color(array(0, 0, 0));
		}
		return $this->_color;
	}
	
	/**
	 * Set font size
	 *
	 * @see M_Font::getSize()
	 * @access public
	 * @param integer $size
	 * 		The font size, expressed in a number of pixels
	 * @return void
	 */
	public function setSize($size) {
		$this->_size = (int) $size;
	}
	
	/**
	 * Set font color
	 *
	 * @see M_Font::getColor()
	 * @access public
	 * @param M_Color $color
	 * @return void
	 */
	public function setColor(M_Color $color) {
		$this->_color = $color;
	}
}
?>