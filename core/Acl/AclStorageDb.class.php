<?php
/**
 * M_AclStorageDb
 * 
 * Is a specific implementation of {@link MI_AclStorage} that will save the ACL
 * and all related objects to the database.
 * 
 * @package Core
 */
class M_AclStorageDb extends M_AclStorage {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Rules
	 * 
	 * Holds the rules that have been stored in the storage object, and is 
	 * populated by {@link M_AclStorageDb::_getRules()}
	 * 
	 * @access private
	 * @var array
	 */
	private $_rules;
	
	/**
	 * Roles
	 * 
	 * Holds the roles that have been stored in the storage object, and is 
	 * populated by {@link M_AclStorageDb::_getRoles()}
	 * 
	 * @access private
	 * @var array
	 */
	private $_roles;
	
	/**
	 * Role Database ID's
	 * 
	 * @access private
	 * @var array
	 */
	private $_roleIds;
	
	/**
	 * Resources
	 * 
	 * Holds the resources that have been stored in the storage object, and is 
	 * populated by {@link M_AclStorageDb::_getResources()}
	 * 
	 * @access private
	 * @var array
	 */
	private $_resources;
	
	/**
	 * Resource Database ID's
	 * 
	 * @access private
	 * @var array
	 */
	private $_resourceIds;
	
	/**
	 * Permissions
	 * 
	 * Holds the permissions that have been stored in the storage object, and is 
	 * populated by {@link M_AclStorageDb::_getPermissions()}
	 * 
	 * @access private
	 * @var array
	 */
	private $_permissions;
	
	/**
	 * Permission Database ID's
	 * 
	 * @access private
	 * @var array
	 */
	private $_permissionIds;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @uses M_AclStorageDb::_installDbTables()
	 * @access public
	 * @return M_AclStorageDb
	 */
	public function __construct() {
		// If the database tables do not yet exist:
		if(! $this->_getDb()->hasTable($this->_getDbTableNameAclRules())) {
			// Then, we install the tables now:
			$this->_installDbTables();
		}
		
		// Initiate roles, resources, permissions and rules:
		$this->_getRolesFromDb();
		$this->_getResourcesFromDb();
		$this->_getPermissionsFromDb();
		$this->_getRulesFromDb();
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get the ACL from the storage
	 * 
	 * @access public
	 * @return M_Acl
	 */
	public function getAcl() {
		// Construct the ACL, including the ACL Objects:
		$acl = $this->_getAclWithObjects();
		
		// For each of the rules:
		foreach($this->_rules as $rule) {
			// Add the current rule:
			$acl->addRule($rule['type'], $rule['role'], $rule['resource'], $rule['permission']);
		}
		
		// Set the storage object in the ACL object:
		$acl->setStorage($this);
		
		// Return the ACL:
		return $acl;
	}
	
	/**
	 * Get roles
	 * 
	 * Will provide with the collection of roles - {@link MI_AclRole} instances - 
	 * that have been stored in the ACL Storage Object.
	 * 
	 * @see MI_AclStorage::addRole()
	 * @access public
	 * @return M_ArrayIterator $roles
	 * 		The collection of {@link MI_AclRole} instances
	 */
	public function getRoles() {
		return new M_ArrayIterator($this->_roles);
	}

	/**
	 * Get rules
	 *
	 * Will provide with the collection of rules that have been stored in the ACL
	 * Storage Object.
	 * 
	 * @return M_ArrayIterator
	 */
	public function getRules() {
		return new M_ArrayIterator($this->_rules);
	}

	/**
	 * Get a rule by it's database id
	 * 
	 * @param int $arg
	 * @return array|false
	 */
	public function getRuleByDatabaseId($arg) {
		foreach($this->_rules AS $rule) {
			if ($rule['database_id'] == (int)$arg) return $rule;
		}

		return false;
	}

	/**
	 * Get resources
	 * 
	 * Will provide with the collection of resources - {@link MI_AclResource} 
	 * instances - that have been stored in the ACL Storage Object.
	 * 
	 * @see MI_AclStorage::addResource()
	 * @access public
	 * @return M_ArrayIterator $resources
	 * 		The collection of {@link MI_AclResource} instances
	 */
	public function getResources() {
		return new M_ArrayIterator($this->_resources);
	}
	
	/**
	 * Get permissions
	 * 
	 * Will provide with the collection of permissions - {@link MI_AclPermission} 
	 * instances - that have been stored in the ACL Storage Object.
	 * 
	 * @see MI_AclStorage::addPermission()
	 * @access public
	 * @return M_ArrayIterator $permissions
	 * 		The collection of {@link MI_AclPermission} instances
	 */
	public function getPermissions() {
		return new M_ArrayIterator($this->_permissions);
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Add a role
	 * 
	 * This method will add a role to the storage object. Typically, this will
	 * cause the storage object to save the role to whatever medium that is being
	 * used by the storage object.
	 *
	 * NOTE:
	 * {@link M_Acl} will call this method, when a role is added to the ACL. See
	 * {@link M_Acl::addRole()} for more information.
	 * 
	 * @access public
	 * @param MI_AclRole $role
	 * @return void
	 */
	public function addRole(MI_AclRole $role) {
		$this->_roles[$role->getRoleId()] = $role;

		// Then, we compose the query to save the role in the db
		$data = array(
			'acl_role_code' => $role->getRoleId(),
			'acl_role_title' => $role->getTitle(),
			'acl_role_description' => $role->getDescription()
		);
		
		$rs = $this
			->_getDb()
			->getTable($this->_getDbTableNameAclRoles())
			->insertOrUpdate($data);
	}
	
	/**
	 * Add a resource
	 * 
	 * This method will add a resource to the storage object. Typically, this will
	 * cause the storage object to save the resource to whatever medium that is 
	 * being used by the storage object.
	 *
	 * NOTE:
	 * {@link M_Acl} will call this method, when a resource is added to the ACL. 
	 * See {@link M_Acl::addResource()} for more information.
	 * 
	 * @access public
	 * @param MI_AclResource $resource
	 * @return void
	 */
	public function addResource(MI_AclResource $resource) {
		$this->_resources[$resource->getResourceId()] = $resource;

		// Then, we compose the query to save the resource in the db
		$data = array(
			'acl_resource_code' => $resource->getResourceId(),
			'acl_resource_title' => (string)$resource->getTitle(),
			'acl_resource_description' => (string)$resource->getDescription()
		);

		$parent = $resource->getParentResource();
		if ($parent) {
			$data['acl_resource_parent'] = $parent->getResourceId();
		}

		$rs = $this
			->_getDb()
			->getTable($this->_getDbTableNameAclResources())
			->insertOrUpdate($data);
	}
	
	/**
	 * Add a permission
	 * 
	 * This method will add a permission to the storage object. Typically, this 
	 * will cause the storage object to save the permission to whatever medium 
	 * that is being used by the storage object.
	 *
	 * NOTE:
	 * {@link M_Acl} will call this method, when a permission is added to the ACL. 
	 * See {@link M_Acl::addPermission()} for more information.
	 * 
	 * @access public
	 * @param MI_AclPermission $permission
	 * @return void
	 */
	public function addPermission(MI_AclPermission $permission) {
		$this->_permissions[$permission->getPermissionId()] = $permission;

		// Then, we compose the query to save the resource in the db
		$data = array(
			'acl_permission_code' => $permission->getPermissionId(),
			'acl_permission_title' => (string)$permission->getTitle(),
			'acl_permission_description' => (string)$permission->getDescription(),
			'acl_permission_resource_id' => $permission->getResourceId(),
		);

		$rs = $this
			->_getDb()
			->getTable($this->_getDbTableNameAclPermissions())
			->insertOrUpdate($data);
	}
	
	/**
	 * Add rule
	 * 
	 * This method will add a rule to the storage object. Typically, this 
	 * will cause the storage object to save the rule to whatever medium 
	 * that is being used by the storage object (eg. database).
	 * 
	 * As with {@link M_Acl::addAllowRule()} and {@link M_Acl::addDenyRule()}, 
	 * you may provide a NULL value to indicate application to all roles, 
	 * resources, and/or permissions.
	 * 
	 * @access public
	 * @param integer $type
	 * 		The type of rule: {@link M_Acl::RULE_ALLOW} or {@link M_Acl::RULE_DENY}
	 * @param MI_AclRole $role
	 * 		The role(s) onto which you wish to apply the rule.
	 * @param MI_AclResource $resource
	 * 		The resource(s), if any, for which you wish to apply the rule
	 * @param MI_AclPermission $permission
	 * 		The permission(s), if any, for which you wish to apply the rule
	 * @param MI_AclAssertion $assert
	 * 		Additional conditions to be taken into account when applying the rule
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function addRule($type, MI_AclRole $role, MI_AclResource $resource = NULL, MI_AclPermission $permission = NULL) {
		$ruleId = $this->_getRuleId($type, $role, $resource, $permission);
		$this->_rules[$ruleId] = array(
			'type'        => $type,
			'role'        => $role,
			'resource'    => $resource,
			'permission'  => $permission,
			'database_id' => NULL
		);

		//compose the data which we will insert
		$data = array(
			'acl_rule_type'        => $type,
			'acl_rule_role'        => $role->getRoleId()
		);
		if (!is_null($permission)) {
			$data['acl_rule_permission'] = $permission->getPermissionId();
		}
		if (!is_null($resource)) {
			$data['acl_rule_resource'] = $resource->getResourceId();
		}
		
		//insert in database
		$this->_getDb()
				->getTable($this->_getDbTableNameAclRules())
				->insert($data);

		//update the database-id
		$this->_rules[$ruleId]['database_id'] = $this->_getDb()->getLastInsertId();
	}
	
	/**
	 * Remove all
	 * 
	 * Will completely clear the ACL Storage object, by removing all of the
	 * elements mentioned below:
	 * 
	 * - Roles
	 * - Resources
	 * - Permissions
	 * 
	 * @access public
	 * @return void
	 */
	public function removeAll() {
		$this->_roles       = array();
		$this->_resources   = array();
		$this->_permissions = array();
		$this->_rules       = array();
	}
	
	/**
	 * Remove role
	 * 
	 * Will remove a role from the ACL Storage Object. Typically, this will cause
	 * the storage object to remove the role from whatever medium (e.g. database) 
	 * that is being used by the storage object.
	 *
	 * @access public
	 * @param MI_AclRole $role
	 * 		The role to be removed from the storage object
	 * @return void
	 */
	public function removeRole(MI_AclRole $role) {
		// If existing:
		if(isset($this->_roles[$role->getRoleId()])) {

			foreach($this->_getRulesByRole($role->getRoleId()) AS $ruleId => $rule) {
				$this->_removeRuleById($ruleId, $rule['database_id']);
			}

			// Then, remove the role
			unset($this->_roles[$role->getRoleId()]);

			// Then, we compose the query to delete the role from the db
			$this
				->_getDb()
				->getTable($this->_getDbTableNameAclRoles())
				->delete('acl_role_code = ?', $role->getRoleId());
		}
	}
	
	/**
	 * Remove resource
	 * 
	 * Will remove a resource from the ACL Storage Object. Typically, this will 
	 * cause the storage object to remove the resource from whatever medium 
	 * (e.g. database) that is being used by the storage object.
	 *
	 * @access public
	 * @param MI_AclResource $resource
	 * 		The resource to be removed from the storage object
	 * @return void
	 */
	public function removeResource(MI_AclResource $resource) {
		// If existing
		if(isset($this->_resources[$resource->getResourceId()])) {
			// Then, remove the resource
			unset($this->_resources[$resource->getResourceId()]);
		}
	}
	
	/**
	 * Remove permission
	 * 
	 * Will remove a permission from the ACL Storage Object. Typically, this will 
	 * cause the storage object to remove the permission from whatever medium 
	 * (e.g. database) that is being used by the storage object.
	 *
	 * @access public
	 * @param MI_AclPermission $permission
	 * 		The permission to be removed from the storage object
	 * @return void
	 */
	public function removePermission(MI_AclPermission $permission) {
		// If existing
		if(isset($this->_permissions[$permission->getPermissionId()])) {
			// Then, remove the permission
			unset($this->_permissions[$permission->getPermissionId()]);
		}
	}
	
	/**
	 * Remove (all) rules
	 * 
	 * Will remove all rules from the ACL Storage Object. Typically, this will 
	 * cause the storage object to remove the rules from whatever medium 
	 * (e.g. database) that is being used by the storage object.
	 * 
	 * In order to remove a specific rule from the ACL Storage Object, you should
	 * use {@link MI_AclStorage::removeRule()} instead.
	 * 
	 * @access public
	 * @return void
	 */
	public function removeRules() {
		$this->_rules = array();
	}
	
	/**
	 * Remove rule
	 * 
	 * Will remove a specific rule from the ACL Storage Object. As with 
	 * {@link M_Acl::addAllowRule()} and {@link M_Acl::addDenyRule()}, 
	 * you may provide a NULL value to indicate application to all roles, 
	 * resources, and/or permissions.
	 * 
	 * @access public
	 * @param integer $type
	 * 		The type of rule: {@link M_Acl::RULE_ALLOW} or {@link M_Acl::RULE_DENY}
	 * @param M_AclRole|string $role
	 * 		The role(s) to be removed
	 * @param M_AclResource|string $resource
	 * 		The resource(s) to be removed
	 * @param M_AclPermission|string $permission
	 * 		The permission(s) to be removed
	 * @return void
	 */
	public function removeRule($type, $role, $resource = NULL, $permission = NULL) {
		// Get the Rule ID:
		$id = $this->_getRuleId($type, $role, $resource, $permission);
		
		// Remove using the id
		$this->_removeRuleById($id);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get rule id
	 * 
	 * Will create a unique ID for the role, resource and permission in a rule,
	 * which is then used to store the rule in the storage object.
	 * 
	 * @access public
	 * @param integer $type
	 *		The type of rule: {@link M_Acl::RULE_ALLOW} or {@link M_Acl::RULE_DENY}
	 * @param M_AclRole|string $role
	 * 		The role(s) to be removed
	 * @param M_AclResource|string $resource
	 * 		The resource(s) to be removed
	 * @param M_AclPermission|string $permission
	 * 		The permission(s) to be removed
	 * @return string
	 */
	protected function _getRuleId($type, $role, $resource = NULL, $permission = NULL) {
		// The output string:
		$out  = '';
		
		// Check if type is an integer (and one which is defined as constant
		// in M_ACL. If we don't do this, and e.g. a boolean is used we will
		// create invalid $ruleId's -> when we cast false to string, this will
		// be empty.
		if($type != M_Acl::RULE_ALLOW && $type != M_Acl::RULE_DENY) {
			throw new M_AclException(sprintf(
				'Unknown type "%s" when trying to remove rule',
				$type)
			);
		}

		// Add the type of the rule:
		$out .= 't:' . (string) $type;
		
		// Add the ID of the Role:
		$out .= 'r:' . $this->_getRole($role)->getRoleId();
		
		// If the resource has been provided:
		if($resource) {
			// Then, add the ID of the resource to the ID:
			$out .= '|o:' . $this->_getResource($resource)->getResourceId();
		}
		
		// If the permission has been provided:
		if($permission) {
			// Then, add the ID of the permission to the ID:
			$out .= '|p:' . $this->_getPermission($permission)->getPermissionId();
		}
		
		// Return the ID:
		return $out;
	}
	
	/**
	 * Get rules
	 * 
	 * IMPORTANT NOTE:
	 * In order to get good results, you should populate ACL roles, resources and 
	 * permissions BEFORE running this method.
	 * 
	 * @access protected
	 * @return array $rules
	 */
	protected function _getRulesFromDb() {
		// Initialize the collection of rules:
		$this->_rules = array();
		
		// Then, we compose the query to fetch the rules from the db, and we
		// execute the query for a result set:
		$rs = $this
			->_getDb()
			->getTable($this->_getDbTableNameAclRules())
			->select()
			->execute();
		
		// If the result set is valid:
		if($rs) {
			// For each of the records in the result set:
			foreach($rs as $row) {
				// Get the ID for this rule:
				$id = $this->_getRuleId($row['acl_rule_type'], $row['acl_rule_role'], $row['acl_rule_resource'], $row['acl_rule_permission']);
				
				// We add the rule to the output array:
				$this->_rules[$id] = array(
					'type'
						=> $row['acl_rule_type'],
					
					'role'
						=> (! empty($row['acl_rule_role']) && isset($this->_roles[$row['acl_rule_role']]))
							? $this->_roles[$row['acl_rule_role']] 
							: NULL,
					
					'resource'
						=> (! empty($row['acl_rule_resource']) && isset($this->_resources[$row['acl_rule_resource']]))
							? $this->_resources[$row['acl_rule_resource']] 
							: NULL,
					
					'permission'
						=> (! empty($row['acl_rule_permission']) && isset($this->_permissions[$row['acl_rule_permission']]))
							? $this->_permissions[$row['acl_rule_permission']] 
							: NULL,
					
					'database_id'
						=> $row['acl_rule_id']
				);
			}
		}
	}

	/**
	 * Get all the rules for a certain role (id)
	 * 
	 * @param string $roleId
	 * @return M_ArrayIterator
	 */
	protected function _getRulesByRole($roleId) {
		$rules = array();
		foreach($this->_rules AS $id => $rule) {
			if ($rule['role']->getRoleId() == $roleId) $rules[$id] = $rule;
		}

		return new M_ArrayIterator($rules);
	}

	/**
	 * Remove a rule by it's id
	 * 
	 * @param int $id
	 * @return bool
	 */
	protected function _removeRuleById($id) {
		// If the rule exists:
		if(isset($this->_rules[$id])) {
			$databaseId = $this->_rules[$id]['database_id'];
			
			// Then, remove the rules from the internal array
			unset($this->_rules[$id]);
			
			// And remove fro db
			return $this->_getDb()
						->getTable($this->_getDbTableNameAclRules())
						->delete('acl_rule_id = ?' , $databaseId);
		}

		return false;
	}
	
	/**
	 * Get roles from DB
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getRolesFromDb() {
		// Initialize the collection of roles:
		$this->_roles   = array();
		$this->_roleIds = array();
		
		// Then, we compose the query to fetch the roles from the db, and we
		// execute the query for a result set:
		$rs = $this
			->_getDb()
			->getTable($this->_getDbTableNameAclRoles())
			->select()
			->order('acl_role_parent', 'ASC')
			->execute();
		
		// If the result set is valid:
		if($rs) {
			// For each of the records in the result set:
			foreach($rs as $row) {
				// Construct the role:
				$role = new M_AclRole(
					// We use the code as the string ID
					$row['acl_role_code'],
					// We check if a parent exists:
					($row['acl_role_parent'] > 0 && isset($this->_roleIds[$row['acl_role_parent']]))
						? $this->_roles[$this->_roleIds[$row['acl_role_parent']]]
						: NULL
				);
				
				// Set the title of the role:
				$role->setTitle($row['acl_role_title']);
				
				// Set the description of the role:
				$role->setDescription($row['acl_role_description']);
				
				// We add the role to the output array:
				$this->_roles[$row['acl_role_code']] = $role;
				$this->_roleIds[] = $row['acl_role_code'];
			}
		}
	}
	
	/**
	 * Get role
	 * 
	 * Will provide with the requested role. Used by other methods to look up
	 * the requested role, which in many case may be a string or an instance of
	 * {@link MI_AclRole}.
	 * 
	 * @access protected
	 * @param MI_AclRole|string $role
	 * 		The requested role
	 * @return MI_AclRole
	 */
	protected function _getRole($role) {
		// If the provided argument is an instance of MI_AclRole
		if(is_object($role) && M_Helper::isInterface($role, 'MI_AclRole')) {
			// Then, we simply return the same object
			return $role;
		}
		// if the provided argument is not an object:
		else {
			// Then, it must be a string!
			if(! is_string($role)) {
				// If not, we throw an exception
				throw new M_Exception(sprintf(
					'Cannot find role with variable %s!',
					var_export($role, TRUE)
				));
			}
			// Check if the role exists:
			if(! isset($this->_roles[(string) $role])) {
				// If not, then, we throw an exception!
				throw new M_Exception(sprintf(
					'Cannot find role with Role ID %s in the storage object %s!',
					var_export($role, TRUE),
					__CLASS__
				));
			}
			
			// Return the role:
			return $this->_roles[(string) $role];
		}
	}
	
	/**
	 * Get resources
	 * 
	 * @access protected
	 * @return array $resources
	 */
	protected function _getResourcesFromDb() {
		// Initialize the collection of resources:
		$this->_resources   = array();
		$this->_resourceIds = array();
		
		// Then, we compose the query to fetch the resources from the db, and we
		// execute the query for a result set:
		$rs = $this
			->_getDb()
			->getTable($this->_getDbTableNameAclResources())
			->select()
			->order('acl_resource_parent', 'ASC')
			->execute();
		
		// If the result set is valid:
		if($rs) {
			// For each of the records in the result set:
			foreach($rs as $row) {
				// Construct the resource:
				$resource = new M_AclResource(
					// We use the code as the string ID
					$row['acl_resource_code'],
					// We check if a parent exists:
					($row['acl_resource_parent'] > 0 && isset($this->_resourceIds[$row['acl_resource_parent']]))
						? $this->_resources[$this->_resourceIds[$row['acl_resource_parent']]]
						: NULL
				);
				
				// Set the title of the resource:
				$resource->setTitle($row['acl_resource_title']);
				
				// Set the description of the resource:
				$resource->setDescription($row['acl_resource_description']);
				
				// We add the resource to the output array:
				$this->_resources[$row['acl_resource_code']] = $resource;
				$this->_resourceIds[$row['acl_resource_id']] = $row['acl_resource_code'];
			}
		}
	}
	
	/**
	 * Get resource
	 * 
	 * Will provide with the requested resource. Used by other methods to look up
	 * the requested resource, which in many case may be a string or an instance of
	 * {@link MI_AclResource}.
	 * 
	 * @access protected
	 * @param MI_AclResource|string $resource
	 * 		The requested resource
	 * @return MI_AclResource
	 */
	protected function _getResource($resource) {
		// If the provided argument is an instance of MI_AclResource
		if(is_object($resource) && M_Helper::isInterface($resource, 'MI_AclResource')) {
			// Then, we simply return the same object
			return $resource;
		}
		// if the provided argument is not an object:
		else {
			// Then, it must be a string!
			if(! is_string($resource)) {
				// If not, we throw an exception
				throw new M_Exception(sprintf(
					'Cannot find resource with variable %s!',
					var_export($resource, TRUE)
				));
			}
			
			// Check if the resource exists:
			if(! isset($this->_resources[(string) $resource])) {
				// If not, then, we throw an exception!
				throw new M_Exception(sprintf(
					'Cannot find resource with Resource ID %s in the storage object %s!',
					var_export($resource, TRUE),
					__CLASS__
				));
			}
			
			// Return the resource:
			return $this->_resources[(string) $resource];
		}
	}
	
	/**
	 * Get permissions
	 * 
	 * @access protected
	 * @return array $roles
	 */
	protected function _getPermissionsFromDb() {
		// Initialize the collection of permissions:
		$this->_permissions = array();
		
		// Then, we compose the query to fetch the permissions from the db, and 
		// we execute the query for a result set:
		$rs = $this
			->_getDb()
			->getTable($this->_getDbTableNameAclPermissions())
			->select()
			->execute();
		
		// If the result set is valid:
		if($rs) {
			// For each of the records in the result set:
			foreach($rs as $row) {
				// Construct the permission:
				$permission = new M_AclPermission(
					// We use the code as the string ID
					$row['acl_permission_code']
				);
				
				// Set the title of the permission:
				$permission->setTitle($row['acl_permission_title']);
				
				// Set the description of the permission:
				$permission->setDescription($row['acl_permission_description']);

				// Set the resource to which this permission applies
				$permission->setResourceId($row['acl_permission_resource_id']);
				
				// We add the permission to the output array:
				$this->_permissions[$row['acl_permission_code']] = $permission;
				$this->_permissionIds[$row['acl_permission_id']] = $row['acl_permission_code'];
			}
		}
	}
	
	/**
	 * Get permission
	 * 
	 * Will provide with the requested permission. Used by other methods to look 
	 * up the requested permission, which in many case may be a string or an 
	 * instance of {@link MI_AclPermission}.
	 * 
	 * @access protected
	 * @param MI_AclPermission|string $permission
	 * 		The requested permission
	 * @return MI_AclPermission
	 */
	protected function _getPermission($permission) {
		// If the provided argument is an instance of MI_AclResource
		if(is_object($permission) && M_Helper::isInterface($permission, 'MI_AclPermission')) {
			// Then, we simply return the same object
			return $permission;
		}
		// if the provided argument is not an object:
		else {
			// Then, it must be a string!
			if(! is_string($permission)) {
				// If not, we throw an exception
				throw new M_Exception(sprintf(
					'Cannot find permission with variable %s!',
					var_export($permission, TRUE)
				));
			}
			
			// Check if the permission exists:
			if(! isset($this->_permissions[(string) $permission])) {
				// If not, then, we throw an exception!
				throw new M_Exception(sprintf(
					'Cannot find permission with Permission ID %s in the storage object %s!',
					var_export($permission, TRUE),
					__CLASS__
				));
			}
			
			// Return the permission:
			return $this->_permissions[(string) $permission];
		}
	}
	
	/**
	 * Get database connection
	 * 
	 * @access protected
	 * @return M_DbDriverAdo
	 */
	protected function _getDb() {
		return M_Db::getInstance();
	}
	
	/**
	 * Get database table name: ACL Roles
	 * 
	 * @access protected
	 * @return string
	 */
	protected function _getDbTableNameAclRoles() {
		return 'acl_roles';
	}
	
	/**
	 * Get database table name: ACL Resources
	 * 
	 * @access protected
	 * @return string
	 */
	protected function _getDbTableNameAclResources() {
		return 'acl_resources';
	}
	
	/**
	 * Get database table name: ACL Permissions
	 * 
	 * @access protected
	 * @return string
	 */
	protected function _getDbTableNameAclPermissions() {
		return 'acl_permissions';
	}
	
	/**
	 * Get database table name: ACL Permissions
	 * 
	 * @access protected
	 * @return string
	 */
	protected function _getDbTableNameAclRules() {
		return 'acl_rules';
	}
	
	/**
	 * Install database tables.
	 * 
	 * When {@link M_AclStorageDb} is being used for the first time, the required
	 * database tables are installed automatically by this method.
	 * 
	 * @access protected
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	protected function _installDbTables() {
		$this->_installDbTableAclRoles();
		$this->_installDbTableAclResources();
		$this->_installDbTableAclPermissions();
		$this->_installDbTableAclRule();
		return TRUE;
	}
	
	/**
	 * Install database table: ACL Roles
	 * 
	 * When {@link M_AclStorageDb} is being used for the first time, the required
	 * database tables are installed automatically. This method is in charge of 
	 * installing the database table that stores ACL Roles.
	 * 
	 * @access protected
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	protected function _installDbTableAclRoles() {
		// Get a new table in the database where data is being stored:
		/* @var $table M_DbTableAdo */
		$table = $this
			->_getDb()
			->getNewTable();
		
		// Set the name of the new database table:
		$table->setName($this->_getDbTableNameAclRoles());
		
		// Create the fields in the new table:
		$columnCode = new M_DbColumn('acl_role_code');
		$columnCode->setType(M_DbColumn::TYPE_VARCHAR);
		$columnCode->setLength(16);
		$table->addColumn($columnCode);
		
		$columnTitle = new M_DbColumn('acl_role_title');
		$columnTitle->setType(M_DbColumn::TYPE_VARCHAR);
		$columnTitle->setLength(255);
		$table->addColumn($columnTitle);
		
		$columnDescription = new M_DbColumn('acl_role_description');
		$columnDescription->setType(M_DbColumn::TYPE_VARCHAR);
		$columnDescription->setLength(255);
		$table->addColumn($columnDescription);
		   
		$columnParent = new M_DbColumn('acl_role_parent');
		$columnParent->setType(M_DbColumn::TYPE_INTEGER);
		$table->addColumn($columnParent);
		
		// Create the PRIMARY Index:
		$primary = new M_DbIndex('primary');
		$primary->setType(M_DbIndex::PRIMARY);
		$primary->addColumn($columnCode);
		
		// Create indexes:
		$indexParent = new M_DbIndex('parent');
		$indexParent->setType(M_DbIndex::INDEX);
		$indexParent->addColumn($columnParent);
		
		// Set indexes in the table:
		$table->setPrimaryKey($primary);
		$table->addIndex($indexParent);
		$table->setComments('Defines the Roles in the ACL of the application');
		return $table->create();
	}
	
	/**
	 * Install database table: ACL Resources
	 * 
	 * When {@link M_AclStorageDb} is being used for the first time, the required
	 * database tables are installed automatically. This method is in charge of 
	 * installing the database table that stores ACL Resources.
	 * 
	 * @access protected
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	protected function _installDbTableAclResources() {
		// Get a new table in the database where data is being stored:
		/* @var $table M_DbTableAdo */
		$table = $this
			->_getDb()
			->getNewTable();
		
		// Set the name of the new database table:
		$table->setName($this->_getDbTableNameAclResources());
		
		// Create the fields in the new table:
		$columnId = new M_DbColumn('acl_resource_id');
		$columnId->setType(M_DbColumn::TYPE_INTEGER);
		$columnId->setIsAutoIncrement(TRUE);
		$table->addColumn($columnId);
		
		$columnCode = new M_DbColumn('acl_resource_code');
		$columnCode->setType(M_DbColumn::TYPE_VARCHAR);
		$columnCode->setLength(64);
		$table->addColumn($columnCode);
		
		$columnTitle = new M_DbColumn('acl_resource_title');
		$columnTitle->setType(M_DbColumn::TYPE_VARCHAR);
		$columnTitle->setLength(255);
		$table->addColumn($columnTitle);
		
		$columnDescription = new M_DbColumn('acl_resource_description');
		$columnDescription->setType(M_DbColumn::TYPE_VARCHAR);
		$columnDescription->setLength(255);
		$table->addColumn($columnDescription);
		   
		$columnParent = new M_DbColumn('acl_resource_parent');
		$columnParent->setType(M_DbColumn::TYPE_INTEGER);
		$table->addColumn($columnParent);
		
		// Create the PRIMARY Index:
		$primary = new M_DbIndex('primary');
		$primary->setType(M_DbIndex::PRIMARY);
		$primary->addColumn($columnId);
		
		// Create indexes:
		$indexParent = new M_DbIndex('parent');
		$indexParent->setType(M_DbIndex::INDEX);
		$indexParent->addColumn($columnParent);
		
		$indexCode = new M_DbIndex('code');
		$indexCode->setType(M_DbIndex::UNIQUE);
		$indexCode->addColumn($columnCode);
		
		// Set indexes in the table:
		$table->setPrimaryKey($primary);
		$table->addIndex($indexParent);
		$table->addIndex($indexCode);
		$table->setComments('Defines the Resources in the ACL of the application');
		return $table->create();
	}
	
	/**
	 * Install database table: ACL Permissions
	 * 
	 * When {@link M_AclStorageDb} is being used for the first time, the required
	 * database tables are installed automatically. This method is in charge of 
	 * installing the database table that stores ACL Permissions.
	 * 
	 * @access protected
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	protected function _installDbTableAclPermissions() {
		// Get a new table in the database where data is being stored:
		/* @var $table M_DbTableAdo */
		$table = $this
			->_getDb()
			->getNewTable();
		
		// Set the name of the new database table:
		$table->setName($this->_getDbTableNameAclPermissions());
		
		// Create the fields in the new table:
		$columnId = new M_DbColumn('acl_permission_id');
		$columnId->setType(M_DbColumn::TYPE_INTEGER);
		$columnId->setIsAutoIncrement(TRUE);
		$table->addColumn($columnId);
		
		$columnCode = new M_DbColumn('acl_permission_code');
		$columnCode->setType(M_DbColumn::TYPE_VARCHAR);
		$columnCode->setLength(64);
		$table->addColumn($columnCode);
		
		$columnTitle = new M_DbColumn('acl_permission_title');
		$columnTitle->setType(M_DbColumn::TYPE_VARCHAR);
		$columnTitle->setLength(255);
		$table->addColumn($columnTitle);
		
		$columnDescription = new M_DbColumn('acl_permission_description');
		$columnDescription->setType(M_DbColumn::TYPE_VARCHAR);
		$columnDescription->setLength(255);
		$table->addColumn($columnDescription);

		$columnResourceId = new M_DbColumn('acl_permission_resource_id');
		$columnResourceId->setType(M_DbColumn::TYPE_VARCHAR);
		$columnResourceId->setLength(64);
		$columnResourceId->setIsNull(true);
		$table->addColumn($columnResourceId);
		
		// Create the PRIMARY Index:
		$primary = new M_DbIndex('primary');
		$primary->setType(M_DbIndex::PRIMARY);
		$primary->addColumn($columnId);
		
		// Create Indexes:
		$index = new M_DbIndex('code');
		$index->setType(M_DbIndex::UNIQUE);
		$index->addColumn($columnCode);
		
		// Set indexes in the table:
		$table->setPrimaryKey($primary);
		$table->addIndex($index);
		$table->setComments('Defines the Permissions in the ACL of the application');
		return $table->create();
	}
	
	/**
	 * Install database table: ACL Rules
	 * 
	 * When {@link M_AclStorageDb} is being used for the first time, the required
	 * database tables are installed automatically. This method is in charge of 
	 * installing the database table that stores ACL Rules.
	 * 
	 * @access protected
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	protected function _installDbTableAclRule() {
		// Get a new table in the database where data is being stored:
		/* @var $table M_DbTableAdo */
		$table = $this
			->_getDb()
			->getNewTable();
		
		// Set the name of the new database table:
		$table->setName($this->_getDbTableNameAclRules());
		
		// Create the fields in the new table:
		$columnId = new M_DbColumn('acl_rule_id');
		$columnId->setType(M_DbColumn::TYPE_INTEGER);
		$columnId->setIsAutoIncrement(TRUE);
		$table->addColumn($columnId);
		
		$columnType = new M_DbColumn('acl_rule_type');
		$columnType->setType(M_DbColumn::TYPE_INTEGER);
		$columnType->setLength(1);
		$table->addColumn($columnType);
		
		$columnRole = new M_DbColumn('acl_rule_role');
		$columnRole->setType(M_DbColumn::TYPE_VARCHAR);
		$columnRole->setLength(64);
		$table->addColumn($columnRole);
		
		$columnResource = new M_DbColumn('acl_rule_resource');
		$columnResource->setType(M_DbColumn::TYPE_VARCHAR);
		$columnResource->setLength(64);
		$columnResource->setIsNull(TRUE);
		$table->addColumn($columnResource);
		
		$columnPermission = new M_DbColumn('acl_rule_permission');
		$columnPermission->setType(M_DbColumn::TYPE_VARCHAR);
		$columnPermission->setLength(64);
		$columnPermission->setIsNull(TRUE);
		$table->addColumn($columnPermission);
		
		$columnAssert = new M_DbColumn('acl_rule_assertion');
		$columnAssert->setType(M_DbColumn::TYPE_VARCHAR);
		$columnAssert->setLength(64);
		$columnAssert->setIsNull(TRUE);
		$table->addColumn($columnAssert);
		
		// Create the PRIMARY Index:
		$primary = new M_DbIndex('primary');
		$primary->setType(M_DbIndex::PRIMARY);
		$primary->addColumn($columnId);
		
		// Set indexes in the table:
		$table->setPrimaryKey($primary);
		$table->setComments('Defines the Allow/Deny Rules in the ACL of the application');
		return $table->create();
	}
}