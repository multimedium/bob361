<?php
/**
 * MI_AclPermission interface
 * 
 * This interface describes the public API that must be implemented by all 
 * permissions that are added to the ACL.
 * 
 * @package Core
 */
interface MI_AclPermission {
	/**
	 * Get ID
	 *
	 * @access public
	 * @return string
	 */
	public function getPermissionId();
	
	/**
	 * Get title
	 * 
	 * This method will provide with the title of the instance. Typically, this
	 * title is used for display. A title can be assigned to the instance with 
	 * {@link MI_AclPermission::setTitle()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getTitle();
	
	/**
	 * Get description
	 * 
	 * This method will provide with the description of the instance. Typically, 
	 * this description is used for display. A description can be assigned to the
	 * instance with {@link MI_AclPermission::setDescription()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getDescription();
	
	/**
	 * Set title
	 * 
	 * @see MI_AclPermission::getTitle()
	 * @access public
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title);
	
	/**
	 * Set description
	 * 
	 * @see MI_AclPermission::getDescription()
	 * @access public
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description);
}