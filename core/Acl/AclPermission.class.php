<?php
/**
 * M_AclPermission
 * 
 * ... The default implementation of {@link MI_AclPermission}.
 * 
 * @package Core
 */
class M_AclPermission extends M_AclObject implements MI_AclPermission {
	
	/* -- PROPERTIES -- */
	
	/**
	 * ID
	 * 
	 * Stores the ID of the permission.
	 * 
	 * @access private
	 * @var string
	 */
	private $_id;

	/**
	 * ResourceId
	 *
	 * Stores if this permission only applies to one resource
	 * 
	 * @var string
	 */
	private $_resourceId;
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $id
	 * 		The ID of the permission
	 * @return M_AclPermission
	 */
	public function __construct($id) {
		$this->_id = (string) $id;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get ID
	 *
	 * @access public
	 * @return string
	 */
	public function getPermissionId() {
		return $this->_id;
	}

	/**
	 * Get the resourceId
	 * 
	 * @return string
	 */
	public function getResourceId() {
		return $this->_resourceId;
	}

	/**
	 * Set the resource to which this permission applies
	 *
	 * @param string $arg
	 * @return M_AclPermission
	 */
	public function setResourceId($arg) {
		$this->_resourceId = $arg;
		return $this;
	}
}