<?php
/**
 * MI_AclResource interface
 * 
 * This interface describes the public API that must be implemented by all 
 * resources that are added to the ACL.
 * 
 * @package Core
 */
interface MI_AclResource {
	/**
	 * Get ID
	 *
	 * @access public
	 * @return string
	 */
	public function getResourceId();
	
	/**
	 * Get parent
	 * 
	 * In an ACL, you may define a hierarchical structure of resources. This way,
	 * you can apply rules by inheritance to a defined set of resources, without 
	 * having to apply the rule to each of the resources separately.
	 * 
	 * This method will provide with the parent resource, if any. Will return NULL
	 * instead, if no parent resource exists for this role.
	 * 
	 * @access public
	 * @return MI_AclResource $resource
	 * 		The parent resource
	 */
	public function getParentResource();
	
	/**
	 * Get title
	 * 
	 * This method will provide with the title of the instance. Typically, this
	 * title is used for display. A title can be assigned to the instance with 
	 * {@link MI_AclResource::setTitle()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getTitle();
	
	/**
	 * Get description
	 * 
	 * This method will provide with the description of the instance. Typically, 
	 * this description is used for display. A description can be assigned to the
	 * instance with {@link MI_AclResource::setDescription()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getDescription();
	
	/**
	 * Set title
	 * 
	 * @see MI_AclResource::getTitle()
	 * @access public
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title);
	
	/**
	 * Set description
	 * 
	 * @see MI_AclResource::getDescription()
	 * @access public
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description);
}