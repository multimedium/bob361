<?php
/**
 * M_Event class
 * 
 * The M_EventDispatcher class notifies about an event being raised,
 * by constructing an M_Event object and passing that object into the
 * event's listeners, or "observers".
 * 
 * The properties of the M_Event class carry basic information about 
 * an event, such as the event's type. For many events, this basic 
 * information is sufficient. Other events, however, may require 
 * more detailed information. You can pass such additional information 
 * to event listeners by extending the M_Event class.
 * 
 * Furthermore, by extending the M_Event class, you can add default 
 * behavior to events. In such a case, the M_Event object should provide 
 * the public API in order to cancel default behavior?
 * 
 * @package Core
 */
class M_Event {
	/**
	 * The event type
	 *
	 * This property defines the type of event we're dealing with. 
	 * Typically, the value of this property is defined by a constant
	 * of an M_Event class.
	 * 
	 * @var string
	 */
	private $type = NULL;
	
	/**
	 * The event target
	 * 
	 * The event target is the object that triggered the event.
	 *
	 * @var mixed
	 */
	private $target = NULL;
	
	/**
	 * Constructor
	 *
	 * @param string $type
	 * 		The event type
	 * @param object $target
	 * 		The event target; the object that triggered the event.
	 * @return M_Event
	 */
	public function __construct($type, $target) {
		$this->type = $type;
		$this->target = $target;
	}
	
	/**
	 * Get event type
	 * 
	 * This method is a getter for the event type.
	 *
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * Get event target
	 * 
	 * This method is a getter for the event target.
	 *
	 * @return object
	 */
	public function getTarget() {
		return $this->target;
	}
}
?>