<?php
/**
 * MI_EventDispatcher interface
 * 
 * Classes that allow event listeners to be attached to it, 
 * implement the MI_EventDispatcher interface. Typically, event
 * dispatching classes extend the abstract {@link M_EventDispatcher}
 * class.
 * 
 * @package Core
 */
interface MI_EventDispatcher {
	public function addEventListener($type, $handler);
	public function dispatchEvent(M_Event $event);
	// TODO: removeEventListener
	public function removeEventListener();
	public function hasEventListeners();
	public function copyEventListeners(MI_EventDispatcher $dispatcher);
}