<?php
/**
 * M_ArrayException class
 * 
 * Exceptions thrown by {@link M_Array}
 * 
 * @package Core
 * @subpackage Array
 */
class M_ArrayException {
}