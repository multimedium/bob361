<?php
/**
 * MI_ImageResource interface
 * 
 * An image resource is an object that represents an image, on which
 * graphical operations can be applied.
 * 
 * Since there are various image libraries available in PHP, there may
 * exist multiple implementations of such an image resource object. We
 * define an interface of the image resource object, so we can program
 * against the interface instead of against a specific implementation.
 * 
 * @package Core
 */
interface MI_ImageResource {
	/**
	 * Get width
	 * 
	 * @access public
	 * @see MI_ImageResource::getHeight()
	 * @return integer
	 */
	public function getWidth();
	
	/**
	 * Get height
	 * 
	 * @access public
	 * @see MI_ImageResource::getWidth()
	 * @return integer
	 */
	public function getHeight();
	
	/**
	 * Get pixel
	 * 
	 * This method can be used to get a specific pixel from the image 
	 * resource. Note that you specify the pixel by providing its X-
	 * and Y-coordinates.
	 *
	 * @access public
	 * @param integer $x
	 * 		The X-Coordinate
	 * @param integer $y
	 * 		The Y-Coordinate
	 * @return M_ImagePixel
	 */
	public function getPixel($x, $y);
	
	/**
	 * Get pixels
	 * 
	 * This method will provide with the complete collection of pixels
	 * that is contained by the image resource. The return value of 
	 * this method is an {@link M_ImageResourcePixelIterator}.
	 * 
	 * @access public
	 * @see MI_ImageResource::getPixel()
	 * @return M_ImageResourcePixelIterator
	 */
	public function getPixelIterator();
	
	/**
	 * Set pixel
	 * 
	 * This method can be used to change a given pixel in the image.
	 * Note that you do not need to provide the X- and Y-coordinates
	 * of the pixel, because that is already contained by the pixel
	 * object itself.
	 *
	 * @access public
	 * @param MI_ImagePixel $pixel
	 * 		The pixel to be changed in the image
	 * @return void
	 */
	public function setPixel(MI_ImagePixel $pixel);
	
	/**
	 * Resize the image
	 * 
	 * This method can be used to resize the image. You only need to
	 * provide the resource with a new width and height. Note that,
	 * in many cases, you will want to use {@link M_ImageRatio} to 
	 * calculate the image's new size.
	 * 
	 * NOTE:
	 * The return value is TRUE on success, FALSE on failure.
	 * 
	 * @access public
	 * @param integer $width
	 * 		The image's new width
	 * @param integer $height
	 * 		The image's new height
	 * @return boolean
	 */
	public function resize($width, $height);
	
	/**
	 * Crop a part of image
	 * 
	 * This method can be used to crop a part out of the image. You 
	 * need to provide the X- and Y-coordinates of the crop rectangle's
	 * top left corner, and the size of the crop rectangle.
	 * 
	 * NOTE:
	 * The return value is TRUE on success, FALSE on failure.
	 *
	 * @access public
	 * @param integer $x
	 * 		The X-Coordinate of the top left corner
	 * @param integer $y
	 * 		The Y-Coordinate of the top left corner
	 * @param integer $width
	 * 		The width of the crop rectangle
	 * @param integer $height
	 * 		The height of the crop rectangle
	 * @return boolean
	 */
	public function crop($x, $y, $width, $height);
	
	/**
	 * Save to file
	 * 
	 * This method can be used to save the image resource to an actual
	 * image file. The return value of this method is an instance of
	 * {@link M_File} that represents the new image file.
	 *
	 * @access public
	 * @param string $filename
	 * 		The filename to which to save the image. Note that this is
	 * 		the filename (without extension), and not the basename (with
	 * 		extension)
	 * @param string $format
	 * 		The format to which the image should be exported. Possible
	 * 		values are: jpg, gif, png
	 * @param int $quality
	 * 		The quality of the new image file. This is an integer, describing the
	 * 		quality, and can range from 0 (worst) to 100 (best). The lower this
	 * 		number, the higher the compression and the lower the quality.
	 * @return M_File
	 */
	public function saveToFile($filename, $format, $quality = 85);
	
	/**
	 * Insert image
	 * 
	 * This method can be used to insert/paste an image into the 
	 * existing image resource. Note that you can specify the position
	 * at which the image should be pasted, by passing in the top left
	 * X- and Y-coordinates. Also, you can specify the size of the 
	 * inserted image, by passing in two additional arguments: the width
	 * and the height.
	 * 
	 * NOTE:
	 * The return value is TRUE on success, FALSE on failure.
	 *
	 * @access public
	 * @param M_Image $image
	 * 		The image to be inserted
	 * @param integer $x
	 * 		The X-Coordinate of the top left corner (optional)
	 * @param integer $y
	 * 		The Y-Coordinate of the top left corner (optional)
	 * @param integer $width
	 * 		The width of the inserted image (optional)
	 * @param integer $height
	 * 		The height of the inserted image (optional)
	 * @return boolean
	 */
	public function insertImage(M_Image $image, $x = 0, $y = 0, $width = NULL, $height = NULL);
	
	/**
	 * Insert text
	 * 
	 * This method can be used to insert text into the image resource.
	 * 
	 * NOTE:
	 * The return value is TRUE on success, FALSE on failure.
	 *
	 * @access public
	 * @param M_Font $font
	 * 		The font in which to write the text
	 * @param integer $x
	 * 		The X-Coordinate of the top left corner of the text's 
	 * 		bounding box
	 * @param integer $y
	 * 		The Y-Coordinate of the top left corner of the text's 
	 * 		bounding box
	 * @param string $text
	 * 		The text to be written
	 * @return boolean
	 */
	public function insertText(M_Font $font, $x, $y, $text);
	
	/**
	 * Draw filled rectangle
	 * 
	 * @access public
	 * @param M_Color $color
	 * 		The color with which to fill the rectangle
	 * @param integer $x
	 * 		The X-Coordinate of the rectangle's top left corner
	 * @param integer $y
	 * 		The Y-Coordinate of the rectangle's top left corner
	 * @param integer $width
	 * 		The rectangle's width
	 * @param integer $height
	 * 		The rectangle's height
	 * @return boolean
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function drawFill(M_Color $color, $x = 0, $y = 0, $width = NULL, $height = NULL);
}
?>