<?php
/**
 * M_ImageResourceAbstract class
 * 
 * Typically, a specific image resource class is implemented by subclassing
 * the abstract M_ImageResourceAbstract. This abstract class provides
 * with some template implementations for the {@link MI_ImageResource}
 * interface.
 * 
 * @abstract
 * @package Core
 */
abstract class M_ImageResourceAbstract implements MI_ImageResource {
	/**
	 * Get pixels
	 * 
	 * This method will provide with the complete collection of pixels
	 * that is contained by the image resource. The return value of 
	 * this method is an {@link M_ImageResourcePixelIterator}.
	 * 
	 * @access public
	 * @see MI_ImageResource::getPixel()
	 * @return M_ImageResourcePixelIterator
	 */
	public function getPixelIterator() {
		return new M_ImageResourcePixelIterator($this);
	}
}
?>