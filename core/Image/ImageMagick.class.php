<?php
/**
 * M_ImageMagick class
 *
 *
 * @package Core
 */

/**
 * Import {@link PHPMailer} package
 */
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . FOLDER_THIRDPARTY . DIRECTORY_SEPARATOR . 'phmagick/phmagick.php';

/**
 * M_ImageMagick class
 *
 *
 * @package Core
 */
class M_ImageMagick {

	/**
	 * phmagick class
	 * @var phmagick
	 */
	private $_phmagick;

	/**
	 * Source file
	 * @var M_File
	 */
	private $_sourceFile;

	/**
	 * Destination file
	 * @var M_File
	 */
	private $_destionationFile;
	
	/**
	 * Constructor
	 * 
	 * @param M_File $sourceFile
	 * @param M_File $destinationFile
	 */
	public function __construct(M_File $sourceFile = null, M_File $destinationFile = null) {

		// Set source and destionation file
		$this->_sourceFile = $sourceFile;
		$this->_destionationFile = $destinationFile;

		// Construct a new phmagick class
		$sourceFilePath = $this->getSourceFile() ? $this->getSourceFile()->getPath() : '';
		$destinationFilePath = $this->getDestinationFile() ? $this->getDestinationFile()->getPath() : '';
		$this->_phmagick = new phmagick($sourceFilePath, $destinationFilePath);
		$this->_phmagick->setImageMagickPath('/opt/local/bin');
		$this->_phmagick->setImageQuality(100);
	}

	/**
	 * Get source file
	 * @return M_File
	 */
	public function getSourceFile() {
		return $this->_sourceFile;
	}

	/**
	 * Get destination file
	 * @return M_File
	 */
	public function getDestinationFile() {
		return $this->_destionationFile;
	}

	/**
	 * Convert
	 */
	public function convert() {
		return $this->_getPhMagick()->convert();
	}

	/**
	 * Convert & Resize
	 * 
	 * @param int $density
	 * @param int $width
	 * @param int $height
	 * @param bool $ignoreAspectRatio
	 * @return phmagick
	 */
	public function convertAndResize($density = 200, $width = 640, $height = 480, $ignoreAspectRatio = FALSE) {
		$phMagick = $this->_getPhMagick();
		$cmd = $phMagick->getBinary('convert');
        $cmd .= ' -density ' . $density;
        $cmd .= ' "' . $phMagick->getSource() .'"';
		$cmd .= ' -resize ' . $width . 'x' . $height;
		if($ignoreAspectRatio) {
			$cmd .= '\!';
		}
		$cmd .= ' "' . $phMagick->getDestination() . '"';

        $phMagick->execute($cmd);
        $phMagick->setSource($phMagick->getDestination());
        $phMagick->setHistory($phMagick->getDestination());
        return  $phMagick;
	}

	/**
	 * Get the internal {@link phmagick} object
	 *
	 * @see _phmagick
	 * @return phmagick
	 *
	 * Note: actually we return a phmagick file, but it extends from {@link phmagick}.
	 * We use phmagick in PHPDoc, since most functuonality derives from this class.
	 * Because phmagick uses a mapper, a IDE cannot retrieve the methods from the base
	 * class: this isn't very usefull during programming.
	 */
	protected function _getPhMagick() {
		return $this->_phmagick;
	}
}