<?php
/**
 * M_ImageResourceGdFromFile class
 * 
 * This class can be used to create a new image resource out of an
 * image stream in a given string. Note that this class, being a subclass 
 * of {@link M_ImageResourceGd}, also is an implementation based on the
 * GD Library.
 * 
 * @package Core
 */
class M_ImageResourceGdFromStream extends M_ImageResourceGd {
	/**
	 * Constructor
	 *
	 * @access public
	 * @param string $stream
	 * 		The string out of which to create a new image resource
	 * @return M_ImageResourceGdFromStream
	 */
	public function __construct($stream) {
		$this->_resource = imagecreatefromstring($stream);
		$this->_width = imagesx($this->_resource);
		$this->_height = imagesy($this->_resource);
	}
}
?>