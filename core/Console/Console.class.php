<?php
/**
 * M_Console class
 * 
 * M_Console is used to log significant system events. Also, M_Console
 * can be used as error handler. In that case, M_Console will also log
 * error messages, and disable PHP's internal error handler.
 * 
 * @package Core
 */
class M_Console {
	/**
	 * New-Line Character(s)
	 * 
	 * The value of this constant is used by {@link M_Console} to 
	 * write new lines in the log files.
	 * 
	 * @var string
	 */
	const NEWLINE = "\n";
	
	/**
	 * Tab Character(s)
	 * 
	 * The value of this constant is used by {@link M_Console} to 
	 * write tabs in the log files.
	 * 
	 * @var string
	 */
	const TAB = "\t";
	
	/**
	 * Singleton instances
	 * 
	 * This property stores the singleton instances that are created
	 * by {@link M_Console::getInstance()}
	 * 
	 * @static
	 * @access private
	 * @var array
	 */
	private static $_instances;
	
	/**
	 * Directory
	 * 
	 * This property stores the location (path) of the log files that
	 * are created by {@link M_Console}
	 * 
	 * @static
	 * @access private
	 * @var string
	 */
	private static $_directory = 'files/log';
	
	/**
	 * File
	 * 
	 * This property stores the file (resource; pointer) to which log 
	 * entries are being written
	 * 
	 * @access private
	 * @var string
	 */
	private $_file;
	
	/**
	 * Filename
	 * 
	 * This property stores the log file's filename.
	 * 
	 * @access private
	 * @var string
	 */
	private $_filename;
	
	/**
	 * Context
	 * 
	 * The context, with which the {@link M_Console} instance has
	 * been constructed.
	 * 
	 * @access private
	 * @var string
	 */
	private $_context;
	
	/**
	 * Timeformat
	 * 
	 * This property stores the time format with which log entries are
	 * to be written to the log file. This time format is used to
	 * format the time of the log entry.
	 * 
	 * @access private
	 * @var string
	 */
	private $_timeFormat = 'Y-m-d H:i:s';
	
	/**
	 * Constructor
	 * 
	 * Note that the constructor is private, so new instances of 
	 * {@link M_Console} can only be created by the class itself. 
	 * To obtain objects of {@link M_Console}, you should use the
	 * method {@link M_Console::getInstance()} instead.
	 * 
	 * @access private
	 * @param string $context
	 * @return M_Console
	 */
	private function __construct($context = NULL) {
		// Set the context of the M_Console instance:
		$this->_context = $context;
		
		// Compose the full path to the log file:
		$this->_filename = M_Loader::getAbsolute(
			$this->getFileOfDate(new M_Date)->getPath()
		);
		
		// Create the file pointer
		$this->_file = fopen($this->_filename, 'a');
		
		// If the file pointer has been created successfully:
		if($this->_file) {
			// Write a test message
			$this->write('Constructed M_Console');
		} else {
			throw new M_ConsoleException('Cannot open M_Console Log file');
		}
	}
	
	/**
	 * Destructor
	 * 
	 * Will close the internal file pointer, and clean up the console.
	 * 
	 * @access public
	 * @return void
	 */
	public function __destruct() {
		$this->write('Destructed M_Console' . self::NEWLINE . '--' . self::NEWLINE);
		fclose($this->_file);
	}
	
	/**
	 * Singleton constructor
	 * 
	 * This method provides with the singleton instance, in a given
	 * context. Depending on the task you want to log, you may want to
	 * give it a context, so it is logged into a separate file.
	 * 
	 * @access public
	 * @return M_Console
	 */
	public static function getInstance($context = NULL) {
		$instance = ($context == NULL ? 'null' : $context);
		if(!isset(self::$_instances[$instance])) {
			self::$_instances[$instance] = new self($context);
		}
		return self::$_instances[$instance];
	}
	
	/**
	 * Set directory
	 * 
	 * Can be used to set the location where log files are being saved.
	 * 
	 * @access public
	 * @param string $directory
	 * 		The location where log files are to be located
	 * @return void
	 */
	public static function setLogDirectory($directory) {
		self::$_directory = M_Helper::rtrimCharlist($directory, '/\\');
	}
	
	/**
	 * Get directory
	 * 
	 * Will provide with the directory where log files are located.
	 * 
	 * @access public
	 * @return string
	 */
	public function getLogDirectory() {
		return self::$_directory . DIRECTORY_SEPARATOR;
	}
	
	/**
	 * Set time format
	 * 
	 * Can be used to set the format of the time that is written along
	 * with every log entry in the file. The format is expected in the
	 * exact same way as it would be by PHP's built-in date() function.
	 * 
	 * @access public
	 * @param string $format
	 * 		The time format
	 * @return void
	 */
	public static function setTimeFormat($format) {
		$this->_timeFormat = $format;
	}
	
	/**
	 * Get time
	 * 
	 * Will provide with a formatted time. To format the time, this
	 * method will use the time format that may have been set previously
	 * with {@link M_Console::setTimeFormat()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getTime() {
		return date($this->_timeFormat);
	}
	
	/**
	 * Get log file
	 * 
	 * Will provide with an instance of {@link M_File} that represents
	 * the log file to which the instance of {@link M_Console} is
	 * writing the log entries.
	 * 
	 * @access public
	 * @return M_File
	 */
	public function getFile() {
		return new M_File($this->_filename);
	}
	
	/**
	 * Get (previous) log file
	 * 
	 * Will provide with an instance of {@link M_File} that represents
	 * the log file that was created by {@link M_Console} at a given
	 * point in time. To indicate the time, you can provide an instance
	 * of {@link M_Date}
	 * 
	 * NOTE:
	 * This method will always provide with an instance of {@link M_File},
	 * even though the requested log file does not exist!
	 * 
	 * @access public
	 * @param M_Date $date
	 * 		The date of which you want to retrieve the log file
	 * @return M_File
	 */
	public function getFileOfDate(M_Date $date) {
		// Compose the full path to the log file:
		$filename = $this->getLogDirectory();
		
		// Add context to the filename, if any
		if($this->_context) {
			$filename .= $this->_context . '-';
		}
		
		// Add date, and extension:
		$filename .= date('Y-m-d', $date->getTimestamp()) . '.log';
		
		// Construct the file:
		return new M_File($filename);
	}
	
	/**
	 * Write a log entry
	 * 
	 * Will write the provided log entry/message (string) to the log
	 * file. Note that you can also provide with a
	 * 
	 * - line-number (typically used to inform about line-number where error occured)
	 * 
	 * @access public 
	 * @param string $message
	 * 		The message to be written to the log file
	 * @param integer $lineNo
	 * 		The line number
	 * @return M_Console $console
	 * 		Returns the console, for a fluent programming interface
	 */
	public function write($message, $lineNo = NULL) {
		// Get the time, formatted as required:
		$line  = $this->getTime();
		
		// Check if the message is an array or an object. If so: we use
		// var_export to make the message readable
		if(is_array($message) || is_object($message)) {
			$message = var_export($message, true);
		}
		
		// Separate the message in the line with a tab
		$line .= self::TAB;
		$line .= $message;

		// Add line number:
		$line .= self::TAB;
		$line .= ($lineNo == NULL) ? '' : $lineNo;
		
		// Add new-line
		$line .= self::NEWLINE;
		
		// If we cannot write to the log file:
		if(!fwrite($this->_file, $line)) {
			// We throw an exception:
			throw new M_ConsoleException(sprintf(
				'Cannot write to M_Console Log file %s',
				$this->_filename
			));
		}
		
		// return myself
		return $this;
	}
	
	/**
	 * Write an object
	 * 
	 * Does exactly the same as {@link M_Console::write()}, but will
	 * write a constructed object to the log file. To do so, it will
	 * use the magic method __console, which is implemented by all Core
	 * classes, thanks to {@link M_Object::__console()}
	 * 
	 * @access public
	 * @param M_Object $object
	 * @return M_Console $console
	 * 		Returns the console, for a fluent programming interface
	 */
	public function writeObject(M_Object $object) {
		// Get the time, formatted as required:
		$line  = $this->getTime();
		
		// Separate the object in the line with a tab
		$line .= self::TAB;
		$line .= $object->__console();
		
		// Add new-line
		$line .= self::NEWLINE;
		
		// If we cannot write to the log file:
		if(! fwrite($this->_file, $line)) {
			// We throw an exception:
			throw new M_ConsoleException(sprintf(
				'Cannot write to M_Console Log file %s',
				$this->_filename
			));
		}
		
		// return myself
		return $this;
	}
	
	/**
	 * Set as error handler
	 * 
	 * You may choose to set an instance of {@link M_Console} as the
	 * error handler, so it handles the errors/notices/warnings that 
	 * are dispatched by PHP's built-in functions.
	 * 
	 * If you choose to do so, {@link M_Console} will write the error
	 * message to the log file, and throw an exception (not always;
	 * depends on the type of error).
	 * 
	 * @access public
	 * @return void
	 */
	public function setAsErrorHandler() {
		set_error_handler(array($this, 'reportError'));
	}
	
	/**
	 * Log error message
	 * 
	 * This method is called when an error occurs. At least, if you
	 * have used {@link M_Console::setAsErrorHandler()} to do so.
	 * 
	 * @access public
	 * @param integer $errno
	 * @param string $errstr
	 * @param string $errfile
	 * @param integer $errline
	 * @return void
	 */
	public function reportError($errno, $errstr, $errfile, $errline) {
		// Depending on the type of the error:
		switch ($errno) {
			// ERROR
			case E_USER_ERROR:
				$this->write("E_USER_ERROR[$errno] $errstr", "file $errfile", $errline);
				
				// In case an error occurs, we throw an exception:
				throw new M_ConsoleException(sprintf(
					'An error occurred, and has been logged by M_Console: %s',
					$errstr
				));
				break;
			
			// WARNING
			case E_USER_WARNING:
				$this->write("E_USER_WARNING[$errno] $errstr", "file $errfile", $errline);
				break;
			
			// NOTICE
			case E_USER_NOTICE:
				$this->write("E_USER_NOTICE[$errno] $errstr", "file $errfile", $errline);
				break;
			
			// OTHER
			default:
				$this->write("ERROR[$errno] $errstr", "file $errfile", $errline);
				break;
		}
		
		// Don't execute PHP internal error handler:
		// (returning TRUE will stop PHP from handling the error)
		return TRUE;
	}
}