<?php
//load the simple_html package
M_Loader::loadRelative('core/_thirdparty/css/emogrifier.php', true);

/**
 * M_CssToInlineStyles is used to convert HTML with CSS into HTML with inline styles. The
 * result can be modified, saved, ...
 *
 * In fact this M_CssToInlineStyles class is a wrapper around the {@link Emogrifier}
 * class
 *
 * NOTE: no support for pseudo selectors
 *
 * @author Wim Van De Mierop
 */
class M_HtmlCssToInlineStyles {

	/* -- PROPERTIES -- */
	
	/**
	 * For internal use only: we use the {@link Emogrifier} object
	 * @var Emogrifier
	 */
	private $_cssToInlineStyles;

	/**
	 * The html-string which we want to parse
	 *
	 * @var string
	 */
	protected $_html;

	/**
	 * The css-string which we want to parse
	 *
	 * @var string
	 */
	protected $_css;

	/* -- PUBLIC -- */

	/**
	 * Start a new CSSToInlineStyles-object (based on a html-string, optionally on a css-string)
	 *
	 * @param string $html
	 */
	public function __construct($html, $css) {
		$this->_html = $html;
		$this->_css = $css;
	}

	/**
	 * Converts the loaded HTML into an HTML-string with inline styles based on the loaded CSS
	 * 
	 * @return string $html
	 */
	public function convert() {
		return $this->_getCSSToInlineStyles()->emogrify();
	}

	/* -- PRIVATE / PROTECTED -- */

	/**
	 * Get {@link Emogrifier} object: internal use only
	 *
	 * @return Emogrifier
	 */
	protected function _getCSSToInlineStyles() {
		if (is_null($this->_cssToInlineStyles)) {
			$this->_cssToInlineStyles = new Emogrifier($this->_html, $this->_css);
		}

		return $this->_cssToInlineStyles;
	}

}