<?php
//load the simple_html package
M_Loader::loadRelative('core/_thirdparty/simple-html/simple_html_dom.php', true);

/**
 * M_HtmlDom let's the user find after html-elements based on css-selectors. The
 * result can be modified, saved, ...
 * 
 * In fact this M_HtmlDom class is a wrapper around the {@link simple_html_dom}
 * class
 * 
 * @author Ben Brughmans
 */
class M_HtmlDom {
	
	/**
	 * For internal use only: we use the {@link simple_html_dom} object
	 * @var simple_html_dom
	 */
	private $_simpleHtml;
	
	/**
	 * The html-string which we want to parse
	 *
	 * @var string
	 */
	protected $_html;
	
	/**
	 * Start a new html-dom object (optionally based on a html string)
	 *
	 * @param string $html
	 */
	public function __construct($html = null) {
		if (!is_null($html)) {
			$this->_html = $html;
		}
	}

	/**
	 * Check if empty
	 *
	 * @return bool
	 */
	public function isEmpty() {
		if(empty($this->_html)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get the string value of this element
	 * 
	 * @see M_HtmlDomNode::toString()
	 * @return string
	 */
	public function __toString() {
		return $this->_getSimpleHtml()->__toString();
	}
	
	/**
	 * Get {@link simple_html_dom} object: internal use only
	 * 
	 * @return simple_html_dom
	 */
	protected function _getSimpleHtml() {
		if (is_null($this->_simpleHtml)) {
			$this->_simpleHtml = new simple_html_dom($this->_html);
		}
		
		return $this->_simpleHtml;
	}
	
	/**
	 * Clear memory
	 */
	public function clear() {
		$this->_getSimpleHtml()->clear();
	}
	
	/**
	 * Search for html elements in the given text by using CSS-selectors. 
	 * As a result the dom parser will return a set of {@link M_HtmlDomNode} 
	 * elements.
	 * 
	 * <code>
	 * $dom = new M_HtmlDom($html);
	 * $dom->find('a[href*=fancybox],img[src*=thumbnail]');
	 * </code>
	 *
	 * @param string $selector
	 * @param int $index
	 * 		Only select the n-th element (starting with 0)
	 * @return ArrayIterator|M_HtmlDomNode|false
	 * 		Contains elements of {@link M_HtmlDomNode} or only one M_HtmlDomNode
	 * 		if index is given. False when index is set but no node was found 
	 */
	public function find($selector, $index = null) {
		$nodes = $this->_getSimpleHtml()->find($selector, $index);
		
		//get the n-th element
		if (!is_null($index) ) {
			//check if the element was found, if not: return false
			return ($nodes instanceof simple_html_dom_node) ? M_HtmlDomNode::constructWithSimpleHtmlNode($nodes) : false;
		}
		$htmlDomNodes = array();
		/* @var $node simple_html_dom_node */
		foreach($nodes AS $node) {
			$htmlDomNodes[] = M_HtmlDomNode::constructWithSimpleHtmlNode($node);
		}
		
		return new ArrayIterator($htmlDomNodes);
	}

}