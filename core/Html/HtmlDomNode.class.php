<?php
/**
 * M_HtmlDomNode is created by {@link M_HtmlDom} when search for nodes.
 * This class is simple a wrapper around {@link simple_html_dom_node}.
 * 
 * @author Ben Brughmans
 */
class M_HtmlDomNode {
	
	/**
	 * simple_html_dom_node-object
	 *
	 * This object is only used internally
	 * 
	 * @see _getSimpleHtmlDomNode()
	 * @see _setSimpleHtmlDomNode()
	 * @var simple_html_dom_node
	 */
	protected $_simpleHtmlNode;
	
	/**
	 * Construct
	 *
	 * Create a new {@link M_HtmlDomNode} by using a {@link simple_html_dom_node}
	 * object
	 * 
	 * @param simple_html_dom_node $node
	 * @return M_HtmlNode
	 */
	public static function constructWithSimpleHtmlNode( simple_html_dom_node $node) {
		$htmlNode = new self();
		$htmlNode->_setSimpleHtmlDomNode($node);
		
		return $htmlNode;
	}
	
	/**
	 * Set the simple_html_node for this object
	 * 
	 * Internally we use the {@link simple_html_node} object. That's why
	 * there's need for a method to set this object.
	 * 
	 * @param simple_html_dom_node $node
	 * @see _getSimpleHtmlDomNode()
	 */
	protected function _setSimpleHtmlDomNode(simple_html_dom_node $node) {
		$this->_simpleHtmlNode = $node;
	}
	
	/**
	 * Get the simple_html_node 
	 *
	 * @see _setSimpleHtmlDomNode()
	 * @return simple_html_dom_node
	 */
	protected function _getSimpleHtmlDomNode() {
		return $this->_simpleHtmlNode;
	}
	
	/**
	 * Get the first child
	 * 
	 * @return M_HtmlNode|false
	 */
	public function getFirstChild() {
		$child = $this->_getSimpleHtmlDomNode()->first_child();
		
		//a child was found
		if ($child) {
			return M_HtmlDomNode::constructWithSimpleHtmlNode($child);
		}
		
		return false;
	}
	
	/**
	 * Get the first child, alias for {@link getFirstChild()}
	 * 
	 * @see getFirstChild()
	 * @return M_HtmlNode|false
	 */
	public function firstChild() {
		return $this->getFirstChild();
	}
	
	/**
	 * Get the parentNode for this element
	 * 
	 * @return M_HtmlDomNode|false
	 */
	public function getParent() {
		$parent = $this->_getSimpleHtmlDomNode()->parent();
		
		//a child was found
		if ($parent) {
			return M_HtmlDomNode::constructWithSimpleHtmlNode($parent);
		}
		
		return false;
	}
	
	/**
	 * Get the parent, alias for {@link getParent()}
	 *
	 * @see getParent()
	 * @return M_HtmlNode|false
	 */
	public function parentNode() {
		return $this->getParent();
	}

	/**
	 * Get an attribute of this element
	 *
	 * @param string $arg
	 */
	public function getAttribute($arg) {
		return $this->_getSimpleHtmlDomNode()->getAttribute($arg);
	}

	/**
	 * Has attribute?
	 *
	 * @param string $arg
	 * @return bool
	 */
	public function hasAttribute($arg) {
		return $this->_getSimpleHtmlDomNode()->hasAttribute($arg);
	}
	
	/**
	 * Set an attribute of this element
	 *
	 * @param string $attribute
	 * @param mixed $value
	 * @return void
	 */
	public function setAttribute($attribute, $value) {
		$this->_getSimpleHtmlDomNode()->setAttribute($attribute, $value);
	}
	
	/**
	 * Get the outertext of this element
	 * 
	 * @return string
	 */
	public function getOuterText() {
		return $this->__get('outertext');
	}
	
	public function setOuterText($value) {
		return $this->__set('outertext', $value);
	}
	
	public function getInnerText() {
		return $this->__get('innertext');
	}
	
	public function setInnerText($value) {
		return $this->__set('innertext', $value);
	}
	
	/**
	 * Get the string value of this element
	 * 
	 * Alias for {M_HtmlDomNode::__toString()}
	 *
	 * @see M_HtmlDomNode::__toString()
	 * @return string
	 */
	public function toString() {
		return $this->__toString();
	}
	
	/* -- Magic functions -- */
	
	/**
	 * Get the string value of this element
	 * 
	 * @see M_HtmlDomNode::toString()
	 * @return string
	 */
	public function __toString() {
		return $this->_getSimpleHtmlDomNode()->__toString();
	}
	
	/**
	 * Magic method: get an element or attribute of this element
	 * 
	 * Possible values:
	 * 
	 * - outertext
	 * - innertext
	 * - plaintext
	 * - xmltext
	 *
	 * @param string $arg
	 * @return mixed
	 */
	public function __get($arg) {
		switch ($arg) {
			case 'outertext':
				return $this->_getSimpleHtmlDomNode()->outertext();
				break;
			case 'innertext':
				return $this->_getSimpleHtmlDomNode()->innertext();
				break;
			default:
				return $this->_getSimpleHtmlDomNode()->$arg;
		}
	}
	
	/**
	 * Magic method: modify an element or attribute of this element
	 * 
	 * This can be one of
	 * 
	 * - outertext
	 * - innertext
	 * - any other attribute of the html element (src, href, alt, id, ...)
	 *
	 * @param string $arg
	 * @param mixed $value
	 * @return void
	 */
	public function __set($arg, $value) {
		switch ($arg) {
			case 'innertext':
				$this->_getSimpleHtmlDomNode()->__set('innertext',$value);
				break;
			case 'outertext':
				$this->_getSimpleHtmlDomNode()->__set('outertext',$value);
				break;
			default:
				$this->setAttribute($arg, $value);
		}
	}
}