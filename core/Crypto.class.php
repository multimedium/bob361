<?php
/**
 * M_Crypto
 * 
 * M_Crypto provides with tools to encrypt (and decrypt) a given 
 * (string) subject.
 * 
 * @package Core
 */
class M_Crypto extends M_Object {
	/**
	 * Encrypt
	 * 
	 * This method will convert the data into an encrypted code, using
	 * the provided key. Note that M_Crypto will (try to) create
	 * a different (randomized) code on every call of encrypt(), even 
	 * if the encrypted subject is the same. Still, each randomized
	 * code can be decrypted with {@link M_Crypto::decrypt()}.
	 * 
	 * @access public
	 * @param string $data
	 * 		The data to be encrypted
	 * @param string $key
	 * 		The key to be used to encrypt the data
	 * @return string
	 */
	public function encrypt($data, $key, $randomize = 3) {
		// "Randomize" the output ($randomize rounds of pseudo-shuffling the
		// characters in the string)
		for($i = 0; $i < $randomize; $i ++) {
			$r = rand(2, 5);
			$temp1 = str_split($data, $r);
			$temp2 = array_pop($temp1);
			$data = $r . implode('', array_reverse($temp1)) . $temp2;
		}
		
		$data .= chr(rand(97, 122));
		
		// add the key to the data:
		$n = strlen($data);
		$key = str_pad($key, $n, $key, STR_PAD_RIGHT);
		$out = '';
		
		for($i = 0; $i < $n; $i ++) {
			$char = ord($data[$i]) + ord($key[$i]);
			if(($char > 64 && $char < 91) || ($char > 96 && $char < 123)) {
				$out .= chr($char);
			} else {
				$out .= floor($char / 35);
				$out .= chr(($char % 35) + 35);
			}
		}
		
		// we keep the encrypted code URL-compatible
		return strtr($out, array(
			'%' => 'az',
			'/' => 'by',
			'?' => 'cx',
			'=' => 'dw',
			'&' => 'ev',
			'>' => 'fu',
			'<' => 'gt',
			'-' => 'hs',
			'*' => 'ir',
			'$' => 'jq',
			'@' => 'lp',
			'#' => 'mo',
			'(' => 'nn',
			')' => 'om',
			'{' => 'pl',
			'}' => 'qk',
			'+' => 'rj',
			':' => 'si',
			';' => 'th',
			'"' => 'ug',
			"'" => 'vf'
		));
	}
	
	/**
	 * Decrypt
	 * 
	 * Makes an encrypted blob of data intelligible again.
	 * 
	 * @access public
	 * @param string $data
	 * 		The blob of data to be decrypted
	 * @param string $key
	 * 		The key to be used to decrypt the data
	 * @return string
	 */
	public function decrypt($data, $key, $randomize = 3) {
		$data = strtr($data, array(
			'az' => '%',
			'by' => '/',
			'cx' => '?',
			'dw' => '=',
			'ev' => '&',
			'fu' => '>',
			'gt' => '<',
			'hs' => '-',
			'ir' => '*',
			'jq' => '$',
			'lp' => '@',
			'mo' => '#',
			'nn' => '(',
			'om' => ')',
			'pl' => '{',
			'qk' => '}',
			'rj' => '+',
			'si' => ':',
			'th' => ';',
			'ug' => '"',
			'vf' => "'"
		));
		
		$n = strlen($data);
		$key = str_pad($key, $n, $key, STR_PAD_RIGHT);
		$out = '';
		
		// remove the key from the data:
		for($i = 0, $j = 0; $i < $n; $i ++, $j ++) {
			$char = ord($data[$i]);
			if($char > 47 && $char < 58) {
				$ord  = (((int) $data[$i]) * 35) + (ord($data[++ $i]) - 35);
				$out .= chr($ord - ord($key[$j]));
			} else {
				$out .= chr($char - ord($key[$j]));
			}
		}
		
		// Remove "randomness"
		$out = substr($out, 0, -1);
		for($i = 0; $i < $randomize; $i ++) {
			$r = (int) $out{0};
			if($r > 0) {
				$temp1 = str_split(substr($out, 1), $r);
				$temp2 = array_pop($temp1);
				$out = implode('', array_reverse($temp1)) . $temp2;
			}
		}
		
		return $out;
	}
}
?>