<?php
/**
 * M_Number class
 * 
 * NOTE:
 * M_Number is part of the Locale API. For more information, start the
 * intro at {@link M_Locale}
 * 
 * @package Core
 */
class M_Number extends M_Object {
	/**
	 * Default number symbol
	 * 
	 * This constant can be used to address the default "thousand 
	 * separator", or grouping character. By default, M_Number uses
	 * USA Formatting :)
	 * 
	 * <code>
	 *    87654.95   ==>   87,654.95
	 * </code>
	 * 
	 * So, by default, M_Number uses a comma as a thousands separator.
	 */
	const DEFAULT_SYMBOL_GROUPING = ',';
	
	/**
	 * Default number symbol
	 * 
	 * This constant can be used to address the default "decimal 
	 * separator". By default, M_Number uses USA Formatting :)
	 * 
	 * <code>
	 *    87654.95   ==>   87,654.95
	 * </code>
	 * 
	 * So, by default, M_Number uses a dot as a decimal separator.
	 */
	const DEFAULT_SYMBOL_DECIMAL = '.';
	
	/**
	 * The number
	 * 
	 * This property holds the number that is being represented by
	 * the M_Number object. This number is being stored as a float.
	 * 
	 * @access private
	 * @var float
	 */
	private $_number;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @uses M_Number::setNumber()
	 * @param integer|float $number
	 * 		The number that is to be represented by M_Number
	 * @return M_Number
	 */
	public function __construct($number = NULL) {
		if(!is_null($number)) {
			$this->setNumber($number);
		}
	}
	
	/**
	 * Constructor, out of string
	 * 
	 * This method can be used to construct an {@link M_Number} object
	 * out of a string value. Typically, an {@link M_Number} is constructed
	 * this way in order to parse the numerical value out of a formatted
	 * number.
	 * 
	 * Example 1:
	 * <code>
	 *    // Extract numerical value:
	 *    $number = M_Number::constructWithString('8,564');
	 *    echo $number->getNumber();
	 * </code>
	 * 
	 * Note that you can provide a locale name to this method. By doing
	 * so, you specify the locale in which the number has been formatted
	 * in the string. This will influence how the number is extracted
	 * from the string. Check out Example 2 to learn more.
	 * 
	 * Example 2:
	 * <code>
	 *    $number = M_Number::constructWithString('8,564', 'en');
	 *    echo $number->getNumber();
	 * </code>
	 * 
	 * In English, the comma is used as a thousands separator. So, 
	 * Example 2 would output the number 8564.
	 * 
	 * Example 3:
	 * <code>
	 *    $number = M_Number::constructWithString('8,564', 'nl');
	 *    echo $number->getNumber();
	 * </code>
	 * 
	 * In Dutch, the comma is used as a decimal separator. So, Example
	 * 3 would output the number 8.564
	 * 
	 * NOTE:
	 * If no locale is provided to this method, the locale category
	 * LC_NUMERIC will be used to fetch the name of the locale. To
	 * learn more, see {@link M_Locale::getCategory()}.
	 * 
	 * @access public
	 * @param string $number
	 * 		The number, represented in a localized string
	 * @param string $locale
	 * 		The locale in which the number is being provided
	 * @return M_Number
	 */
	public static function constructWithString($number, $locale = NULL) {
		// if locale is not provided, set to locale that has been set 
		// in the LC_NUMERIC category
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_NUMERIC);
		}
		
		// if still no locale has been found, we set the default
		// number symbols:
		if(!$locale) {
			$symbols = array(
				'decimal' => self::DEFAULT_SYMBOL_DECIMAL,
				'group'   => self::DEFAULT_SYMBOL_GROUPING
			);
		}
		// if the locale has been identified successfully, we load the
		// locale's data, in order to obtain the requested locale's data
		// and the number symbols:
		else {
			$data = M_LocaleData::getInstance($locale);
			$symbols = $data->getNumberSymbols();
		}
		
		// We remove the thousands separator from the number. That
		// symbol is pure "decoration", and does not add meaning to the
		// number :). Note that we first translate some double-byte 
		// characters to a plain version (eg. a white-space character)
		$number = trim(str_replace(strtr($symbols['group'], array(
			chr(194) . chr(160) => ' ', // white-space
			// Other characters may go here
		)), '', strtr($number, array(
			chr(194) . chr(160) => ' ', // white-space
			// Other characters may go here
			
			// Remove percent sign
			'%' => '', 
			// Remove EURO-sign
			chr(226) . chr(130) . chr(172) => '',
			// Remove YEN-sign
			chr(194).chr(165) => '',
			// Remove DOLLAR-sign
			chr(36) => '', // DOLLAR: US, CANADIAN, AUSTRALIAN...
			// Remove POUND-sign
			chr(194).chr(163) => ''
		))));
		
		// Now, we replace the decimal separator with a dot. By doing
		// so, we are preparing the floating number notation:
		if($symbols['decimal'] <> '.') {
			$number = str_replace($symbols['decimal'], '.', $number);
		}
		
		// Now, we should have a valid number. We try to construct
		// the number object (which is the return value of this method)
		// Note that the constructor will throw an exception if the
		// string is not representing a numeric value:
		return new self($number);
	}
	
	/**
	 * Format a number
	 * 
	 * Let's assume you want to format the number 30 as a string, in a given 
	 * locale: nl. You could do so by writing the following code:
	 * 
	 * <code>
	 *    $n = M_Number(30);
	 *    echo $n->toString('nl');
	 * </code>
	 * 
	 * This (static) function allows you to simplify that code into one single
	 * line of code:
	 * 
	 * <code>
	 *    M_Number::getNumberAsString(30, 'nl');
	 * </code>
	 * 
	 * @static
	 * @uses M_Number::toString()
	 * @access public
	 * @param integer|$float $number
	 * @param string $locale
	 * @return string
	 */
	public static function getNumberAsString($number, $locale = NULL) {
		$n = new self($number);
		return $n->toString($locale);
	}
	
	/**
	 * Format a number as percentage
	 * 
	 * Let's assume you want to format the number 30 as a percent string, in a 
	 * given locale: nl. You could do so by writing the following code:
	 * 
	 * <code>
	 *    $n = M_Number(30);
	 *    echo $n->toPercentString('nl');
	 * </code>
	 * 
	 * This (static) function allows you to simplify that code into one single
	 * line of code:
	 * 
	 * <code>
	 *    M_Number::getNumberAsPercentString(30, 'nl');
	 * </code>
	 * 
	 * @static
	 * @uses M_Number::toPercentString()
	 * @access public
	 * @param integer|$float $number
	 * @param string $locale
	 * @return string
	 */
	public static function getNumberAsPercentString($number, $locale = NULL) {
		$n = new self($number);
		return $n->toPercentString($locale);
	}
	
	/**
	 * Format a number as currency
	 * 
	 * Let's assume you want to format the number 30 as a price in EUR, in a 
	 * given locale: nl. Also, we would like to define a precision of 2 decimals 
	 * for the formatted price. You could do so by writing the following code:
	 * 
	 * <code>
	 *    $n = M_Number(30);
	 *    echo $n->toCurrencyString('EUR', 'nl', 2);
	 * </code>
	 * 
	 * This (static) function allows you to simplify that code into one single
	 * line of code:
	 * 
	 * <code>
	 *    M_Number::getNumberAsCurrencyString(30, 'EUR', 'nl', 2);
	 * </code>
	 * 
	 * @static
	 * @uses M_Number::toCurrencyString()
	 * @access public
	 * @param integer|$float $number
	 * @param string $locale
	 * @return string
	 */
	public static function getNumberAsCurrencyString($number, $currencyCode, $locale = NULL, $precision = NULL) {
		$n = new self($number);
		return $n->toCurrencyString($currencyCode, $locale, $precision);
	}
	
	/**
	 * Set the number
	 * 
	 * This method will set the number that is being represented by
	 * the M_Number object. Note that, if a number has been provided
	 * to the M_Number constructor, this method will be used to set
	 * the number internally.
	 * 
	 * @access public
	 * @see M_Number::__construct()
	 * @param integer|float $number
	 * 		The number that is to be represented by M_Number
	 * @return void
	 */
	public function setNumber($number) {
		if(is_numeric($number)) {
			if(gettype($number) == 'double' || strpos($number, '.') !== FALSE) {
				$this->_number = (float) $number;
			} else {
				$this->_number = (int) $number;
			}
		} else {
			throw new M_NumberException(sprintf(
				'%s expects a numeric value! Cannot accept "%s"', 
				__CLASS__,
				$number
			));
		}
	}
	
	/**
	 * Get number
	 * 
	 * This method will return the number that is being represented
	 * by the M_Number object. The return value will be an integer 
	 * or a float.
	 * 
	 * @access public
	 * @see M_Number::__construct()
	 * @see M_Number::setNumber()
	 * @return integer|float
	 */
	public function getNumber() {
		return $this->_number;
	}
	
	/**
	 * Is float?
	 * 
	 * This method will tell whether or not the number is a floating
	 * number or not. The return value of this method is TRUE if the
	 * number is floating, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isFloat() {
		return is_float($this->_number);
	}
	
	/**
	 * Format the number
	 * 
	 * This method will format the number, with the decimal number
	 * format that has been defined in the current locale's data.
	 * This method has been implemented with the CLDR specifications
	 * in mind. To learn about number formatting in CLDR:
	 * 
	 * - http://www.unicode.org/reports/tr35/#Number_Format_Patterns
     * - http://www.unicode.org/reports/tr35/#NumberElements
     * 
     * Example 1
	 * <code>
	 *   Pattern      | Number         | Output
	 *   -------------|----------------|--------------
	 *   ##0.#        | 12345.12345    | 12345.12345
     *   ##0.00       | 12345.12345    | 12345.12
     *   ##,##0.00    | 12345.12345    | 12,345.12
     * </code>
     * 
     * {@link M_LocaleData} will provide with the pattern for number  
     * formatting, in the current locale. To get the pattern, this
     * method requests the number pattern as following:
     * 
     * Example 2
     * <code>
     *    $data = M_LocaleData::getInstance($locale);
     *    $pattern = $data->getNumberFormat('decimal');
     * </code>
     * 
     * The symbols that are used to separate groups (typically by the
     * thousands) and decimals in numbers, are fetched with 
     * {@link M_LocaleDate::getNumberSymbols()}.
     * 
     * Note that the $locale variable in Example 2 will have been set
     * to the locale that has been set in the locale category 
     * LC_NUMERIC. See {@link M_Locale::getCategory()} for more info. 
     * However, you can force the locale to another one, by providing
     * the locale name to this function.
     * 
     * Example 3
     * <code>
     *    M_Locale::setCategory(M_Locale::LC_NUMERIC, 'en');
     *    
     *    // output the number in french, ignoring current locale
     *    $num = new Number(125489.54);
     *    echo $num->toString('fr');
     * </code>
     * 
     * IMPORTANT NOTES:
     * ' is used to quote special characters in a prefix or suffix, 
     * for example, "'#'#" formats 123 to "#123". To create a single 
     * quote itself, use two in a row: "# o''clock".
     * 
     * @access public
     * @uses M_LocaleData
     * @uses M_Number::toFormattedString()
	 * @param string $locale
	 * 		The locale name from which to retrieve the number pattern
	 * 		for formatting. This locale, if not given, is defaulted to
	 * 		the current locale (set in category LC_NUMERIC).
	 * @return string
	 * 		The formatted number
	 */
	public function toString($locale = NULL) {
		// if locale is not provided, set to locale that has been set in
		// the LC_NUMERIC category
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_NUMERIC);
		}
		
		// if still no locale has been found, we set the default
		// number pattern:
		if(!$locale) {
			$pattern = '#,##0.#';
			$symbols = array(
				'decimal' => self::DEFAULT_SYMBOL_DECIMAL,
				'group'   => self::DEFAULT_SYMBOL_GROUPING
			);
		}
		// if the locale has been identified successfully, we load the
		// locale's data, in order to obtain the requested locale's data
		// and the number pattern:
		else {
			$data = M_LocaleData::getInstance($locale);
			$pattern = $data->getNumberFormat('decimal');
			$symbols = $data->getNumberSymbols();
		}
		
		// parse the number and return result
		return $this->toFormattedString($this->_number, $pattern, $symbols['group'], $symbols['decimal']);
	}
	
	/**
	 * Format the number as currency
	 * 
	 * This method will format the number, expressed in the context of
	 * currency. Typically, this method is used to format product
	 * prices.
	 * 
	 * Read more about number formatting at {@link M_Number::toString()}.
	 * Also in the context of currency, {@link M_LocaleData} will 
	 * provide with the pattern for number formatting. To get the 
	 * pattern, this method requests the number pattern as following:
     * 
     * Example 2
     * <code>
     *    $data = M_LocaleData::getInstance($locale);
     *    $pattern = $data->getNumberFormat('currency');
     * </code>
     * 
     * Note that, if no locale name has not been provided to this 
     * method, this method will default the locale name to the one 
     * that has been set in the category LC_MONETARY. See 
     * {@link M_Locale::getCategory()} for more info.
     * 
     * @access public
     * @uses M_LocaleData
     * @uses M_Number::toFormattedString()
     * @param string $currencyCode
     * 		The code of the currency in which the number should be 
     * 		expressed. For example: "USD", "EUR", etc.
	 * @param string $locale
	 * 		The locale name from which to retrieve the number pattern
	 * 		for formatting. This locale, if not given, is defaulted to
	 * 		the current locale (set in category LC_MONETARY).
	 * @return string
	 * 		The formatted number
	 */
	public function toCurrencyString($currencyCode, $locale = NULL, $precision = NULL) {
		// if locale is not provided, set to locale that has been set in
		// the LC_MONETARY category
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_MONETARY);
		}
		
		// Local cache for patterns with different precisions:
		static $patternsWithPrecisions = array();
		
		// Prepare the currency placeholder character
		$currencyPlaceholder = chr(194) . chr(164);
		
		// if still no locale has been found, we set the default
		// number pattern:
		if(!$locale) {
			$data    = NULL;
			$pattern = "$currencyPlaceholder#,##0.00;($currencyPlaceholder#,##0.00)";
			$symbols = array(
				'decimal' => self::DEFAULT_SYMBOL_DECIMAL,
				'group'   => self::DEFAULT_SYMBOL_GROUPING
			);
		}
		// if the locale has been identified successfully, we load the
		// locale's data, in order to obtain the requested locale's data
		// and the number pattern:
		else {
			$data = M_LocaleData::getInstance($locale);
			$pattern = $data->getNumberFormat('currency');
			$symbols = $data->getNumberSymbols();
		}
		
		// extract the first part of the pattern
		$temp    = explode(';', $pattern);
		$pattern = $temp[0];
		
		// Is precision provided?
		if(! is_null($precision)) {
			// The key with which to look up patterns with different precisions,
			// in the local cache:
			$precisionKey = (string) $locale . '-' . $precision;
			
			// If the pattern was not yet rendered earlier:
			if(! array_key_exists($precisionKey, $patternsWithPrecisions)) {
				// Initiate some working variables:
				$posDot = strpos($pattern, '.');
				$posCurrencyPlaceholder = strpos($pattern, $currencyPlaceholder);
				$isCurrencyAfterDec = ($posCurrencyPlaceholder !== FALSE && $posCurrencyPlaceholder > $posDot);

				// If currency symbol is after the dot:
				if($isCurrencyAfterDec) {
					// Check if the currency symbol is prepended with whitespaces
					// or other characters. If so, we'll modify the position where
					// the currency symbol starts, so these characters are included
					$pre = substr($pattern, $posCurrencyPlaceholder - 1, 1);
					while($pre != '#' && $pre != ',' && $pre != '0' && $pre != '0') {
						$posCurrencyPlaceholder -= 1;
						$pre = substr($pattern, $posCurrencyPlaceholder - 1, 1);
					}
				}

				// Recompose the pattern:
				$pattern = 
					// With the piece before the decimals
					substr($pattern, 0, $posDot) . 
					// The new precision (decimals)
					(
						$precision > 0 
							? '.' . str_repeat('0', $precision)
							: ''
					) . 
					// And, the remaining part:
					(
						$isCurrencyAfterDec
							? substr($pattern, $posCurrencyPlaceholder)
							: ''
					);
				
				// And store in local cache
				$patternsWithPrecisions[$precisionKey] = $pattern;
			}
			// If already rendered earlier:
			else {
				// Then, retrieve the pattern from local cache
				$pattern = $patternsWithPrecisions[$precisionKey];
			}
		}
		
		// parse the number and return result
		return strtr(
			$this->toFormattedString($this->_number, $pattern, $symbols['group'], $symbols['decimal']),
			array(
				$currencyPlaceholder => $data === NULL ? $currencyCode : $data->getCurrencyDisplayName('symbol', $currencyCode),
				str_repeat($currencyPlaceholder, 2) => $currencyCode,
				str_repeat($currencyPlaceholder, 3) => $data === NULL ? $currencyCode : $data->getCurrencyDisplayName('wide', $currencyCode),
			)
		);
	}
	
	/**
	 * Format the number as percentage
	 * 
	 * This method will format the number, expressed in the context of
	 * percentage. Typically, this method is used to express a given
	 * percentage.
	 * 
	 * Read more about number formatting at {@link M_Number::toString()}.
	 * Also in the context of percents, {@link M_LocaleData} will 
	 * provide with the pattern for number formatting. To get the 
	 * pattern, this method requests the number pattern as following:
     * 
     * Example 2
     * <code>
     *    $data = M_LocaleData::getInstance($locale);
     *    $pattern = $data->getNumberFormat('percent');
     * </code>
     * 
     * This method uses the same rules to fetch the locale name as in 
     * {@link M_Number::toString()}.
     * 
     * @access public
     * @uses M_LocaleData
     * @uses M_Number::toFormattedString()
	 * @param string $locale
	 * 		The locale name from which to retrieve the number pattern
	 * 		for formatting. This locale, if not given, is defaulted to
	 * 		the current locale (set in category LC_NUMERIC).
	 * @return string
	 * 		The formatted number
	 */
	public function toPercentString($locale = NULL) {
		// if locale is not provided, set to locale that has been set in
		// the LC_NUMERIC category
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_NUMERIC);
		}
		
		// if still no locale has been found, we set the default
		// percentage number pattern:
		if(!$locale) {
			$pattern = '#,##0%';
			$symbols = array(
				'decimal' => self::DEFAULT_SYMBOL_DECIMAL,
				'group'   => self::DEFAULT_SYMBOL_GROUPING
			);
		}
		// if the locale has been identified successfully, we load the
		// locale's data, in order to obtain the requested locale's data
		// and the number pattern:
		else {
			$data = M_LocaleData::getInstance($locale);
			$pattern = $data->getNumberFormat('percent');
			$symbols = $data->getNumberSymbols();
		}

		// parse the number and return result
		return $this->toFormattedString($this->_number, $pattern, $symbols['group'], $symbols['decimal']);
	}
	
	/**
	 * Format the number
	 * 
	 * This method is used by the following methods, in order to
	 * parse a number into a given pattern:
	 * 
	 * - {@link M_Number::toString()}
	 * - {@link M_Number::toCurrencyString()}
	 * - {@link M_Number::toPercentString()}
	 * 
	 * This method is feeded with a hard-coded number pattern. So, you
	 * should probably NOT be using this method. Instead, use one of 
	 * the methods listed above.
	 * 
	 * Example 1, format a number
	 * <code>
	 *    echo M_Number::toFormattedString(1234568.154, '#,##0.0', ',', '.');
	 * </code>
	 * 
	 * Example 1 would output:
	 * 
	 * <code>
	 *    1,234,568.1
	 * </code>
	 * 
	 * This method has been implemented with the CLDR specifications
	 * in mind. To learn about number formatting in CLDR:
	 * 
	 * - http://www.unicode.org/reports/tr35/#Number_Format_Patterns
     * - http://www.unicode.org/reports/tr35/#NumberElements
     * 
     * @access public
     * @see M_Number::toString()
     * @see M_Number::toCurrencyString()
     * @see M_Number::toPercentString()
     * @param integer|float $number
     * 		The number to be formatted
     * @param string $pattern
     * 		The pattern to be used for number formatting
     * @param string $groupSymbol
     * 		The symbol to be used to separate groups (typically by the
     * 		thousands). The pattern defines the grouping length.
     * @param string $decimalSymbol
     * 		The symbol to be used to separate decimals
     * @return string
     * 		The formatted number
	 */
	public static function toFormattedString($number, $pattern, $groupSymbol, $decimalSymbol) {
		// If number if NULL, return empty string
		if(is_null($number)) {
			return '';
		}
		
		// Get the pattern parts:
		$match = array();
		if(preg_match('/[#0,](,)?([#0]+)\.?([#0]{0,})/', $pattern, $match)) {
			// Cast the number to a string
			$number = (string) $number;
			
			// Remove number symbol (e.g. negative/minus sign)
			$prefixSymbols = '';
			while(! is_numeric(substr($number, 0, 1))) {
				$prefixSymbols = substr($number, 0, 1);
				$number = substr($number, 1);
			}
			
			// Now, we separate the decimals from the original number
			$number = explode('.', $number);

			// Get the decimals definition (if only zero's, it defines 
			// the number of digits in the decimals)
			// If the decimals definition is empty, we do not include
			// the decimals in the output string, even if the original
			// number has decimals.
			// M_Debug::printArray($match);
			if($match[3] == '') {
				if(isset($number[1])) {
					unset($number[1]);
				}
			}
			// if not empty, do decimals:
			elseif(substr($match[3], -1, 1) == '0') {
				// Get the expected number of decimals in the number:
				$zeroPadding = strlen($match[3]);

				// If the original number has decimals:
				if(isset($number[1])) {
					$numberLength = strlen($number[1]);

					// If the number of decimals in the original number is bigger
					// than the allowed number of decimals:
					if($numberLength > $zeroPadding) {
						// We round the number to the precision that is defined
						// by the pattern. In order to do so, we fetch a substring
						// with one extra digit for rounding...
						$number = explode('.', round($number[0] . '.' . substr($number[1], 0, $zeroPadding + 1), $zeroPadding));

						// Set decimals manually, should they not exist
						if(! isset($number[1])) {
							$number[1] = '';
						}
					}

					// Apply zeropadding, if necessary:
					$number[1] = str_pad($number[1], $zeroPadding, '0', STR_PAD_RIGHT);
				}
				// If the original number does not have decimals:
				else {
					if($zeroPadding > 0) {
						$number[1] = str_repeat('0', $zeroPadding);
					}
				}
			}
			
			// Get the length of groups:
			$groupLength = ($match[1] == ',') ? strlen($match[2]) : 0;
			
			// Get zero-padding length
			// (the zero's in the group are used to add zero-padding for
			// smaller numbers)
			$zeroPadding = 1;
			$zeroPos = strpos($match[2], '0');
			if($zeroPos !== FALSE) {
				$zeroPadding = strlen($match[2]) - $zeroPos;
			}
			
			// add zero-padding
			$numberLength = strlen($number[0]);
			if($numberLength < $zeroPadding) {
				$number[0] = str_pad($number[0], $zeroPadding, '0', STR_PAD_LEFT);
			}
			// or, make groups in the number:
			elseif($groupLength > 0 && $numberLength > $groupLength) {
				$number[0] = implode($groupSymbol, array_reverse(array_map('strrev', str_split(strrev($number[0]), $groupLength))));
			}
			
			if(isset($number[1])) {
				return str_replace($match[0], $prefixSymbols . $number[0] . $decimalSymbol . $number[1], $pattern);
			} else {
				return str_replace($match[0], $prefixSymbols . $number[0], $pattern);
			}
		}
		
		return $number;
	}
	
	/**
	 * Export to string
	 * 
	 * This is a "magic method", called automatically by PHP when the
	 * M_Number object is being casted to a string. For example, an 
	 * echo call on the M_Number object will print the result of this 
	 * function.
	 * 
	 * Example 1
	 * <code>
	 *    $num = new M_Number(12);
	 *    echo $num; // prints the result of __toString()
	 * </code>
	 * 
	 * Example 2
	 * <code>
	 *    $num = new M_Number(12);
	 *    $str = (string) $num; // saves the result of __toString()
	 * </code>
	 * 
	 * This method will format the number (in decimal format), in the 
	 * currently active locale. Read {@link M_Number::toString()} for 
	 * more info.
	 * 
	 * Example 3 (exactly the same result as Example 1)
	 * <code>
	 *    $num = new M_Number(12);
	 *    echo $num->toString();
	 * </code>
	 * 
	 * @access public
	 * @uses M_Number::toString()
	 * @return string
	 */
	public function __toString() {
		return $this->toString();
	}
}