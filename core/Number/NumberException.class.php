<?php
/**
 * M_NumberException class
 *
 * Exceptions thrown by {@link M_Number}.
 * 
 * @package Core
 */
class M_NumberException extends M_Exception {
}