<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty css plugin
 *
 * - Type:     function
 * - Purpose:  Add css file to page
 * 
 * This function can be used in a smarty template, to add a css file to the page.
 * This plugin will use {@link M_ViewCss} to do so...
 * 
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_cssfile($params, $smarty) {
	// Get the CSS singleton
	$css = M_ViewCss::getInstance();
	
	// Check if a file has been provided:
	if(isset($params['file']) && ! empty($params['file'])) {
		// Add the file:
		$css->addFile(
			new M_File($params['file']),
			// Context
			isset($params['context']) && ! empty($params['context']) ? $params['context'] : NULL,
			// Conditions
			isset($params['conditions']) && ! empty($params['conditions']) ? $params['conditions'] : NULL,
			// Media
			isset($params['media']) && ! empty($params['media']) ? $params['media'] : 'screen'
		);
	}
	// If the path to the file has not been provided:
	else {
		// Throw an exception
		throw new M_Exception(sprintf(
			'Cannot add CSS file; %s expects a path to the file; {cssfile file=[path] [context=... condition=...]}',
			__FUNCTION__
		));
	}
}