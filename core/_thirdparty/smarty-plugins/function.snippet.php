<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty snippet plugin
 *
 * - Type:     function
 * - Name:     snippet
 * - Purpose:  Get a text snippet
 * 
 * This function can be used in a smarty template, to extract a snippet
 * from a given text.
 * 
 * Usage Example 1
 * <code>
 *    {snippet from="Hello world" length=40}
 * </code>
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_snippet($params, $smarty) {
	// Has untranslated string been provided?
	if(
		array_key_exists('from', $params) &&
		array_key_exists('length',$params) && is_numeric($params['length']) && $params['length'] > 0
	) {
		//etc param?
		$etc = (array_key_exists('etc', $params)) ? $params['etc'] : '...';
		
		// Get text
		$text = new M_Text((string) $params['from']);
		return M_smarty_plugin_result($text->getSnippet((int) $params['length'], $etc), $params, $smarty);
	}
	// Throw exception, if not provided
	else {
		throw new M_ViewException(sprintf(
			'%s requires the following parameters: {snippet from=[string] length=[integer]}',
			'smarty.snippet'
		));
	}
	
	// Return empty string, if still here
	return '';
}
?>