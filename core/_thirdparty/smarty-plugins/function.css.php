<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty css plugin
 *
 * - Type:     function
 * - Purpose:  Get css
 * 
 * This function can be used in a smarty template, to get the css code
 * that has been prepared with {@link M_ViewCss}
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_css($params, $smarty) {
	$css = M_ViewCss::getInstance();
	return M_smarty_plugin_result($css->toString(), $params, $smarty);
}