<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty alpha id plugin
 *
 * - Type:     function
 * - Purpose:  Encode or decode a numeric id into an alpha id
 * 
 * Examples
 * <code>
 *    {alpha_id id=215 type="encode" passkey="$i89cJq&"}
 * </code>
 * 
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_alpha_id($params, $smarty) {
	// Make sure a variable id has been specified
	$id = M_Helper::getArrayElement('id', $params, false);
	
	// If not
	if(! $id || $id == '') {
		// We throw an exception
		throw new M_Exception(sprintf(
			'Cannot retrieve id variable; %s expects a variable id: {alpha_id id=[int|string] [ encode=[bool] passkey=[string] ]}',
			'smarty.alpha_id'
		));
	}

	//check if we need to encode or decode - encode is default
	$encode = (bool) M_Helper::getArrayElement('encode', $params, true);
	$passKey  = M_Helper::getArrayElement('passkey', $params);
	
	if ($encode) {
		$result = M_Helper::encodeAlphaId($id, $passKey);
	}else {
		$result = M_Helper::decodeAlphaId($id, $passKey);
	}
	
	// Return the value
	return M_smarty_plugin_result($result, $params, $smarty);
}