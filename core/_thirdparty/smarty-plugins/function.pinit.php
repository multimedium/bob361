<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty Pin It plugin
 *
 * - Type:     function
 * - Name:     pin it
 * - Purpose:  include pin it (from Pinterest) plugins in your website
 * 
 * 
 * @example {pinit url="http://www.example.com" media="http://www.example.com/test" description="Nice picture!"}
 * @author Tim Segers
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_pinit($params, $smarty) {
	$uri = new M_Uri('http://pinterest.com/pin/create/button/');

	// Get the URL-param
	$url = M_Helper::getArrayElement('url', $params);
	if( ! $url) {
		throw new M_Exception('Please provide an URL to the pinit plugin');
	}
	
	// Get the media-param
	$media = M_Helper::getArrayElement('media', $params);
	if( ! $media) {
		throw new M_Exception('Please provide an media to the pinit plugin');
	}
	
	// Get the description-param
	$description = M_Helper::getArrayElement('description', $params);
	
	// Set the query variables
	$uri->setQueryVariable('url', $url);
	$uri->setQueryVariable('media', $media);
	if($description) {
		$uri->setQueryVariable('description', $description);
	}
	
	// Generate the output
	$html = 
		'<a href="'.$uri->toString().'" ' .
		'class="pin-it-button" count-layout="horizontal">' .
			'<img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" />' .
		'</a>' . 
		'<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>';
	
	// Return result:
	return M_smarty_plugin_result($html, $params, $smarty);
}