<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty identity plugin
 *
 * - Type:     function
 * - Name:     identity
 * - Purpose:  Get the identity of the application
 * 
 * This function can be used in a smarty template, to get the identity of the
 * application. For more info, read {@link M_ApplicationIdentity}
 * 
 * Usage Example 1
 * <code>
 *    {* To get attributes of the identity *}
 *    {identity get="@name"}
 *    {identity get="@address/street"}
 *    {identity get="@address/postalCode @address/city"}
 *    {identity get="@address/country"}
 *    {identity get="@email"}
 *    
 *    {* To get the contact information; instance of M_Contact *}
 *    {identity assignto="id"}
 * </code>
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_identity($params, $smarty) {
	// Get the application identity: contact information
	$contact = M_ApplicationIdentity
		::getInstance()
		->getContact();
	
	// If the getter has been specified:
	if(isset($params['get']) && ! empty($params['get'])) {
		// Prepare values:
		$values = array(
			'@name'               => $contact->getName(),
			'@nickname'           => $contact->getNickName(),
			'@vat'                => $contact->getVatNumber()
		);

		// Get address:
		$address = $contact->getAddress();

		// If an address is available
		if($address) {
			// Then, add the placeholders for the address as well:
			$values['@address/street']     = $contact->getAddress()->getStreetAddress();
			$values['@address/postalCode'] = $contact->getAddress()->getPostalCode();
			$values['@address/city']       = $contact->getAddress()->getCity();
			$values['@address/region']     = $contact->getAddress()->getRegion();
			$values['@address/country']    = $contact->getAddress()->getCountry();
			$values['@address/longitude']  = $contact->getAddress()->getLongitude();
			$values['@address/latitude']   = $contact->getAddress()->getLatitude();
			$values['@email']              = $contact->getEmail()->getEmail();
		}
		
		// Phone number(s)
		/* @var $number M_ContactPhoneNumber */
		foreach($contact->getPhoneNumbers() as $number) {
			// Add the number:
			$values['@phone/' . $number->getType()] = $number->getCompleteNumber();
		}
		
		//Email addresses
		foreach($contact->getEmails() as $key => $email) {
			$values['@email/'.$email->getType()] = $email->getEmail();
		}
		
		// Return the requested string:
		return M_smarty_plugin_result(
			strtr(
				$params['get'],
				$values
			),
			$params,
			$smarty
		);
	}
	
	// If the getter has not been specified:
	return M_smarty_plugin_result($contact, $params, $smarty);
}