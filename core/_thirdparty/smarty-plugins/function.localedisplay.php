<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty localedisplay plugin
 *
 * - Type:     function
 * - Name:     localedisplay
 * - Purpose:  Get the display value of a given locale, territory, or language
 * 
 * This function can be used in a smarty template, to get the display value of a 
 * given locale-related variable.
 * 
 * @example {localedisplay name="be" type="territory"}
 * @example {localedisplay name="nl" type="language"}
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_localedisplay($params, $smarty) {
	// Make sure a name has been provided
	$isNameGiven = (isset($params['name']) && !empty($params['name']));
	
	// If not given:
	if(! $isNameGiven) {
		// We throw an exception
		throw new M_Exception(sprintf(
			'Cannot build locale display name with {localedisplay}; Missing "name" attribute'
		));
	}
	
	// Make sure a type has been provided
	$isTypeGiven = (isset($params['type']) && !in_array($params['name'], array('language', 'territory')));
	
	// If not given:
	if(! $isTypeGiven) {
		// We throw an exception
		throw new M_Exception(sprintf(
			'Cannot build locale display name with {localedisplay}; Please provide "territory" or "language" as the "type" attribute\'s value'
		));
	}
	
	// Check if a locale has been provided:
	$isLocaleGiven = (isset($params['locale']) && M_LocaleMessageCatalog::isInstalled($params['locale']));
	
	// If not:
	if(! $isLocaleGiven) {
		// We fetch the locale from locale category LANG
		$params['locale'] = M_Locale::getCategory(M_Locale::LANG);
	}
	
	// Depending on the type of the display name:
	switch($params['type']) {
		// Language display name
		case 'language':
			return M_smarty_plugin_result(M_Locale::getLanguageDisplayName($params['name'], $params['locale']), $params, $smarty);
		
		// Territory display name
		// case 'territory':
		default:
			return M_smarty_plugin_result(M_Locale::getTerritoryDisplayName($params['name'], $params['locale']), $params, $smarty);
	}
}