<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty plural plugin
 *
 * - Type:     function
 * - Name:     plural
 * - Purpose:  Get translated text in plural form
 * 
 * This function can be used in a smarty template, to translate a given
 * plural form. For more information about retrieving translated
 * strings in the correct plural form, read the docs on:
 * 
 * - {@link M_LocaleMessageCatalog::getPluralText()}
 * - {@link M_LocaleMessageCatalog::getPluralFormula()}
 * - {@link M_LocaleMessageCatalog::getNumberOfPluralForms()}
 * 
 * Usage Example 1
 * <code>
 *    {plural 
 *        singular="1 
 *        file" plural="@count files" 
 *        count=2
 *    }
 * </code>
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_plural($params, $smarty) {
	// Has untranslated string been provided?
	if(
		isset($params['singular']) && !empty($params['singular']) && 
		isset($params['plural']) && !empty($params['plural']) && 
		array_key_exists('count', $params)
	) {
		// Get untranslated strings
		$singular = $params['singular'];
		$plural   = $params['plural'];
		$count    = (int) $params['count'];

		// Array for placeholder values in translated string
		$tmpParams = array();

		// For each of the parameters:
		foreach($params as $name => $value) {
			// If not reserved keyword:
			if($name != 'singular' && $name != 'plural' && $name != 'count') {
				// We prepare placeholder value
				$tmpParams['@' . $name] = $value;
			}
		}
		
		// Return the translated text:
		$catalog = M_LocaleMessageCatalog::getInstance();
		return M_smarty_plugin_result($catalog->getPluralText($singular, $plural, $count, $tmpParams), $params, $smarty);
	}
	// Throw exception, if not provided
	else {
		throw new M_ViewException(sprintf(
			'%s requires the following parameters: {plural singular=[string] plural=[string] count=[integer]}',
			var_export($params, TRUE),
			'smarty.plural'
		));
	}
	
	// Return empty string, if still here
	return '';
}
?>