<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty urlcopy plugin
 *
 * - Type:     function
 * - Name:     urlcopy
 * - Purpose:  Get a copy of the current URI
 * 
 * This function can be used in a smarty template, to get a copy of the current
 * Request URI
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_urlcopy($params, $smarty) {
	// Get the URI from relative path
	if(isset($params['relPath']) && ! empty($params['relPath'])) {
		// Compose the URI, with the relative path:
		$uri = new M_Uri(M_Request::getLink($params['relPath']));
		
		// Get the query variables:
		$q = M_Request::getVariables();
		
		// If provided:
		if($q) {
			// Copy them to the URL:
			$uri->setQueryVariables($q);
		}
	}
	// Or, instantiate an instance of M_Uri from request
	else {
		$uri = M_Request::getUri();
	}

	// For each of the parameters (query variables)
	foreach($params as $name => $value) {
		// Add the query variable:
		$uri->setQueryVariable($name, $value);
	}

	// Filter variables?
	$filterVariables = (isset($params['filter']) && is_string($params['filter']))
		? explode(',', $params['filter'])
		: array();

	// Some predefined variables to be filtered out of the URL:
	$filterVariables = array_merge(
		$filterVariables,
		array(
			'filter',
			'addPath',
			'relPath',
			'queryVariableSeparator',
			'assignto',
			'deleteEmptyVariables',
			'ignoreQueryVariables'
		)
	);

	// For each filter variables:
	foreach($filterVariables as $filterVariable) {
		// Remove it from the URI:
		$uri->removeQueryVariable($filterVariable);
	}


	// Remove empty variables as well, if requested:
	if(isset($params['deleteEmptyVariables']) && M_Helper::isBooleanTrue($params['deleteEmptyVariables'])) {
		foreach($uri->getQueryVariables() as $name => $value) {
			if(empty($value)) {
				$uri->removeQueryVariable($name);
			}
		}
	}
	
	// Add elements to path?
	$a = isset($params['addPath'])
		? M_Helper::trimCharlist($params['addPath'], '/')
		: '';
	
	if($a) {
		$uri->setPath(
			M_Helper::trimCharlist($uri->getPath()) . '/' . $a
		);
	}
	
	// Set variable separator
	$uri->setQuerySeparator(
		isset($params['queryVariableSeparator'])
			? $params['queryVariableSeparator']
			: '&amp;'
	);
	
	// Check if we need to remove the query variables
	if(isset($params['ignoreQueryVariables']) && $params['ignoreQueryVariables'] == true) {
		$uri->removeQueryVariables();
	}
	
	// Return the requested urlcopy:
	return M_smarty_plugin_result($uri->toString(), $params, $smarty);
}