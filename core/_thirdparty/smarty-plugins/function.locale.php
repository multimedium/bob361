<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty locale plugin
 *
 * - Type:     function
 * - Name:     locale
 * - Purpose:  Get the value of a locale category
 * 
 * This function can be used in a smarty template, to get the value of a given
 * locale category
 * 
 * @example {locale category="LC_MESSAGES"}
 * @example {locale category="LC_MESSAGES" assignto="localeMessageCategory"}
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_locale($params, $smarty) {
	// Make sure a locale category has been provided
	$isCategoryGiven = (isset($params['category']) && !empty($params['category']));
	
	// If no category has been provided, we throw an exception!
	if(! $isCategoryGiven) {
		throw new M_ViewException(sprintf(
			'Cannot get locale in smarty template, with %s(); Missing locale category',
			__FUNCTION__
		));
	}
	
	// Return the requested locale category:
	return M_smarty_plugin_result(M_Locale::getCategory($params['category']), $params, $smarty);
}