<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty css external plugin
 *
 * - Type:     function
 * - Purpose:  Add externally hosted css file
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_cssexternal($params, $smarty) {
	// Get the CSS singleton
	$css = M_ViewCss::getInstance();

	// Check if a link has been provided:
	if(isset($params['link']) && ! empty($params['link'])) {
		// Add the file:
		$css->addExternal(
			new M_Uri($params['link']),
			// Context
			isset($params['context']) && ! empty($params['context']) ? $params['context'] : NULL
		);
	}
	// If the path to the file has not been provided:
	else {
		// Throw an exception
		throw new M_Exception(sprintf(
			'Cannot add CSS EXTERNAL; %s expects an URI; {cssexternal link=[uri] [context=... condition=...]}',
			__FUNCTION__
		));
	}
}