<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty tlist plugin
 *
 * - Type:     function
 * - Purpose:  Create a textual list of items
 *
 * This function can be used in a smarty template, in order to create a textual
 * list of items, for example: "Apples, strawberries, and blackberries"
 *
 * Consider the following example, where we have a collection of colors. This
 * collection is an iterator, and can therefore be an array, or an object of the
 * class {@link ArrayIterator}, or {@link M_ArrayIterator}, ...
 *
 * In our example, we define the collection of colors as following:
 * <code>
 *    $colors = Array(
 *       0 => 'Green',
 *       1 => 'Brown',
 *       2 => 'Blue',
 *       3 => 'Yellow'
 *    )
 * </code>
 *
 * Let's say that, in our template, we want to create a textual list of this
 * collection, for use in a translated text. We would use this plugin to do so,
 * as following:
 *
 * <code>
 *    {tlist collection=$colors commaGlue=', ' andGlue=" and "}
 * </code>
 * 
 * Note that a call to this plugin defines the collection, and the glue with which
 * to create the final phrase. A "comma glue" is expected, and defaulted to ", " if
 * not present. Also, an "and glue" is expected, and defaulted to " and " if not
 * present.
 *
 * In the example, the result of our plugin call would be
 *
 * <code>
 *    Green, Brown, Blue and Yellow
 * </code>
 *
 * By default, the "comma glue" will NOT be translated with the {@link t()} function,
 * while the "and glue" will be translated. You can change this behavior, by adding
 * some additional parameters to the plugin call:
 *
 * <code>
 *    {tlist
 *       collection=$colors
 *       commaGlue=', '
 *       andGlue=" and "
 *       translateCommaGlue="true"
 *       translateAndGlue="true"}
 * </code>
 *
 * Note that, if you are working with a collection of objects, you can specify
 * a getter method that returns the value for each of the listed items in the
 * final string. Let's assume that we now provide a list of objects, and the
 * method getName() can be called for each object in the collection:
 *
 * <code>
 *    {tlist
 *       collection=$colors
 *       getter='getName'
 *       commaGlue=', '
 *       andGlue=" and "}
 * </code>
 *
 * You can specify a start- and end-position, to only include a range of the
 * provided collection in the output string. These positions start at 0 (ZERO).
 *
 * <code>
 *    {tlist
 *       collection=$colors
 *       start=1
 *       end=3}
 * </code>
 *
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_tlist($params, $smarty) {
	// Check if a collection has been provided:
	$isCollection = (isset($params['collection']) && M_Helper::isIterator($params['collection']));

	// If the collection has not been provided:
	if(! $isCollection) {
		// Then, we throw an exception to inform about the error:
		throw new M_Exception(sprintf(
			'Missing parameters to Smarty Plugin function %s. The signature of the ' .
			'function is: {tlist collection=$iterator [ commaGlue=$string andGlue=' .
			'$string translateCommaGlue=$booleanString translateAndGlue=$booleanString ' .
			'getter=$string]}',
			'tlist'
		));
	}

	// Look for the "comma glue", and default to ", " if not present:
	$commaGlue = isset($params['commaGlue'])
		? (string) $params['commaGlue']
		: ', ';

	// Look for the "and glue", and default to "and" if not present:
	$andGlue = isset($params['andGlue'])
		? (string) $params['andGlue']
		: ' and ';

	// Translate the "comma glue"? First, we set the default behavior:
	$tCommaGlue = FALSE;

	// Then, we look for a parameter that overrides the default behavior:
	if(isset($params['translateCommaGlue']) && M_Helper::isBooleanTrue($params['translateCommaGlue'])) {
		// If overriden, set the new value for the boolean:
		$tCommaGlue = TRUE;
	}

	// Translate the "and glue"? First, we set the default behavior:
	$tAndGlue = TRUE;

	// Then, we look for a parameter that overrides the default behavior:
	if(isset($params['translateAndGlue'])) {
		$tAndGlue = M_Helper::isBooleanTrue($params['translateAndGlue']) ? TRUE : FALSE;
	}

	// Has a getter method been specified?
	$getter = (isset($params['getter']))
		? (string) $params['getter']
		: NULL;

	// Start position?
	$start = (isset($params['start']) && is_numeric($params['start']) && $params['start'] > 0)
		? (int) $params['start']
		: NULL;
	
	// End position?
	$end = (isset($params['end']) && is_numeric($params['end']) && $params['end'] > 0)
		? (int) $params['end']
		: NULL;
	
	// We remove empty strings from the collection. A final collection for the
	// rendering of the textual list is created in the $temp variable.
	$temp = array();
	
	// So, for each item in the provided collection:
	$i = 0;
	foreach($params['collection'] as $item) {
		// If start and end has been specified, take it into account:
		if((! $start || ($start && $i ++ >= $start)) && (! $end || ($end && $i <= $end))) {
			// If a getter method has been specified:
			if($getter) {
				// Then we assume that each item is an object, and that the requested
				// getter can be called on that object
				$isCallable = (is_object($item) && is_callable(array($item, $getter)));
				if(! $isCallable) {
					// Throw an exception, if not the case:
					throw new M_Exception(sprintf(
						'Cannot call the getter method %s on a variable of type %s',
						$getter,
						gettype($item)
					));
				}

				// Get the result of the getter, cast it to a string, and trim it:
				$current = trim((string) call_user_func(array($item, $getter)));
			}
			// If a getter method is not specified:
			else {
				// We cast the current item to a string, and trim it:
				$current = trim((string) $item);
			}

			// If the current entry is not empty:
			if(! empty($current)) {
				// Then, we add the item to the final collection
				array_push($temp, $current);
			}
		}
	}

	// We prepare the output string:
	$out = '';

	// We prepare some working variables:
	$n = count($temp);
	$i = 0;

	// For each of the items in the final collection
	foreach($temp as $item) {
		// If this is not the first item we are going to append to the output
		// string, then we will need to add glue first:
		if($i ++ > 0) {
			// The glue may be the "comma glue" or the "and glue". The "and glue"
			// is prepended to the last item in the list. So, we check whether
			// or not this is the last item:
			if($i == $n) {
				// Yes, then add the "and glue". Note that we also check
				// whether or not the glue should be translated:
				$out .= $tAndGlue ? t($andGlue) : $andGlue;
			}
			// This is not the last item in the list:
			else {
				// Then add the "comma glue". Note that we also check whether or
				// not the glue should be translated:
				$out .= $tCommaGlue ? t($commaGlue) : $commaGlue;
			}
		}

		// Add the current item to the list:
		$out .= $item;
	}

	// Return final result:
	return M_smarty_plugin_result($out, $params, $smarty);
}