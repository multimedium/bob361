<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty thumbnail plugin
 *
 * - Type:     function
 * - Purpose:  Add a thumbnail to your template using a {@link Image}
 * 
 * This function can be used in a smarty template, to add a thumbnail to the page.
 * 
 * Params:
 * 	image Media
 * 	thumb string Thumbnail-definition used to display image
 * 	href string Thumbnail-definition used to link too
 * 	class string Href class-name (optional), default value is "media"
 * 	related string Href rel-attribute (optional)
 * 
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_thumbnail($params, $smarty) {
	// Check if an image has been provided:
	if(isset($params['image']) && M_Helper::isInstanceOf($params['image'] , 'Media')) {
		/* @var $image Image */
		$image = $params['image'];
		//use the parameters to determine the definitions we want to use, or
		//use default values if they are not provided
		$hrefDef = M_Helper::getArrayElement('href', $params, 'full');
		$thumbDef = M_Helper::getArrayElement('thumb', $params, 'thumb');
		$hrefClass = M_Helper::getArrayElement('class', $params, 'media');
		$related = M_Helper::getArrayElement('rel', $params, '');
		
		// Add the file:
		$title = '';
		if ($image->getTitle() != '') {
			$text = new M_Text($image->getTitle());
			$title = $text->toHtmlAttribute();
		}

		$baseName = $image->getBasenameFromTitle();
		
		$string = '<a class="'.$hrefClass.'" title="'.$title.'" href="'.M_Request::getLink('thumbnail').'/'.$hrefDef.'/'.$image->getId().'/'.$baseName.'" rel="'.$related.'">';
		$string .=' <img src="'.M_Request::getLink('thumbnail').'/'.$thumbDef.'/'.$image->getId().'/'.$baseName.'" alt="'.$title.'"/>';
		$string .= '</a>';
	}
	//cannot add image
	else $string = '';
	
	return M_smarty_plugin_result($string, $params, $smarty);
}