<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty att modifier plugin
 *
 * Type:     modifier<br>
 * Name:     att<br>
 * Purpose:  Prepare a text for use in a HTML attribute, such as alt=""
 * @author Tom Bauwens
 * @param string
 * @param integer
 * @param string
 * @return string
 */
function smarty_modifier_att($string) {
    // Create chain of filters, to remove HTML
    if ($string != "") {
		$text = new M_Text((string) $string);
		// Remove quotes and other special characters that do not belong in HTML attribute value:
		return $text->toHtmlAttribute();
	}

	return $string;
}