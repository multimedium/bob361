<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty akey plugin
 *
 * - Type:     function
 * - Name:     fetch
 * - Purpose:  Get a mapper to fetch Data Objects
 * 
 * {mapper object="Media" module="media" assignto="mapper"}
 * {foreach $mapper->addFilterObject($product)->getAll() as $media}
 * 
 * {/foreach}
 * 
 * @example {mapper object="Product" module="product"}
 * @example {mapper object="Product" module="product" assignto="mapper"}
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_mapper($params, $smarty) {
	// The parameters 'module' and 'object' are required. If these parameters
	// are not provided
	if(! array_key_exists('object', $params) || ! array_key_exists('module', $params)) {
		// We consider this a bad/corrupted call. We throw an exception to
		// inform about the error:
		throw new M_Exception(sprintf(
			'Cannot retrieve Data Object Mapper; Missing parameter "object" ' . 
			'and/or "module"'
		));
	}
	
	// Return the Data Object Mapper:
	return M_smarty_plugin_result(
		M_Loader::getDataObjectMapper($params['object'], $params['module']), 
		$params, 
		$smarty
	);
}
