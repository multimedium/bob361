<?php
/**
 * Smarty plugin functions for M
 * 
 * @package Smarty/Core
 * @subpackage plugins
 */

/**
 * Assign/return
 * 
 * Used to assign the result of a function to the template, or return it as the
 * final result.
 * 
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 * @author Tom Bauwens
 */
function M_smarty_plugin_result($result, $params, $smarty) {
	$doAssign = (isset($params['assignto']) && !empty($params['assignto']));
	if($doAssign) {
		$smarty->assign((string) $params['assignto'], $result);
		return NULL;
	} else {
		return $result;
	}
}