<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty akey plugin
 *
 * - Type:     function
 * - Name:     akey
 * - Purpose:  Get the value of a key in a given array, if any. If the value
 *             does not exist, return a default value
 * 
 * @example {akey array=$array key=0 default="default value"}
 * @example {akey array=$array key=0 default="default value" assignto="keyValue"}
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_akey($params, $smarty) {
	// All required variables provided?
	$c = (isset($params['array']) && is_array($params['array']));
	$c = ($c && array_key_exists('key', $params));
	
	// Throw exception, if not provided
	if(! $c) {
		throw new M_ViewException(sprintf(
			'Cannot get array key; missing parameters; ' . 
			'{akey array=[array] key=[key][ default=[string]]}'
		));
	}
	
	// If array key does not exist:
	if(! array_key_exists($params['key'], $params['array'])) {
		// return default value
		if(isset($params['default'])) {
			return M_smarty_plugin_result($params['default'], $params, $smarty);
		} else {
			// Return empty string, if default value has not been provided
			return M_smarty_plugin_result('', $params, $smarty);
		}
	}
	
	// Return value of array key:
	return M_smarty_plugin_result($params['array'][$params['key']], $params, $smarty);
}