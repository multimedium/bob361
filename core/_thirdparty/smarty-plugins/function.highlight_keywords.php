<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty link plugin
 *
 * - Type:     function
 * - Name:     highlight_keywords
 * - Purpose:  Get the link (for a type of resource, for a module)
 * 
 * This function can be used in a smarty template, to highlight given keywords in
 * a string/text. For more info, read {@link M_Text::highlightKeywords()}
 * 
 * Example:
 * 
 * {highlight_keywords keywords="searchterm" in="{$text}"}
 * 
 * Alternately, you may also provide the format that is to be used for
 * highlighting. By default the format will be the one used by
 * {@link M_Text::highlightKeywords}, but you may specify your own:
 * 
 * Example:
 * 
 * {highlight_keywords keywords="searchterm" in="{$text}" format="<span class='highlighted'>@keyword</span>"}
 * 
 * By default only entire words will be highlighted, but if specified you may
 * choose to ignore punctuation, highlighting parts of strings as well:
 * 
 * Example:
 * 
 * {highlight_keywords keywords="searchterm" in="{$text}" ignorePunctuation=true}
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_highlight_keywords($params, $smarty) {
	// Make sure a text has been provided
	$isTextGiven = (isset($params['in']) && !empty($params['in']));
	
	// If no text has been provided, we throw an exception!
	if(! $isTextGiven) {
		throw new M_ViewException(sprintf(
			'Cannot highlight keywords in smarty template, with %s(); Missing text in which to highlight',
			__FUNCTION__
		));
	}
	
	// Make sure a keyword has been provided
	$isKeywordGiven = (isset($params['keywords']) && !empty($params['keywords']));
	
	// If no keyword has been provided, we throw an exception!
	if(! $isKeywordGiven) {
		throw new M_ViewException(sprintf(
			'Cannot highlight keywords in smarty template, with %s(); Missing keyword(s)',
			__FUNCTION__
		));
	}
	
	// If keywords have been provided, we split them up into separate terms:
	$keywords = array();
	foreach(M_Search::getSearchTerms($params['keywords']) as $term) {
		$keywords[] = $term['term'];
	}
	
	// Check if a format is given
	$format = null;
	if(isset($params['format']) && !empty($params['format'])) {
		$format = $params['format'];
	}
	
	// Check if we have to ignore punctuation, highlighting parts of words
	// as well as entire words
	$ignorePunctuation = false;
	if(isset($params['ignorePunctuation']) && M_Helper::isBooleanTrue($params['ignorePunctuation'])) {
		$ignorePunctuation = true;
	}
	
	// Highlight keywords:
	$text = new M_Text((string) $params['in']);
	$text->highlightKeywords($keywords, $format, $ignorePunctuation);
	
	// Return result:
	return M_smarty_plugin_result($text->getText(), $params, $smarty);
}