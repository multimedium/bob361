<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty javascript plugin
 *
 * - Type:     function
 * - Purpose:  Get javascript
 * 
 * This function can be used in a smarty template, to get the javascript code
 * that has been prepared with {@link M_ViewJavascript}
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_javascript($params, $smarty) {
	$js = M_ViewJavascript::getInstance();
	return M_smarty_plugin_result($js->toString(), $params, $smarty);
}