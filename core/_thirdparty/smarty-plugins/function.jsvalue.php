<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty jsvalue plugin
 *
 * - Type:     function
 * - Name:     jsvalue
 * - Purpose:  Render a valid variable syntax, in javascript
 * 
 * @example {jsvalue value=$array}
 * @example {jsvalue value=$string}
 * @author Tom Bauwens
 * @param array parameters
 * @param Smarty
 * @return string|null
 */
function smarty_function_jsvalue($params, $smarty) {
	// Throw exception, if not all variables provided
	if(! array_key_exists('value', $params)) {
		throw new M_ViewException(sprintf(
			'Cannot get javascript value; missing parameters; ' . 
			'{jsvalue value=[mixed] [singleQuotes=[boolean]]}'
		));
	}
	
	// Get javascript value
	$out = M_JsHelper::getValue(
		$params['value'], 
		array_key_exists('singleQuotes', $params)
			? M_Helper::isBooleanTrue($params['singleQuotes'])
			: TRUE
	);
	
	// Return the output
	return M_smarty_plugin_result($out, $params, $smarty);
}