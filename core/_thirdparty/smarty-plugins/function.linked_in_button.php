<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty LinkedIn Button Plugin
 *
 * - Type:     function
 * - Name:     linked_in_button
 * - Purpose:  Displaying the LinkedIn share button on the page
 * 
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters:
 *		- url: set to url to share
 *		- counter: "right", "top"
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_linked_in_button($params, $smarty) {
	$url = M_Helper::getArrayElement('url', $params);
	if(!$url) {
		$url = M_Request::getUriString();
	}

	$counter = M_Helper::getArrayElement('counter', $params);

	// Parse the JavaScript
	$out = '';
	$out .= '<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>';
	$out .= '<script type="IN/Share"';

	if($counter):
	$out .= ' data-counter="'.$counter.'"';
	endif;
	if($url):
	$out .= ' data-url="'.$url.'"';
	endif;
	$out .= '></script>';

	// Return result:
	return M_smarty_plugin_result($out, $params, $smarty);
}