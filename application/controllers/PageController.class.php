<?php
/**
 * PageController
 *
 * @package App
 */
class PageController extends M_Controller {

	/* -- PROPERTIES -- */
	
	/**
	 * Session Namespace
	 *
	 * This property stores the {@link M_SessionNamespace} object that can be
	 * requested with {@link PageController::_getSessionNamespace()}
	 *
	 * @access private
	 * @var M_SessionNamespace
	 */
	private $_sessionNamespace = NULL;
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return PageController
	 */
	public function __construct() {
		// Enable sanitized values
		M_Request::setSanitizeAllRequestVariables(true);
	}

	/* -- MI_CONTROLLER -- */

	/**
	 * Handle error
	 * 
	 * @access public
	 * @return void
	 * @todo: mail a message to bugs@multimedium.be
	 */
	public function handleError(Exception $e) {
		// Write information about the exception to the log
		M_Console::getInstance()->write(sprintf(
			'Exception %s thrown: "%s\n%s"',
			$e->getCode(),
			$e->getMessage(),
			$e->getTraceAsString()
		));

		// Then, we log the exception:
		try {
			$logger = M_Exception::getExceptionLogger();
			if($logger) {
				$logger->logException($e);
			}
		}
		// If we fail in doing so:
		catch(M_Exception $e) {
			// Then, report about the problem in the log files:
			M_Console
				::getInstance()
				->write(sprintf(
					'Cannot log exception in database with message "%s"',
					$e->getMessage()
				));
		}

		// Load and display the Error Page View:
		M_Loader::loadView('PageErrorView');
		$view = new PageErrorView();
		$view->setException($e);
		$view->display();
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * 404 Page
	 *
	 * When a page doesn't exist anymore, we can send a 404-HTTP-status-code
	 * and show the error page.
	 *
	 * @access protected
	 * @return void
	 */
	protected function _404() {
		$controller = new ErrorController();
		$controller->index();
		die();
	}

	/**
	 * Get Contact information (from Application Identity)
	 *
	 * @see M_ApplicationIdentity
	 * @uses M_ApplicationIdentity::getContact()
	 * @access protected
	 * @return M_Contact
	 */
	protected function _getContact() {
		return M_ApplicationIdentity
			::getInstance()
			->getContact();
	}

	/**
	 * Get Session Namespace
	 *
	 * Will provide with the session namespace in which persistent variables
	 * of the application controller(s) should be stored.
	 *
	 * @access protected
	 * @return M_SessionNamespace
	 */
	protected function _getSessionNamespace() {
		// If not requested before
		if($this->_sessionNamespace === NULL) {
			// Then, we construct the session namespace now:
			$this->_sessionNamespace = new M_SessionNamespace('AppController', TRUE);
		}

		// Return the namespace:
		return $this->_sessionNamespace;
	}

	/**
	 * Get pagejumper
	 *
	 * Will provide with a pagejumper that helps in dividing a list into different
	 * pages. Typically, this page jumper is added as a filter to a data object
	 * mapper, in order to only fetch the instances that are shown on the current
	 * page. Also, it will be provided to a view, so the page navigation can be
	 * rendered for display.
	 *
	 * Note that this method will remember about the active page, by storing the
	 * active page in the session. The session namespace is in turn provided by
	 * {@link PageController::_getSessionNamespace()}.
	 *
	 * @access protected
	 * @param integer $totalCount
	 *		The total number of listed items in the complete list, throughout
	 *		all pages of the page jumper
	 * @param integer $itemsPerPage
	 *		The number of items that is to be shown in each page of the list.
	 * @return M_Pagejumper
	 */
	protected function _getPagejumper($totalCount, $itemsPerPage = 10) {
		// Construct the page jumper:
		$pj = new M_Pagejumper((int) $itemsPerPage, (int) $totalCount);

		// We get the session namespace, in which we store the page jumper(s):
		$pagejumpers = $this->_getSessionNamespace()->get('pagejumper', array());

		// Of course, many page jumpers may exist in the current application, and
		// each list will need to store a different and independent active page
		// number. So, in order to store the active page number, we construct a
		// Unique ID:
		$tmp = debug_backtrace();
		$id  = $tmp[1]['class'] . '::' . $tmp[1]['function'];

		// If the page jumper is at page 1, it means that no other page number
		// has been specified via the request variables:
		if($pj->getPage() == 1 && ! M_Request::getVariable('page', FALSE, M_Request::TYPE_INT)) {
			// In that case, we check if an active page has been set previously
			// for this specific page jumper:
			if(isset($pagejumpers[$id])) {
				// Yes, we have found a stored value. Of course, this value, the
				// active page number, should be greater than 1 but should not be
				// greater than the total number of pages available in the page
				// jumper:
				if($pagejumpers[$id] > 1 && $pagejumpers[$id] <= $pj->getTotalCountOfPages()) {
					// If so, then we set the active page for the page jumper:
					$pj->setPage($pagejumpers[$id]);
				}
			}
		}

		// Now, we store the new active page in the collection of page jumpers:
		$pagejumpers[$id] = $pj->getPage();

		// And, for persistence throughout the pages of the application, we store
		// the active page in the session:
		$this->_getSessionNamespace()->pagejumper = $pagejumpers;

		// Now, we return the page jumper:
		return $pj;
	}

	/**
	 * Output RSS Feed
	 *
	 * Is used by subclasses, to output an RSS Feed. Will set some basic properties
	 * of the RSS Feed...
	 *
	 * @access protected
	 * @param M_FeedRss $rss
	 * @return void
	 */
	protected function _rss(M_FeedRss $rss, $title, $description, $urlSuffix) {
		// Title of RSS Feed:
		$rss->setTitle($this->_getContact()->getName() . ': ' . $title);

		// Set the description of the feed:
		$rss->setDescription($description);

		// Set the link of the RSS feed
		$rss->setLink(
			new M_Uri(
				M_Request::getLink($urlSuffix)
			)
		);
		
		// Flush (Output) the feed
		M_Header::send(M_Header::CONTENT_TYPE_XML);
		echo $rss->toString();
	}
}