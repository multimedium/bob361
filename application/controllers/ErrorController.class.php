<?php
/**
 * ErrorController
 *
 * @package App
 */
class ErrorController extends M_Controller {
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Error 404
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		// send 404 HTTP status
		M_Header::send404();

		// Load the Page404View, and display:
		M_Loader
			::getView('Page404View')
			->setPageTitle(t('Pagina niet gevonden'))
			->setPageDescription(t('De pagina die u zoekt bestaat niet (meer) of is mogelijk verplaatst.'))
			->display();

		// Stop running the app
		die();
	}
}