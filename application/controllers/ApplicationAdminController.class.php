<?php
/**
 * ApplicationAdminController
 * 
 * A superclass all admin side controllers extend from, containing general
 * functionality
 * 
 * @package App
 */
class ApplicationAdminController extends M_Controller {
	
	/* -- PRIVATE / PROTECTED -- */
	
	/**
	 * AJAX Success
	 *
	 * Will perform the default action when an AJAX request has been completed
	 * successfully, without having to echo a value.
	 *
	 * @access protected
	 * @return void
	 */
	protected function _ajaxSuccess() {
		echo '1';
		die();
	}
	
	/**
	 * AJAX Error
	 *
	 * Will perform the default action when an AJAX request is to stop
	 * processing, which is returning an empty string and dying on the spot
	 *
	 * @access protected
	 * @return void
	 */
	protected function _ajaxError() {
		echo '';
		die();
	}
}