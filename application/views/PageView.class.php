<?php
/**
 * PageView
 *
 * @abstract
 * @package App
 */
abstract class PageView extends M_ViewHtml {

	/* -- CONSTANTS -- */

	/**
	 * View mode
	 *
	 * This constant can be used to address a view mode. Typically, this method
	 * is used as an argument to {@link PageView::setViewMode()}
	 *
	 * @access public
	 * @var string
	 */
	const VIEW_MODE_IFRAME = 'iframe';

	/**
	 * View mode
	 *
	 * This constant can be used to address a view mode. Typically, this method
	 * is used as an argument to {@link PageView::setViewMode()}
	 *
	 * @access public
	 * @var string
	 */
	const VIEW_MODE_AJAX_INLINE = 'ajax-inline';

	/**
	 * (Default) View mode
	 *
	 * This constant can be used to address a view mode. Typically, this method
	 * is used as an argument to {@link PageView::setViewMode()}
	 *
	 * @access public
	 * @var string
	 */
	const VIEW_MODE_NORMAL = 'normal';

	/* -- PROPERTIES -- */

	/**
	 * SEO Entry
	 *
	 * @see PageView::_getSeo()
	 * @access private
	 * @var string
	 */
	private $_seo = 0;

	/**
	 * Page title
	 *
	 * @access private
	 * @var string
	 */
	private $_pageTitle;

	/**
	 * Page Description (META-DATA)
	 *
	 * @access private
	 * @var string
	 */
	private $_pageDescription;

	/**
	 * Wrapper Template
	 *
	 * @see PageView::_getResourceWrapper()
	 * @access private
	 * @var M_ViewHtmlResource
	 */
	private $_resourceWrapper;

	/**
	 * Active path
	 *
	 * @see getActivePath()
	 * @access private
	 * @var string
	 */
	private $_activePath;

	/**
	 * Breadcrumb
	 *
	 * @access private
	 * @var array
	 */
	private $_breadcrumb = array();

	/**
	 * Canonical URI
	 *
	 * @see PageView::getCanonicalUri()
	 * @access private
	 * @var M_Uri
	 */
	protected $_canonicalUri;

	/**
	 * View mode
	 *
	 * Stores the view mode of the {@link PageView} object.
	 *
	 * @access private
	 * @var string
	 */
	private $_viewMode;
	
	/**
	 * Menu
	 * 
	 * @var M_Menu
	 */
	private $_menu;
	
	/**
	 * Og Image Url
	 *
	 * @var string 
	 */
	protected $_ogImageUrl;

	/* -- SETTERS -- */

	/**
	 * Set the page description
	 *
	 * Note that {@link PageView::_getMetaDataString()} will be used to clean the
	 * provided page description. The maximum length of a page description is
	 * 200 characters.
	 *
	 * @uses PageView::_getMetaDataString()
	 * @access public
	 * @param string $pageDescription
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPageDescription($pageDescription) {
		// Set the description
		$this->_pageDescription = $this->_getMetaDataString($pageDescription, 200);

		// Return myself...
		return $this;
	}

	/**
	 * Set the page title
	 *
	 * Note that {@link PageView::_getMetaDataString()} will be used to clean the
	 * provided page title. The maximum length of a page title is
	 * 60 characters.
	 *
	 * @uses PageView::_getMetaDataString()
	 * @access public
	 * @param string $pageTitle
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPageTitle($pageTitle) {
		// Set the title
		$this->_pageTitle = $this->_getMetaDataString($pageTitle, 60);

		// Return myself...
		return $this;
	}

	/**
	 * Set the active path
	 *
	 * @param string $arg
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setActivePath($arg) {
		$this->_activePath = (string) $arg;
		return $this;
	}
	
	/**
	 * set the og image url
	 * 
	 * @param string $url
	 * @return  PageView $view
	 */
	public function setOgImageUrl($url) {
		$this->_ogImageUrl = $url;
		return $this;
	}

	/**
	 * Add an entry to the breadcrumb
	 *
	 * @access public
	 * @param string $title
	 *		The title, for display, of the breadcrumb entry
	 * @param string|M_Uri $uri
	 *		The (relative) URI to the page
	 * @param string $description
	 *		The description of the breadcrumb entry. If provided, this description
	 *		will be used as the title attribute of the link in the breadcrumb
	 *		interface.
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function addBreadcrumbEntry($title, $uri = NULL, $description = NULL) {
		// Add the entry
		$this->_breadcrumb[] = array(
			// The label
			(string) $title,
			// The path, including query variables. Note that we always make an
			// absolute link out of the provided path/uri:
			M_Helper::isInstanceOf($uri, 'M_Uri')
				? $uri->toString()
				: ($uri
					// Is the provided link absolute already?
					? substr((string) $uri, 0, 7) == 'http://'
						? (string) $uri
						: M_Request::getLink((string) $uri)
					: NULL),
			// The description
			(string) $description
		);

		// Return myself:
		return $this;
	}

	/**
	 * Set Canonical URI
	 *
	 * @see PageView::_getCanonicalUri()
	 * @param M_Uri $uri
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCanonicalUri(M_Uri $uri) {
		$this->_canonicalUri = $uri;
		return $this;
	}

	/**
	 * Set View Mode
	 *
	 * @see PageView::VIEW_MODE_IFRAME
	 * @see PageView::VIEW_MODE_AJAX_INLINE
	 * @see PageView::VIEW_MODE_NORMAL
	 * @access public
	 * @param string $viewMode
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setViewMode($viewMode) {
		// Make sure that the view mode is supported:
		if(! in_array($viewMode, array(self::VIEW_MODE_NORMAL, self::VIEW_MODE_IFRAME, self::VIEW_MODE_AJAX_INLINE))) {
			// If not, we throw an exception to inform about the error
			throw new M_Exception(sprintf(
				'The view mode "%s" is not supported by %s',
				$viewMode,
				__CLASS__
			));
		}

		// Set the view mode:
		$this->_viewMode = (string) $viewMode;

		// Returns itself;
		return $this;
	}

	/* -- GETTERS -- */

	/**
	 * Get the page description
	 *
	 * @access public
	 * @return string
	 */
	public function getPageDescription() {
		// Get SEO Entry
		$seo = $this->_getSeo();

		// Return the page description:
		return $seo ? $seo->getDescription() : $this->_pageDescription;
	}

	/**
	 * Get the pagetitle
	 *
	 * @return string
	 */
	public function getPageTitle() {
		// Get SEO Entry
		$seo = $this->_getSeo();

		// Return the page title:
		return $seo ? $seo->getTitle() : $this->_pageTitle;
	}

	/**
	 * Get the active path
	 *
	 * Note: if no path has been set, this method will ask and return the
	 * path from {@link M_ControllerDispatcher}
	 *
	 * @return string
	 */
	public function getActivePath() {
		if(! $this->_activePath) {
			$this->_activePath = M_ControllerDispatcher::getInstance()->getPath();
		}
		return $this->_activePath;
	}

	/**
	 * Get the canonical uri
	 *
	 * When multiple pages hold the same content, you can set use a unique
	 * canonical uri: this way robots such as Google know both pages are actually
	 * the same.
	 *
	 * By doing this you will not be penalized for serving duplicate content
	 *
	 * Canonical urls can be used for e.g. when using a pagejumper or other
	 * query variables (sorting, filters). Also http://www.url.be and http://www.url.be/home
	 * could both use one canonical url (http://www.url.be)
	 *
	 * If no {@link M_Uri} is set through {@link PageController::__setCanonicalUrl()}
	 * the active uri without query variables will be used.
	 *
	 * To use the canonical-url in your template you can use the following syntax:
	 *
	 * <link rel="canonical" href="http://www.url.be/path/to/page" />
	 *
	 * @see http://googlewebmastercentral.blogspot.com/2009/02/specify-your-canonical.html
	 * @author Ben Brughmans
	 * @return M_Uri|NULL
	 */
	public function getCanonicalUri() {
		// If a Canonical URI has not been provided to the view:
		if(! $this->_canonicalUri) {
			// Then, we will return the active URI, but without any query variables.
			// That is, of course, if query variables are present:
			$uri = M_Request::getUri();

			// Does the URI have query variables?
			if($uri->hasQueryVariables()) {
				// If so, we remove the query variables:
				$uri->removeQueryVariables();

				// And use the resulting URI as the canonical one:
				$this->_canonicalUri = $uri;
			}
		}

		// Return the Canonical URI:
		return $this->_canonicalUri;
	}

	/**
	 * Get View Mode
	 *
	 * Will provide with the view mode that has been defined for this view
	 * previously with {@link PageView}. Note that, if no view mode has been
	 * specificed to this object explicitely, this method will listen to the
	 * request variable with name 'viewmode'. Depending on the value of that
	 * variable, the view will be set to a given mode:
	 *
	 * <code>'iframe'</code>
	 *
	 * If the variable carries this value, then the view mode will be set to
	 * {@link PageView::VIEW_MODE_IFRAME}
	 *
	 * <code>'ajax-inline'</code>
	 *
	 * If the variable carries any of these value, then the view mode will be
	 * set to {@link PageView::VIEW_MODE_AJAX_INLINE}
	 *
	 * <code>'normal'</code>
	 *
	 * If the variable carries any of these value, then the view mode will be
	 * set to {@link PageView::VIEW_MODE_NORMAL}
	 *
	 * @access public
	 * @return string $viewMode
	 */
	public function getViewMode() {
		// If no view mode has been set
		if(! $this->_viewMode) {
			// Then, we define it now:
			$this->setViewMode(M_Request::getVariable('viewmode', self::VIEW_MODE_NORMAL));
		}

		// Return the view mode:
		return $this->_viewMode;
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Render HTML
	 *
	 * @see M_ViewHtml::_render()
	 * @return string
	 */
	protected function _render() {
		// Get the wrapper
		$rs = $this->_getResourceWrapper();
		
		// System: Browser info
		$this->assignPersistent('browser', array(
			'id'      => M_Browser::getName() . M_Browser::getMajorVersion(),
			'name'    => M_Browser::getName(),
			'version' => array(
				'major' => M_Browser::getMajorVersion(),
				'minor' => M_Browser::getMinorVersion()
			)
		));
		
		$rs->assign('sidebarNews',       $this->_getSidebarNews());
		$rs->assign('sidebarProjects',   $this->_getSidebarProjects());
		
		// System: Persistent variables
		$rs->assign('persistent',      self::getPersistentVariables());

		// The render of the to-be-wrapped template (note that we do this
		// after assigning persistent variables, so the persistents are also
		// available in the wrapped template. Secondary, also note that we
		// do this before assigning the breadcrumb etc, to allow the views
		// to manipulate the variables below, as they are assigned to the
		// resource wrapper)
		$rs->assign('content',         parent::_render());
		
		// Assign presentation variables to the wrapper:
		$rs->assign('pageTitle',       $this->getPageTitle());
		$rs->assign('pageDescription', $this->getPageDescription());
		$rs->assign('activePath',      $this->getActivePath());
		$rs->assign('identity',        M_ApplicationIdentity::getInstance()->getContact());
		$rs->assign('canonicalUri',    $this->getCanonicalUri());
		$rs->assign('breadcrumb',      $this->_breadcrumb);
		$rs->assign('menu',			   $this->_getMenu());
		$rs->assign('ogImageUrl',	   $this->_ogImageUrl);

		// System: Languages
		$rs->assign('languages', M_LocaleMessageCatalog::getInstalled());

		// System: Pending messages
		$messenger = M_Messenger::getInstance();
		$rs->assign('messages', $messenger->getMessages());
		$messenger->clearMessages();

		// Return the render of the wrapper template
		return $rs->fetch();
	}

	/**
	 * Get Sidebar News
	 * 
	 * @access protected
	 * @return M_ArrayIterator || NULL
	 */
	protected function _getSidebarNews() {
		$mapper = M_Loader::getDataObjectMapper('News', 'news');
		/* @var $mapper NewsMapper */
		$news = $mapper
			->addFilterPublishedOnly()
			->addFilterHighlight()
			->addFilterSortByDate('DESC')
			->addFilter(new M_DbQueryFilterLimit(0, 6))
			->getAll();

		return $news;
	}

    /**
     * Get Sidebar Projects
     *
     * @access protected
     * @return M_ArrayIterator || NULL
     */
    protected function _getSidebarProjects() {
        $mapper = M_Loader::getDataObjectMapper('Project', 'portfolio');
        /* @var $mapper ProjectMapper */
        $projects = $mapper
            ->addFilterPublishedOnly()
            ->addFilterHighlight()
            ->addFilter(new M_DbQueryFilterLimit(0, 6))
            ->getAll();

        return $projects;
    }
	
	/**
	 * Get {@link Seo}
	 *
	 * Will fetch the {@link Seo} Data Object that has been created for the
	 * specific request. Will use {@link M_Request::getUri()} to do so...
	 *
	 * @access protected
	 * @return Seo
	 */
	protected function _getSeo() {
		// If not requested before:
		if($this->_seo === 0) {
			// If the SEO Module has been installed in the application:
			if(M_Application::isModuleInstalled('seo')) {
				// Get the URI with which to look up the SEO entry:
				$path = M_Helper::trimCharlist(
					M_Request::getUri()->getRelativePath(),
					'/'
				);

				// If the URI is empty
				if(! $path) {
					// We get the path to the homepage:
					$uri = M_ControllerDispatcher
						::getInstance()
						->getHomepageUri();

					// If a homepage URI is available
					if($uri) {
						// Then, we default to that URI:
						$path = $uri->getRelativePath();
					}
				}

				// Prepare the mapper with which we look up SEO entries:
				$mapper = M_Loader::getDataObjectMapper('Seo', 'seo');

				/* @var $mapper SeoMapper */
				// Ask the mapper for the SEO entry;
				$this->_seo = $mapper
					->getByField('url', $path)
					->current();
			}
		}

		// Return the SEO Entry
		return $this->_seo;
	}

	/**
	 * Get meta-data string
	 *
	 * Will treat the provided text, so it can be used as meta-data in the view.
	 * This method is used mainly by
	 *
	 * - {@link PageView::setPageDescription()}
	 * - {@link PageView::setPageTitle()}
	 *
	 * More specifically, this method does the following:
	 *
	 * - Filter out all HTML from the provided string
	 * - Extract a snippet from the string, in order to respect the maximum length
	 *
	 * @access public
	 * @param string $text
	 * @param string $mexlength
	 * @return string
	 */
	protected function _getMetaDataString($text, $maxlength) {
		// Construct a text object with the provided description:
		$text = new M_Text((string) $text);

		// Extract a snippet (in the meanwhile filtering all HTML)
		return $text->getSnippet($maxlength);
	}

	/**
	 * Get wrapper resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function _getResourceWrapper() {
		// If not requested before:
		if(is_null($this->_resourceWrapper)) {
			// Then, construct the wrapper now. Note that the wrapper of the
			// view depends on the view mode:
			switch($this->getViewMode()) {
				// Iframe
				case self::VIEW_MODE_IFRAME:
					$this->_resourceWrapper = M_ViewHtmlResource::constructWithTemplateBasePath('PageIframe.tpl');
					break;

				// Inline view, probably loaded by AJAX
				case self::VIEW_MODE_AJAX_INLINE:
					$this->_resourceWrapper = M_ViewHtmlResource::constructWithTemplateBasePath('PageAjaxInline.tpl');
					break;

				// Normal view (default):
				case self::VIEW_MODE_NORMAL:
				default:
					$this->_resourceWrapper = M_ViewHtmlResource::constructWithTemplateBasePath('Page.tpl');
					break;
			}
		}

		// Return the wrapper template:
		return $this->_resourceWrapper;
	}
	
	/**
	 * get the main menu
	 * 
	 * @return M_Menu
	 */
	protected function _getMenu() {
		if (! $this->_menu) {
		

			// we construct a menu
			$menu = M_Menu::getInstance('main-menu');

			// add the items
			// projects
			$projects = new M_MenuItem();
			$projects->setPath('projects');
			$projects->setTitle(t('Projects'));
			$projects->setDescription(t('An overview of our recent projects'));
			$menu->addMenuItem($projects);
			// publications
			$publications = new M_MenuItem();
			$publications->setPath('publications');
			$publications->setTitle(t('Publications'));
			$publications->setDescription(t('An overview of our recent publications'));
			$menu->addMenuItem($publications);
			// awards
			$awards = new M_MenuItem();
			$awards->setPath('awards');
			$awards->setTitle(t('Awards'));
			$awards->setDescription(t('An overview of the awards'));
			$menu->addMenuItem($awards);
			// office
			$office = new M_MenuItem();
			$office->setPath('office');
			$office->setTitle(t('office'));
			$office->setDescription(t('About BOB361'));
			$menu->addMenuItem($office);
			// jobs
			$jobs = new M_MenuItem();
			$jobs->setPath('jobs');
			$jobs->setTitle(t('Jobs'));
			$jobs->setDescription(t('An overview of our jobs'));
			$menu->addMenuItem($jobs);
			// contact
			$contact = new M_MenuItem();
			$contact->setPath('contact');
			$contact->setTitle(t('Contact'));
			$contact->setDescription(t('Contact us'));
			$menu->addMenuItem($contact);
			
			// store in the property
			$this->_menu = $menu;
		}
		
		// return the menu from the property
		return $this->_menu;
	}
}