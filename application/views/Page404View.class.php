<?php
/**
 * Page404View
 * 
 * @package App
 */
class Page404View extends PageView {
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get Resource 
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath().'/Page404.tpl');
	}
}