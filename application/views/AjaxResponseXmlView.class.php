<?php
/**
 * AjaxResponseXmlView class
 * 
 * @abstract
 * @package App
 */

// Load superclass
M_Loader::loadView('PageView');

/**
 * AjaxResponseXmlView class
 * 
 * @abstract
 * @package App
 */
abstract class AjaxResponseXmlView extends PageView {

	/* -- GETTERS -- */

	/**
	 * Display
	 *
	 * @uses M_Header::send()
	 * @uses M_Header::CONTENT_TYPE_XML
	 * @see M_ViewHtml::display()
	 * @access public
	 * @return void
	 */
	public function display() {
		// Send the XML headers
		M_Header::send(M_Header::CONTENT_TYPE_XML);
		
		// Display
		parent::display();
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get wrapper resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function _getResourceWrapper() {
		return M_ViewHtmlResource::constructWithTemplateBasePath('AjaxResponseXml.tpl');
	}
}