<?php
/**
 * MailView
 *
 * @abstract
 * @package App
 */
abstract class MailView extends M_ViewHtml {

	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Render HTML
	 *
	 * @see M_ViewHtml::_render()
	 * @return string
	 */
	protected function _render() {
		// Get the wrapper
		$rs = $this->_getResourceWrapper();
		
		// System: Browser info
		$this->assignPersistent('browser', array(
			'id'      => M_Browser::getName() . M_Browser::getMajorVersion(),
			'name'    => M_Browser::getName(),
			'version' => array(
				'major' => M_Browser::getMajorVersion(),
				'minor' => M_Browser::getMinorVersion()
			)
		));
		
		// System: Persistent variables
		$rs->assign('persistent',      self::getPersistentVariables());

		// The render of the to-be-wrapped template (note that we do this
		// after assigning persistent variables, so the persistents are also
		// available in the wrapped template. Secondary, also note that we
		// do this before assigning the breadcrumb etc, to allow the views
		// to manipulate the variables below, as they are assigned to the
		// resource wrapper)
		$rs->assign('content',         parent::_render());
		
		// Assign presentation variables to the wrapper:
		$rs->assign('identity',        M_ApplicationIdentity::getInstance()->getContact());

		// System: Languages
		$rs->assign('languages', M_LocaleMessageCatalog::getInstalled());

		// Return the render of the wrapper template
		return $rs->fetch();
	}

	/**
	 * Get wrapper resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function _getResourceWrapper() {
		return M_ViewHtmlResource::constructWithTemplateBasePath('Mail.tpl');
	}
}