<?php
/**
 * PageErrorView
 * 
 * @package App
 */
M_Loader::loadView('PageView');
class PageErrorView extends PageView {
	
	/* -- SETERS -- */
    /**
     * Set exception
     * 
	 * @access public
     * @param Exception $e
	 * @return void
     */
    public function setException(Exception $e) {
		$this->assign('exception', $e);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get Resource
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath().'/PageError.tpl');
	}
}