// Create the Google map with the location
var mapBxl, mapParis;
var latLngBxl = new google.maps.LatLng(50.844331, 4.3386615);
var latLngParis = new google.maps.LatLng(48.8703768, 2.3924685);
var draggable = true;

/* -- Detect if user uses a mobile device -- */
	
function detectMobile() { 
	if( navigator.userAgent.match(/Android/i)
	|| navigator.userAgent.match(/webOS/i)
	|| navigator.userAgent.match(/iPhone/i)
	|| navigator.userAgent.match(/iPad/i)
	|| navigator.userAgent.match(/iPod/i)
	|| navigator.userAgent.match(/BlackBerry/i)
	|| navigator.userAgent.match(/Windows Phone/i)
	|| window.innerWidth <= 800 && window.innerHeight <= 600
		) {
			return true;
		}
		else {
			return false;
	}
}

if(detectMobile()) {
	draggable = false;
}
else {
	draggable = true;
}

function initializeGoogleMaps() {
	var mapOptionsBxl = {
		zoom: 15,
		center: latLngBxl,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false,
		draggable: draggable,
		disableDefaultUI: true
	}
	
	var mapOptionsParis = {
		zoom: 15,
		center: latLngParis,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false,
		draggable: draggable,
		disableDefaultUI: true
	}

	mapBxl = new google.maps.Map(document.getElementById('map-bxl'), mapOptionsBxl);
	mapParis = new google.maps.Map(document.getElementById('map-paris'), mapOptionsParis);

	var markerBxl = new google.maps.Marker({
		position: latLngBxl,
		map: mapBxl,
		title: 'BOB361 Brussels',
		animation: google.maps.Animation.DROP
	});
	
	var markerParis = new google.maps.Marker({
		position: latLngParis,
		map: mapParis,
		title: 'BOB361 Paris',
		animation: google.maps.Animation.DROP
	});
}



 /* when the document is ready */
$(document).ready(function() {
	initializeGoogleMaps();
})
