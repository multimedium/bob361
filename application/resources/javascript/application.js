// When the DOM is ready
$('document').ready(function () {

    // Fancybox
    $('a.fancybox').fancybox({
        openSpeed: 'fast',
        closeSpeed: 'fast',
        prevSpeed: 'normal',
        nextSpeed: 'normal',
        prevEffect: 'fade',
        nextEffect: 'fade',
        closeClick: false,
        helpers: {
            title: {
                type: 'over'
            }
        },
        padding: 5
    });

    //bxslider
    $('.verticle_slider').bxSlider({
        mode: 'vertical',
        minSlides: '2',
        moveSlides: '1',
        pager: false,
        nextText: '  ',
        prevText: '  ',
        nextSelector: $('#arrow_up'),
        prevSelector: $('#arrow_down'),

    });

    /* -- Autocomplete -- */

    if ($('#search').length > 0) {
        var engine = new Bloodhound({
            name: 'search',
            remote: $('#search').attr('data-search-href') + '?q=%QUERY',
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.val);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit: Number.MAX_VALUE
        });
        engine.initialize();

        $('#search').typeahead({
                minLength: 1,
                highlight: true
            },
            {
                name: 'my-dataset',
                source: engine.ttAdapter(),
                templates: {
                    empty: [
                        $('#tt-empty').html()
                    ].join('\n'),
                    suggestion: Handlebars.compile($('#tt-element').html()),
                    footer: Handlebars.compile($('#tt-footer').html())
                }
            });

        // When an item is selected, redirect to it:
        $('#search').on('typeahead:selected', function (event, suggestion, dataset) {
            window.location = suggestion.href;
        });
    }

    /* -- Lazy load images -- */
    $(function () {
        $("img.lazy").lazyload();
    });

    /* -- Project Slider -- */

    $('.project-slider').flexslider({
        animation: 'slide',
        directionNav: false,
        pausePlay: true
    });

    $('.project-slider').flexslider({
        directionNav: false
    }).flexsliderManualDirectionControls();


    /* -- Responsive Navigation -- */

    $('#nav-toggle').click(function (e) {
        e.preventDefault();
        $('#menu ul').slideToggle();
        return false;
    });
    $(window).resize(function () {
        if (window.innerWidth > 992) {
            $('#menu ul').show();
        } else {
            $('#menu ul').hide();
        }
    });

    /* -- Search button -- */

    if (window.innerWidth > 992) {
        $('#search-button').click(function (event) {
            menu2();
            event.preventDefault();
        });
        if (handler === 'closed') {
            $('#search-form input[type="search"]').css("visibility", "hidden");
            $('#search-button').css("background-color", "transparent");
        }

    }
    else {
        $('#search-button').off('click');
        $('#search-form input[type="search"]').css("visibility", "visible");
    }

    $(window).resize(function () {
        $('#search-button').off('click');
        if (window.innerWidth > 992) {
            $('#search-button').click(function (event) {
                menu2();
                event.preventDefault();
            });
            if (handler === 'closed') {
                $('#search-form input[type="search"]').css("visibility", "hidden");
                $('#search-button').css("background-color", "transparent");
            }
        }
        else {
            $('#search-form input[type="search"]').css("visibility", "visible");
        }
    });
});
/*
 function menu() {
 $('.search-container').mouseover(function () {
 $('#search-form input[type="search"]').css("visibility", "visible");
 })
 .mouseout(function () {
 if (!$('#search').is(":focus")) {
 $('#search-form input[type="search"]').css("visibility", "hidden");
 }

 });

 $('#search').blur(function () {
 $('.search-container').removeClass("focus-container");
 $('#search-form input[type="search"]').css("visibility", "hidden");
 })
 .focus(function () {
 $('.search-container').addClass("focus-container");
 $('#search-form input[type="search"]').css("visibility", "visible");
 });
 }
 */

var handler = 'closed';

function menu2() {
    var eInput = $('#search');
    if (handler === 'closed') {
        $('#search-form input[type="search"]').css("visibility", "visible");
        $('.search-container').addClass("search-onclick");
        setTimeout(function () {
            $('.search-container').css("overflow", "visible")
        }, 500);


        handler = 'open';
    }
    else {
        if (eInput.val() != "") {
            $('#search-form').submit();
            return false;
        }
        $('.search-container').removeClass("search-onclick").css("overflow", "hidden");
        $('#search-form input[type="search"]').css("visibility", "hidden");

        handler = 'closed';
    }
}