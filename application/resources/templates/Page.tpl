<!DOCTYPE html>
{locale category="LANG" assignto="lang"}
<!--[if lt IE 7 ]> <html lang="{$lang}" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="{$lang}" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="{$lang}" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="{$lang}" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="{$lang}" class="no-js"> <!--<![endif]-->

	<head>
		{* Disable IE compatibility mode *}
		{if $persistent.browser.name == 'ie'}
			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		{/if}

		{* Define the charset - Not specifying an encoding can lead to security vulnerabilities. *}
		<meta charset="utf-8" />

		{* Define the viewport *}
		<meta name="viewport" content="width=device-width">

		{* Page Title *}
		<title>{$pageTitle|cat:" &middot; BOB361"}</title>

		{* Meta data *}
		<meta name="description" content="{snippet from=$pageDescription length=155}" />

		{* Facebook *}
		<meta property="og:title" content="{$pageTitle|cat:"BOB361"}" />
		<meta property="og:description" content="{snippet from=$pageDescription length=155}" />
		<meta property="og:type" content="article" />
		{if $ogImageUrl}
		 <meta property="og:image" content="{$ogImageUrl}" /> 
		{/if}
		{* Please see http://developers.facebook.com/docs/opengraph/#types *}

		{* Favicon *}
		<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
		<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="theme-color" content="#ffffff">

		{* CSS *}
		<link rel="stylesheet" href="{link type="css" href="application.css"}" media="screen" />

		{* Additional CSS, to be loaded for this specific view *}
		{css}
		<link rel="stylesheet" href="{link type="css" href="jquery.bxslider.css"}" media="screen" />
		
		{* Javascript files *}
		<script src="{link href="modernizr.min.js" type="javascript"}"></script>

		{* Google Analytics *}
		{google_analytics ua="UA-72931204-1"}

		{* If a Canonical URI applies to this view: *}
		{if $canonicalUri}
			{* Then, include the link to the Canonical URI *}
			<link rel="canonical" href="{$canonicalUri}" />
		{/if}

		{* RSS *}
		<link rel="alternate" type="application/rss+xml" title="{t text="RSS @title" title="Jobs"}" href="{link href="rss/jobs"}" />
		<link rel="alternate" type="application/rss+xml" title="{t text="RSS @title" title="Publicaties"}" href="{link href="rss/publications"}" />
	</head>
	<body>

		{* Sidebar *}
		<div id="sidebar">

			{* Button *}
			<div id="sidebar-button">
				highlights
			</div>
			<div id="sidebar-news">
				<div id='arrow_up'>
					
				</div>
				<div class='verticle_slider'>
					{foreach $sidebarNews as $newsItem}
						{$image = $newsItem->getImage()}
						{$category = $newsItem->getCategory()}
						<section class="sidebar-news-item">
							<a href="{link href="news"}/{$newsItem->getUrlIncludingCategory()}" class="sidebar-news-image">
								{if $image}
									<img src="{link href="thumbnail/sidebar"}/{$image->getBasename()}" alt="{$newsItem->getTitle()}"/>
								{else}
									<img src="{link href="thumbnail/sidebar"}/placeholder.jpg" alt="{$newsItem->getTitle()}"/>
								{/if}
							</a>
							<div class="sidebar-news-text">
								<h1>
									<a href="{link href="news"}/{$newsItem->getUrlIncludingCategory()}">
										{$newsItem->getTitle()}
									</a>
								</h1>
								<p>{snippet from=$newsItem->getText() length=100}</p>
							</div>
						</section>
					{/foreach}
						{foreach $sidebarProjects as $project}
							{$album = $project->getMediaAlbum()}
							{if $album && $album->getImage()}
								{$image = $album->getImage()}
								{$imagePath = {link href="thumbnail/span4/{$image->getBasename()}"}}
								{$imageTitle = $image->getTitle()}
							{else}
								{$imagePath = {link href="application/resources/images/placeholder.jpg" prefix='false'}}
								{t text='No image available' assignto='imageTitle'}
							{/if}
							<section class="sidebar-news-item">

								<a href="{link href="projects/detail"}/{$project->getUrl()}" class="sidebar-news-image">
									<img src="{$imagePath}" data-original="{$imagePath}" alt="{$imageTitle}"/>
								</a>
								<div class="sidebar-news-text">
									<h1>
										<a href="{link href="projects/detail"}/{$project->getUrl()}" class="sidebar-news-image">
											{$project->getName()}
										</a>
									</h1>
									<p>{snippet from=$project->getIntroductionText() length=100}</p>
								</div>

							</section>
						{/foreach}
						
				</div>
			<div id='arrow_down'>

						</div>
			</div>

		</div>

		{* Container *}
		<div class="container">

			{* Header *}


			<div class="row">
				<div class="span4">
					<a id="home-logo" href="{link href="home/"}" title="BOB361 architects">BOB361</a>
				</div>
				{if $menu && $menu->getNumberOfMenuItems() > 0}
				<div class="span8" id="menu">
					<a href="#" id="nav-toggle" title="{t text="Open the navigation menu"}"></a>
					<ul>
						{foreach $menu->getMenuItems() as $item}
							<li class="{if $item->getPath() == $activePath || $item->isActive()}active{/if}">
								<a href="{$item->getLink()}" title="{$item->getDescription()|att}">
									{$item->getTitle()}
								</a>
							</li>
						{/foreach}
                            <li>
                                <div class="search-container">
                                    <form id="search-form" method="get" action="{link href="search"}">
                                        <input id="search" type="search" name="q" data-search-href="{link href="search/ajax"}" />
                                        <button id="search-button"><img src="{link href='search.png' type='images'}" alt="{t('search')}" /></button>
                                    </form>

                                    {literal}
                                        <script id="tt-empty" type="text/template">
                                            <div class="tt-empty-message">
                                                No results found
                                            </div>
                                        </script>

                                        <script id="tt-element" type="text/template">
                                            <div>
                                                <p class="tt-suggestion-title">{{value}}</p>
                                                <p class="tt-suggestion-desc">{{description}}</p>
                                            </div>
                                        </script>

                                        <script id="tt-footer" type="text/template">
                                            <div class="search-footer">

                                            </div>
                                        </script>
                                    {/literal}
                                </div>
                            </li>
					</ul>
				</div>
				{/if}

			{* /Header *}
			</div>

			{* Content Wrapper *}

				{* Content (rendered by wrapped template) *}
				{$content}

			{* /Content*}

			{* Address mobile *}
			<div class="row" id="sidebar-mobile">
				<div class="span6">
					<div class="address">
						<h1>{t text="Brussels"}</h1>
						<ul class="address">
							{*<li>{t text=$streetBxl}</li>
							<li>{t text=$cityBxl}</li>
							<li>{t text=$countryBxl}</li>*}
							{identity get="@phone/tel-bxl" assignto="phoneBxl"}
							<li class="address-space">{t text="T @phone" phone=$phoneBxl}</li>
							{*<li>{t text="F @fax" fax=$faxBxl}</li>
							<li class="address-space"><a href="mailto:{$emailBxl}" title="{$emailBxl}">{$emailBxl}</a></li>*}
						</ul>
					</div>
				</div>
			{* /Address mobile *}
			</div>

		{* /Container *}
		</div>

		{* Include Javascript *}
		{include file='_PageJavascript.tpl'}
	</body>
</html>