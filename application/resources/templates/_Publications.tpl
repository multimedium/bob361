<!DOCTYPE html>
{locale category="LANG" assignto="lang"}
<!--[if lt IE 7 ]> <html lang="{$lang}" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="{$lang}" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="{$lang}" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="{$lang}" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="{$lang}" class="no-js"> <!--<![endif]-->

	<head>
		{* Disable IE compatibility mode *}
		{if $persistent.browser.name == 'ie'}
			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		{/if}
		
		{* Define the charset - Not specifying an encoding can lead to security vulnerabilities. *}
		<meta charset="utf-8" />
		
		{* Page Title *}
		<title>{$pageTitle}</title>

		{* Meta data *}
		<meta name="description" content="{snippet from=$pageDescription length=155}" />
		
		{* Facebook *}
		<meta property="og:title" content="{$pageTitle}" />
		<meta property="og:description" content="{snippet from=$pageDescription length=155}" />
		{* <meta property="og:image" content="" /> *}
		{* Please see http://developers.facebook.com/docs/opengraph/#types *}
		<meta property="og:type" content="article" />
		
		{* Favicon *}
		<link href="{link prefix="false" href="favicon.ico"}" rel="shortcut icon" />

		{* CSS *}
		<link rel="stylesheet" href="{link type="css" href="application.css"}" media="screen" />

		{* Additional CSS, to be loaded for this specific view *}
		{css}

		{* Javascript files *}
		<script src="{link href="modernizr.min.js" type="javascript"}"></script>
		
		{* Google Analytics *}
		{google_analytics ua="?"}
		
		{* If a Canonical URI applies to this view: *}
		{if $canonicalUri}
			{* Then, include the link to the Canonical URI *}
			<link rel="canonical" href="{$canonicalUri}" />
		{/if}
	</head>
	<body>
		{* Sidebar *}
		<div id="sidebar" class="hidden-phone">
	  		<div class="contact pull-right">
	  			<div class="address">
			  		<h1>{t text="Brussels"}</h1>
			  		<ul>
			  			<li>{t text="Poincarélaan 29"}</li>
			  			<li>{t text="B-1070 Brussels"}</li>
			  			<li>{t text="Belgium"}</li>
			  			<li class="address-space">{t text="T +32 (2)511 07 91"}</li>
			  			<li>{t text="F +32 (2) 511 86 07"}</li>
			  			<li class="address-space"><a href="mailto:bxl@bob361.com">{t text="bxl@bob361.com"}</a></li>
			  		</ul>
	  			</div>
	  		</div>
	  	{* /Sidebar *}
		</div>
		
		{* Slider navigation*}
		<div class="slider-nav hidden-phone">
			<div id="slider-nav-left" class="pull-left circle circle-orange"></div>
			<div id="slider-nav-right" class="pull-right circle circle-orange"></div>
		</div>
		{* /Slider navigation*}
	
		{* Container *}
		<div class="container">

			{* Header *}
			<div class="row">
				<div class="span4">
					<a id="home-logo" href="#" title="BOB361 architects">BOB361</a>
				</div>
				<div class="span8" id="menu">
					<ul>
						<li><a href="#" title="{t text="Projects"}">{t text="projects"}</a></li>
						<li class="active"><a href="#" title="{t text="Publications"}">{t text="publications"}</a></li>
						<li><a href="#" title="{t text="Company"}">{t text="company"}</a></li>
						<li><a href="#" title="{t text="Jobs"}">{t text="jobs"}</a></li>
						<li><a href="#" title="{t text="Contact"}">{t text="contact"}</a></li>
					</ul>
				</div>
			{* /Header *}
			</div>
			
			{* Content Wrapper *}
				
				{* Content *}
				<div class="row publication row-large">
					<div class="span2">
						<h1 class="date">2012</h1>
					</div>
					<div class="span5">
						<h1 class="title"><a href="#">Vooruitgangsrapport AG Vespa</a></h1>
						<p class="author">Written by H. Woumans</p>
						<p class="publication">Éditions</p>
					</div>
					<div class="span5 description">
						<p>p 59</p>
						<p>Huis van de Student</p>
					</div>
				</div>
				
				<div class="row publication">
					<div class="span2">
					</div>
					<div class="span5">
						<h1 class="title"><a href="#">Ik ga bouwen #351</a></h1>
						<p class="author">Written by A. Mus</p>
						<p class="publication">Ik ga bouwen, 06.201</p>
					</div>
					<div class="span5 description">
						<p>p 54-61</p>
						<p>Heverlee. Additie aan een additie: 'In een transparant omhulsel'</p>
					</div>
				</div>
				
				<div class="row publication">
					<div class="span2">
					</div>
					<div class="span5">
						<h1 class="title"><a href="#">Comfortable Houses</a></h1>
						<p class="author">Written by X. Rihan</p>
						<p class="publication">H.K. Rihan Int'l Culture Spread Limited</p>
					</div>
					<div class="span5 description">
						<p>p 40-43</p>
						<p>Dendermonde: bibliotheek, scoiaal restaurant en polyvalente ruimte</p>
					</div>
				</div>
				
				<div class="row publication row-large">
					<div class="span2">
						<h1 class="date">2011</h1>
					</div>
					<div class="span5">
						<h1 class="title"><a href="#">Livretd'Architecture Contemporaine à Bruxelles</a></h1>
						<p class="author">Written by S. Duchenne - LAB 1030 Schaerbeek</p>
						<p class="publication">180° Éditions</p>
					</div>
					<div class="span5 description">
						<p>p 48-51</p>
						<p>Jeruzalemstraat, 6 appartementen, Schaarbeek</p>
					</div>
				</div>
				
				<div class="row publication">
					<div class="span2">
					</div>
					<div class="span5">
						<h1 class="title"><a href="#">International New Architecture</a></h1>
						<p class="author">Written by Sun Xueliang</p>
						<p class="publication">International New Architecture, 04.2011</p>
					</div>
					<div class="span5 description">
						<p>p 68-73</p>
						<p>Zwembadsite Leuven:<br/>Tussen 2 pleinen : 40 woongelegenheden en 5 commerciële ruimtes</p>
					</div>
				</div>
				
				<div class="row publication">
					<div class="span2">
					</div>
					<div class="span5">
						<h1 class="title"><a href="#">A+231</a></h1>
						<p class="author">Written by</p>
						<p class="publication">A+ 231, 08-09.2011</p>
					</div>
					<div class="span5 description">
						<p>p 58-60</p>
						<p>Dendermonde: bibliotheek, scoiaal restaurant en polyvalente ruimte</p>
					</div>
				</div>
				
				
				{* /Content*}
				
				
				{* Address mobile *}
				<div class="row visible-phone" id="sidebar-mobile">
					<div class="span6">
						<h1>{t text="Brussels"}</h1>
						<ul class="address">
				  			<li>{t text="Poincarélaan 29"}</li>
				  			<li>{t text="B-1070 Brussels"}</li>
				  			<li>{t text="Belgium"}</li>
				  			<li class="address-space">{t text="T +32 (2)511 07 91"}</li>
				  			<li>{t text="F +32 (2) 511 86 07"}</li>
				  			<li class="address-space"><a href="mailto:bxl@bob361.com">{t text="bxl@bob361.com"}</a></li>
				  		</ul>
					</div>
				{* /Address mobile *}
				</div>

		{* /Container *}
		</div>

		{* Include Javascript *}
		{include file='_PageJavascript.tpl'}
	</body>
</html>