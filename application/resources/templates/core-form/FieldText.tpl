{if $field->getHint() OR $field->getHint() != ''}
	{assign var="fieldId" value=$field->getId()}
	{jsfile file="application/resources/javascript/forms/jquery.input-hint.js" context="form-hint"}
	
	{capture name='inputHint'}
		{literal}
		if(!Modernizr.input.placeholder) { 
			$('#{/literal}{$fieldId}{literal}').inputHint(); 
		}
		{/literal}
	{/capture}
	{jsinline script=$smarty.capture.inputHint context="text-hint-$fieldId"}
{/if}

<input 
	type="text" 
	class="field-text" 
	id="{$viewId}" 
	name="{$field->getId()}" 
	value="{$field->getValue()}" 
	{if $field->getReadonly()}readonly="readonly" {/if}
	{if $field->getMaxlength()}maxlength="{$field->getMaxlength()}" {/if}
	{if $field->getHint() != ''}title="{$field->getHint()|att}" placeholder="{$field->getHint()|att}" {/if}
/>