{* 
	Note: we cannot check groups of checkboxes by the required attribute in HTML5,
	Validation is done by PHP
*}

{assign var="fieldValue" value=$field->getValue()}

<div id="{$viewId}">
	{foreach from=$field->getGroups() item="group"}
		<div class="note">{$group[0]}</div>
			<div class="field-group-checkboxes">
				{foreach from=$group[1] key="value" item="label"}
					<div>
						<input type="checkbox" name="{$field->getId()}[]" value="{$value}" id="{$field->getId()}-{$value}"{if in_array($value, $fieldValue)} checked="checked"{/if} />
						<label for="{$field->getId()}-{$value}" class="checkbox">
							{$label}
						</label>
					</div>
				{/foreach}
			</div>
	{/foreach}	
	
	{foreach from=$field->getItems() key="value" item="label"}
		<div>
			<input type="checkbox" name="{$field->getId()}[]" value="{$value}" id="{$field->getId()}-{$value}"{if in_array($value, $fieldValue)} checked="checked"{/if} />
			<label for="{$field->getId()}-{$value}" class="checkbox">
				{$label}
			</label>
		</div>
	{/foreach}
</div>