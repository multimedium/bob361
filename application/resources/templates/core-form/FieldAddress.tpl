<div id="{$field->getId()}" class="address-container">
	
	{include file='_FieldAddressContainer.tpl' fieldSpecific=$field->getFieldStreet() classSpecific="field-address-street"}

	{if $field->getShowStreetNumber()}
		{include file='_FieldAddressContainer.tpl' fieldSpecific=$field->getFieldStreetNumber() classSpecific="field-address-street-number"}
	{/if}
	
	<div class="m-clear"><!-- --></div>
	
</div>

<div class="address-container">
		
	{if $field->getShowPostalCode()}
		{include file='_FieldAddressContainer.tpl' fieldSpecific=$field->getFieldPostalCode() classSpecific="field-address-postal-code"}
	{/if}

	{if $field->getShowCity()}
		{include file='_FieldAddressContainer.tpl' fieldSpecific=$field->getFieldCity() classSpecific="field-address-city"}
	{/if}

	<div class="m-clear"><!-- --></div>

</div>

{if $field->getShowRegion()}
	<div class="address-container">
		{include file='_FieldAddressContainer.tpl' fieldSpecific=$field->getFieldRegion() classSpecific="field-address-region"}
	</div>
{/if}

{if $field->getShowCountry()}
	<div class="address-container">
		{include file='_FieldAddressContainer.tpl' fieldSpecific=$field->getFieldCountry() classSpecific="field-address-country"}
	</div>
{/if}
