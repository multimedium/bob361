{* Field color picker *}
<div id="{$this->getId()}ColorWrapper">
	<input 
		type="color" 
		class="field-color" 
		id="{$this->getId()}" 
		name="{$field->getId()}" 
		value="{$field->getValue()}"
	/>
</div>

<script>
	if(!Modernizr.input.placeholder) {
		$("#{$this->getId()}").change(function() {
			$(this).parent().css({ padding: "0px 0px 0px 25px", backgroundColor : "#" + $(this).val().replace(/#/, "") });
		});
		$("#{$this->getId()}").change();
	}
</script>