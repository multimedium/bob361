{if $field->getHint() OR $field->getHint() != ''}
	{assign var="fieldId" value=$field->getId()}
	{jsfile file="application/resources/javascript/forms/jquery.input-hint.js" context="form-hint"}
	
	{capture name='inputHint'}
		{literal}
		if(!Modernizr.input.placeholder) { 
			$('#{/literal}{$fieldId}{literal}').inputHint(); 
		}
		{/literal}
	{/capture}
	{jsinline script=$smarty.capture.inputHint context="text-hint-$fieldId"}
{/if}

<textarea 
	id="{$viewId}"
	rows="{$field->getRows()}"
	cols="{$field->getCols()}"
	name="{$field->getId()}"
	class="field-text field-multiline"
	{if $field->getWidth() || $field->getHeight()} style="{if $field->getWidth()}width: {$field->getWidth()}px; {/if}{if $field->getHeight()}height: {$field->getHeight()}px{/if}"{/if}
	{if $field->getHint()} title="{$field->getHint()|att}" placeholder="{$field->getHint()|att}"{/if}
>{$field->getValue()}</textarea>