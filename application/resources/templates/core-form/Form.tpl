{if $form->getErrorMessage()}
	<div class="error-message">
		{$form->getErrorMessage()}
	</div>
{/if}

{* Enable the following if you would like to display a generic error message to the user *}
{*if $form->getErrorCount() > 0}
	<div class="error-message">
		{t text="Error"}
	</div>
{/if*}

<form id="{$viewId}"{foreach from=$form->getProperties() key="name" item="value"} {$name}="{$value}"{/foreach}>
	{foreach from=$form->getVariables() key="name" item="value"}
		{if $value|is_array}
			{foreach from=$value key="key" item="arrayValue"}
				<input type="hidden" name="{$name}[{$key}]" value="{$arrayValue}" />
			{/foreach}
		{else}
			<input type="hidden" name="{$name}" value="{$value}" />
		{/if}
	{/foreach}

	{* Contains all the field-groups *}
	{assign var="fieldGroups" value=$form->getFieldGroups()}
	{* Contains all the fields which are member of a group *}
	{assign var="groupFields" value=$form->getGroupFields()}

	{* Render fields which are not part of a field-group *}
	{foreach from=$form->getFields(false) key="name" item="field"}
		{if $groupFields->offsetExists($field->getId()) == false}
			{$field->getView()}
		{/if}
	{/foreach}

	{* Render field groups *}
	{foreach from=$fieldGroups item="group"}
		{$group->getView()}
	{/foreach}

	<div class="field-row-buttons">
		<button type="submit" class="button-submit left"><span>{$submitButtonLabel}</span></button>
		<div class="m-clear"><!-- --></div>
	</div>
</form>