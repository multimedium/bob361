<div class="field-address-container {if $classSpecific}{$classSpecific}{/if}" id="container-{$fieldSpecific->getId()}">
	{*add the title*}
	{if $fieldSpecific->getTitle()}
		<label for="{$fieldSpecific->getId()}">{$fieldSpecific->getTitle()}</label>
	{/if}

	{* add error message *}
	{if $fieldSpecific->getErrorMessage()}
		<div class="error-message">
			{$fieldSpecific->getErrorMessage()}
		</div>
	{/if}

		{* the field itself *}
		{$fieldSpecific->getInputView()->fetch()}

	{* the description *}
	{if $fieldSpecific->getDescription()}
		<div class="field-description">
			<div class="small note">
				{$fieldSpecific->getDescription()}
			</div>
		</div>
	{/if}
</div>