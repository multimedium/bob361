<input 
	type="password" 
	class="field-password" 
	id="{$viewId}" 
	name="{$field->getId()}" 
	value="{$field->getValue()}" 
	 {if $field->getReadonly()}readonly="readonly" {/if}
	 {if $field->getMaxlength()}maxlength="{$field->getMaxlength()}" {/if}
	 {if $field->getHint() != ''}placeholder="{$field->getHint()|att}" {/if}
/>