{* Variables for javascript *}
<script>
	var jQueryBaseHref = '{link}';
	var jQueryBaseHrefWithoutPrefix = '{link prefix="false"}';
</script>

{* Javascript files *}
{* jQuery: Use CDN version, and fall back to local if required *}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>!window.jQuery && document.write('<script src="{link href="jquery.min.js" type="javascript"}"><\/script>')</script>

{* Other *}
{*<script src="{link href="jquery.mousewheel-3.0.6.pack.js" type="javascript"}"></script>*}
<script src="{link href="lazyload.min.js" type="javascript"}"></script>
<script src="{link href="jquery.fancybox-2.1.4.pack.js" type="javascript"}"></script>
<script src="{link href="jquery.vegas.js" type="javascript"}"></script>
<script src="{link href="jquery.flexslider-min.js" type="javascript"}"></script>
<script src="{link href="jquery.flexslider.manualDirectionControls.js" type="javascript"}"></script>
<script src="{link href="typeahead.bundle.min.js" type="javascript"}"></script>
<script src="{link href="handlebars.js" type="javascript"}"></script>
<script src="{link href="application.js" type="javascript"}"></script>
<script src="{link href="jquery.bxslider.min.js" type="javascript"}"></script>




{* Additional javascript *}
{javascript}