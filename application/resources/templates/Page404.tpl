<div class="row row-large">
	<div class="span12">
		<h1 class="title-404">404</h1>
		<h1 class="subtitle-404">{t text="The page could not be found."}</h1><br/>
		<p><a href="javascript:history.go(-1)" title="Go back">{t text="Go Back"}</a><p>
	</div>
</div>