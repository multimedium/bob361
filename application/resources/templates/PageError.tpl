<div class="row row-large">
	<div class="span12">
		<h1 class="title-404">{t text="Error"}</h1>
		<h1 class="subtitle-404">{t text="Something went wrong."}</h1><br/>
		<p><a href="javascript:history.go(-1)" title="Go back">{t text="Go Back"}</a><p>
	</div>
</div>