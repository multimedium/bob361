<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{identity get="@email" assignto="email"}
	{identity get="@name"  assignto="name"}

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>{$name}</title>
</head>
<body style="background-color: #ffffff; padding:0; margin:0; font-family: Arial, Helvetica, sans-serif; font-size: 14px;" bgcolor="#ffffff">
	<table width="600" cellspacing="0" cellpadding="0" style="background-color: #ffffff;" bgcolor="#ffffff" align="center">
		<tbody>
			<tr>
				<td>
					<img src="{link href='logo.gif' type='images'}" width="198" height="55" alt="" border="0" style="width: 198px; height: 55px;"/>
				</td>
			</tr>
			{* Page title *}
			{if isset($pageTitle) && $pageTitle}
				<tr>
					<td align="left">
						<h1 style="font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: bold;">
							{$pageTitle}
						</h1>
					</td>
				</tr>
			{/if}

			<tr>
				<td align="left">
					<br />
					{$content}
					<br />
					<br />
				</td>
			</tr>
			<tr>
				<td align="center" style="padding: 5px 0; background-color: #eeeeee;">
					&copy; {$name} {date format="YYYY"} &nbsp;&nbsp;&nbsp; &middot; &nbsp;&nbsp;&nbsp;
					<a style="color: #b4b4b4;" href="{link}" title="{$name}">{t|escape text='Bezoek onze website'}</a>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>