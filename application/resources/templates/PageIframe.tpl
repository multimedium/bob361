<!DOCTYPE html>
{locale category="LANG" assignto="lang"}
<!--[if lt IE 7 ]> <html lang="{$lang}" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="{$lang}" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="{$lang}" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="{$lang}" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="{$lang}" class="no-js"> <!--<![endif]-->

	<head>
		{* Disable IE compatibility mode *}
		{if $persistent.browser.name == 'ie'}
			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		{/if}
		
		{* Define the charset - Not specifying an encoding can lead to security vulnerabilities. *}
		<meta charset="utf-8" />
		
		{* Page Title *}
		<title>{$pageTitle}</title>

		{* Meta data *}
		<meta name="description" content="{snippet from=$pageDescription length=155}" />
		
		{* Facebook *}
		<meta property="og:title" content="{$pageTitle}" />
		<meta property="og:description" content="{snippet from=$pageDescription length=155}" />
		{* <meta property="og:image" content="" /> *}
		{* Please see http://developers.facebook.com/docs/opengraph/#types *}
		<meta property="og:type" content="article" />
		
		{* Favicon *}
		<link href="{link prefix="false" href="favicon.ico"}" rel="shortcut icon" />

		{* CSS *}
		<link rel="stylesheet" href="{link type="css" href="application.css"}" media="screen" />

		{* Additional CSS, to be loaded for this specific view *}
		{css}

		{* Javascript files *}
		<script src="{link href="modernizr.min.js" type="javascript"}"></script>
		
		{* Google Analytics *}
		{google_analytics ua="?"}
		
		{* If a Canonical URI applies to this view: *}
		{if $canonicalUri}
			{* Then, include the link to the Canonical URI *}
			<link rel="canonical" href="{$canonicalUri}" />
		{/if}
	</head>
	<body>
		{* Content (rendered by the wrapped template) *}
		{$content}

		{* Include Javascript *}
		{include file='_PageJavascript.tpl'}
	</body>
</html>