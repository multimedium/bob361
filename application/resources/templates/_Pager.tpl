{if $pager}
	{$pageCount = $pager->getTotalCountOfPages()}
	{$pager->setNumberOfPages(5)}
	{$pages = $pager->getPages()}
	{$range = $pager->getRange()} 
	{$pager->alignActivePage('middle')}
	{if $pageCount > 1}		
		<ul class="pager">
			{if $pager->getPage() != 1}
				{if $pageCount > 2}
					<li><a href="{urlcopy page=1}" title="{t|att text='Go to the first page'}">«</a></li>
				{/if}
				<li><a href="{urlcopy page="{if $pager->getPagePrevious()}{$pager->getPagePrevious()}{else}1{/if}"}" title="{t|att text='Go to the previous page'}">‹</a></li>
			{/if}
			{foreach $pages as $page}
				<li {if $page.number == $pager->getPage()} class="active"{/if}><a href="{urlcopy page="{$page.number}"}" title="{t|att text='Go to page @page' page=$page.number}">{$page.number}</a></li>
			{/foreach}
			{if $pager->getPage() != $pageCount}
				<li><a href="{urlcopy page="{if $pager->getPageNext()}{$pager->getPageNext()}{else}{$pager->getTotalCountOfPages()}{/if}"}" title="{t|att text='Go to the next page'}">›</a></li>
				{if $pageCount > 2}
					<li><a class="last" href="{urlcopy page="{$pager->getTotalCountOfPages()}"}" title="{t|att text='Go to the last page'}">»</a></li>
				{/if}
			{/if}
		</ul>
	{/if}
{/if}