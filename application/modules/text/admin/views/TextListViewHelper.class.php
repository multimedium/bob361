<?php
/**
 * TextListViewHelper
 *
 * @package app
 * @subpackage text
 */
class TextListViewHelper {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Holds the collection of {@link Text} objects, used for display
	 * 
	 * @access private
	 * @var array
	 */
	private $_texts = array();

	/* -- PUBLIC FUNCTIONS -- */

	/**
	 * Set column value
	 *
	 * Manipulates the values shown in the list view. A string is
	 * expected from this method.
	 *
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @author Tim Segers
	 * @access public
	 * @param AdminListColumnDefinition $column
	 * 		The column for which to set the value
	 * @param Text $dataObject
	 * 		The instance for which to set the value
	 * @param mixed $value
	 * 		The value that has been prepared by {@link AdminListView}.
	 * 		This will be the listed value, if not manipulated by this
	 * 		method.
	 * @return string|null
	 */
	public function setColumnValue(AdminListColumnDefinition $column, Text $dataObject, $value) {

		switch($column->getIndex()) {

			case 1:
				if($value && ! $dataObject->isRootText()) {
					$parentText = $this->_getText($value);
					return $parentText ? $parentText->getTitle() : ' ';
				} else {
					return '&nbsp;';
				}
				break;

			default:
				return $value;
				break;
		}
	}
	
	/**
	* Set row options
	*
	* NOTE:
	* If this method does not return anything, it will be ignored!
	*
	* @access public
	* @param M_DataObject $dataObject
	* The instance for which to set the row options
	* @return ArrayIterator|null
	*/
	public function setRowOptions(M_DataObject $dataObject) {
		if($dataObject->getDeletable() == 0) {
			$array[] = '<a href="'. M_Request::getLink('admin/edit/' .
			$dataObject->getMapper()->getModule()->getId() . '/' .
			$dataObject->getId() . '/' .
			$dataObject->getMapper()->getModule()->getDataObjectIdOfObject($dataObject)
			. '" class="edit">' . t('Edit this item') . '</a>');

			return $array;
		}
	}

	/* -- PRIVATE / PROTECTED -- */

	/**
	 * Get Text
	 *
	 * @access protected
	 * @param int $id
	 *		The id of the requested {@link Text}
	 * @return Text
	 */
	protected function _getText($id) {
		if(! isset($this->_texts[$id])) {
			$mapper = M_Loader::getDataObjectMapper('Text', 'text');
			$this->_texts[$id] = $mapper->getOneByFieldWithRegistryCache('id', (int) $id);
		}
		return $this->_texts[$id];
	}
}