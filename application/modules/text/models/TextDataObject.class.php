<?php
/** 
 * TextDataObject class
 * 
 * @see TextMapper
 * @see Text
 * @package App
 * @subpackage Text
 */
// Load media class
M_Loader::loadDataObject('DataObjectWithMedia', 'media');
class TextDataObject extends DataObjectWithMedia {
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link TextDataObject::getId()}
	 * - {@link TextDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * parentTextId
	 * 
	 * This property stores the parentTextId. To get/set the value of the parentTextId, 
	 * read the documentation on 
	 * 
	 * - {@link TextDataObject::getParentTextId()}
	 * - {@link TextDataObject::setParentTextId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_parentTextId;
	
	/**
	 * title ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * title. To get/set the value of the title, read the documentation on 
	 * 
	 * - {@link TextDataObject::getTitle()}
	 * - {@link TextDataObject::getTitleId()}
	 * - {@link TextDataObject::setTitle()}
	 * - {@link TextDataObject::setTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_titleId;
	
	/**
	 * text ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * text. To get/set the value of the text, read the documentation on 
	 * 
	 * - {@link TextDataObject::getText()}
	 * - {@link TextDataObject::getTextId()}
	 * - {@link TextDataObject::setText()}
	 * - {@link TextDataObject::setTextId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_textId;
	
	/**
	 * url ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * url. To get/set the value of the url, read the documentation on 
	 * 
	 * - {@link TextDataObject::getUrl()}
	 * - {@link TextDataObject::getUrlId()}
	 * - {@link TextDataObject::setUrl()}
	 * - {@link TextDataObject::setUrlId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_urlId;
	
	/**
	 * realm ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * realm. To get/set the value of the realm, read the documentation on 
	 * 
	 * - {@link TextDataObject::getRealm()}
	 * - {@link TextDataObject::getRealmId()}
	 * - {@link TextDataObject::setRealm()}
	 * - {@link TextDataObject::setRealmId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_realmId;
	
	/**
	 * deletable
	 * 
	 * This property stores the deletable. To get/set the value of the
	 * deletable, read the documentation on 
	 * 
	 * - {@link TextDataObject::getDeletable()}
	 * - {@link TextDataObject::setDeletable()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_deletable;
	
	/**
	 * categoryId
	 * 
	 * This property stores the categoryId. To get/set the value of the
	 * categoryId, read the documentation on 
	 * 
	 * - {@link TextDataObject::getCategoryId()}
	 * - {@link TextDataObject::setCategoryId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_categoryId;
	
	/**
	 * order
	 * 
	 * This property stores the order. To get/set the value of the
	 * order, read the documentation on 
	 * 
	 * - {@link TextDataObject::getOrder()}
	 * - {@link TextDataObject::setOrder()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_order;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link TextDataObject::getPublished()}
	 * - {@link TextDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;
	
	/**
	 * Get title
	 * 
	 * Get title in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the title
	 * @return string|NULL
	 */
	public function getTitle($locale = NULL) {
		return $this->_getLocalizedText('title', $locale);
	}
	
	/**
	 * Get title ID
	 * 
	 * Get numeric reference to the translated title .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTitleId() {
		return $this->_titleId;
	}
	
	/**
	 * Set title
	 * 
	 * Set title, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated title
	 * @param string $locale
	 * 	The language in which to set the title
	 * @return void
	 */
	public function setTitle($text, $locale = NULL) {
		$this->_setLocalizedText('title', $text, $locale);
	}
	
	/**
	 * Set title ID
	 * 
	 * Set numeric reference to the translated title .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of title
	 * @return void
	 */
	public function setTitleId($id) {
		$this->_titleId = $id;
	}
	
	/**
	 * Get text
	 * 
	 * Get text in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the text
	 * @return string|NULL
	 */
	public function getText($locale = NULL) {
		return $this->_getLocalizedText('text', $locale);
	}
	
	/**
	 * Get text ID
	 * 
	 * Get numeric reference to the translated text .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTextId() {
		return $this->_textId;
	}
	
	/**
	 * Set text
	 * 
	 * Set text, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated text
	 * @param string $locale
	 * 	The language in which to set the text
	 * @return void
	 */
	public function setText($text, $locale = NULL) {
		$this->_setLocalizedText('text', $text, $locale);
	}
	
	/**
	 * Set text ID
	 * 
	 * Set numeric reference to the translated text .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of text
	 * @return void
	 */
	public function setTextId($id) {
		$this->_textId = $id;
	}
	
	/**
	 * Get url
	 * 
	 * Get url in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the url
	 * @return string|NULL
	 */
	public function getUrl($locale = NULL) {
		return $this->_getLocalizedText('url', $locale);
	}
	
	/**
	 * Get url ID
	 * 
	 * Get numeric reference to the translated url .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getUrlId() {
		return $this->_urlId;
	}
	
	/**
	 * Set url
	 * 
	 * Set url, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated url
	 * @param string $locale
	 * 	The language in which to set the url
	 * @return void
	 */
	public function setUrl($text, $locale = NULL) {
		$this->_setLocalizedText('url', $text, $locale);
	}
	
	/**
	 * Set url ID
	 * 
	 * Set numeric reference to the translated url .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of url
	 * @return void
	 */
	public function setUrlId($id) {
		$this->_urlId = $id;
	}
	
	/**
	 * Get realm
	 * 
	 * Get realm in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the realm
	 * @return string|NULL
	 */
	public function getRealm($locale = NULL) {
		return $this->_getLocalizedText('realm', $locale);
	}
	
	/**
	 * Get realm ID
	 * 
	 * Get numeric reference to the translated realm .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getRealmId() {
		return $this->_realmId;
	}
	
	/**
	 * Set realm
	 * 
	 * Set realm, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated realm
	 * @param string $locale
	 * 	The language in which to set the realm
	 * @return void
	 */
	public function setRealm($text, $locale = NULL) {
		$this->_setLocalizedText('realm', $text, $locale);
	}
	
	/**
	 * Set realm ID
	 * 
	 * Set numeric reference to the translated realm .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of realm
	 * @return void
	 */
	public function setRealmId($id) {
		$this->_realmId = $id;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return mixed
	 */
	public function setId($id) {
		$this->_id = $id;
	}
	
	/**
	 * Get parentTextId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getParentTextId() {
		return $this->_parentTextId;
	}
	
	/**
	 * Set parentTextId
	 * 
	 * @access public
	 * @param mixed $parentTextId
	 * @return mixed
	 */
	public function setParentTextId($parentTextId) {
		$this->_parentTextId = $parentTextId;
	}
	
	/**
	 * Get deletable
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getDeletable() {
		return $this->_deletable;
	}
	
	/**
	 * Set deletable
	 * 
	 * @access public
	 * @param mixed $deletable
	 * @return mixed
	 */
	public function setDeletable($deletable) {
		$this->_deletable = $deletable;
	}
	
	/**
	 * Get categoryId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCategoryId() {
		return $this->_categoryId;
	}
	
	/**
	 * Set categoryId
	 * 
	 * @access public
	 * @param mixed $categoryId
	 * @return mixed
	 */
	public function setCategoryId($categoryId) {
		$this->_categoryId = $categoryId;
	}
	
	/**
	 * Get order
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Set order
	 * 
	 * @access public
	 * @param mixed $order
	 * @return mixed
	 */
	public function setOrder($order) {
		$this->_order = $order;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return mixed
	 */
	public function setPublished($published) {
		$this->_published = $published;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return TextMapper $mapper
	 */
	public function getMapper() {
		if (is_null($this->_mapper)) {
			M_Loader::loadDataObjectMapper("Text","text");
			$this->_mapper = new TextMapper();
		}
		return $this->_mapper;
	}
	
		
}