<?php
/**
 * Text
 * 
 * @package app
 * @subpackage text
 */
// Load superclass
M_Loader::loadDataObject('TextDataObject', 'text');
class Text extends TextDataObject {
	
	/* -- Properties -- */

	/**
	* Deletable
	*
	* This property stores the deletable. To get/set the value of the
	* deletable, read the documentation on
	*
	* - {@link TextDataObject::getDeletable()}
	* - {@link TextDataObject::setDeletable()}
	*
	* @access protected
	* @var mixed
	*/
	protected $_deletable = 1;
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Set title
	 * 
	 * @access public
	 * @param mixed $text
	 * @return mixed
	 */
	public function setTitle($text, $locale = NULL) {
		// Set the title
		parent::setTitle($text, $locale);
		
		// Set the URL (suffix), if not already done:
		if(! $this->getUrl($locale)) {
			$this->setUrl(M_Uri::getPathElementFromString($text), $locale);
		}
	}
	
	/**
	 * Get parent text
	 * 
	 * NOTE:
	 * Will return NULL if the parent could not be found.
	 * 
	 * @access public
	 * @return void
	 */
	public function getParentText() {
		if($this->getParentTextId()) {
			$text = M_Loader
				::getDataObjectMapper('Text', 'text')
				->addFilterPublishedOnly()
				->getById($this->getParentTextId());
			
			if($text) {
				return $text;
			}
		}
		
		return NULL;
	}
	
	/**
	 * Get URL of parent
	 * 
	 * Will look up the URL of the parent (recursively). Typically used to compose
	 * the URL to the page inside the website.
	 * 
	 * @access public
	 * @return string
	 */
	public function getRecursiveParentUrl() {
		// Output
		$u = array();
		
		// Working variables
		$t = $this;
		$l = array();
		
		// For each of the parents:
		while(is_object($t = $t->getParentText())) {
			// If the current parent has not been added already
			if(! in_array($t->getId(), $l)) {
				// Then, add to output
				array_unshift($u, $t->getUrl());
			}
			// Break out of infinite loop:
			else {
				break;
			}
			
			// Log, to prevent infinite loop
			$l[] = $t->getId();
		}
		
		// Return output
		return implode('/', $u);
	}
	
	/**
	 * Get full page URL 
	 * 
	 * @access public
	 * @return string
	 */
	public function getFullUrl() {
		if ($this->getRecursiveParentUrl()) {
			return '/' . $this->getRecursiveParentUrl() . '/' . $this->getUrl();
		} else {
			return '/' . $this->getUrl();
		}
	}
	
	
	/**
	 * Get child texts
	 * 
	 * @access public
	 * @return ArrayIterator $texts
	 * 		The matching objects; instances of {@link Text}
	 */
	public function getChildTexts() {
		return M_Loader
			::getDataObjectMapper('Text', 'text')
			->addFilterPublishedOnly()
			->addFilterDefaultSorting()
			->addFilterParentText($this)
			->getAll();
	}
	
	/**
	 * Get number of child texts
	 * 
	 * @access public
	 * @return integer $count
	 */
	public function getNumberOfChildTexts() {
		return M_Loader
			::getDataObjectMapper('Text', 'text')
			->addFilterPublishedOnly()
			->addFilterParentText($this)
			->getCount();
	}
	
	/**
	 * Is root texT?
	 * 
	 * Will check if this is a root text, returning TRUE or FALSE, depending
	 * on the result of the check
	 * 
	 * @access public
	 * @return bool
	 */
	public function isRootText() {
		return ! $this->getParentTextId();
	}
	
	/**
	 * get media album
	 * 
	 * @return MediaAlbum
	 */
	public function getMediaAlbum() {
		// construct media album mapper
		M_Loader::loadDataObjectMapper('MediaAlbum', 'media');
		$mapper = new MediaAlbumMapper();
		
		return $mapper->addFilterPublished()->getAlbumByObject($this);
	}
	
	/**
	 * get Images
	 * 
	 * @return M_ArrayIterator
	 */
	public function getImages() {
		$album = $this->getMediaAlbum();
		return $album->getImages();
	}
}