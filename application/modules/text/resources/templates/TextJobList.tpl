<div class="row row-large publication-detail">
	<div class="span12">
		<h1 class="title">Comfortable Houses</h1>
		<p class="subtitle">Written by X. Rihan</p>
		<p class="subtitle">H.K. Rihan Int'l Culture Spread Limited</p>
		<p class="subtitle">2012</p>
	</div>
</div>	
<div class="row publication-detail">
	<div class="span4">
		<div class="img-wrapper">
			<img src="{link href="application/resources/images/publications/71425513035.jpg"}" alt="" title=""/>
		</div>
		<div class="img-wrapper">
			<img src="{link href="application/resources/images/publications/72065513042.jpg"}" alt="" title=""/>
		</div>
	</div>
	<div class="span8">
		<p class="page">p 40-43</p>
		<p class="page">Dendermonde: bibliotheek, scoiaal restaurant en polyvalente ruimte</p>
		<p class="article">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Nullam quis risus eget urna mollis ornare vel eu leo. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id ligula porta felis euismod semper. Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas faucibus mollis interdum.<br/><br/> Nullam quis risus eget urna mollis ornare vel eu leo. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Cras mattis consectetur purus sit amet fermentum. Nullam id dolor id nibh ultricies vehicula ut id elit. Curabitur blandit tempus porttitor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Etiam porta sem malesuada magna mollis euismod.
		</p>
	</div>
</div>	

{* /Content*}


{* Address mobile *}
<div class="row visible-phone" id="sidebar-mobile">
	<div class="span6">
		<h1>{t text="Brussels"}</h1>
		<ul class="address">
			<li>{t text="Poincarélaan 29"}</li>
			<li>{t text="B-1070 Brussels"}</li>
			<li>{t text="Belgium"}</li>
			<li class="address-space">{t text="T +32 (2)511 07 91"}</li>
			<li>{t text="F +32 (2) 511 86 07"}</li>
			<li class="address-space"><a href="mailto:bxl@bob361.com">{t text="bxl@bob361.com"}</a></li>
		</ul>
	</div>
</div>