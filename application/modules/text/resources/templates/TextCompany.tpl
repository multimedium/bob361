<div class="row row-large">
	<div class="span12">
		<p>
			{$text->getText()}
		</p>
		<h1 class="title-dark">Founders</h1>
	</div>
</div>

<div class="row row-small company">
	<div class="span4">
		<img src="{link href="application/resources/images/publications/71425513035.jpg"}" alt="" title=""/>
		<h1 class="title-light">Name Surname</h1>
		<p>
			Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur est at lobortis.
		</p>
	</div>
	<div class="span4">
		<img src="{link href="application/resources/images/publications/71425513035.jpg"}" alt="" title=""/>
		<h1 class="title-light">Name Surname</h1>
		<p>
			Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur est at lobortis.
		</p>
	</div>
	<div class="span4">
		<img src="{link href="application/resources/images/publications/71425513035.jpg"}" alt="" title=""/>
		<h1 class="title-light">Name Surname</h1>
		<p>
			Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur est at lobortis.
		</p>
	</div>
</div>

<div class="row row-large">
	<div class="span12">
		<h1 class="title-dark">Partners</h1>
	</div>
</div>

<div class="row row-small company">
	<div class="span4">
		<img src="{link href="application/resources/images/publications/71425513035.jpg"}" alt="" title=""/>
		<h1 class="title-light">Partner</h1>
		<p>
			Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur est at lobortis.
		</p>
	</div>
	<div class="span4">
		<img src="{link href="application/resources/images/publications/71425513035.jpg"}" alt="" title=""/>
		<h1 class="title-light">Partner</h1>
		<p>
			Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur est at lobortis.
		</p>
	</div>
	<div class="span4">
		<img src="{link href="application/resources/images/publications/71425513035.jpg"}" alt="" title=""/>
		<h1 class="title-light">Partner</h1>
		<p>
			Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur est at lobortis.
		</p>
	</div>
</div>