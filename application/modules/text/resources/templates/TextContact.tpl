{* Google maps *}
{jsexternal external="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"} 
{jsfile file='application/resources/javascript/contact.js' context='contact-js'} 

{identity get="@phone/tel-bxl" assignto="phoneBxl"}
{identity get="@phone/fax-bxl" assignto="faxBxl"}
{config var="Contact/Brussels/Street" assignto="streetBxl"}
{config var="Contact/Brussels/City" assignto="cityBxl"}
{config var="Contact/Brussels/Country" assignto="countryBxl"}
{config var="Contact/Brussels/Email" assignto="emailBxl"}

<div class="row">
	<div class="span8">
		<div id="map-bxl"></div>
	</div>
	<div class="span4 address address-contact vcard">
		<div>
			<h1 class="fn">{t text="Brussels"}</h1>
			<ul>
				<div class="adr">
					<li class="street-address"><b>{t text="Adress"}</b> {t text=$streetBxl}</li>
					<li class="locality">{t text=$cityBxl}</li>
				</div>
				<li class="tel"><b>{t text="Tel"}</b> {t text="@phone" phone=$phoneBxl}</li>
{*				<li class="tel"><b>{t text="Fax"}</b> {t text="@fax" fax=$faxBxl}</li>*}
				<li class="email"><b>{t text="Mail"}</b> <a href="mailto:{$emailBxl}" title="{$emailBxl}">{$emailBxl}</a></li>
			</ul>
		</div>
	</div>
</div>