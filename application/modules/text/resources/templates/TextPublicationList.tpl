<div class="row publication row-large">
	<div class="span2">
		<h1 class="date">2012</h1>
	</div>
	<div class="span5">
		<h1 class="title"><a href="#">Vooruitgangsrapport AG Vespa</a></h1>
		<p class="author">Written by H. Woumans</p>
		<p class="publication">Éditions</p>
	</div>
	<div class="span5 description">
		<p>p 59</p>
		<p>Huis van de Student</p>
	</div>
</div>

<div class="row publication">
	<div class="span2">
	</div>
	<div class="span5">
		<h1 class="title"><a href="#">Ik ga bouwen #351</a></h1>
		<p class="author">Written by A. Mus</p>
		<p class="publication">Ik ga bouwen, 06.2012</p>
	</div>
	<div class="span5 description">
		<p>p 54-61</p>
		<p>Heverlee. Additie aan een additie: 'In een transparant omhulsel'</p>
	</div>
</div>

<div class="row publication">
	<div class="span2">
	</div>
	<div class="span5">
		<h1 class="title"><a href="#">Comfortable Houses</a></h1>
		<p class="author">Written by X. Rihan</p>
		<p class="publication">H.K. Rihan Int'l Culture Spread Limited</p>
	</div>
	<div class="span5 description">
		<p>p 40-43</p>
		<p>Dendermonde: bibliotheek, scoiaal restaurant en polyvalente ruimte</p>
	</div>
</div>

<div class="row publication row-large">
	<div class="span2">
		<h1 class="date">2011</h1>
	</div>
	<div class="span5">
		<h1 class="title"><a href="#">Livre d'Architecture Contemporaine à Bruxelles</a></h1>
		<p class="author">Written by S. Duchenne - LAB 1030 Schaerbeek</p>
		<p class="publication">180° Éditions</p>
	</div>
	<div class="span5 description">
		<p>p 48-51</p>
		<p>Jeruzalemstraat, 6 appartementen, Schaarbeek</p>
	</div>
</div>

<div class="row publication">
	<div class="span2">
	</div>
	<div class="span5">
		<h1 class="title"><a href="#">International New Architecture</a></h1>
		<p class="author">Written by Sun Xueliang</p>
		<p class="publication">International New Architecture, 04.2011</p>
	</div>
	<div class="span5 description">
		<p>p 68-73</p>
		<p>Zwembadsite Leuven:<br/>Tussen 2 pleinen : 40 woongelegenheden en 5 commerciële ruimtes</p>
	</div>
</div>

<div class="row publication">
	<div class="span2">
	</div>
	<div class="span5">
		<h1 class="title"><a href="#">A+231</a></h1>
		<p class="author">Written by</p>
		<p class="publication">A+ 231, 08-09.2011</p>
	</div>
	<div class="span5 description">
		<p>p 58-60</p>
		<p>Dendermonde: bibliotheek, scoiaal restaurant en polyvalente ruimte</p>
	</div>
</div>