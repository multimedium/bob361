<div class="row row-large">
	<div class="span12 project">
		<div class="flexslider project-slider">
			<div class="nav">
				<a class="previous disabled" href="#"><img src="application/resources/images/slider-nav.png" alt="previous"/></a>
				<a class="next pull-right" href="#"><img src="application/resources/images/slider-nav.png" alt="next"/></a>
			</div>
			<ul class="slides">
				<li>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
					<div class="flex-caption">
						<p>Urban development</p>
						<p>Colonie, Antwerp</p>
					</div>
				</li>
				<li>
					<img src="{link href="application/resources/images/projects/8.jpg"}" alt="" title=""/>
					<div class="flex-caption">
						<p>Urban development</p>
						<p>Colonie, Antwerp</p>
					</div>
				</li>
				<li>
					<img src="{link href="application/resources/images/projects/7.jpg"}" alt="" title=""/>
					<div class="flex-caption">
						<p>Urban development</p>
						<p>Colonie, Antwerp</p>
					</div>
				</li>
				<li>
					<img src="{link href="application/resources/images/projects/8.jpg"}" alt="" title=""/>
					<div class="flex-caption">
						<p>Urban development</p>
						<p>Colonie, Antwerp</p>
					</div>
				</li>
				<li>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
					<div class="flex-caption">
						<p>Urban development</p>
						<p>Colonie, Antwerp</p>
					</div>
				</li>
				<li>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
					<div class="flex-caption">
						<p>Urban development</p>
						<p>Colonie, Antwerp</p>
					</div>
				</li>
				<li>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
					<div class="flex-caption">
						<p>Urban development</p>
						<p>Colonie, Antwerp</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
		
<div class="row">
	<div class="span12">
			<p>
				Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Curabitur blandit tempus porttitor.
				Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas sed diam eget risus varius blandit sit amet non magna. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
			</p>
	</div>
</div>
		
<div class="row row-large">
	<div class="span2">
		<ul class="project-detail">
			<li>Location</li>
			<li>Antwerp</li>
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>Size</li>
			<li>100.000 m2</li>
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>Team Leader</li>
			<li>Antoine Leroi</li>
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>Team Members</li>
			<li>Mollis Nibh</li>
			<li>Pharetra Ultricies</li>
			<li>Mattis Cursus</li>
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>Client</li>
			<li>Antwerp</li>
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>Type</li>
			<li>Antwerp</li>
		</ul>
	</div>
</div>
		
<div class="row">
	<div class="span12">
		<hr/>
		<h1 class="title-light">Related projects</h1>
		<ul class="project-list">
			<li class="project">
				<a href="#">
					<h1>Urban Development</h1>
					<h2>Colonie, Antwerpen</h2>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
				</a>
			</li>
			<li class="project">
				<a href="#">
					<h1>Urban Development</h1>
					<h2>Colonie, Antwerpen</h2>
					<img src="{link href="application/resources/images/projects/7.jpg"}" alt="" title=""/>
				</a>
			</li>
			<li class="project">
				<a href="#">
					<h1>Urban Development</h1>
					<h2>Colonie, Antwerpen</h2>
					<img src="{link href="application/resources/images/projects/8.jpg"}" alt="" title=""/>
				</a>
			</li>
			<li class="project">
				<a href="#">
					<h1>Urban Development</h1>
					<h2>Colonie, Antwerpen</h2>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
				</a>
			</li>
			<li class="project">
				<a href="#">
					<h1>Urban Development</h1>
					<h2>Colonie, Antwerpen</h2>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
				</a>
			</li>
			<li class="project">
				<a href="#">
					<h1>Urban Development</h1>
					<h2>Colonie, Antwerpen</h2>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
				</a>
			</li>
			<li class="project">
				<a href="#">
					<h1>Urban Development</h1>
					<h2>Colonie, Antwerpen</h2>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
				</a>
			</li>
			<li class="project">
				<a href="#">
					<h1>Urban Development</h1>
					<h2>Colonie, Antwerpen</h2>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
				</a>
			</li>
			<li class="project">
				<a href="#">
					<h1>Urban Development</h1>
					<h2>Colonie, Antwerpen</h2>
					<img src="{link href="application/resources/images/projects/6.jpg"}" alt="" title=""/>
				</a>
			</li>
		</ul>
	</div>
</div>