<?php
/**
 * TextView
 * 
 * @package app
 * @subpackage text
 */
	
// load super class
M_Loader::loadView('TextView', 'text');
	
class TextJobListView extends TextView {
	/**
	 * Get resource
	 *
	 * @see M_ViewHtml::getResource()
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {		
		return M_ViewHtmlResource::constructWithModuleId('text', 'TextProjectDetail.tpl');
	}
}