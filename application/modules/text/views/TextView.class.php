<?php
/**
 * TextView
 * 
 * @package app
 * @subpackage text
 */
abstract class TextView extends PageView {
	
	/**
	 * Text
	 * 
	 * This property stores the text that is to be displayed in the view
	 * 
	 * @see TextDetailView::setText()
	 * @access private
	 * @var Text
	 */
	protected $_text;	
	
	/* -- PUBLIC FUNCTIONS -- */

	
	/**
	 * Set the text
	 * 
	 * Will set the text that is to be displayed in the view
	 * 
	 * @access public
	 * @param Text $text
	 * @return TextDetailView $view
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setText(Text $text) {
		$this->_text = $text;
		return $this;
	}

	
	/**
	 * Preprocessing
	 * 
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Make sure the text has been provided
		if(! $this->_text) {
			// If not provided, we throw an exception
			throw new M_ViewException(sprintf(
				'Cannot render the Text View; missing Text. Please provide ' . 
				'Text to %s::%s()',
				__CLASS__,
				'setText'
			));
		}
		
		
		// Assign the text to the template
		$this->assign('text', $this->_text);
	}

}