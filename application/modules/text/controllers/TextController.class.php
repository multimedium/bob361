<?php
/**
 * TextController
 * 
 * @package App
 * @subpackage Text
 */
class TextController extends PageController {
	
	/**
	 * Routing
	 * 
	 * @see M_Controller::__router()
	 * @access public
	 * @return array
	 */
	public function __router() {
		return array(
			'detail(:any)' => 'detail$1',
			'(:any)'       => 'detail/$1'
		);
	}
	
	/**
	 * Show text (selected by URL)
	 * 
	 * @uses TextMapper::getByUrl()
	 * @access public
	 * @param string $realm
	 * @return void
	 */
	public function detail() {
		// Catch the path elements:
		$args = func_get_args();
		
		// Get the text(s) for the collection of path elements
		$texts = $this->_getTextsWithPathElements($args);
		
		// If no text has been found
		if(! $texts) {
			// Then, we handle a "404/Page not found" request
			$this->_404();
		}
		
		// Get the top level parent text
		$top = array_shift(array_values($texts));
		/* @var $top Text */
		
		// Get the number of levels in the page tree
		$levels = count($texts);

		// We construct a title
		/* @var $text Text */
		$array = array();
		foreach($texts as $text) {

			// Add the current page's title			
			array_unshift($array, $text->getTitle());
		}
		$title = implode(', ', $array);

		// Get the last text in the array of texts. This is the text we want to
		// show in the view now:
		$text = array_pop($texts);
		
		// we determine which view we need to load
		switch ($text->getRealm()) {
			case 'projects':
				// Construct the view
				M_Loader::loadView('TextProjectListView', 'text');
				$view = new TextProjectListView();
				break;
			case 'publications':
				// Construct the view
				M_Loader::loadView('TextPublicationListView', 'text');
				$view = new TextPublicationListView();
				break;
			case 'company':
				// Construct the view
				M_Loader::loadView('TextCompanyView', 'text');
				$view = new TextCompanyView();
				break;
			case 'jobs':
				// Construct the view
				M_Loader::loadView('TextJobListView', 'text');
				$view = new TextJobListView();
				break;
			case 'contact':
				// Construct the view
				M_Loader::loadView('TextContactView', 'text');
				$view = new TextContactView();
				break;
			default :
				// Construct the default view
				M_Loader::loadView('TextGeneralView', 'text');
				$view = new TextGeneralView();
				break;
		}
		
		
		/* @var $view TextDetailView */
		// Provide the view with
		$view
			// The text to be shown
			->setText($text);
			
		
			
		$view
			// Additional view properties, such as the page title and description
			->setPageTitle($title)
			->setPageDescription($text->getText())
		
			->setActivePath($top->getUrl());
		
		// For each of the texts selected
		foreach($texts as $path => $currentText) {
			/* @var $currentText Text */
			// We add a breadcrumb entry to the view
			$view->addBreadcrumbEntry($currentText->getTitle(), $path);
		}
		
		// Finally, add a breadcrumb entry for the currently selected text:
		$view->addBreadcrumbEntry($text->getTitle());
		
		// Display the view
		$view->display();
	}
	
	/* -- PRIVATE / PROTECTED -- */
	
	/**
	 * Get text(s)
	 * 
	 * @access protected
	 * @param array $args
	 * 		The path element(s) to the text, which compose the complete URL to 
	 * 		the page
	 * @return array $texts
	 * 		The collection of {@link Text} instances that have been found for
	 * 		the elements in the path.
	 */
	protected function _getTextsWithPathElements(array $args) {
		// There should be at least 1 element!
		if(count($args) == 0) {
			// Return NULL, if not the case
			return NULL;
		}
		
		// We will fetch a text object for each of the elements in the
		// path, and store it in the $texts array:
		$texts = array();
		
		// Also, we will maintain the $currentParent variable, in order to
		// look up all pages, starting from element at index 1 (second element
		// in the path). Obviously, this variable is initiated to NULL, because
		// the first text in the path should not have a parent:
		/* @var $currentParent Text */
		$currentParent = NULL;
		
		// Path to the current page:
		$currentPath   = '';
		
		// For each of the elements in the path:
		foreach($args as $elm) {
			// We construct the mapper that looks up the page in the db
			/* @var $mapper TextMapper */
			$mapper = $this
				->_getTextMapper()
				->addFilterPublishedOnly();
			
			// If a parent text is available
			if($currentParent) {
				// Then, we add a filter to the mapper, so only children of 
				// that parent text are fetched from the database:
				$mapper->addFilterParentText($currentParent);
			}
			// If a parent text is not available
			else {
				// Then, we add a filter to the mapper, so only texts with
				// no parent at all are fetched from the database:
				$mapper->addFilterRootTextsOnly();
			}
			
			// Now, we fetch the text object that corresponds to the current
			// element in the path:
			/* @var $text Text */
			$text = $mapper->getByUrlSuffix($elm);
			
			// If the text has not been found:
			if(! $text) {
				// Then, we return NULL instead
				return NULL;
			}
			
			// Add the URL of the current text to the current path
			$currentPath .= empty($currentPath) ? $text->getUrl() : '/' . $text->getUrl();
			
			// We add the text to the array of texts. Note that we use the current
			// path as the key of the text:
			$texts[$currentPath] = $text;
			
			// And we set this text as the parent for the next one:
			$currentParent = $text;
		}
		
		// Return the result
		return $texts;
	}
	
	
	/**
	 * Get TextMapper
	 * 
	 * @access private
	 * @return TextMapper
	 */
	protected function _getTextMapper() {
		return $this->_getDataObjectMapper('Text', 'text');
	}
	
	
	/**
	 * retrieve the latest newsitem
	 * 
	 * @return News 
	 */
	protected function _getNews() {
		// construct the mapper that will fetch the newsitems from the db
		$mapper = M_Loader::getDataObjectMapper('News', 'news');
		/* @var $mapper NewsMapper */
		
		// add files to the mapper,
		$mapper
			// only published
			->addFilterPublishedOnly()
			// articles are sorted by publication date
			->addFilterSortByDate();

		// get the latest article
		return $mapper->getLatest(1)->getOne();
	}
}