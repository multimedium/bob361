<?php
/**
 * Filter on amount of media-items
 * 
 * By default this filter will add SQL syntax to an {@link M_DbSelect} to 
 * select only those rows which have at least one link to a media-item
 * 
 * @author Ben Brughmans
 */
class M_DbQueryFilterWhereMedia extends M_DbQueryFilterWhere  {

	/**
	 * Number of media-items
	 * 
	 * @var int
	 */
	private $_value = 0;
	
	/**
	 * Compare with operator
	 * 
	 * @var string
	 */
	private $_operator = self::GT;
	
	/**
	 * Search only for (un)published media-items
	 * 
	 * @var bool
	 */
	private $_published;

	/**
	 * Search only for a certain type of media-items
	 * 
	 * @var string
	 */
	private $_type;
	
	/**
	 * Construct new filter
	 * 
	 * @param int $value
	 * 		Filter amount of media-items
	 * @param string $operator
	 */
	public function __construct($value = null, $operator = null) {
		if (!is_null($value)) $this->_value = $value;
		if (!is_null($operator)) $this->_operator = $operator;
	}
	
	/**
	 * Get operator on which will be filtered
	 * @return string
	 */
	public function getOperator() {
		return $this->_operator;
	}
	
	/**
	 * Set filter operator
	 * 
	 * @param string $_operator
	 * @return M_DbQueryFilterWhereMedia
	 */
	public function setOperator($_operator) {
		$this->_operator = $_operator;
		return $this;
	}
	
	/**
	 * Get value on which will be filtered
	 * 
	 * @return int
	 */
	public function getValue() {
		return $this->_value;
	}
	
	/**
	 * @param int $_value
	 * @return M_DbQueryFilterWhereMedia
	 */
	public function setValue($_value) {
		$this->_value = $_value;
		return $this;
	}
	
	/**
	 * Do we need to filter on published media items?
	 * 
	 * Returns NULL when filter is disabled, TRUE when we are only searchring
	 * for published media-items, FALSE when we are searching for unpublished
	 * items.
	 * 
	 * @return bool
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published-filter
	 * 
	 * @see getPublished()
	 * @param bool $_published
	 * @return M_DbQueryFilterWhereMedia
	 */
	public function setPublished($_published) {
		$this->_published = $_published;
		return $this;
	}


	/**
	 * Get type of media to filter on
	 *
	 * @return string
	 */
	public function getType() {
		return $this->_type;
	}

	/**
	 * Set filter operator
	 *
	 * @param string $_operator
	 * @return M_DbQueryFilterWhereMedia
	 */
	public function setType($_type) {
		$this->_type = $_type;
		return $this;
	}
	
	/**
	 * Filter on 
	 * 
	 * @param M_DbSelect $query
	 * @param M_DataObjectMapper $mapper
	 * @return M_DbQuery $query
	 */
	public function filter(M_DbQuery $query, M_DataObjectMapper $mapper = null) {
		M_Loader::loadDataObjectMapper('MediaAlbum','media');
		$tableName = $mapper->getTableName();
		
		$query->joinLeft(
			MediaAlbumMapper::TABLE_MEDIA_ALBUM_X_OBJECT, 
			null, 
			'object_id', 
			$tableName . '.' . $mapper->getFieldDbName('id') . ' AND module = "'.$mapper->getModule()->getId().'" AND object = "'.$mapper->getDataObjectClassName().'"'
		);
		$query->joinLeft(
			'media',
			null, 
			'media.album_id', 
			MediaAlbumMapper::TABLE_MEDIA_ALBUM_X_OBJECT.'.album_id'
		);
		$query->group($tableName . '.' . $mapper->getFieldDbName('id'));
		$query->having('COUNT(media.id) != '.$this->_value);
		
		//filter on (un)published?
		if (!is_null($this->getPublished())) {
			$query->where('media.published = ?',(int)$this->getPublished());
		}

		//filter on type?
		if (!is_null($this->getType())) {
			M_Loader::loadDataObject('Media', 'media');
			switch ($this->getType()) {
				case Media::TYPE_IMAGE:
					$fileExtensions = Media::getFileExtensionsByType(Media::TYPE_IMAGE);
					break;
				case Media::TYPE_AUDIO:
					$fileExtensions = Media::getFileExtensionsByType(Media::TYPE_AUDIO);
					break;
				case Media::TYPE_VIDEO:
					$fileExtensions = Media::getFileExtensionsByType(Media::TYPE_VIDEO);
					break;
				case Media::TYPE_DOCUMENT:
					$fileExtensions = Media::getFileExtensionsByType(Media::TYPE_DOCUMENT);
					break;
				default:
					throw new M_DbException(sprintf('Unknown type %s', $this->getType()));
			}
			
			$query->whereIn('media.file_extension', $fileExtensions);
		}
		
		return $query;
	}
	
	/**
	 * Get fields on which this filter filters
	 * 
	 * @return array
	 */
	public function getFields() {
		return array();
	}
	
	/* -- Unimplemented functions -- */
	
	/**
	 * add condition
	 * 
	 * @return void
	 */
	public function addCondition() {
		throw new M_DbException(__FUNCTION__.' not implemented in '.__CLASS__);
	}
	
	/**
	 * reset conditions
	 * 
	 * @return void
	 */
	public function resetConditions() {
		throw new M_DbException(__FUNCTION__.' not implemented in '.__CLASS__);
	}
	
	/**
	 * reset all conditions
	 * 
	 * @return void
	 */
	public function resetAllConditions() {
		throw new M_DbException(__FUNCTION__.' not implemented in '.__CLASS__);
	}
	
	/**
	 * add subfilter
	 * 
	 * @return void
	 */
	public function addSubfilter() {
		throw new M_DbException(__FUNCTION__.' not implemented in '.__CLASS__);
	}
	
	/**
	 * reset subfilters
	 * 
	 * @return void
	 */
	public function resetSubfilters() {
		throw new M_DbException(__FUNCTION__.' not implemented in '.__CLASS__);
	}
}