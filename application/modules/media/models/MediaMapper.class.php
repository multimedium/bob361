<?php
/**
 * MediaMapper
 *
 * @package App
 * @subpackage Media
 */

// Media album mapper is used in different methods in this mapper
M_Loader::loadDataObjectMapper('MediaAlbum', 'media');

/**
 * MediaMapper
 *
 * @package App
 * @subpackage Media
 */
class MediaMapper extends M_DataObjectMapper {
	/**
	 * Get the active module folder name
	 *
	 * @return str
	 */
	protected function _getModuleName() {
		return 'media';
	}
	
	/**
	 * Get a filter which will query on the amount of media-items
	 * 
	 * Using this filter it is possible to filter all items e.g which have at
	 * least 1 media-item
	 * 
	 * @return M_DbQueryFilterWhereMedia
	 */
	public static function getFilterWhereMedia() {
		M_Loader::loadRelative(M_Loader::getModulePath('media').'/core/DbQueryFilterWhereMedia.class.php');
		return new M_DbQueryFilterWhereMedia();
	}
	
	/**
	 * Get all the media for an album
	 * 
	 * @param MediaAlbum $album
	 * @return M_ArrayIterator
	 */
	public function getMediaByAlbum(MediaAlbum $album) {
		return $this->getMediaByAlbumId($album->getId());
	}
	
	/**
	 * Get all the media for an album-id
	 * 
	 * @param int
	 * @return M_ArrayIterator
	 */
	public function getMediaByAlbumId($id) {
		$result = $this->_getFilteredSelect()
			->where($this->getFieldDbName('albumId') . ' = ?', (int) $id)
			->execute();
		
		return $this->_fetchAll($result);
	}
	
	/**
	 * Get random media for an album-id
	 * 
	 * @param int
	 * @return M_ArrayIterator
	 */
	public function getRandomMediaByAlbumId($id, $count = 1) {
		$result = $this->_getFilteredSelect()
			->where($this->getFieldDbName('albumId') . ' = ?', (int) $id)
			->orderRandom()
			->limit(0,(int) $count)
			->execute();
		
		return $this->_fetchAll($result);
	}
	
	/**
	 * Get media count for an album
	 * 
	 * @param MediaAlbum $album
	 * @return M_ArrayIterator
	 */
	public function getMediaCountByAlbum(MediaAlbum $album) {
		return $this->getMediaCountByAlbumId($album->getId());
	}
	
	/**
	 * Get media count for an album-id
	 * 
	 * @param int
	 * @return M_ArrayIterator
	 */
	public function getMediaCountByAlbumId($id) {
		$result = $this->_getFilteredSelect()
			->columnsLiteral('COUNT(0)')
			->where($this->getFieldDbName('albumId') . ' = ?', (int) $id)
			->execute();
		
		return (int) $result->getFirstValue();
	}
	
	/**
	 * Add a filter to this mapper so only (un)published media will be retrieved
	 *
	 * @param bool $published
	 * @return MediaMapper
	 */
	public function addFilterPublished($published = true) {
		$this->addFilter(new M_DbQueryFilterWhere('published', (bool)$published));
		return $this;
	}
	
	/**
	 * Add a filter to this mapper so media-items are sorted using {@link Media::getOrder()}
	 *
	 * @return MediaMapper
	 */
	public function addFilterOrder() {
		$this->addFilter(new M_DbQueryFilterOrder('albumId'));
		$this->addFilter(new M_DbQueryFilterOrder('order'));
		return $this;
	}
	
	/**
	 * Add a filter to this mapper so only media-items of a certain type will
	 * be retrieved
	 * 
	 * @see Media::TYPE_IMAGE
	 * @see Media::TYPE_VIDEO
	 * @see Media::TYPE_AUDIO
	 * @see Media::TYPE_DOCUMENT
	 *
	 * @param string $type
	 * @return MediaMapper
	 */
	public function addFilterType($type) {
		$this->addFilter(new M_DbQueryFilterWhere('type', $type));
		return $this;
	}
	
	/**
	 * Add a filter to this mapper so only images will be retrieved
	 * 
	 * @return MediaMapper
	 */
	public function addFilterImage() {
		M_Loader::loadDataObject('Media', 'media');
		return $this->addFilterType(Media::TYPE_IMAGE);
	}

	/**
	 * Filter results by MediaAlbum
	 *
	 * @param MediaAlbum $album
	 * @return MediaMapper
	 */
	public function addFilterMediaAlbum(MediaAlbum $album) {
		return $this->addFilterMediaAlbumId($album->getId());
	}

	/**
	 * Filter results by MediaAlbumId
	 *
	 * @param Int $id
	 * @return MediaMapper
	 */
	public function addFilterMediaAlbumId($id) {
		return $this->addFilter(
			new M_DbQueryFilterWhere('albumId', (int)$id)
		);
	}

	/**
	 * Filter results by a collection of MediaAlbum
	 *
	 * @param M_ArrayIterator $albums
	 *		The collection of {@link MediaAlbum} instances
	 * @return MediaMapper
	 */
	public function addFilterMediaAlbums(M_ArrayIterator $albums) {
		// Make sure collection of MediaAlbum instances has been provided
		if(! M_Helper::isIteratorOfClass($albums, 'MediaAlbum')) {
			// Inform about error
			throw new M_Exception(sprintf(
				'Cannot add filter Media Albums with the provided iterator. ' .
				'The elements are not instances of MediaAlbum'
			));
		}
		
		// Add the filter:
		$this->addFilterMediaAlbumIds($albums->toRecursiveArray(null, 'getId'));
	}

	/**
	 * Filter results by MediaAlbumId
	 *
	 * @param Int $id
	 * @return MediaMapper
	 */
	public function addFilterMediaAlbumIds(array $ids) {
		return $this->addFilter(
			new M_DbQueryFilterWhere('albumId', $ids, M_DbQueryFilterWhere::IN)
		);
	}
	
	/**
	 * Insert a new media object
	 * 
	 * Before inserting, we need to set the dateAdded value
	 * 
	 * @param Media $media
	 * @return bool
	 */
	public function insert(Media $media) {
		$this->_validateSave($media);
		$media->setDateAdded(new M_Date());
		return parent::insert($media);
	}
	
	/**
	 * Delete media and the file which it's related with
	 * 
	 * @param Media
	 * @param bool $forced 
	 * 		Force delete, if set to TRUE there won't be any validation
	 * 		if the file actually exists
	 * @return bool
	 */
	public function delete(Media $media, $forced = false) {
		try {
			$this->_validateSave($media);
			// try to delete the file
			$media->getFile()->delete();
		}catch(M_Exception $e) {
			//only throw exception when we don't want to force this delete
			if (!$forced) throw $e;
		}
		
		// delete object from database
		return parent::delete($media);
	}
	
	/**
	 * Delete all the media for a an album
	 * 
	 * @param MediaAlbum $album
	 * @return bool
	 */
	public function deleteMediaByAlbum(MediaAlbum $album) {
		// Get the collection of media items:
		$mediaCollection = $this->getMediaByAlbum($album);
		
		// For each of the media items in the collection
		foreach( $mediaCollection AS $media) {
			// Delete the media item
			if (!$media->delete(true)) {
				// If we failed to delete, we return FALSE
				return false;
			}
		}
		
		// If we're still here, everything should have went fine. We return TRUE!
		return true;
	}
	
	/**
	 * Check if a media item can be saved. If anything goes wrong, an 
	 * exception will be thrown
	 * 
	 * @param $media Media
	 * @return void
	 */
	private function _validateSave(Media $media) {
		if ($media->getFile() == false || ($media->getFile() && $media->getFile()->exists() == false)) {
			throw new M_Exception(sprintf(
				'Cannot save media with title "%s" without a file', 
				$media->getTitle()
			));
		}
	}
}