<?php
/**
 * DataObjectWithMedia
 *
 * ...
 * Can be used as a superclass for data objects to which media items are
 * being attached in Multimaange CMS.
 *
 * @package App
 * @subpackage Media
 */
abstract class DataObjectWithMedia extends M_DataObject {

	/* -- GETTERS -- */

	/**
	 * Get image
	 *
	 * Will provide with the (first) image of the instance, which has been attached
	 * to the instance with the Media module. Note that this method will return
	 * (boolean) FALSE if no image could have been found
	 *
	 * @access public
	 * @return M_File
	 */
	public function getImage() {
		$media = $this->getOneMedia();
		if ($media) {
			return $media->getFile();
		}
		else return false;
	}

	/**
	 * Get one media-object
	 * 
	 * @return Media
	 * @author Ben Brughmans
	 */
	public function getOneMedia() {
		// Get the album
		$album = $this->getMediaAlbum();

		// If no album could have been found, we return FALSE
		if(! $album) {
			return FALSE;
		}

		// Get the first media item of type image
		M_Loader::loadDataObject('Media', 'media');
		return $this
			->getMedia(Media::TYPE_IMAGE, 1)
			->current();
	}

	/**
	 * Get a random media-object
	 * @return Media
	 * @author Ben Brughmans
	 */
	public function getRandomMedia() {
		// Get the album
		$album = $this->getMediaAlbum();

		// If no album could have been found, we return FALSE
		if(! $album) {
			return FALSE;
		}

		// Get the first media item of type image
		M_Loader::loadDataObject('Media', 'media');
		return $this
			->getMedia(Media::TYPE_IMAGE, 1, TRUE)
			->current();
	}

	/**
	 * Get media album
	 *
	 * Will provide with the Media Album that has been attached to the instance.
	 * Note that this method will return (boolean) FALSE if the album could not
	 * have been found
	 *
	 * @access public
	 * @return MediaAlbum $album
	 */
	public function getMediaAlbum() {
		// Media gets attached to the event, by assign a media album to the object.
		// We get the MediaAlbum mapper, to fetch the album that has been attached
		// to the object:
		$mediaAlbumMapper = M_Loader::getDataObjectMapper('MediaAlbum', 'media');
		$mediaAlbumMapper->addFilterPublished();

		$reg = M_Registry::getInstance();
		$album = $reg->get('album-'.$this->getClassName().'-'.$this->getId());
		if ($album) {
			return $album;
		}else {
			$album = $mediaAlbumMapper->getAlbumByObject($this);
			$reg->__set('album-'.$this->getClassName().'-'.$this->getId(), $album);
		}
		// Look up the media album:
		return $album;
	}

	/**
	 * Get media items
	 *
	 * Will provide with a collection of Media items, which has been attached to
	 * this object with the Media module.
	 *
	 * @access public
	 * @param string $type
	 * 		The type of media items you want to get
	 * @param integer $maximumNumberOfItems
	 * 		The (maximum) number of items you want to get
	 * @param bool $randomOrder
	 *		If TRUE, a random media item will be fetched instead of the first
	 * @return M_ArrayIterator $media
	 */
	public function getMedia($type = 'image', $maximumNumberOfItems = NULL, $randomOrder = false) {
		// Get the album
		$album = $this->getMediaAlbum();

		// If no album could have been found, we return an empty collection
		if(! $album) {
			return new M_ArrayIterator(array());
		}

		// Prepare a mapper that will fetch the requested media items:
		$mapper = M_Loader
			::getDataObjectMapper('Media', 'media')
			->addFilter(new M_DbQueryFilterWhere('published', 1))
			->addFilter(new M_DbQueryFilterWhere('type', $type));

		// Add an order filter, based on wheter we have to fetch a random
		// item or not
		if ($randomOrder) {
			$mapper->addFilter(new M_DbQueryFilterOrderRandom());
		}else {
			$mapper->addFilter(new M_DbQueryFilterOrder('order', 'ASC'));
		}

		// Fetch a maximum number?
		if($maximumNumberOfItems) {
			$mapper->addFilter(new M_DbQueryFilterLimit(0, (int) $maximumNumberOfItems));
		}

		// Return the media items
		return $mapper->getMediaByAlbumId($album->getId());
	}
}