<?php
/**
 * MediaAlbum class
 * 
 * @package App
 * @subpackage Media
 */

// Load superclass (auto-generated)
M_Loader::loadDataObject('MediaAlbumDataObject','media');

/** 
 * MediaAlbum class
 * 
 * @see MediaAlbumDataObject
 * @see MediaAlbumMapper
 * @package App
 * @subpackage Media
 */
class MediaAlbum extends MediaAlbumDataObject {
	/**
	 * media
	 * 
	 * Holds all the media for this album
	 * 
	 * @var M_ArrayIterator
	 */
	protected $_media;

	/**
	 * Set title
	 *
	 * @access public
	 * @param mixed $text
	 * @return mixed
	 */
	public function setTitle($title, $locale = NULL) {
		// Set the title
		parent::setTitle($title, $locale);

		// If no URL Suffix exists yet for this text:
		if(! $this->getUrl($locale)) {
			// Set the URL Suffix now:
			$this->setUrl(M_Uri::getPathElementFromString($title), $locale);
		}
	}


	/**
	 * Get all media for this album
	 * 
	 * @param bool $refresh
	 * 		If $refresh is true the media will be retreived again, otherwise
	 * 		a cached value will be used to boost performance
	 * @return M_ArrayIterator
	 */
	public function getMedia($refresh = false) {
		if (is_null($this->_media) || $refresh === true) {
			$this->_media = $this->getMapper()->getMediaByAlbum($this);
		}
		return $this->_media;
	}
	
	/**
	 * Delete all media from this album
	 * 
	 * Remove all media-items from this album
	 * 
	 * @return bool
	 * @return M_ArrayIterator
	 */
	public function deleteMedia() {
		return $this->getMapper()->deleteMedia($this);
	}
	
	/**
	 * Get all media of type image
	 * 
	 * @see Media::TYPE_IMAGE
	 * @param bool $published
	 * @return M_ArrayIterator
	 */
	public function getImages($published = null) {
		M_Loader::loadDataObject('Media', 'media');
		$mapper = $this->getMapper();
		
		$filters = array();
		if (!is_null($published)) {
			$filters[] = new M_DbQueryFilterWhere('published', (bool)$published);
		}
		$filters[] = new M_DbQueryFilterOrder('order');
		
		return $mapper->getMediaByAlbum($this, Media::TYPE_IMAGE,$filters);
	}
	
	/**
	 * Get the x-th image of this album
	 * 
	 * @param bool $published
	 * @param int $index
	 * @return Media
	 */
	public function getImage($published = null, $index = 0) {
		M_Loader::loadDataObject('Media', 'media');
		$mapper = $this->getMapper();
		$filters = array();
		if (!is_null($published)) {
			$filters[] = new M_DbQueryFilterWhere('published', (bool)$published);
		}
		$filters[] = new M_DbQueryFilterLimit((int)$index, 1);
		return $mapper->getMediaByAlbum($this, Media::TYPE_IMAGE, $filters)->current();
	}
	
	/**
	 * Get all media of type document
	 * 
	 * @see Media::TYPE_DOCUMENT
	 * @return M_ArrayIterator
	 */
	public function getDocuments() {
		M_Loader::loadDataObject('Media', 'media');
		return $this->getMapper()->getMediaByAlbum($this, Media::TYPE_DOCUMENT);
	}
	
	/**
	 * Get all media of type audio
	 * 
	 * @see Media::TYPE_AUDIO
	 * @return M_ArrayIterator
	 */
	public function getAudio() {
		M_Loader::loadDataObject('Media', 'media');
		return $this->getMapper()->getMediaByAlbum($this, Media::TYPE_AUDIO);
	}
	
	/**
	 * Get all media of type video
	 * 
	 * @see Media::TYPE_VIDEO
	 * @return M_ArrayIterator
	 */
	public function getVideo() {
		M_Loader::loadDataObject('Media', 'media');
		return $this->getMapper()->getMediaByAlbum($this, Media::TYPE_VIDEO);
	}
	
	/**
	 * Count the number of media items
	 *
	 * @param bool Select (non) published items only
	 * @return int
	 */
	public function getMediaCount($published = null) {
		return $this->getMapper()->getMediaCount($this, $published);
	}

	/**
	 * Get Media Count Recursive
	 *
	 * Will calculate and return the total count of all {@link Media} items
	 * in this album AND all child albums, including their child albums'
	 * {@link Media} as well, and so on.
	 *
	 * @access public
	 * @param bool $albumPublished
	 *		If set to TRUE, this function will only take in account child
	 *		albums that are currently published, FALSE if not
	 * @param bool $mediaPublished
	 *		If set to TRUE, this function will only take in account {@link Media}
	 *		items that are currently published, FALSE if not
	 * @return int
	 */
	public function getMediaCountRecursive($albumPublished = true, $mediaPublished = true) {
		// First, fetch the media count from this album alone
		$count = $this->getMediaCount($mediaPublished);

		// If there are child albums attached to this one:
		if($this->getNumberOfChildAlbums($albumPublished) > 0) {
			// Fetch the child albums
			$childs = $this->getChildAlbums($albumPublished);

			// Iterate through them
			foreach($childs as $child) {
				// Add to the media count. Note that we also call the recursive
				// function here, so we allow for infinite album stacking :)
				$count += $child->getMediaCountRecursive($mediaPublished);
			}
		}

		// Return the count
		return $count;
	}

	/**
	 * get mapper
	 * 
	 * @return MediaAlbumMapper
	 */
	public function getMapper() {
		if (is_null($this->_mapper)) {
			M_Loader::loadDataObjectMapper( 'MediaAlbum', 'media' );
			$this->_mapper = new MediaAlbumMapper();
		}
		
		return $this->_mapper;
	}

	/**
	 * Get parent album
	 *
	 * NOTE:
	 * Will return NULL if the parent could not have been found.
	 *
	 * @access public
	 * @return void
	 */
	public function getParentAlbum() {
		if($this->getParentAlbumId()) {
			$album = $this->getMapper()
				->resetFilters()
				->addFilterPublished()
				->getById($this->getParentAlbumId());

			if($album) {
				return $album;
			}
		}

		return NULL;
	}

	/**
	 * Check if this is a root album (and thus is not attached to any other
	 * albums)
	 *
	 * @return bool
	 */
	public function isRootAlbum() {
		return !$this->getParentAlbumId();
	}

	/**
	 * Get URL of parent
	 *
	 * Will look up the URL of the parent (recursively). Typically used to compose
	 * the URL to the page inside the website.
	 *
	 * @access public
	 * @return string
	 */
	public function getRecursiveParentUrl() {
		// Output
		$u = array();

		// Working variables
		$m = $this;
		$l = array();

		// For each of the parents:
		while(is_object($m = $m->getParentAlbum())) {
			// If the current parent has not been added already
			if(! in_array($m->getId(), $l)) {
				// Then, add to output
				array_unshift($u, $m->getUrl());
			}
			// Break out of infinite loop:
			else {
				break;
			}

			// Log, to prevent infinite loop
			$l[] = $m->getId();
		}

		// Return output
		return implode('/', $u);
	}

	/**
	 * Get child albums
	 *
	 * @access public
	 * @param bool $published
	 * @return M_ArrayIterator $albums
	 * 		The matching objects; instances of {@link MediaAlbum}
	 */
	public function getChildAlbums($published = true) {
		return $this->getMapper()
			->resetFilters()
			->addFilterPublished($published)
			->addFilterDefaultSorting()
			->addFilterParentAlbum($this)
			->getAll();
	}

	/**
	 * Get number of child albums
	 *
	 * @access public
	 * @param bool $published
	 * @return integer $count
	 */
	public function getNumberOfChildAlbums($published = true) {
		return $this->getMapper()
			->resetFilters()
			->addFilterPublished($published)
			->addFilterParentAlbum($this)
			->getCount();
	}
	
	/**
	 * Check if this album is deletable
	 *
	 * @return bool
	 */
	public function isDeletable() {
		return $this->getDeletable() ? true : false;
	}
}