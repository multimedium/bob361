<?php
class M_FieldMediaAlbum extends M_FieldSelect  {

	/**
	 * The maximum amount of items to show in the popupmenu. If the data-result
	 * exceeds this amount we will switch view into autocomplete
	 */
	const MAX_ITEMS_POPUPMENU = 4;
	
	/**
	 * When media items are displayed in the form field they will be shown
	 * with the following size in pixels
	 */
	const THUMBNAIL_SIZE = 30;

	/**
	 * When media items are displayed in the form field they will be shown
	 * with the following thumbnail-type
	 *
	 */
	const THUMBNAIL_TYPE = 'square';
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldMediaAlbum} object,
	 * which allows {@link M_FieldMediaAlbum} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @return M_ViewFieldMediaAlbum
	 */
	public function getInputView() {
		// Load necessary classes
		AdminLoader::loadCoreClass('ViewFieldMediaAlbum', 'media');
		
	    //set the options
		M_Loader::loadDataObjectMapper('MediaAlbum', 'media');
		$mediaAlbumMapper = new MediaAlbumMapper();
		$filter = new M_DbQueryFilterOrder('title');
		$mediaAlbumMapper->addFilter($filter);

		//attach filters defined in config
		foreach(self::getFilters() AS $filter) {
			$mediaAlbumMapper->addFilter($filter);
		}
		
		/* @var $mediaAlbum MediaAlbum */
		$mediaAlbumResult = $mediaAlbumMapper->getAll();
		
		//check total amount, and render correct field
		if( $mediaAlbumResult->count() > self::MAX_ITEMS_POPUPMENU) {
			$field = new M_FieldMediaAlbumAutocomplete($this->getId());
			
			//set the textvalue for this field
			$textValue = M_Request::getVariable(
				$field->getId() . M_ViewFieldMediaAlbumAutocomplete::TEXTVALUE_SUFFIX
			);
			
			//if no textvalue has been found, we need to retrieve it from db
			if (!$textValue && $this->getValue()) {
				$mediaAlbum = $mediaAlbumMapper->getById($this->getValue());
				//if it still exists
				if ($mediaAlbum) $textValue = $mediaAlbum->getTitle();
			}
			
			//set the text value
			$field->setTextValue($textValue);
			$field->setDefaultValue($this->getValue());
			$view = $field->getInputView();
			
			//get the description which has been set in the autocomplete field
			//this is done in getInputView, so we can only do this after this
			//has been called
			$this->setDescription($field->getDescription());
			
			return $view;
			
		}
		//popupmenu
		else {
			$mediaAlbums = array();
			foreach($mediaAlbumResult AS $mediaAlbum) {
				$mediaAlbums[$mediaAlbum->getId()] = $mediaAlbum;
			}
			$this->setItems($mediaAlbums);
			
			return new M_ViewFieldMediaAlbum($this);
		}
	}

	/**
	 * Get a list of filters which are defined in config
	 * 
	 * @return array List of filters defined in config
	 * @todo use a more global way to add filters to fields
	 */
	public static function getFilters() {
		// get the config for the filters
		$config = M_Application::getConfigOfModule('media');
		$configField = $config->fieldMediaAlbum;
		if(!empty($configField)) $configFilters = $configField->filters;

		$filters = array();
		
		// if mediaAlbum filters found in config: add the filters
		if(!empty($configFilters)) {
			foreach($configFilters as $filter) {
				// Check if field is given
				if(empty($filter->field)) {
					// If not: throw exception
					throw new M_Exception(sprintf(
						'Could not add filter to M_FieldMediaAlbum: no "field" given in config'
					));
				}
				// Check if field is given
				if(!isset($filter->value)) {
					// If not: throw exception
					throw new M_Exception(sprintf(
						'Could not add filter to M_FieldMediaAlbum: no "value" given in config for field "%s"',
						$filter->field
					));
				}

				//use default EQ comparison
				$comparison = null;

				// Check if comparison is given
				if(!empty($filter->comparison)) {
					$comparison = $filter->comparison;
				}

				$filters[] = new M_DbQueryFilterWhere(
					$filter->field,
					(string)$filter->value,
					$comparison
				);
			}
		}

		return $filters;
	}
}