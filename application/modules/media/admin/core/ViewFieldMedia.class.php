<?php
class M_ViewFieldMedia extends M_ViewField {
	
	/**
	 * Get the resource from the module
	 * 
	 * @return M_ViewHtmlResource 
	 */
	protected function getResource() {
		//make sure javascript and css file are added
		$js = M_ViewJavascript::getInstance();
		
		if ($js->isContextStored('admin-media') == false) {
			$file = new M_File(
				M_Loader::getAbsolute(
					AdminLoader::getAdminResourcesPath('media') . DIRECTORY_SEPARATOR .
					M_ViewHtml::FOLDER_JAVASCRIPT . DIRECTORY_SEPARATOR .
					'media.js'
				)
			);
			$js->addFile($file, 'admin-media');
		}

		$css = M_ViewCss::getInstance();
		if ($css->isContextStored('admin-media') == false) {
			$file = new M_File(
				M_Loader::getAbsolute(
					AdminLoader::getAdminResourcesPath('media') . DIRECTORY_SEPARATOR . 
					M_ViewHtml::FOLDER_CSS . DIRECTORY_SEPARATOR . 
					'media.css'
				)
			);
			$css->addFile($file,'admin-media');
		}
		
		//get template		
		$rs = new M_ViewHtmlResource(
			AdminLoader::getAdminResourcesPath('media') . DIRECTORY_SEPARATOR .  
			M_ViewHtml::FOLDER_TEMPLATES . DIRECTORY_SEPARATOR . 
			'FieldMedia.tpl'
		);
		
		//create albumfilter field
		$albumObjects = $this->_getMediaAlbumMapper()
			->addFilterRootAlbumsOnly()
			->addFilter(new M_DbQueryFilterOrder('title'))
			->getAll();

		$albumField = false;
		
		if ($albumObjects->count() > 0) {
			foreach($albumObjects AS $album) {
				/* @var $album MediaAlbum */
				$albums[$album->getId()] = $album->getTitle() . ' ('.$album->getMediaCount().')';

				//append child albums to $albums - array
				$this->_getAlbumOfParent($albums, $album);
			}
			
			$albumField = new M_FieldSelect('albumFilter');
			$albumField->set('items',$albums);
			$albumField->setTitle(t('Filter on album'));
		}
		
		//assign vars
		$rs->assign('items', $this->getField()->getItems());
		$rs->assign('field', $this->getField());
		$rs->assign(
			'resourcesPath',  
			M_Loader::getAbsolute(AdminLoader::getAdminResourcesPath('media'))
		);
		
		if ($albumField) {
			$albumField = $albumField->getView()->fetch();
		}
		$rs->assign('albumField', $albumField);
		
		return $rs;
	}
	
	public function getHtml() {
		return $this->getResource()->fetch();
	}

	/**
	 * Get all the child albums of a parent
	 * 
	 * @param array $albums
	 * @param MediaAlbum $album
	 * @param string $prefix
	 * @return void
	 */
	private function _getAlbumOfParent(array &$albums, MediaAlbum $album, $prefix = '--') {

		/* @var $childAlbum MediaAlbum */
		foreach($album->getChildAlbums() AS $childAlbum) {
			$albums[$childAlbum->getId()] = $prefix.' '.$childAlbum->getTitle() . ' ('.$childAlbum->getMediaCount().')';

			$this->_getAlbumOfParent($albums, $childAlbum, $prefix.$prefix);
		}
	}

	/**
	 * Get the media album mapper
	 * 
	 * @return MediaAlbumMapper
	 */
	private function _getMediaAlbumMapper() {
		return M_Loader::getDataObjectMapper('MediaAlbum', 'media');
	}
}