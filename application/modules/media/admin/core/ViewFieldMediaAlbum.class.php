<?php
class M_ViewFieldMediaAlbum extends M_ViewFieldSelect {
	/**
	 * Get the resource from the module
	 * 
	 * @return M_ViewHtmlResource 
	 */
	protected function getResource() {
		//make sure javascript and css file are added
		$js = M_ViewJavascript::getInstance();
		if ($js->isContextStored('admin-media') == false) {
			$file = new M_File(
				M_Loader::getAbsolute(
					AdminLoader::getAdminResourcesPath('media') . DIRECTORY_SEPARATOR .
					M_ViewHtml::FOLDER_JAVASCRIPT . DIRECTORY_SEPARATOR .
					'media.js'
				)
			);
			$js->addFile($file, 'admin-media');
		}
		$css = M_ViewCss::getInstance();
		if ($css->isContextStored('admin-media') == false) {
			$file = new M_File(
				M_Loader::getAbsolute(
					AdminLoader::getAdminResourcesPath('media') . DIRECTORY_SEPARATOR . 
					M_ViewHtml::FOLDER_CSS . DIRECTORY_SEPARATOR . 
					'media.css'
				)
			);
			$css->addFile($file,'admin-media');
		}
		
		//get template		
		$rs = new M_ViewHtmlResource(
			AdminLoader::getAdminResourcesPath('media') . DIRECTORY_SEPARATOR .  
			M_ViewHtml::FOLDER_TEMPLATES . DIRECTORY_SEPARATOR . 
			'FieldMediaAlbum.tpl'
		);
		
		//get the selected media-album
		$mediaAlbum = false;
		$value = $this->getField()->getValue();
		if ($value) {
			M_Loader::loadDataObjectMapper('MediaAlbum', 'media');
			$mediaAlbumMapper = new MediaAlbumMapper();
			$mediaAlbum = $mediaAlbumMapper->getById($value);
		}
		
		//assign vars
		$rs->assign('albums', $this->getField()->getItems());
		$rs->assign('field', $this->getField());
		$rs->assign('mediaAlbum', $mediaAlbum);
		$rs->assign(
			'resourcesPath',  
			M_Loader::getAbsolute(AdminLoader::getAdminResourcesPath('media'))
		);
		
		return $rs;
	}
}