<?php
class AdminMediaController extends M_Controller {
	
	/**
	 * Get all media for an album
	 * 
	 * @param int $albumId
	 * 		Filter on an album
	 */
	public function mediaFieldAjax($albumId = null) {
		M_Loader::loadDataObject('Media', 'media');
		M_Loader::loadDataObjectMapper('Media', 'media');
		$mediaMapper = new MediaMapper();
		
		//only add images which are published
		$filter = new M_DbQueryFilterWhere('type', Media::TYPE_IMAGE);
		$filter->addCondition('published', 1);
		$mediaMapper->addFilter($filter);
		
		//filter on media album?
		if ((int)$albumId > 0) {
			$mediaObjects = $mediaMapper->getMediaByAlbumId(
				$albumId
			);
		}else {
			$mediaObjects = $mediaMapper->getAll();
		}
		
		//get config settings
		//$module = new M_ApplicationModule('media');
		//$linkFancyBoxPrefix = $module->getConfig()->get('imageLinkPrefix');

		//output the file preview
		foreach($mediaObjects AS $media) {
			/* @var $media Media */
			$filePreview = new AdminViewFilePreview();
			$filePreview->setFile($media->getFile());
			//remove the link: this causes problems with drag'n drop in IE
			//$filePreview->setLink(M_Request::getLink('thumbnail/' . $linkFancyBoxPrefix . '/'.$media->getFile()->getBasename()));
			$filePreview->setCreateLink(false);
			$filePreview->setClass('media');
			$filePreview->display();
		}		  	
	}
	
	/**
	 * Query for media albums in an autocomplete field
	 *
	 */
	public function autocompleteMediaAlbumFieldAjax() {
		$query = M_Request::getVariable('q');
		$limit = M_Request::getVariable('limit');
		
		$filter = new M_DbQueryFilterWhere(
			'title', 
			$query, 
			M_DbQueryFilterWhere::CONTAINS
		);
		
		$filterLimit = new M_DbQueryFilterLimit(0,$limit);

		M_Loader::loadDataObjectMapper('MediaAlbum', 'media');
		$mediaAlbumMapper = new MediaAlbumMapper();

		//add filters which are defined in config
		foreach(M_FieldMediaAlbum::getFilters() AS $configFilter) {
			$mediaAlbumMapper->addFilter($configFilter);
		}

		$list = $mediaAlbumMapper->addFilter($filter)
			->addFilter($filterLimit)
			->getAll();
		
		AdminLoader::loadView('MediaAlbumAutocompleteAjaxView', 'media');
		$view = new MediaAlbumAutocompleteAjaxView();
		$view->setMediaAlbums($list);
		$view->display();
	}
	
	/**
	 * Generates a form in order to upload multiple media-items at once
	 *
	 */
	public function import() {
		// Get the definition of the MediaImportForm
		AdminLoader::loadCoreClass('AdminImportMediaFormDefinition', 'media');
		$definition = new AdminImportMediaFormDefinition();

		// Get the form, based on the defintion
		$form = $definition->getForm();

		// Load the form-view
		AdminLoader::loadView('MediaImportFormView', 'media');
		$view = new MediaImportFormView();
		
		if(!$form->run()) {
			// Assign the form
			$view->setForm($form);
			// Display
			$view->display();
		}
	}
	
	/**
	 * Truncate (or empty) all media for one album
	 */
	public function truncate($albumId) {
		$mapper = M_Loader::getDataObjectMapper('MediaAlbum', 'media');
		/* @var $mapper MediaAlbumMapper */
		$album = $mapper->getById($albumId);
		/* @var $album MediaAlbum */

		$res = $album->deleteMedia();

		if ($res) {
			M_Messenger::getInstance()->addMessage(
				t('All media in album <strong>@album</strong> has been deleted', array('@album' => $album->getTitle())),
				M_Messenger::SUCCESS
			);
		}else {
			M_Messenger::getInstance()->addMessage(
				t('Media for album <strong>@album</strong> could not be deleted', array('@album' => $album->getTitle())),
				M_Messenger::SUCCESS
			);
		}

		//redirect back to mediaalbum overview page
		M_Header::redirect(M_Request::getLink('admin/overview/media/mediaAlbum'));
	}

	/**
	 * 
	 * Upload one media-item
	 * 
	 * @return void
	 */
	public function uploadMediaItem() {
	    $file_path = M_Loader::getAbsolute('files/temp');
	    
		AdminLoader::loadView('MediaImportResultView', 'media');
		$view = new MediaImportResultView();

		if(isset($_FILES['Filedata']) &&  is_array($_FILES['Filedata'])) {
	
				//$console->write('Er komt een bestand binnen');
			    $file_temp = $_FILES['Filedata']['tmp_name'];
			    $file_name = $_FILES['Filedata']['name'];
			
				// Now, make sure we get a valid file in the import:
				$checkFileExtension = new M_File($file_name);
				
				// Get the definition of the MediaImportForm
				AdminLoader::loadCoreClass('AdminImportMediaFormDefinition', 'media');
				$definition = new AdminImportMediaFormDefinition();
				$mediaForm = $definition->getForm();
				
				// Get allowed extensions and check it!
				$allowedExtensions = $mediaForm->getField('uploadMedia')->getAllowedExtensions();
				if(! in_array(strtolower($checkFileExtension->getFileExtension()), $allowedExtensions)) {
					$view->setSuccess('false', t('Upload failed'));
					$view->display();
					die();
				}
				
   		        //complete upload
		        $filestatus = @move_uploaded_file($file_temp, $file_path."/".$file_name);
		        
			      if(!$filestatus) {
			         $view->setSuccess('false', t('Upload failed'));
			      } else { 
			      	$file = new M_File($file_path."/".$file_name);
			      	$file->setName(M_Helper::getUniqueToken() .'.'. $file->getFileExtension());
			      	
			      	M_Loader::loadDataObject('Media', 'media');

					if(in_array(strtolower($file->getFileExtension()), Media::getImageFileExtensions())) {
						$image = new M_Image($file->getPath());

						$maxWidth = $this->_getConfig()->image->maxUploadWidth;
						$maxHeight = $this->_getConfig()->image->maxUploadHeight;

						if($maxWidth >= $image->getWidth() && $maxHeight >= $image->getHeight()) {
							// Check if autoResize is TRUE and the file is an image
							if(in_array(strtolower($file->getFileExtension()), Media::getImageFileExtensions())) {
									if(M_Helper::isBooleanTrue($this->_getConfig()->image->autoResize)) {
										// Resize the image
										$file = $this->_resizeImage($file);
									}

									$view->setOriginal($file_name);
									$view->setFile($file->getPath());
									$view->setSuccess('true');
							}
						} else {
							$view->setSuccess('false', t('The document\'s dimensions exceed the maximum size.') . ' (' . t('Max size:') . ' ' . $maxWidth .' x ' . $maxHeight . 'px)');
							$file->delete();
						}
					} else {
						$view->setOriginal($file_name);
						$view->setFile($file->getPath());
						$view->setSuccess('true');
					}
			      }
	
		} else {
			$view->setSuccess('false', 'No action requested');
		}
		
		$view->display();

	}
	
	/**
	 * Resize image
	 * 
	 * @param M_File $file
	 */
	private function _resizeImage(M_File $file) {
		
		// Create new image
		$image = new M_Image($file->getPath());
		
		// Create new filter
		$filter = new M_FilterImageResize(
			new M_FilterImage($image), 
			$this->_getConfig()->image->maxWidth,
			$this->_getConfig()->image->maxHeight
		);
		
		// Return the resized image
		return $filter->apply()->saveToFile(
			$file->getDirectoryAndFilename(), 
			strtolower($image->getFileExtension())
		);
	}
	
	/**
	 * Get the config of the media module
	 */
	private function _getConfig() {
		$module = new M_ApplicationModule('media');
		$moduleConfig = $module->getConfig();
		
		return $moduleConfig;
	}
}