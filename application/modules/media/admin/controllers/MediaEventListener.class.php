<?php
/**
 * MediaEventListener
 */
class MediaEventListener {
	
	/**
	 * @var MediaMapper
	 */
	protected $_mediaMapper;
	
	/**
	 * @var MediaAlbumMapper
	 */
	protected $_mediaAlbumMapper;
	
	/**
	 * Construct
	 *
	 * @param AdminController $controller
	 */
	public function __construct(AdminController $controller) {
		// Add Event listeners
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_SAVE, 
			array($this, 'onDataObjectBeforeSave')
		);
		
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_SAVE, 
			array($this, 'onDataObjectSave')
		);
		
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_EDIT, 
			array($this, 'onDataObjectBeforeEdit')
		);

		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_LIST_VIEW,
			array($this, 'onDataObjectBeforeListView')
		);
		
		// Add CSS files to admin page view:
		$css = M_ViewCss::getInstance();
		$css->addFile(new M_File(
			M_Loader::getModulePath('media') . '/admin/resources/css/media.css'
		));
		
		// Add IE6 CSS Hacks files to admin page view:
		$css = M_ViewCss::getInstance();
		$css->addFile(
			new M_File(M_Loader::getModulePath('media') . '/admin/resources/css/browser-hacks-ie6.css'),
			'media-ie6',
			M_ViewCss::CONDITION_LTE_IE6
		);
		
		// Add Javascript files to admin page view
		$js = M_ViewJavascript::getInstance();
		$jsFile = new M_File(
			M_Loader::getModulePath('media') . '/admin/resources/javascript/media.js'
		);
		$js->addFile( $jsFile , 'admin-media');
	}
	
	/**
	 * Do ... before object gets edited
	 * 
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectBeforeEdit(AdminControllerEvent $event) {
		$this->_setDefaultMediaAlbum($event);
		$this->_setMediaAlbumForm($event);
	}
	
	/**
	 * Do ... before object gets saved
	 * 
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectBeforeSave(AdminControllerEvent $event) {
		$this->_processMediaFile($event);
	}
	
	/**
	 * Do ... when object has been saved
	 * 
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectSave(AdminControllerEvent $event) {
		$this->_setMediaAlbum($event);
	}
	
	/**
	 * Set the default media album for a {@link M_FieldMediaAlbum}
	 * 
	 * When an object is edited, we need check if there is already a
	 * media-album linked to the object
	 *
	 * @param AdminControllerEvent $event
	 * @return void
	 */
	private function _setDefaultMediaAlbum(AdminControllerEvent $event) {
		$mediaAlbumField = $this->_getFieldByType($event->getForm(), 'mediaalbum');
		
		//no media album field found... stop processing
		if ($mediaAlbumField instanceof M_FieldMediaAlbum == false) return;

		//try to find album for this object
		$album = $this->_getMediaAlbumMapper()->getAlbumByObject(
			$event->getDataObject()
		);

		//if one is found we set the default value for the form field
		if ($album instanceof MediaAlbum && $album->getId()) {
			$mediaAlbumField->setDefaultValue($album->getId()); 
		}
	}
	
	/**
	 * Set the media-album for the saved object
	 *
	 * @param AdminControllerEvent $event
	 */
	public function _setMediaAlbum(AdminControllerEvent $event) {
		$fieldMediaAlbum = $this->_getFieldByType($event->getForm(), 'mediaalbum');
		
		if ($fieldMediaAlbum == false) return ;
		
		$this->_getMediaAlbumMapper()->setAlbumIdByObject(
			$event->getDataObject(),
			$fieldMediaAlbum->getValue()
		);
	}

	/**
	 * onDataObjectBeforeListView
	 *
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectBeforeListView(AdminControllerEvent $event) {
		// If we are in iframe mode, in mailing-module AND we display a MediaList, add additional javascript!
		if(M_Request::getVariable('viewmode', false, null, M_Request::GET) == AdminPageView::VIEW_MODE_IFRAME
				&& M_Request::getVariable('field', false, null, M_Request::GET) == 'field-media-item'
				&& M_Request::getVariable('fieldId', false, null, M_Request::GET)) {
			$js = M_ViewJavascript::getInstance();
			$js->addFile(
				new M_File(AdminLoader::getAdminResourcesPath('media') . '/javascript/field-media-item-selection.js'),
				'media-iframe'
			);
			$js->addInline(
				'var fieldMediaItemId = "' . M_Request::getVariable('fieldId', false, null, M_Request::GET) . '";'
			);
		}
	}
	
	/**
	 * Process the file which is uploaded with the media field
	 * 
	 * Since this file can either be a .zip file containing different types
	 * of media OR just one media-file we need to check this before we can
	 * continue
	 * 
	 * @param AdminControllerEvent $event
	 * @return void
	 */
	private function _processMediaFile(AdminControllerEvent $event) {
		$uploadField = $this->_getFieldByType($event->getForm(), 'uploadmedia');
		
		//no upload field found... stop processing
		if ($uploadField instanceof M_FieldUpload == false) return false;
		
		//update dataObject with file
		/* @var $dataObject Media */
		$dataObject = $event->getDataObject();

		//if a file was uploaded we set the original filename and the filePath
		//to the file.
		//We don't want to do this when a media item is edited but no new file
		//was uploaded
		//@todo: for now we use getFilenameOriginal() because isUploaded always
		//seems to return true
		$isUploaded = $uploadField->getFilenameOriginal();
		if ($isUploaded) {
			$filePath = $uploadField->getValue();
			$dataObject->setFilenameOriginal(
				$uploadField->getFilenameOriginal()
			);	
		}else 
		{
			$filePath = $dataObject->getFile()->getPath();
		}

		//get the original file: we do this before setting the new file since
		//we want to delete this old file when a new file is added
		$originalFile = $dataObject->getFile();
		
		//set the original filename
		/* @var $dataObject Media */
		$file = new M_File($filePath);
		$dataObject->setFile($file);

		//process the file?
		M_Loader::loadDataObject('Media', 'media');
		switch($dataObject->getType()) {
			case Media::TYPE_IMAGE:
				// Check if we need to resize the image
				if(M_Helper::isBooleanTrue($this->_getConfig()->image->autoResize)) {
					$this->_processImage($file);
					break;
				}
			default;
				//do nothing
				break;
		}
		
		//if a new file was uploaded, everything went fine and the media-item 
		//already had a file, we delete this before we add a new one
		if ($isUploaded && $originalFile && $originalFile->exists()) $originalFile->delete();
		
		return $dataObject;
	}
	
	/**
	 * Process Image
	 * 
	 * When an image is added to the media module we process it so it gets
	 * resized, ...
	 *
	 * @param M_File $file
	 */
	private function _processImage(M_File $file) {
		$module = new M_ApplicationModule('media');
		$moduleConfig = $module->getConfig();
		
		$image = new M_Image($file->getPath());
		$filter = new M_FilterImageResize(
			new M_FilterImage($image), 
			$moduleConfig->image->maxWidth,
			$moduleConfig->image->maxHeight
		);
		
		$filter->apply()->saveToFile(
			$file->getDirectoryAndFilename(), 
			$image->getFileExtension()
		);
	}
	
	/**
	 * Get all media from a set of files and directories
	 * 
	 * When an archive is extracted, it can contain media files as well as other
	 * files or directories. We need to iterate through every directory and 
	 * validate the files in it.
	 * 
	 * @param ArrayIterator $items
	 * @return ArrayIterator
	 */
	private function _getMediaFromItems(ArrayIterator $items) {
		$media = array();
		
		foreach( $items AS $item) {
			//if this is a file, check if it's a valid media file and add it to
			if ($item instanceof M_File && Media::isMediaFile($item)) {
				$media[]  = $item;
			}elseif ($item instanceof M_Directory) {
				/* @var $item M_Directory */
				$media = array_merge($media, (array)$this->_getMediaFromItems($item->getItems()));
			}
		}
		
		return new ArrayIterator($media);
	}
	
	/**
	 * Get a field by its type
	 *
	 * @param M_Form $form
	 * @param string $type
	 * @return M_Field
	 */
	private function _getFieldByType(M_Form $form, $type) {
		/* @var $field M_Field */
		foreach($form->getFields() AS $field) {
			if ($field->getType() == $type) {
				return $field;
			}
		}
		return false;
	}
	
	/**
	 * Get a mediamapper
	 *
	 * @return MediaMapper
	 */
	protected function _getMediaMapper() {
		if (is_null($this->_mediaMapper)) {
			M_Loader::loadDataObjectMapper('Media', 'media');
			$this->_mediaMapper = new MediaMapper();
		}
		
		return $this->_mediaMapper;
	}
	
	/**
	 * Get a mediamapper
	 *
	 * @return MediaAlbumMapper
	 */
	protected function _getMediaAlbumMapper() {
		if (is_null($this->_mediaAlbumMapper)) {
			M_Loader::loadDataObjectMapper('MediaAlbum', 'media');
			$this->_mediaAlbumMapper = new MediaAlbumMapper();
		}
		
		return $this->_mediaAlbumMapper;
	}

	/**
	 * Do some settings in the media-album form
	 * 
	 * @param AdminControllerEvent $event
	 */
	protected function _setMediaAlbumForm(AdminControllerEvent $event) {
		$form = $event->getForm();
		$album = $event->getDataObject();
		/* @var $album MediaAlbum */
		if (!M_Helper::isInstanceOf($album, 'MediaAlbum')) return false;

		//remove "attach to" and "published" fields for non-deletable albums
		if (!$album->isDeletable()) {
			$form->removeField('parentAlbumId')
				->removeField('published')
				->setVariable('published', 1)
				->setVariable('parentAlbumId', 0);
		}
	}
	
	/**
	 * Get the config of the media module
	 */
	private function _getConfig() {
		$module = new M_ApplicationModule('media');
		$moduleConfig = $module->getConfig();
		
		return $moduleConfig;
	}
}