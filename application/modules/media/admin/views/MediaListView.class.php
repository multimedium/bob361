<?php
class MediaListView extends AdminCustomListView {
	/**
	 * @var M_ConfigXml
	 */
	private $_config;
	
	/**
	 * Preprocessing
	 */
	public function preProcessing() {
		$this->assign('resourcesPath', M_Loader::getAbsolute($this->_getResourcesPath()));
		$this->assign('filePath', M_Request::getLinkWithoutPrefix($this->_getConfig()->get('path')));
		
		// Check if the crop module is available
		$crop = false;
		if(M_Application::isModuleInstalled('crop')) {
			$crop = true;
		}
		$this->assign('crop', $crop);
	}
	
	/**
	 * Get the resource for the media view
	 * 
	 * @return M_ViewHtmlResource
	 */
	public function getResource() {
		return new M_ViewHtmlResource(
			$this->_getResourcesPath() . '/templates/MediaList.tpl'
		);
	}
	
	/**
	 * Get the resources path for the media module
	 *
	 * @return string
	 */
	protected function _getResourcesPath() {
		return AdminLoader::getAdminResourcesPath('media');
	}
	
	/**
	 * Get config file
	 * 
	 * @return M_ConfigXml
	 */	
	protected function _getConfig() {
		if (is_null($this->_config)) {
			$configFile = new M_ConfigXml(M_Loader::getModulePath('media').'/config.xml');
			$this->_config = $configFile->get('config');
		}
		
		return $this->_config;
	}
}