<?php
/**
 * MediaListViewHelper
 *
 */
class MediaListViewHelper {
	
	public function setOptions($listDef, $filters) {
		$link = M_Request::getLink('admin/route/media/import');
		$text = t('Create new items in <strong>@module</strong>', array('@module' => 'media'));
		
		return array('
				<a href="#" class="button-plus popmenu" id="idListPlusButton">+</a>
				
				<div id="idListPlusButtonPopmenu">
					<ul>
						<li>
							<a href="' . $link . '">
								' . $text . '
							</a>
						</li>
					</ul>
				</div>
		');
	}
	
}