<?php

class MediaAlbumAutocompleteAjaxView  {

    /**
     * The media-albums to display
     *
     * @var ArrayIterator
     */
    protected $_mediaAlbums;
    
    /**
     * Search string
     *
     * @var string
     */
    protected $_search;
    
	/**
	 * Set the tag for this view
	 *
	 * @param ArrayIterator $mediaAlbums
	 * @return void
	 */
	public function setMediaAlbums(ArrayIterator $mediaAlbums) {
	    $this->_mediaAlbums = $mediaAlbums;
	}
	
	/**
	 * Set the mediaAlbum after which we search after
	 *
	 * @param string $search
	 */
	public function setSearch($search) {
	    $this->_search = $search;
	}

	/**
	 * Get the resource for this view
	 *
	 * @return M_ViewHtmlResource
	 */
	public function display() {
	    /* @var $mediaAlbum MediaAlbum */
	    foreach ($this->_mediaAlbums AS $mediaAlbum) {
			// Get the recursive title for this album
	        $text = new M_Text($this->_getRecusriveAlbumTitle($mediaAlbum));
	        $image = $mediaAlbum->getImages()->current();
	        $imageHtml = '';
	        if ($image instanceof Media ) $imageHtml = '<img src="'.M_Request::getLink().'/thumbnail/'.M_FieldMediaAlbum::THUMBNAIL_TYPE.'/'.M_FieldMediaAlbum::THUMBNAIL_SIZE.'/'.$image->getFilePath().'" style="vertical-align: middle;"/> ';
	        echo $imageHtml . $text->getSnippet('100') . '|' . $mediaAlbum->getId() . M_CodeHelper::getNewLine();
	    }
	}
	
	/* -- PROTECTED FUNCTIONS -- */
	
	/**
	 * Get recursive album title
	 * 
	 * @access protected
	 * @param MediaAlbum $album
	 * @return string
	 */
	protected function _getRecusriveAlbumTitle(MediaAlbum $album) {
		$titleArray = array($album->getTitle());
		
		// Create an array with all the titles
		while($album->getParentAlbum()) {
			$album = $album->getParentAlbum();
			$titleArray[] = $album->getTitle();
		}
		
		// Reverse the array
		$titleArray = array_reverse($titleArray);
		
		return implode(' &gt; ', $titleArray);
	}
}