<?php
class MediaImportFormView extends AdminFormView  {
	
	/**
	 * Get the resource for the media view
	 * 
	 * @return M_ViewHtmlResource
	 */
	public function getResource() {
		return new M_ViewHtmlResource(
			$this->_getResourcesPath() . '/templates/MediaImportFormView.tpl'
		);
	}

	/**
	 * Get the resources path for the media module
	 *
	 * @return string
	 */
	protected function _getResourcesPath() {
		return AdminLoader::getAdminResourcesPath('media');
	}
}