<?php
class MediaListRowView extends AdminListInstanceRowView {
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return MediaListRowView
	 */
	public function __construct() {
		$this->assign('crop', M_Application::isModuleInstalled('crop'));
	}
	
	/**
	 * Get the resource for the media view
	 * 
	 * @return M_ViewHtmlResource
	 */
	public function getResource() {
		return new M_ViewHtmlResource(
			AdminLoader::getAdminResourcesPath('media') . '/templates/MediaListRow.tpl'
		);
	}
}