<div>
	<ul id="idMediaList">
		{* For each of the Media instances: *}
		{foreach from=$instances item=m}
			<li id="idListedInstance{$m->getId()}">
				{* Render the media item, with the _Media.tpl file *}
				{include file="$resourcesPath/templates/_Media.tpl" media=$m crop=$crop}
			</li>
		{/foreach}
	</ul>
	<div class="clear"></div>
</div>

<script type="text/javascript" language="Javascript">
{literal}
	var u = '{/literal}{urlcopy queryVariableSeparator="&"}{literal}';
	u = u.replace(/\/overview-ajax\//, '/instance-row/');
	u = u.replace(/\/overview\//, '/instance-row/');
	u = u.replace(/\/instances-smart-folder\//, '/instance-row-smart-folder/');
	u = u.replace(/\/instances\//, '/instance-row/');
	u = u.replace(/\/smart-folder-ajax\//, '/instance-row-smart-folder/');
	u = u.replace(/\/smart-folder\//, '/instance-row-smart-folder/');
	
	$('ul#idMediaList').M_LiveList({
		listScrollContainer : $('#idContentPaneViewport'),
		listObjectTemplate  : '<li><div class="media-item-icon"></div></li>',
		listTotalCount      : {/literal}{$pagejumper->getTotalCount()}{literal},
		listAjaxProperties  : {
			url : u
		}
	});
{/literal}
</script>

{if $definition->isInstancesDraggableForSorting($filters)}
	{assign var="module" value=$definition->getModule()}
	<script type="text/javascript" language="Javascript">
	{literal}
		var _sortableDraggedItemStart = 0;
		var _sortableDraggedItemEnd   = 0;
		var _sortableDraggedItemDiff  = 0;
		$('.media-media-list ul').sortable({
			revert : true,
			// When started dragging:
			start : function($event, ui) {
				// Get the current index number
				_sortableDraggedItemStart = $('ul#idMediaList').children().index(ui.item);
			},
			// When finished dragging
			update : function($event, ui) {
				// Get the new index number
				_sortableDraggedItemEnd   = $('ul#idMediaList').children().index(ui.item);
	
				// Calculate the difference in index number. This is the direction
				// of movement, and the number of steps in that direction. E.g. -2
				// means that the item has been dragged 2 positions upwards
				_sortableDraggedItemDiff  = _sortableDraggedItemEnd - _sortableDraggedItemStart;
				
				// Get the ID of the dragged instance:
				var i = ui.item.attr('id').substr(16, ui.item.attr('id').length);
	
				// Compose the URL for the AJAX Request, that is going to store the
				// new order index numbers:
				var u = jQueryBaseHref + '/admin/move-instance/{/literal}{$module->getId()}/{$definition->getDataObjectId()}/order/'+ i + '/' + _sortableDraggedItemDiff +'{literal}';
	
				// Do the AJAX Request:
				$.ajax({
					// To URL:
					url : u,
					// Send along the filter values:
					data : {
						{/literal}{foreach from=$smarty.get item="value" key="name" name="i"}{$name} : '{$value}'{if !$smarty.foreach.i.last}, {/if}{/foreach}{literal}
					},
					// Expected Data Type
					dataType : 'html',
					// When done:
					success : function() {
					}
				});
			}
		});
	{/literal}
	</script>
{/if}