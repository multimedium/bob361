<results>
	<success>{$success}</success>
	{if isset($error)}<error>{$error}</error>{/if}
	{if isset($file)}<file>{$file}</file>{/if}
	{if isset($original)}<original>{$original}</original>{/if}
</results>