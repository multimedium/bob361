{* Image template *}
{if $media->getType() == 'image'}
	{assign var="mediaTpl" value="_MediaImage.tpl"}
{else}
	{assign var="mediaTpl" value="_MediaDefault.tpl"}
{/if}
{include file="$resourcesPath/templates/$mediaTpl" media=$media crop=$crop}