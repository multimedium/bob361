{assign var="file" value=$media->getFile()}
{* <a href="{link href="admin/edit/media/"}{$media->getId()}" title="{$media->getTitle()}"> *}
	<div class="media-item-icon"><img src="{link}/thumbnail/square/150/{$media->getFilepath()}" alt="" title="{$media->getTitle()}" /></div>
	<div class="media-item-title">
		{snippet from=$media->getTitle() length="15"} - <strong>{$file->getFileExtension()}</strong>
	</div>
	<span class="buttonsBar">
		<ul>
			<li>
				<a href="{link href="admin/delete/media/"}{$media->getId()}" class="confirm delete" title="{t|att text="Delete this media item"}">
					{t text="Delete this media item"}
				</a>
			</li>
			<li>
				<a href="{link href="admin/edit/media/"}{$media->getId()}" class="edit" title="{t|att text="Edit this item"}">
					{t text="Edit this item"}
				</a>
			</li>
			<li>
				<a href="{link}/thumbnail/resized/640/480/{$media->getFilepath()}" class="zoom fancybox" title="{$media->getTitle()|att}" rel="mediaItem">
					{t text="Show bigger version"}
				</a>
			</li>
			<li>
				<a href="{link href="admin/toggle/media/media/"}{$media->getId()}/published" class="toggle-{if $media->getPublished()}1{else}0{/if}" title="{t|att text="Activate item?"}">
					{t text="Activate item?"}
				</a>
			</li>
			{if $crop}
			<li>
				<a href="{link href="admin/route/crop/index/"}{$media->getId()}" class="button-crop" title="{t|att text="Crop item?"}">
					{t text="Crop item"}
				</a>
			</li>
			{/if}
		</ul>
	</span>
{* </a> *}