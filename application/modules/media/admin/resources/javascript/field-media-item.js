/**
 * When the document is ready
 */
$(document).ready(function() {
	
	// Fancybox settings
	var fancyBoxSettings = {
		width: '80%',
		height: '80%',
		overlayShow: true,
		type: 'iframe',
		titlePosition: 'over',
		hideOnContentClick: false,
		hideOnOverlayClick: false,
		onComplete: function() {
			$(document).trigger('M_Media_IframeShow');
		},
		onClosed: function() {
			// unbind all Events!
			$(document).unbind('M_Media_IframeShow');
		}
	};
	
	// Foreach Field Media Form
	$('.field-media-item-button-wrapper').each(function() {
		
		// Get the Input Id
		var inputId = $(this).find('a').attr('data-id');
		
		// Initialize the Fancybox
		$(this).find('.field-media-item-button').fancybox(fancyBoxSettings);
		
		// Bind the event
		$('#' + inputId).bind('M_Media_Selected', function($event, $id, $src) {

			// Create a small square thumbnail
			$src = $src.replace('square/150/', 'square/30/');
			$(this).parent().find('.field-media-item-thumb').html('<img src="' + $src + '" alt="" />');

			// Set the value of the field
			$(this).val($id);

			// Close the fancybox
			$.fancybox.close();
		});
	});
	

});