$(document).ready(function() {
	/**
	 * Prevent redirect when user clicks in popmenu for mediaAlbum
     */
	$('.mediaAlbumPopmenu a').click(function() {
		// prevent redirect
		return false;
	});

	/**
	 * Confirm truncate from album
	 */
	$('.truncate').live('click', function(){
		var sure = confirm('Are you sure?');
		if (!sure) return false;
	});

    /**
	 * Set click action on popmenu for field FieldMediaAlbum
	 */
	$(document).bind('M_Popmenu_Select', function(e, $selectedOption, $interfaceSender) {
        // update the value of the field which will post the value of the selected item,
        // and trigger change event (hidden fields don't do this automaticcally)
        $interfaceSender.siblings('input[type="hidden"]').val($selectedOption.attr('href')).trigger('change');

        // show selected album
		$interfaceSender.html($selectedOption.html());
	});
	 
	/**
	 * Refresh media when selecting an album in MediaField
	 */
	 $('#idFieldMedia #albumFilter').change(function(){
		 $.M_AjaxRequest({
				url : jQueryBaseHref + '/admin/route/media/mediaFieldAjax/' + $(this).val(),
				success : function(data) {
			 		$('#idFieldMediaListContainer').html(data);
			 		 initMagicCss($('#idFieldMediaListContainer'));
			 		 updateMediaListWidth();
			}
		});
 	});
	 
	 
	 /**
	  * Set media of the first album in MediaField
	  */
	 if($('#idFieldMedia').length > 0) {
		$.M_AjaxRequest({
				url : jQueryBaseHref + '/admin/route/media/mediaFieldAjax/' + $('#albumFilter').val(),
				success : function(data) {
			 		$('#idFieldMediaListContainer').html(data);
			 		initMagicCss($('#idFieldMediaListContainer'));
			 		updateMediaListWidth();
			}
		});
	 }
	 
	/**
	 * Set actions, for when content is loaded:
	 */
	if(! $('#idContentPane').M_HasData('media-list-events')) {
		$('#idContentPane').data('media-list-events', 'yes');
		$('#idContentPane').bind('M_LoadInContentPane_Update', function($eventObject, url) {
			// Show media items' icons
			enableMediaItemIcons($('.media-media-list li'));
			
			// Enable media items' options interface
			enableMediaOptionsInterface($('.media-media-list li'));
			
			// Enable Toggle Button Interface (Media Item also fades in/out)
			enableMediaToggleInterface($('.media-media-list li'));
			
			// Enable live list objects
			enableLiveListObjects();
		});
	}
});

// Enable Media Options interface (animates options into view)
function enableMediaOptionsInterface($container) {
	// Prevent default dragging of images (eg firefox):
	$('img', $container).M_PreventDefaultDragging();
	
	// Options bar
	$('.buttonsBar', $container).hide();
	$container.hover(function() {
		$('.buttonsBar', this).slideDown(150);
	}, function() {
		$('.buttonsBar', this).slideUp(50);
	});
}

// Enable Media Toggle Button Interface
function enableMediaToggleInterface($container) {
	// Alpha transparencies:
	var a = [1, 0.3];
	
	// Listen to the toggle event (toggle buttons):
	$('a.toggle-0, a.toggle-1', $container).bind('M_ToggleButton', function($eventObject, newToggledStatus) {
		// We also animate the image (fade in/out), to illustrate enabled and 
		// disabled images. We determine the target transparency:
		var t = newToggledStatus == 1 ? a[0] : a[1];
		
		// Now, animate the image:
		// a > li > ul > span.buttonsBar > img
		$(this).parent().parent().parent().siblings('.media-item-icon').stop().fadeTo(300, t);
	});
	
	// Also, we need to animate the images into their initial transparency, in
	// order to show current state:
	$('a[class^="toggle"]', $container).each(function() {
		// Get the current state of the media item:
		var c = parseInt($(this).attr('class').substr(7, 8));
		
		// We determine the target transparency:
		var t = c == 1 ? a[0] : a[1];
		
		// Now, we animate the image
		$(this).parent().parent().parent().siblings('.media-item-icon').css({
			opacity : t
		});
	});
}

// Enable (animate into view) Media Items' icons
function enableMediaItemIcons($container, animate) {
	if(animate) {
		var $ic = $container.children('.media-item-icon');
		if($ic.length > 0) {
			$ic.children('img').hide().stop().load(function() {
				$.M_Console('fade in ' + $(this).attr('src'));
				// Fade it into view
				$(this).fadeIn();
			});
		}
	}
}

// Respond to new objects, that are loaded in via List list
function enableLiveListObjects() {
	// When new objects are loaded in:
	$('#idMediaList').unbind('M_LiveList_NewObject').bind('M_LiveList_NewObject', function($eventObject, $newObject) {
		// Initiate magic css
		initMagicCss($newObject, {fillRemainingVspace : false});
		
		// Show media items' icons
		enableMediaItemIcons($newObject, true);
		
		// Enable media items' options interface
		enableMediaOptionsInterface($newObject);
		
		// Enable Toggle Button Interface
		enableMediaToggleInterface($newObject);
	});
}

//update the MediaList Div CSS Width in order to scroll horizontal
function updateMediaListWidth() {
	$('#idFieldMediaListContainer img').hide().load(function() {
		$(this).fadeIn();
		
		//set the default width to 100
		var newWidth = 100;
		
		//foreach div, calculate the div of the head
		$('#idFieldMediaListContainer > div').each(function() {
			
			newWidth += $(this).outerWidth();
			
			//set new width
			$('#idFieldMediaListContainer').width(newWidth);
		});
	});
}

// Initiate:
enableMediaOptionsInterface($('#idContentPane .media-media-list li'));
enableMediaToggleInterface($('#idContentPane .media-media-list li'));
enableLiveListObjects();
