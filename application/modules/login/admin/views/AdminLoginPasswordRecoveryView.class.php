<?php
/**
 * AdminLoginPasswordRecoveryView class file
 * 
 * @package App
 * @subpackage Login
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminLoginPageView.class.php';

/**
 * AdminLoginPasswordRecoveryView class
 * 
 * @package App
 * @subpackage Login
 */
class AdminLoginPasswordRecoveryView extends AdminLoginPageView {
	/**
	 * Set form
	 * 
	 * @access public
	 * @param M_Form $form
	 * @return void
	 */
	public function setPasswordRecoveryForm(M_Form $form) {
		$this->assign('form', $form);
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(M_Loader::getModulePath('login') . '/admin/resources/templates/LoginPasswordRecovery.tpl');
	}
}