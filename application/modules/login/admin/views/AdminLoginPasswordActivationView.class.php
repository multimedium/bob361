<?php
/**
 * AdminLoginPasswordExpiredView class file
 * 
 * @package App
 * @subpackage Login
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminLoginPageView.class.php';

/**
 * AdminLoginPasswordActivationView class
 * 
 * @package App
 * @subpackage Login
 */
class AdminLoginPasswordActivationView extends AdminLoginPageView {
	/**
	 * Set Administrator Account
	 * 
	 * Defines the Administrator Account that has activated the new password
	 * 
	 * @access public
	 * @param Administrator $administrator
	 * @return void
	 */
	public function setAdministrator(Administrator $administrator) {
		$this->assign('administrator', $administrator);
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(M_Loader::getModulePath('login') . '/admin/resources/templates/LoginPasswordActivation.tpl');
	}
}