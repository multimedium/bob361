<h1>{translate text="Error"}</h1>
<p class="reading">
	{t text="Your request could not have been completed. Please try again later."}
	{t text="We sincerely apologize for the inconvencience this may cause you."}
	
	<br />
	<br />
	<a href="{link href="admin/route/login/passwordRecovery"}" title="{t|att text="Recover my password"}">
		{t text="Try again"}
	</a>
	|
	<a href="http://www.multimedium.be/contact">Multimedium</a>
</p>
