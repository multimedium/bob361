<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Multimanage - Login - Powered by Multimedium</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="nl" />
	
	<meta name="Keywords" content="multimedium, maintenance, service, plan, login" />
	<meta name="description" content="Multimedium - Maintenance Service Plan - Login" />
	
	<link rel="stylesheet" href="{link prefix="false"}/application/modules/login/admin/resources/css/all.css" type="text/css" media="screen" />
	
	<meta name="robots" content="all" />
	<meta name="revisit-after" content="5 days" />
	<meta name="rating" content="general" />
	<meta name="distribution" content="global" />
	<meta http-equiv="imagetoolbar" content="no" />
</head>
<body>
	<div id="wrapper">
		<div id="page-wrapper">
			<div id="window-wrapper">
				<div id="window">
					{$content}
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div id="powered-by"><span>Multimanage - Powered by </span> <a href="http://www.multimedium.be" id="multimedium">multimedium</a></div>
		{if $isMultikeyModuleInstalled}
			<div id="footer-links">
				<a href="{link href="admin/route/multikey"}" title="{t|att text="Sign in with your Multikey"}">
					{t text="Sign in with your Multikey"}
				</a>
			</div>
		{/if}
	</div>
</body>
</html>