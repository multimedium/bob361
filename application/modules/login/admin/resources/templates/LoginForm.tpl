
{if $form->getErrorMessage()}
	<div class="error-message">
		{$form->getErrorMessage()}
	</div>
{/if}

<h1>{translate text="Login"}</h1>

<form id="{$viewId}"{foreach from=$form->getProperties() key="name" item="value"} {$name}="{$value}"{/foreach}>
	
	{foreach from=$form->getVariables() key="name" item="value"}
		<input type="hidden" name="{$name}" value="{$value}" />
	{/foreach}
	
	{foreach from=$form->getFields() key="name" item="field"}
		{$field->setModuleOwner('admin')}
		<div class="field-row" id="{$viewId}">
			<label for="{$field->getId()}">
				{$field->getTitle()}
			</label>
			<div class="field-container">
				{if $field->getErrorMessage()}
					<div class="error-message">
						{$field->getErrorMessage()}
					</div>
				{/if}
				{$field->getInputView()}
				{if $field->getDescription()}
					<div class="small note">
						{$field->getDescription()}
					</div>
				{/if}
			</div>
			<div class="clear"></div>
		</div>
	{/foreach}
	
	<div class="field-row-buttons">
		<input type="image" name="go" src="{link href="application/modules/login/admin/resources/images/button-ok.gif" prefix="false"}" value="{if isset($submitButtonLabel)}{$submitButtonLabel}{else}{t text="ok"}{/if}" />
	</div>
	
	<a href="{link href="admin/route/login/passwordRecovery"}" id="passwordRecoveryButton" title="{t|att text="Recover my password"}">
		{t text="Help, I forgot my password!"}
	</a>
	
</form>