<?php
/**
 * DataObjectImage
 * 
 * Provides an object which can be used for images overall, with the option
 * to set some additional properties. This way we can support both the
 * standard {@link Media} objects as well as any other {@link M_Image} objects
 * that should have custom set properties.
 * 
 * @package App
 * @subpackage DataObjectImage
 */
class DataObjectImage extends M_Image {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Title
	 * 
	 * @see DataObjectImage::setTitle()
	 * @access private
	 * @var string
	 */
	private $_title;
	
	/**
	 * Description
	 * 
	 * @see DataObjectImage::setDescription()
	 * @access private
	 * @var string
	 */
	private $_description;
	
	/* -- PUBLIC STATIC FUNCTIONS -- */
	
	/**
	 * Construct With Media
	 * 
	 * @static
	 * @access public
	 * @param Media $media
	 * @return DataObjectImage
	 */
	public static function constructWithMedia(Media $media) {
		// Construct a new image of ourselves
		$image = new self($media->getFilePath());
		
		// Set the properties from the Media item
		$image->setTitle($media->getTitle());
		$image->setDescription($media->getText());
		
		// Return the image
		return $image;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Title
	 * 
	 * @see DataObjectImage::setTitle()
	 * @access public
	 * @return string
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 * Get Description
	 * 
	 * @see DataObjectImage::setDescription()
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/**
	 * Get basename based on the title
	 *
	 * Note: when the title of the media-item is empty, we use the normal
	 * basename. If we don't do this: {@link M_Uri::getPathElementFromString()}
	 * will throw an exception
	 *
	 * @author Wim Van De Mierop
	 * @return string
	 */
	public function getSemanticFilename() {
		$title = $this->getTitle();
		$url = $this->getFilename();
		if($title) {
			$url .= '/' . M_Uri::getPathElementFromString($title)  . '.' . $this->getFileExtension();
		}else {
			$url .= '/' . $this->getBasename();
		}
		
		return $url;
		
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set Title
	 * 
	 * Allows for setting the title of this image
	 * 
	 * @access public
	 * @param string $title
	 * @return DataObjectImage
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
		return $this;
	}
	
	/**
	 * Set Description
	 * 
	 * Allows for setting the description of this image
	 * 
	 * @access public
	 * @param string $description
	 * @return DataObjectImage
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
		return $this;
	}
}