<?php
/**
 * DataObjectWithImages
 * 
 * Superclass which can be used to combine the main image of an
 * {@link M_DataObject} with any {@link Media} items that have been
 * attached to it.
 * 
 * @see DataObjectImage
 * @package App
 * @subpackage DataObjectImage
 */
abstract class DataObjectWithImages extends M_DataObject {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Images
	 * 
	 * Holds the collection of {@link DataObjectImage} objects
	 * 
	 * @access private
	 * @var array
	 */
	private $_images = array();
	
	/* -- GETTERS -- */
	
	/**
	 * Get Images
	 * 
	 * Will provide a combined collection of {@link DataObjectImage} objects,
	 * typically composed of the following:
	 * 
	 * - the main image of the {@link M_DataObject}, which is provided through
	 *   {@link DataObjectWithMedia::_getImageGetter()}
	 * 
	 * - any {@link Media} objects that have been attached. Note that the
	 *   following filters are active by default when fetching {@link Media}:
	 *		- {@link MediaAlbumMapper::addFilterPublished()}
	 *		- {@link MediaMapper::addFilterPublished()}
	 *		- {@link MediaMapper::addFilterOrder()}
	 * 
	 * @access public
	 * @param int $offset
	 *		The offset for fetching images, if required
	 * @param int $limit
	 *		The limit on the number of images returned, if required
	 * @return M_ArrayIterator
	 *		The collection of {@link DataObjectImage} objects
	 */
	public function getImages($offset = 0, $limit = null) {
		// If the collection hasn't been constructed before
		if(empty($this->_images)) {
//			// Fetch the main image
//			$image = $this->_getImageGetter();
//			
//			// If we did not receive an DataObjectImage instance from the function
//			if(! M_Helper::isInstanceOf($image, 'DataObjectImage')) {
//				throw new M_Exception(sprintf(
//					'The provided data object getter %s::%s() must return an ' .
//					'instance of DataObjectImage; cannot proceed in %s::%s()',
//					$this->getClassName(),
//					$this->_getImageGetter(),
//					__CLASS__,
//					__FUNCTION__
//				));
//			}
//			
//			// If it is set, and exists
//			/* @var $image M_Image */
//			if($image->exists()) {
//				// Add it as first element to the property
//				$this->_images[] = $image;
//			}
//			
			// Now check if this data object has media that we have to add.
			// First fetch the media album:
			$album = M_Loader
				::getDataObjectMapper('MediaAlbum', 'media')
				->addFilterPublished()
				->getAlbumByObject($this);
			
			// If an album was found
			if($album) {
				// Load necessary classes
				M_Loader::loadDataObject('DataObjectImage', 'data-object-image');
				
				// Fetch its media
				$media = M_Loader
					::getDataObjectMapper('Media', 'media')
					->addFilterPublished()
					->addFilterOrder()
					->addFilterImage()
					->addFilterMediaAlbum($album)
					->getAll();
				
				// For each media item
				foreach($media as $m) {
					/* @var $m Media */
					
					// If the file exists
					if($m->getFileExists()) {
						// Construct a new DataObjectImage with it, adding it
						// the image to the property
						$this->_images[] = DataObjectImage::constructWithMedia($m);
					}
				}
			}
		}
		
		// Fetch the currently set collection of images
		$images = $this->_images;
		
		// Have any images been set, and has an offset and / or limit been
		// provided:
		if(count($images) > 0 && ($offset > 0 || (int) $limit > 0)) {
			// Then slice out the requested image collection
			$images = array_slice($images, $offset, $limit, true);
		}
		
		// Return the set collection of images
		return new M_ArrayIterator($images);
	}
	
	/**
	 * Get One Image
	 * 
	 * Will return the first image from the collection provided by
	 * {@link DataObjectWithImages::getImages()}
	 * 
	 * @see DataObjectWithImages::getImages()
	 * @access public
	 * @return M_Image
	 */
	public function getOneImage() {
		// Fetch the images
		$images = $this->getImages();
		
		// If none are set
		if($images->count() == 0) return new M_Image();
		
		// Return the first image
		return $images->first();
	}


	/**
	 * Get Images From Text
	 * 
	 * This method will return a collection of images that are contained
	 * in the M_Text object.
	 * 
	 * @acces public
	 * @return M_ArrayIterator
	 *		The collection of {@link M_Image} objects
	 */
	public function getImagesFromText($text) {
		
		// Create an empty M_ArrayIterator to store all images contained
		// in this text
		$images = new M_ArrayIterator();
		
		// Convert this text to an HtmlDom object
		$dom = new M_HtmlDom($text);
		/* @var $href M_HtmlDom */
		
		// Find all images and process them 
		foreach($dom->find('img') AS $node) {
			/* @var $node M_HtmlDomNode */
			// Create an image for this node
			$image = $this->_createImageFromNode($node);
			
			// Append the image object to the M_ArrayIterator
			$images->append($image);
		}
		
		// Finally return the M_ArrayIterator containg a collection of all
		// images found in the current text
		return $images;
	}
	
	/*-- PRIVATE / PROTECTED --*/
	
	/**
	 * Create Image From Node
	 * 
	 * @access protected
	 * @return M_Image
	 */
	protected function _createImageFromNode($node) {
	
		// Get the src attribute of this image
		$src = $node->getAttribute('src');

		// Get the basename from the src element
		$basename = end(explode('/', $src));

		// Get the File path
		$filePath = 'files/media/' . $basename;

		// Try to find the media item for this image
		$mediaMapper = M_Loader::getDataObjectMapper('Media', 'media');
		/* @var $mediaMapper MediaMapper */

		$mediaItem = $mediaMapper->getOneByFieldWithRegistryCache('filePath', $filePath);
		/* @var $mediaItem Media */

		// Set the source & alt attribute
		if( ! $mediaItem) {
			throw new M_Exception(
				sprintf('Image with path %s does not exist', $mediaItem->getBasename())
			);
		}		

		// Create an M_Image object for this image
		M_Loader::loadModel('DataObjectImage', 'data-object-image');
		$image = new DataObjectImage();
		
		// Set the source tag
		$image->setPath(M_Request::getLink('thumbnail/media') . '/' . $mediaItem->getBasename());
		
		// Set the Alt tag
		$title = $mediaItem->getTitle();
		if($title && $title !== '') {
			$image->setTitle($title);
			$image->setDescription($title);
		}
		
		return $image;
		
	}		
	
	/**
	 * Get Image Getter
	 * 
	 * Every {@link M_DataObject} that extends from this class MUST implement
	 * this function, to provide the {@link DataObjectImage} instance
	 * containing the main image for the {@link DataObject}. This image will
	 * typically be used by {@link DataObjectWithImages::getImages()
	 * 
	 * @abstract
	 * @access protected
	 * @return DataObjectImage
	 */
	abstract protected function _getImageGetter();
}