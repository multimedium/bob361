/**
 * Admin - Data Object Image
 * 
 * Functions required for proper use of the data object images
 */

// Initiate the delete buttons
initImageDeleteButtons();
if(! $('#idContentPane').M_HasData('item-delete-buttons')) {
	// Then, initiate now:
	$('#idContentPane').data('item-delete-buttons', 'yes');

	$('#idContentPane').bind('M_LoadInContentPane_Update', function($eventObject, url) {
		// Initiate delete buttons
		initImageDeleteButtons();
	});
}

// Function to initiate the delete image buttons
function initImageDeleteButtons() {
	// Link for deleting the logo of a product
	$('.delete-image').click(function() {
		// The matched element:
		var $e = $(this);	

		// Confirm box:
		var sure = confirm('Are you sure you want to delete this image?');

		// If pressed OK:
		if(sure) {
			// Delete via AJAX
			$.M_AjaxRequest({
				url : $(this).attr('href'),
				success : function(data) {
					// If we received success
					if(data == '1') {
						// Empty the row value
						$e.parent().html('&nbsp;');
					}
				}
			});
		}

		// Override default link functionality
		return false;
 	});
}