<?php
/**
 * DataObjectImageEventListener
 *
 * @package App
 * @subpackage News
 */
class DataObjectImageEventListener {
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Construct
	 *
	 * @access public
	 * @param AdminController $controller
	 */
	public function __construct(AdminController $controller) {
		M_ViewJavascript
			::getInstance()
			->addFile(new M_File('application/modules/data-object-image/admin/resources/javascript/admin-data-object-image.js'), 'admin-data-object-image-js');
	}
}