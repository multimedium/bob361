<?php
/**
 * ThumbnailDefinition class
 *
 * @package App
 * @subpackage Thumbnail
 */
class ThumbnailDefinition {

	/* --
	 * 
	 * Thumbnail-type constants
	 *
	 * Note: don't forget to add a new thumbnail type to
	 * {@link ThumbnailDefinition::getTypes()}
	 *
	 * -- */

	/**
	 * Square thumbnail-type
	 *
	 * @var const TYPE_SQUARE
	 */
	const TYPE_SQUARE = 'square';

	/**
	 * Resized thumbnail-type
	 *
	 * @var const TYPE_RESIZED
	 */
	const TYPE_RESIZED = 'resized';
	
	/**
	 * Resized thumbnail-type
	 *
	 * @var const TYPE_RESIZED
	 */
	const TYPE_RESIZED_SMART = 'resizedSmart';

	/**
	 * Resized forced thumbnail-type
	 *
	 * @var const TYPE_RESIZED_FORCED
	 */
	const TYPE_RESIZED_FORCED = 'resizedForced';

	/**
	 * Resized forced fit thumbnail-type
	 *
	 * @var const TYPE_RESIZED_FORCED_FIT
	 */
	const TYPE_RESIZED_FORCED_FIT = 'resizedForcedFit';

	/**
	 * Cropped thumbnail-type
	 *
	 * @var const TYPE_CROPPED
	 */
	const TYPE_CROPPED = 'cropped';

	/* --
	 *
	 * Image-filters constants
	 *
	 * Note: don't forget to add a new filter type to
	 * {@link ThumbnailDefinition::getImageFilterTypes()}
	 *
	 * -- */

	 /**
	  * Apply image to create a grayscale image
	  *
	  * @var const IMAGE_FILTER_GRAYSCALE
	  */
	 const IMAGE_FILTER_GRAYSCALE = 'grayscale';

	 /**
	  * Create a blur-effect
	  *
	  * @var const IMAGE_FILTER_BLUR
	  */
	 const IMAGE_FILTER_BLUR = 'blur';

	 /**
	  * Create a gaussian blur effect
	  *
	  * @var const IMAGE_FILTER_GAUSSIAN_BLUR
	  */
	 const IMAGE_FILTER_GAUSSIAN_BLUR = 'gaussianBlur';

	 /**
	  * Create a pixelated effect
	  *
	  * @var const IMAGE_FILTER_PIXELATE
	  */
	 const IMAGE_FILTER_PIXELATE = 'pixelate';
	 

	/* -- Properties -- */

	/**
	 * The id of this definition
	 *
	 * @var string
	 */
	private $_id;

	/**
	 * The type of thumbnail
	 *
	 * @var string
	 */
	private $_type;

	/**
	 * The width of thumbnail (in pixels)
	 * 
	 * @var int
	 */
	private $_width;

	/**
	 * The height of thumbnail (in pixels)
	 *
	 * Note: height is only applicable to certain thumbnail-types. In the case
	 * of th square-type, only {@link ThumbnailModel::_width} is used.
	 *
	 * @var int
	 */
	private $_height;

	/**
	 * The quality of the thumbnail-image (0-100)
	 *
	 * Defaults to 85
	 * 
	 * @var int
	 */
	private $_quality = 85;

	/**
	 * The path to the overlay image
	 * 
	 * @var string
	 */
	private $_overlay;

	/**
	 * X-position for overlay-image (in pixels or one of "left", "center", "right")
	 *
	 * You can choose the position of the overlay image which will be placed
	 * on top of the newly created thumbnail. If no position is set, the thumbnail
	 * will be aligned on the left of the thumbnail, or x-postion 0.
	 *
	 * To make life easier, you can also choose "left", "center" (center of the
	 * thumbnail) or "right".
	 * 
	 * @var string
	 */
	private $_overlayX = 0;

	/**
	 * Y-position for overlay-image (in pixels or one of "top", "center", "bottom")
	 *
	 * You can choose the position of the overlay image which will be placed
	 * on top of the newly created thumbnail. If no position is set, the thumbnail
	 * will be aligned at the thop of the thumbnail, or y-postion 0.
	 *
	 * To make life easier, you can also choose "top", "center" (center of the
	 * thumbnail) or "bottom".
	 *
	 * @var string
	 */
	private $_overlayY = 0;

	/**
	 * Starting x-position for cropping  (in pixels or one of "left", "center", "right")
	 *
	 * Note: only applicable when thumbnail-type is set to cropped  or resizedForced
	 * 
	 * @var string
	 */
	private $_cropX;

	/**
	 * Starting x-position for cropping (in pixels or one of "top", "center", "bottom")
	 *
	 * Note: only applicable when thumbnail-type is set to cropped or resizedForced
	 *
	 * @var string
	 */
	private $_cropY;

	/**
	 * Fill color to fill the background of the remaining image-space
	 *
	 * Note: only applicable when thumbnail-type is set to resizedForcedFit
	 * 
	 * @var string
	 */
	private $_fillColor;
	
	/**
	 * Tolerance
	 * 
	 * the amount of color difference allowed by the ResizedSmart 
	 * the higher the value, the highre the difference. 0 means exact matching colors
	 */
	private $_tolerance = 10;

	/**
	 * Padding to add to the image
	 *		We expect an array with 4 variables:
	 *			array(pixelsPaddingTop, pixelsPaddingRIght, pixelsPaddingBottom, pixelsPaddingLeft);
	 * 
	 * @var array
	 */
	private $_padding;
	
	/**
	 * The directory where the original-image files are stored
	 * 
	 * @var string
	 */
	private $_directoryPath = 'files/media';

	/**
	 * Holds the additional image filters
	 * 
	 * @var array
	 */
	private $_imageFilters = array();
	
	/**
	 * Sharpen the image?
	 * 
	 * @access private
	 * @var boolean
	 */
	private $_sharpen = false;
	
	/**
	 * Use semantic filenames?
	 * 
	 * @access private
	 * @var type boolean
	 */
	private $_useSemanticFilenames = false;
	
	

	/** -- Getters and setters -- */

	/**
	 * Get id of the definition
	 *
	 * Define an id for a definition: e.g. header, thumb, full, ... In most
	 * cases the id describes the kind of thumbnail, or the place where it's
	 * used
	 *
	 * @return string
	 */
	 public function getId() {
		return $this->_id;
	 }

	/**
	 * Set Name
	 *
	 * @param string $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getId()
	 */
	 public function setId($arg) {
		$this->_id = (string)$arg;
		return $this;
	 }

	/**
	 * Get type of the definition
	 *
	 * The type will define how the thumbnail will be created. We could crop,
	 * resize, ... an image. By using the correct type you will be able to
	 * create the kind of thumbnail you want.
	 *
	 * @return string
	 */
	 public function getType() {
		return $this->_type;
	 }

	/**
	 * Set type
	 *
	 * @param string $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getType()
	 */
	 public function setType($arg) {
		$this->_type = (string)$arg;
		return $this;
	 }

	/**
	 * Get width of the thumbnail (in pixels)
	 *
	 * @return int
	 */
	 public function getWidth() {
		return $this->_width;
	 }

	/**
	 * Set width of thumbnail
	 *
	 * @param int $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getWidth()
	 */
	 public function setWidth($arg) {
		$this->_width = (int)$arg;
		return $this;
	 }

	/**
	 * Get height of the thumbnail (in pixels)
	 *
	 * @return int
	 */
	 public function getHeight() {
		return $this->_height;
	 }

	/**
	 * Set height of thumbnail
	 *
	 * @param int $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getHeight()
	 */
	 public function setHeight($arg) {
		$this->_height = (int)$arg;
		return $this;
	 }

	/**
	 * Get height of the thumbnail (in pixels)
	 *
	 * @return int
	 */
	 public function getQuality() {
		return $this->_quality;
	 }

	/**
	 * The quality of the thumbnail-image (0-100)
	 *
	 * @param int $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getQuality()
	 */
	 public function setQuality($arg) {
		$this->_quality = (int)$arg;
		return $this;
	 }

	/**
	 * Get overlay image-path
	 *
	 * @return string
	 */
	 public function getOverlay() {
		return $this->_overlay;
	 }

	/**
	 * Set the overlay image-path
	 *
	 * @param string $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getOverlay()
	 */
	 public function setOverlay($arg) {
		$this->_overlay = (string)$arg;
		return $this;
	 }

	/**
	 * Get the x-position of the overlay image-path
	 *
	 * @return string
	 */
	 public function getOverlayX() {
		return $this->_overlayX;
	 }

	/**
	 * Set the x-position of the overlay image-path
	 *
	 * @param string $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getOverlayX()
	 */
	 public function setOverlayX($arg) {
		$this->_overlayX = $arg;
		return $this;
	 }

	/**
	 * Get the y-position of the overlay image-path
	 *
	 * @return string
	 */
	 public function getOverlayY() {
		return $this->_overlayY;
	 }

	/**
	 * Set the y-position of the overlay image-path
	 *
	 * @param string $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getOverlayY()
	 */
	 public function setOverlayY($arg) {
		$this->_overlayY = $arg;
		return $this;
	 }

	/**
	 * Get the crop starting x-position (in pixels)
	 *
	 * @return string|int
	 */
	 public function getCropX() {
		//return default value for square and resizedForced
		if (($this->getType() == self::TYPE_SQUARE || (is_null($this->_cropX) && $this->getType() == self::TYPE_RESIZED_FORCED))) {
			$cropX = M_FilterImageDecorator::POSITION_CENTER;
		}
		//return default value for cropped type if no crop type isset
		elseif (is_null($this->_cropX) && $this->getType() == self::TYPE_CROPPED && is_null($this->_cropX)) {
			$cropX = 0;
		}
		else {
			$cropX = $this->_cropX;
		}

		return $cropX;
	 }

	/**
	 * Set the crop starting x-position (in pixels or one of "left" or "center")
	 *
	 * @param int|string $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getCropX()
	 */
	 public function setCropX($arg) {
		$this->_cropX = $arg;
		return $this;
	 }

	/**
	 * Get the crop starting y-position (in pixels, or one of "top, "center")
	 *
	 * @return string|int
	 */
	 public function getCropY() {
		//return default value for square and resized force
		if (($this->getType() == self::TYPE_SQUARE || (is_null($this->_cropY) && $this->getType() == self::TYPE_RESIZED_FORCED))) {
			$cropY = M_FilterImageDecorator::POSITION_TOP;
		}
		//return default value for cropped type if not crop type isset
		elseif (is_null($this->_cropY) && $this->getType() == self::TYPE_CROPPED && is_null($this->_cropY)) {
			$cropY = 0;
		}
		else {
			$cropY = $this->_cropY;
		}

		return $cropY;
	 }

	/**
	 * Set the crop starting y-position (in pixels)
	 *
	 * @param int|string $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getCropY()
	 */
	 public function setCropY($arg) {
		$this->_cropY = $arg;
		return $this;
	 }

	/**
	 * Get the directory where the original image-files are stored
	 *
	 * @return string
	 */
	 public function getDirectoryPath() {
		return $this->_directoryPath;
	 }

	/**
	 * Set the y-position of the overlay image-path
	 *
	 * @param string $arg
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getDirectoryPath()
	 */
	 public function setDirectoryPath($arg) {
		$this->_directoryPath = (string)$arg;
		return $this;
	 }

	/**
	 * Get the fill color
	 *
	 * @return string
	 */
	 public function getFillColor() {
		return $this->_fillColor;
	 }

	/**
	 * Set the fillColor
	 *
	 * @param string $fillColor
	 * @return ThumbnailModel
	 * @see ThumbnailModel::getFillColor()
	 */
	 public function setFillColor($fillColor) {
		$this->_fillColor = $fillColor;
		return $this;
	 }
	 
	 /**
	  * Get the padding
	  * 
	  * @return array
	  */
	 public function getPadding() {
		 return $this->_padding;
	 }
	 
	 /**
	  * Set padding
	  * @param array $padding
	  * @return ThumbnailDefinition
	  */
	 public function setPadding(array $padding) {
		 $this->_padding = $padding;
		 return $this;
	 }
	 
	 /**
	  * Set flag: sharpen?
	  * 
	  * @access public
	  * @param bool $flag
	  * @return ThumbnailDefinition
	  */
	 public function setFlagSharpen($flag) {
		 $this->_sharpen = (bool) $flag;
		 return $this;
	 }
	 
	 /**
	  * Get flag: sharpen?
	  * 
	  * @access public
	  * @return bool
	  */
	 public function getFlagSharpen() {
		 return $this->_sharpen;
	 }
	 
	 /**
	  * Set flag: use semantic filenames?
	  * 
	  * @access public
	  * @param bool $flag
	  * @return ThumbnailDefinition
	  */
	 public function setUseSemanticFilenames($flag) {
		 $this->_useSemanticFilenames = (bool) $flag;
		 return $this;
	 }
	 
	 /**
	  * Get flag: use semnatic filenames?
	  * 
	  * @access public
	  * @return bool
	  */
	 public function getUseSemanticFilenames() {
		 return $this->_useSemanticFilenames;
	 }
	 
	 /**
	  * Set color distance
	  * 
	  * @access public
	  * @param int $flag
	  * @return ThumbnailDefinition
	  */
	 public function setTolerance($int) {
		$this->_tolerance = $int;
		 return $this;
	 }
	 
	 /**
	  * Get color distance
	  * 
	  * @access public
	  * @return bool
	  */
	 public function getTolerance() {
		 if (is_numeric($this->_tolerance)) {
			return $this->_tolerance;
		 }
		 return 10;
	 }

	 /* -- Other methods -- */

	 /**
	  * Create a new ThumbnailDefinition
	  * 
	  * @param string Id of the thumbnail definition
	  */
	 public function __construct($id = null) {
		//use the id of to parse the existing definition from config
		if(!is_null($id)) {
			//get the thumbnail-config
			$config = M_Application::getConfigOfModule('thumbnail');

			//get the config for the requested definition-id
			$definitionConfig = false;
			$definitionsConfig = $config->get('definitions');
			if ($definitionsConfig) {
				$definitionConfig = $definitionsConfig->get($id);
			}

			//if the definition hasn't been found...
			if (!$definitionConfig) {
//				throw new M_Exception(sprintf('Unknown definition "%s"', $id));
				return NULL;
			}

			//validate if the config
			$this->_validateDefinitionConfig($definitionConfig, $id);
			
			//get all data from config and store it in definition
			$this->setId($id);
			$this->setType($definitionConfig->get('type'));
			$this->setWidth($definitionConfig->get('width'));
			$this->setHeight($definitionConfig->get('height'));
			$this->setDirectoryPath($definitionConfig->get('directory'));
			$this->setOverlay($definitionConfig->get('overlay'));
			$this->setOverlayX($definitionConfig->get('overlayX'));
			$this->setOverlayY($definitionConfig->get('overlayY'));
			$this->setCropX($definitionConfig->get('cropX'));
			$this->setCropY($definitionConfig->get('cropY'));
			$this->setQuality($definitionConfig->get('quality'));
			$this->setFillColor($definitionConfig->get('fillColor'));
			$this->setTolerance($definitionConfig->get('tolerance'));
			$this->setPadding(explode(',',$definitionConfig->get('padding')));
			$this->setFlagSharpen(M_Helper::isBooleanTrue($definitionConfig->get('sharpen')));
			$this->setUseSemanticFilenames(M_Helper::isBooleanTrue($definitionConfig->get('useSemanticFilenames')));

			//add additional filters?
			$filters = $definitionConfig->get('filters');
			if($filters) {

				//filters are separated by a ','
				$filtersArray = explode(',',$filters);

				foreach($filtersArray AS $filter) {
					
					//check if additional arguments are provided
					$args = strpos($filter, ':');
					if ($args) {
						$args = explode(':', $filter);
					}else {
						$args = array($filter);
					}

					//execute the addImageFilter method
					call_user_func_array(array($this, "addImageFilter"), $args);
				}
			}
		}
	 }

	 /**
	  * Add a image-filter to the image to create
	  * various effects (grayscale, blur, ...)
	  *
	  * @see ThumbnailDefinition::getImageFilterTypes()
	  * 
	  * @param string $imageFilter
	  * @param mixed additional argument (optional)
	  * @param mixed additional argument (optional)
	  * @param mixed additional argument (optional)
	  * @return ThumnailDefinition
	  */
	 public function addImageFilter() {
		$args = func_get_args();

		//the first argument holds the type of filter
		$imageFilter = M_Helper::getArrayElement(0, $args);
		
		//check if additional parameters are given
		if(!in_array($imageFilter, self::getImageFilterTypes())) {
			throw new M_Exception(sprintf(
				'Cannot add unknown image filter %s',
				$imageFilter)
			);
		}

		//add the filter internally to process later on
		$this->_imageFilters[] = $args;

		return $this;
	 }

	 /**
	  * Validate a definition
	  *
	  * Note: throws exceptions
	  * 
	  * @param M_ConfigXml $config
	  * @param string The id of the definition which is being validated
	  */
	 public static function _validateDefinitionConfig(M_ConfigXml $config, $id = null) {
		$type = $config->get('type');

		//check if a type is specified
		if (empty($type)) {
			throw new M_Exception(sprintf('No type set for thumbnail definition %s', $id));
		}
		
		//check if this type is known
		if (!in_array($type, self::getTypes())) {
			throw new M_Exception(sprintf('Unknown thumbnail-type "%s"', $type));
		}

		//do some type-specific validation
		switch($type) {

			case self::TYPE_CROPPED:
			case self::TYPE_RESIZED:
			case self::TYPE_RESIZED_SMART:
			case self::TYPE_RESIZED_FORCED:
			case self::TYPE_RESIZED_FORCED_FIT:
			case self::TYPE_SQUARE:
				if (! $config->get('width')) {
					throw new M_Exception(sprintf(
						'Please specify width for thumbnail-type resized in definition %s',
						$id)
					);
				}
				break;
				
			case self::TYPE_CROPPED:
			case self::TYPE_RESIZED:
			case self::TYPE_RESIZED_SMART:
			case self::TYPE_RESIZED_FORCED:
			case self::TYPE_RESIZED_FORCED_FIT:
				if (! $config->get('height')) {
					throw new M_Exception(sprintf(
						'Please specify height for thumbnail-type resized in definition %s',
						$id)
					);
				}
				break;
		}
	 }

	 /**
	  * Get a list of all available thumbnail-types
	  * 
	  * @return array
	  */
	 public static function getTypes() {
		 return array(
			 self::TYPE_CROPPED,
			 self::TYPE_RESIZED,
			 self::TYPE_RESIZED_SMART,
			 self::TYPE_RESIZED_FORCED,
			 self::TYPE_RESIZED_FORCED_FIT,
			 self::TYPE_SQUARE
		 );
	 }

	 /**
	  * Get a list of all available image-filters
	  * 
	  * @return array
	  */
	 public static function getImageFilterTypes() {
		 return array(
			 self::IMAGE_FILTER_BLUR,
			 self::IMAGE_FILTER_GAUSSIAN_BLUR,
			 self::IMAGE_FILTER_GRAYSCALE,
			 self::IMAGE_FILTER_PIXELATE
		 );
	 }

	 /**
	  * Get the directory
	  *
	  * @see ThumbnailModel::getDirectoryPath()
	  * @return M_Directory
	  */
	 public function getDirectory() {
		 return new M_Directory($this->getDirectoryPath());
	 }

	 /**
	  * Get the overlay-image
	  *
	  * @see ThumbnailModel::getOverlay()
	  * @return M_Image
	  */
	 public function getOverlayImage() {
		 return new M_Image($this->_overlay);
	 }

	 /**
	  * Get the filter which will be applied to the thumbnail image
	  * 
	  * @param M_Image $image
	  * @return M_FilterImageResize
	  */
	 public function getFilter(M_Image $image) {
		 // if resized smart filter
		 if ($this->getType() == self::TYPE_RESIZED_SMART) {
			 $this->_setFilterResizeSmart($image);
		 }
		 
		 switch($this->getType()) {
			//apply cropped filter
			case self::TYPE_CROPPED:
				$filter = new M_FilterImageCrop(
					new M_FilterImage($image),
					$this->getWidth(),
					$this->getHeight(),
					$this->getCropX(),
					$this->getCropY()
				);
				break;

			case self::TYPE_RESIZED:
				$filter = new M_FilterImageResize(
					new M_FilterImage($image),
					$this->getWidth(),
					$this->getHeight()
				);
				break;

			case self::TYPE_RESIZED_FORCED:
				$filter = new M_FilterImageResizeForced(
					new M_FilterImage($image),
					$this->getWidth(),
					$this->getHeight(),
					true, //autogrow?
					$this->getCropX(),
					$this->getCropY()
				);
				break;

			case self::TYPE_RESIZED_FORCED_FIT:
				$filter = new M_FilterImageResizeForcedFit(
					new M_FilterImage($image),
					$this->getWidth(),
					$this->getHeight(),
					TRUE,
					$this->getFillColor()
				);

				$filter->setCropX($this->getCropX());
				$filter->setCropY($this->getCropY());
				
				break;

			case self::TYPE_SQUARE:
				$filter = new M_FilterImageResizeSquare(
					new M_FilterImage($image),
					$this->getWidth(),
					$this->getCropX(),
					$this->getCropY()
				);
				break;
			
			default:
				throw new M_Exception(sprintf(
					'No type or unknown type set in thumbnail definition: "%s"',
					$this->getType())
				);
		 }

		 // apply additional filters
		 foreach($this->_imageFilters AS $imageFilter) {
			$imageFilterName = array_shift($imageFilter);
			switch ($imageFilterName) {
				case self::IMAGE_FILTER_GRAYSCALE;
					$filter = new M_FilterImageGrayscale($filter);
					break;
				case self::IMAGE_FILTER_BLUR;
					$filter = new M_FilterImageBlur($filter);
					break;
				case self::IMAGE_FILTER_GAUSSIAN_BLUR;
					$filter = new M_FilterImageGaussianBlur($filter);
					break;
				case self::IMAGE_FILTER_PIXELATE;
					$filter = new M_FilterImagePixelate($filter);
					if(isset($imageFilter[0])) $filter->setBlocksize($imageFilter[0]);
					if(isset($imageFilter[1])) $filter->setAdvanced($imageFilter[1]);
					break;
				default:
					throw new M_ImageException(sprintf(
						'Unknown filter %s',
						$imageFilterName)
					);
			}
		 }

		 return $filter;
	 }

	 /**
	  * Create a thumbnail for an image
	  * 
	  * @param M_Image $image
	  * @return M_File The resized thumbnail-file
	  */
	 public function getThumbnail(M_Image $image) {
		 $thumbnailFile = $this->_getThumbnailFile($image);
		 
		 // If the file could not be found
		 if(!$thumbnailFile) {
			 return false;
		 }
		 
		 //check if the file already exists, if not create a new thumbnail
		 if (!$thumbnailFile->exists()) {
			$resource = $this->getThumbnailResource($image);
			$resource->saveToFile(
				$thumbnailFile->getDirectoryAndFilename(), 
				$thumbnailFile->getFileExtension(), 
				$this->getQuality()
			); 
		 }

		 return $thumbnailFile;
	 }

	 /**
	  * Get the resource from which a thumbnail can be saved
	  * 
	  * @param M_Image $image
	  * @return M_ImageResource The resource to which the filter is applied
	  */
	 public function getThumbnailResource(M_Image $image) {
		 $resource = $this->getFilter($image)->apply();
		 
		 //insert padding
		 if($this->getPadding()) {
			 $resource = $this->_addPadding($resource);
		 }
		 
		  //insert an overlay image, if needed
		 if ($this->getOverlay()) {
			 $this->_insertOverlay($resource);
		 }
		 
		 // If to be sharpened:
		 if($this->getFlagSharpen()) {
			 // Then, sharpen the image:
			 $resource->sharpen();
		 }
		 
		 // Return the image resource:
		 return $resource;
	 }

	 /**
	  * Create a unique has for the combination of an image and a definition
	  *
	  * This hash can be used to check if the exact same image already exists
	  * in the thumbnail folder.
	  * 
	  * @param M_Image $image
	  * @return string
	  */
	 public function getHash(M_Image $image) {
		 // We compose a unique string for, to identify the original image
		$id = array();
		$id[] = $image->getPath();
		$id[] = $this->getWidth();
		$id[] = $this->getHeight();
		$id[] = $this->getCropX();
		$id[] = $this->getCropY();
		$id[] = $this->getFillColor();
		$id[] = serialize($this->_imageFilters);
		
		// Note that we add the modification date of the image to the
		// hash, in order to make sure that the thumbnail is refreshed
		// when the original image changes:
		$id[] = $image->getLastModifiedDate()->toString(M_Date::FULL_DATE_TIME);

		// If an overlay is used we add the data from the overlay image too,
		// in order to make sure the thumbnail gets refreshed when the overlay
		// changes
		if ($this->getOverlay()) {
			$overlay = $this->getOverlayImage();
			 /* @var $overlay M_Image */
			$id[] = $overlay->getPath();
			$id[] = $overlay->getWidth();
			$id[] = $overlay->getHeight();
			$id[] = $overlay->getLastModifiedDate()->toString(M_Date::FULL_DATE_TIME);
		}

		return md5(implode('-' , $id));
	 }



	 /* -- Private functions -- */

	 /**
	  * Get the thumbnail file for an image
	  *
	  * @param M_Image $image
	  * @return M_File
	  */
	 private function _getThumbnailFile(M_Image $image) {
		if(!$image->exists()) {
			return false;
		}
		 
		M_Loader::loadModel('ThumbnailModel', 'thumbnail');
		$thumbnailDirectoryPath = ThumbnailModel::getInstance()->getConfig()->directory;
		
		//check if the thumbnail directory exists, if not create it
		$directory = new M_Directory($thumbnailDirectoryPath);
		if (!$directory->exists()) {
			if (!$directory->make()) {
				throw new M_Exception(
					sprintf('Failed to make directory %s, cannot save thumbnail file', $directory->getPath())
				);
			}
		}

		//return a new file based on the hash and located in the thumbnail
		//directory. It is not sure this file exists, it is simply the unique
		//filename for this image and the current definition
		return new M_File(
			M_Helper::rtrimCharlist($thumbnailDirectoryPath,'/') . '/' .
			$this->getHash($image) . '.' . $image->getFileExtension()
		);
	 }
	 
	 /**
	  * will calculate the 'distance' between 2 colors by comparing each component
	  * 
	  * @param M_Color $color1
	  * @param M_color $color2
	  * @return int
	  */
	 private function _colorDistance(M_Color $color1, M_color $color2) {
		 // get the rgb values
		 $rgb1 = $color1->getRGB();
		 $rgb2 = $color2->getRGB();
		 
		 // prepare the output
		 $out = 0;
		 
		 // compare each channel
		 foreach ($rgb1 as $i => $tmp) {
			 $out += abs($rgb1[$i] - $rgb2[$i]);
		 }
		 
		 return $out;
	 }
	 
	 
	 /**
	  * set filter resize smart
	  * 
	  * will determine which filter to use:
	  * if all the edges are the same color -> resized forced fit with auto color detection
	  * else -> resized forced
	  */
	 private function _setFilterResizeSmart(M_Image $image) {
		// get the actual image resource
		$gd = $image->getResourceGd();
		// get the dimensions and stote the top left pixel for comparison
		$width = $gd->getWidth();
		$height = $gd->getHeight();
		$topLeft = $gd->getPixel(0, 0)->getColor();
		// set the flag to true
		$sameColor = true;
		
		// get the input and target ratio
		$inputRatio = $width / $height;
		if ($this->_height) {
			$targetRatio = $this->_width / $this->_height; 
		}
		
		// the max color distance for comparison
		$maxColorDistance = $this->getTolerance();
		// the number of pixels to skip
		$pixelStep = 5;
		
		// check the top and bottom edges if we have to make the target image taler
		for ($i = 1; $i < $width && $sameColor && $targetRatio < $inputRatio; $i += $pixelStep) {
			// top edge different then top left pixel
			if ($this->_colorDistance($gd->getPixel($i, 0)->getColor(), $topLeft) > $maxColorDistance) {
				$sameColor = false;
			}
			// bottom edge different then top left pixel
			if ($this->_colorDistance($gd->getPixel($i, $height-1)->getColor(), $topLeft) > $maxColorDistance) {
				$sameColor = false;
			}
		}	
		
		// check the left and right edges if we have to make the target image wider
		for ($i = 1; $i < $height && $sameColor && $targetRatio > $inputRatio; $i += $pixelStep) {
			// left edge different then top left pixel
			if ($this->_colorDistance($gd->getPixel(0, $i)->getColor(), $topLeft) > $maxColorDistance) {
				$sameColor = false;
			}
			// right edge defferent then top left pixel
			if ($this->_colorDistance($gd->getPixel($width-1, $i)->getColor(), $topLeft) > $maxColorDistance) {
				$sameColor = false;
			}
		}

		// if the edges all have the same color as the top left pixel
		if ($sameColor) {
			// we want to use the Resized Forced Fit filter
			$this->setType(self::TYPE_RESIZED_FORCED_FIT);
			// and we set the fill color to the same one as the edge color
			$this->setFillColor($this->getFillColor() ? : $topLeft->getHex());
		} else {
			// we want to use the Resized Forced filter
			$this->setType(self::TYPE_RESIZED_FORCED);
		}
	 }
	 
	 
	 /**
	 * Insert overlay image into resource
	 *
	 * @param M_ImageResourceGd $resource
	 * @return boolean Success
	 */
	private function _insertOverlay(M_ImageResourceGd $resource) {
		// Set x position
		$x = $this->getOverlayX();

		// Default to center if no position is found
		if (is_null($x)) $x = 'center';

		if (!is_numeric($x)) {
			// Calculate position if a non-numeric value has been set
			switch ($x) {
				case M_FilterImageDecorator::POSITION_LEFT:
					$x = 0;
					break;
				case M_FilterImageDecorator::POSITION_RIGHT:
					// Calculate right edge
					$x = ($resource->getWidth() - $this->getOverlayImage()->getWidth());
					break;
				case M_FilterImageDecorator::POSITION_CENTER:
					// Calculate center of the image
					$x = ($resource->getWidth() / 2) - ($this->getOverlayImage()->getWidth() / 2);
					break;
				default:
					// Unknown format
					throw new M_Exception(sprintf('Unknown thumbnail overlay property for overlayX: "%s  in definition %s"', $x, $this->getId()));
			}
		}


		// Set Y position
		$y = $this->getOverlayY();

		// Default to center if no position is found
		if (is_null($y)) $y = 'center';

		if (!is_numeric($y)) {
			// Calculate position if a non-numeric value has been set
			switch ($y) {
				case M_FilterImageDecorator::POSITION_TOP:
					$y = 0;
					break;
				case M_FilterImageDecorator::POSITION_BOTTOM:
					// Calculate bottom edge
					$y = ($resource->getHeight() - $this->getOverlayImage()->getHeight());
					break;
				case M_FilterImageDecorator::POSITION_CENTER:
					// Calculate center of the image
					$y = ($resource->getHeight() / 2) - ($this->getOverlayImage()->getHeight() / 2);
					break;
				default:
					// Unknown format
					throw new M_Exception(sprintf('Unknown thumbnail property for overlayY: "%s"', $y));
			}
		}
		// Insert the overlay image
		return $resource->insertImage($this->getOverlayImage(), (int)$x, (int)$y);
	}
	
	/**
	 * Add padding
	 * 
	 * @param M_ImageResourceGd $resource
	 * @return M_ImageResourceGd
	 */
	private function _addPadding(M_ImageResourceGd $resource) {
		// First, we get the padding
		$paddingTop = M_Helper::getArrayElement(0, $this->_padding);
		$paddingRight = M_Helper::getArrayElement(1, $this->_padding);
		$paddingBottom = M_Helper::getArrayElement(2, $this->_padding);
		$paddingLeft = M_Helper::getArrayElement(3, $this->_padding);
		
		// If no padding is known: just return the resource:
		if(!$paddingTop && !$paddingRight && !$paddingBottom && !$paddingLeft) {
			return $resource;
		}
		
		// We calculate the new width and height
		$newWidth = $resource->getWidth() + $paddingLeft + $paddingRight;
		$newHeight = $resource->getHeight() + $paddingTop + $paddingBottom;
		
		// We create the new resource and we add the old one
		$newResource = new M_ImageResourceGd($newWidth, $newHeight);
		$newResource->drawFill(new M_Color($this->getFillColor()), 100);
		$newResource->insertResource($resource, $paddingLeft, $paddingTop);
		
		return $newResource;
	}
}
