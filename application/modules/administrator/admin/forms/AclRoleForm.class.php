<?php
class AclRoleForm extends M_Form {

	/**
	 * Create a new form
	 */
	public function __construct() {
		$this->_addFieldCode()
			->_addFieldTitle()
			->_addFieldDescription();
		
		parent::__construct('aclrole');
	}

	/**
	 * Add the title field to the form
	 * @return AclRoleForm
	 */
	protected function _addFieldCode() {
		$field = new M_FieldText('role-code');
		$field->setMandatory(true);
		$field->setTitle(t('Code'));
		$this->addField($field);
		return $this;
	}

	/**
	 * Add the title field to the form
	 * @return AclRoleForm
	 */
	protected function _addFieldTitle() {
		$field = new M_FieldText('role-title');
		$field->setMandatory(true);
		$field->setTitle(t('Title'));
		$this->addField($field);
		return $this;
	}

	/**
	 * Add the description field to the form
	 * @return AclRoleForm
	 */
	protected function _addFieldDescription() {
		$field = new M_FieldTextarea('role-description');
		$field->setTitle(t('Description'));
		$field->setDescription(t('Additional information about this role: this might come in handy to distuingish the roles from eachother when a lot of roles are used.'));
		$this->addField($field);
		return $this;
	}

	/**
	 * Tell the form this is the role we are editing
	 * 
	 * @param M_AclRole $role
	 * @return AclRoleForm
	 */
	public function setRole(M_AclRole $role) {
		$this->getField('role-title')->setDefaultValue($role->getTitle());
		$this->getField('role-description')->setDefaultValue($role->getDescription());
		$codeField = $this->getField('role-code');
		$codeField->setDefaultValue($role->getRoleId());
		$codeField->setReadonly(true);

		return $this;
	}

	/**
	 * Save the role
	 * 
	 * @param array $values
	 */
	public function actions(array $values) {
		$acl = M_Acl::getInstance();

		//get the role if it already existed
		$role = $acl->getRole($values['role-code']);
		if(!$role) {
			$role = new M_AclRole($values['role-code']);
		}

		$role->setTitle($values['role-title']);
		$role->setDescription($values['role-description']);

		$acl->addRole($role);
		return true;
	}
}