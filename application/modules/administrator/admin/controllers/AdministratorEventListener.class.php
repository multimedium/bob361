<?php
/**
 * AdministratorEventListener
 */
class AdministratorEventListener {
	
	/**
	 * Construct
	 *
	 * @param AdminController $controller
	 */
	public function __construct(AdminController $controller) {
		// Add Event listeners
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_EDIT, 
			array($this, 'onDataObjectBeforeEdit')
		);
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_CREATE,
			array($this, 'onDataObjectBeforeCreate')
		);
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_SAVE,
			array($this, 'onDataObjectBeforeSave')
		);
	}

	/**
	 * When we are createing a new administrator
	 *
	 * @param AdminControllerEvent $event
	 * @return void
	 */
	public function onDataObjectBeforeCreate(AdminControllerEvent $event) {
		if($event->getForm()->getId() != 'administrator') return false;

		$this->_addAclRoles($event);
		
		//when an administrator is added: set the password field to mandatory
		$this->_setPassword($event);
	}
	
	/**
	 * Do ... before object gets edited
	 * 
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectBeforeEdit(AdminControllerEvent $event) {
		if($event->getForm()->getId() != 'administrator') return false;
		
		$this->_addAclRoles($event);

		//add a description to the password field when an account is edited
		$this->_setPassword($event, true);
	}

	/**
	 *
	 * @param AdminControllerEvent $event
	 * @return void
	 */
	public function onDataObjectBeforeSave(AdminControllerEvent $event) {
		if($event->getForm()->getId() != 'administrator') return false;
		
		//get the password value and set it into the account, only if it's not empty.
		//this way we can make the field only mandatory when a new account is added
		$field = $this->_getField($event, 'passwordTmp');
		if($field && $field->getValue() != "") {
			$event->getDataObject()->setPassword($field->deliver());
		}
	}

	/**
	 * If an account is edited: we want to validate if both passwords match and
	 * if it needs to be edited (if empty, keep the currenct password)
	 *
	 * @param AdminControllerEvent $event
	 * @param bool edit-mode
	 */
	protected function _setPassword(AdminControllerEvent $event, $edit = false) {
		if($event->getForm()->getId() != 'administrator') return false;

		//get fields
		$passwordField = $this->_getField($event, 'passwordTmp');
		$passwordField2 = $this->_getField($event, 'passwordTmp2');

		//add a description
		$passwordField->setDescription(
			t('Leave empty to keep the current password.')
		);

		$passwordValue = $passwordField->getValue();

		//if a password is given
		if($passwordValue != "") {

			//add a validator to check both passwords
			$validator = new M_ValidatorEquals();
			$validator->setCriteria(M_ValidatorEquals::VALUE, $passwordField->deliver());
			$passwordField2->addValidatorObject($validator, t("Passwords don't match"));

			//make second password-field mandatory
			$passwordField2->setMandatory(true);
		}elseif($edit == true) {
			$passwordField->setMandatory(false);
			$passwordField2->setMandatory(false);
		}
	}

	/**
	 * Get a field from the Account-form
	 *
	 * @param AdminControllerEvent $event
	 * @param string $fieldId
	 * @return M_Field
	 */
	private function _getField(AdminControllerEvent $event, $fieldId) {
		$field = $event->getForm()->getField($fieldId);
		if(!$field) {
			throw new M_Exception(sprintf('field %s not found', $fieldId));
		}

		return $field;
	}
	
	/**
	 * Get the roles from {@link M_Acl} and set them in the form
	 * 
	 * @param AdminControllerEvent $event
	 * @return void
	 */
	private function _addAclRoles(AdminControllerEvent $event) {
		$form = $event->getForm();

		$roles = array();
		/* @var $role M_AclRole */
		foreach(M_Acl::getInstance()->getRoles() AS $role) {
			$roles[$role->getRoleId()] = $role->getTitle();
		}
		$field = $form->getField('aclRoleId');
		if ($field) $field->setItems($roles);
	}
}