<?php
class AclAdvancedView extends AdminPageView {
	
	/**
	 * @var M_ArrayIterator
	 */
	private $_roles;

	/**
	 * @var M_ArrayIterator
	 */
	private $_resources;

	/**
	 * @var M_ArrayIterator
	 */
	private $_defaultPermissions;

	/**
	 * Assign a list of {@link M_AclRole} instances to the view
	 * 
	 * @param M_ArrayIterator $roles
	 * @return AclView
	 */
	public function setRoles(M_ArrayIterator $roles) {
		$this->_roles = $roles;
		return $this;
	}

	/**
	 * Assign a list of {@link M_AclResource} instances to the view
	 *
	 * @param M_ArrayIterator $resources
	 * @return AclView
	 */
	public function setResources(M_ArrayIterator $resources) {
		$this->_resources = $resources;
		return $this;
	}

	/**
	 * Assign a list of {@link M_AclPermission} instances to the view
	 *
	 * @param M_ArrayIterator $defaultPermissions
	 * @return AclView
	 */
	public function setDefaultPermissions(M_ArrayIterator $defaultPermissions) {
		$this->_defaultPermissions = $defaultPermissions;
		return $this;
	}

	/**
	 * Do some preprocessing
	 */
	protected function preprocessing() {
		/* Sort resources per module */
		/* @var $resource M_AclResource */
		$resourcesByModule = array();
		foreach($this->_resources AS $resource) {
			//store all DataObjects for every module in an array
			if (!isset($resourcesByModule[$resource->getModuleId()])) {
				$resourcesByModule[$resource->getModuleId()] = array();
			}
			$resourcesByModule[$resource->getModuleId()][] = $resource;
		}
		
		$this->assign('roles', $this->_roles);
		$this->assign('resources', $resourcesByModule);
		$this->assign('defaultPermissions', $this->_defaultPermissions);
	}
	
	/**
	 * Get the resource for this view
	 *
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(
			AdminLoader::getAdminResourcesPath('administrator').'/templates/AclAdvanced.tpl'
		);
	}
}