<div id="idContentPaneViewport" class="content-pane-viewport fill-remaining-vspace">
	<div class="breadcrumb">
		<ul>
			<li><a href="{link href="admin/route/administrator/overviewRole"}">{t text="ACL Roles"}</a></li>
			<li><span title="{t|att text="Manage your ACL roles"}">{t text="Edit role"}</span></li>
		</ul>
	</div>
	{$form->setModuleOwner('admin')}
	{$form->getView()}
</div>