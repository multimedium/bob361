<?php
/**
 * AdministratorMapper
 * 
 * @package App
 * @subpackage Administrator
 */
class AdministratorMapper extends M_DataObjectMapper {
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'administrator';
	}

	/**
	 * AddFilterActive
	 *
	 * Will only fetch the administrators that are currently active
	 *
	 * @access public
	 * @return AdministratorMapper
	 */
	public function addFilterActive() {
		return $this->addFilter(new M_DbQueryFilterWhere(
			'active',
			'1'
		));
	}
	
	/**
	 * Get {@link Administrator}, by name and password
	 * 
	 * @access public
	 * @param string $name
	 * 		The username of the Administrator
	 * @param string $password
	 * 		The password of the Administrator
	 * @return Administrator|bool
	 * 		Will return an instance of {@link Administrator}, or FALSE
	 * 		if no administrator could have been found
	 */
	public function getByCredentials($name, $password) {
		$select = $this->_getFilteredSelect()
			->where($this->getField('username')->db_name . ' = ?', (string) $name)
			->where($this->getField('password')->db_name . ' = ?', (string) $password);
		return $this->_fetchOne($select->execute());
	}
	
	/**
	 * Get by Administrator Key (Hash) Code
	 * 
	 * Is used to look up a unique {@link Administrator}, with a hash
	 * code, such as the one that would be produced by a Yubikey
	 * 
	 * @access public
	 * @param string $key
	 * 		The unique key code of the Administrator
	 * @return Administrator|bool
	 * 		Will return an instance of {@link Administrator}, or FALSE
	 * 		if no administrator could have been found
	 */
	public function getByKeyCode($key) {
		$select = $this->_getFilteredSelect()->where($this->getField('key')->db_name . ' = ?', (string) $key);
		return $this->_fetchOne($select->execute());
	}
}