<?php
/**
 * NewsController
 *
 * @package App
 * @subpackage News
 */
class NewsController extends PageController {

	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Routing
	 *
	 * @see M_Controller::__router()
	 * @access public
	 * @return array
	 */
	public function __router() {
		return array(
			// RSS
			'rss(:any)'					=>	'rss$1',
			
			// Main news overview
			'index(:any)'				=>	'index$1',
			
			// Detail page
			'(:string)/(:string)(:any)'	=>	'detail/$1/$2$3',
			
			// By default, any other path element points to the news
			// category page
			'(:any)'					=>	'category/$1'
		);
	}
	
	/**
	 * Index
	 * 
	 * Will redirect to the first {@link NewsCategory} page
	 * 
	 * @access public
	 * @return void
	 */
	public function index() {
		// Fetch the first news category
		$category = M_Loader
			::getDataObjectMapper('NewsCategory', 'news')
			->addFilterDefaultSorting()
			->addFilterPublished()
			->getOne();
		
		// Display the category page
		/* @var $category NewsCategory */
		$this->category($category->getUrl(), $category);
	}
	
	/**
	 * Category
	 * 
	 * Will construct and display the overview page of a single
	 * {@link NewsCategory}
	 * 
	 * @access public
	 * @param string $url
	 * @param NewsCategory $category
	 *		Optional: if a {@link NewsCategory} is provided already, it
	 *		will be used and the provided url will be ignored
	 * @return void
	 */
	public function category($url, NewsCategory $category = null) {
		// Is a category given?
		if(! $category) {
			// Use the url to fetch it
			$category = M_Loader
				::getDataObjectMapper('NewsCategory', 'news')
				->addFilterPublished()
				->getByUrlSuffix($url);
			
			// If not found
			if(! $category) $this->_404();
		}

		// Construct the view
		M_Loader::getView('NewsCategoryView', 'news')
			// Provide the category
			->setNewsCategory($category)
			// Provide the news item
			->setNews(
				M_Loader::getDataObjectMapper('News', 'news')
				->addFilterPublishedOnly()
				->addFilterSortByDate()
				->addFilterNewsCategory($category)
				->getAll()
			)
			// Set additional page properties
			->setActivePath($category->getUrl())
			->setPageTitle($category->getTitle())
			->setPageDescription($category->getTitle())
			// Add the homepage breadcrumb entry
			->addBreadcrumbEntry(
				t('Home'), 
				M_Request::getLink(), 
				t('Ga terug naar de @name homepage', array(
					'@name' => M_ApplicationIdentity::getInstance()->getContact()->getName()
				))
			)
			// Add a breadcrumb entry for the category
			->addBreadcrumbEntry(
				$category->getTitle(), 
				NULL, 
				$category->getTitle()
			)
			// And display
			->display();
	}
	
	/**
	 * Detail
	 * 
	 * Displays the detail page of a single {@link News} item
	 * 
	 * @access public
	 * @param string $categoryUrl
	 * @param string $url
	 * @return void
	 */
	public function detail($categoryUrl, $url) {
		// Fetch the news item
		$news = M_Loader::getDataObjectMapper('News', 'news')
			->addFilterPublishedOnly()
			->addFilterLocaleExists()
			->getByUrlSuffix($url);
		
		// If not found
		/* @var $news News */
		if(! $news) $this->_404();
		
		// Fetch the news category
		$category = $news->getCategory();
		
		// If not found, or the url doesn't match, or the category isn't
		// published
		if(! $category || $category->getUrl() != $categoryUrl || ! $category->getPublished()) {
			$this->_404();
		}
		
		// Construct the view
		M_Loader::getView('NewsDetailView', 'news')
			// Set the category
			->setNewsCategory($news->getCategory())
			// Set the news item itself
			->setNews($news)
			// Set some additional page properties
			->setPageTitle($news->getTitle())
			->setPageDescription($news->getTitle())
			->setActivePath($category->getUrl())
			// Add the homepage breadcrumb entry
			->addBreadcrumbEntry(
				t('Home'), 
				M_Request::getLink(), 
				t('Ga terug naar de @name homepage', array(
					'@name' => M_ApplicationIdentity::getInstance()->getContact()->getName()
				))
			)
			// Add a breadcrumb entry for the category
			->addBreadcrumbEntry(
				$category->getTitle(), 
				M_Request::getLink('publications/' . $category->getUrl()), 
				$category->getTitle()
			)
			// Add a breadcrumb entry for the news item itself
			->addBreadcrumbEntry(
				$news->getTitle(), 
				NULL, 
				$news->getTitle()
			)
			// And display
			->display();
	}
	
	/**
	* RSS: Highlighted news
	*
	* @access public
	* @return void
	*/
	public function rss($categoryUrl) {
		// Use the url to fetch it
		$category = M_Loader
			::getDataObjectMapper('NewsCategory', 'news')
			->addFilterPublished()
			->getByUrlSuffix($categoryUrl);
		
		/* @var $category NewsCategory */
		// If not found
		if(! $category) {
			$this->_404();
		}
		
		// Fetch news, from this category
		$newsCollection = M_Loader::getDataObjectMapper('News', 'news')
			->addFilterPublishedOnly()
			->addFilterSortByDate()
			->addFilterNewsCategory($category)
			->addFilter(new M_DbQueryFilterLimit(0, 10))
			->getAll();
		
		// Construct a new RSS Feed
		$feed = new M_FeedRss();

		// For each of the (latest) news items in the website:
		foreach($newsCollection as $news) {
			/* @var $news News */
			// Construct a new item in the feed:
			$item = new M_FeedRssItem();

			// Set the title of the item
			$item->setTitle($news->getTitle());

			// Set the description of the news item
			$item->setDescription(M_Text::getTextAsSnippet($news->getText(), 255));

			// Set the link to the item's detail page in the website
			$item->setLink(new M_Uri(M_Request::getLink('publications/' . $category->getUrl() . '/' . $news->getUrl())));

			// If a start date is known for the news item
			if($news->getStartDate()) {
				// Then, we add it as the publication date to the RSS feed:
				$item->setPublicationDate($news->getStartDate());
			}

			// Add the item to the feed:
			$feed->addItem($item);
		}
		
		// Output the feed
		$this->_rss(
			$feed,
			$category->getTitle(),
			t('Volg @name via onze RSS Feed', array(
				'@name' => $this->_getContact()->getName()
			)),
			'publications/' . $category->getUrl()
		);
	}
}