<?php
/** 
 * NewsCategoryDataObject class
 * 
 * @see NewsCategoryDataObjectMapper
 * @see NewsCategoryDataObjectModel
 * @package App
 */
class NewsCategoryDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link NewsCategoryDataObject::getId()}
	 * - {@link NewsCategoryDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * title ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * title. To get/set the value of the title, read the documentation on 
	 * 
	 * - {@link NewsCategoryDataObject::getTitle()}
	 * - {@link NewsCategoryDataObject::getTitleId()}
	 * - {@link NewsCategoryDataObject::setTitle()}
	 * - {@link NewsCategoryDataObject::setTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_titleId;
	
	/**
	 * url ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * url. To get/set the value of the url, read the documentation on 
	 * 
	 * - {@link NewsCategoryDataObject::getUrl()}
	 * - {@link NewsCategoryDataObject::getUrlId()}
	 * - {@link NewsCategoryDataObject::setUrl()}
	 * - {@link NewsCategoryDataObject::setUrlId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_urlId;
	
	/**
	 * realm
	 * 
	 * This property stores the realm. To get/set the value of the realm,
	 * read the documentation on 
	 * 
	 * - {@link NewsCategoryDataObject::getRealm()}
	 * - {@link NewsCategoryDataObject::setRealm()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_realm;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link NewsCategoryDataObject::getPublished()}
	 * - {@link NewsCategoryDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;
	
	/**
	 * order
	 * 
	 * This property stores the order. To get/set the value of the order,
	 * read the documentation on 
	 * 
	 * - {@link NewsCategoryDataObject::getOrder()}
	 * - {@link NewsCategoryDataObject::setOrder()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_order;
	
	/**
	 * Get title
	 * 
	 * Get title in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the title
	 * @return string|NULL
	 */
	public function getTitle($locale = NULL) {
		return $this->_getLocalizedText('title', $locale);
	}
	
	/**
	 * Get title ID
	 * 
	 * Get numeric reference to the translated title .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTitleId() {
		return $this->_titleId;
	}
	
	/**
	 * Set title
	 * 
	 * Set title, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated title
	 * @param string $locale
	 * 	The language in which to set the title
	 * @return void
	 */
	public function setTitle($text, $locale = NULL) {
		$this->_setLocalizedText('title', $text, $locale);
	}
	
	/**
	 * Set title ID
	 * 
	 * Set numeric reference to the translated title .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of title
	 * @return void
	 */
	public function setTitleId($id) {
		$this->_titleId = $id;
	}
	
	/**
	 * Get url
	 * 
	 * Get url in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the url
	 * @return string|NULL
	 */
	public function getUrl($locale = NULL) {
		return $this->_getLocalizedText('url', $locale);
	}
	
	/**
	 * Get url ID
	 * 
	 * Get numeric reference to the translated url .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getUrlId() {
		return $this->_urlId;
	}
	
	/**
	 * Set url
	 * 
	 * Set url, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated url
	 * @param string $locale
	 * 	The language in which to set the url
	 * @return void
	 */
	public function setUrl($text, $locale = NULL) {
		$this->_setLocalizedText('url', $text, $locale);
	}
	
	/**
	 * Set url ID
	 * 
	 * Set numeric reference to the translated url .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of url
	 * @return void
	 */
	public function setUrlId($id) {
		$this->_urlId = $id;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return NewsCategoryDataObject
	 */
	public function setId($id) {
	$this->_id = $id;
		return $this;
	}
	
	/**
	 * Get realm
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getRealm() {
		return $this->_realm;
	}
	
	/**
	 * Set realm
	 * 
	 * @access public
	 * @param mixed $realm
	 * @return NewsCategoryDataObject
	 */
	public function setRealm($realm) {
	$this->_realm = $realm;
		return $this;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return NewsCategoryDataObject
	 */
	public function setPublished($published) {
	$this->_published = $published;
		return $this;
	}
	
	/**
	 * Get order
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Set order
	 * 
	 * @access public
	 * @param mixed $order
	 * @return NewsCategoryDataObject
	 */
	public function setOrder($order) {
	$this->_order = $order;
		return $this;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return NewsCategoryMapper $mapper
	 */
	public function getMapper() {
		if (is_null($this->_mapper)) {
			M_Loader::loadDataObjectMapper("NewsCategory","news");
			$this->_mapper = new NewsCategoryMapper();
		}
		return $this->_mapper;
	}
	
		
}