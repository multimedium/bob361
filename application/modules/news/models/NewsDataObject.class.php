<?php
/** 
 * NewsDataObject
 * 
 * @see NewsMapper
 * @see News
 * @package App
 * @subpackage News
 */
class NewsDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link NewsDataObject::getId()}
	 * - {@link NewsDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * categoryId
	 * 
	 * This property stores the categoryId. To get/set the value of the
	 * categoryId, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getCategoryId()}
	 * - {@link NewsDataObject::setCategoryId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_categoryId;
	
	/**
	 * startDate
	 * 
	 * This property stores the startDate. To get/set the value of the
	 * startDate, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getStartDate()}
	 * - {@link NewsDataObject::setStartDate()}
	 * 
	 * @access protected
	 * @var M_Date
	 */
	protected $_startDate;
	
	/**
	 * endDate
	 * 
	 * This property stores the endDate. To get/set the value of the endDate,
	 * read the documentation on 
	 * 
	 * - {@link NewsDataObject::getEndDate()}
	 * - {@link NewsDataObject::setEndDate()}
	 * 
	 * @access protected
	 * @var M_Date
	 */
	protected $_endDate;
	
	/**
	 * publicationDate
	 * 
	 * This property stores the publicationDate. To get/set the value of the
	 * publicationDate, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getPublicationDate()}
	 * - {@link NewsDataObject::setPublicationDate()}
	 * 
	 * @access protected
	 * @var M_Date
	 */
	protected $_publicationDate;
	
	/**
	 * webpage
	 * 
	 * This property stores the webpage. To get/set the value of the webpage,
	 * read the documentation on 
	 * 
	 * - {@link NewsDataObject::getWebpage()}
	 * - {@link NewsDataObject::setWebpage()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_webpage;
	
	/**
	 * projectId
	 * 
	 * This property stores the projectId. To get/set the value of the
	 * projectId, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getProjectId()}
	 * - {@link NewsDataObject::setProjectId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_projectId;
	
	/**
	 * title ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * title. To get/set the value of the title, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getTitle()}
	 * - {@link NewsDataObject::getTitleId()}
	 * - {@link NewsDataObject::setTitle()}
	 * - {@link NewsDataObject::setTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_titleId;
	
	/**
	 * author ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * author. To get/set the value of the author, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getAuthor()}
	 * - {@link NewsDataObject::getAuthorId()}
	 * - {@link NewsDataObject::setAuthor()}
	 * - {@link NewsDataObject::setAuthorId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_authorId;
	
	/**
	 * medium ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * medium. To get/set the value of the medium, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getMedium()}
	 * - {@link NewsDataObject::getMediumId()}
	 * - {@link NewsDataObject::setMedium()}
	 * - {@link NewsDataObject::setMediumId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_mediumId;
	
	/**
	 * page ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * page. To get/set the value of the page, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getPage()}
	 * - {@link NewsDataObject::getPageId()}
	 * - {@link NewsDataObject::setPage()}
	 * - {@link NewsDataObject::setPageId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_pageId;
	
	/**
	 * publicationTitle ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * publicationTitle. To get/set the value of the publicationTitle, read
	 * the documentation on 
	 * 
	 * - {@link NewsDataObject::getPublicationTitle()}
	 * - {@link NewsDataObject::getPublicationTitleId()}
	 * - {@link NewsDataObject::setPublicationTitle()}
	 * - {@link NewsDataObject::setPublicationTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_publicationTitleId;
	
	/**
	 * url ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * url. To get/set the value of the url, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getUrl()}
	 * - {@link NewsDataObject::getUrlId()}
	 * - {@link NewsDataObject::setUrl()}
	 * - {@link NewsDataObject::setUrlId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_urlId;
	
	/**
	 * text ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * text. To get/set the value of the text, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getText()}
	 * - {@link NewsDataObject::getTextId()}
	 * - {@link NewsDataObject::setText()}
	 * - {@link NewsDataObject::setTextId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_textId;
	
	/**
	 * imageFilename
	 * 
	 * This property stores the imageFilename. To get/set the value of the
	 * imageFilename, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getImageFilename()}
	 * - {@link NewsDataObject::setImageFilename()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_imageFilename;

	/**
	 * pdfFilename
	 *
	 * This property stores the pdfFilename. To get/set the value of the
	 * imageFilename, read the documentation on
	 *
	 * - {@link NewsDataObject::getPdfFilename()}
	 * - {@link NewsDataObject::setPdfFilename()}
	 *
	 * @access protected
	 * @var mixed
	 */
	protected $_pdfFilename;

	/**
	 * highlight
	 * 
	 * This property stores the highlight. To get/set the value of the
	 * highlight, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getHighlight()}
	 * - {@link NewsDataObject::setHighlight()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_highlight;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getPublished()}
	 * - {@link NewsDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;
	
	/**
	 * Get title
	 * 
	 * Get title in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the title
	 * @return string|NULL
	 */
	public function getTitle($locale = NULL) {
		return $this->_getLocalizedText('title', $locale);
	}
	
	/**
	 * Get title ID
	 * 
	 * Get numeric reference to the translated title .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTitleId() {
		return $this->_titleId;
	}
	
	/**
	 * Set title
	 * 
	 * Set title, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated title
	 * @param string $locale
	 * 	The language in which to set the title
	 * @return void
	 */
	public function setTitle($text, $locale = NULL) {
		return $this->_setLocalizedText('title', $text, $locale);
	}
	
	/**
	 * Set title ID
	 * 
	 * Set numeric reference to the translated title .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of title
	 * @return void
	 */
	public function setTitleId($id) {
		$this->_titleId = $id;
		return $this;
	}
	
	/**
	 * Get author
	 * 
	 * Get author in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the author
	 * @return string|NULL
	 */
	public function getAuthor($locale = NULL) {
		return $this->_getLocalizedText('author', $locale);
	}
	
	/**
	 * Get author ID
	 * 
	 * Get numeric reference to the translated author .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getAuthorId() {
		return $this->_authorId;
	}
	
	/**
	 * Set author
	 * 
	 * Set author, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated author
	 * @param string $locale
	 * 	The language in which to set the author
	 * @return void
	 */
	public function setAuthor($text, $locale = NULL) {
		return $this->_setLocalizedText('author', $text, $locale);
	}
	
	/**
	 * Set author ID
	 * 
	 * Set numeric reference to the translated author .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of author
	 * @return void
	 */
	public function setAuthorId($id) {
		$this->_authorId = $id;
		return $this;
	}
	
	/**
	 * Get medium
	 * 
	 * Get medium in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the medium
	 * @return string|NULL
	 */
	public function getMedium($locale = NULL) {
		return $this->_getLocalizedText('medium', $locale);
	}
	
	/**
	 * Get medium ID
	 * 
	 * Get numeric reference to the translated medium .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getMediumId() {
		return $this->_mediumId;
	}
	
	/**
	 * Set medium
	 * 
	 * Set medium, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated medium
	 * @param string $locale
	 * 	The language in which to set the medium
	 * @return void
	 */
	public function setMedium($text, $locale = NULL) {
		return $this->_setLocalizedText('medium', $text, $locale);
	}
	
	/**
	 * Set medium ID
	 * 
	 * Set numeric reference to the translated medium .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of medium
	 * @return void
	 */
	public function setMediumId($id) {
		$this->_mediumId = $id;
		return $this;
	}
	
	/**
	 * Get page
	 * 
	 * Get page in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the page
	 * @return string|NULL
	 */
	public function getPage($locale = NULL) {
		return $this->_getLocalizedText('page', $locale);
	}
	
	/**
	 * Get page ID
	 * 
	 * Get numeric reference to the translated page .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getPageId() {
		return $this->_pageId;
	}
	
	/**
	 * Set page
	 * 
	 * Set page, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated page
	 * @param string $locale
	 * 	The language in which to set the page
	 * @return void
	 */
	public function setPage($text, $locale = NULL) {
		return $this->_setLocalizedText('page', $text, $locale);
	}
	
	/**
	 * Set page ID
	 * 
	 * Set numeric reference to the translated page .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of page
	 * @return void
	 */
	public function setPageId($id) {
		$this->_pageId = $id;
		return $this;
	}
	
	/**
	 * Get publicationTitle
	 * 
	 * Get publicationTitle in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the publicationTitle
	 * @return string|NULL
	 */
	public function getPublicationTitle($locale = NULL) {
		return $this->_getLocalizedText('publicationTitle', $locale);
	}
	
	/**
	 * Get publicationTitle ID
	 * 
	 * Get numeric reference to the translated publicationTitle .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getPublicationTitleId() {
		return $this->_publicationTitleId;
	}
	
	/**
	 * Set publicationTitle
	 * 
	 * Set publicationTitle, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated publicationTitle
	 * @param string $locale
	 * 	The language in which to set the publicationTitle
	 * @return void
	 */
	public function setPublicationTitle($text, $locale = NULL) {
		return $this->_setLocalizedText('publicationTitle', $text, $locale);
	}
	
	/**
	 * Set publicationTitle ID
	 * 
	 * Set numeric reference to the translated publicationTitle .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of publicationTitle
	 * @return void
	 */
	public function setPublicationTitleId($id) {
		$this->_publicationTitleId = $id;
		return $this;
	}
	
	/**
	 * Get url
	 * 
	 * Get url in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the url
	 * @return string|NULL
	 */
	public function getUrl($locale = NULL) {
		return $this->_getLocalizedText('url', $locale);
	}
	
	/**
	 * Get url ID
	 * 
	 * Get numeric reference to the translated url .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getUrlId() {
		return $this->_urlId;
	}
	
	/**
	 * Set url
	 * 
	 * Set url, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated url
	 * @param string $locale
	 * 	The language in which to set the url
	 * @return void
	 */
	public function setUrl($text, $locale = NULL) {
		return $this->_setLocalizedText('url', $text, $locale);
	}
	
	/**
	 * Set url ID
	 * 
	 * Set numeric reference to the translated url .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of url
	 * @return void
	 */
	public function setUrlId($id) {
		$this->_urlId = $id;
		return $this;
	}
	
	/**
	 * Get text
	 * 
	 * Get text in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the text
	 * @return string|NULL
	 */
	public function getText($locale = NULL) {
		return $this->_getLocalizedText('text', $locale);
	}
	
	/**
	 * Get text ID
	 * 
	 * Get numeric reference to the translated text .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTextId() {
		return $this->_textId;
	}
	
	/**
	 * Set text
	 * 
	 * Set text, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated text
	 * @param string $locale
	 * 	The language in which to set the text
	 * @return void
	 */
	public function setText($text, $locale = NULL) {
		return $this->_setLocalizedText('text', $text, $locale);
	}
	
	/**
	 * Set text ID
	 * 
	 * Set numeric reference to the translated text .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of text
	 * @return void
	 */
	public function setTextId($id) {
		$this->_textId = $id;
		return $this;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return NewsDataObject
	 */
	public function setId($id) {
		$this->_id = $id;
		return $this;
	}
	
	/**
	 * Get categoryId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCategoryId() {
		return $this->_categoryId;
	}
	
	/**
	 * Set categoryId
	 * 
	 * @access public
	 * @param mixed $categoryId
	 * @return NewsDataObject
	 */
	public function setCategoryId($categoryId) {
		$this->_categoryId = $categoryId;
		return $this;
	}
	
	/**
	 * Get startDate
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getStartDate() {
		return $this->_startDate;
	}
	
	/**
	 * Set startDate
	 * 
	 * @access public
	 * @param M_Date $startDate
	 * @return NewsDataObject
	 */
	public function setStartDate( M_Date $startDate = null) {
		$this->_startDate = $startDate;
		return $this;
	}
	
	/**
	 * Get endDate
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getEndDate() {
		return $this->_endDate;
	}
	
	/**
	 * Set endDate
	 * 
	 * @access public
	 * @param M_Date $endDate
	 * @return NewsDataObject
	 */
	public function setEndDate( M_Date $endDate = null) {
		$this->_endDate = $endDate;
		return $this;
	}
	
	/**
	 * Get publicationDate
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getPublicationDate() {
		return $this->_publicationDate;
	}
	
	/**
	 * Set publicationDate
	 * 
	 * @access public
	 * @param M_Date $publicationDate
	 * @return NewsDataObject
	 */
	public function setPublicationDate( M_Date $publicationDate = null) {
		$this->_publicationDate = $publicationDate;
		return $this;
	}
	
	/**
	 * Get webpage
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getWebpage() {
		return $this->_webpage;
	}
	
	/**
	 * Set webpage
	 * 
	 * @access public
	 * @param mixed $webpage
	 * @return NewsDataObject
	 */
	public function setWebpage($webpage) {
		$this->_webpage = $webpage;
		return $this;
	}
	
	/**
	 * Get projectId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getProjectId() {
		return $this->_projectId;
	}
	
	/**
	 * Set projectId
	 * 
	 * @access public
	 * @param mixed $projectId
	 * @return NewsDataObject
	 */
	public function setProjectId($projectId) {
		$this->_projectId = $projectId;
		return $this;
	}
	
	/**
	 * Get imageFilename
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getImageFilename() {
		return $this->_imageFilename;
	}
	
	/**
	 * Set imageFilename
	 * 
	 * @access public
	 * @param mixed $imageFilename
	 * @return NewsDataObject
	 */
	public function setImageFilename($imageFilename) {
		$this->_imageFilename = $imageFilename;
		return $this;
	}

	/**
	 * Get pdfFilename
	 *
	 * @access public
	 * @return mixed
	 */
	public function getPdfFilename() {
		return $this->_pdfFilename;
	}

	/**
	 * Set pdfFilename
	 *
	 * @access public
	 * @param mixed $pdfFilename
	 * @return NewsDataObject
	 */
	public function setPdfFilename($pdfFilename) {
		$this->_pdfFilename = $pdfFilename;
		return $this;
	}
	
	/**
	 * Get highlight
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getHighlight() {
		return $this->_highlight;
	}
	
	/**
	 * Set highlight
	 * 
	 * @access public
	 * @param mixed $highlight
	 * @return NewsDataObject
	 */
	public function setHighlight($highlight) {
		$this->_highlight = $highlight;
		return $this;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return NewsDataObject
	 */
	public function setPublished($published) {
		$this->_published = $published;
		return $this;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return NewsMapper $mapper
	 */
	public function getMapper() {
		return M_Loader::getDataObjectMapper('News', 'news');
	}
	
}