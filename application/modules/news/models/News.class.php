<?php
/**
 * News
 * 
 * @package App
 * @subpackage News
 */
// Load superclass
M_Loader::loadDataObject('NewsDataObject', 'news');
class News extends NewsDataObject {

	/* -- SETTERS -- */
	
	/**
	 * Set Title
	 * 
	 * @access public
	 * @param mixed $text
	 * @return mixed
	 */
	public function setTitle($text, $locale = NULL) {
		parent::setTitle($text, $locale);
		$this->setUrl(M_Uri::getPathElementFromString($text), $locale);
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Projects
	 * 
	 * @access public
	 * @return M_ArrayIterator || NULL
	 */
	public function getProjects() {
		
		$mapper = M_Loader::getDataObjectMapper('NewsXProject', 'news');
		/* @var $mapper NewsXProjectMapper */
		
		return $mapper->getProjectsByNewsId($this->getId());
		
	}
	
	/**
	 * Get News Category
	 * 
	 * Will fetch and return the {@link NewsCategory} for this {@link News}
	 * 
	 * @access public
	 * @return NewsCategory
	 */
	public function getCategory() {
		// If no category id is set, return NULL
		if(! $this->getCategoryId()) return null;
		
		// Fetch the category, returning it
		return M_Loader
			::getDataObjectMapper('NewsCategory', 'news')
			->getOneByFieldWithRegistryCache('id', $this->getCategoryId());
	}
	
	/**
	 * Get URL, including the URL of the category
	 * 
	 * @access public
	 * @return string 
	 */
	public function getUrlIncludingCategory() {
		// Output
		$out = array();
		
		// Get the category:
		$category = $this->getCategory();
		
		// If found:
		if($category) {
			$out[] = $category->getUrl();
		}
		
		// Add the URL of the news headline:
		$out[] = $this->getUrl();
		
		// Return the URL:
		return implode('/', $out);
	}
	
	/**
	 * Get project by id
	 * 
	 * @access string
	 * @return array
	 */
	
	public function getProject() {
		M_Loader::loadDataObjectMapper('Project', 'portfolio');
		$mapper = new ProjectMapper();
		return $mapper->getById($this->getProjectId());
	}
	
	/**
	 * get media album
	 * 
	 * @return MediaAlbum
	 */
	public function getMediaAlbum() {
		// construct media album mapper
		M_Loader::loadDataObjectMapper('MediaAlbum', 'media');
		$mapper = new MediaAlbumMapper();
		
		return $mapper->addFilterPublished()->getAlbumByObject($this);
	}
	
	/**
	 * get Images
	 * 
	 * @return M_ArrayIterator
	 */
	public function getImages() {
		$album = $this->getMediaAlbum();
		if( ! $album) {
			return;
		}
		return $album->getImages();
	}	
	
	/**
	 * Get Image
	 */
	public function getImage() {
		$images = $this->getImages();
		if($images) {
			return $images->first();
		}
	}

	/**
	 * Get imageFilePath
	 *
	 * @access public
	 * @return M_Image
	 */
	public function getImageFile() {
		return new M_Image($this->getImageFilename());
	}

	/**
	 * Get pdfFilePath
	 *
	 * @access public
	 * @return M_File
	 */
	public function getPdfFile() {
		return new M_File($this->getPdfFilename());
	}
}