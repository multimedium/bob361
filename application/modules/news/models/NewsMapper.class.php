<?php
// Load superclass
M_Loader::loadDataObjectMapper('SearchableDataObject', 'search');

/**
 * NewsMapper
 * 
 * @package App
 * @subpackage News
 */
class NewsMapper extends SearchableDataObjectMapper {
	
	/* -- SETTERS -- */
	
	/**
	 * Add filter: published only
	 * 
	 * Add a filter so only published news get fetched from the database.
	 * 
	 * @access public
	 * @return NewsMapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFilterPublishedOnly() {
		// start >= today || start == 0000-00-00
		$filter1 = new M_DbQueryFilterWhere('startDate', NULL, M_DbQueryFilterWhere::IS);
		$filter1->addCondition('startDate', new M_Date(), M_DbQueryFilterWhere::LTEQ, M_DbQuery::OPERATOR_OR);
		
		// start >= today && (eind >= vandaag || eind == 0000-00-00)
		$filter2 = new M_DbQueryFilterWhere('endDate', new M_Date(), M_DbQueryFilterWhere::GTEQ);
		$filter2->addCondition('endDate', NULL, M_DbQueryFilterWhere::IS, M_DbQuery::OPERATOR_OR);
		
		// Prepare filter wrappers
		$filter = clone $filter1;
		
		// Add filters to the wrapper:
		$filter
			->resetAllConditions()
			->addSubfilter($filter1)
			->addSubfilter($filter2, M_DbQuery::OPERATOR_AND);
		
		// Add the wrapper of filters to the mapper:
		$this
			->addFilter(new M_DbQueryFilterWhere('published', 1))
			->addFilterLocaleExists()
			->addFilter($filter);
		
		return $this;
	}

	/**
	 * Add filter: category
	 * 
	 * Add a filter so only news items of a given category get fetched from 
	 * the database.
	 * 
	 * @access public
	 * @param NewsCategory $category
	 * @return NewsMapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFilterNewsCategory(NewsCategory $category) {
		return $this->addFilterNewsCategoryId($category->getId());
	}

	/**
	 * Add filter: category id
	 * 
	 * Add a filter so only news items of a given category get fetched from 
	 * the database.
	 * 
	 * @access public
	 * @param int $id
	 * @return NewsMapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFilterNewsCategoryId($id) {
		return $this->addFilter(new M_DbQueryFilterWhere('categoryId', (int) $id));
	}
	
	/**
	 * Add filter: sort by date
	 * 
	 * @access public
	 * @param string $order
	 *		The order, can be either ASC or DESC.
	 * @return NewsMapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFilterSortByDate($order = 'DESC') {
		return $this->addFilter(new M_DbQueryFilterOrder('startDate', (string) $order));
	}

	/**
	 * Add Filter: "locale exists"
	 *
	 * Will filter out items with an empty title on the currently active
	 * locale
	 *
	 * @access public
	 * @return NewsMapper
	 */
	public function addFilterLocaleExists() {
		return $this->addFilter(new M_DbQueryFilterWhere('title', '', M_DbQueryFilterWhere::NEQ));
	}

	/**
	 * Add Filter: "highlight"
	 *
	 * @access public
	 * @return NewsMapper
	 */
	public function addFilterHighlight() {
		return $this->addFilter(new M_DbQueryFilterWhere('highlight', 1));
	}
	
	/**
	 * Add filter: search
	 * 
	 * Allows for filtering for search
	 * 
	 * @access public
	 * @param string $term
	 * @return NewsMapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFilterSearch($term) {
		return $this->addFilter(new M_DbQueryFilterSearch(
			array('title', 'text'), 
			(string) $term
		));
	}
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Get by URL Suffix
	 * 
	 * Will try to fetch a unique object that matches the provided URL Suffix. 
	 * Returns an instance of {@link News}, or NULL if no matching object 
	 * could have been found.
	 * 
	 * @access public
	 * @param string $urlSuffix
	 * 		The URL Suffix to search a news item with
	 * @return News $news
	 * 		The matching object, or NULL if not found
	 */
	public function getByUrlSuffix($urlSuffix) {
		// Get the result of the SELECT statement, with all of the filters 
		// applied to it:
		$rs = $this->_getFilteredSelect(
			// ... add filter to look up by URL Suffix:
			new M_DbQueryFilterWhere('url', (string) $urlSuffix)
		)
			// Only fetch one object:
			->limit(0, 1)
			// Execute:
			->execute();
		
		// If the result set contains the record we are looking for:
		if($rs && $rs->count() == 1) {
			// Return the object:
			return $this->_createObjectFromRecord($rs->current());
		}
		
		// Return NULL if still here
		// (If still here, it means that we did not find any matching object)
		return NULL;
	}
	
	/* -- OVERWRITES -- */
	
	/**
	 * Overwriting the delete function
	 *
	 * @access public
	 * @param News $dataObject
	 * @return boolean
	 *		Returns TRUE if everything succeeded, FALSE if not
	 */
	public function delete(News $dataObject) {
		// Start database transaction
		$this->getDb()->beginSmartTransaction();

		// If the item exists
		if($dataObject->getId()) {
			// Does it have an image?
			$image = $dataObject->getImage();
			if($image->exists()) {
				// Then delete it
				$image->delete();
			}
		}

		// Delete the item itself
		if(! parent::delete($dataObject)) {
			$this->getDb()->failSmartTransaction();
			return $this->getDb()->completeSmartTransaction();
		}

		// Commit database changes
		return $this->getDb()->completeSmartTransaction();
	}

	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'news';
	}
}