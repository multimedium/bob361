<?php
// Load superclass
M_Loader::loadDataObjectMapper('SearchableDataObject', 'search');
/**
 * NewsCategoryMapper
 * 
 * @package App
 * @subpackage News
 */
class NewsCategoryMapper extends SearchableDataObjectMapper {
	
	/* -- SETTERS -- */
	
	/**
	 * Add filter: default sorting
	 * 
	 * Will sort the categories by their order index
	 * 
	 * @access public
	 * @return NewsCategoryMapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFilterDefaultSorting() {
		return $this->addFilter(new M_DbQueryFilterOrder('order', 'asc'));
	}
	
	/**
	 * Add filter: published
	 * 
	 * @access public
	 * @return NewsCategoryMapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFilterPublished() {
		return $this->addFilter(new M_DbQueryFilterWhere('published', 1));
	}

    /**
     * Add filter: published for search
     *
     * @access public
     * @return NewsCategoryMapper
     */
    public function addFilterPublishedOnly() {
        return $this->addFilterPublished();
    }

	/* -- GETTERS -- */
	
	/**
	 * Get by URL Suffix
	 * 
	 * Will try to fetch a unique object that matches the provided URL Suffix. 
	 * Returns an instance of {@link NewsCategory}, or NULL if no matching object 
	 * could have been found.
	 * 
	 * @access public
	 * @param string $urlSuffix
	 * 		The URL Suffix to search a news item with
	 * @return NewsCategory
	 * 		The matching object, or NULL if not found
	 */
	public function getByUrlSuffix($urlSuffix) {
		// Get the result of the SELECT statement, with all of the filters 
		// applied to it:
		$rs = $this->_getFilteredSelect(
			// ... add filter to look up by URL Suffix:
			new M_DbQueryFilterWhere('url', (string) $urlSuffix)
		)
			// Only fetch one object:
			->limit(0, 1)
			// Execute:
			->execute();
		
		// If the result set contains the record we are looking for:
		if($rs && $rs->count() == 1) {
			// Return the object:
			return $this->_createObjectFromRecord($rs->current());
		}
		
		// Return NULL if still here
		// (If still here, it means that we did not find any matching object)
		return NULL;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'news';
	}
}