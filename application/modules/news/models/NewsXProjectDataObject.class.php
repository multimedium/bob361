<?php
/** 
 * NewsXProjectDataObject
 * 
 * @see NewsXProjectMapper
 * @see NewsXProject
 * @package App
 * @subpackage News
 */
class NewsXProjectDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link NewsXProjectDataObject::getId()}
	 * - {@link NewsXProjectDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * projectId
	 * 
	 * This property stores the projectId. To get/set the value of the
	 * projectId, read the documentation on 
	 * 
	 * - {@link NewsXProjectDataObject::getProjectId()}
	 * - {@link NewsXProjectDataObject::setProjectId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_projectId;
	
	/**
	 * newsId
	 * 
	 * This property stores the newsId. To get/set the value of the newsId,
	 * read the documentation on 
	 * 
	 * - {@link NewsXProjectDataObject::getNewsId()}
	 * - {@link NewsXProjectDataObject::setNewsId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_newsId;
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return NewsXProjectDataObject
	 */
	public function setId($id) {
		$this->_id = $id;
		return $this;
	}
	
	/**
	 * Get projectId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getProjectId() {
		return $this->_projectId;
	}
	
	/**
	 * Set projectId
	 * 
	 * @access public
	 * @param mixed $projectId
	 * @return NewsXProjectDataObject
	 */
	public function setProjectId($projectId) {
		$this->_projectId = $projectId;
		return $this;
	}
	
	/**
	 * Get newsId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getNewsId() {
		return $this->_newsId;
	}
	
	/**
	 * Set newsId
	 * 
	 * @access public
	 * @param mixed $newsId
	 * @return NewsXProjectDataObject
	 */
	public function setNewsId($newsId) {
		$this->_newsId = $newsId;
		return $this;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return NewsXProjectMapper $mapper
	 */
	public function getMapper() {
		return M_Loader::getDataObjectMapper('NewsXProject', 'news');
	}
	
}