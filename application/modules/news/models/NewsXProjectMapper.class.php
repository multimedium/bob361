<?php
/**
 * NewsXProjectMapper
 * 
 * @package App
 * @subpackage News
 */
class NewsXProjectMapper extends M_DataObjectMapper {
	
	/**
	 * Add Filter: Project Id
	 * 
	 * @access public
	 * @param mixed $projectId
	 * @return ProjectXTeamMemberMapper
	 */
	public function addFilterProjectId($projectId) {
		return $this->addFilter(new M_DbQueryFilterWhere('projectId', (int) $projectId, M_DbQueryFilterWhere::EQ));
	}

	/**
	 * Add Filter: News Id
	 * 
	 * @access public
	 * @param mixed $newsId
	 * @return ProjectXTeamMemberMapper
	 */
	public function addFilterNewsId($newsId) {
		return $this->addFilter(new M_DbQueryFilterWhere('newsId', (int) $newsId, M_DbQueryFilterWhere::EQ));
	}
	
	/**
	 * Get Projects by News Id
	 * 
	 * @access public
	 * @param type $newsId
	 * @return M_ArrayIterator || NULL
	 */
	public function getProjectsByNewsId($newsId) {
		
		// Get Project Ids
		$projectIds = $this->getProjectIdsByNewsId($newsId);
		
		if(!$projectIds) {
			return NULL;
		}
		
		// Get all Projects
		$mapper = M_Loader::getDataObjectMapper('Project', 'portfolio');
		$projects = $mapper->getByIds($projectIds);
		
		return $projects;
		
	}

	/**
	 * Get Project Ids By News Id
	 * 
	 * @access public
	 * @param type $newsId
	 * @return array || NULL
	 */
	public function getProjectIdsByNewsId($newsId) {
		
		// New instance of this mapper
		$mapper = new self();
		
		// Fetch all NewsXProjects for this News id
		$newsXProjects = $mapper
			->addFilterNewsId($newsId)
			->getAll();
		
		if(!$newsXProjects) {
			return NULL;
		}		
		
		// Collect Project ids
		$projectIds = array();
		foreach($newsXProjects as $newsXProject) {
			array_push($projectIds, $newsXProject->getProjectId());
		}
		
		return $projectIds;
		
	}	
	
	/**
	 * Delete By News Id
	 * 
	 * @access public
	 * @param mixed $newsId
	 * @return void
	 */
	public function deleteByNewsId($newsId) {
		
		// New instance of this mapper
		$mapper = new self();
		
		// Fetch all NewsXProjects for this project id
		$newsXProjects = $mapper
			->addFilterNewsId($newsId)
			->getAll();
		
		// Delete all NewsXProjects
		foreach($newsXProjects as $newsXProject) {
			$newsXProject->delete();
		}
		
	}
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'news';
	}
	
}