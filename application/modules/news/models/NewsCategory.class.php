<?php
/**
 * NewsCategory
 *
 * @package App
 * @subpackage News
 */
// Load superclass
M_Loader::loadDataObject('NewsCategoryDataObject', 'news');
class NewsCategory extends NewsCategoryDataObject {

	/* -- PUBLIC FUNCTIONS -- */

	/**
	 * Set Title
	 *
	 * @access public
	 * @param mixed $text
	 * @return mixed
	 */
	public function setTitle($text, $locale = NULL) {
		parent::setTitle($text, $locale);
		$this->setUrl(M_Uri::getPathElementFromString($text), $locale);
	}
	
	/**
	 * Get News Count
	 * 
	 * @access public
	 * @return int
	 */
	public function getNewsCount() {
		return M_Loader::getDataObjectMapper('News', 'news')
			->addFilterPublishedOnly()
			->addFilterLocaleExists()
			->addFilterNewsCategory($this)
			->getCount();
	}
	
	/**
	 * Get News Item
	 * 
	 * @access public
	 * @return News
	 */
	public function getNewsItem() {
		return M_Loader
			::getDataObjectMapper('News', 'news')
			->addFilterPublishedOnly()
			->addFilterNewsCategory($this)
			->addFilterSortByDate()
			->addFilterLocaleExists()
			->getOne();
	}
}