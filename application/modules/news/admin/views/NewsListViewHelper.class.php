<?php
/*
 * NewsListViewHelper
 *
 * @package App
 * @subpackage News
 */
class NewsListViewHelper {
	
	/* -- PROPERTIES -- */
	
	/**
	 * News Categories
	 * 
	 * Holds the collection of {@link NewsCategory} objects
	 * 
	 * @access private
	 * @var array
	 */
	private $_newsCategories;
	
	/* -- PUBLIC FUNCTIONS -- */

	/**
	 * Set column value
	 *
	 * Manipulates the values shown in the list view. A string is
	 * expected from this method.
	 *
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param AdminListColumnDefinition $column
	 * 		The column for which to set the value
	 * @param News $dataObject
	 * 		The instance for which to set the value
	 * @param mixed $value
	 * 		The value that has been prepared by {@link AdminListView}.
	 * 		This will be the listed value, if not manipulated by this
	 * 		method.
	 * @return string|null
	 */
	public function setColumnValue(AdminListColumnDefinition $column, News $dataObject, $value) {
		switch($column->getIndex()) {
			case 1:
				$cat = $this->_getNewsCategory($value);
				return $cat ? $cat->getTitle() : '&nbsp;';
				break;

			case 2:
				if($value && $dataObject) {
					return $dataObject->getStartDate()->toString(M_Date::MEDIUM);
				} else {
					return '&nbsp;';
				}
				break;

			case 3:
				if($value && $dataObject) {
					return $dataObject->getEndDate()->toString(M_Date::MEDIUM);
				} else {
					return '&nbsp;';
				}
				break;
				
			case 4:
				if($value) {
					// Form the filename
					$file = new M_File($value);

					// If the file exists
					if($file->exists()) {
						// Return the file's basename and add a delete button.
						// Additionally, add a direct link to the file
						return '<a class="confirm delete left delete-image" href="'
								. M_Request::getLink('admin/route/news/ajax-delete-image/'
								. $dataObject->getId())
								. '" title="Delete this image?">X</a>'
								. '<a href="'. M_Request::getLinkWithoutPrefix($value)
								. '" class="fancybox">'
								. t('Image') . ' ' . M_FsHelper::getFileSizeString($file->getSize())
								. '</a>';
					} else {
						return '&nbsp;';
					}
				} else {
					return '&nbsp;';
				}
				break;

			default:
				return $value;
				break;
		}
	}
	
	/* -- PRIVATE / PROTECTED -- */
	
	/**
	 * Get News Category
	 * 
	 * @access protected
	 * @param int $id
	 *		The {@link NewsCategory} id
	 * @return NewsCategory
	 */
	protected function _getNewsCategory($id) {
		$id = (int) $id;
		if(! isset($this->_newsCategories[$id])) {
			$this->_newsCategories[$id] = M_Loader
				::getDataObjectMapper('NewsCategory', 'news')
				->getOneByFieldWithRegistryCache('id', $id);
		}
		return $this->_newsCategories[$id];
	}
}