<?php
/**
 * NewsEventListener
 * 
 * @package App
 * @subpackage News
 */
class NewsEventListener {

	/**
	 * Construct
	 * 
	 * @access public
	 * @param AdminController $controller
	 */
	public function __construct(AdminController $controller) {
		// Add an event listener for creating items
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_EDIT,
			array($this, 'onDataObjectBeforeEdit')
		);

		// Add an event listener for editing items
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_SAVE,
			array($this, 'onDataObjectSave')
		);
	}

	/**
	 * Do ... when object is about to be saved
	 * 
	 * @access public
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectBeforeEdit(AdminControllerEvent $event) {

		// Make sure it is a news
		if(! M_Helper::isInstanceOf($event->getDataObject(), 'News')) return false;
		
		// Set default values for Projects
		$this->_setProjectsDefaultValues($event);
		
	}

	/**
	 * Do ... when object is about to be saved
	 * 
	 * @access public
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectSave(AdminControllerEvent $event) {

		// Make sure it is a news
		if(! M_Helper::isInstanceOf($event->getDataObject(), 'News')) return false;
		
		// Store Projects
		$this->_storeProjects($event);

	}

	/*-- PROTECTED FUNCTIONS --*/
	
	/**
	 * Set Projects Default Values
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _setProjectsDefaultValues(AdminControllerEvent $event) {
		
		// Variables
		$news = $event->getDataObject();
		$field = $event->getForm()->getField('projects');
		
		// Get NewsXProjects
		$mapper = M_Loader::getDataObjectMapper('NewsXProject', 'news');
		/* @var $mapper NewsXProjectMapper */
		$newsXProjects = $mapper
			->addFilterNewsId($news->getId())
			->getAll();
		
		// Collect Project ids
		$projectIds = array();
		foreach ($newsXProjects as $newsXProject) {
			array_push($projectIds, $newsXProject->getProjectId());
		}
		
		// Set Default values
		$field->setDefaultValue($projectIds);
		
	}
	
	/**
	 * Store Projects
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _storeProjects(AdminControllerEvent $event) {
		
		// Get the field values for "team members"
		$news = $event->getDataObject();
		
		// Delete all NewsXProjects before creating new ones
		$mapper = M_Loader::getDataObjectMapper('NewsXProject', 'news');
		/* @var $mapper NewsXProjectMapper */
		$mapper->deleteByNewsId($news->getId());

		$form = $event->getForm();
		$values = $form->getField('projects')->deliver();
		
		// Load necessary classes
		M_Loader::loadModel('NewsXProject', 'news');
		
		// Store the Projects
		foreach($values as $value) {
			
			$newsXProject = new NewsXProject();
			$newsXProject
				->setProjectId($value)
				->setNewsId($news->getId())
				->save();
			
		}		
		
	}
	
}