<?php
/**
 * AdminNewsController
 * 
 * @package App
 * @subpackage News
 */
class AdminNewsController extends ApplicationAdminController {
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * AJAX - Delete Image
	 *
	 * This function will repond to an AJAX call, deleting the set image of a
	 * given {@link News} instance, specified by its id
	 *
	 * @access public
	 * @param int $id
	 *		The id of the {@link News} instance
	 * @return void
	 */
	public function ajaxDeleteImage($id) {
		// Fetch the news item
		$news = M_Loader
			::getDataObjectMapper('News', 'news')
			->getOneByFieldWithRegistryCache('id', $id);

		// If the news item could not be found
		/* @var $news News */
		if(! $news) $this->_ajaxError();
		
		// Fetch the file
		$file = $news->getImage();
		
		// If it does not exist
		if(! $file->exists()) $this->_ajaxError();
		
		// Delete the file
		$file->delete();

		// Empty the filename
		$news->setImageFilename('');
		
		// Save the news item
		if(! $news->save()) $this->_ajaxError();

		// If we are still here, everything went well
		$this->_ajaxSuccess();
	}
}