<div class="row row-large">
	<div class="span12 project">
		{$images = $news->getImages()}
		{$image = $news->getImage()}
		{$count = $images|@count}
		{if $count <= 1 && $image}
				<div class="image-wrapper image-wrapper-big">
					{foreach $images as $image}
						<img src="{link href="thumbnail/span12/"}/{$image->getBasename()}" alt="{$news->getTitle()}"/>
					{/foreach}
				</div>
		{elseif $count > 0}
			<div class="flexslider project-slider">
				<div class="nav">
					<a class="nav-control previous pull-left" href="#"></a>
					<a class="nav-control next pull-right" href="#"></a>
				</div>
				<ul class="slides">
				{foreach $images as $image}
					<li>
						<a href="{link href="thumbnail/full/"}{$image->getBasename()}" title="{$imageTitle}" class="fancybox" rel="news">
							<img src="{link href="thumbnail/span12"}/{$image->getBasename()}" alt="{$imageTitle}"/>
						</a>
					</li>
				{/foreach}
				</ul>
			</div>
		{else}
		{/if}
	</div>
</div>	