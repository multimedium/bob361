<div class="row row-large publication-detail">
	<div class="span12">
		<h1 class="title">{$news->getTitle()}</h1>
		{if $author}<p class="subtitle">{t|escape text='Written by'} {$news->getAuthor()}</p>{/if}
		{if $news->getMedium()}<p class="subtitle">{$news->getMedium()}</p>{/if}
		<p class="subtitle">{$news->getStartDate()->toString('yyyy')}</p>
	</div>
</div>	
	
{include file="_Slider.tpl" news=$news}	
	
<div class="row publication-detail">
	<div class="span8">
		{if $news->getPage()}
			<p class="page">{t|escape text='p.'} {$news->getPage()}</p>
		{/if}
		{if $news->getPublicationTitle()}
			<p class="page">{$news->getPublicationTitle()}</p>
		{/if}
		<p class="article">{$news->getText()}</p>
		{if $news->getWebpage()}
			<p class="website website-first">
				{t text='Project: '}
				{foreach $news->getProjects() as $project}
					<a href="{link href="projects/detail"}/{$project->getUrl()}" title="{$project->getName()|att}">{$project->getName()}</a>{if !$project@last}, {/if}
				{/foreach}
			</p>
			<p class="website">
				{t text='Link: '}
				<a href="{$news->getWebpage()|att}" title="{$news->getWebpage()|att}">{$news->getWebpage()}</a>
			</p>
		{/if}
	</div>
</div>
	
{$relatedProjects = $news->getProjects()}

{if $relatedProjects}
<div class="row">
	<div class="span12">
		<hr/>
		<h1 class="title-light">Related projects</h1>
		<ul class="project-list">
			{foreach $relatedProjects as $relatedProject}

				<li class="project">
					<a href="{link href="projects/detail/{$relatedProject->getUrl()}"}" title="{$relatedProject->getName()}">
						{* Info *}
						<div class="project-info">
							<div class="project-info-sub-title">
								{$relatedProject->getName()}, {$relatedProject->getCity()}
							</div>
						</div>
						
						{* Image*}
						<div class="project-info-image">
							{$albumRelated = $relatedProject->getMediaAlbum()}
							{if $albumRelated}
								{$imageRelated = $albumRelated->getImage()}
								{$imagePathRelated = {link href="thumbnail/span4/{$imageRelated->getBasename()}"}}
								{$imageTitleRelated = $imageRelated->getTitle()}
							{else}
								{$imagePathRelated = {link href="application/resources/images/placeholder.jpg" prefix='false'}}
								{t text='No image available' assignto='imageTitle'}
							{/if}
							<img src="{$imagePathRelated}" alt="{$imageTitleRelated}" />
						</div>
						
					</a>
				</li>
			{/foreach}
		</ul>
	</div>
</div>
{/if}