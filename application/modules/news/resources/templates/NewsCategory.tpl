{if $news && $news->count() > 0}
    {foreach $news as $newsitem}
        {$title = $newsitem->getTitle()}
        {$author = $newsitem->getAuthor()}
        {$medium = $newsitem->getMedium()}
        {$page = $newsitem->getPage()}
        {$publicationTitle = $newsitem->getPublicationTitle()}
        {$url = $newsitem->getWebpage()}
        {$category = $newsitem->getCategory()->getTitle()|lower}
        {$description = $newsitem->getText()}
        {$album = $newsitem->getMediaAlbum()}

        <div class="row publication">
            {if $category == "publications"}
                <div class="span2">
                    <h1 class="date">{$newsitem->getStartDate()->toString('yyyy')}</h1>
                </div>
                <div class="span2">
                    {if $album}
                        {$images = $album->getImages()}
                        {$count = $images|@count}
                        {if $count > 0}
                            {$image = $images->current()}
                            {$imageTitle = $image->getTitle()}
                            {$imagePath = {link href="thumbnail/a4/{$image->getBasename()}"}}
                            <img src="{$imagePath}" alt="{$newsitem->getTitle()}" title="{$newsitem->getTitle()}"/>
                        {/if}
                    {elseif $newsitem->getImageFile() && $newsitem->getImageFile()->exists()}
                        <img src="{link href="thumbnail/a4/{$newsitem->getImageFile()->getBasename()}"}" alt="{$newsitem->getTitle()}">
                    {/if}
                </div>
                <div class="span8 description">
                    {if $newsitem->getPdfFile() && $newsitem->getPdfFile()->exists()}
                    <a href="{link}/files/media/{$newsitem->getPdfFile()->getBasename()}" target="_blank" class="publication-pdf-link">
                    {/if}
                        {if $description}
                            {$description}
                        {else}
                            {if $author}{$author}.{/if} {$publicationTitle},<br>
                            {if $medium}{$medium}{/if}{if $page}, {$page}{/if}
                        {/if}
                    {if $newsitem->getPdfFile() && $newsitem->getPdfFile()->exists()}
                    </a>
                    {/if}

                </div>
            {else}
                <div class="span2">
                    {if $category != "jobs"}
                        <h1 class="date">{$newsitem->getStartDate()->toString('yyyy')}</h1>
                    {else}
                        <h1 class="date">{$newsitem@iteration}</h1>
                    {/if}
                </div>
                <div class="{if $category == "awards"}span10{else}span5{/if}">
                    <h1 class="title"><a href="{link href="news/{$category}/{$newsitem->getUrl()}" prefix='false'}" title="{$title}">{$title}</a></h1>
                    {if $author}<p class="author">{t|escape text='Written by'} {$author}</p>{/if}
                    {if $medium}<p class="publication">{$medium}</p>{/if}
                </div>
                <div class="span5 description">
                    {if $page}<p>{t|escape text='p.'} {$page}</p>{/if}
                    {if $publicationTitle}<p>{$publicationTitle}</p>{/if}
                </div>
            {/if}
        </div>
    {/foreach}
{else}
    {if $newsCategory->getTitle() == "Jobs"}
    <div class="row">
        <div class="span12">
            <p>{t text='Please send portfolio and CV to @email' email="<a href='mailto:bxl@bob361.com'>bxl@bob361.com</a>"}</p>
        </div>
    </div>

    {else}
    <div class="row">
        <div class="span12">
            <p>{t|escape text='Momenteel zijn er geen @category. Kom later zeker eens opnieuw een kijkje nemen!' category="{$newsCategory->getTitle()}"}</p>
        </div>
    </div>
    {/if}
{/if}