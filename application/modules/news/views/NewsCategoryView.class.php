<?php
/**
 * NewsCategoryView
 *
 * @package App
 * @subpackage News
 */
M_Loader::loadView('NewsView', 'news');
class NewsCategoryView extends NewsView {

	/* -- PROPERTIES -- */

	/**
	 * News
	 * 
	 * Holds the collection of {@link News} items that are to be displayed
	 * in the view
	 * 
	 * @see NewsCategoryView::setNews()
	 * @access private
	 * @var M_ArrayIterator
	 */
	private $_news;

	/* -- PUBLIC FUNCTIONS -- */

	/**
	 * Set News
	 * 
	 * Allows for setting the collection of {@link News} items that are to be
	 * displayed in the view
	 * 
	 * @access public
	 * @param M_ArrayIterator $news
	 * @return NewsCategoryView
	 *		Returns itself, for a fluent programming interface
	 */
	public function setNews(M_ArrayIterator $news) {
		$this->_news = $news;
		return $this;
	}

	/* -- PROTECTED/PRIVATE -- */

	/**
	 * Preprocessing
	 *
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Run parent preprocessing
		parent::preProcessing();
		
		// Make sure the news has been provided:
		if(! $this->_news) {
			// If not, throw an exception
			throw new M_ViewException(sprintf(
				'%s cannot render %s; please provide the News to %s::%s()',
				__CLASS__,
				__CLASS__,
				'setNews'
			));
		}

		// Assign required template variables
		$this->assign('news', $this->_news);
	}

	/**
	 * Get resource
	 *
	 * @see M_ViewHtml::getResource()
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('news', 'NewsCategory.tpl');
	}
}