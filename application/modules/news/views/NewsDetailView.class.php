<?php
/**
 * NewsDetailView
 *
 * @package App
 * @subpackage News
 */
M_Loader::loadView('NewsView', 'news');
class NewsDetailView extends NewsView {

	/* -- PROPERTIES -- */

	/**
	 * News
	 * 
	 * Holds the {@link News} item that is to be displayed in the view
	 * 
	 * @see NewsDetailView::setNews()
	 * @access private
	 * @var News
	 */
	private $_news;

	/* -- PUBLIC FUNCTIONS -- */

	/**
	 * Set News
	 * 
	 * Allows for setting the {@link News} item that is to be displayed
	 * in the view
	 * 
	 * @access public
	 * @param News $news
	 * @return NewsDetailView
	 *		Returns itself, for a fluent programming interface
	 */
	public function setNews(News $news) {
		$this->_news = $news;
		return $this;
	}
	
	/* -- PROTECTED/PRIVATE -- */

	/**
	 * Preprocessing
	 *
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Run parent preprocessing
		parent::preProcessing();
		
		// Make sure the news has been provided:
		if(! $this->_news) {
			// If not, throw an exception
			throw new M_ViewException(sprintf(
				'%s cannot render %s; please provide the News to %s::%s()',
				__CLASS__,
				__CLASS__,
				'setNews'
			));
		}
		
		// Assign required template variables
		$this->assign('news', $this->_news);
	}

	/**
	 * Get resource
	 *
	 * @see M_ViewHtml::getResource()
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('news', 'NewsDetail.tpl');
	}
}