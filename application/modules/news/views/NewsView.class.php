<?php
/**
 * NewsView
 * 
 * @package App
 * @subpackage News
 */
abstract class NewsView extends PageView {
	
	/* -- PROPERTIES -- */
	
	/**
	 * News Category
	 * 
	 * Holds the {@link NewsCategory} that is to be displayed in the view
	 * 
	 * @see NewsView::setNewsCategory()
	 * @access private
	 * @var NewsCategory
	 */
	private $_newsCategory;
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Set News Category
	 * 
	 * Allows for setting the {@link NewsCategory} object that is to be
	 * displayed in the view
	 * 
	 * @access public
	 * @param NewsCategory $category
	 * @return NewsCategoryView
	 *		Returns itself, for a fluent programming interface
	 */
	public function setNewsCategory(NewsCategory $category) {
		$this->_newsCategory = $category;
		return $this;
	}
	
	/* -- PRIVATE / PROTECTED -- */
	
	/**
	 * Preprocessing
	 * 
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Make sure the news category has been provided:
		if(! $this->_newsCategory) {
			// If not, throw an exception
			throw new M_ViewException(sprintf(
				'%s cannot render %s; please provide the NewsCategory to %s::%s()',
				__CLASS__,
				__CLASS__,
				'setNewsCategory'
			));
		}
		
		// Assign required template variables
		$this->assign('newsCategory', $this->_newsCategory);
	}
}