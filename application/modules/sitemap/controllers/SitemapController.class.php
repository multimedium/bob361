<?php
/**
 * SitemapController
 *
 * @package App
 * @subpackage Sitemap
 */
class SitemapController extends PageController {

	/* -- PUBLIC -- */

	/**
	 * Sitemap index files (to group multiple sitemap files)
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->detail();
	}

	/**
	 * Sitemap 
	 *
	 * @access public
	 * @return void
	 */
	public function detail() {
		// Construct the view
		$view = M_Loader::getView('SitemapDetailView', 'sitemap');

		
		// Add a URL for the Comapny overview page:
		$view->addUrl('company', SitemapDetailView::UPDATE_FREQ_MONTHLY);
		
		// Add a URL for the Contact overview page:
		$view->addUrl('contact', SitemapDetailView::UPDATE_FREQ_MONTHLY);
		
		// For each of the projects:
		foreach($this->_getProjects() as $project) {
			// Then, we add a URL to the view
			$view->addUrl(
				// The relative URL
				'projects/detail/' . $project->getUrl(),
				// The update frequency
				SitemapDetailView::UPDATE_FREQ_MONTHLY
			);
		}
		
		// For each of the publications:
		foreach($this->_getPublications() as $publication) {
			// Then, we add a URL to the view
			$category = strtolower($publication->getCategory()->getTitle());
			$view->addUrl(
				// The relative URL
				'news/'. $category .'/' . $publication->getUrl(),
				// The update frequency
				SitemapDetailView::UPDATE_FREQ_MONTHLY
			);
		}

		// Finally, display the view
		$view->display();
	}

	/**
	 * Get projects
	 *
	 * @access protected
	 * @return M_ArrayIterator $items
	 *		The collection of items to be included in the sitemap, each
	 *		represented by an instance of {@link Product}
	 */
	protected function _getProjects() {
		// Construct the mapper
		$mapper = M_Loader::getDataObjectMapper('Project', 'portfolio');

		/* @var $mapper ProjectMapper */
		// Add filters to the mapper, so only published items get fetched. Also
		// add other filters, that are required to only fetch items that are
		// allowed in the sitemap
		$mapper
			->addFilterIsPublished();

		// Return the collection
		return $mapper->getAll();
	}

	/**
	 * Get publications
	 *
	 * @access protected
	 * @return M_ArrayIterator $items
	 *		The collection of items to be included in the sitemap, each
	 *		represented by an instance of {@link Product}
	 */
	protected function _getPublications() {
		// Construct the mapper
		$mapper = M_Loader::getDataObjectMapper('News', 'news');

		/* @var $mapper ProjectMapper */
		// Add filters to the mapper, so only published items get fetched. Also
		// add other filters, that are required to only fetch items that are
		// allowed in the sitemap
		$mapper
			->addFilterPublishedOnly();

		// Return the collection
		return $mapper->getAll();
	}
	
}