<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	{foreach from=$locales item='locale'}
		<sitemap>
			<loc>{link prefix="false"}/{$locale}/sitemap</loc>
		</sitemap>
	{/foreach}
</sitemapindex>