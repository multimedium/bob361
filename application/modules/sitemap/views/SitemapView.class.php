<?php
/**
 * SitemapView
 *
 * @package App
 * @subpackage Sitemap
 */
class SitemapView extends M_ViewHtml {

	/* -- PRIVATE/PROTECTED -- */

	public function  __construct() {
		parent::__construct();

		// Send XML header
		M_Header::send(M_Header::CONTENT_TYPE_XML);
	}

	/**
	 * Preprocessing
	 */
	public function preProcessing() {
		$this->assign('locales', M_LocaleMessageCatalog::getInstalled());
	}

	/**
	 * Get resource
	 *
	 * @see M_ViewHtml::getResource()
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('sitemap', 'Sitemaps.tpl');
	}
}