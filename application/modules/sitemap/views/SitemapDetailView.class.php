<?php
/**
 * SitemapDetailView
 *
 * @package App
 * @subpackage Sitemap
 */
class SitemapDetailView extends M_ViewHtml {

	/* -- CONSTANTS -- */
	
	/**
	 * Update Frequency: DAILY
	 * 
	 * @access public
	 * @var string
	 */
	const UPDATE_FREQ_DAILY = 'daily';

	/**
	 * Update Frequency: WEEKLY
	 *
	 * @access public
	 * @var string
	 */
	const UPDATE_FREQ_WEEKLY = 'weekly';

	/**
	 * Update Frequency: MONTHLY
	 *
	 * @access public
	 * @var string
	 */
	const UPDATE_FREQ_MONTHLY = 'monthly';

	/* -- PROPERTIES -- */

	/**
	 * URL's
	 *
	 * @see SitemapDetailView::addUrl()
	 * @access private
	 * @var array $urls
	 */
	private $_urls = array();

	/* -- GETTERS -- */

	/**
	 * Display
	 *
	 * @access public
	 * @return void
	 */
	public function  display() {
		// Send XML header
		M_Header::send(M_Header::CONTENT_TYPE_XML);

		// And display
		parent::display();
	}

	/* -- SETTERS -- */

	/**
	 * Add URL
	 *
	 * @access public
	 * @param string $url
	 *		The RELATIVE link to the page
	 * @param string $updateFrequency
	 *		The update frequency of the URL
	 * @return SitemapDetailView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function addUrl($url, $updateFrequency) {
		$this->_urls[(string) $url] = (string) $updateFrequency;
		return $this;
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Preprocessing
	 *
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Assign presentation variables:
		$this->assign('urls', $this->_urls);
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get resource
	 *
	 * @see M_ViewHtml::getResource()
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('sitemap', 'SitemapDetail.tpl');
	}
}