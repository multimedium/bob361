<?php
/** 
 * AdminSmartFolderFilterDataObject class
 * 
 * @see AdminSmartFolderFilterMapper
 * @package App
 * @subpackage Admin
 */
class AdminSmartFolderFilterDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link AdminSmartFolderFilterDataObject::getId()}
	 * - {@link AdminSmartFolderFilterDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * folderId
	 * 
	 * This property stores the folderId. To get/set the value of the
	 * folderId, read the documentation on 
	 * 
	 * - {@link AdminSmartFolderFilterDataObject::getFolderId()}
	 * - {@link AdminSmartFolderFilterDataObject::setFolderId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_folderId;
	
	/**
	 * filterIndexInListDefinition
	 * 
	 * This property stores the filterIndexInListDefinition. To get/set the value 
	 * of the filterIndexInListDefinition, read the documentation on 
	 * 
	 * - {@link AdminSmartFolderFilterDataObject::getFilterIndexInListDefinition()}
	 * - {@link AdminSmartFolderFilterDataObject::setFilterIndexInListDefinition()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_filterIndexInListDefinition;
	
	/**
	 * comparison
	 * 
	 * This property stores the comparison. To get/set the value of the
	 * comparison, read the documentation on 
	 * 
	 * - {@link AdminSmartFolderFilterDataObject::getComparison()}
	 * - {@link AdminSmartFolderFilterDataObject::setComparison()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_comparison;
	
	/**
	 * value
	 * 
	 * This property stores the value. To get/set the value of the value,
	 * read the documentation on 
	 * 
	 * - {@link AdminSmartFolderFilterDataObject::getOperator()}
	 * - {@link AdminSmartFolderFilterDataObject::setOperator()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_value;
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return mixed
	 */
	public function setId($id) {
		$this->_id = $id;
	}
	
	/**
	 * Get folderId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getFolderId() {
		return $this->_folderId;
	}
	
	/**
	 * Set folderId
	 * 
	 * @access public
	 * @param mixed $folderId
	 * @return mixed
	 */
	public function setFolderId($folderId) {
		$this->_folderId = $folderId;
	}
	
	/**
	 * Get filterIndexInListDefinition
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getListFilterDefinitionIndex() {
		return $this->_filterIndexInListDefinition;
	}
	
	/**
	 * Set filterIndexInListDefinition
	 * 
	 * @access public
	 * @param mixed $index
	 * @return mixed
	 */
	public function setListFilterDefinitionIndex($index) {
		$this->_filterIndexInListDefinition = $index;
	}
	
	/**
	 * Get comparison
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getComparison() {
		return $this->_comparison;
	}
	
	/**
	 * Set comparison
	 * 
	 * @access public
	 * @param mixed $comparison
	 * @return mixed
	 */
	public function setComparison($comparison) {
		$this->_comparison = $comparison;
	}
	
	/**
	 * Get value
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getValue() {
		return $this->_value;
	}
	
	/**
	 * Set value
	 * 
	 * @access public
	 * @param mixed $value
	 * @return mixed
	 */
	public function setValue($value) {
		$this->_value = $value;
	}
	
}