<?php
/**
 * AdminControllerHelper provides methods to AdminControler which may be used
 * by other objects too.
 * 
 * This way we abstract code which can be re-used by e.g. ListHelpers
 */
class AdminControllerHelper {

	/* -- PROPERTIES -- */

	/**
	 * Session namespace for filter values
	 *
	 * @static
	 * @access private
	 * @var M_SessionNamespace
	 */
	private static $_sessionNamespaceFilters;

	/* -- (STATIC) GETTERS -- */

	/**
	 * Get list filters, from request variables
	 * 
	 * This method will create a collection of {@link AdminListFilterDefinition}
	 * instances, based on the variables that have been provided in the request.
	 * {@link M_Request::getVariable()} is used to check for the filter definitions.
	 * 
	 * @access private
	 * @param AdminListDefinition $definition
	 * 		The definition of the list, in order to get the definition of the
	 * 		filters that may have been applied in the request.
	 * @return M_ArrayIterator
	 *		Collection of {@link AdminListFilterDefinition} instances
	 */
	public static function getListFiltersFromRequest(AdminListDefinition $definition) {
		// Prepare return value
		$out = array();

		// Working variables:
		$i = 0;
		
		// Fetch the filters that may have been applied in the URL
		do {
			// By default, this is the End-Of-Filters
			$eof = TRUE;
			
			// Check if a filter has been selected. Filters are selected by index
			// number, from the list definition:
			$index = M_Request::getVariable('f' . (++ $i), FALSE, M_Request::TYPE_INT);

			// We get the value of the filter.
			$value = M_Request::getVariable('v' . $i, FALSE);

			// We get the operator of the filter (optional):
			$operator = M_Request::getVariable('o' . $i, NULL, M_Request::TYPE_STRING);

			// If both the index number and the value of the filter have been
			// provided in the request variables:
			if($index !== FALSE && $value !== FALSE) {
				// We add the filter to the output collection:
				if(self::_addFilter($out, $definition, $index, $value, $operator)) {
					// If the filter has been added to the output collection,
					// then we set the End-Of-Filters boolean to FALSE, meaning
					// that we'll look for more filters in the next iteration
					// of this loop:
					$eof = FALSE;
				}
			}
		}
		while(! $eof);
		
		// Return collection of filters:
		return new M_ArrayIterator($out);
	}

	/**
	 * Get list filters, from request
	 *
	 * This method will create a collection of {@link AdminListFilterDefinition}
	 * instances, based on the variables that have been provided in the request.
	 * {@link M_Request::getVariable()} is used to check for the filter definitions.
	 *
	 * @access private
	 * @param AdminListDefinition $definition
	 * 		The definition of the list, in order to get the definition of the
	 * 		filters that may have been applied in the request.
	 * @return M_ArrayIterator
	 *		Collection of {@link AdminListFilterDefinition} instances
	 */
	public static function getListFiltersFromSession(AdminListDefinition $definition) {
		// Prepare return value
		$out = array();

		// Each list definition is stored in the session with a unique ID. We prepare
		// the ID of the provided definition. Since the ACL Resource ID of the
		// definition already provides with a unique ID, we just use that ID for
		// the session as well:
		$id = $definition->getAclResourceId();

		// We get the session namespace in which the variables are stored:
		$session = self::_getSessionNamespaceFilters();

		// We check if the provided definition has already been stored in the session.
		if(! isset($session->$id)) {
			// If not stored, we return an empty collection of filters:
			return new M_ArrayIterator($out);
		}

		// Note that the stored value, in the session, should be an array:
		if(is_array($session->$id)) {
			// For each of the values that have been stored in the session, for this
			// specific definition
			foreach($session->$id as $spec) {
				// Note that each filter in the session should be an array, and
				// that it should contain values for the keys 'index', 'operator'
				// and 'value'
				$c = is_array($spec);
				$c = ($c && array_key_exists('index', $spec) && is_numeric($spec['index']) && $spec['index'] >= 0);
				$c = ($c && array_key_exists('operator', $spec));
				$c = ($c && array_key_exists('value', $spec));

				// If that's the case:
				if($c) {
					// We add the filter to the output collection:
					self::_addFilter($out, $definition, (int) $spec['index'], $spec['value'], (string) $spec['operator']);
				}
			}
		}

		// Return collection of filters:
		return new M_ArrayIterator($out);
	}

	/* -- STATIC SETTERS -- */

	/**
	 * Store list filters, in session
	 *
	 * Will store the provided definitions of filters in the session, so it is
	 * remembered for future page requests. The filters that are stored can be
	 * fetched with {@link AdminControllerHelper::getListFiltersFromSession()}
	 * later on...
	 *
	 * @access public
	 * @param AdminListDefinition $definition
	 *		The list definition, for which to store the provided collection of
	 *		filters
	 * @param M_ArrayIterator $filters
	 *		The collection of {@link AdminListFilterDefinition} instances to be
	 *		stored in the session
	 * @return void
	 */
	public static function setListFiltersInSession(AdminListDefinition $definition, M_ArrayIterator $filters) {
		// We prepare a temporary array:
		$temp = array();

		// For each of the provided filters:
		foreach($filters as $filter) {
			/* @var $filter AdminListFilterLimitDefinition */
			// We check the type of filter. The way in which each variable is used,
			// depends on the type of the filter at hand:
			switch($filter->getType()) {
				// WHERE
				case 'where':
					// Add the filter to the temporary array
					$temp[] = array(
						'index'    => $filter->getIndex(),
						'operator' => $filter->getComparisonOperator(),
						'value'    => $filter->getValue()
					);
					break;

				// ORDER
				case 'order':
					// Add the filter to the temporary array
					$temp[] = array(
						'index'    => $filter->getIndex(),
						'operator' => $filter->getValue(),
						'value'    => $filter->getOrder()
					);
					break;

				// If the filter type is "order", we'll also require a
				// comparison operator, for the filter to be complete.
				// In this case, the comparison operator is used to
				// indicate the sorted field
				case 'limit':
					// Add the filter to the temporary array
					$temp[] = array(
						'index'    => $filter->getIndex(),
						'operator' => NULL,
						'value'    => $filter->getValue()
					);
					break;
			}
		}

		// Each list definition is stored in the session with a unique ID. We prepare
		// the ID of the provided definition. Since the ACL Resource ID of the
		// definition already provides with a unique ID, we just use that ID for
		// the session as well:
		$id = $definition->getAclResourceId();

		// Store the filters:
		self::_getSessionNamespaceFilters()->$id = $temp;
	}

	/**
	 * Clear list filters, in session
	 *
	 * @see AdminControllerHelper::setListFiltersInSession()
	 * @access public
	 * @param AdminListDefinition $definition
	 *		The list definition, for which to store the provided collection of
	 *		filters
	 * @return void
	 */
	public static function clearListFiltersInSession(AdminListDefinition $definition) {
		// Each list definition is stored in the session with a unique ID. We prepare
		// the ID of the provided definition. Since the ACL Resource ID of the
		// definition already provides with a unique ID, we just use that ID for
		// the session as well:
		$id = $definition->getAclResourceId();

		// Clear the filters for this list definition:
		self::_getSessionNamespaceFilters()->$id = array();
	}

	/**
	 * Load all core classes for the Admin-module
	 *
	 * @return void
	 */
	public static function loadAdminCoreClasses() {
		// Before loading anything else, we load the AdminLoader class. We will
		// use that class to load other classes. First, we compose the complete
		// (absolute) path to the file:
		$_adminBaseDirectory = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR;

		// Then, we load it:
		require_once $_adminBaseDirectory . 'core/AdminLoader.class.php';

		// Load core classes
		AdminLoader::loadCoreClass('AdminDefinition');
		AdminLoader::loadCoreClass('AdminFormDefinition');
		AdminLoader::loadCoreClass('AdminListDefinition');
		AdminLoader::loadCoreClass('AdminListColumnDefinition');
		AdminLoader::loadCoreClass('AdminListFilterDefinition');
		AdminLoader::loadCoreClass('AdminListFilterFieldDefinition');
		AdminLoader::loadCoreClass('AdminListFilterWhereDefinition');
		AdminLoader::loadCoreClass('AdminListFilterWithValuesDefinition');
		AdminLoader::loadCoreClass('AdminListFilterOrderDefinition');
		AdminLoader::loadCoreClass('AdminListFilterLimitDefinition');
		AdminLoader::loadCoreClass('AdminApplication');
		AdminLoader::loadCoreClass('AdminModule');
		AdminLoader::loadCoreClass('AdminViewFilePreview');

		// Load forms
		AdminLoader::loadForm('AdminFormDataObject',                   'admin');

		// Load views:
		AdminLoader::loadView('AdminMyAccountView',                    'admin');
		AdminLoader::loadView('AdminFormView',                         'admin');
		AdminLoader::loadView('AdminCustomListView',                   'admin');
		AdminLoader::loadView('AdminListInstanceRowView',              'admin');
		AdminLoader::loadView('AdminListView',                         'admin');
		AdminLoader::loadView('AdminListColumnsView',                  'admin');
		AdminLoader::loadView('AdminListFiltersView',                  'admin');
		AdminLoader::loadView('AdminListFilterValuesView',             'admin');
		AdminLoader::loadView('AdminListFilterComparisonsView',        'admin');
		AdminLoader::loadView('AdminListFilterComparisonValueView',    'admin');
		AdminLoader::loadView('AdminListFilterSortingOrdersView',      'admin');
		AdminLoader::loadView('AdminListViewHelper',                   'admin');
		AdminLoader::loadView('AdminToggleAjaxView',                   'admin');
		AdminLoader::loadView('AdminSmartFolderChooseModuleView',      'admin');
		AdminLoader::loadView('AdminDeleteDataObjectAjaxResponseView', 'admin');

		// Controllers
		AdminLoader::loadController('AdminControllerEvent',             'admin');
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Add filter definition to output array
	 *
	 * @static
	 * @access protected
	 * @param array $out
	 *		A reference to the array, to which the filter should be added
	 * @param AdminListDefinition $definition
	 *		The list definition, from which to fetch information about the filter
	 * @param integer $index
	 *		The index of the filter to be added
	 * @param mixed $value
	 *		The value of the filter
	 * @param string $operator
	 *		The operator of the filter (optional)
	 * @return bool $flag
	 *		Returns TRUE on success, FALSE on failure
	 */
	protected static function _addFilter(&$out, AdminListDefinition $definition, $index, $value, $operator = NULL) {
		// We ask the list definition for a definition of the filter:
		$filter = $definition->getFilter($index);

		// If the filter has been found in the list definition:
		if($filter) {
			// We check the type of filter.
			switch($filter->getType()) {
				// If the filter type is "where", we'll also require a
				// comparison operator, for the filter to be complete:
				case 'where':
					// We check if the operator is being supported
					// by the filter:
					if($filter->isComparisonOperatorSupported($operator)) {
						// If so, we set the comparison operator:
						$filter->setComparisonOperator($operator);

						// we apply the filter value. We first
						// construct the field, so we can ask a
						// deliver()ed value
						$field = $filter->getFieldForComparisonOperator($operator);
						$field->setDefaultValue($value);
						$filter->setValue($field->deliver());

						// Add the filter to the collection:
						$out[] = $filter;

						// We return TRUE, to indicate success
						return TRUE;
					}
					break;

				// If the filter type is "order", we'll also require a
				// comparison operator, for the filter to be complete.
				// In this case, the comparison operator is used to
				// indicate the sorted field
				case 'order':
					// Check for a comparison operator
					if($operator) {
						// If so, we set the comparison operator:
						$filter->setValue($operator);

						// we apply the sorting order
						$filter->setOrder($value);

						// Add the filter to the collection:
						$out[] = $filter;

						// We return TRUE, to indicate success
						return TRUE;
					}
					break;

				// If the filter type is "order", we'll also require a
				// comparison operator, for the filter to be complete.
				// In this case, the comparison operator is used to
				// indicate the sorted field
				case 'limit':
					// we apply the limit
					$filter->setValue($value);

					// Add the filter to the collection:
					$out[] = $filter;

					// We return TRUE, to indicate success
					return TRUE;
			}
		}

		// If still here, we return FALSE to indicate failure
		return FALSE;
	}

	/**
	 * Get session namespace
	 *
	 * Will return the instance of {@link M_SessionNamespace} in which filter
	 * values are stored for the current session.
	 *
	 * @access protected
	 * @return M_SessionNamespace
	 */
	protected static function _getSessionNamespaceFilters() {
		// If not requested before:
		if(! self::$_sessionNamespaceFilters) {
			// Then, we construct the namespace now:
			// (and lock it, in order to prevent other classes from using the
			// same namespace)
			self::$_sessionNamespaceFilters = new M_SessionNamespace('MultimanageList', TRUE);
		}

		// Return the namespace:
		return self::$_sessionNamespaceFilters;
	}
}