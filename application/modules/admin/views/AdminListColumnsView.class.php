<?php
/**
 * AdminListColumnsView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminViewWithHelpers.class.php';

/**
 * AdminListColumnsView
 * 
 * Renders the default list of data objects in the admin module. Typically, this
 * view is inserted into {@link AdminListView}
 * 
 * @package App
 * @subpackage Admin
 */
class AdminListColumnsView extends AdminViewWithHelpers {
	/**
	 * List Definition
	 * 
	 * This property stores the definition of the list. The definition
	 * is represented by an instance of {@link AdminListDefinition}.
	 *
	 * @see AdminListColumnsView::setListDefinition()
	 * @access private
	 * @var AdminListDefinition
	 */
	private $_definition;
	
	/**
	 * List Filter Definitions
	 * 
	 * This property stores the definitions of the applied filters. This collection
	 * contains instances of {@link AdminListFilterDefinition}
	 *
	 * @see AdminListColumnsView::setListFilterDefinitions()
	 * @access private
	 * @var ArrayIterator
	 */
	private $_appliedFilters;
	
	/**
	 * Listed instances
	 * 
	 * This property stores the collection of listed instances. For
	 * more information, read {@link AdminListView::setDataObjects()}.
	 * 
	 * @see AdminListColumnsView::setDataObjects()
	 * @access private
	 * @var ArrayIterator
	 */
	private $_instances = array();

	/**
	 * Page jumper
	 *
	 * This property stores the {@link M_Pagejumper} that is being used to build
	 * the page navigation.
	 *
	 * @access private
	 * @var M_Pagejumper
	 */
	private $_pagejumper;

	/**
	 * Flag: 'Show Delete Button?'
	 *
	 * @see AdminListColumnsView:setShowDeleteButton()
	 * @access private
	 * @var M_Pagejumper
	 */
	private $_showDeleteButton = TRUE;

	/**
	 * Flag: 'Show Edit Button?'
	 *
	 * @see AdminListColumnsView:setShowEditButton()
	 * @access private
	 * @var M_Pagejumper
	 */
	private $_showEditButton = TRUE;

	/**
	 * Flag: 'Show Locale Buttons?'
	 *
	 * @see AdminListColumnsView:setShowLocaleButtons()
	 * @access private
	 * @var M_Pagejumper
	 */
	private $_showLocaleButtons = TRUE;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param AdminListDefinition $definition
	 * 		The list definition that is to be used to render the view
	 * @return AdminListView
	 */
	public function __construct(AdminListDefinition $definition) {
		// Set the list definition:
		$this->setListDefinition($definition);
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set list definition
	 * 
	 * @access public
	 * @param AdminListDefinition $definition
	 * @return void
	 */
	public function setListDefinition(AdminListDefinition $definition) {
		$this->_definition = $definition;
	}
	
	/**
	 * Set collection of applied filters
	 * 
	 * @access public
	 * @param ArrayIterator $filters
	 * @return void
	 */
	public function setListFilterDefinitions(ArrayIterator $filters) {
		if(M_Helper::isIteratorOfClass($filters, 'AdminListFilterDefinition')) {
			$this->_appliedFilters = $filters;
			
			// Notify helpers:
			foreach($this->getHelpers() as $helper) {
				$this->_getHelperResult($helper, 'setListFilterDefinitions', array($filters));
			}
		} else {
			throw new M_ViewException(sprintf(
				'Expecting collection of %s instances in %s',
				'AdminListFilterDefinition',
				__CLASS__
			));
		}
	}
	
	/**
	 * Set listed instances
	 * 
	 * Can be used to set the collection of instances that is included
	 * in the list view. Note that {@link AdminListView} will make sure
	 * that the instances are:
	 * 
	 * - Instances of {@link M_DataObject}
	 * - Instances of the Application Module's Data Object. For more
	 *   info, read {@link M_ApplicationModule::getDataObjectClassName()}
	 * 
	 * If any of the above-mentioned conditions are not met, an exception
	 * will be thrown!
	 * 
	 * @throws M_ViewException
	 * @access public
	 * @param ArrayIterator $instances
	 * 		The collection of {@link M_DataObject} instances that is
	 * 		to be listed in the view
	 * @return void
	 */
	public function setDataObjects(ArrayIterator $instances) {
		$this->_instances = $instances;
	}
	
	/**
	 * Set Page jumper
	 * 
	 * Will set the {@link M_Pagejumper} instance that is being used to build
	 * the page navigation.
	 * 
	 * @access public
	 * @param M_Pagejumper $pagejumper
	 * @return void
	 */
	public function setPagejumper(M_Pagejumper $pagejumper) {
		$this->_pagejumper = $pagejumper;
	}

	/**
	 * Set flag: "Show delete button?"
	 *
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE in order to show the delete button, FALSE to not
	 * @return void
	 */
	public function setShowDeleteButton($flag) {
		$this->_showDeleteButton = (bool) $flag;
	}

	/**
	 * Set flag: "Show Edit button?"
	 *
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE in order to show the edit button, FALSE to not
	 * @return void
	 */
	public function setShowEditButton($flag) {
		$this->_showEditButton = (bool) $flag;
	}

	/**
	 * Set flag: "Show locale buttons?"
	 *
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE in order to show the buttons, FALSE to not
	 * @return void
	 */
	public function setShowLocaleButtons($flag) {
		$this->_showLocaleButtons = (bool) $flag;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * get list definition
	 * 
	 * @see AdminListColumnsView::setListDefinition()
	 * @access public
	 * @return AdminListDefinition
	 */
	public function getListDefinition() {
		return $this->_definition;
	}
	
	/**
	 * get applied filters
	 * 
	 * @see AdminListColumnsView::setListFilterDefinitions()
	 * @access public
	 * @return ArrayIterator $iterator
	 * 		An iterator of {@link AdminListFilterDefinition} instances
	 */
	public function getListFilterDefinitions() {
		return $this->_appliedFilters;
	}
	
	/**
	 * Get listed instances
	 * 
	 * @see AdminListColumnsView::setDataObjects()
	 * @access public
	 * @return ArrayIterator $iterator
	 * 		An iterator of {@link M_DataObject} instances
	 */
	public function getDataObjects() {
		return $this->_instances;
	}
	
	/**
	 * Get Page jumper
	 * 
	 * @see AdminListColumnsView::setPagejumper()
	 * @access public
	 * @return M_Pagejumper $pagejumper
	 */
	public function getPagejumper() {
		return $this->_pagejumper;
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Preprocessing
	 * 
	 * Magic method, called by {@link M_ViewHtml} to do some preprocessing
	 * of the view, prior to rendering the HTML (template) resource.
	 * 
	 * @access public
	 * @return void
	 */
	protected function preProcessing() {
		// Prepare some presentation variables:
		$_tplInstances = array();
		
		// Get the columns from the list definition:
		// (we fetch a copy of the array, not the iterator)
		$columns = $this->_definition->getColumns()->getArrayCopy();
		
		// IF we are to display the locale buttons
		if($this->_showLocaleButtons) {
			// If we are dealing with localized data objects:
			$isLocalizedList = $this->_definition->getModule()->getDataObjectMapper($this->_definition->getDataObjectId())->isLocalized();
			if($isLocalizedList) {
				// Get the installed locales:
				$locales = M_LocaleMessageCatalog::getInstalled();

				// If at least 2 locales are available:
				if(count($locales) > 1) {
					// For each of the installed locales:
					foreach($locales as $locale) {
						// Create a column for the locale:
						$localeColumn = new AdminListColumnDefinition($locale, count($columns), 30);
						$localeColumn->setType('locale');

						// Add the column for the locale:
						$columns[] = $localeColumn;
					}
				}
			}
		}
		
		// We merge the extra columns from the helper with the ones
		// from the list definition. For each of the columns we receive
		// from the helper:
		foreach($this->_getColumnsFromHelper() as $column) {
			// Insert the column at the requested position:
			$columns = M_Array::insertValueAtPosition($columns, $column, $column->getIndex());
		}
		
		// For each of the columns:
		foreach($columns as $i => $column) {
			// Note that we override the column's index number, since view
			// helpers may have added columns in between...
			$column->setIndex($i);
			
			// Let helpers set additional column properties
			$this->_getHelpersMergedResult('setColumnProperties', $column);
		}
		
		// If a collection of listed items have been provided
		if($this->_instances) {
			// For each of the listed instances:
			foreach($this->_instances as $i => $instance) {
				
				// We create a temporary array, in which we store the
				// values of all columns for the current instance:
				$_tmpColumns = array();
				
				// For each of the columns:
				foreach($columns as $i => $column) {
					// Get the value of the column
					$value = $this->_getColumnValue($column, $instance);
					
					// Ask the helper to set the value of this column, 
					// for the current instance:
					$temp = $this->_getColumnValueFromHelper(
						$column, 
						$instance, 
						$value
					);
					
					// We only accept the helper's feedback, if we get
					// a return value from it:
					if($temp) {
						$value = $temp;
					}
					
					// Add the value to the temporary column values:
					$_tmpColumns[$i] = $value;
				}
				
				// We add the instance to the presentation variables:
				$_tplInstances[] = array(
					'object'  => $instance,
					'columns' => $_tmpColumns,
					'options' => $this->_getRowOptionsFromHelper($instance)
				);
			}
		}
		
		// We assign the columns + instances to the view's template
		$this->assign('columns',         $columns);
		$this->assign('instances',       $_tplInstances);
		$this->assign('pagejumper',      $this->_pagejumper);
		
		// We assign the module + definition to the template
		$this->assign('definition',      $this->_definition);
		$this->assign('filters',         $this->_appliedFilters);
		$this->assign('module',          $this->_definition->getModule());
		
		// We create a URL, with the applied filters, for the "Create new item"
		// button (so filter values are copied to the form).
		$uri = new M_Uri(M_Request::getLink('admin/edit/' . $this->_definition->getModule()->getId() . '/0/' . $this->_definition->getDataObjectId()));
		
		// For each of the applied filters:
		foreach($this->_appliedFilters as $filter) {
			// Does the filter carry a field name?
			if(M_Helper::isInstanceOf($filter, 'AdminListFilterFieldDefinition')) {
				// If so, we add the field value to the URL:
				$uri->setQueryVariable($filter->getField(), $filter->getValue());
			}
		}
		
		// Assign the URL:
		$this->assign('hrefNewItemInList', $uri->getUri());
	}
	
	/**
	 * Get row options, for a given data object
	 * 
	 * @uses AdminListColumnsView::_getRowOptionsFromHelperGetter()
	 * @access private
	 * @param M_DataObject $dataObject
	 * 		The data object for which to fetch options
	 * @return string
	 */
	protected function _getRowOptionsFromHelper(M_DataObject $dataObject) {
		// We ask the helper to set the row options for the data object:
		$options = $this->_getRowOptionsFromHelperGetter('setRowOptions', $dataObject);
		
		// Has the helper provided us with options?
		if($options) {
			// Then return these options:
			return $options;
		}
		// If the helper did not provide with any options:
		else {
			// If the helper has explicitely emptied the options:
			if($options === FALSE) {
				// Return empty array
				return array();
			}
			
			// We provide with the default set of options:
			$default = array();

			// Add DELETE button by default?
			if($this->_showDeleteButton) {
				$default[] = 
					'<a href="'. M_Request::getLink('admin/delete/' . $this->_definition->getModule()->getId() . '/' . $dataObject->getId() . '/' . $this->_definition->getDataObjectId()) .'" ' .
						'title="'.t('Delete this item?').'" ' .
						'class="confirm delete">' .
							t('Delete this item') .
					'</a>';
			}

			// Add EDIT button by default?
			if($this->_showEditButton) {
				$default[] = 
					'<a href="'. M_Request::getLink('admin/edit/' . $this->_definition->getModule()->getId() . '/' . $dataObject->getId() . '/' . $this->_definition->getDataObjectId()) .'" ' .
						'title="'.t('Edit this item?').'" ' .
						'class="edit">' .
							t('Edit this item') .
					'</a>';
			}
			
			// We check if the helper ADDS options:
			$options = $this->_getRowOptionsFromHelperGetter('addRowOptions', $dataObject);
			
			// If added options:
			if($options) {
				// return merged options:
				return array_merge($default, $options);
			}
			// If no options have been added:
			else {
				// Return default options:
				return $default;
			}
		}
	}
	
	/**
	 * Get row options, for a given data object
	 * 
	 * Is used by {@link AdminListColumnsView::_getRowOptionsFromHelper()} to
	 * get row options, from:
	 * 
	 * - ViewHelper::setRowOptions(), or
	 * - ViewHelper::addRowOptions()
	 * 
	 * @access protected
	 * @param string $getter
	 * 		The getter method, to fetch options
	 * @param M_DataObject $dataObject
	 * 		The data object for which to fetch options
	 * @return array|null
	 */
	protected function _getRowOptionsFromHelperGetter($getter, $dataObject) {
		// We ask the helper for options:
		$options = $this->_getHelpersMergedResult($getter, $dataObject);
		
		// Has the helper provided us with options?
		if($options) {
			// We make sure that the helper provided us with an iterator (array 
			// or iterator interface):
			if(M_Helper::isIterator($options)) {
				// Return value:
				$out = array();
				
				// For each of the elements in the iterator:
				foreach($options as $option) {
					if($option) {
						$out[] = (string) $option;
					}
				}
				
				// Return the final collection of options:
				if(count($out) > 0) {
					return $out;
				}
			}
			// If the return value of the helper is not an iterator:
			else {
				throw new M_ViewException(sprintf(
					'%s expects an Array or Iterator from Helper::%s()',
					__CLASS__,
					$getter
				));
			}
		}
		
		// Has FALSE been returned (explicitely emptied)?
		if($options === FALSE) {
			return FALSE;
		}
		
		// Return NULL, if still here
		return NULL;
	}
	
	/**
	 * Get column value for a given data object
	 * 
	 * @uses AdminViewWithHelpers::_getColumnValueLongText()
	 * @uses AdminViewWithHelpers::_getColumnValueToggle()
	 * @access private
	 * @return string
	 */
	protected function _getColumnValue(AdminListColumnDefinition $column, M_DataObject $object) {
		// Get the field name of the column:
		// (this field name is used to fetch the value from
		// the current instance, which is an M_DataObject)
		$field = $column->getField();
		
		// Get the value from the instance, if a field name 
		// is available:
		if($field) {
			$value = $object->$field;
		}
		// If the column does not carry a field name, we 
		// default the value to an empty string
		else {
			$value = '';
		}
		
		// Depending on the type of the column, we render the column value in a
		// different way:
		switch(strtolower($column->getType())) {
			// Toggle state button
			case 'toggle':
				return $this->_getColumnValueToggle($column, $object, $value);
			
			// Currency
			case 'currency':
				return $this->_getColumnValueCurrency($column, $object, $value);

			// Number
			case 'number':
				return $this->_getColumnValueNumber($column, $object, $value);
			
			// Show long text
			case 'longtext':
				return $this->_getColumnValueLongText($column, $object, $value);
			
			// Show long text
			case 'locale':
				return $this->_getColumnValueLocale($column, $object, $column->getTitle());

			// Show edit text: add a link to edit the data-object
			case 'edit':
				return $this->_getColumnValueEdit($column, $object, $value);
			
			// Other:
			default:
				return $value;
		}
	}
	
	/**
	 * Get locale value in a column
	 * 
	 * @access private
	 * @return string
	 */
	protected function _getColumnValueLocale(AdminListColumnDefinition $column, M_DataObject $object, $locale) {
		// Get the template, to render the toggle button
		$rs = $this->_getResourceColumnLocale();
		
		// Assign the list definition to the template:
		$rs->assign('definition', $this->_definition);
		
		// Assign the data object to the template:
		$rs->assign('dataObject', $object);
		
		// Assign the column to the template:
		$rs->assign('column', $column);
		
		// Assign flag to the template: is locale available?
		$rs->assign('isAvailable', in_array($locale, $object->getLocalesOfLocalizedTexts()->getArrayCopy()));
		
		// Assign the locale to the template:
		$rs->assign('locale', $locale);
		
		// Render the template, and return the HTML Source Code:
		return $rs->fetch();
	}
	
	/**
	 * Get long text in a column
	 * 
	 * @access private
	 * @return string
	 */
	protected function _getColumnValueLongText(AdminListColumnDefinition $column, M_DataObject $object, $value) {
		// Get the template, to render the toggle button
		$rs = $this->_getResourceColumnLongText();
		
		// Assign the list definition to the template:
		$rs->assign('definition', $this->_definition);
		
		// Assign the data object to the template:
		$rs->assign('dataObject', $object);
		
		// Assign the column to the template:
		$rs->assign('column', $column);
		
		// Assign the value to the template:
		$rs->assign('text', $value);
		
		// Render the template, and return the HTML Source Code:
		return $rs->fetch();
	}
	
	/**
	 * Get toggle button in a given column, for a given data object
	 * 
	 * @access private
	 * @return string
	 */
	protected function _getColumnValueToggle(AdminListColumnDefinition $column, M_DataObject $object, $value) {
		// Get the template, to render the toggle button
		$rs = $this->_getResourceColumnToggle();
		
		// Assign the list definition to the template:
		$rs->assign('definition', $this->_definition);
		
		// Assign the data object to the template:
		$rs->assign('dataObject', $object);
		
		// Assign the column to the template:
		$rs->assign('column', $column);
		
		// Assign current toggle state 
		$rs->assign('toggleState', $value ? 1 : 0);
		
		// Render the template, and return the HTML Source Code:
		return $rs->fetch();
	}
	
	
	/**
	 * Get currency in a column
	 * 
	 * @access private
	 * @return string
	 */
	protected function _getColumnValueCurrency(AdminListColumnDefinition $column, M_DataObject $object, $value) {
		// Get the template, to render the toggle button
		$rs = $this->_getResourceColumnCurrency();
		
		// Assign the list definition to the template:
		$rs->assign('definition', $this->_definition);
		
		// Assign the data object to the template:
		$rs->assign('dataObject', $object);
		
		// Assign the column to the template:
		$rs->assign('column', $column);
		
		// Assign number 
		$rs->assign('number', $value);
		
		// Render the template, and return the HTML Source Code:
		return $rs->fetch();
	}

	/**
	 * Get number in a column
	 *
	 * @param AdminListColumnDefinition $column
	 * @param M_DataObject $object
	 * @param mixed $value
	 * @access private
	 * @return string
	 */
	protected function _getColumnValueEdit(AdminListColumnDefinition $column, M_DataObject $object, $value) {
		return '<a href="'. M_Request::getLink('admin/edit/' . $this->_definition->getModule()->getId() . '/' . $object->getId() . '/' . $this->_definition->getDataObjectId()) .'" ' .
					'title="'.t('Edit this item?').'">' .
						$value.
				'</a>';
	}
	
	/**
	 * Get number in a column
	 * 
	 * @param AdminListColumnDefinition $column
	 * @param M_DataObject $object
	 * @param mixed $value
	 * @access private
	 * @return string
	 */
	protected function _getColumnValueNumber(AdminListColumnDefinition $column, M_DataObject $object, $value) {
		return new M_Number($value);
	}

	/**
	 * Ask the helper for a column value
	 * 
	 * The helper will receive the result of {@link AdminListColumnsView::_getColumnValue()}
	 * as the current value.
	 * 
	 * @uses AdminViewWithHelpers::_getHelpersMergedResult()
	 * @access private
	 * @return string
	 */
	protected function _getColumnValueFromHelper(AdminListColumnDefinition $column, M_DataObject $object, $value) {
		// Ask the helper for the value:
		$rs = $this->_getHelpersDecoratedResult('setColumnValue', 2, $column, $object, (string) $value);
		
		// If the helper has provided us with a value:
		if($rs) {
			// We make sure that the result is a string:
			// (or any other plain and "displayable" value)
			if(! is_string($rs)) {
				throw new M_ViewException(sprintf(
					'%s cannot use the result of type %s from Helper::%s() for display',
					__CLASS__,
					gettype($rs),
					'setColumnValue'
				));
			}
		}
		
		// return value
		return $rs;
	}
	
	/**
	 * Ask the helper for extra columns
	 * 
	 * @uses AdminViewWithHelpers::_getHelpersMergedResult()
	 * @access private
	 * @return array
	 */
	protected function _getColumnsFromHelper() {
		// The return value:
		$out = array();
		
		// We ask the helper to add columns to the list view:
		$columns = $this->_getHelpersMergedResult('addColumns');
		
		// Has the helper provided us with extra columns?
		if($columns) {
			// We make sure that the helper provided us with an 
			// iterator (array or iterator interface):
			if(M_Helper::isIterator($columns)) {
				// If that's the case, we loop the columns:
				foreach($columns as $column) {
					// We also make sure that each of the columns has 
					// been provided in the form of an AdminListColumnDefinition
					// instance:
					if(is_object($column) && $column instanceof AdminListColumnDefinition) {
						// Add the column to the output:
						$out[$column->getIndex()] = $column;
					}
					// If the column is not being described by an instance
					// of AdminListColumnDefinition:
					else {
						throw new M_ViewException(sprintf(
							'%s expects a collection of AdminListColumnDefinition objects from Helper::%s()',
							__CLASS__,
							'addColumns'
						));
					}
				}
			}
			// If the return value of the helper is not an iterator:
			else {
				throw new M_ViewException(sprintf(
					'%s expects an Array or Iterator from Helper::%s()',
					__CLASS__,
					'addColumns'
				));
			}
		}
		
		// return the collection of added columns:
		return $out;
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminListColumns.tpl');
	}
	
	/**
	 * Get template, for toggle button
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the toggle button
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function _getResourceColumnToggle() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminListColumnToggle.tpl');
	}
	
	/**
	 * Get template, for long text
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render a long text
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function _getResourceColumnLongText() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminListColumnLongText.tpl');
	}
	
	/**
	 * Get template, for currency
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render a long text
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function _getResourceColumnCurrency() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminListColumnCurrency.tpl');
	}
	
	/**
	 * Get template, for locale button
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the locale button of an instance
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function _getResourceColumnLocale() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminListColumnLocale.tpl');
	}
}