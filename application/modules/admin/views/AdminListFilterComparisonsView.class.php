<?php
/**
 * AdminListFilterComparisonsView
 * 
 * Renders the menu that allows the user to pick a filter comparison operator 
 * from a menu
 * 
 * @package App
 * @subpackage Admin
 */
class AdminListFilterComparisonsView extends M_ViewHtml {
	/**
	 * Set filter definition
	 * 
	 * Can be used to set the filter definition.
	 * 
	 * @access public
	 * @param AdminListFilterDefinition $definition
	 * 		The definition of the filter for which to render a menu
	 * @return void
	 */
	public function setFilterDefinition(AdminListFilterDefinition $definition) {
		$this->assign('definition', $definition);
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is being used by 
	 * the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminListFilterComparisons.tpl');
	}
}