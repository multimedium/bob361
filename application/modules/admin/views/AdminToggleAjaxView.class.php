<?php
/**
 * AdminToggleAjaxView
 * 
 * AdminToggleAjaxView, a subclass of {@link AdminPageView}, renders the
 * AJAX Response that informs about the result of toggling an attribute of
 * a data object. For more info, read {@link AdminController::toggleAjax()}
 * 
 * @package App
 * @subpackage Admin
 */
class AdminToggleAjaxView extends M_ViewHtml {
	/**
	 * Set affected data object
	 * 
	 * @access public
	 * @param M_DataObject $object
	 * @return void
	 */
	public function setDataObject(M_DataObject $object) {
		$this->assign('object', $object);
	}
	
	/**
	 * Set new (toggled) value
	 * 
	 * @access public
	 * @param integer $toggled
	 * @return void
	 */
	public function setNewToggledValue($toggled) {
		$this->assign('value', (int) $toggled);
	}
	
	/**
	 * Set result of toggle operation
	 * 
	 * Can be used to set the result of the toggle operation (SUCCESS/FAILURE).
	 * This method expects a boolean, to describe the result.
	 * 
	 * @access public
	 * @param bool $flag
	 * 		The result of the toggle operation
	 * @return void
	 */
	public function setResult($flag) {
		$this->assign('flag', (bool) $flag);
	}
	
	/**
	 * Display the view
	 * 
	 * @see M_ViewHtml::display()
	 * @access public
	 * @return void
	 */
	public function display() {
		M_Header::send(M_Header::CONTENT_TYPE_XML);
		parent::display();
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Preprocessing
	 * 
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function _preProcessing() {
		if(! $this->getVariable('flag')) {
			throw new M_ViewException('Cannot render Toggle Ajax Response; Missing new (toggled) value');
		}
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminToggleAjax.tpl');
	}
}