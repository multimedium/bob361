<?php
/**
 * AdminPageIframeView
 *
 * @package App
 * @subpackage Admin
 */

/**
 * AdminPageView
 *
 * AdminPageView, a subclass of {@link AdminViewWithHelpers}, renders the view of
 * the administration page (Header, Source Pane, etc.).
 *
 * @package App
 * @subpackage Admin
 */
abstract class AdminPageIframeView extends AdminViewWithHelpers {

	/**
	 * Administrator
	 *
	 * This property stores the {@link Administrator} account for which the view
	 * is being rendered
	 *
	 * @access protected
	 * @var Administrator
	 */
	protected $_administrator = NULL;

	/**
	 * Render HTML
	 *
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return string
	 */
	protected function _render() {
		// Construct the view resource (template)
		$rs = new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminPageIframe.tpl');

		// Assign administrator:
		$rs->assign('administrator', $this->_getAdministrator());

		// Render the HTML of this view, and assign it to the template:
		$rs->assign('content', parent::_render());

		// Assign messages from the messenger:
		$messenger = M_Messenger::getInstance();
		$rs->assign('messages', $messenger->getMessages());
		$messenger->clearMessages();

		// Assign javascript and css files
		$js = M_ViewJavascript::getInstance();
		$css = M_ViewCss::getInstance();
		$rs->assign('viewJavascript', $js->toString());
		$rs->assign('viewCss', $css->toString());

		// return the rendered HTML from the template:
		return $rs->fetch();
	}

	/**
	 * Get administrator
	 *
	 * @access protected
	 * @return Administrator
	 */
	protected function _getAdministrator() {
		// If not requested before:
		if(! $this->_administrator) {
			// Load the Administrator Mapper, which we'll use to look up the Administrator account
			M_Loader::loadDataObjectMapper('Administrator', 'administrator');

			// Get the M_Auth Singleton
			$auth = M_Auth::getInstance();

			// Ask the M_Auth Singleton for the administrator:
			$mapper = new AdministratorMapper();
			$this->_administrator = $mapper->getById($auth->getIdentity());
		}

		// Return the administrator
		return $this->_administrator;
	}
}