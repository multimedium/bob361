<?php
/**
 * AdminPageViewWithHelper
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminPageView.class.php';

/**
 * AdminListView
 * 
 * @package App
 * @subpackage Admin
 */
class AdminListView extends AdminPageView {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Allow new items?
	 * 
	 * @see AdminListView::setAllowNewItems()
	 * @access private
	 * @var bool
	 */
	private $_allowNewItems = FALSE;
	
	/* -- SETTERS -- */
	
	/**
	 * Set List Definition
	 * 
	 * @access public
	 * @param AdminListDefinition $definition
	 * @return void
	 */
	public function setListDefinition(AdminListDefinition $definition) {
		$this->assign('definition', $definition);
	}
	
	/**
	 * Set List Filter Definitions
	 * 
	 * Sets the collection of {@link AdminListFilterDefinition} instances that
	 * has been applied to the list.
	 * 
	 * @access public
	 * @param ArrayIterator $filters
	 * 		The collection of {@link AdminListFilterDefinition} instances
	 * @return void
	 */
	public function setListFilterDefinitions(ArrayIterator $filters) {
		$this->assign('appliedFilters', $filters->getArrayCopy());
		
		// Notify helpers:
		foreach($this->getHelpers() as $helper) {
			$this->_getHelperResult($helper, 'setListFilterDefinitions', array($filters));
		}
	}
	
	/**
	 * Set Filters' view
	 * 
	 * @access public
	 * @param M_ViewHtml $view
	 * @return void
	 */
	public function setFiltersView(M_ViewHtml $view) {
		$this->assign('filtersView', $view);
	}
	
	/**
	 * Set Listed instances' view
	 * 
	 * @access public
	 * @param M_ViewHtml $view
	 * @return void
	 */
	public function setListView(M_ViewHtml $view) {
		$this->assign('listView', $view);
	}
	
	/**
	 * Set flag: allow new items to be created from the view?
	 * 
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE to allow new items to be created, FALSE to not allow
	 * @return void
	 */
	public function setAllowNewItems($flag) {
		$this->_allowNewItems = (bool) $flag;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get List Definition
	 * 
	 * @see AdminListView::setListDefinition()
	 * @access public
	 * @return AdminListDefinition
	 */
	public function getListDefinition() {
		return $this->getVariable('definition', NULL);
	}
	
	/**
	 * Get List Filter Definitions
	 * 
	 * @see AdminListView::setListFilterDefinitions()
	 * @access public
	 * @return ArrayIterator $filters
	 * 		The collection of {@link AdminListFilterDefinition} instances that has
	 * 		been applied to the list
	 */
	public function getListFilterDefinitions() {
		return new M_ArrayIterator($this->getVariable('appliedFilters', array()));
	}
	
	/**
	 * Set Filters' view
	 * 
	 * @access public
	 * @return M_ViewHtml $view
	 */
	public function getFiltersView() {
		return $this->getVariable('filtersView', NULL);
	}
	
	/**
	 * Set Listed instances' view
	 * 
	 * @access public
	 * @param M_ViewHtml $view
	 * @return void
	 */
	public function getListView() {
		return $this->getVariable('listView', NULL);
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Preprocessing
	 * 
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Check if all required elements are present:
		if(! $this->getListDefinition() || ! $this->getListFilterDefinitions() || ! $this->getFiltersView() || ! $this->getListView()) {
			// If not, throw exception:
			throw new M_ViewException(sprintf(
				'Cannot render view %s; Missing "List Definition", "Collection of Applied Filters", "Filters\' view", or "Listed Instances\' view"',
				__CLASS__
			));
		}
		
		
		
		// Assign the list options:
		$this->assign('listOptions', $this->_getListOptionsFromHelper());
	}
	
	/**
	 * Get list options
	 * 
	 * @access private
	 * @return ArrayIterator $iterator
	 * 		A collection of {@link M_Menu} instances
	 */
	protected function _getListOptionsFromHelper() {
		// We ask the helper to set the options for the list:
		$options = $this->_getListOptionsFromHelperGetter('setOptions');
		
		// Has the helper provided us with options?
		if($options) {
			// Then return these options:
			return $options;
		}
		// If the helper did not provide with any options:
		else {
			// If the helper has explicitely emptied the options:
			if($options === FALSE) {
				// Return empty array
				return array();
			}
			
			// We provide with the default set of options:
			$default = $this->_getDefaultListOptions();
			
			// We check if the helper ADDS options:
			$options = $this->_getListOptionsFromHelperGetter('addOptions');
			
			// If added options:
			if($options) {
				// return merged options:
				return array_merge($default, $options);
			}
			// If no options have been added:
			else {
				// Return default options:
				return $default;
			}
		}
	}
	
	/**
	 * Get list options, from a selected getter method in helper objects
	 * 
	 * Is used by {@link AdminListView::_getListOptionsFromHelper()} to get 
	 * options, from:
	 * 
	 * - ViewHelper::setOptions(), or
	 * - ViewHelper::addOptions()
	 * 
	 * @access protected
	 * @param string $getter
	 * 		The getter method, to fetch options from
	 * @return array|null
	 */
	protected function _getListOptionsFromHelperGetter($getter) {
		// We ask the helper for options:
		$options = $this->_getHelpersMergedResult($getter, $this->getListDefinition(), $this->getListFilterDefinitions());
		
		// Has the helper provided us with options?
		if($options) {
			// We make sure that the helper provided us with an iterator (array 
			// or iterator interface):
			if(M_Helper::isIterator($options)) {
				// Return value:
				$out = array();
				
				// For each of the elements in the iterator:
				foreach($options as $option) {
					if($option) {
						$out[] = (string) $option;
					}
				}
				
				// Return the final collection of options:
				if(count($out) > 0) {
					return $out;
				}
			}
			// If the return value of the helper is not an iterator:
			else {
				throw new M_ViewException(sprintf(
					'%s expects an Array or Iterator from Helper::%s()',
					__CLASS__,
					$getter
				));
			}
		}
		
		// Has FALSE been returned (explicitely emptied)?
		if($options === FALSE) {
			return FALSE;
		}
		
		// Return NULL, if still here
		return NULL;
	}
	
	/**
	 * Get default list options
	 * 
	 * @access private
	 * @return ArrayIterator $iterator
	 * 		A collection of {@link M_Menu} instances
	 */
	protected function _getDefaultListOptions() {
		// If new items are not allowed to be created from the view:
		if(! $this->_allowNewItems) {
			// Return empty array of options
			return array();
		}
		
		// Get the Base URI for NEW instances in the list:
		$uri = new M_Uri(
			M_Request::getLink(
				'admin/edit/' . $this->getListDefinition()->getModule()->getId() . '/0/' . $this->getListDefinition()->getDataObjectId()
			)
		);
		
		// Based on the filters that have been applied to the list, we compose
		// an URL that maintains the filter values in the form. For each of the
		// applied filters:
		foreach($this->getVariable('appliedFilters') as $filter) {
			// If the filter is applied to a field:
			if($filter instanceof AdminListFilterFieldDefinition) {
				// We get the field, and the filter's value, and we add it to the
				// Base URI:
				$uri->setQueryVariable($filter->getField(), $filter->getValue());
			}
		}
		
		// Get the template, to render the default options
		$rs = $this->_getResourceDefaultListOptions();
		
		// Assign the list definition to the template:
		$rs->assign('definition', $this->getListDefinition());
		
		// Assign the URI to the template:
		$rs->assign('uriNew', $uri->getUri());
		
		// Render the template, and return the HTML Source Code:
		return array($rs->fetch());
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminList.tpl');
	}
	
	/**
	 * Get template, for default list options
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the default list options
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function _getResourceDefaultListOptions() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminListDefaultOptions.tpl');
	}
}