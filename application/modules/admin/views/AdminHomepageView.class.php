<?php
/**
 * AdminHomepageView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminPageView.class.php';

/**
 * AdminHomepageView
 * 
 * AdminPageView, a subclass of {@link M_ViewHtml}, renders the view of the
 * start page in the administration.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminHomepageView extends AdminPageView {
	/**
	 * Preprocessing
	 * 
	 * @access protected
	 * @see M_ViewHtml::_render
	 * @return void
	 */
	protected function preProcessing() {
		$this->assign('administrator', $this->_getAdministrator());
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminHomepage.tpl');
	}
}