<?php
/**
 * AdminModuleReleaseNotesView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminPageView.class.php';

/**
 * AdminModuleReleaseNotesView
 * 
 * @package App
 * @subpackage Admin
 */
class AdminModuleReleaseNotesView extends AdminPageView {
	/**
	 * Set module
	 * 
	 * @access public
	 * @param AdminModule $modules
	 * 		The module of which to show the release notes
	 * @return void
	 */
	public function setModule(AdminModule $module) {
		$this->assign('module', $module);
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminModuleReleaseNotes.tpl');
	}
}