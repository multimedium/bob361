<?php
/**
 * AdminModuleDataSelectorView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminViewWithHelper.class.php';

/**
 * AdminModuleDataSelectorView
 * 
 * AdminModuleDataSelectorView, a subclass of {@link AdminViewWithHelper}, renders 
 * the view of a Module Data Selector. Such a Module Data Selector is an input
 * control (field) that enables the user to select data that is contained by a 
 * module.
 * 
 * A good example of such a Module Data Selector would be a selector that allows
 * the user to pick an album from the Media Module.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminModuleDataSelectorView extends AdminViewWithHelper {
	// public function setSelectedValue($value)
	// public function setNewCollection()
	// public function setValueOfItemInDefaultView($item)
	// public function setLabelOfItemInDefaultView($item)
	// public function setViewHtmlResource()
}