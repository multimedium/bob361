<?php
/**
 * AdminFormMyAccount
 * 
 * @package App
 * @subpackage Admin
 */
class AdminFormMyAccount extends M_FormDataObject {
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param Administrator $administrator
	 * 		The administrator of which the data should be edited by the form
	 * @return void
	 */
	public function __construct(Administrator $administrator) {
		// Construct the form:
		parent::__construct('admin-account');
		
		// Construct the directory where administrator picture are stored
		$dir = new M_Directory('files/administrator');
		
		// If the directory does not yet exist, we create it now:
		if(! $dir->exists()) {
			$dir->make();
			
			// Do not forget about the permissions:
			$dir->setPermissions(M_FsPermissions::constructWithOctal(0777));
		}
		
		// Set the administrator:
		$this->setAdministrator($administrator);
		
		// Create a field for the administrator's title
		$field = new M_FieldSelect('title');
		$field->setTitle(t('Title'));
		$field->setDefaultValue($administrator->getTitle());
		$field->setItem('', '-');
		$field->setItem('Mr.', t('Mr.'));
		$field->setItem('Ms.', t('Ms.'));
		$field->setItem('Miss', t('Miss'));
		$field->setItem('Master', t('Master'));
		$field->setItem('Architect', t('Architect'));
		$field->setItem('Dr.', t('Doctor'));
		$field->setItem('Ir.', t('Engineer'));
		$field->setItem('Notary', t('Notary'));
		$field->setItem('Prof.', t('Professor'));
		$this->addField($field);
		
		// Create a field for the administrator's first name
		$field = new M_FieldText('firstName');
		$field->setTitle(t('First name'));
		$field->setMaxlength(255);
		$field->setMandatory(TRUE);
		$field->setDefaultValue($administrator->getFirstname());
		$this->addField($field);
		
		// Create a field for the administrator's surname(s)
		$field = new M_FieldText('surNames');
		$field->setTitle(t('Last name'));
		$field->setMaxlength(255);
		$field->setMandatory(TRUE);
		$field->setDefaultValue($administrator->getSurnames());
		$this->addField($field);
		
		// Create a field for the administrator's organisation
		$field = new M_FieldText('organisation');
		$field->setTitle(t('Organisation'));
		$field->setMaxlength(255);
		$field->setDefaultValue($administrator->getOrganisation());
		$this->addField($field);
		
		// Create a field for the administrator's picture
		$field = new M_FieldUpload('pictureFilename');
		$field->setTitle(t('Picture'));
		$field->setDefaultValue($administrator->getPictureFilename());
		$field->setDirectory($dir->getPath());
		$field->setFilename(md5(time() . '-' . $administrator->getId() . '-' . rand(0, 2000000)));
		$field->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
		$field->setMaxFileSize(512000); // 500K
		$this->addField($field);
		
		// Create a field for the administrator's address: street name
		$field = new M_FieldText('streetAddress');
		$field->setTitle(t('Street name (+house number)'));
		$field->setDefaultValue($administrator->getStreetAddress());
		$field->setMaxlength(255);
		$this->addField($field);
		
		// Create a field for the administrator's address: postal code
		$field = new M_FieldText('zip');
		$field->setTitle(t('Postal Code'));
		$field->setDefaultValue($administrator->getZip());
		$field->setMaxlength(16);
		$this->addField($field);
		
		// Create a field for the administrator's address: city
		$field = new M_FieldText('city');
		$field->setTitle(t('City'));
		$field->setDefaultValue($administrator->getCity());
		$field->setMaxlength(255);
		$this->addField($field);
		
		// Create a field for the administrator's address: Country
		$field = new M_FieldSelect('country');
		$field->setTitle(t('Country'));
		$field->setDefaultValue($administrator->getCountry());
		$field->setItem('', '-');
		foreach(M_LocaleData::getInstance()->getTerritories() as $code => $name) {
			$field->setItem($code, $name);
		}
		$this->addField($field);
		
		// Create a field for the administrator's locale
		$field = new M_FieldSelect('locale');
		$field->setTitle(t('Language'));
		$field->setMandatory(TRUE);
		$field->setDefaultValue($administrator->getLocale());
		foreach(M_LocaleMessageCatalog::getInstalled() as $locale) {
			$field->setItem($locale, M_Locale::getLanguageDisplayName($locale));
		}
		$this->addField($field);
		
		// Create a field for the administrator's organisation
		$field = new M_FieldText('website');
		$field->setTitle(t('Website'));
		$field->setMaxlength(255);
		$field->setDefaultValue($administrator->getWebsite());
		$this->addField($field);
		
		// Create a field for the administrator's email address
		$field = new M_FieldText('email');
		$field->setTitle(t('Email address'));
		$field->setMaxlength(255);
		$field->setMandatory(TRUE);
		$field->setDefaultValue($administrator->getEmail());
		$field->addValidator('M_ValidatorIsEmail', t('Please introduce a valid email address'));
		$this->addField($field);
		
		// Create a field for the administrator's phone number
		$field = new M_FieldText('phoneNumber');
		$field->setTitle(t('Phone number'));
		$field->setMaxlength(32);
		$field->setDefaultValue($administrator->getPhoneNumber());
		$this->addField($field);
		
		// Create a field for the administrator's fax number
		$field = new M_FieldText('faxNumber');
		$field->setTitle(t('Fax number'));
		$field->setMaxlength(32);
		$field->setDefaultValue($administrator->getFaxNumber());
		$this->addField($field);
	}
	
	/**
	 * Set Administrator
	 * 
	 * Can be used to set the administrator of which the data should be edited by
	 * the form.
	 * 
	 * @access public
	 * @param Administrator $administrator
	 * 		The administrator of which the data should be edited by the form
	 * @return void
	 */
	public function setAdministrator(Administrator $administrator) {
		$this->setDataObject($administrator);
	}
}