<?php
/**
 * AdminApplication
 * 
 * AdminApplication extends the {@link M_Application} class, in order
 * to add admin-specific functionality. The administration module will
 * always communicate with the AdminApplication class, instead of with the
 * {@link M_Application} class.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminApplication extends M_Application {
	/**
	 * Get modules
	 * 
	 * Overrides {@link M_Application::getModules()}, in order to provide with 
	 * instances of {@link AdminModule} instead of {@link M_ApplicationModule}
	 * 
	 * @see M_Application::getModules()
	 * @access public
	 * @return array
	 */
	public static function getModules() {
		// Return value:
		$out = array();
		
		// For each of the modules
		foreach(parent::getModules() as $i => $module) {
			// Add the module to output:
			$out[$i] = new AdminModule($module->getId());
		}
		
		// Return the iterator:
		return $out;
	}
}