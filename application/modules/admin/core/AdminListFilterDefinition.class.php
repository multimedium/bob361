<?php
/**
 * AdminListFilterDefinition
 * 
 * Used by {@link AdminListDefinition} to work with filters. Note that this class
 * is an abstract implementation, used as superclass for specific types of filters
 * in the list definition.
 *
 * @package App
 * @subpackage Admin
 */
abstract class AdminListFilterDefinition {
	/**
	 * Index number
	 * 
	 * This property stores the filter's index number. This index number
	 * defines the position in which the filter has been defined.
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_index = 0;
	
	/**
	 * Title
	 * 
	 * This property stores the title of the filter.
	 * 
	 * @see AdminListFilterDefinition::setTitle()
	 * @see AdminListFilterDefinition::getTitle()
	 * @access protected
	 * @var string
	 */
	protected $_title;
	
	/**
	 * Filter value
	 * 
	 * This property stores the filter value; which is used to apply the filter
	 * 
	 * @see AdminListFilterDefinition::setValue()
	 * @see AdminListFilterDefinition::getValue()
	 * @access protected
	 * @var integer
	 */
	protected $_value;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param string $title
	 * 		The title of the column
	 * @param integer $index
	 * 		The index number of the column
	 * @return AdminListColumnDefinition
	 */
	public function __construct($title, $index = 0) {
		$this->setTitle($title);
		$this->setIndex($index);
	}
	
	/**
	 * Get index number
	 * 
	 * @see AdminListColumnDefinition::$_index
	 * @access public
	 * @return integer
	 */
	public function getIndex() {
		return $this->_index;
	}
	
	/**
	 * Get title
	 *
	 * @access public
	 * @return string
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 * Get value
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getValue() {
		return $this->_value;
	}
	
	/**
	 * Set index number
	 * 
	 * For more information about the filter's index number, read the
	 * docs on {@link AdminListFilterDefinition::$_index}. This method
	 * can be used to set the index number.
	 * 
	 * Note that the index number must be equal to or greater than 0
	 * (ZERO)!
	 * 
	 * @access public
	 * @param integer $index
	 * 		The new index number of the filter
	 * @return void
	 */
	public function setIndex($index) {
		if(is_numeric($index) && $index >= 0) {
			$this->_index = (int) $index;
		} else {
			throw new M_Exception(sprintf(
				'A List Filter must be assigned a numerical Index Number that is equal to or greater than ZERO'
			));
		}
	}
	
	/**
	 * Set title
	 *
	 * @access public
	 * @param string $title
	 * 		The new title of the filter
	 * @return void
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	/**
	 * Set filter value
	 *
	 * @access public
	 * @param mixed $value
	 * 		The new value of the filter
	 * @return void
	 */
	public function setValue($value) {
		$this->_value = $value;
	}
	
	/* -- Abstract; to be implemented by subclasses -- */
	
	/**
	 * Get {@link M_DbQueryFilter}
	 * 
	 * Will create an instance of {@link M_DbQueryFilter} out of the current
	 * object. The return value of this method can be used as a filter for a
	 * mapper, in order to manipulate the results.
	 * 
	 * This is an abstract method. Note that {@link AdminListFilterDefinition} is 
	 * an abstract implementation, and defines this method in order to make sure 
	 * that subclasses implement this method!
	 * 
	 * @abstract
	 * @access public
	 * @return M_DbQueryFilter
	 */
	abstract public function getDbQueryFilter();
	
	/**
	 * Get {@link AdminSmartFolderFilter}
	 * 
	 * Will create an instance of {@link AdminSmartFolderFilter} out of the
	 * current object. The return value of this method can be used as a filter
	 * in an instance of {@link AdminSmartFolder} object.
	 * 
	 * This is an abstract method. Note that {@link AdminListFilterDefinition} is 
	 * an abstract implementation, and defines this method in order to make sure 
	 * that subclasses implement this method!
	 * 
	 * @abstract
	 * @access public
	 * @return AdminSmartFolderFilter
	 */
	abstract public function getAdminSmartFolderFilter();
	
	/**
	 * Get type of filter
	 * 
	 * Will indicate the type of the filter.
	 * 
	 * This is an abstract method. Note that {@link AdminListFilterDefinition} is 
	 * an abstract implementation, and defines this method in order to make sure 
	 * that subclasses implement this method!
	 * 
	 * @abstract
	 * @access public
	 * @return string
	 */
	abstract public function getType();
}