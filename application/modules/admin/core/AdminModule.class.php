<?php
/**
 * AdminModule
 * 
 * AdminModule extends the {@link M_ApplicationModule} class, in order
 * to add admin-specific functionality. The administration module will
 * always communicate with the AdminModule class, instead of with the
 * {@link M_ApplicationModule} class.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminModule extends M_ApplicationModule {
	/**
	 * Get list definition
	 * 
	 * This method will provide with the list definition of a given 
	 * {@link M_DataObject}. Similarly to
	 * 
	 * - {@link M_ApplicationModule::getDataObjectClassName()},
	 * - {@link M_ApplicationModule::getDataObjectMapper()},
	 * - etc...
	 *
	 * this method also expects you to provide the ID of the module's
	 * Data Object.
	 * 
	 * @access public
	 * @param string $id
	 * 		The ID of the data object
	 * @return AdminListDefinition
	 */
	public function getListDefinition($id) {
		return new AdminListDefinition($this, $id);
	}
	
	/**
	 * Get form definition
	 * 
	 * This method will provide with the form definition of a given 
	 * {@link M_DataObject}. Similarly to
	 * 
	 * - {@link M_ApplicationModule::getDataObjectClassName()},
	 * - {@link M_ApplicationModule::getDataObjectMapper()},
	 * - etc...
	 *
	 * this method also expects you to provide the ID of the module's
	 * Data Object.
	 * 
	 * @access public
	 * @param string $id
	 * 		The ID of the data object
	 * @return AdminFormDefinition
	 */
	public function getFormDefinition($id) {
		return new AdminFormDefinition($this, $id);
	}
	
	/**
	 * Is module "multimanageable"?
	 * 
	 * Will tell whether or not the module is available in the admin
	 * module.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the module is available in the administration
	 *		module, FALSE if not.
	 */
	public function isMultimanageable() {
		$v = (string) $this->_getInfoXmlFile()->multimanageable;
		return ($v != 'NO');
	}
}