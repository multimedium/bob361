/**
 * Bind an event (only once)
 * 
 * @access public
 * @param string type
 *		The type of event
 * @param function handler
 *		The event listener function, that responds to the DOM event being 
 *		triggered
 * @return $(matchedElements)
 */
$.fn.M_BindOnce = function(type, handler) {
	// Get the functions that have been bound to the DOM element:
	var events = $._data(this, 'events');
	
	// If an event handler, or multiple event handlers, of the requested type 
	// has already been bound to the element:
	if(events && typeof events[type] != 'undefined') {
		// Then, for each of the handlers:
		jQuery.each(events[type], function(i, handlerObj) {
			// We compare the current event handler to the one that has been
			// provided to this function
			if(handlerObj.handler.toString() == handler.toString()) {
				// Then, we unbind the handler. We will bind again with the
				// new handler. This is done to make sure that the handler
				// will keep working with new variable values.
				$(this).unbind(type, handlerObj.handler);
			}
		});
	}

	// Bind the event handler now:
	$(this).bind(type, handler);

	// Return the matched elements
	return this;
};

// Assign a Unique ID to element:
$.fn.M_AssignId = function() {
	if(typeof M_UniqueIdCounter == 'undefined') {
		M_UniqueIdCounter = 0;
	}
	
	this.each(function() {
		var i = $(this).attr('id');
		if(! i) {
			$(this).attr('id', 'm-interface-id-' + (++ M_UniqueIdCounter));
		}
	});
	
	return this;
}

// Check if data exists, in element
$.fn.M_HasData = function(dataId) {
	var _c1 = (typeof $(this).data(dataId) == 'object' && $(this).data(dataId) == null);
	var _c2 = (typeof $(this).data(dataId) == 'undefined');
	if(_c1 || _c2) {
		return false;
	} else {
		return true;
	}
}

// Get UNIX timestamp
$.M_Timestamp = function() {
	return parseInt(new Date().getTime().toString().substring(0, 10));
}

// Set default AJAX Properties
var __M_AjaxRequestSetup = {
	checkAuthIdentity  : true,
	checkAuthIdentityUrl : jQueryBaseHref + '/admin/authenticated',
	checkAuthIdentityInterval : 300, // check Auth Identity every x milliseconds
	checkAuthIdentityCallback : function(data) {
		return (parseInt($('auth authenticated', data).text()) == 1);
	},
	checkAuthIdentityUnknownMessage : 'Your session has expired. \n\nIn order to continue, it is required that you login again! To do so, press <OK>',
	checkAuthIdentityUnknownUrl : jQueryBaseHref + '/admin/route/login',
	timestampLastRequest : jQuery.M_Timestamp()
};

// Function to set Default AJAX Properties
$.M_AjaxRequestSetup = function(options) {
	__M_AjaxRequestSetup = jQuery.extend(__M_AjaxRequestSetup, options);
}

// Do AJAX Request
$.M_AjaxRequest = function(options) {
	// Has AJAX been set up to check login status before sending the new request?
	// Then, check the interval with which ajax requests should check the login
	// status. If the interval has expired, we need to control the login state
	// right now:
	if(__M_AjaxRequestSetup.checkAuthIdentity && __M_AjaxRequestSetup.checkAuthIdentityInterval < (jQuery.M_Timestamp() - __M_AjaxRequestSetup.timestampLastRequest)) {
		// Check login status:
		$.ajax({
			url : __M_AjaxRequestSetup.checkAuthIdentityUrl,
			success : function(data) {
				// If authenticated, we do the AJAX Call:
				if(__M_AjaxRequestSetup.checkAuthIdentityCallback(data)) {
					_doAjax(options);
				}
				// If not authenticated
				else {
					// we show the "unknown" message (if any)
					if(__M_AjaxRequestSetup.checkAuthIdentityUnknownMessage) {
						alert(__M_AjaxRequestSetup.checkAuthIdentityUnknownMessage);
					}
					
					// And, we redirect
					document.location.href = __M_AjaxRequestSetup.checkAuthIdentityUnknownUrl;
				}
			}
		});
		
		// Set new time of last request
		__M_AjaxRequestSetup.timestampLastRequest = jQuery.M_Timestamp();
	}
	// If login status should not be checked at this moment, we simply do the 
	// AJAX Request:
	else {
		_doAjax(options);
	}
	
	// Do the ajax request:
	function _doAjax(options) {
		$.ajax(options);
	}
}

// Debugging messages
$.M_Console = function(msg) {
	// console.log(msg);
}

// Add confirm box in between
$.fn.M_ConfirmLink = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		confirmText  : ""
	}, options);
	
	this.click(function() {
		var s = confirm(_options.confirmText);
		if(s) {
			return true;
		} else {
			return false;
		}
	});
}

// Make an input field adapt to the amount of text placed inside. Also adds listeners,
// so input will keep adjusting itself to text width
$.fn.M_InputAdjustToTextWidth = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		padding  : 25,
		minWidth : 50,
		maxWidth : 300
	}, options);
	
	// Set actions:
	return this.each(function(i){
		var e = jQuery(this);
		// create copy for each element
		var o = { pad: options.pad, min_limit: options.min_limit, max_limit: options.max_limit }; 
		if (e.attr('type') == 'text') {
			if (!o.min_limit) o.min_limit = parseInt(e.css('min-width'));
			if (!o.max_limit) o.max_limit = parseInt(e.css('max-width'));
			
			// Safari reads a non-existant value as -1 making our calcs a mess
			if (o.min_limit < 0 || isNaN(o.min_limit)) o.min_limit = 0;
			if (o.max_limit < 0 || isNaN(o.max_limit)) o.max_limit = 0;

			var html = '<' + 'div style="position:absolute; top:0; visibility:hidden; left:0; padding:0 ' + o.pad + 'px 0 0!important; margin:0!important" id="_grow' + i + '"></' + 'div>';
			var c = jQuery('body').append(html);
			// adjust the style of the container to match the font of this element
			var ff = e.css('font-family');
			var fs = e.css('font-size');

			jQuery('div#_grow' + i).css({ 'font-size': fs, 'font-family': ff });

			resize = function(){
				var dl = jQuery('div#_grow' + i).html(e.val().replace(/ /g,'&nbsp;')).get(0).offsetWidth;
				if (o.max_limit && (dl + o.pad) > o.max_limit) {
					e.css('width', o.max_limit + 'px')
				} else if ((dl + o.pad) <= o.min_limit) {
					e.css('width', o.min_limit + 'px')
				} else { // resize
					e.css('width', dl + 'px')
				}
			}

			// auto resize based on current content
			resize();
			e.keypress(resize);
		}
	})
}

// Disable browser's built-in drag-n-drop of images
$.fn.M_PreventDefaultDragging = function() {
	this.mousedown(function(event){
		if(event.preventDefault) {
			event.preventDefault();
		}
	});
}

// M_Foldable
// @todo: Add plugin to remember interface state/preference!!
$.fn.M_Foldable = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		showFolded                 : false,
		foldedCssClass             : 'folded',
		foldableElement            : null, //unfold when anything changes in this element
		unfoldOnChange			   : false,
		callbackAnimationShow      : function($e) {
			$e.slideDown(100);
		},
		callbackAnimationHide      : function($e) {
			$e.slideUp(100);
		}
	}, options);
	
	// We maintain the collection of foldable elements:
	if(typeof _matchedFoldables == 'undefined') {
		_matchedFoldables = new Array();
	}
	
	// For each of the matched elements:
	this.each(function() {
		// Get a reference to the current element
		var $t = $(this);
		
		// If the foldable element is not given, we default to the next sibling
		var $e;
		if(! _options.foldableElement) {
			// @todo
			// IS THERE A BETTER WAY TO FIND NEXT SIBLING?
			$e = null;
			$(jQuery.sibling(this)).next().each(function(i) {
				if(i == 0) {
					$e = $(this);
				}
			});
		} else {
			$e = _options.foldableElement;
		}
		
		// If the foldable element has been found
		if($e && $e.length && $e.length > 0) {
			// This plugin requires the tags to have an ID. We make sure the tags
			// come with a populated ID Attribute
			$(this).M_AssignId();
			$e.M_AssignId();
			
			// Save the foldable:
			_matchedFoldables[$(this).attr('id')] = $e;
			
			// When the foldable content is updated, we automatically unfold:
			if (_options.unfoldOnChange) {
				$e.change(function() {
					_unfold($t, true);
				});
			}
		}

		// Fold? DISABLE FOR NOW, AS COOKIES ARE DISABLED OVERALL
//		if($.cookie($e.M_AssignId().attr('id') + '-m-foldable-state') == 1) {
//			_fold($t, false);
//		} else {
//			_unfold($t, false);
//		}

		// Based on the CSS Class of the element, fold or unfold on init
		$t.hasClass(_options.foldedCssClass) ? _fold($t, false) : _unfold($t, false);
		
		// On initial setup: check if a folded status is provided
	});
	
	// If the initial state is "folded", we hide all of the matched elements:
	if(_options.showFolded) {
		_fold(this, false);
	}
	
	// We set the event listeners for each of the matched elements. In the case
	// of the M_Foldable plugin, we need to listen to a "click" event. 
	// In order words, when the matched item(s) is/are clicked, we do the following:
	this.click(function() {
		// Get the foldable content:
		var $e = _getFoldableElement($(this));
		
		// Stop any running animation(s)
		$e.stop();
		
		// Toggle the collapse state of foldable element(s)
		if(_isUnfolded($e)) {
			_fold($(this), true);
		} else {
			_unfold($(this), true);
		}
		
		// We are overriding the default behavior of the clicked HTML Element
		// (which is probably a link). This way, we degrade gracefully. To do
		// so, we return false:
		return false;
	});
	
	// function that tells whether or not the content is showing:
	function _isUnfolded($element) {
		return ($('#' + $element.attr('id') + ':visible').length == 1);
	}
	
	// function that SHOWS the foldable content
	function _unfold($triggerElement, doAnimation) {
		$triggerElement.each(function() {
			// Get the foldable content:
			var $e = _getFoldableElement($(this));
			
			// Remove the class from the triggering element, to show that we have unfolded:
			$(this).removeClass(_options.foldedCssClass);
			
			// Show the collapsable content:
			if(doAnimation) {
				_options.callbackAnimationShow($e);
			} else {
				$e.show();
			}

			// Set a cookie, to remember
			$.cookie($e.M_AssignId().attr('id') + '-m-foldable-state', 0);
		});
	}
	
	// function that HIDES the foldable content
	function _fold($triggerElement, doAnimation) {
		$triggerElement.each(function() {
			// Get the foldable content:
			var $e = _getFoldableElement($(this));
			
			// Add the class to the triggering element, to show that we have hidden:
			$(this).addClass(_options.foldedCssClass);
			
			// Hide the collapsable content:
			if(doAnimation) {
				_options.callbackAnimationHide($e);
			} else {
				$e.hide();
			}

			// Set a cookie, to remember
			$.cookie($e.M_AssignId().attr('id') + '-m-foldable-state', 1);
		});
	}
	
	// Get foldable content of trigger element
	function _getFoldableElement($triggerElement) {
		if(typeof _matchedFoldables[$triggerElement.attr('id')] != 'undefined') {
			return _matchedFoldables[$triggerElement.attr('id')];
		} else {
			return null;
		}
	}
}

// Load in Content Pane
$.fn.M_LoadInContentPane = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		contentPaneSelector  : '#idContentPane',
		callbackUrl          : function($triggerElement) {
			return ($triggerElement.attr('href') + '-ajax');
		}
	}, options);
	
	// Remember about matched elements:
	var _matchedElements = this;
	
	// TODO: Maintain history:
	// this.remote(_options.contentPaneSelector, function(){ alert('test'); });
	
	// We set the event listeners for each of the matched elements. In the case
	// of the M_LoadInContentPane plugin, we need to listen to a "click" event. 
	// In order words, when the matched item is clicked, we do the following:
	_matchedElements.click(function() {
		// Show the loading state in the content pane
		_toggleLoadingStateOfContentPane(true);
		
		// Get Ajax properties:
		var ajaxProperty = _getButtonAjaxProperties($(this));
		
		// Do the Ajax Call
		$.M_AjaxRequest({
			url      : ajaxProperty.url,
			data     : ajaxProperty.data,
			dataType : ajaxProperty.dataType,
			// When the Ajax Call has been completed, and we get a response from
			// the server:
			success  : function(data, textStatus) {
				// Show the loaded content:
				$(_options.contentPaneSelector).html(data);
				
				// Look for javascript files in the content:
				//$('script', $(_options.contentPaneSelector)).each(function() {
				//	alert($(this).attr('src'));
				//});
				
				// Dispatch an event, to inform about the update of the content:
				$(_options.contentPaneSelector).trigger('change');
				
				// Dispatch an event, to inform about the update of the content
				// pane container:
				$(_options.contentPaneSelector).trigger('M_LoadInContentPane_Update', [ajaxProperty.url]);
				
				// We remove the loading state from the content pane:
				_toggleLoadingStateOfContentPane(false);
			}
		});
		
		// We are overriding the default behavior of the clicked HTML Element
		// (which is probably a link). This way, we degrade gracefully. To do
		// so, we return false:
		return false;
	});
	
	// Function that provides with the AJAX Call Properties, based on the triggering
	// element in the page
	function _getButtonAjaxProperties($triggerElement) {
		// Return Properties:
		return {
			url : _options.callbackUrl($triggerElement),
			data: {},
			dataType: "html"
		};
	}
	
	// Function to show the loading state:
	function _toggleLoadingStateOfContentPane(flag) {
		// If loading:
		if(flag) {
			// Show Loading DIV:
			$(_options.contentPaneSelector).M_LoadingState({
				isLoading : true
			});
		}
		// If done loading:
		else {
			// Hide Loading DIV:
			$(_options.contentPaneSelector).M_LoadingState({
				isLoading : false
			});
		}
	}
}

$.fn.M_LoadingState = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		isLoading : true,
		bgColor : '#fff',
		opacity : 1,
		showHtml : '<div class="loading"></div>'
	}, options);
	
	// prepare the ID of the loading container:
	var i = 'm-loading-state';
	
	// Remove previously created loader
	$('#' + i).remove();
	
	// If loading:
	if(_options.isLoading) {
		// Create the loading DIV
		$('body').append('<div id="'+ i +'">'+ _options.showHtml +'</div>');
		
		// Set some CSS properties on the loading div
		$('#' + i).css({
			'background-color' : _options.bgColor,
			'opacity'          : _options.opacity,
			'position'         : 'absolute',
			'top'              : this.offset().top,
			'left'             : this.offset().left,
			'width'            : this.width(),
			'height'           : this.height(),
			'z-index'          : 60000
		});
	}
}

// Menu (with toggling active item)
$.fn.M_ToggleActive = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		activeClass               : 'active',
		callbackGetToggledElement : function($triggerElement) {
			return $triggerElement;
		}
	}, options);
	
	// Remember about matched elements:
	var _matchedElements = this;
	
	// We set the event listeners for each of the matched elements. In the case
	// of the M_ToggleActive plugin, we need to listen to a "click" event. 
	// In order words, when the matched item is clicked, we do the following:
	_matchedElements.click(function() {
		// element to be toggled:
		var $e;
		
		// Remove active state from any previously selected item:
		_matchedElements.each(function() {
			// We get the element from which we should remove the active 
			// class now:
			$e = _options.callbackGetToggledElement($(this));
			
			// Remove the active class:
			$e.removeClass(_options.activeClass);
		});
		
		// We get the element that should be assigned the active class now:
		$e = _options.callbackGetToggledElement($(this));
		
		// Assign the class to the element:
		$e.addClass(_options.activeClass);
		
		// We are overriding the default behavior of the clicked HTML Element
		// (which is probably a link). This way, we degrade gracefully. To do
		// so, we return false:
		//return false;
	});
}

// Popmenu
$.fn.M_Popmenu = function(options) {
	// Compose the options of the functions
	// (merge with default options)
	var _options = jQuery.extend({
		speed            : 100,
		width            : 0,
		marginFromWindow : 5,
		menuContainer    : null
	}, options);

	// Is this the first call of the plugin?
	if(typeof _popmenuCallCounter == 'undefined') {
		_popmenuCallCounter = 0;
		
		// If this is the first call, we set the initial event listeners:
		_setInitialListeners(this);
	} else {
		_popmenuCallCounter ++;
	}
	
	// Hide original menu container:
	_hideOriginalContainer(_options.menuContainer);
	
	// Listen to the click event on each of the matched elements:
	this.click(function($eventObject) {
		// Create the menu element
		var $popMenu = _buildPopmenu(_options.menuContainer, $(this));
		
		// position the menu element on the page
		_positionPopmenu($popMenu, $(this));
		
		// Trigger an event, to notify that the menu is about to be opened
		$(this).trigger('M_Popmenu_Before_Open', [$popMenu]);
		
		// show the menu item
		var $t = $(this);
		$popMenu.fadeIn(_options.speed, function() {
			// Trigger an event, to notify others about the menu having been opened
			$t.trigger('M_Popmenu_Open', [$popMenu]);
		});
		
		// return false, so whatever default behavior (eg. link) is
		// overridden. This allows to gracefully degrade to plain HTML.
		return false;
	});
	
	// Function that appends the menu html to the page:
	function _buildPopmenu($container, $interfaceSender) {
		// Remove any previously appended menu:
		_closePopmenu($interfaceSender);
		
		// Prepare local variables:
		var type  = '';
		var html  = '';
		
		// Start the HTML wrapper:
		html += '<div id="jquery-popmenu" class="jquery-m-popmenu-wrapper-1" style="display: none; '+ (_options.width ? 'width: ' + _options.width +'px' : '' ) + ';">';
		html +=    '<div class="jquery-m-popmenu-wrapper-2">';
		html +=       '<div class="jquery-m-popmenu-wrapper-3">';
		html +=          '<div class="jquery-m-popmenu-wrapper-4">';
		html +=             '<div class="jquery-m-popmenu-wrapper-5">';
		
		// For each of the items in the menu:
		var items = _getItemsFromContainer($container);
		for(var i in items) {
			// Get the type of the current item:
			type = (typeof items[i].type == 'undefined') ? 'link' : items[i].type;
			
			// The way we render the current item, depends on the type of the item
			switch(type) {
				case 'title':
					html += '<div class="group">';
					html +=    items[i].title;
					html += '</div>';
					break;
				
				case 'link':
					html += '<a href="'+ items[i].href +'" id="jquery-popmenu-a-'+ i +'">';
					html +=    items[i].title;
					html += '</a>';
					break;
				
				case 'separator':
					html += '<div class="separator"><!-- --></div>';
					break;
			}
		}
		
		// Finish the HTML Wrapper
		html +=             '</div>';
		html +=          '</div>';
		html +=       '</div>';
		html +=    '</div>';
		html += '</div>';
		
		// Append the menu to the page:
		$('body').append(html);
		
		// Now, we also copy the events:
		for(var j in items) {
			//
			$('a#jquery-popmenu-a-'+ j).click(function() {
				// Extract the number
				var n = parseInt($(this).attr('id').substr(17, $(this).attr('id').length));
				
				var e = $._data(items[n].a[0], "events");
				
				// has click event been attached?
				if(e && e.click) {
					// trigger a new event on the document, so we can use the
                    // original element ($interfaceSender) and the selected
                    // options (items[n].a) when we want to perform a different
                    // action as a redirect
					$(document).trigger('M_Popmenu_Select', [items[n].a, $interfaceSender]);

					// Trigger a click on the original link
					items[n].a.trigger('click');
					
					// Remove the popmenu
					_closePopmenu($interfaceSender);
					
					// Return false, to override default behavior
					return false;
				}
				// If not, load href
				else {
					document.location.href = $(this).attr('href');
				}
			});
		}
		
		// Trigger an event, to notify others about the menu having been created
		$interfaceSender.trigger('M_Popmenu_Build');
		
		// Return the new element as result of this method:
		return $('#jquery-popmenu');
	}
	
	// Hide original container
	function _hideOriginalContainer($container) {
		// Hide the original container:
		$container.css({
			'position' : 'absolute',
			'z-index'  : -100,
			'top'      : -1000,
			'left'     : -1000
		}).hide().height(0);
		
		// Since we have hidden the original container by resizing, we simulate
		// the window being resized:
		$(window).trigger('resize');
	}
	
	// Function that provides with currently opened popmenu
	function _getPopmenu() {
		// Check if the menu exists:
		var $d = $('#jquery-popmenu');
		if($d.length > 0) {
			return $d;
		} else {
			return null;
		}
	}
	
	// Function that closes the menu
	function _closePopmenu($interfaceSender) {
		// Check if the menu exists:
		var $d = _getPopmenu();
		if($d) {
			// Remove the menu:
			$d.remove();
			
			// Trigger an event, to notify others about the menu having been created
			$interfaceSender.trigger('M_Popmenu_Close');
		}
	}
	
	// Get menu items from container:
	function _getItemsFromContainer($container) {
		// Output array
		var o = new Array();
		
		// For each of the lists in the container:
		$('ul', $container).each(function(i) {
			// If this is not the first list, we add a separator:
			if(i > 0) {
				o[o.length] = {
					type  : 'separator'
				};
			}
			
			// Get the Group Title of the items
			var t = $(this).attr('title');
			if(t) {
				// If available, add the title to the output:
				o[o.length] = {
					type  : 'title',
					title : t,
					elm   : null
				};
			}
			
			// For each of the children:
			$(this).children().each(function() {
				// Get the link:
				var $a = $('a', $(this));
				
				//quit if element does not exist
				if($a.length == 0) return; 
				
				// Make sure the link has a unique ID:
				$a.M_AssignId();
				
				// Add the item:
				o[o.length] = {
					type  : 'link',
					title : $a.html(),
					href  : $a.attr('href'),
					a     : $a
				}
			});
		});
		
		// Return output
		return o;
	}
	
	// function that positions the menu on the page
	function _positionPopmenu($popMenu, $interfaceSender) {
		// Get position for menu:
		var posX = $interfaceSender.offset().left;
		var posY = $interfaceSender.offset().top + $interfaceSender.height();
		
		// After positioning, check if it is indeed visible in the viewport.
		// If it isn't, manually, set it higher:
		if(posY > $interfaceSender.parent().height()) {
			posY = $interfaceSender.offset().top - $popMenu.height();
		}
		
		// Position the element
		$popMenu.css({
			position: 'absolute',
			top: posY,
			left: posX
		});

		// Max height of the popmenu:
		var maxH = ($(document).height() - 50 - _options.marginFromWindow * 2);

		// Never higher than 750px
		if(maxH > 750) {
			maxH = 750;
		}

		// Is the popmenu higher?
		if($popMenu.height() > maxH) {
			// Then, limit to max height
			$('.jquery-m-popmenu-wrapper-5', $popMenu).css('height', maxH + 'px');
		}
		
		// Make sure the element stays inside the viewport:
		if(posX + $popMenu.width() > $(document).width()) {
			$popMenu.css({
				left: ($(document).width() - $popMenu.width() - _options.marginFromWindow)
			});
		}

		if(posY + $popMenu.height() > $(document).height()) {
			$popMenu.css({
				top: ($(document).height() - $popMenu.height() - _options.marginFromWindow)
			});
		}

		// Make sure the top position is never negative
		if(posY < 0) {
			$popMenu.css({
				top: 0
			});
		}
		
		// Trigger an event, to notify others about the menu having been positioned
		$interfaceSender.trigger('M_Popmenu_Position', [$popMenu]);
		
		// When the window is resized, we reposition the element:
		// $(window).resize(function() {
		//	_posMenuInPage($popMenu, $interfaceSender);
		//});
	}
	
	// Now, we listen to any other click outside of the menu. Such
	// a click will cause the menu to be hidden
	function _setInitialListeners($interfaceSender) {
		// When the user clicks (anywhere in the page)
		$(document).click(function($eventObjectOfDocumentClick) {
			// If a popmenu is open:
			var $d = _getPopmenu();
			if($d) {
				// do a hit-test on the menu
				var minX = parseInt($d.css('left'));
				var minY = parseInt($d.css('top'));
				var maxX = minX + $d.width();
				var maxY = minY + $d.height();
				var isX = ($eventObjectOfDocumentClick.pageX >= minX && $eventObjectOfDocumentClick.pageX <= maxX);
				var isY = ($eventObjectOfDocumentClick.pageY >= minY && $eventObjectOfDocumentClick.pageY <= maxY);
				
				// If the click does not hit the menu:
				if(!isX || !isY) {
					// Close the menu:
					_closePopmenu($interfaceSender);
				}
			}
		});
		
		// When the window is resized:
		$(window).resize(function() {
			// If a popmenu is open:
			var $d = _getPopmenu();
			if($d) {
				// Reposition the menu:
				_positionPopmenu($d, $interfaceSender);
			}
		});
	}
}

// Make a div fill the remaining vertical space
$.fn.M_FillRemainingVerticalSpace = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		watchSiblings       : ''
	}, options);
	
	// Is this the first call of the plugin?
	if(typeof _matchedFillingObjects == 'undefined') {
		// Prepare repository of matched objects:
		_matchedFillingObjects = new Array();
		
		// In order to maintain the height of the matched elements, we need to respond
		// to the window being resized. We set the event listener only once (at
		// plugin's first call)
		$(window).resize(_onResizedWindow);
	}
	
	// Assign an ID, if missing:
	this.M_AssignId();
	
	// Remember about matched objects in the page:
	this.each(function() {
		// Get the ID of the element:
		var id = $(this).attr('id');
		
		// If not already registered, do so now:
		if(jQuery.inArray(id, _matchedFillingObjects) == -1) {
			_matchedFillingObjects.push(id);
		}
	});
	
	// Initially, resize the matched elements:
	_resizeMatchedObjects();
	
	// Respond to resized window:
	function _onResizedWindow($eventObject) {
		_resizeMatchedObjects();
	}
	
	// Resize the matched elements, given a window width and height
	function _resizeMatchedObjects(windowWidth, windowHeight) {
		// If width is not given, default to window's width
		if(typeof windowWidth == 'undefined') {
			windowWidth = $(window).width();
		}
		
		// If height is not given, default to window's height
		if(typeof windowHeight == 'undefined') {
			windowHeight = $(window).height();
		}
		
		// For each of the elements that should be filling remaining space:
		for(var i = 0; i < _matchedFillingObjects.length; i ++) {
			// Get a reference to the current element:
			var $t = $('#' + _matchedFillingObjects[i]); // _matchedFillingObjects[i]
			var _p = $t.position();
			
			// We get the parent element's height. This is the base height that
			// should be filled.
			var h = $t.parent().height();
			
			// Of course, we need to take into account all of the element's siblings
			// (inside the parent), and subtract their height from the base height.
			// This way, we calculate the remaining height.
			var $siblings = _options.watchSiblings ? $t.siblings(_options.watchSiblings) : $t.siblings();
			$siblings.each(function() {
				// If we should not ignore the item, we make sure it's not
				// "floating". We only take into account the elements that
				// are positioned above, or below:
				var p = $(this).position();
				if(p.top != _p.top) {
					h -= $(this).outerHeight(true);
				}
			});
			
			// Set the height of the element to the calculated one:
			$t.height(h);
		}
	}
}

// Set min dimensions for the matched elements
$.fn.M_MinimumSize = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		width  : 0,
		height : 0
	}, options);
	
	// Resize, if too small:
	if(_isTooSmall(this)) {
		_resizeToMin(this);
	}
	
	// When resized, check minimum again:
	this.resize(function() {
		if(_isTooSmall($(this))) {
			_resizeToMin($(this));
		}
	});
	
	// Function to check if element is too small
	function _isTooSmall($element) {
		return ($element.width() < _options.width || $element.height() < _options.height);
	}
	
	// Function to resize element to minimum size
	function _resizeToMin($element) {
		$element.width(_options.width);
		$element.height(_options.height);
		alert("Resized to min: " + _options.width + ' x ' + _options.height);
	}
}

// Plugin to create a new button
// (e.g. Used by Plus Button to create a new button in the Source Pane)
$.fn.M_ElementCreator = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		newButtonContainer         : null,
		defaultValue               : 'Untitled',
		// Default button decorator:
		callbackElementDecorator   : function($element) {},
		// Default input field decorator:
		callbackInputDecorator     : function($input) {
			$input.css({
				'border' : 'none',
				'background' : 'none',
				'background-color' : '#fff'
			});
		},
		
		// We have a set of "private" options, which can be configured but should
		// only be changed if you have a good understanding of this plugin.
		_newButtonInputFieldId : 'jquery-m-new-button'
	}, options);
	
	// If the newButtonContainer has not been given, we notify about the missing
	// property:
	if(! _options.newButtonContainer && ! _options.newButtonContainer.length) {
		alert('M_NewButton: Missing the "newButtonContainer" property!');
	}
	
	// Maintain internal counter
	if(typeof jQueryNewButtonCounter == 'undefined') {
		jQueryNewButtonCounter = 0;
	}
	
	// We set the event listeners for each of the matched elements. In the case
	// of the M_ElementCreator plugin, we need to listen to a "click" event. In order 
	// words, when the matched item is clicked, we do the following:
	this.click(function() {
		// Prepare the ID of the new field:
		var n = _options._newButtonInputFieldId;
		var i = _options._newButtonInputFieldId + '-id';
		
		// Create a new input field, so the user can introduce the label of the
		// new button:
		_options.newButtonContainer.append('<input type="text" name="'+ n +'" value="'+ _options.defaultValue +'" id="'+ i +'" />');
		
		// We decorate the new input field:
		_options.callbackInputDecorator($('#' + i));
		
		// We trigger the change() event of the container:
		// (so other plugins can respond to this event)
		_options.newButtonContainer.trigger('change');
		
		// Select the input field:
		$('#' + i).focus().select();
		
		// Now, we listen to some events of the input field:
		// - When the field is completed:
		$('#' + i).blur(function() {
			_dispatchCreatedButtonLabel($(this));
		});
		
		// When the <Enter> Key is pressed, we simulate the blur event
		$('#' + i).keyup(function($eventObject) {
			var k = $eventObject.charCode ? $eventObject.charCode : $eventObject.keyCode ? $eventObject.keyCode : 0;
			if(k == 13) { // "Enter" key
				$(this).trigger('blur');
			}
		});
		
		// We are overriding the default behavior of the clicked HTML Element
		// (which is probably a link). This way, we degrade gracefully. To do
		// so, we return false:
		return false;
	});
	
	// Dispatches the created button label (called when the user has finished
	// introducing the text for the new button). The result of this function is
	// provided to the callback function "callbackDecorateButton"
	function _dispatchCreatedButtonLabel($inputControl) {
		// Get the introduced text:
		var t = $inputControl.val();
		
		// Remove the input field:
		$inputControl.remove();
		
		// We prepare the ID string for the new button:
		var i = _options._newButtonInputFieldId + '-' + (++ jQueryNewButtonCounter);
		
		// Append the button to the container:
		_options.newButtonContainer.append('<span id="'+ i +'">'+ t +'</span>');
		
		// Decorate the new button
		_options.callbackElementDecorator($('#' + i));
	}
}

// Plugin to clone elements
$.fn.M_Clone = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		duplicateContainer    : null,
		callbackAnimationShow : function($e) {
			// Hide before sliding down. This way, it always gets animated!
			$e.hide().slideDown(150, function() {
				// Simulate window-resize. This way, we notify about the siblings
				// being resized?
				$(window).resize();
			});
		}
	}, options);
	
	// Maintain a counter of duplicated elements:
	if(typeof _duplicateCounter == 'undefined') {
		_duplicateCounter = 0;
	}
	
	// Clone the matched element(s):
	var $c = this.clone();
	
	// Set the ID of the cloned element(s):
	$c.each(function() {
		$(this).attr('id', 'jquery-m-cloned-element-' + (++ _duplicateCounter));
	});
	
	// Set container of clone, if not given:
	var $t = _options.duplicateContainer ? _options.duplicateContainer : this.parent();
	
	// Append the element to the container:
	$c.appendTo($t);
	
	// Animate the cloned element into view:
	_options.callbackAnimationShow($c);
	
	// Dispatch an event, to notify about the element being cloned:
	// (we pass a reference to the container of cloned element(s) along with the event)
	$(this).trigger('M_Clone', [$c, this, $t]);
	
	// Also, dispatch another event, to notify about the cloned elements'
	// container having been changed:
	$t.trigger('change');
	
	return $(this);
}

$.fn.M_BoundingBoxDraw = function(options) {
	var _options = jQuery.extend({
		// Number of pixels for thickness
		thickness : 2,
		// Color (hex):
		color : '#000000',
		// Transparency
		opacity : 0.8,
		// Position (inner/outer)
		position : 'outer',
		// Style (solid, dashed, ...)
		style : 'solid'
	});
	
	// For each of the matched elements:
	$(this).each(function() {
		// Make sure the element has an ID, then get the offset:
		var offset = $(this).M_AssignId().offset();
		
		// Calculate the coordinates of the bounding box:
		var xmin = offset.left;
		var xmax = offset.left + $(this).width() - _options.thickness;
		var ymin = offset.top;
		var ymax = offset.top + $(this).height() - _options.thickness;
		
		// If the position is "inner", modify the coordinates:
		if(_options.position == 'inner') {
			xmin += _options.thickness;
			xmax -= (_options.thickness * 2);
			ymin += _options.thickness;
			ymax -= (_options.thickness * 2);
		}
		
		// The HTML element that draws a bounding box:
		var $box = $('<div id="'+ $(this).attr('id') +'BoundingBox" style="position: absolute; top: '+ ymin +'px; left: '+ xmin +'px; width: '+ (xmax - xmin) +'px; height: '+ (ymax - ymin) +'px; border: '+ _options.thickness +'px '+ _options.style +' '+ _options.color +'"></div>');
		
		// Apply the opacity:
		$box.css({opacity : _options.opacity, 'z-index' : ($(this).css('z-index') + 1)});
		
		// Add to body:
		$('body').append($box);
	});
}

var M_FollowRegisteredElements = 0;
var M_FollowRegisteredElementIds = [];
$.fn.M_Follow = function(options) {
	// Merge options
	var _options = jQuery.extend({
		element : null,
		followX : true,
		followY : true
	}, options);
	
	// Register the element:
	if(typeof M_FollowRegisteredElementIds[$(this).M_AssignId().attr('id')] == 'undefined') {
		M_FollowRegisteredElements ++;
	}
	
	M_FollowRegisteredElementIds[$(this).attr('id')] = [_options.element.M_AssignId().attr('id'), _options.followX, _options.followY];
	
	// If at least 1 element is following:
	if(M_FollowRegisteredElements > 0) {
		// If interval not yet created:
		if(typeof M_FollowIntervalId == 'undefined') {
			// Every x milliseconds:
			M_FollowIntervalId = setInterval(function() {
				// For each of the following elements:
				for(var i in M_FollowRegisteredElementIds) {
					// Check if element still exists:
					var $e = $('#' + i);
					var $f = $('#' + M_FollowRegisteredElementIds[i][0]);
					if($e.length > 0 && $f.length > 0) {
						// If the element is not being animated at the moment:
						if(! $e.is(':animated')) {
							// We get the position of the element we are following
							var o = $f.offset();
							
							// Set the CSS, to keep following:
							$e.css({ position : 'absolute' });
							if(M_FollowRegisteredElementIds[i][2]) {
								$e.css({ top : o.top + 'px' });
							}
							if(M_FollowRegisteredElementIds[i][1]) {
								$e.css({ left : o.left + 'px' });
							}
							
							// Debugging
							// $.M_Console($e.attr('id') + ' is following ' + $f.attr('id'));
						}
					}
					// If element no longer exists:
					else {
						// Remove from registered elements:
						M_FollowRegisteredElements --;
						delete M_FollowRegisteredElementIds[i];
					}
				}
			}, 100);
		}
	}
	// If no elements following:
	else {
		if(typeof M_FollowIntervalId != 'undefined') {
			clearInterval(M_FollowIntervalId);
			delete M_FollowIntervalId;
		}
	}
}

$.fn.M_BoundingBoxRemove = function() {
	// For each of the matched elements:
	$(this).each(function() {
		// Remove the box
		$('#' + $(this).attr('id') +'BoundingBox').remove();
	});
}

$.fn.M_Tooltip = function(options) {
	// For each of the matched elements:
	this.each(function() {
		// Get the parent of current item:
		var $p = $(this).parent();
		var $t = $(this);
		
		// If the tooltip html is not empty:
		if(jQuery.trim($(this).html()) != '') {
			// Get the tooltip html, and store it in the element parent's data:
			$p.data('M_TooltipOptions', jQuery.extend({
				tooltipHtml : $(this).html(),
				// Tooltip class
				tooltipClass : 'tooltip',
				// Position reference (to determine where the tooltip should be positioned)
				// A jQuery selection is expected here
				tooltipPositioningReferenceX : $p,
				tooltipPositioningReferenceY : $p,
				// Animation?
				tooltipAnimate : false,
				tooltipAnimationDuration : 150,
				// Maintain tooltip?
				tooltipMaintain : true,
				tooltipMaintainDuration : 500
			}, options));
			
			// Remove the original html
			$(this).remove();
			
			// Set actions on the parent
			$p.M_AssignId().hover(function() {
				// Remove timeout that hides the tooltip
				if(typeof _m_tooltipTimeout != 'undefined') {
					clearTimeout(_m_tooltipTimeout);
				}
				
				// Remove previously created tooltip(s)
				$.M_TooltipRemove();
				
				// Dispatch an event:
				$(document).trigger('M_Tooltip_BeforeShow', [$(this)]);
				
				// Prepare the ID of the tooltip:
				var i = 'M_Tooltip_ElementId';
				
				// Get a new tooltip element:
				var $tooltip = $('<div class="'+ $(this).data('M_TooltipOptions').tooltipClass +'" id="'+ i +'" style="position: absolute; top: 0; left: 0; float: left; display: none;">' + $(this).data('M_TooltipOptions').tooltipHtml + '</div>');
				
				// Remember about parent:
				$tooltip.data('parent', $(this).M_AssignId().attr('id'));
				
				// Append to document:
				$('body').append($tooltip);
				//$tooltip.hide();
				
				// Get positioning:
				var pos = _getTooltipPosition($(this), $tooltip);
				
				// Apply CSS to tooltip:
				$tooltip.css(pos[0]);
				
				// Animate the tooltip into view:
				pos[1]();
				
				// Tooltip follows parent:
				$tooltip.M_Follow({ element : $(this), followX : false });
				
				// Move over/out tooltip:
				$tooltip.hover(function() {
					if(typeof _m_tooltipTimeout != 'undefined') { clearTimeout(_m_tooltipTimeout); }
				}, function() {
					$.M_TooltipRemove();
				});
				
				// Dispatch an event:
				$(document).trigger('M_Tooltip_Show', [$(this), $tooltip]);
				
			}, function($eventObject) {
				// Get the tooltip:
				var $tooltip = $('#M_Tooltip_ElementId');
				
				// Should the tooltip be removed? If not:
				if(! $(this).data('M_TooltipOptions').tooltipMaintain) {
					// Remove the tooltip:
					$.M_TooltipRemove();
				}
				// If so:
				else {
					// Then, only for a given duration:
					if($(this).data('M_TooltipOptions').tooltipMaintainDuration > 0) {
						_m_tooltipTimeout = setTimeout('$.M_TooltipRemove();', $(this).data('M_TooltipOptions').tooltipMaintainDuration);
					}
				}
			});
		} // END if tooltip HTML is not empty
	});
	
	// If this is the first time the tooltip is used, we set some global event
	// listeners:
	if(typeof M_TooltipInit == 'undefined') {
		// Set variable, so this piece of code gets ignored next time!
		M_TooltipInit = true;
		
		// When click anywhere in the document:
		$('body').mousedown(function($eventObject) {
			// get the tooltip:
			var $t = $('#M_Tooltip_ElementId');
			
			// Do a hitTest with the tooltip
			var h = false;
                        if($t.length > 0) {
			    h = ($eventObject.pageX >= $t.offset().left && $eventObject.pageX <= ($t.offset().left + $t.width()));
			    h = (h && $eventObject.pageY >= $t.offset().top && $eventObject.pageY <= ($t.offset().top + $t.height()));
                        }
			
			// If hitTest failed, we remove the tooltip:
			if(! h) {
				$.M_TooltipRemove();
			}
		});
	}
	
	// Get the position of a given tooltip, relative to its parent. This function
	// Will return an object with the CSS properties that should be applied. Also,
	// this function returns an animation function, in order to animate the tooltip
	// into, and out of, view
	function _getTooltipPosition($parentElement, $tooltipElement) {
		// Get options:
		var _options = $parentElement.data('M_TooltipOptions');
		
		// Get the positioning reference:
		var o = _getTooltipPositioningReference($parentElement, $tooltipElement);
		
		// By default, the tooltip appears on the right hand side of the triggering
		// element. However, this might change if the tooltip would not enter in
		// the current viewport. We default the X-Y Coordinates:
		var x = $parentElement.offset().left + $parentElement.width(); // right hand side of triggering element
		var y = $parentElement.offset().top; // same y as triggering element
		var w = 'right';
		
		// We also default the animation specs:
		var funcIn = function() {
			if(_options.tooltipAnimate) {
				//@todo: removed slide effect: caused bug in IE
				if($.browser.msie) {
					$tooltipElement.stop().show(_options.tooltipAnimationDuration);
				}else {
					$tooltipElement.stop().show("slide", { direction: "left" }, _options.tooltipAnimationDuration);
				}
			} else {
				$tooltipElement.show();
			}
		}
		
		// Is there space on the right hand side?
		if((x + $tooltipElement.width()) > o.x.width) {
			// No? then place it at the left hand side:
			x = $parentElement.offset().left - $tooltipElement.width();
			
			// Also, we animate it differently:
			funcIn = function() {
				//@todo: removed slide effect: caused bug in IE
				if($.browser.msie) {
					$tooltipElement.stop().show(_options.tooltipAnimationDuration);
				}else {
					$tooltipElement.stop().show("slide", { direction: "right" }, _options.tooltipAnimationDuration);
				}
			}
			
			// Inform where the tooltip is:
			var w = 'left';
		}
		
		// Make sure we stay inside parent
		if(y < o.y.top) {
			y = o.y.top;
		} else {
			if(y > (o.y.top + o.y.height)) {
				y = o.y.top + o.y.height - $tooltipElement.height();
			}
		}
		
		// Position the element:
		return [{ 'position' : 'absolute', 'float' : 'left', 'z-index' : 750, 'top' : y, 'left' : x }, funcIn];
	}
	
	// Get position + size of Positioning Reference Element
	function _getTooltipPositioningReference($parentElement, $tooltipElement) {
		var $p = $parentElement.data('M_TooltipOptions').tooltipPositioningReferenceX;
		var out = {
			x : null,
			y : null
		};
		
		out.x = {
			top    : $p.offset().top,
			left   : $p.offset().left,
			width  : $p.width(),
			height : $p.height()
		};
		
		$p = $parentElement.data('M_TooltipOptions').tooltipPositioningReferenceY;
		out.y = {
			top    : $p.offset().top,
			left   : $p.offset().left,
			width  : $p.width(),
			height : $p.height()
		};
		
		return out;
	}
}

// Remove tooltip
$.M_TooltipRemove = function() {
	// Remove timeout that hides the tooltip
	if(typeof _m_tooltipTimeout != 'undefined') {
		clearTimeout(_m_tooltipTimeout);
	}
	
	// Get the tooltip
	var $tooltip = $('#M_Tooltip_ElementId');
	
	// Dispatch an event;
	$(document).trigger('M_Tooltip_BeforeHide', [$('#' + $tooltip.data('parent')), $tooltip]);
	
	// Remove the tooltip
	$tooltip.remove();
	
	// Dispatch an event;
	$(document).trigger('M_Tooltip_Hide');
}

// -----------------------------------------------------------------------------
// ---- Live-list --------------------------------------------------------------
// -----------------------------------------------------------------------------
$.fn.M_LiveList = function(options) {
	// If not already activated for the element:
	if(! $(this).M_HasData('M_LiveListOptions')) {
		$.M_Console('Live List INIT');
	
		// Merge given options with the default ones:
		$(this).data('M_LiveListOptions', jQuery.extend({
			// The (estimated) total number of listed objects
			listTotalCount : 0,
			
			// AJAX Properties (to fetch more objects)
			listAjaxProperties : {},
			
			// The initial list offset
			listOffset : 0,
			
			// The template of a listed object. This template is used to show
			// a listed object that is being loaded. When such a template is 
			// visible in the list, an AJAX call is triggered to populate the
			// template with real data. An HTML string is expected here:
			listObjectTemplate : '',
			
			// Listen to scroll in...
			// (a jQuery selection is expected here)
			listScrollContainer : null,
			
			// Show scroll position when scrolling more than n pixels
			listScrollDistanceForPositionMessage : 10,
			
			// Remove invisible items?
			// Set to TRUE if you wish to remove the items that fall outside the
			// viewport, due to the event of scrolling
			listRemoveHidden : true
		}, options));
		
		// Remember the current scroll position in the list. We do this, in order
		// to be able to check the direction in which the user is scrolling through
		// the list.
		var $scroller = _getScrollContainer($(this));
		_setScrollPosition($scroller, $scroller.scrollTop(), $scroller.scrollLeft());
		
		// $t is a reference to the list
		var $t = $(this);
		$.M_Console($t);
		
		// First of all, we set the offsets of the contained list objects. For
		// each of the listed objects:
		$(this).children().each(function(i) {
			// Assign the offset number:
			$(this).data('M_LiveListOffset', ($t.data('M_LiveListOptions').listOffset + i));
		});
		
		// We listen to the scroll event. Whenever the user scrolls through the 
		// list, we check whether or not new objects should be added to the list!
		$scroller.unbind('scroll').scroll(function() {
			_onLiveListScroll($t, $(this));
		});
		
		// If the initial offset is greater than ZERO, we check if we should
		// add objects to the top of the list:
		if($t.data('M_LiveListOptions').listOffset > 0) {
			//_addListObjectTemplate($t, 'prepend');
		}
		
		// Append more objects:
		_addListObjectTemplate($t, 'append');
		
		// Also, we listen to the "M_LiveListUpdate" event. This event is triggered
		// from the outside, to provoke an update of the live list
		$t.bind('M_LiveListUpdate', function() {
			_onLiveListScroll($t, $scroller);
		});
		
		// Also, we listen to the "M_LiveListAddToTotal" event. This event is triggered
		// from the outside, to provoke an update of total count in the list
		$t.bind('M_LiveListAddToTotal', function($event, diff) {
			$t.data('M_LiveListOptions').listTotalCount += diff;
		});
		
		// Also, we listen to the "M_LiveListDeleteItem" event. This event is triggered
		// from the outside, to provoke an update of the items in the list
		$t.bind('M_LiveListDeleteItem', function($event, deletedItem) {
			// For each of the listed items:
			// (siblings of the deleted item)
			var deletedItemOffset = $(deletedItem).data('M_LiveListOffset');
			$(deletedItem).siblings().each(function() {
				// Get the offset of the current item
				var d = $(this).data('M_LiveListOffset');

				// If the offset is bigger, it means that this item is shown
				// after the deleted one. In that case:
				if(d > deletedItemOffset) {
					// We lower the offset by 1:
					$(this).data('M_LiveListOffset', d - 1);
				}
			});

			// Update the list
			$t.data('M_LiveListOptions').listTotalCount --;
			$t.trigger('M_LiveListUpdate');
		});

		// Also, we listen to an update of sorting in the list
		$t.bind('sortstart', function(event, ui) {
			$t.data('M_LiveListSortStart', $t.children().index(ui.item));
		});

		$t.bind('sortupdate', function(event, ui) {
			// We calculate the distance and direction in which the dragged item
			// has been dragged:
			var end   = $t.children().index(ui.item);
			var start = $t.data('M_LiveListSortStart');
			var diff  = end - start;

			// Prepare some working variables, depending on the direction in 
			// which we have dragged the item:
			if(diff < 0) {
				var d = 1;
				var c = function(i, start) { return (i <= start) };
				var e = end + 1;
			} else {
				var d = -1;
				var c = function(i, start) { return (i >= start) };
				var e = end - 1;
			}

			// Set the new offset of the dragged item:
			$(ui.item).attr('offset', $(ui.item).data('M_LiveListOffset') + diff);
			$(ui.item).data('M_LiveListOffset', $(ui.item).data('M_LiveListOffset') + diff);

			// We run through each of the affected items in the list:
			for(var i = e; c(i, start); i += d) {
				// Get the current item:
				var $i = $t.children(':eq('+ i +')');

				// We set the new offset for this item:
				$i.attr('offset', $i.data('M_LiveListOffset') + d);
				$i.data('M_LiveListOffset', $i.data('M_LiveListOffset') + d);
			}
		});

		//M_LiveListDeleteItem
	}
	
	// Get direction of a given scroll position
	// (used to determine the direction in which the user is scrolling through
	// the list: down/up and right/left)
	function _getScrollDirection($scrollContainer, newScrollTop, newScrollLeft) {
		var v = newScrollTop  - _getPreviousScrollPosition($scrollContainer).top;
		var h = newScrollLeft - _getPreviousScrollPosition($scrollContainer).left;
		return {
			'vertical'   : v > 0  ? 'down'  : 'up',
			'horizontal' : h > 0 ? 'right' : 'left',
			'verticalDistance' : v < 0 ? v * -1 : v,
			'horizontalDistance' : h < 0 ? h * -1 : h
		};
	}
	
	// Remember new scrolling position:
	function _setScrollPosition($scrollContainer, newScrollTop, newScrollLeft) {
		$scrollContainer.data('M_LiveListScrollPosition', {
			'top'  : newScrollTop,
			'left' : newScrollLeft
		});
	}
	
	// Remember new scrolling position:
	function _getPreviousScrollPosition($scrollContainer) {
		return $scrollContainer.data('M_LiveListScrollPosition');
	}
	
	// When scrolling through the list:
	function _onLiveListScroll($listContainer, $scrollableContainer) {
		// Get current scroll position:
		var t = $scrollableContainer.scrollTop();
		var l = $scrollableContainer.scrollLeft();
		
		// Get the direction in which the user has been scrolling:
		var d = _getScrollDirection($scrollableContainer, t, l);
		
		// Stop scrolling?
		// (boolean that indicates whether or not the scrolling should be stopped,
		// if user is scrolling too fast)
		var s = false;
		
		// If scrolling to the right, or downwards:
		if(d.vertical == 'down') { // Not needed for now: || d.horizontal == 'right') {
			// Add list object(s)
			_addListObjectTemplate($listContainer, 'append');
		}
		// If scrolling to the left:
		else if(d.vertical == 'up') { // Not needed for now: || d.horizontal == 'left') {
			// Add list object(s)
			var $listObject = _addListObjectTemplate($listContainer, 'prepend');
		}
		
		// Remove invisible items:
		if($listContainer.data('M_LiveListOptions').listRemoveHidden) {
			_removeInvisibleObjects($listContainer, d.vertical);
		}
		
		// Show current position, if scrolled at least 1 pixel
		if(d.verticalDistance > $listContainer.data('M_LiveListOptions').listScrollDistanceForPositionMessage) {
			_showCurrentPositionInList($listContainer);
		}
		
		// Dispatch LiveList_Scroll event:
		$listContainer.trigger('M_LiveList_Scroll', [d, $scrollableContainer]);
		
		// Remember new scrolling position
		_setScrollPosition($scrollableContainer, t, l);
	}
	
	// Show current position in list
	function _showCurrentPositionInList($listContainer) {
		// Get the scroll container:
		var $scrollableContainer = _getScrollContainer($listContainer);
		
		// Get listed objects:
		var c = $listContainer.children();
		var o = $scrollableContainer.offset();
		var f = 0;
		var l = 0;
		var t = 0;
		
		// Remove previously created message:
		$('#M_LiveListPositionIndicator').remove();
		
		// If at least 1 object:
		if(c.length > 0) {
			// Get first offset:
			f = $(c[0]).data('M_LiveListOffset') + 1;
			
			// Get last
			l = $(c[(c.length - 1)]).data('M_LiveListOffset') + 1;
			
			// Get total:
			t = $listContainer.data('M_LiveListOptions').listTotalCount;
			
			// Compose HTML for message:
			var $m = $('<div id="M_LiveListPositionIndicator" class="live-list-position" style="visibility: hidden; position: absolute; top: '+ Math.round(o.top + ($scrollableContainer.height() / 2)) +'px; left: '+ Math.round(o.left + ($scrollableContainer.width() / 2)) +'px">'+ f +' - ' + l + ' / '+ t +'</div>');
			
			// Show the message:
			$('body').append($m);
			
			// Position the message:
			$m.css({
				top  : Math.round(o.top  + ($scrollableContainer.height() / 2)) - ($m.height() / 2) + 'px',
				left : Math.round(o.left + ($scrollableContainer.width()  / 2)) - ($m.width() / 2)  + 'px',
				visibility : 'visible'
			})
		}
		
		// Set timeout, to remove the message:
		if(typeof M_LiveListTimeout != 'undefined') {
			clearTimeout(M_LiveListTimeout);
		}
		
		M_LiveListTimeout = setTimeout('$("#M_LiveListPositionIndicator").fadeOut(300, function() { $(this).remove(); });', 1000);
	}
	
	// Add an object to the list:
	function _addListObjectTemplate($listContainer, position) {
		// Declare some local variables:
		var newOffsetNumber;
		var $listObject;
		var $scrollContainer = _getScrollContainer($listContainer);
		var c = $listContainer.children();
		var b = false;
		
		// The new object's offset number is calculated with the position at which
		// it is going to be added to the list ("prepend" or "append").
		switch(position) {
			case 'prepend':
				// The new offset number is the offset of first one in the list,
				// minus 1
				newOffsetNumber = $(c[0]).data('M_LiveListOffset') - 1;
				
				// We will only add an object, if the first item in the list
				// is visible at the moment
				b = $(c[0]).M_IsVisible({partialHit : $scrollContainer});
				break;
			
			case 'append':
				// The new offset number is the offset of last one in the list,
				// plus 1
				newOffsetNumber = $(c[(c.length - 1)]).data('M_LiveListOffset') + 1;
				
				// We will only add an object, if the last item in the list
				// is visible at the moment
				b = $(c[(c.length - 1)]).M_IsVisible({partialHit : $scrollContainer});
				break;
		}
		
		// The new object is added to the list with a given offset number. Of course,
		// we need to check whether or not this offset falls inside the range of 
		// the total number of items in the list.
		if(b) {
			// Debugging
			$.M_Console('Yes, the first/last item is visible. Add object at offset '+ newOffsetNumber +'?');
			
			// If in range
			if(newOffsetNumber >= 0 && newOffsetNumber < $listContainer.data('M_LiveListOptions').listTotalCount) {
				// Dispatch M_LiveList_BeforeTemplateObject event:
				$listContainer.trigger('M_LiveList_BeforeTemplateObject');
				
				// Yes, the offset is valid (falls inside range)! So, now we append
				// or prepend the template object to the list. First, we create the
				// new object:
				$listObject = $($listContainer.data('M_LiveListOptions').listObjectTemplate.replace(/@averageHeight/, _getAverageHeight($listContainer)));
				
				// We set the new offset number in the object:
				$listObject.data('M_LiveListOffset', newOffsetNumber);
				
				// Append/prepend
				switch(position) {
					case 'append':
						$listContainer.append($listObject);
						break;
					
					case 'prepend':
						$listContainer.prepend($listObject);
						
						// Scroll back a bit
						$scrollContainer.scrollTop($scrollContainer.scrollTop() + _getAverageHeight($listContainer) + 10);
						break;
				}
				
				// Debugging
				$.M_Console('Added object to the list');
				
				// Dispatch M_LiveList_TemplateObject event:
				$listContainer.trigger('M_LiveList_TemplateObject', [$listObject]);
				
				// Now, we update the placeholder of absent objects:
				_setAbsentObjectsPlaceholder($t, position);
				
				// Populate the list object:
				_populateListObjectTemplate($listContainer, $listObject, position);
			}
		} else {
			$.M_Console('No objects added');
		}
	}
	
	// Populate an object template in the list:
	function _populateListObjectTemplate($listContainer, $listedObject, position) {
		// Get AJAX Properties:
		var ajaxProperties = $listContainer.data('M_LiveListOptions').listAjaxProperties;
		
		// If no AJAX Properties are available
		if(! ajaxProperties) {
			$.M_Console('Cannot populate with data; Missing option "listAjaxProperties"!');
			return;
		}
		
		// If no URL is available in the properties object:
		if(! ajaxProperties.url) {
			$.M_Console('Cannot populate with data; Missing value(s) in option "listAjaxProperties"!');
			return;
		}
		
		// Get the offset of the new object
		var o = $listedObject.data('M_LiveListOffset');
		
		// Now, we perform the AJAX call:
		$.M_AjaxRequest({
			url      : ajaxProperties.url,
			data     : jQuery.extend({ offset : o }, ajaxProperties.data),
			dataType : 'html',
			// When done
			success  : function(data) {
				// Get jQuery element out of incoming data:
				var d = $(data);
				
				// Replace the template with the final HTML (incoming data):
				$listedObject.replaceWith(d);
				
				// Set the offset number of new HTML element:
				d.data('M_LiveListOffset', o);
				
				// Now, we update the placeholder of absent objects:
				_setAbsentObjectsPlaceholder($listContainer, position);
				
				// Dispatch M_LiveList_TemplateObject event:
				$listContainer.trigger('M_LiveList_NewObject', [d]);
				
				// If the new item is visible, we create another one (in order
				// to keep populating until screen is fully populated)
				if(d.M_IsVisible({partialHit : _getScrollContainer($listContainer)})) {
					// Yes, it is visible. So, we append/prepend another object:
					_addListObjectTemplate($listContainer, position);
				}
			}
		});
	}
	
	// Removes invisible items in the list
	function _removeInvisibleObjects($listContainer, scrollDirection) {
		// Debugging
		$.M_Console('Remove invisible objects from list (direction: ' + scrollDirection + ')');
		
		// Now, we update the placeholder of absent objects:
		_setAbsentObjectsPlaceholder($t, 'prepend');
		_setAbsentObjectsPlaceholder($t, 'append');
		
		// Get the scrollable container of the list:
		var $scroller = _getScrollContainer($listContainer);
		
		// The items in the list:
		var $children = $listContainer.children();
		
		// The way in which we iterate throufh the items in the list, depends on
		// the position at which items should be removed. This position is 
		// defined by the scroll direction...
		var j = 0;
		switch(scrollDirection) {
			// If scrolling downwards
			case 'down':
				// Starting from the beginning:
				for(var i = 0; i < $children.length; i ++) {
					// If the current item is visible:
					if($($children[i]).M_IsVisible({partialHit : $scroller, partialHitTolerance : 400})) {
						// stop looping:
						break;
					}
					// If not visible:
					else {
						// Add to temp array
						_removeListObject($listContainer, $($children[i]));
						if(j ++ == 1) {
							break;
						}
					}
				}
				break;
			
			// If scrolling upwards
			case 'up':
				// Starting from the beginning:
				for(var i = $children.length - 1; i > 0; i --) {
					// If the current item is visible:
					if($($children[i]).M_IsVisible({partialHit : $scroller, partialHitTolerance : 400})) {
						// stop looping:
						break;
					}
					// If not visible:
					else {
						// Remove the item
						_removeListObject($listContainer, $($children[i]));
						if(j ++ == 1) {
							break;
						}
					}
				}
				break;
		}
	}
	
	// removes given item from list:
	function _removeListObject($listContainer, $listObject) {
		// Debugging
		$.M_Console('Remove Object at offset ' + $listObject.data('M_LiveListOffset'));
		
		// Dispatch event:
		$listContainer.trigger('M_LiveList_BeforeRemoveObject', [$listObject]);
		
		// Remove:
		$listObject.remove();
		
		// Dispatch event:
		$listContainer.trigger('M_LiveList_RemoveObject');
	}
	
	// Adds/Updates the placeholder for absent objects in the list. This placeholder is
	// given a certain height, to make the scroller bar grow/shrink
	function _setAbsentObjectsPlaceholder($listContainer, position) {
		// Prepare some local variables:
		var n = 0;
		var c = $listContainer.children();
		
		// Prepare the ID of the placeholder:
		var i = 'M_LiveListAbscentObjects_'+ position;
		
		// If the placeholder has not yet been created:
		if($('#' + i).length == 0) {
			// We do so now:
			switch(position) {
				case 'prepend':
					_getScrollContainer($listContainer).prepend('<div id="'+ i +'">');
					break;
				
				case 'append':
					_getScrollContainer($listContainer).append('<div id="'+ i +'">');
					break;
			}
		}
		
		// The placeholder's job is to occupy the place that N listed objects
		// would. The number of listed objects (not present) depends on the
		// requested position:
		switch(position) {
			case 'prepend':
				// Number of absent listed objects:
				n = c.length > 0 ? $(c[0]).data('M_LiveListOffset') - 0 : 0;
				break;
			
			case 'append':
				// Number of absent listed objects:
				n = c.length > 0 ? $listContainer.data('M_LiveListOptions').listTotalCount - $(c[(c.length - 1)]).data('M_LiveListOffset') - 1 : 0;
				break;
		}
		
		// If the number of objects is ZERO, we remove the placeholder:
		if(n <= 0) {
			$('#' + i).remove();
		}
		// If the number of objects is greater than ZERO:
		else {
			// Now, we assign the height to the placeholder, by multiplying the
			// average height with the number of absent objects in the list
			// $('#' + i).css({height : (_getAverageHeight($listContainer) * n) + 'px'});
			$('#' + i).css({ height : '100px' });
		}
	}
	
	// Get average height of items in the list
	function _getAverageHeight($listContainer) {
		// Prepare some local variables
		var h = 0, t = 0;
		
		// We calculate the average size of the items in the list:
		$listContainer.children().each(function() {
			// h is the accumulated height, t is the total number of items.
			// The average is then (h / t)
			h += $(this).height();
			t ++;
		});
		
		// return the average:
		return Math.round(h / t);
	}
	
	// Gets the DIV that is scrollable (and contains the list)
	function _getScrollContainer($listContainer) {
		var o = $listContainer.data('M_LiveListOptions');
		if(o && o.listScrollContainer) {
			var $scroller = o.listScrollContainer;
			if(! $scroller) {
				$scroller = $listContainer;
			}
			return $scroller;
		} else {
			return $listContainer;
		}
	}
}

// Check if an item passes the hit test
$.fn.M_IsVisible = function(options) {
	// Merge given options with the default ones:
	var _options = jQuery.extend({
		// Provide with an element if you want the item to be (at least partially)
		// inside that element:
		partialHit : null,
		
		// Tolerance. We can specify how many pixels you want to apply as tolerance
		// in partialHit:
		partialHitTolerance : 0,
		
		// Provide with an element if you want the item to be inside that
		// element
		fullHit : null
	}, options);
	
	// If the item is not visible, we return false
	if(! this.is(':visible')) {
		return false;
	}
	
	// If we need to check a partial hit:
	if(_options.partialHit) {
		// Get information about the position of the element to compare with
		var pPartialHit = _options.partialHit.offset();
		
		// Also, get information about the position of the item we're testing
		var pItem = this.offset();
		
		// Check the partial hit. First, we check if the left top corner is inside
		// the partial hit-test element:
		var pTop    = pPartialHit.top  - _options.partialHitTolerance;
		var pLeft   = pPartialHit.left - _options.partialHitTolerance;
		var pBottom = pPartialHit.top  + _options.partialHit.height() + _options.partialHitTolerance;
		var pRight  = pPartialHit.left + _options.partialHit.width()  + _options.partialHitTolerance;
		
		if(pItem.top >= pTop && pItem.top < pBottom && pItem.left >= pLeft && pItem.left < pRight) {
			return true;
		}
		// If that's not the case, we check the right bottom corner is inside:
		else {
			var rbcY = pItem.top  + this.height();
			var rbcX = pItem.left + this.width();
			if(rbcY >= pTop && rbcY < pBottom && rbcX >= pLeft && rbcX < pRight) {
				return true;
			}
		}
		
		// Return false, if still here:
		return false;
	}
	
	// Return TRUE, if we are still here:
	return true;
}

// Show list options
$.fn.M_ShowOptions = function() {
}

// Selectables in list
var _m_activeSelectableList;
var _m_activeSelectableItem;
$.fn.M_Selectables = function(options) {
	// If not yet applied:
	if(! $(this).data('M_Selectables')) {
		// Set the options:
		$(this).data('M_Selectables', jQuery.extend({
			// selected class:
			selectedClass : 'selected',
			// Number of selected items:
			selectedCount : 0,
			// Make active list of selectables?
			makeActiveList : true
		}));
		
		// If latest active selectable list is not yet defined:
		if(typeof _m_activeSelectableList == 'undefined' || $(this).data('M_Selectables').makeActiveList) {
			_m_activeSelectableList = $(this);
		}
		
		// For each of the children (selectables)
		var $t = $(this);
		$t.children().each(function() {
			// Prevent text selection
			if($.browser.mozilla) { //Firefox
				$(this).css('MozUserSelect','none');
			} else if($.browser.msie) { //IE
				$(this).bind('selectstart',function(){return false;});
			} else { //Opera, etc.
				$(this).mousedown(function(){return false;});
			}
			
			// Set initial state:
			var $c = $(this);
			$c.data('M_Selectables_Selected', false);
			
			// If no item has been marked as active yet:
			if(typeof _m_activeSelectableItem == 'undefined') {
				_m_activeSelectableItem = $c;
			}
			
			// When clicked on:
			$c.click(function($event) {
				// Check if a link has been hit (will get preference over selectable)
				var isLinkHit = false;
				$(this).find('a').each(function() {
					if(! isLinkHit) {
						var o = $(this).offset();
						if($event.pageX > o.left && $event.pageY > o.top) {
							if($event.pageX < (o.left + $(this).width()) && $event.pageY < (o.top + $(this).height())) {
								isLinkHit = true;
							}
						}
					}
				});
				
				// if so, we do nothing
				if(isLinkHit) {
					return true;
				}
				
				// Get the selectables:
				var $c = $.M_SelectablesActiveList().children();
				
				// Get index of previous selection
				var pi = $c.index($.M_SelectablesActiveItem());
				
				// Toggle selectable
				var s = $(this).M_SelectableToggle();
				
				// Get index of new selection
				var ni = $c.index($.M_SelectablesActiveItem());
				
				// If the SHIFT is being pressed:
				if(s && $event.shiftKey) {
					// Then, we also select all items between previous and new:
					if(!isNaN(pi) &&!isNaN(ni)) {
						while(pi != ni) {
							if(s) {
								$($c[pi]).M_SelectableSelect();
							} else {
								$($c[pi]).M_SelectableUnselect();
							}
							if(pi < ni) {
								pi ++;
							} else {
								pi --;
							}
						}
					}
				}
				
				// Set latest active selectable list:
				_m_activeSelectableList = $t;
			});
		});
		
		return $(this);
	}
}

// Get latest active selectable list
$.M_SelectablesActiveList = function() {
	return _m_activeSelectableList;
}

// Get latest active selectable item
$.M_SelectablesActiveItem = function() {
	return _m_activeSelectableItem;
}

// Toggle a selectable (switch). Returns TRUE is selected, FALSE if not
$.fn.M_SelectableToggle = function() {
	// If the item is not selected:
	if(! $(this).data('M_Selectables_Selected')) {
		// Select it:
		$(this).M_SelectableSelect();
		return true;
	}
	// If now selected:
	else {
		// De-select it:
		$(this).M_SelectableUnselect();
		return false;
	}
}

// Select a selectable:
$.fn.M_SelectableSelect = function() {
	// If the parent holds selectables:
	var $p = $(this).parent();
	if($p.data('M_Selectables') && ! $(this).data('M_Selectables_Selected')) {
		// Select the item:
		$(this).data('M_Selectables_Selected', true);
		
		if(! $(this).data('M_Selectables_Class_Backup')) {
			$(this).data('M_Selectables_Class_Backup', $(this).attr('class'));
		}
		
		$(this).attr('class', $p.data('M_Selectables').selectedClass);
		
		// Increment number of selected items:
		$p.data('M_Selectables').selectedCount ++;
		
		// Set this item as the active item:
		_m_activeSelectableItem = $(this);
		
		// Dispatch event:
		$p.trigger('M_Selectables_Select', [$(this), $p.data('M_Selectables').selectedCount]);
	}
}

// Unselect a selectable:
$.fn.M_SelectableUnselect = function() {
	// If the parent holds selectables:
	var $p = $(this).parent();
	if($p.data('M_Selectables') && $(this).data('M_Selectables_Selected')) {
		// Unselect the item:
		$(this).data('M_Selectables_Selected', false);
		$(this).attr('class', $(this).data('M_Selectables_Class_Backup'));
		
		// Reduce number of selected items:
		$p.data('M_Selectables').selectedCount --;
		
		// Set this item as the active item:
		_m_activeSelectableItem = $(this);
		
		// Dispatch event:
		$p.trigger('M_Selectables_Unselect', [$(this), $p.data('M_Selectables').selectedCount]);
	}
}

// Clear selection of selectables
$.fn.M_SelectionSelectAll = function() {
	// If M_Selectables has been applied to the element(s)
	if($(this).data('M_Selectables')) {
		// For each of the selectables:
		var $t = $(this);
		$t.children().each(function() {
			// If the item is not selected:
			if(! $(this).data('M_Selectables_Selected')) {
				// Select it:
				$(this).M_SelectableSelect();
			}
		});
	}
}

// Clear selection of selectables
$.fn.M_SelectionClear = function() {
	// If M_Selectables has been applied to the element(s)
	if($(this).data('M_Selectables')) {
		// For each of the selectables:
		var $t = $(this);
		$t.children().each(function() {
			// If the item is selected:
			var $c = $(this);
			if($c.data('M_Selectables_Selected')) {
				// Select it:
				$c.click();
			}
		});
	}
}

// Inverse selection of selectables
$.fn.M_SelectionInverse = function() {
	if($(this).data('M_Selectables')) {
		$(this).children().click();
	}
}

// Get selection in list with selectables
$.fn.M_Selection = function() {
	// If M_Selectables has been applied to the element(s)
	if($(this).data('M_Selectables')) {
		// Get matched element(s):
		var $t = $(this);
		
		// Prepare output variable:
		var o = new Array();
		
		// For each of the children (selectables)
		$t.children().each(function() {
			// If selected:
			if($(this).data('M_Selectables_Selected')) {
				// Add ID to output
				o.push($(this).M_AssignId().attr('id'));
			}
		});
	}
	
	// return output
	return o;
}

$.fn.M_defaultvalue = function(options) {
	var _options = jQuery.extend({
		defaultValueClass : "with-default-value",
		defaultValueInputType : 'text',
		inputType : 'text'
	}, options);
	
	var _matchedElements = this;
	
	// Actions for matched elements:
	_matchedElements.each(function() {
		// Set the class for the default value:
		if($(this).val() == _getDef($(this))) {
			$(this).addClass(_options.defaultValueClass);
			_setInputType($(this), _options.defaultValueInputType);
		}
		
		// Set event listeners:
		// - Remove default value, when the field is focussed:
		$(this).focus(function() {
			$(this).removeClass(_options.defaultValueClass);
			if($(this).val() == _getDef($(this))) {
				$(this).val('');
				_setInputType($(this), _options.inputType);
			}
		});
		
		// - Restore default value, if blurred and left empty:
		$(this).blur(function() {
			if(jQuery.trim($(this).val()) == '') {
				$(this).val(_getDef($(this)));
				$(this).addClass(_options.defaultValueClass);
				_setInputType($(this), _options.defaultValueInputType);
			}
		});
	});
	
	// Function to get default value
	function _getDef($input) {
		return document.getElementById($input.attr('id')).defaultValue;
	}
	
	// Function to change type of input field:
	function _setInputType($input, inputType) {
		document.getElementById($input.attr('id')).type = inputType;
	}
}

/**
 * Suckerfish menu
 *
 * Will handle suckerfish menu's in the website. A lookup of trigger elements, and
 * a call to this plugin function, will do the job :)
 *
 * @return $(matchedElements)
 */
$.fn.M_Suckerfish = function(options) {
	// Z-Index Counter
	zIndexCounter = 200;

	// Define the default option values for the suckerfish menu
	var sfOptions = jQuery.extend({
		// Define the event that triggers the suckerfish menu. Possible values
		// are "hover", "click"
		trigger  : 'hover',
		// The callback function that is to be called when the suckerfish menu
		// is shown:
		onShow   : function($triggerElement, $submenu) {},
		// The callback function that is to be called when the suckerfish menu
		// is hidden:
		onHide   : function($triggerElement, $submenu) {},
		// Callback that is used to get the suckerfish menu. By default, we define
		// a function that looks for an UL tag as a child of the trigger element:
		onGetSf  : function($triggerElement) {
			// Return the UL tag:
			return $triggerElement.children('ul');
		},
		// The duration of animation:
		duration : 200,
		// The margin to be added to the Y-axis of the suckerfish (can be negative)
		offsetY  : 0,
		// The margin to be added to the X-axis of the suckerfish (can be negative)
		offsetX  : 0

	// Merge them with the provided option values:
	}, options);

	// We define the actions for the matched element(s). Note that, in order to
	// do so, we first check the type of event that has been requested:
	switch(sfOptions.trigger) {
		// If the suckerfish is to be shown when hovering the element
		case 'hover':

			// For this plugin to work with the "hover" event, we depend on the
			// hoverIntent plugin to be installed:
			if(typeof $.fn.hoverIntent == 'undefined') {
				// We inform about error, if that plugin is not available:
				alert('$.fn.M_Suckerfish cannot function without jQuery hoverIntent Plugin');
				return;
			}

			// Activate the hoverIntent on the matched element(s)
			$(this).hoverIntent({
				// Define the timeout for the hover intent to 300 milliseconds
				timeout: 150,

				// When a main menu item is being hovered:
				over: function() {
					// Get the corresponding submenu:
					var $ul = sfOptions.onGetSf($(this));

					// If the submenu has been found:
					if($ul.length > 0) {
						
						// Get the position of the hovered link
						var o = $(this).offset();

						// Position the submenu with the position of the hovered
						// link, and fade in...
						$ul.css({
							position  : 'absolute',
							top       : (o.top + $(this).height() + parseInt(sfOptions.offsetY)) + 'px',
							left      : (o.left + parseInt(sfOptions.offsetX)) + 'px',
							'z-index' : ++ zIndexCounter
						});

						if(! $.browser.msie) {
							$ul.fadeIn(sfOptions.duration);
						} else {
							$ul.show();
						}

						// If a callback function has been defined:
						if(typeof sfOptions.onShow == 'function') {
							// Then, we call it now:
							sfOptions.onShow($(this), $ul);
						}
					}
				},
				// When the mouse cursor is removed from the main menu item
				out: function() {
					// Get the corresponding submenu:
					var $ul = sfOptions.onGetSf($(this));

					// If the submenu has been found:
					if($ul.length > 0) {
						// Reference to matching element:
						var $t = $(this);

						// Hide the submenu. Note that in IE we do not animate,
						// in order to avoid crappy font rendering :(
						if(! $.browser.msie) {
							$ul.fadeOut(sfOptions.duration, function() {
								// If a callback function has been defined:
								if(typeof sfOptions.onHide == 'function') {
									// Then, we call it now:
									sfOptions.onHide($t, $ul);
								}
							});
						}
						// In IE, we simply hide:
						else {
							$ul.hide();

							// If a callback function has been defined:
							if(typeof sfOptions.onHide == 'function') {
								// Then, we call it now:
								sfOptions.onHide($t, $ul);
							}
						}
					}
				}
			});

			// Done with event type "hover"
			break;
		
		// If the suckerfish is to be shown when clicking the element
		case 'click':
			// Register an event listener for the click event
			$(this).click(function($e) {
				// Reference to matching element:
				var $t = $(this);
				
				// Get the corresponding submenu:
				var $ul = sfOptions.onGetSf($(this));
				
				// If the click was registered on the suckerfish menu (may be a
				// child of the original trigger element):
				if(($ul.M_HitTestXY($e.pageX, $e.pageY))) {
					// Then, we do nothing at all:
					// return false;
				}

				// If the submenu has been found:
				if($ul.length > 0) {
					// Get the position of the hovered link
					var o = $(this).offset();

					// Position the submenu with the position of the hovered
					// link, with CSS
					var ulH  = $ul.height();
					var topB = (o.top + $(this).height() + parseInt(sfOptions.offsetY));
					var topT = (o.top - $(this).height() - parseInt(sfOptions.offsetY) - ulH);
					$ul.css({
						position  : 'absolute',
						top       : (topB + ulH <= $(window).height() || topT < 0) ? topB + 'px' : topT + 'px',
						left      : (o.left + parseInt(sfOptions.offsetX)) + 'px',
						'z-index' : ++ zIndexCounter

					// Fade in the submenu:
					}).fadeIn(sfOptions.duration);

					// If a callback function has been defined:
					if(typeof sfOptions.onShow == 'function') {
						// Then, we call it now:
						sfOptions.onShow($t, $ul);
					}

					// Set the active suckerfish menu now, and the element that
					// triggered the suckerfish menu to be shown in the page:
					$sfActive = $ul;
					$sfActiveTrigger = $(this);
					
					// We are overriding the default behavior of the link, so
					// we return FALSE:
					return false;
				}
			});

			// We register an event listener on any click in the document.
			$(document).unbind('mousedown').mousedown(function($e) {
				
				//if no menu is active, we stop
				if (typeof $sfActive == "undefined") {
					return true;
				}
				
				// If a suckerfish menu is showing at the moment
				// We hide the suckerfish as a reaction to the click:
				$sfActive.fadeOut(sfOptions.duration);

				// If a callback function has been defined:
				if(typeof sfOptions.onHide == 'function') {
					// Then, we call it now:
					sfOptions.onHide($sfActiveTrigger, $sfActive);
				}

				// And, reset working variables:
				$sfActive = undefined;
				$sfActiveTrigger = undefined;
			});
			
			// Done with event type "click"
			break;
	}
}

/**
 * Perform hit test, with X- and Y-coordinates
 *
 * @return bool
 *		Returns true if a hit has been detected, false if not
 */
$.fn.M_HitTestXY = function(x, y) {
	// Prepare the output variable:
	var h = false;

	// Get the matched element's offset
	var o = $(this).offset();

	if(o != null) {
		// Perform the hit test,
		// - On the X-axis:
		h = (x >= o.left && x <= (o.left + $(this).width()));
		// - And on the Y-axis:
		h = (h && y >= o.top && y <= (o.top + $(this).height()));

		// Return the result of the hit test:
		return h;
	}

	return false;
}