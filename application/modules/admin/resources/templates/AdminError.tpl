<div class="padding-from-frame">
	<h2 class="big">
		{t text="Oops... something went wrong"}
	</h2>
	<p class="longtext">
		{t text="We are very sorry: the page you requested can't be displayed. We will try to fix this as soon as possible."}
	</p>
</div>