<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{include file="_AdminPageHead.tpl"}
</head>
<body>
	<div id="header">
		<h1 id="idButtonLogo"><span>Multimanage</span></h1>
		{if $logo}<div id="appLogo"><img src="{link href="$logo" prefix="false"}"  alt="logo" /></div>{/if}
		<div id="my-menu">
			{t text="Welcome <strong>@name</strong>" name=$administrator->getFirstname()}
			<ul>
				<li><a href="{link href="admin/my-account"}" id="idButtonMyAccount" class="left button-header" title="{t|htmlentities text="My Account"}"><span>{t text="My Account"}</span></a></li>
				<li><a href="{link href="admin/logout"}" id="idButtonLogout" class="left button-logout" title="{t|htmlentities text="Close session"}"><span>{t text="Close session"}</span></a></li>
			</ul>
		</div>
	</div>
	{if $messages|@count > 0}
	<div class="messages">
		{foreach from=$messages item="message"}
			<div class="message-row">
				<a href="#" class="message-close" title="{t|att text="Hide this message"}">{t text="Hide this message"}</a>
				<strong>{$message->title}</strong>
				{$message->description}
			</div>
		{/foreach}
	</div>
	{/if}
	
	{config var="menuStyle" module="admin" assignto=menuStyle default="vertical"}
	<div class="source-pane{if $menuStyle == 'vertical'} fill-remaining-vspace{/if} {$menuStyle}" id="idSourcePane">
		
		{* Show filter menu? *}
		{if $showFilterMenu}
			<div id="filtermenu-wrapper">
				<form method="get" action=""><input id="filtermenu" type="text" title="{t text="Search modules..."}" value="" /></form>
				<a id="filtermenu-clear" style="display:none;" href="#">clear</a>
				<div class="clear"><!-- --></div>
			</div>
		{/if}
		<div class="source-pane-viewport fill-remaining-vspace" id="idSourcePaneViewport">
			{foreach from=$sourcePaneMenu item="menu"}
				{if $menu->getNumberOfMenuItems() > 0}
					{if $menuStyle == 'horizontal'}
						{include file="_AdminMenuHorizontal.tpl"}
					{elseif $menuStyle == 'vertical'}
						{include file="_AdminMenuVertical.tpl"}
					{/if}
				{/if}
			{/foreach}
		</div>
		
		{* Only show on vertical menu *}
		{if $menuStyle == 'vertical'}
			<div class="menu-sticky-bottom"><a href="#" id="idPlusButton" class="button-plus popmenu">+</a></div>
		{/if}
	</div>
	<div class="content-pane fill-remaining-vspace {$menuStyle}" id="idContentPane">
		{$content}
	</div>
	
	{* Only show menu on vertical menu *}
	{if $menuStyle == 'vertical'}
	
		{* Contextual menu; activated by jQuery M_Popmenu *}
		<div id="idPlusButtonPopmenu" class="hidden">
			<ul>
				<li><a href="#" id="idButtonNewSmartFolder">{t text="Create a new Smart Folder"}</a></li>
			</ul>
			{foreach from=$plusMenu item="menu"}
				{if $menu->getNumberOfMenuItems() > 0}
					<ul{if $menu->getTitle()} title="{$menu->getTitle()|att}"{/if}>
						{foreach from=$menu->getMenuItems() item="menuItem"}
							<li{if $menuItem->isActive()} class="active"{/if}><a href="{link href=$menuItem->getPath()}" title="{$menuItem->getDescription()|att}">{$menuItem->getTitle()}</a></li>
						{/foreach}
					</ul>
				{/if}
			{/foreach}
			<div class="clear"><!-- --></div>
		</div>
	{/if}

	{* Javascript resources; to build interactive interface *}
	{$viewJavascript}

</body>
</html>