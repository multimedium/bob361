{* If a file preview is given *}
{if isset($preview) && $preview && ! $field->getErrorMessage()}
	{$preview}
{/if}

{* Add the upload field *}
<input type="file" name="{$field->getId()}" id="{$field->getId()}"/>

{* If a value is already given *}
{if $field->getValue()}
<input type="hidden" name="{$field->getId()}_hidden" value="{$field->getValue()}"/>
{/if}

{* If an original filename is already available *}
{if $field->getFilenameOriginal()}
<input type="hidden" name="{$field->getId()}_filenameOriginal_hidden" value="{$field->getFilenameOriginal()}" />
{/if}