<input type="text" 
	   id="{$viewId}"
	   name="{$field->getId()}"
	   value="{$field->getValue()}"
	   class="field-text field-uri-path-element"
	   {if $field->getReadonly()}readonly="readonly" {/if}
	   {if $field->getMaxlength()}maxlength="{$field->getMaxlength()}" {/if}
	   {if $field->getHint()}title="{$field->getHint()}" {/if}
	   style="width: {if $field->getWidth()}{$field->getWidth()}px{else}200px{/if}"  />

{if $field->getSourceFieldId()}
<input type="hidden" id="{$viewId}-source-field-id" class="field-uri-path-element-source-field-id" value="{$field->getSourceFieldId()}" />
{*<a href="#" class="field-uri-path-element-update">{t text="update"}</a>*}
{/if}
<input type="hidden" id="{$viewId}-maximum-amount-of-words" class="field-uri-path-element-maximum-amount-of-words" value="{$field->getMaximumAmountOfWords()}" />