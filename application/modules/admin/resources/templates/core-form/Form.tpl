{if $form->getErrorMessage()}
	<div class="error-message">
		{$form->getErrorMessage()}
	</div>
{/if}

<form id="{$viewId}"{foreach from=$form->getProperties() key="name" item="value"} {$name}="{$value}"{/foreach}>
	{foreach from=$form->getVariables() key="name" item="value"}
		{if $value|is_array}
			{foreach from=$value key="key" item="arrayValue"}
				<input type="hidden" name="{$name}[{$key}]" value="{$arrayValue}" />
			{/foreach}
		{else}
			<input type="hidden" name="{$name}" value="{$value}" />
		{/if}
	{/foreach}
	
	{* Contains all the field-groups *}
	{assign var="fieldGroups" value=$form->getFieldGroups()}
	{* Contains all the fields which are member of a group *}
	{assign var="groupFields" value=$form->getGroupFields()}
	
	{* Render fields which are not part of a field-group *}
	{foreach from=$form->getFields(false) key="name" item="field"}
		{if $groupFields->offsetExists($field->getId()) == false}
			{$field->setModuleOwner('admin')}
			{$field->getView()}
		{/if}
	{/foreach}

	{* Render field groups *}
	{foreach from=$fieldGroups item="group"}
		{$group->setModuleOwner('admin')}
		{$group->getView()}
	{/foreach}
	
	<div class="field-row-buttons">
		<label class="left button-submit"><input type="submit" name="submit-{$viewId}" value="{$submitButtonLabel|att}" /><span>{$submitButtonLabel}</span></label>
		{foreach from=$submitButtons item=b name="submitButtons"}
		<label class="left button button-submit-additional"><input name="submit-{$viewId}-{$smarty.foreach.submitButtons.iteration}" type="submit" value="{$b.value}" /><span>{$b.title}</span></label>
		{/foreach}
		{foreach from=$buttons item=b name="buttons"}
		<a href="{$b.href}" class="button-additional button left"><span>{$b.title}</span></a>
		{/foreach}
		<div class="clear"><!-- --></div>
	</div>
</form>