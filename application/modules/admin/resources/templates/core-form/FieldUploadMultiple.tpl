<div id="error" style="display:none;" class="error-message"><!--  --></div>

{jsfile file="core/_thirdparty/uploadify/swfobject.js"}
{jsfile file="core/_thirdparty/uploadify/jquery.uploadify.min.js"}

{cssfile file="core/_thirdparty/uploadify/default.css"}
{cssfile file="core/_thirdparty/uploadify/uploadify.css"}

{link prefix="false" href="core/_thirdparty/uploadify" assignto='uploadify'}

{capture name="inlineUploadify"}
	{literal}
		$(document).ready(function() {

			// If there is already html in the fileQueue: add class!
			if($.trim($('#fileQueue').html()) != '') {
				$('#fileQueue').addClass('uploadifyFileQueue');
			}

			// On submit, we post the queued elements and the html, in order to display when the form is not validated successfully
			$('.field-row-buttons .button-submit').click(function() {
				$('#fileQueueElementsTextarea').val($('#fileQueueElements').html());
				$('#fileQueueHtmlTextarea').val($('#fileQueue').html());
			});

			// Initialize the uploadify jQuery plugin in order to upload multiple
			// files
			$('#uploadWrapper').uploadify({
				{/literal}
					'swf': '{$uploadify}/uploadify.swf',
					'uploader': '{link}/{$field->getProcessUrl()}',
					'formData': {$field->getProcessData()},
					'queueID': 'fileQueue',
					'buttonImage': '{$uploadify}/addfiles.jpg',
					'multi': true,
					'auto': true,
					'fileTypeDesc': '{foreach from=$field->getAllowedExtensions() item='ext' name='ext1'}{$ext}{if !$smarty.foreach.ext1.last}, {/if}{/foreach}',
					'fileTypeExts': '{foreach from=$field->getAllowedExtensions() item='ext' name='ext2'}*.{$ext}{if !$smarty.foreach.ext2.last};{/if}{/foreach}',
					'sizeLimit': {$field->getMaxFileSize()},
					'buttonText': '{t text="Add files"}',
					'onUploadSuccess': onUploadComplete,
					'onQueueComplete': onUploadCompleteAll,
					'onUploadStart': onUploadStart,
					'onSelectError': onErrorEvent,
					'removeCompleted' : false,
					'width' : 101
				{literal}
			});

			// Event handler when an upload is completed
		   function onUploadComplete(file, response, succes) {
				var pos = response.indexOf('<results>');
				var queueID = 'fileQueue';

				// Check if we get a valid response
				if(pos != -1) {
					response = parseXml(response);

					var success = $(response).find('success').text();

					if(success == 'true') {
						// When an upload is completed, we add an hidden field to handle the media-items later
						$('#fileQueueElements').append(
							'<input type="hidden" id="upload_' + queueID + '" name="{/literal}{$field->getId()}{literal}[]" value="' + $(response).find('file').text() + ', ' + $(response).find('original').text() + '" />'
						);

						var element = $('#uploadWrapper' + queueID);

						// Replace cancel button
						//replaceCancelButton(element, queueID);
					} else {
						// If we do not get a false response: display error!
						{/literal}
							var msg = $(response).find('error').text();
						{literal}

						displayError(queueID, msg);
					}
				} else {
					// If we do not get a valid response: display error!
					// Probabily the response is a Fatal Error, this error was caused by a memory size error
					{/literal}
					var msg = '{t text="Error: problems with handling the file."}';
					{literal}

					displayError(queueID, msg);
				}
			}

			// Event handler when an error occurs
			function onErrorEvent(event, queueID ,fileObj, errorObj) {
			   var msg;
				if (errorObj.status == 404) {
				   {/literal}
					  msg = '{t text="Error 404"}';
				   {literal}
				} else if (errorObj.type === "HTTP")
				   msg = errorObj.type+": "+errorObj.status;
				else if (errorObj.type ==="File Size")
				  // msg = fileObj.name+'<br>'+errorObj.type+' Limit: '+Math.round(errorObj.sizeLimit/1024)+'KB';
				   {/literal}
				   msg = '{t text="File size is too big"}';
				   {literal}
				else
				   msg = errorObj.type+": "+errorObj.text;

				displayError(queueID, msg);

				return false;
			}

			// Display the error on the screen
			function displayError(queueID, msg) {
				var filename = $('.fileName', '#uploadWrapper' + queueID).html();

				$('#uploadWrapper' + queueID).hide();

			   //alert(queueID);
				 /*setTimeout(function() {
				   $('#uploadWrapper').uploadifyCancel(queueID);
				 } , 1000);*/


				var errorElement = $('<div class="uploadifyQueueItem uploadifyError">\
					   <span class="fileName">' + filename + '</span>\
					   <span class="percentage"> - ' + msg + '</span>\
					   <div class="clear"><!-- --></div>\
					</div>');

				errorElement.appendTo('#fileQueue');
			}

			// Event handler when the total upload queue is completed:
			function onUploadCompleteAll() {
				{/literal}
					// Display text
					$('#container-{$field->getId()} .note').html('{t text="Upload complete: you can add extra files or click the <strong>send-button</strong> to generate the items on the server."}');
				{literal}
				// Show submit button
				$('.field-row-buttons').show();
			}

			// Event handler when the upload is started
			function onUploadStart() {
				$('#fileQueue').addClass('uploadifyFileQueue');
				$('.field-row-buttons').hide();
				// Change text
				$('#container-{/literal}{$field->getId()}{literal} .note').html('{/literal}{t text="Please wait, uploading..."}{literal}');
			}

			// Replace the cancel button by a custom cancel button when the upload is already completed
			function replaceCancelButton(element, queueID) {
				{/literal}
					$('.cancel', element).html('<a href="#" id="removeUploadedImage_'+ queueID +'"><img src="' + '{$uploadify}' + '/cancel.png" alt="" border="0" /></a>');
				{literal}

				// Remove the uploaded item
				$('#removeUploadedImage_' + queueID).click(function(e) {
					e.preventDefault();
					$('#upload_' + queueID).remove();
					$('#uploadWrapper' + queueID).fadeOut(250, function() { $(this).remove(); });
				});
			}

		// Parse response, in ie
		function parseXml(xml) {
			if (jQuery.browser.msie) {
				var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.loadXML(xml);
				xml = xmlDoc;
			}
			return xml;
		}

			$('.uploadifyQueueItem', '#fileQueue').each(function() {
				var qID = $(this).attr('id').replace('uploadWrapper', '');
				replaceCancelButton($(this), qID);
			});
		});

	{/literal}
{/capture}
{jsinline script=$smarty.capture.inlineUploadify}

<div id="fileQueue">
    {* File Queue HTML is not empty when the form is posted, but not validated yet *}
    {$field->getFileQueueHtml()}
</div>
<div id="uploadWrapper"></div>

<div id="fileQueueElements">
    {* File Queue is not empty when the form is posted, but not validated yet *}
    {$field->getFileQueueElements()}
</div>


{* We use the textarea's to post the fileQueue items and HTML *}
<div style="display:none;">
    <textarea id="fileQueueElementsTextarea" name="fileQueueElements"></textarea>
    <textarea id="fileQueueHtmlTextarea" name="fileQueueHtml"></textarea>
</div>
