<select size="1">
	{foreach from=$definition->getSupportedValues() item="title" key="value"}
		<option value="{$value}"{if $value == $definition->getValue()} selected="selected"{/if}>{t text=$title}</option>
	{/foreach}
</select>