<select size="1">
	{foreach from=$definition->getSupportedComparisonOperators() item="title" key="value"}
		<option value="{$value}"{if $value == $definition->getComparisonOperator()} selected="selected"{/if}>
			{t text=$title}
		</option>
	{/foreach}
</select>