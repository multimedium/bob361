{assign var="module" value=$definition->getModule()}
{assign var="isSortable" value=$definition->isInstancesDraggableForSorting($filters)}
<div class="list">
	<table cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th class="tooltip"></th>
				{foreach from=$columns item="column" name="j"}
					<th{if $smarty.foreach.j.first && $isSortable} class="draggable"{/if}{if $column->getWidth()} width="{$column->getWidth()}"{/if}{if $column->getType() == 'locale'} class="locale"{/if}>
						{t text=$column->getTitle()}
					</th>
				{/foreach}
			</tr>
		</thead>
		<tbody>
			{include file="AdminListInstanceRow.tpl"}
		</tbody>
	</table>
</div>

{if $instances|@count > 0}
	<script type="text/javascript" language="Javascript">
	{literal}
		// Function to update fixed table head
		function setFixedTableHeadInList() {
			var c = ($.browser.msie && $.browser.version.substring(0, 1) == '7');
			if(! c) {
				$('.filters').height(86);
				$('.list table thead').css({ position : 'absolute', top: '46px', left: '0px', width : $('#idContentPaneViewport').width() + 'px' });

				// Look up the headers that have a predefined width
				var $th = $('.list table th[width]');

				// Apply the same width to corresponding cells in table:
				tw = 0;
				$th.each(function(i) {
					var w = $(this).attr('width');
					$('.list table td:eq('+ ($(this).index() - 1) +')').attr('width', w);
					tw += parseInt(w);
				});

				// Look up the headers that do not have a fixed width:
				var $thNoWidth = $('.list table th:gt(0):not([width])');
				var w = Math.round(($('#idContentPaneViewport').width() - tw) / $thNoWidth.length);

				// For each of the headers with no fixed, predefined width:
				$thNoWidth.each(function(i) {
					// We apply this width (via CSS) to the header and to the
					// corresponding cells in the table:
					$(this).css('width', w + 'px');
					$('.list table td:eq('+ ($(this).index() - 1) +')').css('width', w + 'px');
				});
			}
		}

		// When a window is resized:
		/*
		if(! $(window).M_HasData('FixedTableHead')) {
			$(window).data('FixedTableHead', 1);
			$(window).resize(function() {
				// Again, set fixed table head
				setFixedTableHeadInList();
			});
		}

		// When a new object is added to the list, we also update the fixed table head
		$('#idContentPaneViewport .list table tbody').bind('M_LiveList_TemplateObject', function() {
			setFixedTableHeadInList();
		});
		
		$('#idContentPaneViewport .list table tbody').bind('M_LiveList_NewObject', function() {
			setFixedTableHeadInList();
		});
		 */

		// Tooltips
		$(document).ready(function() {
		
			//only apply tooltip if we aren't using horizontal menu
			if(!$('#idSourcePane').hasClass('horizontal')) {
				$('.list td.tooltip').M_Tooltip({
					tooltipClass : 'row-options-tooltip',
					tooltipPositioningReferenceX : $('#idContentPaneViewport'),
					tooltipPositioningReferenceY : $('#idContentPaneViewport')
				});
			}

			// We create a fixed table head
			// (we wait until tooltips are initiated, because the tooltips will
			// affect the presence of th and td)
			/* setFixedTableHeadInList(); */
		});
	
		// Init magic CSS, when tooltip is shown:
		$(document).bind('M_Tooltip_Show', function($eventObject, $parentElement, $tooltipElement) {
			initMagicCss($tooltipElement, { fillRemainingVspace : 'ignore' });
		});
	
		// Set Live List (updated via ajax, when scrolling)
		var u = '{/literal}{urlcopy queryVariableSeparator="&"}{literal}';
		u = u.replace(/\/overview-ajax\//, '/instance-row/');
		u = u.replace(/\/overview\//, '/instance-row/');
		u = u.replace(/\/instances-smart-folder\//, '/instance-row-smart-folder/');
		u = u.replace(/\/instances\//, '/instance-row/');
		u = u.replace(/\/smart-folder-ajax\//, '/instance-row-smart-folder/');
		u = u.replace(/\/smart-folder\//, '/instance-row-smart-folder/');
		
		$('#idContentPaneViewport .list table tbody').M_LiveList({
			listScrollContainer : $('#idContentPaneViewport'),
			listObjectTemplate  : '<tr><td colspan="{/literal}{($columns|count)+1}{literal}" class="loading" style="height: @averageHeightpx;">Loading</td></tr>',
			listTotalCount      : {/literal}{$pagejumper->getTotalCount()}{literal},
			listAjaxProperties  : {
				url : u
			}
		});
	
		// When new objects are added to the list (Live list):
		$('#idContentPaneViewport .list table tbody').bind('M_LiveList_NewObject', function($eventObject, $newObject) {
			// Apply the tooltip, if we aren't using horizontal menu'
			if (!$('#idSourcePane').hasClass('horizontal')) {
				$('td.tooltip', $newObject).M_Tooltip({
					tooltipClass : 'row-options-tooltip',
					tooltipPositioningReferenceX : $('#idContentPaneViewport'),
					tooltipPositioningReferenceY : $('#idContentPaneViewport')
				});
			}
	
			// Apply row classes, depending on iterations of each object in the list
			$('#idContentPaneViewport .list table tbody').children().each(function(i) {
				$(this).attr('class', (i % 2 == 0) ? 'row1' : 'row2');
			});
			
			// Apply magic css
			initMagicCss($newObject);
		});

		// Selectables
		{/literal}{* $('.list table tbody').M_Selectables().bind('M_Selectables_Select', function($event, $selectedElement, selectedCount) {
			if(selectedCount == 1) {
				// Get container of list options
				var $optionsContainer = $('#{/literal}{$module->getId()}-{$definition->getDataObjectId()}{literal}-list-options');
				
				// Backup list options properties:
				if(typeof $optionsContainer.data('__bk') == 'undefined') {
					$optionsContainer.data('__bk', {
						height : $optionsContainer.height()
					});
				}

				// Animate to new height, and set new options:
				$optionsContainer.stop().animate({height: '150px'}, function() {
					// Prepare the HTML of the new options:
					var h = '';
					h += '<div id="selectables-options">';
					h +=    '<h2>Selection</h2>';
					h +=    '<p>You have made a selection of items. What do you want to do with your selection?</p>';
					h +=    '<a href="#" class="button left" id="selectables-option-delete"><span>Delete items</span></a>';
					h +=    '<a href="#" class="button left" id="selectables-option-inverse-selection"><span>Inverse selection</span></a>';
					h +=    '<a href="#" class="button left" id="selectables-option-undo-selection"><span>Undo selection</span></a>';
					h += '</div>';

					// Append the HTML:
					$optionsContainer.append(h);

					// Simulate window resize
					$(window).resize();

					// Set actions:
					// - Undo selection
					$('#selectables-option-undo-selection').click(function() {
						$('.list table tbody').M_SelectionClear();
						return false;
					});

					// - Inverse selection
					$('#selectables-option-inverse-selection').click(function() {
						$('.list table tbody').M_SelectionInverse();
						return false;
					});
					
					// - Delete selection
					$('#selectables-option-delete').click(function() {
						// Confirm box:
						var sure = confirm('Are you sure you want to delete the selection?');
						
						// If pressed OK:
						if(sure) {
							// Get the current selection
							var sel = $('.list table tbody').M_Selection();

							// Prepare work variables
							// - Current instance ID
							var id;

							// For each of the instances in the selection
							for(var i= 0; i < sel.length; i ++) {
								// Get the ID
								id = parseInt(sel[i].substring(16, sel[i].length));

								// We start deleting the item (via AJAX)
								$.M_AjaxRequest({
									// Compose the URL, based on the current URL
									url : jQueryBaseHref + '/admin/delete-ajax/{/literal}{$module->getId()}/' + id + '/{$definition->getDataObjectId()}{literal}',
									// The expected data type is XML
									dataType : 'xml',
									// When the response comes in:
									success : function(data) {
										var $id = $('response id', data);
										var $rs = $('response result', data);
										var $ll = $('.list table tbody');
										
										// If the object has been deleted successfully:
										if($rs.length == 1 && $id.length == 1 && $rs.text() == "1") {
											// Unselect the row
											$('#idListedInstance' + $id.text()).M_SelectableUnselect();
											
											// Fade out the object's row:
											$('#idListedInstance' + $id.text()).fadeOut(200, function() {
												// Then, remove the row
												$(this).remove();
												
												// Remove the tooltip:
												$.M_TooltipRemove();
												
												// Update the live list:
												if($ll.data('M_LiveListOptions')) {
													$ll.trigger('M_LiveListAddToTotal', -1);
													$ll.trigger('M_LiveListUpdate');
												}
											});
										}
									}
								});
							}
						}

						// Override default behavior
						return false;
					});
				});
			}
		});
		
		$('.list table tbody').bind('M_Selectables_Unselect', function($event, $deselectedElement, selectedCount) {
			if(selectedCount == 0) {
				// Remove options:
				$('#selectables-options').remove();
				
				// Get container of list options
				var $optionsContainer = $('#{/literal}{$module->getId()}-{$definition->getDataObjectId()}{literal}-list-options');
				
				// Get Backup of list options properties:
				var _bk = $optionsContainer.data('__bk');

				// Animate to new height, and set new options:
				$optionsContainer.stop().animate({height: _bk.height}, 220, function() {
					$(window).resize();
				});
			}
		}); *}{literal}
	{/literal}
	</script>
	
	{if $isSortable}
		<script type="text/javascript" language="Javascript">
		{literal}
			$(document).ready(function() {
				var _sortableDraggedItemStart = 0;
				var _sortableDraggedItemEnd   = 0;
				var _sortableDraggedItemDiff  = 0;
				$('tbody').sortable({
					axis : 'y',
					revert : true,
					// Make helper adapt the original size:
					helper : function(e, ui) {  
						ui.children().each(function() {  
							$(this).width($(this).width());  
						});  
						return ui;
					},
					// When started dragging:
					start : function($event, ui) {
						// Get the current index number
						_sortableDraggedItemStart = $('tbody').children().index(ui.item);
					},
					// When dragging:
					sort : function($event, ui) {
						// ui.helper.parent().css({width : '2000px', border: "1px solid red"});
					},
					// When finished dragging
					update : function($event, ui) {
						// Get the new index number
						_sortableDraggedItemEnd   = $('tbody').children().index(ui.item);
			
						// Calculate the difference in index number. This is the direction
						// of movement, and the number of steps in that direction. E.g. -2
						// means that the item has been dragged 2 positions upwards
						_sortableDraggedItemDiff  = _sortableDraggedItemEnd - _sortableDraggedItemStart;
			
						// Get the ID of the dragged instance:
						var i = ui.item.attr('id').substr(16, ui.item.attr('id').length);
			
						// Compose the URL for the AJAX Request, that is going to store the
						// new order index numbers:
						var u = jQueryBaseHref + '/admin/move-instance/{/literal}{$module->getId()}/{$definition->getDataObjectId()}/order/'+ i + '/' + _sortableDraggedItemDiff +'{literal}';
			
						// Do the AJAX Request:
						$.ajax({
							// To URL:
							url : u,
							// Send along the filter values:
							data : {
								{/literal}{foreach from=$smarty.get item="value" key="name" name="i"}{$name} : '{$value}'{if !$smarty.foreach.i.last}, {/if}{/foreach}{literal}
							},
							// Expected Data Type
							dataType : 'html',
							// When done:
							success : function() {
							}
						});
		
						// Re-do the css classes of the rows:
						$('tbody').children().each(function(i) {
							$(this).attr('class', 'row' + (i % 2 == 0 ? '1' : '2'));
						});
					}
				});
			});
		{/literal}
		</script>
	{/if}
{/if}