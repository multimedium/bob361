<?xml version="1.0" encoding="UTF-8"?>
<toggle>
	{if isset($object)}
		<id>{$object->getId()}</id>
	{/if}
	<result>{if $flag}1{else}0{/if}</result>
	{if isset($value)}
		<value>{$value}</value>
	{/if}
</toggle>