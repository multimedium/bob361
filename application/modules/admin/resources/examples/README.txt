_DataObject_List.xml
--------------------------------------------------------------------------------
An example of how you could define a list in your module. The example file illustrates
how you would go about defining the listed columns, and the filters that may be
applied on the list.

Note that you should create this file in the following folder, inside the module's
root folder:

/admin/resources/definitions

The name of the XML file is very simple. You should replace _DataObject_ by
the name of the data object, which should have been defined in the info.xml file
of the module. Assuming you have defined the following data objects in your module:

[info.xml]
[...]
	<dataObject>
		<product>Product</product>
		<category>ProductCategory</category>
		<highlight>ProductHighlight</highlight>
	</dataObject>
[...]

Then, you could define the list of "Product Highlights", by creating the file
ProductHighlightList.xml in the /admin/resources/definitions folder of the module.


_DataObject_Form.xml
--------------------------------------------------------------------------------
An example of how you could define a form in your module. The example file illustrates
how you would go about defining the fields in the form, and some other additional
properties of the form.

Note that you should create this file in the following folder, inside the module's
root folder:

/admin/resources/definition

The name of the XML file is very simple. You should replace _DataObject_ by
the id of the data object, which should have been defined in the info.xml file
of the module. Assuming you have defined the following data objects in your module:

[info.xml]
[...]
	<dataObject>
		<product>Product</product>
		<category>ProductCategory</category>
		<highlight>ProductHighlight</highlight>
	</dataObject>
[...]

Then, you could define the form of "Product Highlights", by creating the file
ProductHighlightForm.xml in the /admin/resources/definitions folder of the module.
