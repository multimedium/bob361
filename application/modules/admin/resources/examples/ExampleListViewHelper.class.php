<?php 
class ExampleListViewHelper {
	/**
	 * Add columns
	 *
	 * Plugs in additional columns into the default list view. An 
	 * ArrayIterator (or array) is expected from this method. Also, the
	 * values of the collection must be {@link AdminListColumnDefinition}
	 * instances, which describe the columns to be added to the view.
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 * 
	 * @access public
	 * @return ArrayIterator|null
	 */
	public function addColumns() {
	}
	
	/**
	 * Set column properties
	 * 
	 * No return value is expected from this method.
	 * 
	 * @access public
	 * @param AdminListColumnDefinition $column
	 * 		The column of which to set the properties
	 * @return void
	 */
	public function setColumnProperties(AdminListColumnDefinition $column) {
		
	}
	
	/**
	 * Set column value
	 * 
	 * Manipulates the values shown in the list view. A string is 
	 * expected from this method.
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param AdminListColumnDefinition $column
	 * 		The column for which to set the value
	 * @param M_DataObject $dataObject
	 * 		The instance for which to set the value
	 * @param mixed $value
	 * 		The value that has been prepared by {@link AdminListView}.
	 * 		This will be the listed value, if not manipulated by this
	 * 		method.
	 * @return string|null
	 */
	public function setColumnValue(AdminListColumnDefinition $column, M_DataObject $dataObject, $value) {
	}
	
	/**
	 * Set row options
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 * 
	 * NOTE:
	 * If this method returns (bool) FALSE, no list options will be available
	 * at all!
	 *
	 * @access public
	 * @param M_DataObject $dataObject
	 * 		The instance for which to set the row options
	 * @return array|null
	 */
	public function setRowOptions(M_DataObject $dataObject) {
	}
	
	/**
	 * Add row options
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param M_DataObject $dataObject
	 * 		The instance for which to add the row options
	 * @return array|null
	 */
	public function addRowOptions(M_DataObject $dataObject) {
	}
	
	/**
	 * Set options
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 * 
	 * NOTE:
	 * If this method returns (bool) FALSE, no list options will be available
	 * at all!
	 *
	 * @access public
	 * @return array|null|false
	 */
	public function setOptions($listDef, $filters) {
	} 
	
	/**
	 * Add options
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 * 
	 * NOTE:
	 * If this method returns (bool) FALSE, no list options will be available
	 * at all!
	 *
	 * @access public
	 * @return array|null|false
	 */
	public function addOptions($listDef, $filters) {
	}	
}