<?php
class TerrainViewHelper {
	public function addSourcePaneMenu() {
		$menu = M_Menu::getInstance('MyMenu');
		$menu->setTitle('My custom menu');
		$menu->addMenuItem(new M_MenuItem('admin/test', 'test'));
		$menu->addMenuItem(new M_MenuItem('admin/test2', 'test 2'));
		return array($menu);
	}

	public function setSourcePaneMenu(M_Menu $menu) {
		$menu->addMenuItem(new M_MenuItem('/admin/test', 'test'));
	}

	public function setSourcePaneMenuWeight(M_Menu $menu) {
		switch($menu->getId()) {
			case 'MyMenu':
				return 0;
		}
	}
	
	public function addPlusMenu() {
		$menu = M_Menu::getInstance('MyPlusMenu');
		$menu->setTitle('My custom menu');
		$menu->addMenuItem(new M_MenuItem('admin/test', 'test'));
		$menu->addMenuItem(new M_MenuItem('admin/test2', 'test 2'));
		return array($menu);
	}

	public function setPlusMenu(M_Menu $menu) {
		$menu->addMenuItem(new M_MenuItem('/admin/test', 'test'));
	}

	public function setPlusMenuWeight(M_Menu $menu) {
		switch($menu->getId()) {
			case 'MyPlusMenu':
				return 0;
		}
	}
}