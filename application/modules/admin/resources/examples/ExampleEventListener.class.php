<?php
/**
 * ExampleEventListener
 * 
 * NOTES:
 * - Event Listeners are loaded automatically
 * - "Example" is the Module ID
 * - Constructor receives the Event Dispatcher (AdminController instance)
 */
class ExampleEventListener {
	public function __construct(AdminController $controller) {
		$controller->addEventListener(AdminControllerEvent::DATA_OBJECT_BEFORE_EDIT, array($this, 'onDataObjectBeforeEdit'));
	}
	
	public function onDataObjectBeforeEdit(AdminControllerEvent $event) {
		echo $event->getDataObject()->getId() . ' is about to be edited';
	}
}