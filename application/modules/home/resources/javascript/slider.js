/* -- Background Slider -- */
$.vegas('slideshow', {
	delay:4000,
	backgrounds:backgroundSliderImages //get global declared images array
});

$('#slider-nav-left').click(function() {
	$.vegas('previous');
});

$('#slider-nav-right').click(function() {
	$.vegas('next');
});

if($(window).width()<992){
	$.vegas('stop');
}