{* Load extra CSS *}
{cssfile file="application/modules/home/resources/css/menu.css" context="home-css"}
{jsfile file="application/modules/home/resources/javascript/slider.js" context="home-slider-js"}

<script type='text/javascript'>
	var backgroundSliderImages = [
	{$images = $mediaAlbum->getImages(true)}
	{$fade="800"}
	{if $images}
		{foreach $images as $image}
			{ldelim}src:'{link href='thumbnail/slider/'}{$image->getBasename()}', fade:{$fade}{rdelim}
			{if ! $image@last}
				,
			{else}
				]
			{/if}
		{/foreach}
	{/if}
</script>

{* Background slider navigation*}
		<div class="slider-nav">
			<div id="slider-nav-left" class="pull-left circle circle-orange"></div>
			<div id="slider-nav-right" class="pull-right circle circle-orange"></div>
		</div>
{* /Background slider*}