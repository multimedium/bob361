<?php
/**
 * HomeController
 *
 * @package App
 * @subpackage Home
 */
class HomeController extends PageController {

	/* -- PUBLIC FUNCTIONS -- */

	/**
	 * Homepage
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		// Construct the view
		$view = M_loader::getView('HomeView', 'home');

		$view->setMediaAlbum($this->_getMediaAlbumRealm('slider-homepage'));
		
		$view->setPageTitle('Welcome');
		$view->setPageDescription('Welcome');
		
		// Display
		/* @var $view HomeView */
		$view->display();
	}
	
	/**
	 * get media album for background slider
	 * 
	 * @access private
	 * @return mediaAlbum
	 */
	
	protected function _getMediaAlbumRealm($realm) {
		// construct media album mapper
		M_Loader::loadDataObjectMapper('MediaAlbum', 'media');
		$mapper = new MediaAlbumMapper();
		return $mapper->getByRealm($realm);
	}
}
