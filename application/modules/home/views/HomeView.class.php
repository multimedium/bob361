<?php
/**
 * HomeView
 *
 * @package App
 * @subpackage Home
 */
class HomeView extends PageView {
	public function setPublications(M_ArrayIterator $publications) {
		$this->assign('publications', $publications);
	}
	
	public function setMediaAlbum(MediaAlbum $mediaAlbum) {
		$this->assign('mediaAlbum', $mediaAlbum);
	}
	
	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get the resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('home', 'Home.tpl');
	}
}