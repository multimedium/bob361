{* Title *}
<div class="row">
    <div class="span">
        <h2 class="headerBlock m-left">
            {t text="No keywords"}
        </h2>
    </div>
</div>
{* Message *}
<div class="row">
    <div class="span">
        <p>{t text="You haven't submitted any keywords. Type in the search box to search this website."}</p>
    </div>
</div>

