{* Title *}
<div class="row">
    <div class="span">
        <h2>
            {t text='Search Results for <em>"@searchPhrase"</em>' searchPhrase=$expression}
        </h2>
    </div>
</div>
{* If results were found *}
{if $results|count > 0}
    {* Message *}

    {* For each of the search results *}
    {foreach from=$results item="result" name="s"}
        <div class="row">
            {* Render the current item: *}
            <div class="span">
                <a href="{$result->getUri()->toString()}" title="{$result->getTitle()|att}">

                    {* Image *}
                    {*
                    {if $result->hasImage()}
                        <img src='{link href="thumbnail/search/{$result->getImage()->getPath()}"}'
                             alt="{$result->getTitle()|att}" width="211" height="146"/>
                    {else}
                        <img src='{link href="placeholder-col14.gif" type="images"}' alt="{$result->getTitle()|att}"
                             width="211" height="146"/>
                    {/if}
                    *}
                    {* Title *}
                    <span class="title">
						{snippet|escape from=$result->getTitle() length="120"}
					</span>
            </div>

            </a>
        </div>
    {/foreach}

{else}
    <div class="row">
        <div class="span">
            <p>{t|escape text='sorry, no results were found. Please try again.'}</p>
        </div>
    </div>
{/if}

