<?php
/**
 * SearchEmptyExpressionView
 * 
 * @package App
 * @subpackage Search
 */
class SearchResultsView extends PageView {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Search expression
	 * 
	 * @access private
	 * @var string
	 */
	private $_expression;
	
	/**
	 * Search results
	 * 
	 * @access private
	 * @var array
	 */
	private $_results;
	
	/* -- SETTERS -- */
	
	/**
	 * Set Search Expression
	 * 
	 * @access public
	 * @param M_ArrayIterator $searchExpression
	 * @return SearchResultsView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSearchExpression($expression) {
		$this->_expression = (string) $expression;
		return $this;
	}
	
	/**
	 * Set Search Results
	 * 
	 * @access public
	 * @param M_ArrayIterator $searchResults
	 * @return SearchResultsView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSearchResults(M_ArrayIterator $searchResults) {
		$this->_results = $searchResults->getArrayCopy();
		return $this;
	}

	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Preprocessing
	 *
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Make sure all variables have been provided
		if(is_null($this->_expression) || is_null($this->_results)) {
			// Throw exception
			throw new M_Exception(sprintf(
				'Cannot render view %s; Missing search expression or results',
				__CLASS__
			));
		}
		
		// Assign presentation variables:
		$this->assign('expression', $this->_expression);
		$this->assign('results', $this->_results);
	}

	/**
	 * Get the resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('search', 'SearchResults.tpl');
	}
}