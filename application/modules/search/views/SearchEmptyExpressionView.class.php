<?php
/**
 * SearchEmptyExpressionView
 * 
 * @package App
 * @subpackage Search
 */
class SearchEmptyExpressionView extends PageView {

	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Preprocessing
	 *
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
	}

	/**
	 * Get the resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('search', 'SearchEmptyExpression.tpl');
	}
}