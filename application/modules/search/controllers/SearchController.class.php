<?php
/**
 * SearchController
 * 
 * @package App
 * @subpackage Search
 */
class SearchController extends PageController {
	/**
	 * Search
	 * 
	 * @access public
	 * @return void
	 */
	public function index() {
		// Get the search expression
		$q = trim(M_Request::getVariable('q', NULL, M_Request::TYPE_STRING_SANITIZED));
		
		// If no search expression is provided
		if(! $q) {
			// Then, we construct a view to inform about an empty query:
			M_Loader
				::getView('SearchEmptyExpressionView', 'search')
				->setPageTitle(t('No keywords'))
				->setPageDescription(t(
                    "You haven't submitted any keywords. Type in the search box to search this website."
				))
			// And, we display:
				->display();
		}
		// If a search expression has been provided:
		else {
			// Load required models:
			M_Loader::loadModel('SearchQuery', 'search');
			
			// Construct a new search query:
			$query = new SearchQuery($q);
			
			// Construct the view
			$view = M_Loader::getView('SearchResultsView', 'search');
			
			/* @var $view SearchResultsView */
			// Provide the view with
			$view
				// The search results
				->setSearchResults($query->getSearchResults())
				->setSearchExpression($q)
				// The page title and description
				->setPageTitle(t('Search results'))
				->setPageDescription(t('Search results'));
			
			// Finally, display
			$view->display();
		}
	}
}