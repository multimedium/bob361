<?php
M_Loader::loadController('SearchController', 'search');
/**
 * SearchAjax
 */
class SearchAjaxController extends SearchController {

    /**
     * Index
     */
    public function index() {
        // Get the search expression
        $q = trim(M_Request::getVariable('q'));

        // Construct a new search query:
        M_Loader::loadModel('SearchQuery', 'search');
        $query = new SearchQuery($q);


        // Construct the view
        $result = array();

        foreach($query->getSearchResults() as $searchResult) {
            /*@var $searchResult SearchResult */

//            $imageObject = $searchResult->getImage();
//            $image = '';
//            if($imageObject) {
//                $image =  M_Reest('thumbnail/product/' . $imageObject->getHashIdEncrypted() . '/' . \M\Uri\Path::getFromString($searchResult->getTitle()) . '.' . $imageObject->getFileExtension());
//            }
            $result[] = [
                'value' => $searchResult->getTitle(),
                'description' => M_Text::getTextAsSnippet($searchResult->getDescription(), 60),
//                'image'	=> $image,
                'href'	=> $searchResult->getUri()->toString()
            ];
        }

        // Return the result as JSON:
        echo json_encode($result);
    }

}