<?php
/**
 * SearchableDataObjectMapper
 * 
 * @abstract
 * @package App
 * @subpackage Search
 */
abstract class SearchableDataObjectMapper extends M_DataObjectMapper {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Filter: search
	 * 
	 * @access private
	 * @var string
	 */
	private $_filterSearch;

    /* -- ABSTRACT -- */

    /**
     * Add FilterPublishedOnly
     *
     * @return SearchableDataObjectMapper
     */
    abstract function addFilterPublishedOnly();

	/* -- SETTERS -- */
	
	/**
	 * Reset filters
	 * 
	 * @access public
	 * @return SearchableDataObjectMapper $mapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function resetFilters() {
		// Reset regular filters:
		parent::resetFilters();
		
		// Reset search filter
		$this->_filterSearch = NULL;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Add Filter Search
	 * 
	 * This method allows to add a filter to the mapper of data objects, in order
	 * to search with a given search expression.
	 *
	 * @access public
	 * @param string $expression
	 * @return SearchableDataObjectMapper $mapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFilterSearchExpression($expression) {
		// Set the search expression
		$this->_filterSearch = (string) $expression;
		
		// Return myself:
		return $this;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get Filtered Query
	 * 
	 * @acces protected
	 * @param M_DbQuery $query
	 * @param array $filters
	 * @return M_DbQuery
	 */
	protected function _getFilteredQuery(MI_DbQuery $query, array $filters) {
		// Apply the filter with search
		return $this->_getFilteredQueryWithSearch(
			// Onto the query with "regular" filters
			parent::_getFilteredQuery($query, $filters)
		);
	}
	
	/**
	 * Get Filtered Query, with search conditions
	 * 
	 * Used by {@link SearchableDataObjectMapper::_getFilteredQuery()} to add
	 * the SQL conditions for the search filter to the database query provided.
	 * 
	 * @acces protected
	 * @param MI_DbQuery $query
	 * @return MI_DbQuery
	 */
	protected function _getFilteredQueryWithSearch(MI_DbQuery $query) {
		// If no search expression is defined as a filter
		if(! $this->_filterSearch) {
			// Then, we do not add any conditions to the query. We stop here:
			return $query;
		}
		
		// Get the search terms from the expression provided
		$terms  = M_Search::getSearchTerms($this->_filterSearch);
		
		// Get the number of search terms:
		$nTerms = count($terms);
		
		// If at least 1 term is extracted from the expression
		if($nTerms > 0) {
			// We prepare some working variables. The first one, $sql, will be
			// used to store the SQL conditions along with the placeholder values
			$sql    = array('');
			
			// The second one, $tagIds, is an array with Tag ID's that match with
			// terms in the search expression. This array will later be used to
			// add SQL conditions, thus adding search results with tag matches.
			$tagIds = array();
			
			// This mapper will look for all LOCALIZED fields in the definition
			// of the data object, in order to match on these fields. First, we
			// create an array to store the localized fields:
			$locs = array();
			
			// The locale with which to search:
			$locale = M_Locale::getCategory(M_Locale::LANG);
			
			// For each of the fields:
			foreach($this->getFields() as $field) {
				/* @var $field SimpleXmlElement */
				// Is the current field a localized field?
				if($this->isLocalizedField($field)) {
					// In preparation of the SQL conditions to be added further
					// down this function, we add a JOIN with the table of 
					// localized texts:
					$this->_addLocalizedFieldJoin(
						$query, 
						(string) $field->name, 
						$locale
					);
					
					// Then, we add the field to the array of localized fields:
					$locs[] = (object) array(
						// With its name:
						'dbName'   => (string) $field->name,
						// With its database name:
						'dbName'   => (string) $field->db_name,
						// And the prefix to address it (the JOINED table)
						'dbPrefix' => $this->getTableName() . '_text_' . (string) $field->db_name . '_' . $locale,
					);
				}
			}
			
			// We should have collected at least 1 localized field to start the
			// search with. If that is not the case:
			if(count($locs) == 0) {
				// Then, we mark the mapper with "no results"
				$this->setFlagEmptyResultSet(TRUE);
				
				// And we stop here:
				return $query;
			}
			
			// Is the Tag module installed?
			$isTagsInstalled = M_Application::isModuleInstalled('tag');
			
			// If so:
			if($isTagsInstalled) {
				// Construct tag mapper
				$tagMapper = M_Loader::getDataObjectMapper('Tag', 'tag');
				/* @var $tagMapper TagMapper */
			}
			
			// For each of the search terms extracted from the expression:
			foreach($terms as $condition) {
				// If SQL conditions were generated previously
				if(! empty($sql[0])) {
					// Then, we add the logical operator to connect to previous
					// conditions:
					$sql[0] .= ' AND ';
				}
				
				// Add SQL conditions to search for the current search term
				// in the data object's storage space:
				$sql[0] .= '(';
				
				// For each of the localized fields we look up earlier:
				foreach($locs as $i => $field) {
					// Connect to previous field with logical operator "OR"
					if($i > 0) {
						$sql[0] .= ' OR ';
					}
					
					// Match with the search term:
					$sql[0] .= $this->getDb()->quoteIdentifier($field->dbPrefix . '.text_value');
					$sql[0] .= ' LIKE ?';
					
					// Add placeholder
					array_push($sql, '%' . $condition['term'] . '%');
				}
				
				// Finish conditions:
				$sql[0] .= ' )';
				
				// If the tags module is installed
				if($isTagsInstalled) {
					// Look up the tag that matches the current search term:
					$tagMapper->resetFilters();
					$tagMapper->addFilter(new M_DbQueryFilterWhere('title', $condition['term']));

					/* @var $tagMapper TagMapper */
					// For each of the tags found:
					foreach($tagMapper->getAll() as $tag) {
						// We add the ID of the tag to the array of Tag ID's. We
						// will use this array further down, to add additional
						// SQL conditions:
						array_push($tagIds, $tag->getId());
					}
				}
			}
			
			// If the Tags Module is installed and if Tag ID's were collected 
			// previously in the $tagIds array:
			if($isTagsInstalled && count($tagIds) > 0) {
				// If SQL conditions have been generated previously
				if(! empty($sql[0])) {
					// Wrap them all in a where block, to correctly separate them
					// from the conditions for matching with tags:
					$sql[0] = '(' . $sql[0] . ')';
				}
			
				// Join with tag_x_objects
				$query->joinLeft(
					// The table
					'tag_x_object', 
					// Table alias:
					$this->getTableName() . '_txo', 
					// Join on:
					$this->getDb()->quoteIdentifier(
						$this->getTableName() . '.' . $this->getFieldDbName('id')
					), 
					// Joining conditions:
					'object_id AND ' . 
						$this->getDb()->quoteIdentifier('object') . ' = "'. 
						$this->getDataObjectClassName() .'"'
				);
				
				// Add to previous SQL
				if(! empty($sql[0])) {
					$sql[0] .= ' OR ';
				}
				
				// Add match with Tag ID's
				$sql[0] .= $this->getDb()->quoteIdentifier($this->getTableName() . '_txo.tag_id') . ' IN(';
				$sql[0] .=    implode(',', $tagIds);
				$sql[0] .= ')';
			}
			
			// If SQL conditions have been created at the end of this function:
			if(! empty($sql[0])) {
				// Then, we construct a callback that allows us to apply all
				// SQL conditions generated:
				$callback = new M_Callback();
				
				// Set the callback object and function:
				$callback->setObject($query);
				$callback->setFunctionName('where');
				
				// Use the $sql arguments to set the arguments of the callback
				// function. The first argument will then be the SQL conditions
				// (string). All additional arguments are placeholder values:
				foreach($sql as $arg) {
					$callback->addArgument($arg);
				}
				
				// Run the callback
				$callback->run();
				
				// Group by ID, to prevent double records
				$query->group(array($this->getFieldDbName('id')));
			}
		}
		
		// Return the query with search conditions
		return $query;
	}
}