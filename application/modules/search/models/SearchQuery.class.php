<?php
/**
 * SearchQuery
 * 
 * @package App
 * @subpackage Search
 */
class SearchQuery extends M_Object {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Search keywords
	 * 
	 * @access private
	 * @var string
	 */
	private $_keywords;
	
	/**
	 * Page jumper
	 * 
	 * @access private
	 * @var M_Pagejumper
	 */
	private $_pagejumper;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Construct the search query
	 * 
	 * @access public
	 * @param string $keywords
	 * @return SearchQuery
	 */
	public function __construct($keywords) {
		// Set the keywords
		$this->setKeywords($keywords);
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set keywords
	 * 
	 * This method allows to set the keywords with which a search is to be
	 * conducted in the website.
	 * 
	 * @access public
	 * @param string $keywords
	 * @return SearchQuery $query
	 *		Returns itself, for a fluent programming interface
	 */
	public function setKeywords($keywords) {
		// Set the search keywords
		$this->_keywords = (string) $keywords;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set Pagejumper
	 * 
	 * This method allows to set the pagejumper that is be taken into account,
	 * when fetching search results from the database.
	 * 
	 * @access public
	 * @param M_Pagejumper $pagejumper
	 * @return SearchQuery $query
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPagejumper(M_Pagejumper $pagejumper) {
		// Set the pagejumper
		$this->_pagejumper = $pagejumper;
		
		// Return myself
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get search results
	 * 
	 * @access public
	 * @return M_ArrayIterator $searchResults
	 *		The search results, each represented by an instance of the 
	 *		{@link SearchResult} class
	 */
	public function getSearchResults() {
		// Output
		$out = array();
		
		// For each of the data objects encountered:
		foreach($this->_getMapper()->getAll() as $dataObject) {
			// Add the search result to the output
               $out[] = $this->_getSearchResultFromDataObject($dataObject);
		}
		
		// Return the collection of search results:
		return new M_ArrayIterator($out);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get Search Result, from data object
	 * 
	 * @access protected
	 * @param M_DataObject $dataObject
	 * @return SearchResult
	 */
	protected function _getSearchResultFromDataObject(M_DataObject $dataObject) {
		// Load required classes
		M_Loader::loadModel('SearchResult', 'search');
		
		// Construct a new SearchResult object. This is the output of this function
		$out = new SearchResult();
		
		// Set the URI, so we can link to the search result:
		$out->setUri($this->_getDataObjectUri($dataObject));
		
		// Set the title of the search result:
		$out->setTitle($this->_getDataObjectValue($dataObject, 'title'));
		
		// And the description:
		$out->setDescription($this->_getDataObjectValue($dataObject, 'description'));
		
		// Get the image from the data object:
		$image = $this->_getDataObjectValue($dataObject, 'image');
		
		// If the image returned is an instance of M_Image
		if($image && M_Helper::isInstanceOf($image, 'M_Image')) {
			// Then, set the image in the search result:
			$out->setImage($image);
		}
		
		// Return the search result:
		return $out;
	}
	
	/**
	 * Get value from data object
	 * 
	 * @access protected
	 * @param M_DataObject $dataObject
	 * @param string $property
	 * @return mixed
	 */
	protected function _getDataObjectValue(M_DataObject $dataObject, $property) {
		// Get the name of the data object provided
		$name = $dataObject->getClassName();
		
		// Get the Search Result Mapping for this data object. This mapping will 
		// tell us how to map a data object's properties to the properties of a
		// search result.
		$mapping = M_Application
			::getConfigOfModule('search')
			->getRequired('mapping/' . $name);
		
		// Check if a mapping was defined for the property requested:
		if(M_Helper::isInstanceOf($mapping, 'M_Config') && isset($mapping->$property) && ! empty($mapping->$property)) {
			// If so, we construct a callback function in order to run the
			// getter that is defined in the mapping.
			$callback = new M_Callback();
			$callback->setObject($dataObject);
			$callback->setFunctionName((string) $mapping->$property);

			// If not callable:
			if(! $callback->isCallable()) {
				// Throw an exception to inform about the error;
				throw new M_Exception(sprintf(
					'Cannot construct SearchResult instance with data object ' . 
					'of class %s; Cannot retrieve the %s: the callback ' . 
					'%s::%s() is not callable!',
					$name,
					$property,
					$name,
					$getter
				));
			}

			// Get the result of the getter:
			return $callback->run();
		}
		// If a mapping is not defined:
		else {
			// Then, we'll try to map the properties of the data object 
			// to a search result object automatically. For each of the properties
			// provided by the data object:
			foreach($dataObject->toArray() as $p => $value) {
				// If a title is requested, and the current property looks like
				// a name/title
				if($property == 'title' && in_array($p, array('name', 'title'))) {
					// Then, we return the current property as the title:
					return $value;
				}
				// If a description is requested, and the property looks like
				// a description or a text:
				elseif($property == 'description' && in_array($p, array(
					'description', 
					'intro', 
					'introduction', 
					'introductionText', 
					'summary', 
					'teaser', 
					'teaserText',
					'text'
				))) {
					// Set the description of the search result:
					return $value;
				}
				// If an image is requested:
				elseif($property == 'image') {
					// Look for a getter of images, in the class of the data 
					// object provided:
					$imageGetter = method_exists($dataObject, 'getImage')
						? 'getImage'
							: NULL;

					// If found:
					if($imageGetter) {
						// Return the result of the getter:
						return $dataObject->$imageGetter();
					}
				}
			}
		}
		
		// If still here, return NULL
		return NULL;
	}
	
	/**
	 * Get link for data object
	 * 
	 * @access protected
	 * @param M_DataObject $dataObject
	 * @return M_Uri
	 */
	protected function _getDataObjectUri(M_DataObject $dataObject) {
		// Get the name of the data object provided
		$name = $dataObject->getClassName();
		
		// Get the definition of the link:
		$link = M_Application
			::getConfigOfModule('search')
			->getRequired('mapping/' . $name . '/link');

		// Extract all getters in the link. These are placeholders, to be replaced
		// by their return value:
		$getters = array();
		preg_match_all('/@([^(]+)\(\)/', $link, $getters);
		
		// For each of the getters found:
		foreach($getters[1] as $getter) {
			// Construct a callback to retrieve the placeholder value:
			$callback = new M_Callback();
			$callback->setObject($dataObject);
			$callback->setFunctionName($getter);
			
			// If the callback is not callable:
			if(! $callback->isCallable()) {
				// We throw an exception to inform about the error:
				throw new M_Exception(sprintf(
					'Cannot render a link for data object of class %s; Callback ' . 
					'%s::%s() is not callable!',
					$name,
					$name,
					$getter
				));
			}
			
			// Replace the placeholder by the value:
			$link = str_replace('@' . $getter . '()', $callback->run(), $link);
		}
		
		// Return the link:
		return new M_Uri(M_Request::getLink($link));
	}
	
	/**
	 * Get mapper to search
	 * 
	 * @access protected
	 * @return M_DataObjectMapper
	 */
	protected function _getMapper() {
		// Get the searchable data object mappers:
		$mappers = $this->_getSearchableDataObjectMappers();
		
		// If no mappers exist:
		if(count($mappers) == 0) {
			// We throw an exception to inform about the error:
			throw new M_Exception(sprintf(
				'No searchable data object mappers exist in the application. ' . 
				'Inherit from SearchableDataObjectMapper in order to make the ' .
				'corresponding data objects searchable!'
			));
		}
		
		// For each of the mappers:
		foreach($mappers as $currentMapper) {
			/* @var $currentMapper SearchableDataObjectMapper */
			// Add a filter, to search for the search keywords
			$currentMapper
                ->addFilterPublishedOnly()
                ->addFilterSearchExpression($this->_keywords);
		}
		
		// Get the first mapper:
		$mapper = array_shift($mappers);
		
		/* @var $mapper SearchableDataObjectMapper */
		// For each of the remaining mappers:
		foreach($mappers as $currentMapper) {
			/* @var $currentMapper SearchableDataObjectMapper */
			// Create a union with the first mapper:
			$mapper->addUnion($currentMapper);
		}
		
		// If a page jumper is provided:
		if($this->_pagejumper) {
			// Then, add a filter to the mapper to limit the number of results:
			$mapper->addFilter(new M_DbQueryFilterPagejumper($this->_pagejumper));
		}
		
		// Return the mapper with unions:
		return $mapper;
	}
	
	/**
	 * Get Searchable Data Object Mappers
	 * 
	 * This method is used internally to collect the data object mappers that
	 * can be used to search, in the application.
	 * 
	 * @access protected
	 * @return array
	 */
	protected function _getSearchableDataObjectMappers() {
		// Output
		$out = array();
		
		// Load required classes
		M_Loader::loadDataObjectMapper('SearchableDataObject', 'search');
		
		// For each of the modules in the application:
		foreach(M_Application::getModules() as $module) {
			/* @var $module M_ApplicationModule */
			// Get the data object definitions in the current module. For each of 
			// the data objects:
			foreach(array_keys($module->getDataObjectNames()->getArrayCopy()) as $dataObjectId) {
				// Get the mapper:
				$mapper = $module->getDataObjectMapper($dataObjectId);
				
				// Is the mapper an instance of SearchableDataObjectMapper?
				if(M_Helper::isInstanceOf($mapper, 'SearchableDataObjectMapper')) {
					// If so, add to output:
					array_push($out, $mapper);
				}
			}
		}
		
		// Return the output
		return $out;
	}
}