<?php
/**
 * SearchResult
 * 
 * @package App
 * @subpackage Search
 */
class SearchResult extends M_Object {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Title
	 * 
	 * @access private
	 * @var string
	 */
	private $_title;
	
	/**
	 * Description
	 * 
	 * @access private
	 * @var string
	 */
	private $_description;
	
	/**
	 * Image
	 * 
	 * @access private
	 * @var M_Image
	 */
	private $_image;
	
	/**
	 * URI
	 * 
	 * @access private
	 * @var M_Uri
	 */
	private $_uri;
	
	/* -- SETTERS -- */
	
	/**
	 * Set title
	 * 
	 * @access public
	 * @param string $title
	 * @return SearchResult $result
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
		return $this;
	}
	
	/**
	 * Set description
	 * 
	 * @access public
	 * @param string $description
	 * @return SearchResult $result
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
		return $this;
	}
	
	/**
	 * Set image
	 * 
	 * @access public
	 * @param M_Image $image
	 * @return SearchResult $result
	 *		Returns itself, for a fluent programming interface
	 */
	public function setImage(M_Image $image) {
		$this->_image = $image;
		return $this;
	}
	
	/**
	 * Set URI
	 * 
	 * @access public
	 * @param M_Uri $uri
	 * @return SearchResult $result
	 *		Returns itself, for a fluent programming interface
	 */
	public function setUri(M_Uri $uri) {
		$this->_uri = $uri;
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get title
	 * 
	 * @access public
	 * @return string
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 * Get description
	 * 
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/**
	 * Get image
	 * 
	 * @access public
	 * @return M_Image
	 */
	public function getImage() {
		return $this->_image;
	}
	
	/**
	 * Has existing image?
	 * 
	 * @access public
	 * @return bool $flag
	 *		Returns TRUE if an existing image is contained by the search result, 
	 *		FALSE if not
	 */
	public function hasImage() {
		return ($this->_image && $this->_image->exists());
	}
	
	/**
	 * Get URI
	 * 
	 * @access public
	 * @return M_Uri
	 */
	public function getUri() {
		return $this->_uri;
	}
}