<?php
/**
 * AdminWebmasterLocaleImportFormView
 * 
 * @package App
 * @subpackage Webmaster
 */

// Load superclass
AdminLoader::loadView('AdminWebmasterLocaleImportFormView', 'webmaster');

/**
 * AdminWebmasterLocaleImportFormView
 * 
 * @package App
 * @subpackage Webmaster
 */
class AdminWebmasterLocaleImportFormView extends AdminPageView {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Form
	 * 
	 * This property stores the form that is to be shown in the view
	 * 
	 * @access private
	 * @var AdminWebmasterLocaleImportForm
	 */
	private $_form;
	
	/* -- SETTERS -- */
	
	/**
	 * Set form
	 * 
	 * This method allows you to set the form (import po file) that is to be
	 * displayed in the view.
	 * 
	 * @access public
	 * @param AdminWebmasterLocaleImportForm $form
	 * @return void
	 */
	public function setLocaleImportForm(AdminWebmasterLocaleImportForm $form) {
		$this->_form = $form;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Preprocessing
	 * 
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Make sure the form has been provided:
		if(! $this->_form) {
			// Throw an exception, if not so:
			throw new M_Exception(sprintf(
				'Cannot show form; please provide form to %s::%s()',
				__CLASS__,
				'setLocaleImportForm'
			));
		}
		
		// Assign presentation variables
		$this->assign('form', $this->_form);
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(AdminLoader::getAdminResourcesPath('webmaster') . '/templates/AdminWebmasterLocaleImportForm.tpl');
	}
}