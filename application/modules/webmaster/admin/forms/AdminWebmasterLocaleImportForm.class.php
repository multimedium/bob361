<?php
/**
 * AdminWebmasterLocaleImportForm
 * 
 * @package App
 * @subpackage Webmaster
 */
class AdminWebmasterLocaleImportForm extends M_Form {
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return AdminWebmasterLocaleImportForm
	 */
	public function __construct() {
		// Construct the form:
		parent::__construct('admin-po-import');
		
		// Create a selector, so the target locale can be specified
		$field = new M_FieldSelect('locale');
		$field->setTitle(t('Language'));
		
		// For each of the installed locales:
		foreach(M_LocaleMessageCatalog::getInstalled() as $locale) {
			// Set the option in the field:
			$field->setItem($locale, M_Locale::getLanguageDisplayName($locale));
		}
		
		$field->setMandatory(TRUE);
		$this->addField($field);
		
		// Create a field for the PO File Upload
		$field = new M_FieldUpload('po');
		$field->setTitle(t('Portable Object'));
		$field->setMandatory(TRUE);
		$field->setDirectory($this->_getWebmasterModuleConfig()->temporaryFilesFolder);
		$field->setFilename(md5(time() . rand(0, 2000000)));
		$field->setAllowedExtensions(array('po'));
		$field->setMaxFileSize(1048576); // 1MB
		$this->addField($field);
	}
	
	/* -- M_Form -- */
	
	/**
	 * Form actions
	 *
	 * @see M_Form::actions()
	 * @access public
	 * @param array $values
	 * 		The values that have been collected by the method {@link M_Form::getValues()}.
	 * @return unknown
	 */
	public function actions(array $values) {
		// Also, we log the event of importing translated strings into the
		// application
		M_Console
			::getInstance()
			->write(sprintf(
				'Request for import of PO File in locale "%s", by Administrator %s',
				$values['locale'],
				M_Auth::getInstance()->getIdentity()
			));

		// Get the message catalog, that contains all translated strings
		$mc = M_LocaleMessageCatalog::getInstance($values['locale']);
		
		// Remove cache:
		$cache = $mc->getCacheObject();
		if($cache) {
			$cache->remove();
		}
		
		// The po file:
		$poFile = new M_File($values['po']);
		
		// Now, we construct a Portable Object with the language file
		$po = new M_LocalePortableObject($poFile);

		// Also, we log some information about the PO File:
		M_Console
			::getInstance()
			->write(sprintf(
				'Successfully parsed the PO file %s: %s strings',
				$poFile->getBasename(),
				$po->getIterator()->count()
			));

		// We import the translated strings from the po into the message catalog
		$mc->setPortableObject($po, M_LocaleMessageCatalog::PO_MERGE);
		
		// Remove the original file
		$poFile->delete();

		// Also, we log the event of having imported translated strings into the
		// application
		M_Console
			::getInstance()
			->write(sprintf(
				'PO File "%s" (%s) successfully imported into the application, by Administrator %s',
				$values['locale'],
				$poFile->getBasename(),
				M_Auth::getInstance()->getIdentity()
			));
		
		// Return success
		return TRUE;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get config of webmaster module
	 * 
	 * @access protected
	 * @return MI_Config
	 */
	protected function _getWebmasterModuleConfig() {
		$module = new M_ApplicationModule('webmaster');
		return $module->getConfig();
	}
}