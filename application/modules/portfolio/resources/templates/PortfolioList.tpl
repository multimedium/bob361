{function getProjectHtml project=null span=12}
{$album = $project->getMediaAlbum()}
	{if $album && $album->getImage()}
		{$image = $album->getImage()}
		{$imagePath = {link href="thumbnail/span{$span}/{$image->getBasename()}"}}
		{$imageTitle = $image->getTitle()}
	{else}
		{$imagePath = {link href="application/resources/images/placeholder.jpg" prefix='false'}}
		{t text='No image available' assignto='imageTitle'}
	{/if}
	<div id="item-{$project->getId()}" class="span{$span} project">
		<a href="{link href="projects/detail/{$project->getUrl()}"}" title="{$project->getName()}">
			
			{* Info *}
			<div class="project-info">
				{if $project->getCategory()}
				<div class="project-info-title" style="color: #{$project->getTitleColor()};">
					{$project->getCategory()->getCategoryTitle()}
				</div>
				{/if}
				<div class="project-info-sub-title" style="color: #{$project->getSubTitleColor()};">
					{$project->getName()}, {$project->getCity()}
				</div>
			</div>

			{* Image*}
			<div class="project-info-image">
				<img src="{link href="blank.gif" type="images"}" data-original="{$imagePath}" alt="{$imageTitle}" class="lazy"/>
			</div>			
			
		</a>
	</div>
{/function}

{if $teamMember}
	<div class="row">
		<div class="span12">
			<h1>{$teamMember->getFullName()}</h1>
		</div>
	</div>
{/if}


{$total = sizeof($projects)}
{$segments = ($total/6)|floor}
{$remaining = $total - ($segments*6)}
{$index = 0}
{$projects = $projects->getArrayCopy()}
{for $foo=1 to $segments}
	<div class="row">
		{getProjectHtml project=$projects[$index++] span=6}
		{getProjectHtml project=$projects[$index++] span=6}
	</div>
	<div class="row">
		{getProjectHtml project=$projects[$index++] span=4}
		{getProjectHtml project=$projects[$index++] span=4}
		{getProjectHtml project=$projects[$index++] span=4}
	</div>
	<div class="row">
		{getProjectHtml project=$projects[$index++] span=12}
	</div>
{/for}

{if $remaining > 0}
	<div class="row">
		{if $projects[$index]}{getProjectHtml project=$projects[$index] span=6}{/if}
		{$index = $index+1}
		{if $projects[$index]}{getProjectHtml project=$projects[$index] span=6}{/if}
		{$index = $index+1}
	</div>
	<div class="row">
		{if $projects[$index]}{getProjectHtml project=$projects[$index] span=4}{/if}
		{$index = $index+1}
		{if $projects[$index]}{getProjectHtml project=$projects[$index] span=4}{/if}
		{$index = $index+1}
		{if $projects[$index]}{getProjectHtml project=$projects[$index] span=4}{/if}
		{$index = $index+1}
	</div>
	<div class="row">
		{if $projects[$index]}{getProjectHtml project=$projects[$index] span=12}{/if}
	</div>
{/if}

<div class="row">
	<div class="span12">
		{include file='application/resources/templates/_Pager.tpl' pager=$pager}
	</div>
</div>
