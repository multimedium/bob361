{$relatedProjects = $project->getRelatedProjects()}
{$album = $project->getMediaAlbum()}

{if $album}
	{$images = $album->getImages()}
	{$count = $images|@count}
	{if $count == 1}
	<div class="row row-large">
		<div class="span12 project">
			<a href="" title="">
				{if $project->getCategory()}
					<h1 style="color: #{$project->getTitleColor()};">{$project->getCategory()->getCategoryTitle()}</h1>
				{/if}
				<h2 style="color: #{$project->getSubTitleColor()};">{$project->getName()}, {$project->getCity()}</h2>
				<div class="image-wrapper image-wrapper-big">
					{$images = $album->getImages()}
					{foreach $images as $image}
						{$imageTitle = $image->getTitle()}
						{$imagePath = {link href="thumbnail/span12slider/{$image->getBasename()}"}}
						<img src="{$imagePath}" alt="{$imageTitle}" title="{$imageTitle}"/>
					{/foreach}
				</div>
			</a>
		</div>
	</div>	
	{elseif $count > 0}
	<div class="row row-large">
		<div class="span12 project">
			<div class="flexslider project-slider">
				<div class="nav">
					<a class="nav-control previous pull-left" href="#"></a>
					<a class="nav-control next pull-right" href="#"></a>
				</div>
				<ul class="slides">
				{foreach $images as $image}
					{$imageTitle = $image->getTitle()}
					{$imagePath = {link href="thumbnail/span12slider/{$image->getBasename()}"}}
					<li class="slide">
						<img src="{$imagePath}" alt="{$imageTitle}" title="{$imageTitle}"/>
						<div class="flex-caption">
							{if $project->getCategory()}
								<p style="color: #{$project->getTitleColor()};"><strong>{$project->getCategory()->getCategoryTitle()}</strong></p>
							{/if}
							&#8226; <p style="color: #{$project->getSubTitleColor()};">{$project->getName()}, {$project->getCity()}</p>
						</div>
					</li>
				{/foreach}
				</ul>
			</div>
		</div>
	</div>	
	{/if}
{/if}

		
<div class="row">
	<div class="span12">
		<p>
			{$project->getIntroductionText()}
		</p>
		<p>
			{$project->getDescriptionText()}
		</p>
	</div>
</div>

{$projectLeader = $project->getProjectLeader()}

<div class="row row-large">
	<div class="span2">
		<ul class="project-detail">
			<li>Location</li>
			<li>{$project->getCity()}</li>
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>Size</li>
			<li>{$project->getSurface()} m2</li>
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>Budget</li>
			<li>{if $project->getBudget() > 0}{number n=$project->getBudget() format="currency" currency="EUR"}{else}-{/if}</li>
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>Status</li>
			<li>{$project->getStatus()}</li>
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>Type</li>
			<li>
				{if $project->getTypeForDisplay() != false}
					{$project->getTypeForDisplay()}
				{else}
					/
				{/if}
			</li>
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>Date</li>
			<li>{$project->getRealizationDateText()}</li>
		</ul>
	</div>		
</div>
		
<div class="row">
	<div class="span2">
		<ul class="project-detail">
			<li>Client</li>
			<li>{$project->getClient()}</li>
		</ul>
	</div>		
	<div class="span2">
		<ul class="project-detail">
			<li>Team</li>
			{*if $projectLeader}
				<li>{$project->getProjectLeader()->getFirstName()} {$project->getProjectLeader()->getSurnames()}</li>
			{/if*}
			{foreach $project->getTeamMembers() as $teamMember}
				<li>{$teamMember->getFullName()}</li>
			{/foreach}
		</ul>
	</div>
	<div class="span2">
		<ul class="project-detail">
			<li>External consultation</li>
			{foreach $project->getNetworkMembers() as $networkMember}
				<li>{$networkMember->getTitle()}</li>
			{/foreach}
		</ul>
	</div>
</div>

{if $relatedProjects}
<div class="row">
	<div class="span12">
		<hr/>
		<h1 class="title-light">Related projects</h1>
		<ul class="project-list">
			{foreach $relatedProjects as $relatedProject}
				{if $relatedProject->getId() != $project->getId()}
				<li class="project">
					<a href="{link href="projects/detail/{$relatedProject->getUrl()}"}" title="{$relatedProject->getName()}">
						
						{* Info *}
						<div class="project-info">
							{if $project->getCategory()}
								<div class="project-info-title" style="color: #{$relatedProject->getTitleColor()};">
									{$relatedProject->getCategory()->getCategoryTitle()}
								</div>
								<div class="project-info-sub-title" style="color: #{$relatedProject->getSubTitleColor()};">
									{$relatedProject->getName()}, {$relatedProject->getCity()}
								</div>
							{/if}
						</div>
						
						{* Image*}
						<div class="project-info-image">
							{$albumRelated = $relatedProject->getMediaAlbum()}
							{if $albumRelated && $albumRelated->getImage()}
								{$imageRelated = $albumRelated->getImage()}
								{$imagePathRelated = {link href="thumbnail/span4/{$imageRelated->getBasename()}"}}
								{$imageTitleRelated = $imageRelated->getTitle()}
							{else}
								{$imagePathRelated = {link href="application/resources/images/placeholder.jpg" prefix='false'}}
								{t text='No image available' assignto='imageTitle'}
							{/if}
							<img src="{$imagePathRelated}" alt="{$imageTitleRelated}" />
						</div>
						
					</a>
				</li>
				{/if}
			{/foreach}
		</ul>
	</div>
</div>
{/if}