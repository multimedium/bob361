<?php
/**
 * PortfolioEventListener
 * 
 * @package App
 * @subpackage Portfolio
 */
class PortfolioEventListener {

	/**
	 * Construct
	 * 
	 * @access public
	 * @param AdminController $controller
	 */
	public function __construct(AdminController $controller) {
		// Add an event listener for editing items
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_EDIT,
			array($this, 'onDataObjectBeforeEdit')
		);

		// Add an event listener for creating items
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_SAVE,
			array($this, 'onDataObjectSave')
		);
	}

	/**
	 * Do ... when object is about to be edited
	 * 
	 * @access public
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectBeforeEdit(AdminControllerEvent $event) {

		// Fetch the data object
		$project = $event->getDataObject();
		/* @var Project $project */

		// Make sure it is a news
		if(! M_Helper::isInstanceOf($project, 'Project')) return false;
		
		// Set default values for TeamMembers
		$this->_setTeamMembersDefaultValues($event);
		
		// Set default values for NetworkMembers
		$this->_setNetworkMembersDefaultValues($event);
		
	}

	/**
	 * Do ... when object is about to be saved
	 * 
	 * @access public
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectSave(AdminControllerEvent $event) {

		// Fetch the data object
		$project = $event->getDataObject();
		/* @var $project Project */

		// Make sure it is a news
		if(! M_Helper::isInstanceOf($project, 'Project')) return false;
		
		// Store TeamMembers
		$this->_storeTeamMembers($event);

		// Store NetworkMembers
		$this->_storeNetworkMembers($event);
		
	}

	/*-- PROTECTED FUNCTIONS --*/
	
	/**
	 * Set Team Members Default Values
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _setTeamMembersDefaultValues(AdminControllerEvent $event) {
		
		// Variables
		$project = $event->getDataObject();
		$field = $event->getForm()->getField('teamMembers');
		
		// Get ProjectXTeamMembers
		$mapper = M_Loader::getDataObjectMapper('ProjectXTeamMember', 'portfolio');
		/* @var $mapper ProjectXTeamMemberMapper */
		$projectXTeamMembers = $mapper
			->addFilterProjectId($project->getId())
			->getAll();
		
		// Collect TeamMember ids
		$teamMemberIds = array();
		foreach ($projectXTeamMembers as $projectXTeamMember) {
			array_push($teamMemberIds, $projectXTeamMember->getTeamMemberId());
		}
		
		// Set Default values
		$field->setDefaultValue($teamMemberIds);
		
	}
	
	/**
	 * Set Network Members Default Values
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _setNetworkMembersDefaultValues(AdminControllerEvent $event) {
		
		// Variables
		$project = $event->getDataObject();
		$field = $event->getForm()->getField('networkMembers');
		
		// Get ProjectXNetworkMembers
		$mapper = M_Loader::getDataObjectMapper('ProjectXNetworkMember', 'portfolio');
		/* @var $mapper ProjectXNetworkMemberMapper */
		$projectXNetworkMembers = $mapper
			->addFilterProjectId($project->getId())
			->getAll();
		
		// Collect NetworkMember ids
		$networkMemberIds = array();
		foreach ($projectXNetworkMembers as $projectXNetworkMember) {
			array_push($networkMemberIds, $projectXNetworkMember->getNetworkMemberId());
		}
		
		// Set Default values
		$field->setDefaultValue($networkMemberIds);
		
	}
	
	/**
	 * Store Team Members
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _storeTeamMembers(AdminControllerEvent $event) {
		
		// Get the field values for "team members"
		$project = $event->getDataObject();
		$form = $event->getForm();
		$values = $form->getField('teamMembers')->deliver();
		
		// Delete all ProjectXTeamMembers before creating new ones
		$mapper = M_Loader::getDataObjectMapper('ProjectXTeamMember', 'portfolio');
		/* @var $mapper ProjectXTeamMemberMapper */
		$mapper->deleteByProjectId($project->getId());
		
		// Load necessary classes
		M_Loader::loadModel('ProjectXTeamMember', 'portfolio');
		
		// Store the team members
		foreach($values as $value) {
			
			$teamMember = new ProjectXTeamMember();
			$teamMember
				->setTeamMemberId($value)
				->setProjectId($project->getId())
				->save();
			
		}		
		
	}
	
	/**
	 * Store Network Members
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _storeNetworkMembers(AdminControllerEvent $event) {
		
		// Get the field values for "network members"
		$project = $event->getDataObject();
		$form = $event->getForm();
		$values = $form->getField('networkMembers')->deliver();
		
		// Delete all ProjectXNetworkMembers before creating new ones
		$mapper = M_Loader::getDataObjectMapper('ProjectXNetworkMember', 'portfolio');
		/* @var $mapper ProjectXNetworkMemberMapper */
		$mapper->deleteByProjectId($project->getId());
		
		// Load necessary classes
		M_Loader::loadModel('ProjectXNetworkMember', 'portfolio');
		
		// Store the network members
		foreach($values as $value) {
			
			$networkMember = new ProjectXNetworkMember();
			$networkMember
				->setNetworkMemberId($value)
				->setProjectId($project->getId())
				->save();
			
		}		
		
	}
	
}