<?php
/**
 * PortfolioController
 *
 * @package App
 * @subpackage Team
 */
class PortfolioController extends PageController {
	
	/**
	 * Routing
	 * 
	 * @see M_Controller::__router()
	 * @access public
	 * @return array
	 */
	public function __router() {
		return array(
			'index'					=> 'index',
			'member/(:any)'			=> 'member/$1',
			'detail/(:any)'			=> 'detail/$1'
		);
	}
		
	
	/**
	 * Index
	 * 
	 * @access public
	 * @return void
	 */
	public function index() {
		$view = M_Loader::getView('PortfolioListView', 'portfolio');
		/* @var $view PortfolioListView */
		$view
			->setActivePath('projects')
			->setPageTitle('Projects')
			->setPageDescription('Projects')
			->setProjectList(
                M_Loader
                    ::getDataObjectMapper('Project', 'portfolio')
                    ->addFilterIsPublished()
                    ->addFilterDefaultSorting()
                    ->getAll()
            )
			->display();
	}
	
	/**
	 * Member
	 * 
	 * @access public
	 * @return void
	 */
	public function member($url) {
		
		// Get projects
		$mapper = M_Loader::getDataObjectMapper('Project', 'portfolio');
		/* @var $mapper ProjectMapper */

		// Get the project Ids
		$projectIds = $this->_getProjectIdsByTeamMemberUrl($url);
		
		$pager = $this->_getPagejumper(sizeof($projectIds), 6);
		$projects = $mapper
			->addFilterIsPublished()
			->addFilter(new M_DbQueryFilterPagejumper($pager))
			->getByIds($projectIds);
		
		$view = M_Loader::getView('PortfolioListView', 'portfolio');
		/* @var $view PortfolioListView */
		// Set some additional page properties
		$view
			->setActivePath('projects')
			->setPageTitle('Projects')
			->setPageDescription('Projects')
			->setProjectList($projects)
			->setPager($pager);
		
		if($this->_getTeamMemberByUrl($url)) {
			$view->setTeamMember($this->_getTeamMemberByUrl($url));
		}
		
		$view->display();
	}

	/**
	 * Detail
	 * 
	 * @access public
	 * @param string $url
	 * @return void
	 */
	public function detail($url) {
		$mapper = M_Loader::getDataObjectMapper('Project', 'portfolio');
		/* @var $mapper ProjectMapper */
		
		// fetch the requested project
		$project = $mapper
			->addFilterIsPublished()
			->getByUrlSuffix($url);
		
		if (! $project) {
			$this->_404();
		}
		
		$view = M_Loader::getView('PortfolioDetailView', 'portfolio');
		
		$images = $project->getImages();
		if($images) {
			$image = $images->first();
			$imageUrl = M_Request::getLink("thumbnail/span12slider/".$image->getBasename());
			$view->setOgImageUrl($imageUrl);
		}
		
		/* @var $view PortfolioDetailView */
		$view
			->setActivePath('projects')
			->setPageTitle($project->getName())
			->setPageDescription($project->getDescriptionText())
			->setProject($project)
			->display();
	}
	
	/*-- PROTECTED FUNCTIONS --*/
	
	/**
	 * Get Team Member By URL
	 * 
	 * @access protected
	 * @param type $url
	 * @return TeamMember || NULL
	 */
	protected function _getTeamMemberByUrl($url) {
		
		// Get the TeamMember Id for this url
		$teamMemberMapper = M_Loader::getDataObjectMapper('TeamMember', 'team');
		/* @var $teamMemberMapper TeamMemberMapper */

		$teamMember = $teamMemberMapper
			->getOneByFieldWithRegistryCache('url', $url);
		
		return $teamMember;
		
	}
	
	/**
	 * Get Project Ids by Team Member Url
	 * 
	 * @access public
	 * @param mixed $teamMemberId
	 * @return array()
	 */	
	protected function _getProjectIdsByTeamMemberUrl($url) {
		
		// Get the TeamMember Id for this url
		$teamMember = $this->_getTeamMemberByUrl($url);
		
		if( ! $teamMember) {
			return NULL;
		}
		// Get All ProjectXTeamMembers 
		$mapper = M_Loader::getDataObjectMapper('ProjectXTeamMember', 'portfolio');
		/* @var $mapper ProjectXTeamMemberMapper */
		$projectIds = $mapper->getProjectsIdsByTeamMemberId($teamMember->getId());
		
		return $projectIds;
		
	}
}