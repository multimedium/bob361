<?php
/**
 * PortfolioListView
 *
 * @package App
 * @subpackage Portfolio
 */
class PortfolioListView extends PageView {
	
	/**
	 * Set Project List
	 * 
	 * @access public
	 * @param M_ArrayIterator $projects
	 * @return PortfolioListView
	 */
	public function setProjectList(M_ArrayIterator $projects) {
		return $this->assign('projects', $projects);
	}
	
//	/**
//	 * Set Pager
//	 * 
//	 * @access public
//	 * @param M_Pagejumper $pager
//	 * @return PortfolioListView
//	 */
//	public function setPager(M_Pagejumper $pager) {
//		return $this->assign('pager', $pager);
//	}
	
	/**
	 * Set Team Member
	 * 
	 * @access public
	 * @param TeamMember $teamMember
	 * @return PortfolioListView
	 */
	public function setTeamMember(TeamMember $teamMember) {
		return $this->assign('teamMember', $teamMember);
	}
	
	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get the resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('portfolio', 'PortfolioList.tpl');
	}
}