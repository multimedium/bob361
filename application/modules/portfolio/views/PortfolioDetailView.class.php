<?php
/**
 * PortfolioDetailView
 *
 * @package App
 * @subpackage Portfolio
 */
class PortfolioDetailView extends PageView {
	
	public function setProject(M_Object $project) {
		return $this->assign('project', $project);
	}
	
	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get the resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('portfolio', 'PortfolioDetail.tpl');
	}
}