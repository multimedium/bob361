<?php
/**
 * ProjectXTeamMemberMapper
 * 
 * @package App
 * @subpackage Portfolio
 */
class ProjectXTeamMemberMapper extends M_DataObjectMapper {
	
	/**
	 * Add Filter: Project Id
	 * 
	 * @access public
	 * @param mixed $projectId
	 * @return ProjectXTeamMemberMapper
	 */
	public function addFilterProjectId($projectId) {
		return $this->addFilter(new M_DbQueryFilterWhere('projectId', (int) $projectId, M_DbQueryFilterWhere::EQ));
	}

	/**
	 * Add Filter: Team Member Id
	 * 
	 * @access public
	 * @param mixed $teamMemberId
	 * @return ProjectXTeamMemberMapper
	 */
	public function addFilterTeamMemberId($teamMemberId) {
		return $this->addFilter(new M_DbQueryFilterWhere('teamMemberId', (int) $teamMemberId, M_DbQueryFilterWhere::EQ));
	}
	
	/**
	 * Get Team Members by Project Id
	 * 
	 * @access public
	 * @param type $projectId
	 * @return M_ArrayIterator || NULL
	 */
	public function getTeamMembersByProjectId($projectId) {
		
		// Get TeamMemberIds
		$teamMemberIds = $this->getTeamMembersIdsByProjectId($projectId);
		
		if(!$teamMemberIds) {
			return NULL;
		}
		
		// Get all TeamMembers
		$teamMemberMapper = M_Loader::getDataObjectMapper('TeamMember', 'team');
		$teamMembers = $teamMemberMapper
			->getByIds($teamMemberIds);
		
		return $teamMembers;
		
	}

	/**
	 * Get Team Members Ids by Project Id
	 * 
	 * @access public
	 * @param type $projectId
	 * @return M_ArrayIterator || NULL
	 */
	public function getTeamMembersIdsByProjectId($projectId) {
		
		// New instance of this mapper
		$projectXTeamMemberMapper = new self();
		
		// Fetch all ProjectTeamMembers for this project id
		$projectXTeamMembers = $projectXTeamMemberMapper
			->addFilterProjectId($projectId)
			->getAll();
		
		// Collect TeamMember ids
		$teamMemberIds = array();
		foreach($projectXTeamMembers as $projectXTeamMember) {
			array_push($teamMemberIds, $projectXTeamMember->getTeamMemberId());
		}
		
		if (empty($teamMemberIds)) {
			return NULL;
		}
		
		return $teamMemberIds;
		
	}	
	
	/**
	 * Get Projects by Team Member Id
	 * 
	 * @access public
	 * @param type $projectId
	 * @return M_ArrayIterator || NULL
	 */
	public function getProjectsByTeamMemberId($teamMemberId) {
		
		// Get project Ids
		$projectIds = $this->getProjectsIdsByTeamMemberId($teamMemberId);
		
		// Get all Projects
		$projectMapper = M_Loader::getDataObjectMapper('Project', 'portfolio');
		$projects = $projectMapper
			->getByIds($projectIds);
		
		return $projects;
		
	}
	
	/**
	 * Get ProjectIds by Team Member Id
	 * 
	 * @access public
	 * @param type $projectId
	 * @return M_ArrayIterator || NULL
	 */
	public function getProjectsIdsByTeamMemberId($teamMemberId) {
		
		// New instance of this mapper
		$projectXTeamMemberMapper = new self();
		
		// Fetch all ProjectTeamMembers for this team member id
		$projectXTeamMembers = $projectXTeamMemberMapper
			->addFilterTeamMemberId($teamMemberId)
			->getAll();
		
		// Collect Project ids
		$projectIds = array();
		foreach($projectXTeamMembers as $projectXTeamMember) {
			array_push($projectIds, $projectXTeamMember->getProjectId());
		}
		
		if (empty($projectIds)) {
			return NULL;
		}
		
		return $projectIds;
		
	}	
	
	/**
	 * Delete By Project Id
	 * 
	 * @access public
	 * @param mixed $projectId
	 * @return void
	 */
	public function deleteByProjectId($projectId) {
		
		// New instance of this mapper
		$mapper = new self();
		
		// Fetch all ProjectTeamMembers for this project id
		$projectXTeamMembers = $mapper
			->addFilterProjectId($projectId)
			->getAll();
		
		// Delete all projectXTeamMembers
		foreach($projectXTeamMembers as $projectXTeamMember) {
			$projectXTeamMember->delete();
		}
		
	}
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'portfolio';
	}
	
}