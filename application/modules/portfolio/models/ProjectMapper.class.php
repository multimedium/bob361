<?php
// Load superclass
M_Loader::loadDataObjectMapper('SearchableDataObject', 'search');
/**
 * ProjectMapper
 * 
 * @package App
 * @subpackage Portfolio
 */
class ProjectMapper extends SearchableDataObjectMapper {
	
	/**
	 * Project types
	 */
	const TYPE_COMPETITION = 1;
	const TYPE_OTHER = 2;
	const TYPE_COMPETITION_1ST = 3;
	const TYPE_COMPETITION_2ND = 4;
	const TYPE_COMPETITION_3RD = 5;
	const TYPE_OPEN_CALL = 6;
	const TYPE_DIRECT_ORDER = 7;

	/**
	 * Project statusses
	 */
	const STATUS_COMPETITION = 1;
	const STATUS_PRELIMINARY_DESIGN = 2;
	const STATUS_WORKING_DRAWINGS = 3;
	const STATUS_BUILDING_SITE = 4;
	const STATUS_COMPLETED = 5;

    /**
     * Sorting priorities
     */
    const SORTING_PRIORITY_A = 1;
    const SORTING_PRIORITY_B = 2;
    const SORTING_PRIORITY_C = 3;

	/**
	 * Get Project Type options
	 * 
	 * return an array of all available ptoject types, that can also
	 * be used as an option list on a select field
	 * 
	 * @return array
	 */
	public static function getProjectTypeOptions() {
		return array(
			self::TYPE_COMPETITION => t('Competition'),
			self::TYPE_COMPETITION_1ST => t('Competition 1st Price'),
			self::TYPE_COMPETITION_2ND => t('Price Competition 2nd'),
			self::TYPE_COMPETITION_3RD => t('3rd Price Competition'),
			self::TYPE_OPEN_CALL => t('Open oproep'),
			self::TYPE_DIRECT_ORDER => t('Direct order'),
			self::TYPE_OTHER => t('Other')
		);
	}

	/**
	 * Get Project Status options
	 * 
	 * return an array of all available ptoject types, that can also
	 * be used as an option list on a select field
	 * 
	 * @return array
	 */
	public static function getProjectStatusOptions() {
		return array(
			self::STATUS_COMPETITION => t('Competition'),
			self::STATUS_PRELIMINARY_DESIGN => t('Preliminary design'),
			self::STATUS_WORKING_DRAWINGS => t('Working drawings'),
			self::STATUS_BUILDING_SITE => t('Building site'),
			self::STATUS_COMPLETED => t('Completed')
		);
	}

    /**
     * Get sorting priority options
     *
     * @return array
     */
    public static function getSortingPriorityOptions() {
        return array(
            self::SORTING_PRIORITY_A => t('A (first)'),
            self::SORTING_PRIORITY_B => t('B'),
            self::SORTING_PRIORITY_C => t('C (last)')
        );
    }
	
	/**
	 * Add Filter: Is Published
	 * 
	 * @access public
	 * @param boolean $published
	 * @return ProjectMapper
	 */
	public function addFilterIsPublished($published = true) {
		return $this->addFilter(new M_DbQueryFilterWhere('published', (bool) $published));
	}

    /**
     * Add Filter: Is Published Only
     *
     * for searchableDataObject
     *
     * @access public
     * @return ProjectMapper
     */
    public function addFilterPublishedOnly() {
        return $this->addFilterIsPublished();
    }
	
	/**
	 * Add Filter: Is New
	 * 
	 * @access public
	 * @param boolean $new
	 * @return ProjectMapper
	 */
	public function addFilterIsNew($new = true) {
		return $this->addFilter(new M_DbQueryFilterWhere('new', (bool) $new));
	}

    /**
     * Add Filter: "highlight"
     *
     * @access public
     * @return ProjectMapper
     */
    public function addFilterHighlight() {
        return $this->addFilter(new M_DbQueryFilterWhere('highlight', 1));
    }

	/**
	 * Add filter: date before 2009
	 *
	 * @param $date M_Date
	 * @param $operator string
	 * @access public
	 * @return ProjectMapper
	 */
	public function addFilterDate($date, $operator)
	{
		return $this->addFilter(new M_DbQueryFilterWhere('realizationDate', $date, $operator));
	}

	/**
	 * Add filter: sort by start date
	 *
	 * @access public
	 * @return ProjectMapper
	 */
	public function addFilterSortByDate()
	{
		return $this
				->addFilter(new M_DbQueryFilterOrder('realizationDate', 'ASC'));
	}

    /**
     * Add filter: default sorting
     *
     * @return $this
     */
	public function addFilterDefaultSorting() {
        return $this
            ->addFilter(new M_DbQueryFilterOrder('sortingPriority', 'ASC'))
            ->addFilter(new M_DbQueryFilterOrderRandom())
        ;
    }

	/**
	 * Get by URL
	 * 
	 * Will try to fetch a unique object that matches the provided URL suffix. 
	 * Returns an instance of {@link Text}, or NULL if no matching 
	 * object could have been found.
	 * 
	 * @access public
	 * @param string $urlSuffix
	 * 		The URL Suffix to search with
	 * @return Project
	 * 		The matching object, or NULL if not found
	 */
	public function getByUrlSuffix($urlSuffix) {
		// Compose the SELECT statement, and execute
		$rs = $this->_getFilteredSelect(
			array(
				new M_DbQueryFilterWhere('url', (string) $urlSuffix)
			)
		)
			->limit(0, 1)
			->execute();
		
		// If the result set contains the record we are looking for:
		if($rs && $rs->count() == 1) {
			// Return the object:
			return $this->_createObjectFromRecord($rs->current());
		}
		
		// Return NULL if still here
		// (If still here, it means that we did not find any matching object)
		return NULL;
	}

	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'portfolio';
	}	
	
}