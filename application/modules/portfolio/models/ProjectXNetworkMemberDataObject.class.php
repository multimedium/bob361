<?php
/** 
 * ProjectXNetworkMemberDataObject
 * 
 * @see ProjectXNetworkMemberMapper
 * @see ProjectXNetworkMember
 * @package App
 * @subpackage Portfolio
 */
class ProjectXNetworkMemberDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link ProjectXNetworkMemberDataObject::getId()}
	 * - {@link ProjectXNetworkMemberDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * projectId
	 * 
	 * This property stores the projectId. To get/set the value of the
	 * projectId, read the documentation on 
	 * 
	 * - {@link ProjectXNetworkMemberDataObject::getProjectId()}
	 * - {@link ProjectXNetworkMemberDataObject::setProjectId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_projectId;
	
	/**
	 * networkMemberId
	 * 
	 * This property stores the networkMemberId. To get/set the value of the
	 * networkMemberId, read the documentation on 
	 * 
	 * - {@link ProjectXNetworkMemberDataObject::getNetworkMemberId()}
	 * - {@link ProjectXNetworkMemberDataObject::setNetworkMemberId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_networkMemberId;
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return ProjectXNetworkMemberDataObject
	 */
	public function setId($id) {
		$this->_id = $id;
		return $this;
	}
	
	/**
	 * Get projectId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getProjectId() {
		return $this->_projectId;
	}
	
	/**
	 * Set projectId
	 * 
	 * @access public
	 * @param mixed $projectId
	 * @return ProjectXNetworkMemberDataObject
	 */
	public function setProjectId($projectId) {
		$this->_projectId = $projectId;
		return $this;
	}
	
	/**
	 * Get networkMemberId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getNetworkMemberId() {
		return $this->_networkMemberId;
	}
	
	/**
	 * Set networkMemberId
	 * 
	 * @access public
	 * @param mixed $networkMemberId
	 * @return ProjectXNetworkMemberDataObject
	 */
	public function setNetworkMemberId($networkMemberId) {
		$this->_networkMemberId = $networkMemberId;
		return $this;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return ProjectXNetworkMemberMapper $mapper
	 */
	public function getMapper() {
		return M_Loader::getDataObjectMapper('ProjectXNetworkMember', 'portfolio');
	}
	
}