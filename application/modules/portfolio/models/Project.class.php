<?php
/**
 * Project
 * 
 * @package App
 * @subpackage Portfolio
 */

// Load superclass
M_Loader::loadDataObject('ProjectDataObject','portfolio');

/**
 * Project
 * 
 * @package App
 * @subpackage Portfolio
 */
class Project extends ProjectDataObject {
	
	/**
	 * Get Status
	 * 
	 * @access public
	 * @return string
	 * 
	 */
	public function getStatus() {
		$statusId = $this->getStatusId();
		
		if( ! $statusId) {
			return false;
		}
		M_Loader::loadDataObjectMapper('Project', 'portfolio');
		$statuses = ProjectMapper::getProjectStatusOptions();
		return $statuses[$statusId];
	}
	
	/**
	 * Get Team Members
	 * 
	 * @access public
	 * @return M_ArrayIterator||NULL
	 */
	public function getTeamMembers() {
		
		// Get ProjectXTeamMembers
		$mapper = M_Loader::getDataObjectMapper('ProjectXTeamMember', 'portfolio');
		/* @var $mapper ProjectXTeamMemberMapper */
		$teamMembers = $mapper->getTeamMembersByProjectId($this->getId());
		
		return $teamMembers;
		
	}
	
	/**
	 * Get Network Members
	 * 
	 * @access public
	 * @return M_ArrayIterator||NULL
	 */
	public function getNetworkMembers() {
		
		// Get ProjectXTeamMembers
		$mapper = M_Loader::getDataObjectMapper('ProjectXNetworkMember', 'portfolio');
		/* @var $mapper ProjectXNetworkMemberMapper */
		$networkMembers = $mapper->getNetworkMembersByProjectId($this->getId());
		
		return $networkMembers;
		
	}
	
	/**
	 * Set title
	 * 
	 * @access public
	 * @param mixed $text
	 * @return mixed
	 */
	public function setName($text, $locale = NULL) {
		// Set the title
		parent::setName($text, $locale);
		
		// Set the URL (suffix), if not already done:
		if(! $this->getUrl($locale)) {
			$this->setUrl($text, $locale);
		}
	}
	
	/**
	 * set url
	 * 
	 * @param type $text
	 * @param type $locale
	 */
	public function setUrl($text, $locale) {
		// convert input to url
		$url = M_Uri::getPathElementFromString($text);
		
		// prepare the mapper
		$mapper = new ProjectMapper();
		// add filter for url
		$mapper->addFilter(new M_DbQueryFilterWhere('url', $url));
		// if already in DB, exclude self
		if (! $this->isNew()) {
			$mapper->addFilter(new M_DbQueryFilterWhere('id', $this->getId(), M_DbQueryFilterWhere::NEQ));
		}
		// if found, we add the alphaid to the url an try again
		if ($mapper->getCount() > 0) {
			$this->setUrl($text . ' ' . M_Helper::getRandomString(4, true, false), $locale);
		} else {
			parent::setUrl($url);
		}
	}
	
	/**
	 * get type for display
	 * 
	 * returns a text representation of the project type
	 * 
	 * @return string
	 */
	public function getTypeForDisplay() {
		// fetch all available types
		$types = ProjectMapper::getProjectTypeOptions();
		// check if the current type is valid
		if (array_key_exists($this->getTypeId(), $types)) {
			return $types[$this->getTypeId()];
		}
		return t('unkown');
	}
	
	public function getAddress() {
		$address = new M_ContactAddress();
		$address->setStreetName($this->getStreetName());
		$address->setStreetNumber($this->getStreetNumber());
		$address->setPostalCode($this->getZipCode());
		$address->setCity($this->getCity());
		$address->setCountryISO($this->getCountryIso());
		$address->setLatitude($this->getLatitude());
		$address->setLongitude($this->getLongitude());
		return $address;
	}
	
	public function setAddress(M_ContactAddress $address) {
		$this->setStreetName($address->getStreetName());
		$this->setStreetNumber($address->getStreetNumber());
		$this->setZipCode($address->getPostalCode());
		$this->setCity($address->getCity());
		$this->setCountryIso($address->getCountryISO());
		$this->setLatitude($address->getLatitude());
		$this->setLongitude($address->getLongitude());
	}
	
	public function getCountry() {
		return M_Locale::getTerritoryDisplayName($this->getCountryIso());
	}
	
	public function getCategory() {
		M_Loader::loadDataObjectMapper('ProjectType', 'portfolio');
		$mapper = new ProjectTypeMapper();
		return $mapper->getById($this->getCategoryId());
	}
	
	public function getProjectLeader() {
		M_Loader::loadDataObjectMapper('TeamMember', 'team');
		$mapper = new TeamMemberMapper();
		return $mapper->getById($this->getProjectLeaderId());
	}
	
	public function getRelatedProjects() {
		M_Loader::loadDataObjectMapper('Project', 'portfolio');
		$mapper = new ProjectMapper();
		$mapper	-> addFilterIsPublished()->addFilter(new M_DbQueryFilterWhere('categoryId', $this->getCategoryId(), M_DbQueryFilterWhere::EQ));
		return $mapper->getAll();
	}
	
	/**
	 * get media album
	 * 
	 * @return MediaAlbum
	 */
	public function getMediaAlbum() {
		// construct media album mapper
		M_Loader::loadDataObjectMapper('MediaAlbum', 'media');
		$mapper = new MediaAlbumMapper();
		
		return $mapper->addFilterPublished()->getAlbumByObject($this);
	}
	
	/**
	 * get Images
	 * 
	 * @return M_ArrayIterator
	 */
	public function getImages() {
		$album = $this->getMediaAlbum();
		return $album->getImages();
	}
}