<?php
/** 
 * ProjectXTeamMemberDataObject
 * 
 * @see ProjectXTeamMemberMapper
 * @see ProjectXTeamMember
 * @package App
 * @subpackage Portfolio
 */
class ProjectXTeamMemberDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link ProjectXTeamMemberDataObject::getId()}
	 * - {@link ProjectXTeamMemberDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * projectId
	 * 
	 * This property stores the projectId. To get/set the value of the
	 * projectId, read the documentation on 
	 * 
	 * - {@link ProjectXTeamMemberDataObject::getProjectId()}
	 * - {@link ProjectXTeamMemberDataObject::setProjectId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_projectId;
	
	/**
	 * teamMemberId
	 * 
	 * This property stores the teamMemberId. To get/set the value of the
	 * teamMemberId, read the documentation on 
	 * 
	 * - {@link ProjectXTeamMemberDataObject::getTeamMemberId()}
	 * - {@link ProjectXTeamMemberDataObject::setTeamMemberId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_teamMemberId;
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return ProjectXTeamMemberDataObject
	 */
	public function setId($id) {
		$this->_id = $id;
		return $this;
	}
	
	/**
	 * Get projectId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getProjectId() {
		return $this->_projectId;
	}
	
	/**
	 * Set projectId
	 * 
	 * @access public
	 * @param mixed $projectId
	 * @return ProjectXTeamMemberDataObject
	 */
	public function setProjectId($projectId) {
		$this->_projectId = $projectId;
		return $this;
	}
	
	/**
	 * Get teamMemberId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getTeamMemberId() {
		return $this->_teamMemberId;
	}
	
	/**
	 * Set teamMemberId
	 * 
	 * @access public
	 * @param mixed $teamMemberId
	 * @return ProjectXTeamMemberDataObject
	 */
	public function setTeamMemberId($teamMemberId) {
		$this->_teamMemberId = $teamMemberId;
		return $this;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return ProjectXTeamMemberMapper $mapper
	 */
	public function getMapper() {
		return M_Loader::getDataObjectMapper('ProjectXTeamMember', 'portfolio');
	}
	
}