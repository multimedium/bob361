<?php
/**
 * ProjectXNetworkMemberMapper
 * 
 * @package App
 * @subpackage Portfolio
 */
class ProjectXNetworkMemberMapper extends M_DataObjectMapper {
	
	/**
	 * Add Filter: Project Id
	 * 
	 * @access public
	 * @param mixed $projectId
	 * @return ProjectXNetworkMemberMapper
	 */
	public function addFilterProjectId($projectId) {
		return $this->addFilter(new M_DbQueryFilterWhere('projectId', (int) $projectId, M_DbQueryFilterWhere::EQ));
	}

	/**
	 * Add Filter: Network Member Id
	 * 
	 * @access public
	 * @param mixed $networkMemberId
	 * @return ProjectXNetworkMemberMapper
	 */
	public function addFilterNetworkMemberId($networkMemberId) {
		return $this->addFilter(new M_DbQueryFilterWhere('networkMemberId', (int) $networkMemberId, M_DbQueryFilterWhere::EQ));
	}
	
	/**
	 * Get Network Members By Project Id
	 * 
	 * @access public
	 * @param type $projectId
	 * @return M_ArrayIterator || NULL
	 * 
	 */
	public function getNetworkMembersByProjectId($projectId) {
		
		$ids = $this->getNetworkMembersIdsByProjectId($projectId);
		if( ! $ids) {
			return;
		}
		
		$mapper = M_Loader::getDataObjectMapper('Text', 'text');
		/* @var $mapper TextMapper */
		
		return $mapper->addFilterPublishedOnly()->addFilterDefaultSorting()->getByIds($ids);
		
	}
	
	/**
	 * Get Network Members Ids by Project Id
	 * 
	 * @access public
	 * @param type $projectId
	 * @return M_ArrayIterator || NULL
	 */
	public function getNetworkMembersIdsByProjectId($projectId) {
		
		// New instance of this mapper
		$projectXNetworkMemberMapper = new self();
		
		// Fetch all ProjectNetworkMembers for this project id
		$projectXNetworkMembers = $projectXNetworkMemberMapper
			->addFilterProjectId($projectId)
			->getAll();
		
		// Collect NetworkMember ids
		$networkMemberIds = array();
		foreach($projectXNetworkMembers as $projectXNetworkMember) {
			array_push($networkMemberIds, $projectXNetworkMember->getNetworkMemberId());
		}
		
		if (empty($networkMemberIds)) {
			return NULL;
		}
		
		return $networkMemberIds;
		
	}	
	
	/**
	 * Get Projects by Network Member Id
	 * 
	 * @access public
	 * @param type $projectId
	 * @return M_ArrayIterator || NULL
	 */
	public function getProjectsByNetworkMemberId($networkMemberId) {
		
		// Get project Ids
		$projectIds = $this->getProjectsIdsByNetworkMemberId($networkMemberId);
		
		// Get all Projects
		$projectMapper = M_Loader::getDataObjectMapper('Project', 'portfolio');
		$projects = $projectMapper
			->getByIds($projectIds);
		
		return $projects;
		
	}
	
	/**
	 * Get ProjectIds by Network Member Id
	 * 
	 * @access public
	 * @param type $projectId
	 * @return M_ArrayIterator || NULL
	 */
	public function getProjectsIdsByNetworkMemberId($networkMemberId) {
		
		// New instance of this mapper
		$projectXNetworkMemberMapper = new self();
		
		// Fetch all ProjectNetworkMembers for this network member id
		$projectXNetworkMembers = $projectXNetworkMemberMapper
			->addFilterNetworkMemberId($networkMemberId)
			->getAll();
		
		// Collect Project ids
		$projectIds = array();
		foreach($projectXNetworkMembers as $projectXNetworkMember) {
			array_push($projectIds, $projectXNetworkMember->getProjectId());
		}
		
		if (empty($projectIds)) {
			return NULL;
		}
		
		return $projectIds;
		
	}	
	
	/**
	 * Delete By Project Id
	 * 
	 * @access public
	 * @param mixed $projectId
	 * @return void
	 */
	public function deleteByProjectId($projectId) {
		
		// New instance of this mapper
		$mapper = new self();
		
		// Fetch all ProjectNetworkMembers for this project id
		$projectXNetworkMembers = $mapper
			->addFilterProjectId($projectId)
			->getAll();
		
		// Delete all projectXNetworkMembers
		foreach($projectXNetworkMembers as $projectXNetworkMember) {
			$projectXNetworkMember->delete();
		}
		
	}
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'portfolio';
	}
	
}