<?php
/**
 * ProjectXTeamMember
 * 
 * @package App
 * @subpackage Portfolio
 */

// Load superclass
M_Loader::loadDataObject('ProjectXTeamMemberDataObject','portfolio');

/**
 * ProjectXTeamMember
 * 
 * @package App
 * @subpackage Portfolio
 */
class ProjectXTeamMember extends ProjectXTeamMemberDataObject {
}