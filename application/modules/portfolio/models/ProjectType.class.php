<?php
/**
 * ProjectType
 * 
 * @package App
 * @subpackage Portfolio
 */

// Load superclass
M_Loader::loadDataObject('ProjectTypeDataObject','portfolio');

/**
 * ProjectType
 * 
 * @package App
 * @subpackage Portfolio
 */
class ProjectType extends ProjectTypeDataObject {
}