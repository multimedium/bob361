<?php
/**
 * ProjectXNetworkMember
 * 
 * @package App
 * @subpackage Portfolio
 */

// Load superclass
M_Loader::loadDataObject('ProjectXNetworkMemberDataObject','portfolio');

/**
 * ProjectXNetworkMember
 * 
 * @package App
 * @subpackage Portfolio
 */
class ProjectXNetworkMember extends ProjectXNetworkMemberDataObject {
}