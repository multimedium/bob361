<?php
/** 
 * ProjectDataObject
 * 
 * @see ProjectMapper
 * @see Project
 * @package App
 * @subpackage Portfolio
 */
class ProjectDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link ProjectDataObject::getId()}
	 * - {@link ProjectDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * categoryId
	 * 
	 * This property stores the categoryId. To get/set the value of the
	 * categoryId, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getCategoryId()}
	 * - {@link ProjectDataObject::setCategoryId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_categoryId;
	
	/**
	 * typeId
	 * 
	 * This property stores the typeId. To get/set the value of the typeId,
	 * read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getTypeId()}
	 * - {@link ProjectDataObject::setTypeId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_typeId;
	
	/**
	 * name ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * name. To get/set the value of the name, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getName()}
	 * - {@link ProjectDataObject::getNameId()}
	 * - {@link ProjectDataObject::setName()}
	 * - {@link ProjectDataObject::setNameId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_nameId;
	
	/**
	 * url ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * url. To get/set the value of the url, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getUrl()}
	 * - {@link ProjectDataObject::getUrlId()}
	 * - {@link ProjectDataObject::setUrl()}
	 * - {@link ProjectDataObject::setUrlId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_urlId;
	
	/**
	 * client ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * client. To get/set the value of the client, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getClient()}
	 * - {@link ProjectDataObject::getClientId()}
	 * - {@link ProjectDataObject::setClient()}
	 * - {@link ProjectDataObject::setClientId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_clientId;
	
	/**
	 * budget
	 * 
	 * This property stores the budget. To get/set the value of the budget,
	 * read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getBudget()}
	 * - {@link ProjectDataObject::setBudget()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_budget;
	
	/**
	 * locationName ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * locationName. To get/set the value of the locationName, read the
	 * documentation on 
	 * 
	 * - {@link ProjectDataObject::getLocationName()}
	 * - {@link ProjectDataObject::getLocationNameId()}
	 * - {@link ProjectDataObject::setLocationName()}
	 * - {@link ProjectDataObject::setLocationNameId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_locationNameId;
	
	/**
	 * streetName
	 * 
	 * This property stores the streetName. To get/set the value of the
	 * streetName, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getStreetName()}
	 * - {@link ProjectDataObject::setStreetName()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_streetName;
	
	/**
	 * streetNumber
	 * 
	 * This property stores the streetNumber. To get/set the value of the
	 * streetNumber, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getStreetNumber()}
	 * - {@link ProjectDataObject::setStreetNumber()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_streetNumber;
	
	/**
	 * zipCode
	 * 
	 * This property stores the zipCode. To get/set the value of the zipCode,
	 * read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getZipCode()}
	 * - {@link ProjectDataObject::setZipCode()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_zipCode;
	
	/**
	 * city
	 * 
	 * This property stores the city. To get/set the value of the city, read
	 * the documentation on 
	 * 
	 * - {@link ProjectDataObject::getCity()}
	 * - {@link ProjectDataObject::setCity()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_city;
	
	/**
	 * countryIso
	 * 
	 * This property stores the countryIso. To get/set the value of the
	 * countryIso, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getCountryIso()}
	 * - {@link ProjectDataObject::setCountryIso()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_countryIso;
	
	/**
	 * latitude
	 * 
	 * This property stores the latitude. To get/set the value of the
	 * latitude, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getLatitude()}
	 * - {@link ProjectDataObject::setLatitude()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_latitude;
	
	/**
	 * longitude
	 * 
	 * This property stores the longitude. To get/set the value of the
	 * longitude, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getLongitude()}
	 * - {@link ProjectDataObject::setLongitude()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_longitude;
	
	/**
	 * titleColor
	 * 
	 * This property stores the titleColor. To get/set the value of the
	 * titleColor, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getTitleColor()}
	 * - {@link ProjectDataObject::setTitleColor()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_titleColor;
	
	/**
	 * subTitleColor
	 * 
	 * This property stores the subTitleColor. To get/set the value of the
	 * subTitleColor, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getSubTitleColor()}
	 * - {@link ProjectDataObject::setSubTitleColor()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_subTitleColor;
	
	/**
	 * surface
	 * 
	 * This property stores the surface. To get/set the value of the surface,
	 * read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getSurface()}
	 * - {@link ProjectDataObject::setSurface()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_surface;
	
	/**
	 * projectLeaderId
	 * 
	 * This property stores the projectLeaderId. To get/set the value of the
	 * projectLeaderId, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getProjectLeaderId()}
	 * - {@link ProjectDataObject::setProjectLeaderId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_projectLeaderId;
	
	/**
	 * statusId
	 * 
	 * This property stores the statusId. To get/set the value of the
	 * statusId, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getStatusId()}
	 * - {@link ProjectDataObject::setStatusId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_statusId;
	
	/**
	 * realizationDate
	 * 
	 * This property stores the realizationDate. To get/set the value of the
	 * realizationDate, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getRealizationDate()}
	 * - {@link ProjectDataObject::setRealizationDate()}
	 * 
	 * @access protected
	 * @var M_Date
	 */
	protected $_realizationDate;

	/**
	 * realizationDateText ID
	 *
	 * This property stores the numeric reference to the translated text of
	 * realizationDateText. To get/set the value of the realizationDateText, read
	 * the documentation on
	 *
	 * - {@link ProjectDataObject::getRealizationDateText()}
	 * - {@link ProjectDataObject::getRealizationDateTextId()}
	 * - {@link ProjectDataObject::setRealizationDateText()}
	 * - {@link ProjectDataObject::setRealizationDateTextId()}
	 *
	 * @access protected
	 * @var integer
	 */
	protected $_realizationDateTextId;
	
	/**
	 * introductionText ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * introductionText. To get/set the value of the introductionText, read
	 * the documentation on 
	 * 
	 * - {@link ProjectDataObject::getIntroductionText()}
	 * - {@link ProjectDataObject::getIntroductionTextId()}
	 * - {@link ProjectDataObject::setIntroductionText()}
	 * - {@link ProjectDataObject::setIntroductionTextId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_introductionTextId;
	
	/**
	 * descriptionText ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * descriptionText. To get/set the value of the descriptionText, read the
	 * documentation on 
	 * 
	 * - {@link ProjectDataObject::getDescriptionText()}
	 * - {@link ProjectDataObject::getDescriptionTextId()}
	 * - {@link ProjectDataObject::setDescriptionText()}
	 * - {@link ProjectDataObject::setDescriptionTextId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_descriptionTextId;
	
	/**
	 * order
	 * 
	 * This property stores the order. To get/set the value of the order,
	 * read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getOrder()}
	 * - {@link ProjectDataObject::setOrder()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_order;
	
	/**
	 * new
	 * 
	 * This property stores the new. To get/set the value of the new, read
	 * the documentation on 
	 * 
	 * - {@link ProjectDataObject::getNew()}
	 * - {@link ProjectDataObject::setNew()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_new;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link ProjectDataObject::getPublished()}
	 * - {@link ProjectDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;

    /**
     * highlight
     *
     * This property stores the highlight. To get/set the value of the
     * highlight, read the documentation on
     *
     * - {@link ProjectDataObject::getHighlight()}
     * - {@link ProjectDataObject::setHighlight()}
     *
     * @access protected
     * @var mixed
     */
    protected $_highlight;

    /**
     * Sorting priority
     *
     * @var int
     */
    protected $_sortingPriority;

	/**
	 * Get name
	 * 
	 * Get name in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the name
	 * @return string|NULL
	 */
	public function getName($locale = NULL) {
		return $this->_getLocalizedText('name', $locale);
	}
	
	/**
	 * Get name ID
	 * 
	 * Get numeric reference to the translated name .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNameId() {
		return $this->_nameId;
	}
	
	/**
	 * Set name
	 * 
	 * Set name, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated name
	 * @param string $locale
	 * 	The language in which to set the name
	 * @return void
	 */
	public function setName($text, $locale = NULL) {
		return $this->_setLocalizedText('name', $text, $locale);
	}
	
	/**
	 * Set name ID
	 * 
	 * Set numeric reference to the translated name .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of name
	 * @return void
	 */
	public function setNameId($id) {
		$this->_nameId = $id;
		return $this;
	}
	
	/**
	 * Get url
	 * 
	 * Get url in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the url
	 * @return string|NULL
	 */
	public function getUrl($locale = NULL) {
		return $this->_getLocalizedText('url', $locale);
	}
	
	/**
	 * Get url ID
	 * 
	 * Get numeric reference to the translated url .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getUrlId() {
		return $this->_urlId;
	}
	
	/**
	 * Set url
	 * 
	 * Set url, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated url
	 * @param string $locale
	 * 	The language in which to set the url
	 * @return void
	 */
	public function setUrl($text, $locale = NULL) {
		return $this->_setLocalizedText('url', $text, $locale);
	}
	
	/**
	 * Set url ID
	 * 
	 * Set numeric reference to the translated url .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of url
	 * @return void
	 */
	public function setUrlId($id) {
		$this->_urlId = $id;
		return $this;
	}
	
	/**
	 * Get client
	 * 
	 * Get client in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the client
	 * @return string|NULL
	 */
	public function getClient($locale = NULL) {
		return $this->_getLocalizedText('client', $locale);
	}
	
	/**
	 * Get client ID
	 * 
	 * Get numeric reference to the translated client .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getClientId() {
		return $this->_clientId;
	}
	
	/**
	 * Set client
	 * 
	 * Set client, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated client
	 * @param string $locale
	 * 	The language in which to set the client
	 * @return void
	 */
	public function setClient($text, $locale = NULL) {
		return $this->_setLocalizedText('client', $text, $locale);
	}
	
	/**
	 * Set client ID
	 * 
	 * Set numeric reference to the translated client .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of client
	 * @return void
	 */
	public function setClientId($id) {
		$this->_clientId = $id;
		return $this;
	}
	
	/**
	 * Get locationName
	 * 
	 * Get locationName in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the locationName
	 * @return string|NULL
	 */
	public function getLocationName($locale = NULL) {
		return $this->_getLocalizedText('locationName', $locale);
	}
	
	/**
	 * Get locationName ID
	 * 
	 * Get numeric reference to the translated locationName .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getLocationNameId() {
		return $this->_locationNameId;
	}
	
	/**
	 * Set locationName
	 * 
	 * Set locationName, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated locationName
	 * @param string $locale
	 * 	The language in which to set the locationName
	 * @return void
	 */
	public function setLocationName($text, $locale = NULL) {
		return $this->_setLocalizedText('locationName', $text, $locale);
	}
	
	/**
	 * Set locationName ID
	 * 
	 * Set numeric reference to the translated locationName .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of locationName
	 * @return void
	 */
	public function setLocationNameId($id) {
		$this->_locationNameId = $id;
		return $this;
	}
	
	/**
	 * Get introductionText
	 * 
	 * Get introductionText in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the introductionText
	 * @return string|NULL
	 */
	public function getIntroductionText($locale = NULL) {
		return $this->_getLocalizedText('introductionText', $locale);
	}
	
	/**
	 * Get introductionText ID
	 * 
	 * Get numeric reference to the translated introductionText .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getIntroductionTextId() {
		return $this->_introductionTextId;
	}
	
	/**
	 * Set introductionText
	 * 
	 * Set introductionText, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated introductionText
	 * @param string $locale
	 * 	The language in which to set the introductionText
	 * @return void
	 */
	public function setIntroductionText($text, $locale = NULL) {
		return $this->_setLocalizedText('introductionText', $text, $locale);
	}
	
	/**
	 * Set introductionText ID
	 * 
	 * Set numeric reference to the translated introductionText .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of introductionText
	 * @return void
	 */
	public function setIntroductionTextId($id) {
		$this->_introductionTextId = $id;
		return $this;
	}
	
	/**
	 * Get descriptionText
	 * 
	 * Get descriptionText in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the descriptionText
	 * @return string|NULL
	 */
	public function getDescriptionText($locale = NULL) {
		return $this->_getLocalizedText('descriptionText', $locale);
	}
	
	/**
	 * Get descriptionText ID
	 * 
	 * Get numeric reference to the translated descriptionText .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getDescriptionTextId() {
		return $this->_descriptionTextId;
	}
	
	/**
	 * Set descriptionText
	 * 
	 * Set descriptionText, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated descriptionText
	 * @param string $locale
	 * 	The language in which to set the descriptionText
	 * @return void
	 */
	public function setDescriptionText($text, $locale = NULL) {
		return $this->_setLocalizedText('descriptionText', $text, $locale);
	}
	
	/**
	 * Set descriptionText ID
	 * 
	 * Set numeric reference to the translated descriptionText .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of descriptionText
	 * @return void
	 */
	public function setDescriptionTextId($id) {
		$this->_descriptionTextId = $id;
		return $this;
	}

	/**
	 * Get realizationDateText
	 *
	 * Get realizationDateText in the requested language
	 *
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 *
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the realizationDateText
	 * @return string|NULL
	 */
	public function getRealizationDateText($locale = NULL) {
		return $this->_getLocalizedText('realizationDateText', $locale);
	}

	/**
	 * Get realizationDateText ID
	 *
	 * Get numeric reference to the translated realizationDateText .
	 *
	 * @access public
	 * @return integer
	 */
	public function getRealizationDateTextId() {
		return $this->_realizationDateTextId;
	}

	/**
	 * Set realizationDateText
	 *
	 * Set realizationDateText, in a given language
	 *
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale
	 * categories.
	 *
	 * @access public
	 * @param string $text
	 * 	The translated realizationDateText
	 * @param string $locale
	 * 	The language in which to set the realizationDateText
	 * @return void
	 */
	public function setRealizationDateText($text, $locale = NULL) {
		return $this->_setLocalizedText('realizationDateText', $text, $locale);
	}

	/**
	 * Set realizationDateText ID
	 *
	 * Set numeric reference to the translated realizationDateText .
	 *
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of realizationDateText
	 * @return void
	 */
	public function setRealizationDateTextId($id) {
		$this->_realizationDateTextId = $id;
		return $this;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return ProjectDataObject
	 */
	public function setId($id) {
		$this->_id = $id;
		return $this;
	}
	
	/**
	 * Get categoryId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCategoryId() {
		return $this->_categoryId;
	}
	
	/**
	 * Set categoryId
	 * 
	 * @access public
	 * @param mixed $categoryId
	 * @return ProjectDataObject
	 */
	public function setCategoryId($categoryId) {
		$this->_categoryId = $categoryId;
		return $this;
	}
	
	/**
	 * Get typeId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getTypeId() {
		return $this->_typeId;
	}
	
	/**
	 * Set typeId
	 * 
	 * @access public
	 * @param mixed $typeId
	 * @return ProjectDataObject
	 */
	public function setTypeId($typeId) {
		$this->_typeId = $typeId;
		return $this;
	}
	
	/**
	 * Get budget
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getBudget() {
		return $this->_budget;
	}
	
	/**
	 * Set budget
	 * 
	 * @access public
	 * @param mixed $budget
	 * @return ProjectDataObject
	 */
	public function setBudget($budget) {
		$this->_budget = $budget;
		return $this;
	}
	
	/**
	 * Get streetName
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getStreetName() {
		return $this->_streetName;
	}
	
	/**
	 * Set streetName
	 * 
	 * @access public
	 * @param mixed $streetName
	 * @return ProjectDataObject
	 */
	public function setStreetName($streetName) {
		$this->_streetName = $streetName;
		return $this;
	}
	
	/**
	 * Get streetNumber
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getStreetNumber() {
		return $this->_streetNumber;
	}
	
	/**
	 * Set streetNumber
	 * 
	 * @access public
	 * @param mixed $streetNumber
	 * @return ProjectDataObject
	 */
	public function setStreetNumber($streetNumber) {
		$this->_streetNumber = $streetNumber;
		return $this;
	}
	
	/**
	 * Get zipCode
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getZipCode() {
		return $this->_zipCode;
	}
	
	/**
	 * Set zipCode
	 * 
	 * @access public
	 * @param mixed $zipCode
	 * @return ProjectDataObject
	 */
	public function setZipCode($zipCode) {
		$this->_zipCode = $zipCode;
		return $this;
	}
	
	/**
	 * Get city
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCity() {
		return $this->_city;
	}
	
	/**
	 * Set city
	 * 
	 * @access public
	 * @param mixed $city
	 * @return ProjectDataObject
	 */
	public function setCity($city) {
		$this->_city = $city;
		return $this;
	}
	
	/**
	 * Get countryIso
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCountryIso() {
		return $this->_countryIso;
	}
	
	/**
	 * Set countryIso
	 * 
	 * @access public
	 * @param mixed $countryIso
	 * @return ProjectDataObject
	 */
	public function setCountryIso($countryIso) {
		$this->_countryIso = $countryIso;
		return $this;
	}
	
	/**
	 * Get latitude
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getLatitude() {
		return $this->_latitude;
	}
	
	/**
	 * Set latitude
	 * 
	 * @access public
	 * @param mixed $latitude
	 * @return ProjectDataObject
	 */
	public function setLatitude($latitude) {
		$this->_latitude = $latitude;
		return $this;
	}
	
	/**
	 * Get longitude
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getLongitude() {
		return $this->_longitude;
	}
	
	/**
	 * Set longitude
	 * 
	 * @access public
	 * @param mixed $longitude
	 * @return ProjectDataObject
	 */
	public function setLongitude($longitude) {
		$this->_longitude = $longitude;
		return $this;
	}
	
	/**
	 * Get titleColor
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getTitleColor() {
		return $this->_titleColor;
	}
	
	/**
	 * Set titleColor
	 * 
	 * @access public
	 * @param mixed $titleColor
	 * @return ProjectDataObject
	 */
	public function setTitleColor($titleColor) {
		$this->_titleColor = $titleColor;
		return $this;
	}
	
	/**
	 * Get subTitleColor
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getSubTitleColor() {
		return $this->_subTitleColor;
	}
	
	/**
	 * Set subTitleColor
	 * 
	 * @access public
	 * @param mixed $subTitleColor
	 * @return ProjectDataObject
	 */
	public function setSubTitleColor($subTitleColor) {
		$this->_subTitleColor = $subTitleColor;
		return $this;
	}
	
	/**
	 * Get surface
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getSurface() {
		return $this->_surface;
	}
	
	/**
	 * Set surface
	 * 
	 * @access public
	 * @param mixed $surface
	 * @return ProjectDataObject
	 */
	public function setSurface($surface) {
		$this->_surface = $surface;
		return $this;
	}
	
	/**
	 * Get projectLeaderId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getProjectLeaderId() {
		return $this->_projectLeaderId;
	}
	
	/**
	 * Set projectLeaderId
	 * 
	 * @access public
	 * @param mixed $projectLeaderId
	 * @return ProjectDataObject
	 */
	public function setProjectLeaderId($projectLeaderId) {
		$this->_projectLeaderId = $projectLeaderId;
		return $this;
	}
	
	/**
	 * Get statusId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getStatusId() {
		return $this->_statusId;
	}
	
	/**
	 * Set statusId
	 * 
	 * @access public
	 * @param mixed $statusId
	 * @return ProjectDataObject
	 */
	public function setStatusId($statusId) {
		$this->_statusId = $statusId;
		return $this;
	}
	
	/**
	 * Get realizationDate
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getRealizationDate() {
		return $this->_realizationDate;
	}
	
	/**
	 * Set realizationDate
	 * 
	 * @access public
	 * @param M_Date $realizationDate
	 * @return ProjectDataObject
	 */
	public function setRealizationDate( M_Date $realizationDate = null) {
		$this->_realizationDate = $realizationDate;
		return $this;
	}
	
	/**
	 * Get order
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Set order
	 * 
	 * @access public
	 * @param mixed $order
	 * @return ProjectDataObject
	 */
	public function setOrder($order) {
		$this->_order = $order;
		return $this;
	}
	
	/**
	 * Get new
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getNew() {
		return $this->_new;
	}
	
	/**
	 * Set new
	 * 
	 * @access public
	 * @param mixed $new
	 * @return ProjectDataObject
	 */
	public function setNew($new) {
		$this->_new = $new;
		return $this;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return ProjectDataObject
	 */
	public function setPublished($published) {
		$this->_published = $published;
		return $this;
	}

    /**
     * Get highlight
     *
     * @access public
     * @return mixed
     */
    public function getHighlight() {
        return $this->_highlight;
    }

    /**
     * Set highlight
     *
     * @access public
     * @param mixed $highlight
     * @return ProjectDataObject
     */
    public function setHighlight($highlight) {
        $this->_highlight = $highlight;
        return $this;
    }

    /**
     * Get sorting priority
     *
     * @return int
     */
    public function getSortingPriority()
    {
        return $this->_sortingPriority;
    }

    /**
     * Set sorting priority
     *
     * @param int $sortingPriority
     * @return $this
     */
    public function setSortingPriority($sortingPriority)
    {
        $this->_sortingPriority = (int) $sortingPriority;
        return $this;
    }

	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return ProjectMapper $mapper
	 */
	public function getMapper() {
		return M_Loader::getDataObjectMapper('Project', 'portfolio');
	}
	
}