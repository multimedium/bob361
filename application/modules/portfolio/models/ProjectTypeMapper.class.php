<?php
/**
 * ProjectTypeMapper
 * 
 * @package App
 * @subpackage Portfolio
 */
class ProjectTypeMapper extends M_DataObjectMapper {
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'portfolio';
	}
	
}