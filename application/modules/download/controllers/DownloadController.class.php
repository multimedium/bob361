<?php
/**
 * DownloadController
 * 
 * @package App
 * @subpackage Download
 * 
 */
class DownloadController extends PageController {

	/**
	 * Download a media file and count the download with the DownloadCounter
	 * 
	 * @access public
	 * @param integer $id
	 * 		The Media Item's ID
	 * @return void
	 */
	public function media($id) {
		$this->_download($id, true);
	}
	
	/**
	 * Download a media file without counting this action in the DownloadCounter
	 * To use the DownloadCounter, please use the function {@link DownloadsController::media()}
	 *
	 * @param int $id
	 */
	public function ghost($id) {
		$this->_download($id, false);
	}
	
	public function passwordOk() {
		M_Loader::getView('DownloadFormOkView', 'download')->setViewMode(PageView::VIEW_MODE_IFRAME)->display();
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get MediaMapper
	 * 
	 * @access protected
	 * @return MediaMapper
	 */
	protected function _getMediaMapper() {
		return M_Loader::getDataObjectMapper('Media', 'media');
	}
	
	/**
	 * Get a published media-item by ID
	 *
	 * @param unknown_type $id
	 * @return Media
	 */
	protected function _getMediaById($id) {
		// Look up the media item
		$media = $this->_getMediaMapper()
			->addFilter(new M_DbQueryFilterWhere('published', 1))
			->addFilter(new M_DbQueryFilterWhere('id', (int) $id))
			->getOne();
			
		return $media;
	}
	
	/**
	 * Add Download Count
	 * If no downloadcounter exists, this function creates a new one.
	 * The count is added up with +1
	 *
	 * @param Media $media
	 * @return flag
	 */
	protected function _addDownloadCount(Media $media) {
		//Search for a DownloadCounter
		M_Loader::loadDataObjectMapper('DownloadCounter', 'download');
		$counterMapper = new DownloadCounterMapper();
		$counter = $counterMapper->getByField('mediaItemId', $media->getId())->current();
		
		//If no counter is found: create new counter
		if(empty($counter)) {
			//Create new Download Counter
			M_Loader::loadDataObject('DownloadCounter', 'download');
			$counter = new DownloadCounter();
			$counter->setMediaItemId($media->getId());	
		}
		
		//Add count:
		$counter->setTitle($media->getTitle());
		$counter->addCount();
		$counter->save();
		
		return true;
	}
	
	/**
	 * Download a media-item and increment it's download counter,
	 * authorise the user if a password is required
	 * 
	 * @param int $mediaId
	 * @param bool	$count
	 * 		Do we need to increment the download counter
	 *
	 *
	 * @author Wouter Lenaerts
	 */
	protected function _download($mediaId, $count = true) {
		//Get the media-item
		$media = $this->_getMediaById($mediaId);
		//check if media still exists
		if(!$media) {
			$this->_404();
		}

		// Get the downloadSection
		$myDownloadSection = $this
			->_getDownloadSectionMapper()
			->getDownloadSectionByMedia($media);

		// If the download section exists:
		if($myDownloadSection) {
			// Load the form
			$form = $this->_getDownloadSectionAuthorizationForm($myDownloadSection);
			$form->setRedirectUrl(M_Form::OK, 'download/password-ok');

			// Only run and display the form when the user is not Authorized
			if(! $form->isAuthenticatedForDownload()){
				if(! $form->run()) {
					if (M_Request::getVariable('password')) {
						$form->setErrorMessage('Fout wachtwoord. Probeer opnieuw aub.');
					}
					$view = $this
						->_getDownloadSectionAuthorizationFormView()
					   ->setViewMode(PageView::VIEW_MODE_IFRAME)
						->assign('passwordFalse', 1)
						->setForm($form)
						->display();
					return null;
				}
			}
		}

		// If found:
		if($media) {
			//Get the file
			$file = $media->getFile();

			// If the file exists:
			if($file->exists()) {
				//Add a download count
				if ($count) $this->_addDownloadCount($media);

				if(is_null($media->getTitle())) {
					$filename = $file->getBasename();
				} else {
					$filename = M_Uri::getPathElementFromString($media->getTitle()) . '.' . $file->getFileExtension();
				}

				// Send the download:
				M_Header::sendDownload(
					$file,
					$filename
				);
			}
		}
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get mapper: download sections
	 *
	 * @access protected
	 * @return DownloadSectionMapper $mapper
	 *		The requested data object mapper
	 */
	protected function _getDownloadSectionMapper() {
		return M_Loader::getDataObjectMapper("DownloadSection","download");
	}

	/**
	 * Get form: password form
	 *
	 * @access protected
	 * @param DownloadSection $downloadSection
	 *		The download section that is manipulated by the requested form
	 * @return DownloadSectionAuthorizationForm $form
	 *		The requested form
	 */
	protected function _getDownloadSectionAuthorizationForm(DownloadSection $downloadSection) {
		M_Loader::loadForm('DownloadSectionAuthorizationForm', 'download');
		return new DownloadSectionAuthorizationForm($downloadSection);
	}

	/**
	 * Get view: Password form view
	 *
	 * @access protected
	 * @return DownloadSectionAuthorizationView $view
	 *		The requested view
	 */
	protected function _getDownloadSectionAuthorizationFormView() {
		return M_Loader::getView('DownloadSectionAuthorizationFormView', 'download');
	}
}