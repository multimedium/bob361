<?php
/**
 * DownloadCounterMapper
 * 
 * @see DownloadCounterDataObject
 * @see DownloadCounter
 * @package App
 * @subpackage Download
 */
class DownloadCounterMapper extends M_DataObjectMapper {
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'download';
	}
}