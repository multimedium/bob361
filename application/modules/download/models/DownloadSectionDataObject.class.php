<?php
/** 
 * DownloadSectionDataObject class
 * 
 * @see DownloadSectionMapper
 * @see DownloadSection
 * @package App
 * @subpackage Download
 */
class DownloadSectionDataObject extends M_DataObject {
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link DownloadSectionDataObject::getId()}
	 * - {@link DownloadSectionDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * title ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * title. To get/set the value of the title, read the documentation on 
	 * 
	 * - {@link DownloadSectionDataObject::getTitle()}
	 * - {@link DownloadSectionDataObject::getTitleId()}
	 * - {@link DownloadSectionDataObject::setTitle()}
	 * - {@link DownloadSectionDataObject::setTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_titleId;
	
	/**
	 * description ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * description. To get/set the value of the description, read the
	 * documentation on 
	 * 
	 * - {@link DownloadSectionDataObject::getDescription()}
	 * - {@link DownloadSectionDataObject::getDescriptionId()}
	 * - {@link DownloadSectionDataObject::setDescription()}
	 * - {@link DownloadSectionDataObject::setDescriptionId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_descriptionId;
	
	/**
	 * realm
	 * 
	 * This property stores the realm. To get/set the value of the realm,
	 * read the documentation on 
	 * 
	 * - {@link DownloadSectionDataObject::getRealm()}
	 * - {@link DownloadSectionDataObject::setRealm()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_realm;
	
	/**
	 * password
	 * 
	 * This property stores the password. To get/set the value of the
	 * password, read the documentation on 
	 * 
	 * - {@link DownloadSectionDataObject::getPassword()}
	 * - {@link DownloadSectionDataObject::setPassword()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_password;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link DownloadSectionDataObject::getPublished()}
	 * - {@link DownloadSectionDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;
	
	/**
	 * order
	 * 
	 * This property stores the order. To get/set the value of the order,
	 * read the documentation on 
	 * 
	 * - {@link DownloadSectionDataObject::getOrder()}
	 * - {@link DownloadSectionDataObject::setOrder()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_order;
	
	/**
	 * deletable
	 * 
	 * This property stores the deletable. To get/set the value of the
	 * deletable, read the documentation on 
	 * 
	 * - {@link DownloadSectionDataObject::getDeletable()}
	 * - {@link DownloadSectionDataObject::setDeletable()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_deletable;
	
	/**
	 * mediaAlbumId
	 * 
	 * This property stores the mediaAlbumId. To get/set the value of the
	 * mediaAlbumId, read the documentation on 
	 * 
	 * - {@link DownloadSectionDataObject::getMediaAlbumId()}
	 * - {@link DownloadSectionDataObject::setMediaAlbumId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_mediaAlbumId;
	
	/**
	 * Get title
	 * 
	 * Get title in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the title
	 * @return string|NULL
	 */
	public function getTitle($locale = NULL) {
		return $this->_getLocalizedText('title', $locale);
	}
	
	/**
	 * Get title ID
	 * 
	 * Get numeric reference to the translated title .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTitleId() {
		return $this->_titleId;
	}
	
	/**
	 * Set title
	 * 
	 * Set title, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated title
	 * @param string $locale
	 * 	The language in which to set the title
	 * @return void
	 */
	public function setTitle($text, $locale = NULL) {
		$this->_setLocalizedText('title', $text, $locale);
	}
	
	/**
	 * Set title ID
	 * 
	 * Set numeric reference to the translated title .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of title
	 * @return void
	 */
	public function setTitleId($id) {
		$this->_titleId = $id;
	}
	
	/**
	 * Get description
	 * 
	 * Get description in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the description
	 * @return string|NULL
	 */
	public function getDescription($locale = NULL) {
		return $this->_getLocalizedText('description', $locale);
	}
	
	/**
	 * Get description ID
	 * 
	 * Get numeric reference to the translated description .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getDescriptionId() {
		return $this->_descriptionId;
	}
	
	/**
	 * Set description
	 * 
	 * Set description, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated description
	 * @param string $locale
	 * 	The language in which to set the description
	 * @return void
	 */
	public function setDescription($text, $locale = NULL) {
		$this->_setLocalizedText('description', $text, $locale);
	}
	
	/**
	 * Set description ID
	 * 
	 * Set numeric reference to the translated description .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of description
	 * @return void
	 */
	public function setDescriptionId($id) {
		$this->_descriptionId = $id;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return mixed
	 */
	public function setId($id) {
		$this->_id = $id;
	}
	
	/**
	 * Get realm
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getRealm() {
		return $this->_realm;
	}
	
	/**
	 * Set realm
	 * 
	 * @access public
	 * @param mixed $realm
	 * @return mixed
	 */
	public function setRealm($realm) {
		$this->_realm = $realm;
	}
	
	/**
	 * Get password
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPassword() {
		return $this->_password;
	}
	
	/**
	 * Set password
	 * 
	 * @access public
	 * @param mixed $password
	 * @return mixed
	 */
	public function setPassword($password) {
		$this->_password = $password;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return mixed
	 */
	public function setPublished($published) {
		$this->_published = $published;
	}
	
	/**
	 * Get order
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Set order
	 * 
	 * @access public
	 * @param mixed $order
	 * @return mixed
	 */
	public function setOrder($order) {
		$this->_order = $order;
	}
	
	/**
	 * Get deletable
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getDeletable() {
		return $this->_deletable;
	}
	
	/**
	 * Set deletable
	 * 
	 * @access public
	 * @param mixed $deletable
	 * @return mixed
	 */
	public function setDeletable($deletable) {
		$this->_deletable = $deletable;
	}
	
	/**
	 * Get mediaAlbumId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getMediaAlbumId() {
		return $this->_mediaAlbumId;
	}
	
	/**
	 * Set mediaAlbumId
	 * 
	 * @access public
	 * @param mixed $mediaAlbumId
	 * @return mixed
	 */
	public function setMediaAlbumId($mediaAlbumId) {
		$this->_mediaAlbumId = $mediaAlbumId;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return DownloadSectionMapper $mapper
	 */
	public function getMapper() {
		if (is_null($this->_mapper)) {
			M_Loader::loadDataObjectMapper("DownloadSection","download");
			$this->_mapper = new DownloadSectionMapper();
		}
		return $this->_mapper;
	}
}