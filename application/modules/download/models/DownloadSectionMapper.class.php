<?php
/**
 * DownloadSectionMapper
 * 
 * @see DownloadSectionDataObject
 * @see DownloadSection
 * @package App
 * @subpackage Download
 */
class DownloadSectionMapper extends M_DataObjectMapper {
	
	/* -- PROPERTIES -- */
	
	/**
	 * @param MediaMapper
	 */
	protected $_mediaMapper;
	
	/**
	 * Get the mediamapper
	 * 
	 * We only instantiate the media-mapper once, so we can add filters from
	 * outside this mapper
	 * 
	 * @return MediaMapper
	 */
	public function getMediaMapper() {
		if (is_null($this->_mediaMapper)) {
			M_Loader::loadDataObjectMapper('Media', 'media');
			$this->_mediaMapper = new MediaMapper();
		}
		
		return $this->_mediaMapper;
	}

	/**
	 * Add filter: order
	 *
	 * Orders the items by order index. This allows for the user to determine
	 * item order through dragging in MultiManage
	 *
	 * @access public
	 * @return DownloadSectionMapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFilterOrder() {
		return $this->addFilter(new M_DbQueryFilterOrder(
			'order',
			'ASC'
		));
	}
	
	/* @author Wouter Lenaerts
	 *
	 * Get all the media within a DownloadSection
	 *
	 * @param DownloadSection $section
	 * @return M_ArrayIterator
	 */
	public function getMediaBySection(DownloadSection $section) {

		//Get the album by ID
		$media = $this->getMediaMapper()
			->addFilterPublished()
			->getByField(
			'albumId',
			$section->getMediaAlbumId()
		);
		
		//Return media
		return $media;
	}

	/*
	 * Get DownloadSection from a particular Media object
	 *
	 * @param Media $media
	 * @return DownloadSection
	 */
	public function getDownloadSectionByMedia(Media $media) {
		// Load DownloadSection by AlbumId of Media object
		$select = $this
			->_getFilteredSelect()
			->where(
				$this->getField('mediaAlbumId')->db_name . ' = ?',
				(int) $media->getAlbumId()
			);
		
		return $this->_fetchOne($select->execute());

	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'download';
	}
}