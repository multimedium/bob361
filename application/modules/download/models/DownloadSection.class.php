<?php
/**
 * DownloadSection
 * 
 * @package App
 * @subpackage Download
 */

// Load superclass
M_Loader::loadDataObject('DownloadSectionDataObject', 'download');

/**
 * DownloadSection
 * 
 * @package App
 * @subpackage Download
 */
class DownloadSection extends DownloadSectionDataObject {
	
	/* -- GETTERS -- */
	
	/**
	 * Get published media
	 */
	public function getPublishedMedia() {
		$mapper = $this->getMapper();
		$mapper->getMediaMapper()->addFilter(
			new M_DbQueryFilterWhere('published', 1)
		);
		$mapper->getMediaMapper()->addFilter(
			new M_DbQueryFilterOrder('order')
		);
		return $mapper->getMediaBySection($this);
	}
	
	/**
	 * Can this download section be deleted?
	 * 
	 * @return bool
	 */
	public function getDeletable() {
		//set default value
		if (is_null(parent::getDeletable())) $this->setDeletable(1);
		return parent::getDeletable();
	}
	
	/**
	 * Can this download section be deleted
	 * 
	 * Alias for {@link DownloadSection::getDeletable()}
	 * 
	 * @return bool
	 */
	public function isDeletable() {
		return $this->getDeletable();
	}

	/*
	 * Is this download protected with a password
	 * 
	 * @return bool
	 */
	public function isProtected() {
		return ($this->getPassword() != '');
	}
	
	
	/**
	 * Check if already autheticated for this section (quick and dirty)
	 * 
	 * @return boolean 
	 */
	public function isAuthenticated() {
		// Authorization
		if(! $this->isProtected()){
			// No password for this section
			return true;
		}

		// Take user-provided password data out of the session
		$sessionDownloadSections = M_SessionNamespace::constructFromCookie('download', false)->downloadSections;


		// Compare the downloadSection password with user-provided password for this downloadSection
		$c = isset($sessionDownloadSections[$this->getId()]);
		$c = ($c && $this->getPassword() == $sessionDownloadSections[$this->getId()]);
		if($c) {
			// Password required: The password is correct
			return true;
		}else{
			// Password required: The password is incorrect
			return false;
		}
	}
}