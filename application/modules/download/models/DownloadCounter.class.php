<?php
/**
 * DownloadCounter
 * 
 * @package App
 * @subpackage Download
 */

// Load superclass
M_Loader::loadDataObject('DownloadCounterDataObject', 'download');

/**
 * DownloadCounter
 * 
 * @package App
 * @subpackage Download
 */
class DownloadCounter extends DownloadCounterDataObject {
	/**
	 * Add one count to the object
	 *
	 */
	public function addCount() {
		//Get this count
		$count = $this->getCount();
		//count + 1
		$count++;
		
		//set count
		$this->setCount($count);
	}
}