<?php
/** 
 * DownloadCounterDataObject
 * 
 * @see DownloadCounterMapper
 * @see DownloadCounter
 * @package App
 * @subpackage Download
 */
class DownloadCounterDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link DownloadCounterDataObject::getId()}
	 * - {@link DownloadCounterDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * mediaItemId
	 * 
	 * This property stores the mediaItemId. To get/set the value of the
	 * mediaItemId, read the documentation on 
	 * 
	 * - {@link DownloadCounterDataObject::getMediaItemId()}
	 * - {@link DownloadCounterDataObject::setMediaItemId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_mediaItemId;
	
	/**
	 * count
	 * 
	 * This property stores the count. To get/set the value of the count,
	 * read the documentation on 
	 * 
	 * - {@link DownloadCounterDataObject::getCount()}
	 * - {@link DownloadCounterDataObject::setCount()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_count;
	
	/**
	 * title
	 * 
	 * This property stores the title. To get/set the value of the title,
	 * read the documentation on 
	 * 
	 * - {@link DownloadCounterDataObject::getTitle()}
	 * - {@link DownloadCounterDataObject::setTitle()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_title;
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return DownloadCounterDataObject
	 */
	public function setId($id) {
		$this->_id = $id;
		return $this;
	}
	
	/**
	 * Get mediaItemId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getMediaItemId() {
		return $this->_mediaItemId;
	}
	
	/**
	 * Set mediaItemId
	 * 
	 * @access public
	 * @param mixed $mediaItemId
	 * @return DownloadCounterDataObject
	 */
	public function setMediaItemId($mediaItemId) {
		$this->_mediaItemId = $mediaItemId;
		return $this;
	}
	
	/**
	 * Get count
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCount() {
		return $this->_count;
	}
	
	/**
	 * Set count
	 * 
	 * @access public
	 * @param mixed $count
	 * @return DownloadCounterDataObject
	 */
	public function setCount($count) {
		$this->_count = $count;
		return $this;
	}
	
	/**
	 * Get title
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 * Set title
	 * 
	 * @access public
	 * @param mixed $title
	 * @return DownloadCounterDataObject
	 */
	public function setTitle($title) {
		$this->_title = $title;
		return $this;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return DownloadCounterMapper $mapper
	 */
	public function getMapper() {
		return M_Loader::getDataObjectMapper('DownloadCounter', 'download');
	}
	
}