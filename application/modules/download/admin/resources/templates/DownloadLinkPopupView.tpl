<div id="downloads-popup-window">

	{if $sections|@count > 0}
		
		<h2>{t text="Insert a link to your downloads"}</h2>
		
		<div id="download-section-filter">
					
			<label for="albumSelect">{t text="Show files in Download Section: "}</label>
			<select id="albumSelect" class="albumSelect" name="albumSelect">
				<option value="">-</option>
				{foreach from=$sections item='section'}
						{assign var='items' value=$section->getPublishedMedia()}
						{assign var='count' value=$items->count()}
						{if $count > 0}
							<option {if $activeSection && ($activeSection->getId() == $section->getId())}selected{/if} value="{$section->getId()}">{$section->getTitle()} ({$count})</option>
						{/if}
				{/foreach}
			</select>
					
		</div>
		
		<div class="list">
			
			<table cellpadding="8" cellspacing="0" border="0" width="100%">
				<tr>
					<th>{t text="Title"}</th>
					<th width="80">{t text="Options"}</th>
				</tr>
				{foreach from=$media item='item' name="i"}
					{if $item->getTitle()}
					<tr class="{if $smarty.foreach.i.iteration % 2 == 0}row2{else}row1{/if}">
						<td>
							{$item->getTitle()}
							<div class="small note">
								<a href="{link href="download/ghost/"}{$item->getId()}">{t text="Download now"}</a>
							</div>
						</td>
						<td width="80">
							<a href="{link href="download/media/"}{$item->getId()}/{$item->getBasenameFromTitle()}" class="insertDownloadLink">&larr; {t text="Insert link"}</a>
						</td>
					</tr>
					{/if}
				{/foreach}
			</table>
			
		</div>
		
	{else}
		{t text="No items found."}
	{/if}
</div>