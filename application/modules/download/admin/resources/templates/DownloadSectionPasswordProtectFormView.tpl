<div class="content-pane-viewport fill-remaining-vspace">
	<div class="breadcrumb">
		<ul>
			<li><a href="{link href="admin/overview/download/downloadsections"}">{t text="Download sections"}</a></li>
			<li><span>{t text="Password protect"}</span></li>
		</ul>
	</div>
	{$form->setModuleOwner('admin')}
	{$form->getView()}
</div>