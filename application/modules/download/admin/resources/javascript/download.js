range = '';

// WYSIWYG Editor(s) are initialized
$(document).bind('M_Wysiwyg', function() {
	$('.mceEditor').each(function() {
			if($('.mce_downloads', this).length == 0) {
				addTinyMCEButton($(this));
			}
	});
});

/**
 * Add the button (and its actions) to the tinyMCE toolbar
 */
function addTinyMCEButton($parent) {

	// check if mceEditor exists
	if($('.mceEditor').length > 0) {
	
		if($('#fieldDownloads').length == 0) {
			// Add 'fieldDownloads'-div to the body (reserved for the popup-box)
			$('<div id="fieldDownloads" />').appendTo('body');
		}

		// Add the download-button to all tinyMCE toolbars
		$('<td><a title="Downloads" class="mceButton mceButtonEnabled mce_downloads" href="'+ jQueryBaseHref + '/admin/route/download/field"><span class="mceIcon mce_downloads"/></a></td>').insertBefore($parent.find(".mceToolbarEnd"));
		
		// Bind the click to fancybox window
		$('a.mce_downloads').fancybox({
			openSpeed : 'fast',
			closeSpeed : 'fast',
			prevSpeed : 'normal',
			nextSpeed : 'normal',
			prevEffect : 'fade',
			nextEffect : 'fade',
			closeClick : false,
			helpers : {
				title : {
					type : 'over'
				}
			},
			padding: 5,
			type: 'ajax',
			// On show: add listeners to the fancybox content
			afterShow : function() {
				initializeResults()
			},
			// Just before closing: hide the loading state
			beforeClose: function() {
				// Hide the loading state
				$('#downloads-popup-window .list').M_LoadingState({
					isLoading : false
				});
			}
		});
	}
}

/**
 * Function to process the content after the fancybox is opened
 * 
 */
function initializeResults() {
	// Set actions for the selector (filter on download section)
	$('#downloads-popup-window .albumSelect').change(function(e) {
		// When changed, show loading state
		$('#downloads-popup-window .list').M_LoadingState({
			isLoading : true
		});
		
		// Unbind the event so it isn't fired multiple times on the next change
		//$('#downloads-popup-window .albumSelect').unbind('change');
		
		// Load new downloads view
		$.M_AjaxRequest({
			url : jQueryBaseHref + '/admin/route/download/field/' + $(this).val(),
			dataType : 'html',
			// When loaded:
			success : function(data) {
				// Hide loading state:
				$('#downloads-popup-window .list').M_LoadingState({
					isLoading : false
				});
				
				// Show the new downloads view
				$('#fancybox-inner').html(data);
			}
		})
	});
	
	// Get current selection of text in the editor:
	var t = getSelectedTextInEditor();
	
	// Add the DownloadLink listener
	$('#downloads-popup-window .insertDownloadLink').click(function(e) {
		// Ask the user for the name of the link:
		// (Only if no text has been selected)
		if(jQuery.trim(t) == '') {
			t = prompt('Please insert the name of your new link');
		}
		
		// If text has been introduced:
		if(t) {
			// tinyMCE add this link to the content
			tinyMCE.activeEditor.selection.setContent('<a class="download-link"  href="' + $(this).attr('href') + '">'+ t +'</a>');
			
			// close the fancybox
			$.fancybox.close();
		}
		
		// Override default behavior of link
		e.preventDefault();
		return false;
	});
	
}
 
/**
 * Function to get selection of text
 */
function getSelectedText() {
	if(window.getSelection) {
		return window.getSelection();
	} else if(document.getSelection) {
		return document.getSelection();
	} else if(document.selection) {
		return document.selection.createRange().text;
	} else {
		return;
	}
}

/**
 * Function to get selection of text in editor
 */
function getSelectedTextInEditor() {
	return tinyMCE.activeEditor.selection.getContent();
}
 
 /*
 
 TOM:
 - ONPASTE EVENT VANUIT WORD

 </script>*/