<?php
/**
 * DownloadEventListener
 */
class DownloadEventListener {
	
	/**
	 * Construct
	 *
	 * @param AdminController $controller
	 */
	public function __construct(AdminController $controller) {
		// Add Javascript files to admin page view
		M_ViewJavascript
			::getInstance()
			->addFile(AdminLoader::getModuleAdminSourceFile('download', 'resources/javascript/download.js'), 'admin-download-js');
		
		// Add CSS files to admin page view:
		// Add Javascript files to admin page view
		M_ViewCss
			::getInstance()
			->addFile(AdminLoader::getModuleAdminSourceFile('download', 'resources/css/download.css'), 'admin-download-css');
	}

}