<?php
class AdminDownloadController extends M_Controller {

    /**
     * Lets the user select a download to insert in a {@link M_FieldWysiwyg}
     *
     * @param int $sectionId
     */
    public function field($sectionId = NULL) {

	//Load the view
	AdminLoader::loadView('DownloadLinkPopupView', 'download');
	$view = new DownloadLinkPopupView();

	//Load the album mapper
	M_Loader::loadDataObjectMapper('DownloadSection', 'download');
	$sectionMapper = new DownloadSectionMapper();
	$sections = $sectionMapper->getAll();
	$section = false;

	$documents = array();

	//Get the active album
	if(!empty($sectionId)) {
		$section  = $sectionMapper->getById($sectionId);
	}

	if($section) {
		//Get the documents of the section
		$documents = $sectionMapper->getMediaBySection($section);
		$view->setActiveSection($section);

	} else {

		/*@var $item Media */
		foreach($sections as $section) {
			$media = $sectionMapper->getMediaBySection($section);
			foreach($media as $item) {
				$documents[] = $item;
			}
		}

		//Put the docs in an ArrayIterator
		if($documents) {
			$documents = new ArrayIterator($documents);
		}
	}

	//Set albums and documents
	if($sections) {
		$view->setSections($sections);
	}

	if($documents) {
		$view->setMedia($documents);
	}

	//display
	$view->display();
    }


    /**
	 * Password protect a {@link DownloadSection}
     *
     * @param int $downloadSectionId
     *	    The id of the download section we want to protect
	 * 
     * @return void
	 *
	 * @author Wouter Lenaerts
     */
    public function passwordProtect($downloadSectionId ) {
		// retrieve download section from database
		$downloadSectionMapper = M_Loader::getDataObjectMapper('DownloadSection', 'download');
		$downloadSection = $downloadSectionMapper->getById($downloadSectionId);

		// If the download section cannot be found in the database:
		if($downloadSection == false) {
			// Then, we redirect the overview of download sections
			M_Header::redirect('admin/overview/download/downloadsection');
		}
		// If the download section has been found:
		else {
			// Construct the form
			AdminLoader::loadForm("DownloadSectionPasswordProtectForm", "download");
			$form = new DownloadSectionPasswordProtectForm($downloadSection);

			// set redirect url's
			$form->setRedirectUrl(M_Form::SUCCESS, 'admin/overview/download/downloadsection');
			$form->setRedirectUrl(M_Form::FAILURE, 'admin/overview/download/downloadsection');

			// set messages
			$form->setResultMessage(M_Form::SUCCESS, t('Password has been changed succesfully'));
			$form->setResultMessage(M_Form::FAILURE, t('Password could not be changed'));

			// run and display the form
			if (!$form->run()) {
				AdminLoader::loadView("DownloadSectionPasswordProtectView", "download");
				$view = new DownloadSectionPasswordProtectView();
				$view->setForm($form);
				$view->display();
			}
		}
    }
}