<?php
/**
 * DownloadCounterListViewHelper
 *
 */
class DownloadCounterListViewHelper {

	/**
	 * Set column value
	 * 
	 * Manipulates the values shown in the list view. A string is 
	 * expected from this method.
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param AdminListColumnDefinition $column
	 * 		The column for which to set the value
	 * @param M_DataObject $dataObject
	 * 		The instance for which to set the value
	 * @param mixed $value
	 * 		The value that has been prepared by {@link AdminListView}.
	 * 		This will be the listed value, if not manipulated by this
	 * 		method.
	 * @return string|null
	 */
	public function setColumnValue(AdminListColumnDefinition $column, M_DataObject $dataObject, $value) {
		/*@var $dataObject M_DataObject */
		
		switch($column->getIndex()) {
			case 0:
				// fetch the linked media item
				$media = $this->_getMediaById($dataObject->getMediaItemId());
				// media item still exists
				if($media) {
					// return link to edit form
					return '<a href="'. M_Request::getLink('admin/edit/media/' . $value) . '">' . $media->getTitle() . '</a>';
				// media item no longer exists	
				} else {
					// return the title from the 'counter' object
					return $dataObject->getTitle() . ' <em>(' . t('deleted') . ')</em>';
				}
			default:
				return $value;
		}
	}
	
	/**
	 * Set row options
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param M_DataObject $dataObject
	 * 		The instance for which to set the row options
	 * @return ArrayIterator|null
	 */
	public function setRowOptions(M_DataObject $dataObject) {
		// fetch the linked media item
		$media = $this->_getMediaById($dataObject->getMediaItemId());
		
		// if the media item exists, we show an edit option
		if ($media) {
			return new M_ArrayIterator(array(
				'<a class="edit confirm" title="edit this download item" href="'. M_Request::getLink('admin/edit/media') . '/'.  $media->getId() . '">edit</a>'
			));
		}
		
		// if the media no longer exists, we offer the user the ability to delete
		// this count object
		return new M_ArrayIterator(array(
			'<a class="delete confirm" title="delete this item" href="'. M_Request::getLink('admin/delete/download') . '/'.  $dataObject->getId() . '/downloadcounter">delete</a>'
		));

	}
	
	/**
	 * get media item by download count id
	 * 
	 * @param type $id
	 * @return MediaItem
	 */
	public function _getMediaById($id) {
		M_Loader::loadDataObjectMapper('Media', 'media');
		$mediaMapper = new MediaMapper();
		return $mediaMapper->getOneByFieldWithRegistryCache('id', $id);
	}

}