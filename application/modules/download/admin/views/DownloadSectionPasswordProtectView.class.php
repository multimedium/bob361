<?php
class DownloadSectionPasswordProtectView extends AdminPageView {
	/**
	 * Get the resource for the media view
	 *
	 * @return M_ViewHtmlResource
	 */
	public function getResource(){
		return new M_ViewHtmlResource(
			AdminLoader::getAdminResourcesPath('download') . '/templates/DownloadSectionPasswordProtectFormView.tpl'
		);
	}


	/*
	 * setForm
	 *
	 * @access public
	 * @param DownloadSectionPasswordProtectForm form
	 *
	 * @return void
	 *
	 * @author Wouter Lenaerts
	 */
	public function setForm(DownloadSectionPasswordProtectForm $form){
		$this->assign('form', $form);
	}
	
}