<?php
/**
 * DownloadSectionListViewHelper
 *
 */
class DownloadSectionListViewHelper {

	/**
	 * Set column value
	 * 
	 * Manipulates the values shown in the list view. A string is 
	 * expected from this method.
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param AdminListColumnDefinition $column
	 * 		The column for which to set the value
	 * @param M_DataObject $dataObject
	 * 		The instance for which to set the value
	 * @param mixed $value
	 * 		The value that has been prepared by {@link AdminListView}.
	 * 		This will be the listed value, if not manipulated by this
	 * 		method.
	 * @return string|null
	 */
	public function setColumnValue(AdminListColumnDefinition $column, M_DataObject $dataObject, $value) {
		
		switch($column->getIndex()) {
			case 0:
				return '<a href="'. M_Request::getLink('admin/edit/download/' . $dataObject->getId()) .'/downloadsection">' . $value . '</a>';
			default:
				return $value;
		}
	}
	
	/**
	 * Set row options
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param M_DataObject $dataObject
	 * 		The instance for which to set the row options
	 * @return ArrayIterator|null
	 */
	public function setRowOptions(M_DataObject $dataObject) {
	    if($dataObject->isDeletable() == 0) {
		    $array[] = '<a href="'. M_Request::getLink('admin/edit/' .
		    $dataObject->getMapper()->getModule()->getId() . '/' .
		    $dataObject->getId() . '/' .
		    $dataObject->getMapper()->getModule()->getDataObjectIdOfObject($dataObject)
		    . '" class="edit">' . t('Edit this item') . '</a>');

		    return $array;
	    }
	}

	/**
	 * Add row options
	 *
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param M_DataObject $dataObject
	 * 		The instance for which to add the row options
	 * @return array|null
	 */
	public function addRowOptions(M_DataObject $dataObject) {
	    $url = M_Request::getLink(
		'admin/route/download/password-protect/' . $dataObject->getId()
	    );
	    return array('<a href="' . $url . '">'.t('Password protect').'</a>');
	}
}