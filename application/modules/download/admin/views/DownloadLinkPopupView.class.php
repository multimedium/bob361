<?php
/**
 * DownloadLinkPopupView
 * 
 * @package App
 * @subpackage Download
 */
class DownloadLinkPopupView extends M_ViewHtml {
	
	/* -- PROPERTIES -- */
	
	/**
	 * ActiveSection
	 * 
	 * @access private
	 * @var DownloadSection
	 */
	private $_activeSection;
	
	/**
	 * Sections
	 * 
	 * @access private
	 * @var ArrayIterator
	 *		The collection of {@link DownloadSection} instances
	 */
	private $_sections;
	
	/**
	 * Media
	 * 
	 * @access private
	 * @var ArrayIterator
	 *		The collection of {@link Media} instances
	 */
	private $_media;
	
	/* -- SETTERS -- */
	
	/**
	 * Set Active Section
	 * 
	 * Allows for setting the active {@link DownloadSection} in the view
	 * 
	 * @access public
	 * @param DownloadSection $section
	 * @return DownloadLinkPopupView
	 *		Returns itself, for a fluent programming interface
	 */
	public function setActiveSection(DownloadSection $section) {
		$this->_activeSection = $section;
		return $this;
	}

	/**
	 * Set Sections
	 * 
	 * Allows for setting the collection of available {@link DownloadSection}
	 * instances in the view
	 * 
	 * @access public
	 * @param ArrayIterator $sections
	 * @return DownloadLinkPopupView
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSections(ArrayIterator $sections) {
		$this->_sections = $sections;
		return $this;
	}
	
	/**
	 * Set Media
	 * 
	 * Allows for setting the collection of available {@link Media}
	 * instances to the view
	 * 
	 * @access public
	 * @param ArrayIterator $media
	 * @return DownloadLinkPopupView
	 *		Returns itself, for a fluent programming interface
	 */
	public function setMedia(ArrayIterator $media) {
		$this->_media = $media;
		return $this;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Preprocessing
	 * 
	 * @access public
	 * @return void
	 */
	protected function preProcessing() {
		// Assign presentation variables to the template:
		$this->assign('activeSection', $this->_activeSection);
		$this->assign('sections', $this->_sections);
		$this->assign('media', $this->_media);
	}

	/**
	 * Get the resource for the media view
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(
			AdminLoader::getAdminResourcesPath('download') . '/templates/DownloadLinkPopupView.tpl'
		);
	}
}