<?php
/* 
 * DownloadSectionPasswordProtectForm
 *
 * @see M_FormDataObject
 * @author Wouter Lenaerts
 */
class DownloadSectionPasswordProtectForm extends M_FormDataObject {
	/**
	 * Create a new form, and add all fields
	 *
	 * @param DownloadSection $downloadSection
	 *		The download section to be protected with a password, by this form
	 * @return DownloadSectionPasswordProtectForm
	 */
	public function __construct(DownloadSection $downloadSection) {
		// Construct with a predefined Form ID
		parent::__construct('download-section-password-protect-form');

		// Set the data object in this form:
		$this->setDataObject($downloadSection);

		// Add the fields in the form
		$this
			->_addFieldDownloadSection()
			->_addFieldPassword1()
			->_addFieldPassword2();
	}

	/**
	 * Validate the form
	 *
	 * Check if password1 and password2 are equal,
	 * if they are not equal then send out false
	 *
	 * @return boolean
	 * @author Wouter Lenaerts
	 */
	public function __validate() {
		$password1 = $this->getField('password1')->deliver();
		$password2 = $this->getField('password2')->deliver();

		if($password1 === $password2){
			return true;
		}else {
			$this
				->getField('password2')
				->setErrorMessage(
					t('this password must be the same as the above')
				);
			return false;
		}
	}

	/**
	 * Execute the form
	 *
	 * @param array $variables
	 * @return bool
	 */
	public function actions(array $variables) {
		// Load provided password from the form
		$password = $variables['password1'];

		$downloadSection = $this->getDataObject();

		// If the password has 0 chars, then set password empty
		// else set this password
		if(strlen($password) > 0){
			$downloadSection->setPassword(md5($password));
		}else{
			$downloadSection->setPassword('');
		}

		return $downloadSection->save();
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * addFieldDownloadSection
	 *
	 * Creates a portion of HTML text that displays information according
	 * to the content of the $downloadSection object
	 *
	 * @author Wouter Lenaerts
	 * @return M_FieldHtml
	 */
	protected function _addFieldDownloadSection() {
		$downloadSection = $this->getDataObject();
		
		$field = new M_FieldHtml('info');
		$infohtml =	t(
			'Set a password for download section <strong>@title</strong>. In order ' .
			'to remove the password from the download section, please leave the ' .
			'password fields empty.',
			array(
				'@title' => $downloadSection->getTitle()
			)
		);
		$field->setHtml($infohtml);
		$this->addField($field);
		return $this;
	}

	/**
	 * addFieldPassword1
	 *
	 * Creates a Password field so the user can provide a password
	 *
	 * @author Wouter Lenaerts
	 * @return M_FieldHtml
	 */
	protected function _addFieldPassword1() {
		$field = new M_FieldPassword('password1');
		$field->setModuleOwner('admin');
		$field->setShowingStrength(true);
		$field->setMandatory(false);

		// Password must be a particular minimum amount of characters
		$field->addValidatorObject(
			new M_ValidatorIsNumCharacters(array(
				M_ValidatorIsNumCharacters::MIN => 6
			)),
			t(
				'Password must be at least @count characters',
				array(
					'@count' => 6
				)
			)
		);

		$field->setTitle(t('Set a password'));
		$field->setDescription(t('Add a password, or leave empty to remove'));

		$this->addField($field);

		return $this;
	}

	/**
	 * addFieldPassword2
	 *
	 * Creates a Password field which will be used for the confirmation of
	 * password 1
	 *
	 * @author Wouter Lenaerts
	 * @return M_FieldHtml
	 */
	protected function _addFieldPassword2() {
		$field = new M_FieldPassword('password2');
		$field->setModuleOwner('admin');
		$field->setShowingStrength(true);
		$field->setMandatory(false);
		$field->setTitle(t('Confirm password'));
		$field->setDescription(t('Confirm the password, or leave empty to remove'));
		$this->addField($field);
		return $this;
	}
}