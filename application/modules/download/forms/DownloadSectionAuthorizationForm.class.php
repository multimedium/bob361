<?php

/*
 * DownloadSectionAuthorizationForm
 *
 * @package App
 * @subpackage Download
 *
 * @see M_FormDataObject
 *
 * @author Wouter Lenaerts
*/

class DownloadSectionAuthorizationForm extends M_FormDataObject {

	/**
	 * downloadSection
	 *
	 * This property stores the downloadSection object. It gets set by the
	 * constructor of this class. Getter and Setter are also available.
	 *
	 * @access private
	 * @var DownloadSection
	 */
	private $_downloadSection;

	/**
	 * sessionNamespace
	 *
	 * This property stores a sessionNamespace which contains the
	 * user-provided passwords for each DownloadSection, so these can be
	 * used for Authentication of the download.
	 *
	 * @access private
	 * @var M_SessionNamespace
	 */
	private $_sessionNamespace;

	/**
	 * Create a new form and add all fields
	 * Form needs to have 1 password field, so the user can provide
	 * the password for a particular DownloadSection
	 *
	 * @param DownloadSection $downloadSection
	 *		The specific DownloadSection to which the downloadable Media belongs
	 *
	 * @return void
	 */
    public function __construct(DownloadSection $downloadSection) {
		parent::__construct('download-section-authorization-form');

		$this->_downloadSection = $downloadSection;
		$this->_sessionNamespace = new M_SessionNamespace('download', false);

		$this->addField($this->_getFieldPassword());

	}

	/**
	 * Get mediaItemId
	 *
	 * @access public
	 * @return DownloadSection
	 */
	public function getDownloadSection(){
		return $this->_downloadSection;
	}

	/**
	 * Set downloadSection
	 *
	 * @access public
	 * @param DownloadSection $downloadSection
	 * @return void
	 */
	public function setDownloadSection(DownloadSection $downloadSection){
		$this->_downloadSection = $downloadSection;
	}

	/*
	 * is Authenticated For Download
	 *
	 * Checks whether a password is required for the DownloadSection,
	 * or checks through the session if the user has submitted
	 * the correct password when required
	 *
	 * @return boolean
	 */
	public function isAuthenticatedForDownload() {
		$authenticated =  $this->_downloadSection->isAuthenticated();
	
		return $authenticated;
	}
	

	/**
	 * Form actions
	 *
	 * This will store a user-provided password per downloadSection, in the Session
	 * It returns Authentication.
	 *
	 * @see M_Form::actions()
	 * @see DownloadSectionAuthorizationForm::isAuthenticatedForDownload()
	 *
	 * @access public
	 * @param array $values
	 * 		The values that have been collected by the method {@link M_Form::getValues()}.
	 * @return boolean
	 */
	public function actions(array $variables) {
		// Load variables for use
		$password = md5($variables['password']);
		$downloadSection = $this->_downloadSection;

		// Get the sessionDownloadSections array from the Session,
		// or create a new array
		$sessionDownloadSections = $this->_sessionNamespace->downloadSections;
		if (is_null(($sessionDownloadSections))) {
			$sessionDownloadSections = array();
		}

		// Store password per DownloadSection in the Session
		$sessionDownloadSections[$downloadSection->getId()] = $password;
		$this->_sessionNamespace->downloadSections = $sessionDownloadSections;
		
		return $this->isAuthenticatedForDownload();
	}
	
	/**
	 * get field password
	 * 
	 * @access protected
	 * @return M_FieldPassword 
	 */
	protected function _getFieldPassword() {
		// create Password Field
		$field = new M_FieldPassword('password');
		
		// set properties
		$field->setMandatory(true);
		$field->setDescription(t('Deze download is beveilgd. Gelieve het wachtwoord op te geven.'));
		$field->setDefaultErrorMessage(t('Geef een wachtwoord op aub.'));
		
		// return the field
		return $field;
	}

}