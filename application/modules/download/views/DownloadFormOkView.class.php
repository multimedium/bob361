<?php

/**
 * DownloadSectionAuthorizationView Class
 *
 * @author Peter Van raemdonck
 * @package app
 * @subpackage download
 */
class DownloadFormOkView extends PageView {

	
	/**
	 * Get template
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(
			M_Loader::getResourcesPath('download').'/templates/DownloadFormOk.tpl'
		);
	}
}