<?php

/**
 * DownloadSectionAuthorizationView Class
 *
 * @author Peter Van raemdonck
 * @package app
 * @subpackage download
 */
class DownloadSectionAuthorizationFormView extends PageView {

	/* -- PROPERTIES -- */

	/**
	 * Form
	 *
	 * This property saves the authorization form
	 *
	 * @access private
	 * @var M_Form
	 */
	private $_form;

	/* -- SETTERS -- */

	/**
	 * Set From
	 *
	 * This setter can be used to set the form
	 *
	 * @access public
	 * @param M_From $form
	 * @return DownloadSectionAuthorizationView
	 *		Returns itself, for a fluent programming interface
	 */
	public function setForm(M_Form $form) {
		$this->_form = $form;
		return $this;
	}

	/**
	 * Preprocessing
	 * 
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Make sure the form has been provided
		if(! $this->_form) {
			// Throw an exception, because if not provided we cannot render
			throw new M_ViewException(sprintf(
				'Cannot render the Download Section Authorization Form View; Please provide DownloadSectionAuthorizationForm to %s::%s()',
				__CLASS__,
				'setForm'
			));
		}
		
		// Assign presentation variables
		$this->assign('form', $this->_form);
	}
	
	
	/**
	 * Get template
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(
			M_Loader::getResourcesPath('download').'/templates/DownloadSectionAuthorizationForm.tpl'
		);
	}
}