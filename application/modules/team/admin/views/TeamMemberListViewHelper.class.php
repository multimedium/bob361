<?php
/**
 * TeamMemberListViewHelper
 *
 * @package app
 * @subpackage Team
 */
class TeamMemberListViewHelper {
	
	/* -- PUBLIC FUNCTIONS -- */

	/**
	 * Set column value
	 *
	 * Manipulates the values shown in the list view. A string is
	 * expected from this method.
	 *
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @author Tim Segers
	 * @access public
	 * @param AdminListColumnDefinition $column
	 * 		The column for which to set the value
	 * @param TeamMember $teamMember
	 * 		The instance for which to set the value
	 * @param mixed $value
	 * 		The value that has been prepared by {@link AdminListView}.
	 * 		This will be the listed value, if not manipulated by this
	 * 		method.
	 * @return string|null
	 */
	public function setColumnValue(AdminListColumnDefinition $column, TeamMember $teamMember, $value) {

		switch($column->getIndex()) {

			case 2:
				if($teamMember->getCategory()) {
					return $teamMember->getCategoryName();
				} else {
					return '&nbsp;';
				}
				break;

			default:
				return $value;
				break;
		}
	}
	
}