<?php
/**
 * TeamController
 *
 * @package App
 * @subpackage Team
 */
class TeamController extends PageController {
	
	/**
	 * Index
	 * 
	 * @access public
	 * @return void
	 */
	public function index() {
		
		// Get the TeamCategories
		$mapper = M_Loader::getDataObjectMapper('TeamCategory', 'team');
		/* @var $mapper TeamCategoryMapper */
		
		$categories = $mapper
			->addFilterDefaultSorting()
			->addFilterPublishedOnly()
			->getAll();
		
		// Get the intro text
		M_Loader::loadModuleCoreClass('TeamHelper', 'team');
		$teamHelper = new TeamHelper();
		$introText = $teamHelper->getNetworkText();
		
		// Get Network
		$networkMembers = $teamHelper->getNetworkMembers();
		
		// Construct the view
		$view = M_Loader::getView('TeamMemberListView', 'team');
		/* @var $view TeamMemberListView */
		
		// Set properties to the view
		$view
			->setCategories($categories)
			->setIntroText($introText)
			->setNetworkMembers($networkMembers)
			->setPageTitle('Office')
			->setPageDescription('Office')
			->display();
	}
	
}