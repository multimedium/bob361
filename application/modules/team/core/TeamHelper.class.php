<?php

class TeamHelper extends M_Object {
	
	/**
	 * Get Network Members
	 * 
	 * @access public
	 * @return void
	 */
	public static function getNetworkMembersOptions() {
		$teamHelper = new self;
		$networkMembers = $teamHelper->getNetworkMembers();
		$output = array();
		foreach($networkMembers as $networkMember) {
			$output[$networkMember->getId()] = $networkMember->getTitle();
		}
		return $output;
	}
	
	/**
	 * Get Network Member By Id
	 * 
	 * @access public
	 * @param $id int
	 * @return Text || NULL
	 */
	public function getNetworkMemberById($id) {
		if( ! $id) {
			return;
		}
		$mapper = M_Loader::getDataObjectMapper('Text', 'text');
		/* @var $mapper TextMapper */
		return $mapper->getById($id);
	}
	
	/**
	 * Get Network Members
	 * 
	 * @access public
	 * @return M_ArrayIterator || NULL
	 */
	public function getNetworkMembers() {
		
		// Get the Network Text
		$text = $this->getNetworkText();
		if( ! $text) {
			return;
		}
		
		// Get all members
		return $text->getChildTexts();
		
	}
	
	/**
	 * Get Network Text
	 * 
	 * @access public
	 * @return Text || NULL
	 */
	public function getNetworkText() {
		
		// Construct the mapper that will fetch the texts from the db
		$mapper = M_Loader::getDataObjectMapper('Text', 'text');
		/* @var $mapper TextMapper */
		
		return $mapper->getByRealm('team-intro');
		
	}
	
	
	
}