<?php
/**
 * TeamMemberListView
 *
 * @package App
 * @subpackage Team
 */
class TeamMemberListView extends PageView {
	
	/**
	 * Set Categories
	 * 
	 * @param mixed $categories
	 * @return TeamMemberListView
	 */
	public function setCategories($categories) {
		return $this->assign('categories', $categories);
	}
	
	/**
	 * Set Intro Text
	 * 
	 * @param Text $text
	 * @return TeamMemberListView
	 */
	public function setIntroText(Text $text) {
		return $this->assign('introText', $text);
	}
	
	/**
	 * Set Network Members
	 * 
	 * @param mixed $networkMembers
	 * @return TeamMemberListView
	 */
	public function setNetworkMembers($networkMembers) {
		return $this->assign('networkMembers', $networkMembers);
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get the resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('team', 'TeamMemberList.tpl');
	}
}