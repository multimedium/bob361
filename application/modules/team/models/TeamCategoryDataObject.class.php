<?php
/** 
 * TeamCategoryDataObject
 * 
 * @see TeamCategoryMapper
 * @see TeamCategory
 * @package App
 * @subpackage Team
 */
class TeamCategoryDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link TeamCategoryDataObject::getId()}
	 * - {@link TeamCategoryDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * title ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * title. To get/set the value of the title, read the documentation on 
	 * 
	 * - {@link TeamCategoryDataObject::getTitle()}
	 * - {@link TeamCategoryDataObject::getTitleId()}
	 * - {@link TeamCategoryDataObject::setTitle()}
	 * - {@link TeamCategoryDataObject::setTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_titleId;
	
	/**
	 * url ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * url. To get/set the value of the url, read the documentation on 
	 * 
	 * - {@link TeamCategoryDataObject::getUrl()}
	 * - {@link TeamCategoryDataObject::getUrlId()}
	 * - {@link TeamCategoryDataObject::setUrl()}
	 * - {@link TeamCategoryDataObject::setUrlId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_urlId;
	
	/**
	 * order
	 * 
	 * This property stores the order. To get/set the value of the order,
	 * read the documentation on 
	 * 
	 * - {@link TeamCategoryDataObject::getOrder()}
	 * - {@link TeamCategoryDataObject::setOrder()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_order;
	
	/**
	 * showImages
	 * 
	 * This property stores the showImages. To get/set the value of the
	 * showImages, read the documentation on 
	 * 
	 * - {@link TeamCategoryDataObject::getShowImages()}
	 * - {@link TeamCategoryDataObject::setShowImages()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_showImages;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link TeamCategoryDataObject::getPublished()}
	 * - {@link TeamCategoryDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;
	
	/**
	 * Get title
	 * 
	 * Get title in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the title
	 * @return string|NULL
	 */
	public function getTitle($locale = NULL) {
		return $this->_getLocalizedText('title', $locale);
	}
	
	/**
	 * Get title ID
	 * 
	 * Get numeric reference to the translated title .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTitleId() {
		return $this->_titleId;
	}
	
	/**
	 * Set title
	 * 
	 * Set title, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated title
	 * @param string $locale
	 * 	The language in which to set the title
	 * @return void
	 */
	public function setTitle($text, $locale = NULL) {
		return $this->_setLocalizedText('title', $text, $locale);
	}
	
	/**
	 * Set title ID
	 * 
	 * Set numeric reference to the translated title .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of title
	 * @return void
	 */
	public function setTitleId($id) {
		$this->_titleId = $id;
		return $this;
	}
	
	/**
	 * Get url
	 * 
	 * Get url in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the url
	 * @return string|NULL
	 */
	public function getUrl($locale = NULL) {
		return $this->_getLocalizedText('url', $locale);
	}
	
	/**
	 * Get url ID
	 * 
	 * Get numeric reference to the translated url .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getUrlId() {
		return $this->_urlId;
	}
	
	/**
	 * Set url
	 * 
	 * Set url, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated url
	 * @param string $locale
	 * 	The language in which to set the url
	 * @return void
	 */
	public function setUrl($text, $locale = NULL) {
		return $this->_setLocalizedText('url', $text, $locale);
	}
	
	/**
	 * Set url ID
	 * 
	 * Set numeric reference to the translated url .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of url
	 * @return void
	 */
	public function setUrlId($id) {
		$this->_urlId = $id;
		return $this;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return TeamCategoryDataObject
	 */
	public function setId($id) {
		$this->_id = $id;
		return $this;
	}
	
	/**
	 * Get order
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Set order
	 * 
	 * @access public
	 * @param mixed $order
	 * @return TeamCategoryDataObject
	 */
	public function setOrder($order) {
		$this->_order = $order;
		return $this;
	}
	
	/**
	 * Get showImages
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getShowImages() {
		return $this->_showImages;
	}
	
	/**
	 * Set showImages
	 * 
	 * @access public
	 * @param mixed $showImages
	 * @return TeamCategoryDataObject
	 */
	public function setShowImages($showImages) {
		$this->_showImages = $showImages;
		return $this;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return TeamCategoryDataObject
	 */
	public function setPublished($published) {
		$this->_published = $published;
		return $this;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return TeamCategoryMapper $mapper
	 */
	public function getMapper() {
		return M_Loader::getDataObjectMapper('TeamCategory', 'team');
	}
	
}