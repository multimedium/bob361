<?php
/**
 * TeamMemberMapper
 * 
 * @package App
 * @subpackage Team
 */
class TeamMemberMapper extends M_DataObjectMapper {
	
	/* -- SETTERS -- */
	
	/**
	 * Add filter: Published Only
	 * 
	 * @access public
	 * @return TeamMemberMapper
	 */
	public function addFilterPublishedOnly() {
		return $this->addFilter(new M_DbQueryFilterWhere('published', 1));
	}
	
	/**
	 * Add filter: default sorting
	 * 
	 * @access public
	 * @return TeamMemberMapper
	 * 		Returns itself, for a fluent programming interface
	 */
	public function addFilterDefaultSorting() {
		return $this->addFilter(new M_DbQueryFilterOrder('order', 'ASC'));
	}	
	
	/**
	 * Add Filter: company member
	 * 
	 * @access public
	 * @return TeamMemberMapper
	 */
	public function addFilterCompanyMember() {
		return $this->addFilter(new M_DbQueryFilterWhere('companyMember', '1', M_DbQueryFilterWhere::EQ));
	}
	
	/**
	 * Add Filter: categoryId
	 * 
	 * @access public
	 * @return TeamMemberMapper
	 */
	public function addFilterCategoryId($id) {
		return $this->addFilter(new M_DbQueryFilterWhere('categoryId', $id));
	}
	
	/**
	 * Save
	 * 
	 * @access public
	 * @param M_DataObject $object
	 * @return bool
	 */
	public function save(TeamMember $object) {
		
		// Set URL suffix
		$firstName = $object->getFirstName() ? $object->getFirstName() : '';
		$lastName = $object->getSurnames() ? $object->getSurnames() : '';
		$object->setUrl(M_Uri::getPathElementFromString($firstName . '-' . $lastName));
		
		return parent::save($object);
	}

		/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'team';
	}
	
}