<?php
/**
 * TeamCategory
 * 
 * @package App
 * @subpackage Team
 */

// Load superclass
M_Loader::loadDataObject('TeamCategoryDataObject','team');

/**
 * TeamCategory
 * 
 * @package App
 * @subpackage Team
 */
class TeamCategory extends TeamCategoryDataObject {
	
	/**
	 * Set title
	 * 
	 * @access public
	 * @param mixed $text
	 * @return mixed
	 */
	public function setTitle($text, $locale = NULL) {
		// Set the title
		parent::setTitle($text, $locale);
		
		// Set the URL (suffix), if not already done:
		if(! $this->getUrl($locale)) {
			$this->setUrl(M_Uri::getPathElementFromString($text), $locale);
		}
	}	
	
	/**
	 * Get Members
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getMembers() {
		
		$mapper = M_Loader::getDataObjectMapper('TeamMember', 'team');
		/* @var $mapper TeamMemberMapper */
		
		return $mapper
			->addFilterCategoryId($this->getId())
			->addFilterDefaultSorting()
			->addFilterPublishedOnly()
			->getAll();
		
	}
	
}