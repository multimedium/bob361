<?php
/**
 * TeamCategoryMapper
 * 
 * @package App
 * @subpackage Team
 */
class TeamCategoryMapper extends M_DataObjectMapper {
	
	/* -- SETTERS -- */
	
	/**
	 * Add filter: default sorting
	 * 
	 * @access public
	 * @return TeamCategoryMapper
	 * 		Returns itself, for a fluent programming interface
	 */
	public function addFilterDefaultSorting() {
		return $this->addFilter(new M_DbQueryFilterOrder('order', 'ASC'));
	}
	
	/**
	 * Add filter: published
	 * 
	 * Add a filter so only published texts get fetched from the database.
	 * 
	 * @access public
	 * @return TeamCategoryMapper
	 */
	public function addFilterPublishedOnly() {
		return $this->addFilter(new M_DbQueryFilterWhere('published', 1));
	}	
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get Module Name
	 * 
	 * @access protected
	 * @return string $moduleName
	 */
	protected function _getModuleName() {
		return 'team';
	}
	
}