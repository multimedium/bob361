<?php
/** 
 * TeamMemberDataObject
 * 
 * @see TeamMemberMapper
 * @see TeamMember
 * @package App
 * @subpackage Team
 */
class TeamMemberDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link TeamMemberDataObject::getId()}
	 * - {@link TeamMemberDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * categoryId
	 * 
	 * This property stores the categoryId. To get/set the value of the
	 * categoryId, read the documentation on 
	 * 
	 * - {@link TeamMemberDataObject::getCategoryId()}
	 * - {@link TeamMemberDataObject::setCategoryId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_categoryId;
	
	/**
	 * surnames
	 * 
	 * This property stores the surnames. To get/set the value of the
	 * surnames, read the documentation on 
	 * 
	 * - {@link TeamMemberDataObject::getSurnames()}
	 * - {@link TeamMemberDataObject::setSurnames()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_surnames;
	
	/**
	 * firstName
	 * 
	 * This property stores the firstName. To get/set the value of the
	 * firstName, read the documentation on 
	 * 
	 * - {@link TeamMemberDataObject::getFirstName()}
	 * - {@link TeamMemberDataObject::setFirstName()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_firstName;
	
	/**
	 * url
	 * 
	 * This property stores the url. To get/set the value of the url, read
	 * the documentation on 
	 * 
	 * - {@link TeamMemberDataObject::getUrl()}
	 * - {@link TeamMemberDataObject::setUrl()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_url;
	
	/**
	 * pictureFilepath
	 * 
	 * This property stores the pictureFilepath. To get/set the value of the
	 * pictureFilepath, read the documentation on 
	 * 
	 * - {@link TeamMemberDataObject::getPictureFilepath()}
	 * - {@link TeamMemberDataObject::setPictureFilepath()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_pictureFilepath;
	
	/**
	 * function ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * function. To get/set the value of the function, read the documentation
	 * on 
	 * 
	 * - {@link TeamMemberDataObject::getFunction()}
	 * - {@link TeamMemberDataObject::getFunctionId()}
	 * - {@link TeamMemberDataObject::setFunction()}
	 * - {@link TeamMemberDataObject::setFunctionId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_functionId;
	
	/**
	 * companyMember
	 * 
	 * This property stores the companyMember. To get/set the value of the
	 * companyMember, read the documentation on 
	 * 
	 * - {@link TeamMemberDataObject::getCompanyMember()}
	 * - {@link TeamMemberDataObject::setCompanyMember()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_companyMember;
	
	/**
	 * order
	 * 
	 * This property stores the order. To get/set the value of the order,
	 * read the documentation on 
	 * 
	 * - {@link TeamMemberDataObject::getOrder()}
	 * - {@link TeamMemberDataObject::setOrder()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_order;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link TeamMemberDataObject::getPublished()}
	 * - {@link TeamMemberDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;
	
	/**
	 * Get function
	 * 
	 * Get function in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the function
	 * @return string|NULL
	 */
	public function getFunction($locale = NULL) {
		return $this->_getLocalizedText('function', $locale);
	}
	
	/**
	 * Get function ID
	 * 
	 * Get numeric reference to the translated function .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getFunctionId() {
		return $this->_functionId;
	}
	
	/**
	 * Set function
	 * 
	 * Set function, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated function
	 * @param string $locale
	 * 	The language in which to set the function
	 * @return void
	 */
	public function setFunction($text, $locale = NULL) {
		return $this->_setLocalizedText('function', $text, $locale);
	}
	
	/**
	 * Set function ID
	 * 
	 * Set numeric reference to the translated function .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of function
	 * @return void
	 */
	public function setFunctionId($id) {
		$this->_functionId = $id;
		return $this;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return TeamMemberDataObject
	 */
	public function setId($id) {
		$this->_id = $id;
		return $this;
	}
	
	/**
	 * Get categoryId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCategoryId() {
		return $this->_categoryId;
	}
	
	/**
	 * Set categoryId
	 * 
	 * @access public
	 * @param mixed $categoryId
	 * @return TeamMemberDataObject
	 */
	public function setCategoryId($categoryId) {
		$this->_categoryId = $categoryId;
		return $this;
	}
	
	/**
	 * Get surnames
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getSurnames() {
		return $this->_surnames;
	}
	
	/**
	 * Set surnames
	 * 
	 * @access public
	 * @param mixed $surnames
	 * @return TeamMemberDataObject
	 */
	public function setSurnames($surnames) {
		$this->_surnames = $surnames;
		return $this;
	}
	
	/**
	 * Get firstName
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getFirstName() {
		return $this->_firstName;
	}
	
	/**
	 * Set firstName
	 * 
	 * @access public
	 * @param mixed $firstName
	 * @return TeamMemberDataObject
	 */
	public function setFirstName($firstName) {
		$this->_firstName = $firstName;
		return $this;
	}
	
	/**
	 * Get url
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getUrl() {
		return $this->_url;
	}
	
	/**
	 * Set url
	 * 
	 * @access public
	 * @param mixed $url
	 * @return TeamMemberDataObject
	 */
	public function setUrl($url) {
		$this->_url = $url;
		return $this;
	}
	
	/**
	 * Get pictureFilepath
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPictureFilepath() {
		return $this->_pictureFilepath;
	}
	
	/**
	 * Set pictureFilepath
	 * 
	 * @access public
	 * @param mixed $pictureFilepath
	 * @return TeamMemberDataObject
	 */
	public function setPictureFilepath($pictureFilepath) {
		$this->_pictureFilepath = $pictureFilepath;
		return $this;
	}
	
	/**
	 * Get companyMember
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCompanyMember() {
		return $this->_companyMember;
	}
	
	/**
	 * Set companyMember
	 * 
	 * @access public
	 * @param mixed $companyMember
	 * @return TeamMemberDataObject
	 */
	public function setCompanyMember($companyMember) {
		$this->_companyMember = $companyMember;
		return $this;
	}
	
	/**
	 * Get order
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Set order
	 * 
	 * @access public
	 * @param mixed $order
	 * @return TeamMemberDataObject
	 */
	public function setOrder($order) {
		$this->_order = $order;
		return $this;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return TeamMemberDataObject
	 */
	public function setPublished($published) {
		$this->_published = $published;
		return $this;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return TeamMemberMapper $mapper
	 */
	public function getMapper() {
		return M_Loader::getDataObjectMapper('TeamMember', 'team');
	}
	
}