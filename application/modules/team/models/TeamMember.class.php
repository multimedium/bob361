<?php
/**
 * TeamMember
 * 
 * @package App
 * @subpackage Team
 */

// Load superclass
M_Loader::loadDataObject('TeamMemberDataObject','team');

/**
 * TeamMember
 * 
 * @package App
 * @subpackage Team
 */
class TeamMember extends TeamMemberDataObject {
	
	/**
	 * Get Category Name
	 * 
	 * @access public
	 * @return string
	 */
	public function getCategoryName() {
		
		$category = $this->getCategory();
		if( ! $category) {
			return '';
		}
		
		return $category->getTitle();
		
	}
	
	/**
	 * Get Category
	 * 
	 * @access public
	 * @return void
	 */
	public function getCategory() {
		
		$mapper = M_Loader::getDataObjectMapper('TeamCategory', 'team');
		/* @var $mapper TeamCategoryMapper */
		
		return $mapper
			->addFilterDefaultSorting()
			->addFilterPublishedOnly()
			->getById($this->getCategoryId());
		
	}
	
	/**
	 * Get Full Name
	 * 
	 * @access public
	 * @return string
	 */
	public function getFullName() {
		return $this->getFirstName() . ' ' . $this->getSurnames();
	}
	
	/**
	 * get picture file
	 * 
	 * returns an M_File object of an picture file path
	 * 
	 * @return M_File
	 */
	public function getPictureFile() {
		$file = new M_File($this->getPictureFilepath());
		if (! $file->exists()) {
			return null;
		}
		return $file;
	}
}