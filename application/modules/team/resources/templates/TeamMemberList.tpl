<div class="row row-large">
	<div class="span12">
		<p>
			{$introText->getText()}
		</p>
	</div>
</div>

{* Team members *}			
{if $categories && sizeof($categories) > 0}
	{foreach $categories as $category}
		<h1 class="title-dark">{$category->getTitle()}</h1>
		{include file="_Members.tpl" category=$category}
	{/foreach}
{/if}

{* Network *}
{if $networkMembers && sizeof($networkMembers) > 0}
	<h1 class="title-dark">Network</h1>
	{include file="_NetworkMembers.tpl" networkMembers=$networkMembers}
{/if}