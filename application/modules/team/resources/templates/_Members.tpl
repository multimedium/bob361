{$members = $category->getMembers()}

{foreach $members as $member}
	{$iteration = $member@iteration -1}
	{* open row *}
	{if $member@iteration == 1 || $iteration % 3 == 0}
		<div class="row row-small company">
	{/if}
	<div class="span4">
		
		{* Image *}
		{if $category->getShowImages()}
			{$memberName= $member->getFirstName()|cat:" "|cat:$member->getSurnames()|cat:" - "|cat:$member->getFunction()}
			{$image = $member->getPictureFile()}
			{$imageTitle = $memberName}
			{if $image}
				{$imagePath = {link href="thumbnail/teamMember/{$image->getBasename()}"}}
			{else}
				{$imagePath = {link href="application/resources/images/placeholder2.jpg" prefix='false'}}
				{t text='No image available' assignto='imageTitle'}
			{/if}
			<img src="{$imagePath}" alt="{$imageTitle}" title="{$imageTitle}"/>
		{/if}
		
		{* Name *}
		<div class="title-light">{$member->getFirstName()} {$member->getSurnames()}</div>
		
		{* Function *}
		<p>
			{$member->getFunction()}
		</p>
		
	</div>
	{* close row *}
	{if $member@iteration % 3 == 0 OR $member@last}
		</div>
	{/if}	
{/foreach}