<?php
// Set error reporting
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
ini_set('display_errors', 0);
// header("Location: http://www.bob361.com/placeholder/index.html");
// Load Core classes and M_Loader
require_once 'bootstrap-secure.php';
require_once 'core/Object/Object.class.php';
require_once 'core/M.class.php';
require_once 'core/Loader.class.php';

// Set M_Loader as the autoloader
M_Loader::setAsAutoloader();

// Set date time zone
M_DateTimezone::setDefaultTimezone('Europe/Brussels');

// Use our own exception handler
M_ExceptionErrorHandler::start();

// Load Project-specific files:
require_once 'config/routing.php';
require_once 'config/hash.php';

// Set encoding
$header = new M_Header();
$header->send(M_Header::CONTENT_TYPE_TEXT_HTML, 'charset=utf-8');

// Send P3P headers
M_Header::sendP3P();

// Load configuration of the website:
// - Database connection(s)
M_Loader::loadRelative('config/db-connection.php');

// Start the session:
$session = M_Session::getInstance();
$session->start();

// Register the default database connection (not yet establishing
// the connection though)
M_Db::registerConnectionId(DB_DRIVER, DB_URL);

// Register database connection to central database of errors (exceptions)
//M_Db::registerConnectionId(DB_DRIVER_EXCEPTION, DB_URL_EXCEPTION, DB_ID_EXCEPTION);

// Create a logger of exceptions, and set it up in such a way that errors get
// logged in the central database. The connection to the database is configured
// in config/db-connection.php
//$logger = new M_ExceptionLoggerDb();
//$logger->setDbId(DB_ID_EXCEPTION);

// Set the logger
//M_Exception::setExceptionLogger($logger);

// Construct an instance of the M_ControllerDispatcher class, to handle
// the page request:
$dispatcher = M_ControllerDispatcher::getInstance();

// Get installed locales:
$locales = M_LocaleMessageCatalog::getInstalled();
$localeCount = count($locales);

// If more than one locale is available
if($localeCount > 1) {
	// Get the default locale. The default locale is used to set the 
	// Homepage URI:
	$locale = M_Browser::getLanguageDefaultAndSupported();

	// Set the Homepage URI
	$dispatcher->setHomepageUri(new M_Uri(M_Request::getLink($locale . '/home')));
	
	// Enable locales in the application's paths:
	// (will prepend the locale name to the URL's)
	$dispatcher->setEnableLocale(TRUE);
}
// If only one locale can be used in the app:
elseif($localeCount == 1) {
	// Set the Homepage URI (without locale prefix)
	$dispatcher->setHomepageUri(new M_Uri(M_Request::getLink('home')));

	// Set the default locale:
	M_Locale::setCategory(M_Locale::LANG, $locales[0]);
}
// No locales found.
else {
	// Handle the error:
	$pageController = new PageController();
	$pageController->handleError(new M_Exception('No locales found'));
}

// Set a cache object, to optimize the message catalog:
M_LocaleMessageCatalog::setCacheObject(
	new M_CachePHP(
		M_Loader::getAbsolute('files/cache')
	)
);

// Set up routing
$dispatcher->setRoutingRules($routing);

// Dispatch
$dispatcher->dispatch();
